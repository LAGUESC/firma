﻿//This prints output to the text field on the page
var wgssSignatureSDK;
var sigObj = null;
var sigCtl = null;
var dynCapt = null;
var l_name = null;
var l_reason = null;
//Assumes the default host / port for sig captX (localhost, 8000). Checks for sigcapt service
function startSession() {
    //print("Detecting SigCaptX");
    wgssSignatureSDK = new WacomGSS_SignatureSDK(onDetectRunning, 8000);

    function onDetectRunning() {
        //print("SigCaptX detected");
        clearTimeout(timeout);
    }
    var timeout = setTimeout(timedDetect, 1500);

    function timedDetect() {
        if (wgssSignatureSDK.running) {
            //print("SigCaptX detected");
        }
        else {
            if (wgssSignatureSDK.service_detected) {
                print("SigCaptX service detected, but not the server");
            }
            else {
                print("SigCaptX service not detected");
            }
        }
    }
}
//Reset the session for signature capture
function restartSession(callback) {
    //First, reset the objects 
    wgssSignatureSDK = null;
    sigCtl = null;
    sigObj = null;
    dynCapt = null;
    var timeout = setTimeout(timedDetect, 1500);
    //Startup the SDK - assumes default port
    wgssSignatureSDK = new WacomGSS_SignatureSDK(onDetectRunning, 8000);

    function timedDetect() {
        if (wgssSignatureSDK.running) {
            //print("Signature SDK Service detected.");
            start();
        }
        else {
            print("Signature SDK Service not detected.");
        }
    }

    function onDetectRunning() {
        if (wgssSignatureSDK.running) {
            //print("Signature SDK Service detected.");
            clearTimeout(timeout);
            start();
        }
        else {
            print("Signature SDK Service not detected.");
        }
    }

    function start() {
        if (wgssSignatureSDK.running) {
            sigCtl = new wgssSignatureSDK.SigCtl(onSigCtlConstructor);
        }
    }

    function onSigCtlConstructor(sigCtlV, status) {
        if (wgssSignatureSDK.ResponseStatus.OK == status) {
            //dynCapt = new wgssSignatureSDK.DynamicCapture(onDynCaptConstructor);
            sigCtl.PutLicence("AgAkAMlv5nGdAQVXYWNvbQ1TaWduYXR1cmUgU0RLAgOBAgJkAACIAwEDZQA", onSigCtlPutLicence);
        }
        else {
            print("SigCtl constructor error: " + status);
        }
    }

    function onSigCtlPutLicence(sigCtlV, status) {
        if (wgssSignatureSDK.ResponseStatus.OK == status) {
            dynCapt = new wgssSignatureSDK.DynamicCapture(onDynCaptConstructor);
        }
        else {
            print("SigCtl constructor error: " + status);
        }
    }

    function onDynCaptConstructor(dynCaptV, status) {
        if (wgssSignatureSDK.ResponseStatus.OK == status) {
            sigCtl.GetSignature(onGetSignature);
        }
        else {
            print("DynCapt constructor error: " + status);
        }
    }

    function onGetSignature(sigCtlV, sigObjV, status) {
        if (wgssSignatureSDK.ResponseStatus.OK == status) {
            sigObj = sigObjV;
            sigCtl.GetProperty("Component_FileVersion", onSigCtlGetProperty);
        }
        else {
            print("SigCapt GetSignature error: " + status);
        }
    }

    function onSigCtlGetProperty(sigCtlV, property, status) {
        if (wgssSignatureSDK.ResponseStatus.OK == status) {
            //print("DLL: flSigCOM.dll  v" + property.text);
            dynCapt.GetProperty("Component_FileVersion", onDynCaptGetProperty);
        }
        else {
            print("SigCtl GetProperty error: " + status);
        }
    }

    function onDynCaptGetProperty(dynCaptV, property, status) {
        if (wgssSignatureSDK.ResponseStatus.OK == status) {
            //print("DLL: flSigCapt.dll v" + property.text);
            //print("Test application ready.");
            //print("Press 'Start' to capture a signature.");
            if ('function' === typeof callback) {
                callback();
            }
        }
        else {
            print("DynCapt GetProperty error: " + status);
        }
    }
}
//Do the capture
function Capture() {
    debugger;
    if (wgssSignatureSDK == null || !wgssSignatureSDK.running || null == dynCapt) {
        //print("Session error. Restarting the session.");
        restartSession(window.Capture);
        return;
    }
    dynCapt.Capture(sigCtl, "who", "why", null, null, onDynCaptCapture);
    debugger;

    function onDynCaptCapture(dynCaptV, SigObjV, status) {
        debugger;
        if (wgssSignatureSDK.ResponseStatus.INVALID_SESSION == status) {
            print("Error: invalid session. Restarting the session.");
            restartSession(window.Capture);
        }
        else {
            if (wgssSignatureSDK.DynamicCaptureResult.DynCaptOK != status) {
                print("Capture returned: " + status);
            }
            switch (status) {
                case wgssSignatureSDK.DynamicCaptureResult.DynCaptOK:
                    sigObj = SigObjV;
                    //print("Signature captured successfully");
                    var flags = wgssSignatureSDK.RBFlags.RenderOutputBase64 | wgssSignatureSDK.RBFlags.RenderColor32BPP | wgssSignatureSDK.RBFlags.RenderBackgroundTransparent;
                    var imageBox = document.getElementById("imageBox");
                    sigObj.RenderBitmap("png", imageBox.clientWidth, imageBox.clientHeight, 0.7, 0x00000000, 0x00FFFFFF, flags, 0, 0, onRenderBitmapBase64);
                    break;
                case wgssSignatureSDK.DynamicCaptureResult.DynCaptCancel:
                    print("Signature capture cancelled");
                    break;
                case wgssSignatureSDK.DynamicCaptureResult.DynCaptPadError:
                    print("No capture service available");
                    break;
                case wgssSignatureSDK.DynamicCaptureResult.DynCaptError:
                    print("Tablet Error");
                    break;
                case wgssSignatureSDK.DynamicCaptureResult.DynCaptIntegrityKeyInvalid:
                    print("The integrity key parameter is invalid (obsolete)");
                    break;
                case wgssSignatureSDK.DynamicCaptureResult.DynCaptNotLicensed:
                    print("No valid Signature Capture licence found");
                    break;
                case wgssSignatureSDK.DynamicCaptureResult.DynCaptAbort:
                    print("Error - unable to parse document contents");
                    break;
                default:
                    print("Capture Error " + status);
                    break;
            }
        }
    }

    function onRenderBitmapBase64(sigObjV, bmpObj, status) {
        if (wgssSignatureSDK.ResponseStatus.OK == status) {
            var imageBox = document.getElementById("imageBox");
        }
        else {
            print("Signature Render Bitmap error: " + status);
        }
        $.ajax({
            type: 'POST'
            , url: 'ConsentimientosStandar.aspx'
            , data: { bmpObj }
            , contentType: "application/json; charset=utf-8"
            , dataType: "json"
            , success: function (data) {
                if (data.success == true) { // if true (1)
                    setTimeout(function () { // wait for 5 secs(2)
                        location.reload(); // then reload the page.(3)
                    }, 5000);
                }
            }
        });
    }
}
