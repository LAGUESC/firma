﻿function Mensaje(msg,aux)
{
    switch (msg)
    {
        case 1: { swal({title: "", text: "Consentimiento guardado exitosamente. ", type: "success", confirmButtonColor: "#428bca", confirmButtonText: "Aceptar", closeOnConfirm: false }); break }
        case 2: { swal({ title: "", text: "La operación no esta disponible.\n Intente de nuevo mas tarde.", type: "warning", confirmButtonColor: "#428bca", confirmButtonText: "Aceptar", closeOnConfirm: false }); break }
        case 3: { swal("La operación no esta disponible.\n Intente de nuevo mas tarde"); break }
        case 4: { swal("Registro Nuevo"); break }
        case 5: { swal("Registro Cargado"); break}
        case 6: { swal("Debe diligenciar todas las categorias"); break}
        case 7: { swal("Registro retirado Exitosamente"); break}
        case 8: { swal("Las Contraseñas no coinciden.\n Recuerde que si esta actualiando un usuario que ya existe\n puede dejar los campos de contraseña en blanco para que este continue con ella"); break}
        case 9: { swal("La red se encuentra en los sgtes Distritos:\n " + aux); break}
        case 10: { swal("El Distrito se encuentra en las sgtes Zonas de Venta:\n " + aux); break }
        case 11: { swal({ title: "Está seguro que desea retirar el registro?",text: "",type: "warning",showCancelButton: true,confirmButtonClass: "btn-danger",confirmButtonText: "Sí, retirar!",closeOnConfirm: false},function () {swal("Retirado!", "Registro retirado exitosamente.", "success");}); break }
        case 12: { swal({ title: "Eliminar los registros seleccionados?", text: "", type: "warning", showCancelButton: true, confirmButtonClass: "btn-danger", confirmButtonText: "Sí, eliminar!", closeOnConfirm: false }, function () { swal("Eliminado!", "Registro eliminado exitosamente.", "success"); }); break }
        case 13: { return confirm("Esta seguro que desea guardar el registro?"); break }
        case 14: { swal("Debe ingresar la descripción"); break }
        case 15: { return confirm("Esta seguro que desea reactivar el registro?"); break }
        case 16: { swal("Registro reactivado Exitosamente"); break}
        case 17: { swal("Los campos marcados con asterisco son obligatorios (*)"); break }
        case 18: { swal("El asunto no existe"); break }
        case 19: { swal("La fecha final no puede ser menor a la fecha inicial"); break }
        case 20: { swal("La fecha de corte debe ser menor que las fechas del rango de facturación"); break }
        case 21: { swal("El criterio no existe"); break }
        case 22: { swal("La Hora Inicial no puede ser mayor a la Hora Final."); break }
        case 23: { swal("La semana de la fecha fin no puede ser mayor a la semana seleccionada"); break }
        case 24: { swal("No puede retirar el nivel porque esta asociado a uno o mas usuarios"); break }        
        case 25: { return confirm("Esta seguro de cancelar la operacion?"); break }
        case 26: { return confirm("Esta seguro que desea eliminar el archivo?"); break }
        case 27: { swal("El rango de salas no es consistente."); break }        
        case 28: { swal("No puede retirar el menú porque esta asociado a uno o mas " + aux); break }                   
        case 29: { swal("No puede retirar la aplicación porque esta asociado a uno o mas " + aux); break }     
        case 30: { swal("No puede retirar el modulo porque esta asociado a uno o mas " + aux); break }     
        case 31: { swal("No puede retirar el submodulo porque esta asociado a uno o mas casos de soporte"); break }     
        case 32: { swal("No puede retirar la prioridad porque esta asociado a uno o mas casos de soporte"); break }     
        case 33: { swal("No puede retirar la compañia porque esta asociado a uno o mas clientes"); break }     
        case 34: { swal("La actividad fue registrada con exito,\n pero no se envio correo electronico por un problema con la dirección del responsable"); break }     
        case 35: { swal("No puede retirar el estado porque esta asociado a uno o mas " + aux); break }     
        case 36: { swal("No puede retirar el pais porque esta asociado a uno o mas " + aux); break }     
        case 37: { swal("No puede retirar el departamento porque esta asociado a uno o mas " + aux); break }     
        case 38: { swal("No puede retirar la actividad porque esta asociada a uno o mas registros de actividades"); break }     
        case 39: { swal("No puede retirar porque esta esta asociada a uno o mas " + aux); break }     
        case 40: { swal("No puede retirar la clasificacion porque esta asociada a uno o mas casos de soporte"); break }     
        case 41: { swal("No puede retirar el cliente porque esta asociada a uno o mas" + aux); break }     
        case 42: { swal("No puede retirar el asunto porque esta asociada a uno o mas casos de soporte"); break }     
        case 43: { swal("No puede retirar el municipio porque esta asociada a uno o mas clientes"); break }     
        case 44: { swal("No puede retirar la etapa porque esta asociada a una o mas actividades"); break }     
        
        
        case 44: { return confirm("Esta seguro que desea desactivar el pais?\ndesactivara: Departamentos,Municipios y Clientes asociados a este pais"); break }        
        case 45: { return confirm("Esta seguro que desea desactivar los paises seleccionados?\ndesactivara: Departamentos,Municipios y Clientes asociados a estos paises"); break }        
        
        case 46: { return confirm("Esta seguro que desea desactivar el departamento?\ndesactivara: Municipios y Clientes asociados a este departamento"); break }        
        case 47: { return confirm("Esta seguro que desea desactivar los departamentos seleccionados?\ndesactivara: Municipios y Clientes asociados a estos departamentos"); break }        
        
        case 48: { return confirm("Esta seguro que desea desactivar el municipio?\ndesactivara los Clientes asociados a este departamento"); break }        
        case 49: { return confirm("Esta seguro que desea desactivar los municipios seleccionados?\ndesactivara los Clientes asociados a estos municipios"); break }        
        
        case 50: { return confirm("Esta seguro que desea desactivar la aplicacion?\ndesactivara: Menu, Modulos, Submodulos y Clientes asociados a esta aplicacion"); break }        
        case 51: { return confirm("Esta seguro que desea desactivar las aplicaciones seleccionadas?\ndesactivara: Menu, Modulos, Submodulos y Clientes asociados a estas aplicaciones"); break }        
        
        case 52: { return confirm("Esta seguro que desea desactivar el menu?\ndesactivara: Modulos y Submodulos asociados a este menu"); break }        
        case 53: { return confirm("Esta seguro que desea desactivar los menu seleccionados?\ndesactivara: Modulos y Submodulos asociados a estos menus"); break }        
                   
        case 54: { return confirm("Esta seguro que desea desactivar el modulo?\ndesactivara los Submodulos asociados a este modulo"); break }        
        case 55: { return confirm("Esta seguro que desea desactivar los modulos seleccionados?\ndesactivara Submodulos asociados a estos modulos"); break }                           
        
        case 56: { return confirm("Esta seguro que desea desactivar el submodulo?"); break }        
        case 57: { return confirm("Esta seguro que desea desactivar los submodulos seleccionados?"); break }  
        
        case 58: { return confirm("Esta seguro que desea desactivar el area?\ndesactivara los cargos asociados a esta"); break }        
        case 59: { return confirm("Esta seguro que desea desactivar las areas seleccionados?\ndesactivara cargos asociados a estas"); break }                                                    

        case 60: { swal({ title: "", text: "Falta diligenciar algunos campos que son requeridos.", type: "warning", confirmButtonColor: "#428bca", confirmButtonText: "Aceptar", closeOnConfirm: false }); break }
        case 61: { swal({ title: "", text: "No existe resultado para la búsqueda.", type: "warning", confirmButtonColor: "#428bca", confirmButtonText: "Aceptar", closeOnConfirm: false }); break }
        
        case 62: { swal("No puede Asignar una Actividad para una semana que ya paso"); break }
        case 63: { swal("Usuario y/o Clave Incorrectos"); break }
        case 64: { swal("Debe indicar al menos un solo parametro (Servicio - Especialidad - Rango Fechas)"); break }
        case 65: { swal("No existe registros en base de datos para la busqueda realizada"); break }
        case 66: { swal("Ha diligenciado de forma erronea el correo, por favor verifique"); break }
        case 67: { swal("La sesion del usuario ha expirado. Por favor ingrese nuevamente a la aplicacion"); break }
        case 68: { swal("Falta o ha diligenciado erroneamente algunos campos en el formulario. Por favor verifique"); break }
        case 69: { swal("Ocurrieron incidencias en la busqueda del paciente, por favor dirigase con el personal de Informatica"); break }
        case 70: { swal("Ocurrieron incidencias en la busqueda del Diagnostico del paciente, por favor dirigase con el personal de Informatica"); break }
        case 71: { swal("Ocurrieron incidencias en la generación del anexo, por favor dirigase con el personal de Informatica"); break }
        case 72: { swal("Ocurrieron incidencias en la busqueda del correo de la entidad a la cual se le enviará el anexo, por favor dirigase con el personal de Informatica"); break }
        case 73: { swal("Ocurrieron incidencias con el envio de correo, por favor dirigase con el personal de Informatica"); break }
        case 74: { swal("Ocurrieron incidencias al generar el pdf para imprimir, por favor dirigase con el personal de Informatica"); break }
        case 75: { swal("Ocurrieron incidencias al buscar el usuario en sesión, por favor dirigase con el personal de Informatica"); break }
        case 76: { swal("No existe resultado para la busqueda, o el anexo ya se encuentra en estado cerrado"); break }
        case 77: { swal("Ocurrieron incidencias en la busqueda de las prestaciones por defecto, por favor dirigase con el personal de Informatica"); break }
        case 78: { swal("El convenio del paciente no tiene registrado correo electronico"); break }
        case 79: { swal("La cama seleccionada no tiene asociada ningun paciente, por favor verifique"); break }
        case 80: { swal("Falta o ha diligenciado erroneamente algunos campos en el formulario ó falta seleccionar la persona que reporta."); break }
        case 81: { swal("Debe seleccionar un valor de hora diferente a 00:00."); break }
        case 82: { swal("Falta diligenciar algunos campos que son requeridos"); break }
        case 83: { swal("Se encontraron problemas en la gestión de la autorización. Por favor consulte con Sistemas"); break }
        case 84: { swal({ title: "", text: "La fecha final no puede ser mayor a la fecha actual.", type: "warning", confirmButtonColor: "#428bca", confirmButtonText: "Aceptar", closeOnConfirm: false }); break }
        case 85: { swal({ title: "", text: "La fecha inicial no puede ser mayor a la fecha final.", type: "warning", confirmButtonColor: "#428bca", confirmButtonText: "Aceptar", closeOnConfirm: false }); break }
        case 86: { swal({ title: "", text: "La fecha inicial no puede ser mayor a la fecha actual.", type: "warning", confirmButtonColor: "#428bca", confirmButtonText: "Aceptar", closeOnConfirm: false }); break }

        default: {  return false; break }
    }
}


