﻿function SoloNumeros(evt)
{
    var nav4 = window.Event ? true : false;
    // NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57	
    var key = nav4 ? evt.which : evt.keyCode;	
    return (key <= 13 || (key >= 48 && key <= 57));
}


function SoloLetras(evt) { 
    tecla = (document.all) ? evt.keyCode : evt.which; 
    if (tecla==8) return true;
    patron =/[A-Za-z\s]/; 
    te = String.fromCharCode(tecla);
    return patron.test(te);
} 

function LetrasyNum(evt) { 
    tecla = (document.all) ? evt.keyCode : evt.which; 
    if (tecla==8) return true;
    patron =/[A-Za-z\s|0-9]/; 
    te = String.fromCharCode(tecla);
    return patron.test(te);
} 

function SoloDecimal(evt)
{
    var nav4 = window.Event ? true : false;
    // NOTE: Backspace = 8, Enter = 13, '0' = 48, '9' = 57	, ',' = 44
    var key = nav4 ? evt.which : evt.keyCode;	
    return (key <= 13 || (key >= 48 && key <= 57) || key == 44 || key == 46);
}

function Semana()
{
    __doPostBack('ctl00$ContentPlaceHolder1$lnkSemana','')
}

function ValidaSemana()
{
    __doPostBack('ctl00$ContentPlaceHolder1$lnkValidaSemana','')
}

function mascara(d,sep,nums){
pat = new Array(2,2,4)
if(d.valant != d.value){
	val = d.value
	largo = val.length
	val = val.split(sep)
	val2 = ''
	for(r=0;r<val.length;r++){
		val2 += val[r]	
	}
	if(nums){
		for(z=0;z<val2.length;z++){
			if(isNaN(val2.charAt(z))){
				letra = new RegExp(val2.charAt(z),"g")
				val2 = val2.replace(letra,"")
			}
		}
	}
	val = ''
	val3 = new Array()
	for(s=0; s<pat.length; s++){
		val3[s] = val2.substring(0,pat[s])
		val2 = val2.substr(pat[s])
	}
	for(q=0;q<val3.length; q++){
		if(q ==0){
			val = val3[q]
		}
		else{
			if(val3[q] != ""){
				val += sep + val3[q]
				}
		}
	}
	d.value = val
	d.valant = val
	}
}



function cargarReferencia(referencia)
{
    location.href = 'Referencias.aspx?ref=' + referencia;
}

function redirect(url)
{
    location.href = url;
}

function clickButton(e, buttonid)
{
      var evt = e ? e : window.event;
      var bt = document.getElementById(buttonid);
      if (bt){
          if (evt.keyCode == 13){
             bt.click();
                return false;
          }
      }
}

function cargaPromociones(ctrl,vlr,ctrl2,vlr2)
{
    window.opener.document.getElementById(ctrl).value = vlr;
    window.opener.document.getElementById(ctrl2).value = vlr2;
    window.close();
}

function cargarOpener(url)
{
    window.opener.location.href = url;
    window.opener.focus();
    window.close();
}

function Mayus(e)
{  
     e.value = e.value.toUpperCase();
} 

function PonerComa(e)
{  
    e.value = e.value.replace('.', ',');
} 

function abrirVentana(url)
{
    winNew = open(url,'NewWindow','top=0,left=0, width=650,height=400,status=yes, resizable=yes,scrollbars=yes');    
    winNew.focus();
}

function SeleccionarText(id)
{
    document.getElementById(id).focus();
    document.getElementById(id).select(); 
}

function comprobarRed(valor,id)
{
    //control = document.getElementById(id);
    if(trim(valor) != '')
    {
        __doPostBack('ctl00$ContentPlaceHolder1$lnkComprobar','');
    }
    //else        
      //  control.focus();
}

function enterRed(e)
{
      var evt = e ? e : window.event;
          if (evt.keyCode == 13){
            __doPostBack('ctl00$ContentPlaceHolder1$lnkComprobar','');
            return false;
          }
}

function postNivelGrid(fila)
{
    //alert('ctl00$ContentPlaceHolder1$gvNivel$ctl0' + fila + '$lnkNive');
    if(fila < 10)
        __doPostBack('ctl00$ContentPlaceHolder1$gvNivel$ctl0' + fila + '$lnkNivel','')
    else
        __doPostBack('ctl00$ContentPlaceHolder1$gvNivel$ctl' + fila + '$lnkNivel','')
}

function postRedGrid(fila)
{
    //alert('ctl00$ContentPlaceHolder1$gvNivel$ctl0' + fila + '$lnkNive');
    if(fila < 10)
        __doPostBack('ctl00$ContentPlaceHolder1$gvRed$ctl0' + fila + '$lnkRed','')
    else
        __doPostBack('ctl00$ContentPlaceHolder1$gvRed$ctl' + fila + '$lnkRed','')
}

function trim(cadena)
{
	for(i=0; i<cadena.length; )
	{
		if(cadena.charAt(i)==" ")
			cadena=cadena.substring(i+1, cadena.length);
		else
			break;
	}

	for(i=cadena.length-1; i>=0; i=cadena.length-1)
	{
		if(cadena.charAt(i)==" ")
			cadena=cadena.substring(0,i);
		else
			break;
	}
	
	return(cadena);
}

function validarControlesBoton(ctrlValidar, ctrlValidador, vlrValidar)
{
    estado = true;    
    for (i = 0; i < ctrlValidar.length; i++)
    {       
        ctrlValida = document.getElementById(ctrlValidar[i]);
        ctrlValidado = document.getElementById(ctrlValidador[i]);        

        if(ctrlValida.value != vlrValidar[i])
        {
            ctrlValidado.innerHTML = '';
        }
        else
        {
            ctrlValidado.innerHTML = '*';
            estado = false
        }
    }
    return estado    
}

function validarControlesBotonMsg(ctrlValidar, ctrlValidador, vlrValidar)
{  
    estado = Mensaje(2);
    if(estado == true)
    {
        for (i = 0; i < ctrlValidar.length; i++)
        {       
            ctrlValida = document.getElementById(ctrlValidar[i]);
            ctrlValidado = document.getElementById(ctrlValidador[i]);
            
            if(ctrlValida.value != vlrValidar[i])
            {
                ctrlValidado.innerHTML = '';
            }
            else
            {
                ctrlValidado.innerHTML = '*';
                estado = false
            }
        }
        return estado
    } 
}

function enterEventValidate(e, buttonid,ctrlValidar, ctrlValidador, vlrValidar)
{
      var evt = e ? e : window.event;    
          if (evt.keyCode == 13){
            if(validarControlesBotonMsg(ctrlValidar, ctrlValidador, vlrValidar))
                __doPostBack(buttonid,'');
          }
}

function enterEvent(e, buttonid)
{
      var evt = e ? e : window.event;
          if (evt.keyCode == 13){
            __doPostBack(buttonid,'');
                //return false;
          }
}

function CheckAllCheckBoxes(checkVal) 
{		
    var total = 0;
    for(i = 0; i < document.forms[0].elements.length; i++) 
    {
        elm = document.forms[0].elements[i]
        
        if (elm.type == 'checkbox' && elm.disabled == false) 
        {
            elm.checked = checkVal;
            total++;
        }
    }
    
    
} 

function CheckAllCheckBoxesMaestros(checkVal,idBtn,checkId) 
{		
    var total = 0;
    for(i = 0; i < document.forms[0].elements.length; i++) 
    {
        elm = document.forms[0].elements[i]
        
        if (elm.type == 'checkbox' && elm.disabled == false) 
        {
            elm.checked = checkVal;
            total++;
        }
    }
    
    if(total > 1)
    {
        HabilitarBotonBorrarCheck(idBtn,checkVal)
    }
    else
    {
        checkId.checked = false;
    }
} 

function HabilitarBotonBorrarCheck(idBtn,valCheck)
{            
    for(i = 0; i < document.forms[0].elements.length; i++) 
    {
        elm = document.forms[0].elements[i]
        
        if (elm.type == 'checkbox' && elm.checked) 
        {
            document.getElementById(idBtn).src = '../icons/eliminar.gif';  
            document.getElementById(idBtn).disabled = false;
            break;     
        }
        else
        {
            document.getElementById(idBtn).src = '../icons/eliminar_d.gif';
            document.getElementById(idBtn).disabled = true;
        }
    }
}

function nitRazonSocial(cadena,idRazon)
{
    document.getElementById(idRazon).value = cadena;
}

function tabular(e,obj) 
{
    tecla=(document.all) ? e.keyCode : e.which;
    if(tecla!=13) return;
        frm=obj.form;
    for(i=0;i<frm.elements.length;i++) 
    if(frm.elements[i]==obj) 
    { 
      if (i==frm.elements.length-1) i=-1;
        break 
    }
    frm.elements[i+1].focus();
    return false;
} 

function negocioDatos(negocio, idcliente)
{
    if (trim(negocio.value) == '')
        negocio.value = document.getElementById(idcliente).value
}

function SetActiveTab(tabControl, tabNumber)
{
  var ctrl = $find(tabControl);
  ctrl.set_activeTab(ctrl.get_tabs()[tabNumber]);
}

function CambiarTab(tabControl, tabNumber,e,idfoco)
{
    var evt = e ? e : window.event;
      if (evt.keyCode == 13)
      {
            SetActiveTab(tabControl, tabNumber);
            document.getElementById(idfoco).focus();
            return false;
      }
}

function checkActividad(chk,txt)
{
    ctrlCheck = document.getElementById(chk);
    ctrlTxt = document.getElementById(txt);
    
    if(ctrlCheck.checked)
    {
        ctrlTxt.value = '100';
    }
    else
    {
        ctrlTxt.value = '';
    }
}

function txtActividad(chk,txt)
{
    ctrlCheck = document.getElementById(chk);
    ctrlTxt = document.getElementById(txt);
    if(ctrlCheck.checked)
    {
        ctrlTxt.value = '100';
        return false;
    }
    else
    {
        if(ctrlTxt.value.replace(',','.') > 100)
        {
            ctrlTxt.value = '100';
            return false;
        }
    }
}

function ChecksGrid(idcheck,idgrid,filas,check)
{    
    for (var i=0;i<filas;i++)
    {
        if (i < 10)
        {
            document.forms.aspnetForm[idgrid + '_ctl0' + (i + 2) + '_' + idcheck].checked = check;
        }
        else
        {
            document.forms.aspnetForm[idgrid + '_ctl' + (i + 2) + '_' + idcheck].checked = check;
        }
    }
}


function mulmaxlength(e,m)
{                     
    if (e.value.length > m)
    {            
        e.value = e.value.substring(0,m);                   
    }    
}

function PDF()
{
    var mywindow = window.open("VerPDF.aspx", "mywindow", "resizable=no,location=1,status=1,scrollbars=1,width=1200,height=900");
    mywindow.moveTo(400, 100);
}