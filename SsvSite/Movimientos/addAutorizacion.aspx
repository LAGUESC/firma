﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="addAutorizacion.aspx.cs" Inherits="Movimientos_addAutorizacion" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addAutorizacion.aspx.cs" Inherits="Movimientos_addAutorizacion" Title="Agregar Autorizaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    

    
    <table style="width: 100%">
        <tr>
            <td>
                <table align="center" style="width: 328px">
                    <tr>
                        <td align="left">Tipo Identificacion:</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlTipoId" runat="server" CssClass="form_input" Height="20px" Width="151px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Identificacion:</td>
                        <td align="left">
                            <asp:TextBox ID="txtId" runat="server" CssClass="form_input" Width="143px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnConsultar" runat="server" CssClass="btn" onmouseout="this.className='btn'" onmouseover="this.className='btnhov'" Text="Consultar" />
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel1" runat="server" CssClass="form_input">
                    <table style="width: 100%">
                        <tr>
                            <td>
                                <asp:Panel ID="Panel2" runat="server" CssClass="form_input">
                                    <table style="width: 100%">
                                        <tr>
                                            <td style="width: 140px">Paciente:</td>
                                            <td>
                                                <asp:TextBox ID="txtNombre" runat="server" CssClass="form_input" Enabled="False" Width="381px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 140px">Motivo de la Solicitud:</td>
                                            <td>
                                                <asp:TextBox ID="TextBox1" runat="server" CssClass="form_input" Height="44px" TextMode="MultiLine" Width="381px"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="center" colspan="2">
                                                <asp:CheckBox ID="CheckBox1" runat="server" Text="% Copago" />
                                                &nbsp;
                                                <asp:TextBox ID="txtCopago" runat="server" CssClass="form_input" Enabled="False">0</asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center"><b>Autorizacion</b></td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel3" runat="server" CssClass="form_input">
                                    <table style="width: 100%">
                                        <tr>
                                            <td align="center" bgcolor="#CCCCCC" style="color: #FFFFFF" colspan="3"><b>Datos Generales</b></td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:CheckBox ID="CheckBox2" runat="server" Text="No Autoriza--Causa" CssClass="form_input" />
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="DropDownList1" runat="server" Width="177px" CssClass="form_input" Height="24px">
                                                </asp:DropDownList>
                                            </td>
                                            <td align="right">
                                                <asp:TextBox ID="TextBox2" runat="server" Width="91px" CssClass="form_input"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                   
                                    <table style="width: 100%">
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                            <td>Entidad Autorizadora</td>
                                        </tr>
                                        <tr>
                                            <td>Nro. Autorizacion</td>
                                            <td>Alcance</td>
                                            <td>
                                                <asp:DropDownList ID="DropDownList2" runat="server" Height="24px" Width="216px" CssClass="form_input">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="TextBox3" runat="server" CssClass="form_input"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="DropDownList5" runat="server" Height="24px" Width="155px" CssClass="form_input">
                                                </asp:DropDownList>
                                            </td>
                                            <td>Convenio</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">&nbsp;</td>
                                            <td>
                                                <asp:DropDownList ID="DropDownList3" runat="server" Height="24px" Width="216px" CssClass="form_input">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td align="right">Nro. Horas</td>
                                            <td>Autorizado Por</td>
                                        </tr>
                                        <tr>
                                            <td style="height: 26px"></td>
                                            <td align="right" style="height: 26px">
                                                <asp:TextBox ID="TextBox4" runat="server" Width="65px" CssClass="form_input"></asp:TextBox>
                                            </td>
                                            <td style="height: 26px">
                                                <asp:DropDownList ID="DropDownList4" runat="server" Height="24px" Width="216px" CssClass="form_input">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="3">
                                                <asp:Panel ID="Panel4" runat="server" CssClass="form_input">
                                                    <table style="width: 100%">
                                                        <tr align="center" bgcolor="#CCCCCC" style="color: #FFFFFF" colspan="3">
                                                            <td align="center" bgcolor="#CCCCCC" style="width: 353px" colspan="4">Estancia Hospitalaria</td>
                                                            <td align="center" bgcolor="#CCCCCC" colspan="2">Vigencia</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="width: 353px">Tipo</td>
                                                            <td style="width: 353px">
                                                                <asp:DropDownList ID="DropDownList6" runat="server" Width="208px" CssClass="form_input" Height="24px">
                                                                </asp:DropDownList>
                                                            </td>
                                                            <td style="width: 353px">Dias</td>
                                                            <td style="width: 353px">
                                                                <asp:TextBox ID="TextBox7" runat="server" Width="42px" CssClass="form_input"></asp:TextBox>
                                                            </td>
                                                            <td align="center">Desde</td>
                                                            <td align="center">Hasta</td>
                                                        </tr>
                                                        <tr>
                                                            <td colspan="2" style="height: 30px">
                                                                <asp:CheckBox ID="CheckBox3" runat="server" Text="Material No Almacenable" />
                                                            </td>
                                                            <td colspan="2" style="height: 30px">%&nbsp;
                                                                <asp:TextBox ID="TextBox8" runat="server" Width="42px"></asp:TextBox>
                                                            </td>
                                                            <td style="height: 30px">
                                                                <asp:TextBox ID="TextBox5" runat="server" Width="79px" CssClass="form_input"></asp:TextBox>
                                                            </td>
                                                            <td style="height: 30px">
                                                                <asp:TextBox ID="TextBox6" runat="server" Width="79px" CssClass="form_input"></asp:TextBox>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </asp:Panel>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Panel ID="Panel5" runat="server" CssClass="form_input">
                                    <table style="width: 100%">
                                        <tr>
                                            <td align="center" bgcolor="#CCCCCC" style="color: #FFFFFF" colspan="2">Autorizacion Recibida Por</td>
                                        </tr>
                                        <tr>
                                            <td style="width: 146px">
                                                <asp:TextBox ID="TextBox9" runat="server" Enabled="False" Width="163px" CssClass="form_input"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="TextBox10" runat="server" Enabled="False" Width="345px" CssClass="form_input"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>

                    
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center"></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </table>
    
    

    
</asp:Content>
