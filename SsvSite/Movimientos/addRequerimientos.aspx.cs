﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Movimientos_addRequerimientos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Requerimientos", this.Page);
            if (Session["Nick"] == null)
                Procedimientos.ValidarSession(this.Page, Request.Url.AbsoluteUri);
            Procedimientos.ValidarSession(this.Page);
            txtCodigo.Text = null;
            ListarClientes();
            ListarReq();
            Procedimientos.LlenarCombos(ddlAsignar, new ClinicaCES.Logica.LUsuarios().UsuarioVSConsultar(), "USUARIO", "NOMBRE");
            hdnHoraInicial.Value = DateTime.Now.ToShortTimeString();

            if (Request.QueryString["id"] != null)
            {
                if (Request.QueryString["id"] == "A")
                {
                    txtCodigo.Enabled = true;
                    txtCodigo.Focus();
                }
                else
                {
                    Consultar(Request.QueryString["id"]);
                }
            }
        }

        litScript.Text = string.Empty;
    }

    private void ListarClientes()
    {
        Procedimientos.LlenarCombos(ddlCliente, new ClinicaCES.Logica.LClientes().ClienteConsultar(), "NIT", "NOMCOMCIAL");
        Procedimientos.LlenarCombos(ddlManosde, new ClinicaCES.Logica.LClientes().EncargadoConsultar(), "CODIGO", "ENCARGADO");
    }

    private void ListarReq()
    {
        Procedimientos.LlenarCombos(ddlEstado, new ClinicaCES.Logica.LRequerimientos().EstadosListar(), "CODIGO", "DESCESTADO");
        Procedimientos.comboEstadoInicial(ddlAplicacion);
        Procedimientos.comboEstadoInicial(ddlReporto);

    }

    private void Consultar(string idReq)
    {
        DataSet dsReq = new ClinicaCES.Logica.LRequerimientos().ReqConsultar(idReq);

        DataTable dtReq = dsReq.Tables[0];
        DataTable dtDetalle = dsReq.Tables[1];

        if (dtReq.Rows.Count > 0)
        {
            DataRow row = dtReq.Rows[0];
            txtCodigo.Text = row["IDREQUERIMIENTO"].ToString();
            hdnIdAsunto.Value = row["IDREQUERIMIENTO"].ToString();
            ddlCliente.SelectedValue = row["CLIENTE"].ToString();

            string[] cliente = ddlCliente.SelectedValue.Split('|');
            ListarReporta(cliente[0], cliente[1]);
            ListarAppXClie(cliente[0], cliente[1]);

            ddlReporto.SelectedValue = row["IDUSUARIO"].ToString();

            ConsultarDatosUsuario(ddlReporto.SelectedValue);

            if (row["IDAPLICACION"].ToString() != "-1")
            {
                ddlAplicacion.SelectedValue = row["IDAPLICACION"].ToString();
            }

            txtDescripcion.Text = row["DESCRIPCION"].ToString();
            //ddlManosde.SelectedValue = row["IDENCARGADO"].ToString();
            ddlManosde.SelectedIndex = 0;
            hdnHoraInicial.Value = row["HORA"].ToString();
            chkFacturable.Checked = bool.Parse(row["FACTURABLE"].ToString());
            txtFecha.Text = row["FECHACOMP"].ToString();
            ddlCliente.Enabled = false;
            ddlReporto.Enabled = false;
            txtTelefono.Enabled = false;
            txtEmail.Enabled = false;
            ddlAplicacion.Enabled = false;
            txtDescripcion.Enabled = false;

            pnlDetalle.Visible = true;

            lblFecha.Text = row["FECHA"].ToString();
            lblHora.Text = row["HORA"].ToString();

            hdnHoraInicialDetalle.Value = DateTime.Now.ToShortTimeString();

            if (dtDetalle.Rows.Count > 0)
            {
                Procedimientos.LlenarGrid(dtDetalle, gvDetalle);

                for (int num = 0; num <= dtDetalle.Rows.Count - 1; num++)
                {
                    DataRow rows = dtDetalle.Rows[num];
                    if (rows["IDESTADO"].ToString() == "SO")
                    {
                        ddlManosde.Enabled = false;
                        ddlEstado.Enabled = false;
                        ddlConsulto.Enabled = false;
                        txtInformacion.Enabled = false;
                    }
                }
            }
            if (Request.QueryString["id"] == "A")
                Response.Redirect("addRequerimientos.aspx?id=" + idReq);
        }
        else
        {
            Response.Redirect("addRequerimientos.aspx?id=A");
        }
    }

    private void ListarReporta(string nit, string negocio)
    {
        Procedimientos.LlenarCombos(ddlReporto, new ClinicaCES.Logica.LClientes().UsuarioReporta(nit, negocio), "USUARIO", "NOMBRE");
        Procedimientos.LlenarCombos(ddlConsulto, new ClinicaCES.Logica.LClientes().UsuarioReporta(nit, negocio), "USUARIO", "NOMBRE");
    }

    private void ListarAppXClie(string nit, string negocio)
    {
        AppXCli axc = new AppXCli();
        axc.Nit = nit;
        axc.Negocio = negocio;
        Procedimientos.LlenarCombos(ddlAplicacion, new ClinicaCES.Logica.LAppXCli().AppXCliBuscar(axc), "IDAPLICACION", "IDAPLICACION");
    }

    private void ConsultarDatosUsuario(string usr)
    {
        Usuarios user = new Usuarios();
        user.Usuario = usr;
        DataTable dt = new ClinicaCES.Logica.LUsuarios().UsuarioConsultar(user);

        if (dt.Rows.Count > 0)
        {
            txtTelefono.Text = dt.Rows[0]["TELEFONO"].ToString();
            txtEmail.Text = dt.Rows[0]["EMAIL"].ToString();
        }
    }

    private void Guardar
    (
        string IdReq,
        string horareq,
        string idcliente,
        string negocio,
        string idusuario,
        string telefono,
        string email,
        string idaplicacion,
        string idestado,
        string idencargado,
        string idatendio,
        string fechacomp,
        string Descripcion,
        bool? facturable,

        string idEstado,
        string idUsuClie,
        string Informacion,
        string Hora,
        string idUsuSeg,
        string IdResponsable,
        string asignado
    )
    {
        if (!ValidaGuardar())
            return;

        Requerimientos req = new Requerimientos();

        string msg = "3";

        req.IdReq = IdReq;
        req.horaReq = horareq;
        req.idcliente = idcliente;
        req.negocio = negocio;
        req.idusuario = idusuario;
        req.telefono = telefono;
        req.email = email;
        req.idaplicacion = idaplicacion;
        req.idestado = idestado;
        req.idencargado = idencargado;
        req.idatendio = idatendio;
        req.fechaComp = fechacomp;
        req.Descripcion = Descripcion;
        req.facturable = facturable;


        IdReq = new ClinicaCES.Logica.LRequerimientos().ReqActualizar(req);
        if (!string.IsNullOrEmpty(IdReq))
        {
            if (Request.QueryString["id"] != null)
                IdReq = Request.QueryString["id"];
            hdnIdAsunto.Value = IdReq;
            txtCodigo.Text = IdReq;
            msg = "1";

            if (pnlDetalle.Visible)
            {
                if (!ValidaGuardarDetalle())
                    return;

                req.idEstado = idEstado;
                if (idUsuClie == "-1")
                    idUsuClie = "";
                req.idUsuClie = idUsuClie; //ddlConsulto.SelectedValue;
                req.Informacion = Informacion;//txtInformacion
                req.Hora = Hora;//hdnHoraDetalle.Value;
                req.IdUsuario = idUsuSeg; 
                req.IdResponsable = IdResponsable;
                req.idAsignado = asignado;
                if (!new ClinicaCES.Logica.LRequerimientos().ReqDetalleActualizar(req))
                {
                    msg = "3";

                }
                else
                {
                    if (ddlAsignar.SelectedIndex != 0)
                    {
                        string[] usr = ddlAsignar.SelectedValue.Split('|');
                        Procedimientos.EnviarCorreo(usr[1], "Requerimiento Asignado: " + IdReq + " " + Request.Url.AbsoluteUri, "Requerimiento Asignado");
                    }
                }
            }

            ///*DECLARANDO VARIABLES PARA CALCULAR LA FECHA DE COMPROMISO DE LA TAREA*/
            //string horaFin = ConfigurationManager.AppSettings["horaFin"];
            //double horasSoporte = double.Parse(ConfigurationManager.AppSettings["horasSoporte"]);
            //double Dias = 0;
            //DateTime fechaIngreso = DateTime.Now;
            //string horaIngreso = fechaIngreso.ToShortTimeString();

            //Prioridades pr = new Prioridades();
            //pr.Codigo = idprioridad;
            //double horasPrioridad = double.Parse(new ClinicaCES.Logica.LPrioridades().PrioridadConsultar(pr).Rows[0]["HORAS"].ToString());
            //double horasOtros = 0;
            ///*FIN DECLARACION VARIABLES*/

            ///*CALCULANDO LA FECHA DE COMPROMISO*/
            //DateTime totalHoras = DateTime.Parse("01/01/2000 " + horaIngreso.ToString()).AddHours(horasPrioridad);
            //if (totalHoras > DateTime.Parse("01/01/2000 " + horaFin))
            //{
            //    horasOtros = DateTime.Parse("01/01/2000 " + horaFin).Hour - DateTime.Parse("01/01/2000 " + horaIngreso.ToString()).Hour;
            //    horasOtros = horasPrioridad - horasOtros;
            //    Dias = Math.Ceiling(horasOtros / horasSoporte);
            //}
            ///*----------------------------------------*/
            //fechaIngreso = fechaIngreso.AddDays(Dias);
            ///*validando dias no habiles*/
            //DataTable dtFechaNoLab = new ClinicaCES.Logica.LAsunto().FestivosConsultar(fechaIngreso.Month, fechaIngreso.Year);
            //bool romper = false;
            //while (!romper)
            //{
            //    dtFechaNoLab = Procedimientos.dtFiltrado(null, "FECHA = '" + fechaIngreso.ToShortDateString() + "'", dtFechaNoLab);

            //    if (dtFechaNoLab.Rows.Count > 0)
            //    {
            //        int mes = fechaIngreso.Month;
            //        fechaIngreso = fechaIngreso.AddDays(1);
            //        if (mes != fechaIngreso.Month)
            //            dtFechaNoLab = new ClinicaCES.Logica.LAsunto().FestivosConsultar(fechaIngreso.Month, fechaIngreso.Year);
            //    }
            //    else
            //        romper = true;
            //}

            /*---------------------------------*/


            if (ddlAsignar.SelectedIndex != 0)
            {
                string[] usrAsign = ddlAsignar.SelectedValue.Split('|');
                string responsable = ddlAsignar.SelectedValue;
                if (responsable.Equals("-1"))
                    responsable = Session["Nick"].ToString();
                else
                    responsable = usrAsign[0];
                GuardarRegistroActividades(Semana(DateTime.Now.ToShortDateString()), responsable, idcliente, negocio, "3", idaplicacion,
                    "REQUERIMIENTO: " + IdReq, "R", IdReq, "5", false, DateTime.Now.ToShortDateString(), "", idestado);   
            }
        }
        Procedimientos.Script("Mensaje(" + msg + "); redirect('addRequerimientos.aspx?id=" + IdReq + "')", litScript);
    }


    private bool ValidaGuardar()
    {
        bool Valido = true;
        System.Drawing.Color blue = System.Drawing.Color.Blue;
        System.Drawing.Color red = System.Drawing.Color.Red;
        if (ddlCliente.SelectedValue.Trim() == "-1")
        {
            lblValidaCliente.ForeColor = red;
            ddlCliente.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            ddlCliente.CssClass = "form_input";
            lblValidaCliente.ForeColor = blue;
        }

        if (ddlReporto.SelectedValue.Trim() == "-1")
        {
            lblValidaReporto.ForeColor = red;
            ddlReporto.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            ddlReporto.CssClass = "form_input";
            lblValidaReporto.ForeColor = blue;
        }

        if (string.IsNullOrEmpty(txtTelefono.Text.Trim()))
        {
            lblValidaTelefono.ForeColor = red;
            txtTelefono.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaTelefono.ForeColor = blue;
            txtTelefono.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtEmail.Text.Trim()))
        {
            txtEmail.CssClass = "invalidtxt";
            lblValidaEmail.ForeColor = red;
            Valido = false;
        }
        else
        {
            lblValidaEmail.ForeColor = blue;
            txtEmail.CssClass = "form_input";
        }

        if (ddlAplicacion.SelectedValue.Trim() == "-1")
        {
            lblValidaAplicacion.ForeColor = red;
            ddlAplicacion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaAplicacion.ForeColor = blue;
            ddlAplicacion.CssClass = "form_input";
        }


        if (string.IsNullOrEmpty(txtDescripcion.Text.Trim()))
        {
            lblValidaDescripcion.ForeColor = red;
            txtDescripcion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaDescripcion.ForeColor = blue;
            txtDescripcion.CssClass = "form_input";
        }

        if (ddlManosde.SelectedValue.Trim() == "-1")
        {
            lblValidaManosDe.ForeColor = red;
            ddlManosde.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaManosDe.ForeColor = blue;
            ddlManosde.CssClass = "form_input";
        }

        return Valido;
    }

    private bool ValidaGuardarDetalle()
    {
        bool Valido = true;
        System.Drawing.Color blue = System.Drawing.Color.Blue;
        System.Drawing.Color red = System.Drawing.Color.Red;


        if (ddlManosde.SelectedValue.Trim() == "-1")
        {
            lblValidaManosDe.ForeColor = red;
            ddlManosde.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaManosDe.ForeColor = blue;
            ddlManosde.CssClass = "form_input";
        }

        if (ddlEstado.SelectedValue.Trim() == "-1")
        {
            lblvalidaEstado.ForeColor = red;
            ddlEstado.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblvalidaEstado.ForeColor = blue;
            ddlEstado.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtInformacion.Text.Trim()))
        {
            lblValidaInformacion.ForeColor = red;
            txtInformacion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaInformacion.ForeColor = blue;
            txtInformacion.CssClass = "form_input";
        }

        return Valido;

    }

    protected void txtCodigo_TextChanged(object sender, EventArgs e)
    {
        ListarReq();
        Consultar(txtCodigo.Text.ToUpper());
    }

    protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCliente.SelectedIndex != 0)
        {
            string[] cliente = ddlCliente.SelectedValue.Split('|');
            ListarReporta(cliente[0], cliente[1]);
            ListarAppXClie(cliente[0], cliente[1]);
        }
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        {
            if (ValidaGuardar())
            {
                string idReq = null;

                if (!string.IsNullOrEmpty(hdnIdAsunto.Value))
                    idReq = hdnIdAsunto.Value;

                string[] cliente = ddlCliente.SelectedValue.Split('|');
                Guardar(idReq, hdnHoraInicial.Value, cliente[0], cliente[1], ddlReporto.SelectedValue, txtTelefono.Text.Trim().ToUpper(),
                    txtEmail.Text.Trim().ToUpper(), ddlAplicacion.SelectedValue, null/*estado*/,
                    ddlManosde.SelectedValue/*encargado*/, Session["Nick"].ToString(),
                    txtFecha.Text.Trim(), txtDescripcion.Text.Trim().ToUpper(), chkFacturable.Checked /*facturable*/
                    , ddlEstado.SelectedValue, ddlConsulto.SelectedValue, txtInformacion.Text.Trim().ToUpper(), hdnHoraInicialDetalle.Value,
                    Session["Nick"].ToString(), ddlManosde.SelectedValue, ddlAsignar.SelectedValue
                    );
            }
        }
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addRequerimientos.aspx");
        else if (sender.Equals(btnActualizar))
            Response.Redirect("addRequerimientos.aspx?id=A");
    }

    protected void ddlReporto_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlReporto.SelectedIndex != 0)
            ConsultarDatosUsuario(ddlReporto.SelectedValue);
        else
        {
            txtEmail.Text = string.Empty;
            txtTelefono.Text = string.Empty;
        }

    }

    protected void gvDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string info = e.Row.Cells[3].Text;
            if (info.Length > 100)
            {
                e.Row.Cells[3].Text = e.Row.Cells[3].Text.Substring(0, 100) + " ...";
                e.Row.ToolTip = info;
            }
        }
    }

    private void GuardarRegistroActividades
    (
        string Semana,
        string Responsable,
        string Cliente,
        string Negocio,
        string Linea,
        string Aplicacion,
        string Programa,
        string CNA,
        string Req,
        string Actividad,
        bool? Planeada,
        string Fecha,
        string Tiempo//,
        //string codigo
        , string Estado
    )
    {

        DataTable dt = new ClinicaCES.Logica.LActividades().ActividadResponsableReq(Req);
        string ResponSableAnterior = string.Empty;
        string borrar = null;
        if (dt.Rows.Count > 0)
        {
            ResponSableAnterior = dt.Rows[0]["RESPONSABLE"].ToString();
            borrar = dt.Rows[0]["CODIGO"].ToString();
        }

        if (Responsable.Equals(ResponSableAnterior))
            return;

        ActividadReg act = new ActividadReg();

        act.Semana = Semana;
        act.Responsable = Responsable;
        act.Cliente = Cliente;
        act.Negocio = Negocio;
        act.Linea = Linea;
        act.Aplicacion = Aplicacion;
        act.Programa = Programa;
        act.CNA = CNA;
        act.requerimiento = Req;
        act.Actividad = Actividad;
        act.Planeada = Planeada;
        act.Fecha = Fecha;
        act.Tiempo = Tiempo;
        if (Estado == "SO")
            act.Finalizado = true;
        //act.Codigo = codigo;


        if (!string.IsNullOrEmpty(borrar))
            act.Borrar = borrar;

        string msg = "3";

        if (new ClinicaCES.Logica.LActividades().ActividadActualizar(act))
        {
            msg = "1";
            //if (string.IsNullOrEmpty(hdnCodigo.Value))
            //{
            string email = new ClinicaCES.Logica.LUsuarios().Email(Responsable);

            Procedimientos.EnviarCorreo(email, "Nueva actividad asignada: " + Programa, "Nueva actividad asignada");
            //}
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string Semana(string fecha)
    {
        DateTime Fecha = Convert.ToDateTime(fecha);
        string year = Fecha.Year.ToString();
        return year + "-" + getWeek(Fecha).ToString();
    }

    public int getWeek(DateTime date)
    {
        System.Globalization.CultureInfo cult_info = System.Globalization.CultureInfo.CreateSpecificCulture("no");
        System.Globalization.Calendar cal = cult_info.Calendar;
        int weekCount = cal.GetWeekOfYear(date, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek);
        return weekCount;
    }

    protected void ddlAsignar_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAsignar.SelectedIndex != 0)
        {
            pnlTiempo.Visible = true;
        }
        else
        {
            pnlTiempo.Visible = false;
        }
    }
}
