﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Movimientos_srcAdjuntosPop : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            string Codigo = Request.QueryString["id"];
            string Consecutivo = Request.QueryString["cons"];
            Procedimientos.Titulo("Archivos Adjuntos del caso " + Codigo + " en el consecutivo " + Consecutivo,this.Page);
            Buscar(Codigo, Consecutivo);
        }
    }

    private void Buscar(string Codigo, string Consecutivo)
    {
        DataTable dt = new ClinicaCES.Logica.LAsunto().AsuntoConsultarAdjuntos(Codigo, Consecutivo);
        Procedimientos.LlenarGrid(dt, gvAdjuntos);
    }

    protected void gvAdjuntos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string[] Nombre = e.Row.Cells[0].Text.Split('/');
            string fileName = Nombre[Nombre.Length - 1].Replace("%20", " ");
            Nombre = fileName.Split('.');
            string extencion = icono(Nombre[Nombre.Length - 1].ToLower());
            e.Row.Cells[0].Text = "<a href=" + e.Row.Cells[0].Text.Replace(" ","%20") + " ><img src=../icons/" + extencion + " border=0 /> " + fileName + "</a>";
        }
    }

    private string icono(string extencion)
    {
        switch (extencion)
        {
            case "xls":
                return "xls.gif";
            case "doc":
                return "doc.gif";
            case "txt":
                return "txt.jpg";
            case "zip":
                return "zip.gif";
            case "rar":
                return "rar.gif";
            case "jpg":
                return "image_icon.png";
            case "gif":
                return "image_icon.png";
            case "bmp":
                return "image_icon.png";
            case "jpeg":
                return "image_icon.png";
            default:
                return "icon_attachment.gif";

        }
    }
}
