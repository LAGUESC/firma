﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addRequerimientos.aspx.cs" Inherits="Movimientos_addRequerimientos" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<table align="center" width="100%">
    <tr>
        <td align="right">Codigo:</td>
        <td colspan="2">
            <asp:TextBox runat="server" 
                ID="txtCodigo" CssClass="form_input" AutoPostBack="true" ontextchanged="txtCodigo_TextChanged" 
                MaxLength="6" TabIndex="1" Enabled="False" Width="74px"></asp:TextBox></td>
        <td align="right" style="width: 442px">Cliente: <asp:Label runat="server" ID="lblValidaCliente" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="2">
            <asp:DropDownList ID="ddlCliente" runat="server" AutoPostBack="true"
                onselectedindexchanged="ddlCliente_SelectedIndexChanged" TabIndex="2"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right">Reporto: <asp:Label runat="server" ID="lblValidaReporto" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="2">
            <asp:DropDownList ID="ddlReporto" runat="server" AutoPostBack="true"
                onselectedindexchanged="ddlReporto_SelectedIndexChanged" TabIndex="3"></asp:DropDownList></td>
        <td align="right" style="width: 442px">Telefono: <asp:Label runat="server" ID="lblValidaTelefono" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="2"><asp:TextBox runat="server" ID="txtTelefono" CssClass="form_input" MaxLength="25" TabIndex="4"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right">E-mail: <asp:Label runat="server" ID="lblValidaEmail" Text="(*)" ForeColor="Blue"></asp:Label></td>        
        <td colspan="2">
            <asp:TextBox Width="159px" runat="server" ID="txtEmail" 
                CssClass="form_input" MaxLength="50" TabIndex="5"></asp:TextBox></td>
        <td align="right" style="width: 442px">Aplicación: <asp:Label runat="server" ID="lblValidaAplicacion" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="2"><asp:DropDownList ID="ddlAplicacion" runat="server" TabIndex="6"></asp:DropDownList></td>
    </tr>
    <tr>
        <td align="right"  valign="top">Descripcion: <asp:Label runat="server" ID="lblValidaDescripcion" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td colspan="5">
            <asp:TextBox Height="50px" Width="97%"  ID="txtDescripcion" 
                runat="server" CssClass="form_input" TextMode="MultiLine" MaxLength="4000" 
                TabIndex="13"></asp:TextBox></td>
    </tr>
    <tr>
        <td align="right" valign="top" style="white-space:nowrap">En manos de: <asp:Label runat="server" ID="lblValidaManosDe" Text="(*)" ForeColor="Blue"></asp:Label></td>
        <td valign="top" style="width: 113px">
            <asp:DropDownList ID="ddlManosde" runat="server" TabIndex="14">
            </asp:DropDownList>
        </td>
        <td valign="top">
            &nbsp;</td>
        <td style="width: 442px">
                F. Entrega:</td>        
        <td>
                <asp:UpdatePanel ID="upnlSemaniviris" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:TextBox ID="txtFecha" runat="server" onkeyup="mascara(this,'/',true)" CssClass="form_input" Width="60px" style="margin-left: 0px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtFecha" FilterType="Custom, Numbers" ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy" ID="CalendarExtender1" OnClientDateSelectionChanged="ValidaSemana"
                    runat="server" TargetControlID="txtFecha" PopupButtonID="img1"></ajaxToolkit:CalendarExtender>       
                    <img ID="img1" runat="server" src="../icons/calendar.png" 
                        style="cursor:pointer" />
                </ContentTemplate>
            </asp:UpdatePanel>
            </td>        
        <td>
                <asp:CheckBox ID="chkFacturable" runat="server" Text="Facturable" 
                    TextAlign="Left" />
            </td>        
    </tr>
</table>
<asp:Panel ID="pnlDetalle" Visible="false" runat="server">
    <table  align="center" width="100%">
        <tr>
            <td align="right" style="width: 76px">Fecha:</td>
            <td>
                <asp:Label ID="lblFecha" runat="server"></asp:Label>
            </td>
            <td style="white-space:nowrap">
                Hora:
                <asp:Label ID="lblHora" runat="server"></asp:Label>
            </td>
            <td align="right">
                Estado: <asp:Label ID="lblvalidaEstado" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="2">
                <asp:DropDownList ID="ddlEstado" runat="server">
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" colspan="6">
                <hr style="margin-left: 0px" />
            </td>
        </tr>
        <tr>
            <td align="right">Consulto:</td>
            <td colspan="2">
                <asp:DropDownList ID="ddlConsulto" runat="server">
                </asp:DropDownList>
            </td>
            <td>
                Asignar:
            </td>
            <td>
                <asp:DropDownList ID="ddlAsignar" runat="server" AutoPostBack="true"
                    onselectedindexchanged="ddlAsignar_SelectedIndexChanged"></asp:DropDownList>
            </td>
            <asp:Panel ID="pnlTiempo" runat="server" Visible="false">
            <td>Tiempo:</td>
            <td><asp:TextBox onkeypress="tabular(event,this); event.returnValue=SoloNumeros(event)" runat="server" ID="txtTiempo" CssClass="form_input" Width="30px" MaxLength="3"></asp:TextBox></td>
            </asp:Panel>
        </tr>
        <tr>
            <td align="right" valign="top">Información: <asp:Label ID="lblValidaInformacion" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="5">
                <asp:TextBox ID="txtInformacion" runat="server" CssClass="form_input" 
                    Height="50px" TextMode="MultiLine" Width="96%"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="6">
                <hr />
            </td>
        </tr>
    </table>
    
    <table align="center">
        <tr>
            <td>
                <asp:GridView AutoGenerateColumns="False" ID="gvDetalle" runat="server"
                CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
                    PageSize="15" onrowdatabound="gvDetalle_RowDataBound" >                   
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle CssClass="normalrow" />
                    <AlternatingRowStyle CssClass="alterrow" />
                    <PagerStyle CssClass="cabeza" ForeColor="White"  />
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                    <Columns>     
                        <asp:BoundField DataField="CONSECUTIVO" HeaderText="Cons" />
                        <asp:BoundField DataField="FECHA" HeaderText="Fecha" />
                        <asp:BoundField DataField="HORA" HeaderText="Hora" ItemStyle-Wrap="false" >                     
                            <ItemStyle Wrap="False" />
                        </asp:BoundField>
                        <asp:BoundField DataField="INFORMACION" HeaderText="Informacion" />
                        <asp:BoundField DataField="IDUSUCLI" HeaderText="Consulto" />
                        <asp:BoundField DataField="IDESTADO" HeaderText="Estado" />
                        <asp:BoundField DataField="IDRESPONSABLE" HeaderText="Encargado"  />
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Panel>
<table align="center">
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                    <td><asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick="return Mensaje(13)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>
                    <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>
                    <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcRequerimientos.aspx')" /></td>                                                                                
                    <td><asp:Button ID="btnActualizar" runat="server" Text="Actualizar" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" style="margin-left: 0px" 
                            Width="64px"   /></td>                     
                    <td>&nbsp;<asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" 
                            OnClientClick="return Mensaje(11)" onmouseover="this.className='btnhov'" 
                            onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" 
                            Visible="False"  /> </td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
<asp:HiddenField ID="hdnIdAsunto" runat="server" />
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<asp:HiddenField ID="hdnHoraInicialDetalle" runat="server" />
<asp:HiddenField ID="hdnHoraInicial" runat="server" />
</asp:Content>

