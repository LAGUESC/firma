﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;
using System.IO;

public partial class Movimientos_addAsunto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            if (Session["Nick"] == null)
                Procedimientos.ValidarSession(this.Page, Request.Url.AbsoluteUri);

            Procedimientos.Titulo("Casos", this.Page);
            txtCodigo.Text = null;
            ListarClientes();
            ListarAsunto();
            Procedimientos.LlenarCombos(ddlAsignar, new ClinicaCES.Logica.LUsuarios().UsuarioVSConsultar(), "USUARIO", "NOMBRE");
            hdnHoraInicial.Value = DateTime.Now.ToShortTimeString();

            if (Request.QueryString["id"] != null)
            {
                if (Request.QueryString["id"] == "A")
                {
                    txtCodigo.Enabled = true;
                    txtCodigo.Focus();
                }
                else
                {
                    Consultar(Request.QueryString["id"]);
                }
            }
        }

        litScript.Text = string.Empty;
    }

    private void ListarClientes()
    {
        Procedimientos.LlenarCombos(ddlCliente, new ClinicaCES.Logica.LClientes().ClienteConsultar(), "NIT", "NOMCOMCIAL");
        Procedimientos.LlenarCombos(ddlManosde, new ClinicaCES.Logica.LClientes().EncargadoConsultar(), "CODIGO", "ENCARGADO");   
        
    }

    private void ListarReporta(string nit,string negocio)
    {
        Procedimientos.LlenarCombos(ddlReporto, new ClinicaCES.Logica.LClientes().UsuarioReporta(nit, negocio), "USUARIO", "NOMBRE");
        Procedimientos.LlenarCombos(ddlConsulto, new ClinicaCES.Logica.LClientes().UsuarioReporta(nit, negocio), "USUARIO", "NOMBRE");
        Procedimientos.LlenarCombos(ddlRecibio, new ClinicaCES.Logica.LClientes().UsuarioReporta(nit, negocio), "USUARIO", "NOMBRE");        


    }

    private void ListarAppXClie(string nit, string negocio)
    {
        AppXCli axc = new AppXCli();
        axc.Nit = nit;
        axc.Negocio = negocio;
        Procedimientos.LlenarCombos(ddlAplicacion, new ClinicaCES.Logica.LAppXCli().AppXCliBuscar(axc), "IDAPLICACION", "IDAPLICACION");                
    }


    private void Guardar
    (
        string horaini,
        string idcliente,
        string negocio,
        string idusuario,
        string telefono,
        string email,
        string idaplicacion,
        string idmenu,
        string idmodulo,
        string idsubmodulo,
        string version,
        string asunto,
        string idestado,
        string idencargado,
        string solicita,
        string idatendio,
        string idclasificacion,
        string idprioridad,
        string horagra,
        string IdAsunto,
        string Descripcion,

        string idEstado,
        string idUsuClie,
        string Informacion ,
        string HoraIni,
        string HoraFin,
        bool? Recibieron ,
        string FRecibieron ,
        string IdUsuarioRecibio,
        string IdResponsable,
        string Asignado
    )
    {
        if (!ValidaGuardar())
            return;

        Asunto asu = new Asunto();

        string msg = "3";

        asu.horaini = horaini;
        asu.idcliente = idcliente;
        asu.negocio = negocio;
        asu.idusuario = idusuario;
        asu.telefono = telefono;
        asu.email = email;
        asu.idaplicacion = idaplicacion;
        asu.idmenu = idmenu;
        asu.idmodulo = idmodulo;
        asu.idsubmodulo = idsubmodulo;
        asu.version = version;
        asu.asunto = asunto;
        asu.idestado = idestado;
        asu.idencargado = idencargado;
        asu.solicita = solicita;
        asu.idatendio = idatendio;
        asu.idclasificacion = idclasificacion;
        asu.idprioridad = idprioridad;
        asu.horagra = horagra;
        asu.IdAsunto = IdAsunto;
        asu.Descripcion = Descripcion;

        
       // IdAsunto = new ClinicaCES.Logica.LAsunto().AsuntoActualizar(asu);
        if (!string.IsNullOrEmpty(IdAsunto))
        {
            //if (Request.QueryString["id"] != null && Request.QueryString["id"])
            //    IdAsunto = Request.QueryString["id"];
            hdnIdAsunto.Value = IdAsunto;
            txtCodigo.Text = IdAsunto;
            msg = "1";

            if (pnlDetalle.Visible)
            {
                if (!ValidaGuardarDetalle())
                    return;

                asu.idEstado = idEstado;//ddlEstado.SelectedValue
                if (idUsuClie == "-1")
                    idUsuClie = "";
	            asu.idUsuClie = idUsuClie; //ddlConsulto.SelectedValue;
	            asu.Informacion = Informacion;//txtInformacion
	            asu.HoraIni = HoraIni;//hdnHoraInicialDetalle.Value;
	            asu.HoraFin = HoraFin; //DateTime.Now.ToShortTimeString();
	            asu.Recibieron = Recibieron;//chkRecibido.Checked;
	            asu.FRecibieron = FRecibieron;//txtFecha.Text;
                if (IdUsuarioRecibio == "-1")
                    IdUsuarioRecibio = "";
	            asu.IdUsuarioRecibio = IdUsuarioRecibio;//ddlRecibio.SelectedValue
                asu.IdResponsable = IdResponsable;
                asu.xmlArchivos = xmlArchivos();
                asu.Asignado = Asignado;
                if (!new ClinicaCES.Logica.LAsunto().AsuntoDetalleActualizar(asu))
                {
                    msg = "3";

                }
                else
                {
                    if (ddlAsignar.SelectedIndex != 0)
                    {
                        string[] usr = ddlAsignar.SelectedValue.Split('|');
                        //ClinicaCES.Correo.Correo objCorreo = new ClinicaCES.Correo.Correo();
                        //objCorreo.Asunto = "Caso Asignado Soporte";
                        //objCorreo.De = ConfigurationManager.AppSettings["mailSoporte"];
                        //objCorreo.Mensaje = "Caso Asignado: " + IdAsunto + " " + Request.Url.AbsoluteUri;
                        //objCorreo.Para = usr[1];

                        //objCorreo.Enviar();
                        Procedimientos.EnviarCorreo(usr[1], "Caso Asignado: " + IdAsunto + " " + Request.Url.AbsoluteUri, Session["Nombre"] +  " le Asigno Caso de Soporte");
                    }


                }
            }

            if (ddlManosde.SelectedValue == "CL")
            {
                //ClinicaCES.Correo.Correo objCorreo = new ClinicaCES.Correo.Correo();
                string mensaje;
                string[] Adjuntos = new string[gvAdjuntos.Rows.Count];
                foreach (GridViewRow item in gvAdjuntos.Rows)
                {
                    Adjuntos[item.RowIndex] = Server.MapPath(gvAdjuntos.DataKeys[item.RowIndex]["ruta"].ToString());
                }
                //objCorreo.Asunto = "Información Caso " + IdAsunto;
                //objCorreo.De = ConfigurationManager.AppSettings["mailSoporte"];
                //objCorreo.Mensaje = asu.Informacion;
                //objCorreo.Para = asu.email;
                //objCorreo.Adjuntos = Adjuntos;
                //objCorreo.Enviar();

                if (asu.Informacion == null)
                { 
                    mensaje=asu.solicita;
                }
                else
                {
                    mensaje = asu.Informacion;
                }


                Procedimientos.EnviarCorreo(asu.email, mensaje, "Información Caso " + IdAsunto, Adjuntos);

            }

            /*DECLARANDO VARIABLES PARA CALCULAR LA FECHA DE COMPROMISO DE LA TAREA*/
            string horaFin = ConfigurationManager.AppSettings["horaFin"];
            double horasSoporte = double.Parse(ConfigurationManager.AppSettings["horasSoporte"]);
            double Dias = 0;
            DateTime fechaIngreso = DateTime.Now;
            string horaIngreso = fechaIngreso.ToShortTimeString();

            Prioridades pr = new Prioridades();
            pr.Codigo = idprioridad;
            double horasPrioridad = double.Parse(new ClinicaCES.Logica.LPrioridades().PrioridadConsultar(pr).Rows[0]["HORAS"].ToString());
            double horasOtros = 0;
            /*FIN DECLARACION VARIABLES*/
            
            /*CALCULANDO LA FECHA DE COMPROMISO*/
            DateTime totalHoras = DateTime.Parse("01/01/2000 " + horaIngreso.ToString()).AddHours(horasPrioridad);
            if (totalHoras > DateTime.Parse("01/01/2000 " + horaFin))
            {
                horasOtros = DateTime.Parse("01/01/2000 " + horaFin).Hour - DateTime.Parse("01/01/2000 " + horaIngreso.ToString()).Hour;
                horasOtros = horasPrioridad - horasOtros;
                Dias = Math.Ceiling(horasOtros / horasSoporte);
            }
            /*----------------------------------------*/
            fechaIngreso = fechaIngreso.AddDays(Dias);
            /*validando dias no habiles*/
            DataTable dtFechaNoLab = new ClinicaCES.Logica.LAsunto().FestivosConsultar(fechaIngreso.Month, fechaIngreso.Year);
            bool romper = false;
            while (!romper)
            {
                dtFechaNoLab = Procedimientos.dtFiltrado(null, "FECHA = '" + fechaIngreso.ToShortDateString() + "'", dtFechaNoLab);

                if (dtFechaNoLab.Rows.Count > 0)
                {
                    int mes = fechaIngreso.Month;
                    fechaIngreso = fechaIngreso.AddDays(1);
                    if(mes != fechaIngreso.Month)
                        dtFechaNoLab = new ClinicaCES.Logica.LAsunto().FestivosConsultar(fechaIngreso.Month, fechaIngreso.Year);
                }
                else
                    romper = true;
            }

            /*---------------------------------*/


            string[] usrAsign = ddlAsignar.SelectedValue.Split('|');
            string responsable = ddlAsignar.SelectedValue;
            if (responsable.Equals("-1"))
                responsable = Session["Nick"].ToString();
            else
                responsable = usrAsign[0];
            GuardarRegistroActividades(Semana(DateTime.Now.ToShortDateString()), responsable, idcliente, negocio, "3", idaplicacion,
                "SOLUCIONAR CASO: " + IdAsunto, "C", IdAsunto, "5", false, fechaIngreso.ToShortDateString(), horasPrioridad.ToString(),idestado);
        }
        Procedimientos.Script("Mensaje(" + msg + "); redirect('addAsunto.aspx?id=" + IdAsunto + "')", litScript);

    }


    private bool ValidaGuardar()
    {
        bool Valido = true;
        System.Drawing.Color blue = System.Drawing.Color.Blue;
        System.Drawing.Color red = System.Drawing.Color.Red;

        if (string.IsNullOrEmpty(txtSolicitamos.Text.Trim()) && (ddlManosde.SelectedValue == "CL") && (pnlDetalle.Visible == false))
        {
            txtSolicitamos.CssClass = "invalidtxt";
            Valido = false;
        }
        else
            txtSolicitamos.CssClass = "form_input";

        if (ddlCliente.SelectedValue.Trim()=="-1")
        {
            lblValidaCliente.ForeColor = red;
            ddlCliente.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            ddlCliente.CssClass = "form_input";
            lblValidaCliente.ForeColor = blue;
        }

        if (ddlReporto.SelectedValue.Trim()=="-1")
        {
            lblValidaReporto.ForeColor = red;
            ddlReporto.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            ddlReporto.CssClass = "form_input";
            lblValidaReporto.ForeColor = blue;
        }

        if (string.IsNullOrEmpty(txtTelefono.Text.Trim()))
        {
            lblValidaTelefono.ForeColor = red;
            txtTelefono.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaTelefono.ForeColor = blue;
            txtTelefono.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtEmail.Text.Trim()))
        {
            txtEmail.CssClass = "invalidtxt";
            lblValidaEmail.ForeColor = red;
            Valido = false;
        }
        else
        {
            lblValidaEmail.ForeColor = blue;
            txtEmail.CssClass = "form_input";
        }

        if (ddlAplicacion.SelectedValue.Trim()=="-1")
        {
            lblValidaAplicacion.ForeColor = red;
            ddlAplicacion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaAplicacion.ForeColor = blue;
            ddlAplicacion.CssClass = "form_input";
        }

        if (ddlPrioridad.SelectedValue.Trim()=="-1")
        {
            lblPrioridad.ForeColor = red;
            ddlPrioridad.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblPrioridad.ForeColor = blue;
            ddlPrioridad.CssClass = "form_input";
        }

        if (ddlClasificacion.SelectedValue.Trim() == "-1")
        {
            lblValidaClasificacion.ForeColor = red;
            ddlClasificacion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaClasificacion.ForeColor = blue;
            ddlClasificacion.CssClass = "form_input";
        }

        if (ddlAsunto.SelectedValue.Trim()=="-1")
        {
            lblValidaAsunto.ForeColor = red;
            ddlAsunto.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaAsunto.ForeColor = blue;
            ddlAsunto.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtDescripcion.Text.Trim()))
        {
            lblValidaDescripcion.ForeColor = red;
            txtDescripcion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaDescripcion.ForeColor = blue;
            txtDescripcion.CssClass = "form_input";
        }

        if (ddlManosde.SelectedValue.Trim() == "-1")
        {
            lblValidaManosDe.ForeColor = red;
            ddlManosde.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaManosDe.ForeColor = blue;
            ddlManosde.CssClass = "form_input";
        }

        return Valido;

    }

    private bool ValidaGuardarDetalle()
    {
        bool Valido = true;
        System.Drawing.Color blue = System.Drawing.Color.Blue;
        System.Drawing.Color red = System.Drawing.Color.Red;
        if (ddlClasificacion.SelectedValue.Trim() == "-1")
        {
            ddlClasificacion.CssClass = "invalidtxt";
            lblValidaClasificacion.ForeColor = red;
            Valido = false;
        }
        else
        {
            lblValidaClasificacion.ForeColor = blue;
            ddlClasificacion.CssClass = "form_input";
        }

        if (ddlAsunto.SelectedValue.Trim() == "-1")
        {
            ddlAsunto.CssClass = "invalidtxt";
            lblValidaAsunto.ForeColor = red;
            Valido = false;
        }
        else
        {
            lblValidaAsunto.ForeColor = blue;
            ddlAsunto.CssClass = "form_input";
        }
        
        
        if (ddlManosde.SelectedValue.Trim() == "-1")
        {
            lblValidaManosDe.ForeColor = red;
            ddlManosde.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaManosDe.ForeColor = blue;
            ddlManosde.CssClass = "form_input";
        }

        if (ddlEstado.SelectedValue.Trim() == "-1")
        {
            lblvalidaEstado.ForeColor = red;
            ddlEstado.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblvalidaEstado.ForeColor = blue;
            ddlEstado.CssClass = "form_input";
        }
        
        if (string.IsNullOrEmpty(txtInformacion.Text.Trim()))
        {
            lblValidaInformacion.ForeColor = red;
            txtInformacion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            lblValidaInformacion.ForeColor = blue;
            txtInformacion.CssClass = "form_input";
        }       
       
        return Valido;

    }

    private void ListarAsunto()
    {
        Procedimientos.LlenarCombos(ddlAsunto, new ClinicaCES.Logica.LAsunto().AsuntoListar(), "CODIGO", "NOMBRE");
        //Procedimientos.LlenarCombos(ddlManosde, new ClinicaCES.Logica.LAsunto().EncargadoListar(), "USUARIO", "NOMBRE");
        Procedimientos.LlenarCombos(ddlPrioridad, new ClinicaCES.Logica.LAsunto().PrioridadListar(), "CODIGO", "PRIORIDAD");
        Procedimientos.LlenarCombos(ddlClasificacion, new ClinicaCES.Logica.LAsunto().ClasificacionListar(), "CODIGO", "CLASIFICACION");
        Procedimientos.LlenarCombos(ddlEstado, new ClinicaCES.Logica.LAsunto().EstadosListar(), "CODIGO", "DESCESTADO");
        Procedimientos.comboEstadoInicial(ddlAplicacion);
        Procedimientos.comboEstadoInicial(ddlMenu);
        Procedimientos.comboEstadoInicial(ddlModulo);
        Procedimientos.comboEstadoInicial(ddlReporto);
        Procedimientos.comboEstadoInicial(ddlSubModulo);
        //Procedimientos.comboEstadoInicial(ddlVersion);

        //ddlManosde.SelectedValue = Session["Nick"].ToString();



    }

    protected void txtCodigo_TextChanged(object sender, EventArgs e)
    {
        //ListarAsunto();
        //Consultar(txtCodigo.Text.ToUpper());
        Response.Redirect("addAsunto.aspx?id=" + txtCodigo.Text); 
    }

    protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCliente.SelectedIndex != 0)
        {
            string[] cliente = ddlCliente.SelectedValue.Split('|');
            ListarReporta(cliente[0], cliente[1]);
            ListarAppXClie(cliente[0], cliente[1]);
        }
    }

    private void ListarMenu(string App)
    {
        MMenu men = new MMenu();
        men.Aplicacion = App;
        Procedimientos.LlenarCombos(ddlMenu, new ClinicaCES.Logica.LMenu().MenuConsultar(men), "CODIGO", "MENU");                
    }

    private void ListarModulo(string App, string Menu)
    {
        Modulo men = new Modulo();
        men.Aplicacion = App;
        men.Menu = Menu;
        Procedimientos.LlenarCombos(ddlModulo, new ClinicaCES.Logica.LModulo().ModuloConsultar(men), "CODIGO", "MODULO");                
        
    }

    private void ListarSubModulo(string App, string Menu, string Modulo)
    {
        SubModulo sub = new SubModulo();
        sub.Aplicacion = App;
        sub.Menu = Menu;
        sub.Mod = Modulo;
        Procedimientos.LlenarCombos(ddlSubModulo, new ClinicaCES.Logica.LSubModulo().SubModuloConsultar(sub), "CODIGO", "SUBMODULO");                
        
    }

    protected void ddlAplicacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAplicacion.SelectedIndex != 0)
        {
            ListarMenu(ddlAplicacion.SelectedValue);
        }
        Procedimientos.comboEstadoInicial(ddlModulo);
        Procedimientos.comboEstadoInicial(ddlSubModulo);
    }

    protected void ddlModulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlModulo.SelectedIndex != 0)
        {
            ListarSubModulo(ddlAplicacion.SelectedValue,ddlMenu.SelectedValue,ddlModulo.SelectedValue);
        }
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        {
            if (ValidaGuardar())
            {
                string idAsunto = null;

                if (!string.IsNullOrEmpty(hdnIdAsunto.Value))
                    idAsunto = hdnIdAsunto.Value;

                if ((txtSolicitamos != null) && (txtCodigo == null))
                {
                    ddlManosde.SelectedValue = "CL";
                }

                string Asignado = null;
                string[] usrAsig = ddlAsignar.SelectedValue.Split('|');
                if (ddlAsignar.SelectedIndex != 0)
                    Asignado = usrAsig[0];

                string[] cliente = ddlCliente.SelectedValue.Split('|');
                Guardar(hdnHoraInicial.Value, cliente[0], cliente[1], ddlReporto.SelectedValue, txtTelefono.Text.Trim().ToUpper(),
                    txtEmail.Text.Trim().ToUpper(), ddlAplicacion.SelectedValue, ddlMenu.SelectedValue, ddlModulo.SelectedValue,
                    ddlSubModulo.SelectedValue, null /*ddlVersion.SelectedValue*/, ddlAsunto.SelectedValue, null/*estado*/,
                    ddlManosde.SelectedValue/*encargado*/, txtSolicitamos.Text.Trim()/*solicita*/, Session["Nick"].ToString(), ddlClasificacion.SelectedValue, ddlPrioridad.SelectedValue,
                    DateTime.Now.ToShortTimeString(), idAsunto, txtDescripcion.Text.Trim().ToUpper()
                    , ddlEstado.SelectedValue, ddlConsulto.SelectedValue, txtInformacion.Text.Trim().ToUpper(), hdnHoraInicialDetalle.Value,
                    DateTime.Now.ToShortTimeString(), chkRecibido.Checked, txtFecha.Text.Trim(), ddlRecibio.SelectedValue, ddlManosde.SelectedValue,Asignado
                    );
            }
        }
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addAsunto.aspx");
        else if (sender.Equals(btnActualizar))
            Response.Redirect("addAsunto.aspx?id=A");
        else if (sender.Equals(btnAdjuntar))
        {
            AdjuntarArchivo(txtCodigo.Text);
        }
    }

    private void AdjuntarArchivo(string codigo)
    {
        //string fileExt = System.IO.Path.GetExtension(fuArchivo.FileName);
        string path = "../Adjuntos/" + codigo + "_" + gvDetalle.Rows.Count.ToString();

        if (!System.IO.Directory.Exists(Server.MapPath(path)))
        {
            System.IO.Directory.CreateDirectory(Server.MapPath(path));
        }

        if(fuArchivo.HasFile)
            fuArchivo.SaveAs(Server.MapPath(path + "\\" + fuArchivo.FileName));

        CargarArchivos(codigo);
    }

    private void CargarArchivos(string codigo)
    {
        string path = "../Adjuntos/" + codigo + "_" + gvDetalle.Rows.Count.ToString();
        if (!System.IO.Directory.Exists(Server.MapPath(path)))
            return;
        string[] files = System.IO.Directory.GetFiles(Server.MapPath(path));
        string[] cols = {"Archivo","ruta"};
        DataTable dt = new DataTable();
        foreach (string item in files)
        {
            string[] archivo = item.Split('\\');
            string fila = path.Replace(" ", "%20") + "/" + archivo[archivo.Length - 1].Replace(" ", "%20");
            Procedimientos.CrearDatatable(cols, new string[] { fila,fila.Replace("%20"," ") }, dt);
        }

        Procedimientos.LlenarGrid(dt, gvAdjuntos);
    }

    private void Consultar(string idAsun)
    {
        DataSet dsAsunto = new ClinicaCES.Logica.LAsunto().AsuntoConsultar(idAsun);

        DataTable dtAsunto = dsAsunto.Tables[0];
        DataTable dtDetalle = dsAsunto.Tables[1];

        if (dtAsunto.Rows.Count > 0)
        {
            //imgExcel.Visible = true;
            //imgExcel.Attributes.Clear();
            imgExcel.Attributes.Add("onclick", "abrirVentana('infAsuntoExcel.aspx?id=" + idAsun + "')");
            DataRow row = dtAsunto.Rows[0];
            txtCodigo.Text = row["IDASUNTO"].ToString();
            hdnIdAsunto.Value = row["IDASUNTO"].ToString();
            ddlCliente.SelectedValue = row["CLIENTE"].ToString();

            string[] cliente = ddlCliente.SelectedValue.Split('|');
            ListarReporta(cliente[0], cliente[1]);
            ListarAppXClie(cliente[0], cliente[1]);

            ddlReporto.SelectedValue = row["IDUSUARIO"].ToString();

            ConsultarDatosUsuario(ddlReporto.SelectedValue);

            if (row["IDAPLICACION"].ToString()!= "-1")
            {
            ddlAplicacion.SelectedValue = row["IDAPLICACION"].ToString();
            ListarMenu(ddlAplicacion.SelectedValue);

                if (row["IDMENU"].ToString() != "-1")
                {
                    ddlMenu.SelectedValue = row["IDMENU"].ToString();
                    ListarModulo(ddlAplicacion.SelectedValue, ddlMenu.SelectedValue);

                    if (row["IDMODULO"].ToString()!="-1")
                    {
                        ddlModulo.SelectedValue = row["IDMODULO"].ToString();
                        ListarSubModulo(ddlAplicacion.SelectedValue, ddlMenu.SelectedValue, ddlModulo.SelectedValue);
                        if (row["IDSUBMODULO"].ToString()!="-1")
                            ddlSubModulo.SelectedValue = row["IDSUBMODULO"].ToString();
                    }
                }
            }

            ddlPrioridad.SelectedValue = row["IDPRIORIDAD"].ToString();
            ddlAsunto.SelectedValue = row["ASUNTO"].ToString();
            ddlClasificacion.SelectedValue = row["IDCLASIFICACION"].ToString();
            txtDescripcion.Text = row["DESCRIPCION"].ToString();
            //ddlManosde.SelectedValue = row["IDENCARGADO"].ToString();
            ddlManosde.SelectedIndex = 0;
            txtSolicitamos.Text = row["SOLICITA"].ToString();
            hdnHoraInicial.Value = row["HORAINI"].ToString();
            ddlCliente.Enabled = false;
            ddlReporto.Enabled = false;
            txtTelefono.Enabled = false;
            txtEmail.Enabled = false;
            ddlAplicacion.Enabled = false;
            ddlMenu.Enabled = false;
            ddlModulo.Enabled = false;
            ddlSubModulo.Enabled = false;
            ddlPrioridad.Enabled = false;
            txtDescripcion.Enabled = false;
            txtSolicitamos.Enabled = false;

            pnlDetalle.Visible = true;

            lblFecha.Text = row["FECHA"].ToString();
            lblHora.Text = row["HORAFIN"].ToString();

            

            hdnHoraInicialDetalle.Value = DateTime.Now.ToShortTimeString();

            if (dtDetalle.Rows.Count > 0)
            {
                Procedimientos.LlenarGrid(dtDetalle, gvDetalle);
                //CargarArchivos(idAsun);
                for (int num = 0; num <= dtDetalle.Rows.Count - 1; num++)
                {
                    DataRow rows = dtDetalle.Rows[num];
                    if (rows["IDESTADO"].ToString() == "SO")
                    {
                        ddlAsunto.Enabled = false;
                        ddlClasificacion.Enabled = false;
                        ddlManosde.Enabled = false;
                        ddlEstado.Enabled = false;
                        ddlConsulto.Enabled = false;
                        txtInformacion.Enabled = false;
                    }
                }
            }
            //if (Request.QueryString["id"] == "A")
            //    Response.Redirect("addAsunto.aspx?id=" + idAsun); 
        }
        else
        {
            Response.Redirect("addAsunto.aspx?id=A");
            //imgExcel.Visible = false;
        }
    }

    private void ConsultarDatosUsuario(string usr)
    { 
        Usuarios user = new Usuarios();
        user.Usuario = usr;
        DataTable dt = new ClinicaCES.Logica.LUsuarios().UsuarioConsultar(user);

        if (dt.Rows.Count > 0)
        {
            txtTelefono.Text = dt.Rows[0]["TELEFONO"].ToString();
            txtEmail.Text = dt.Rows[0]["EMAIL"].ToString();
        }
    }

    protected void ddlReporto_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlReporto.SelectedIndex != 0)
            ConsultarDatosUsuario(ddlReporto.SelectedValue);
        else
        {
            txtEmail.Text = string.Empty;
            txtTelefono.Text = string.Empty;
        }
    }

    protected void gvDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (bool.Parse(gvDetalle.DataKeys[e.Row.RowIndex]["hasFile"].ToString()))
            {
                e.Row.Cells[7].Text = "<img style=\"cursor:pointer\" onclick=\"abrirVentana('srcAdjuntosPop.aspx?id=" + txtCodigo.Text + "&cons=" + e.Row.Cells[0].Text + "')\" src=../icons/icon_attachment.gif border=0 />";
            }

            
            //string info = e.Row.Cells[3].Text;
            //if (info.Length > 100)
            //{
            //    e.Row.Cells[3].Text = e.Row.Cells[3].Text.Substring(0, 100) + " ...";
            //    e.Row.ToolTip = info;
            //}
        }
    }

    protected void ddlMenu_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlMenu.SelectedIndex != 0)
        {
            ListarModulo(ddlAplicacion.SelectedValue, ddlMenu.SelectedValue);
        }
    }

    protected void gvAdjuntos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string[] Nombre = e.Row.Cells[0].Text.Split('/');
            string fileName = Nombre[Nombre.Length - 1].Replace("%20"," ");
            Nombre = fileName.Split('.');
            string extencion = icono(Nombre[Nombre.Length - 1].ToLower());
            e.Row.Cells[0].Text = "<a href=" + e.Row.Cells[0].Text.Replace(" ", "%20") + " ><img src=../icons/" + extencion + " border=0 /> " + fileName + "</a>";
        }
    }

    private string icono(string extencion)
    {
        switch (extencion)
        { 
            case "xls":
                return "xls.gif";
            case "doc":
                return "doc.gif";
            case "txt":
                return "txt.jpg";
            case "zip":
                return "zip.gif";
            case "rar":
                return "rar.gif";
            case "jpg":
                return "image_icon.png";
            case "gif":
                return "image_icon.png";
            case "bmp":
                return "image_icon.png";
            case "jpeg":
                return "image_icon.png";
            default:
                return "icon_attachment.gif";

        }
    }
    protected void gvAdjuntos_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton imgEliminarAdjunto = (ImageButton)e.Row.FindControl("imgEliminarAdjunto");
            imgEliminarAdjunto.CommandArgument = e.Row.RowIndex.ToString();
        }
    }

    protected void gvAdjuntos_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "eliminar")
        {
            int fila = int.Parse(e.CommandArgument.ToString());
            GridViewRow row = gvAdjuntos.Rows[fila];
            string path = Server.MapPath(gvAdjuntos.DataKeys[row.RowIndex]["ruta"].ToString());
            using (StreamWriter sw = File.CreateText(path)) { }
            File.Delete(path);
            row.Visible = false;
        }
    }

    private string xmlArchivos()
    {
        string xml = "<Raiz>";
        string Archivo;
        foreach (GridViewRow row in gvAdjuntos.Rows)
        {
            Archivo = gvAdjuntos.DataKeys[row.RowIndex]["ruta"].ToString();
            xml += "<Datos Archivo = '" + Archivo + "' />";
        }

        return xml + "</Raiz>";
    }

    private void GuardarRegistroActividades
    (
        string Semana,
        string Responsable,
        string Cliente,
        string Negocio,
        string Linea,
        string Aplicacion,
        string Programa,
        string CNA,
        string Asunto,
        string Actividad,
        bool? Planeada,
        string Fecha,
        string Tiempo//,
        //string codigo
        ,string Estado
    )
    {

        DataTable dt = new ClinicaCES.Logica.LActividades().ActividadResponsable(Asunto);
        string ResponSableAnterior = string.Empty;
        string borrar = null;
        if (dt.Rows.Count > 0)
        {
            ResponSableAnterior = dt.Rows[0]["RESPONSABLE"].ToString();
            borrar = dt.Rows[0]["CODIGO"].ToString();
        }
            
        if (Responsable.Equals(ResponSableAnterior))
            return;

        ActividadReg act = new ActividadReg();

        act.Semana = Semana;
        act.Responsable = Responsable;
        act.Cliente = Cliente;
        act.Negocio = Negocio;
        act.Linea = Linea;
        act.Aplicacion = Aplicacion;
        act.Programa = Programa;
        act.CNA = CNA;
        act.Asunto = Asunto;
        act.Actividad = Actividad;
        act.Planeada = Planeada;
        act.Fecha = Fecha;
        act.Tiempo = Tiempo;
        if (Estado == "SO")
            act.Finalizado = true;
        //act.Codigo = codigo;


        if(!string.IsNullOrEmpty(borrar))
            act.Borrar = borrar;

        string msg = "3";

        if (new ClinicaCES.Logica.LActividades().ActividadActualizar(act))
        {
            msg = "1";
            //if (string.IsNullOrEmpty(hdnCodigo.Value))
            //{
            string email = new ClinicaCES.Logica.LUsuarios().Email(Responsable);
            
            Procedimientos.EnviarCorreo(email, "Nueva actividad asignada: " + Programa, "Nueva actividad asignada");
            //}
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string Semana(string fecha)
    {
        DateTime Fecha = Convert.ToDateTime(fecha);
        string year = Fecha.Year.ToString();
        return year + "-" + getWeek(Fecha).ToString();
    }

    public int getWeek(DateTime date)
    {
        System.Globalization.CultureInfo cult_info = System.Globalization.CultureInfo.CreateSpecificCulture("no");
        System.Globalization.Calendar cal = cult_info.Calendar;
        int weekCount = cal.GetWeekOfYear(date, cult_info.DateTimeFormat.CalendarWeekRule, cult_info.DateTimeFormat.FirstDayOfWeek);
        return weekCount;
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
        {
            if (ValidaGuardar())
            {
                string idAsunto = null;

                if (!string.IsNullOrEmpty(hdnIdAsunto.Value))
                    idAsunto = hdnIdAsunto.Value;

                if ((txtSolicitamos != null) && (txtCodigo == null))
                {
                    ddlManosde.SelectedValue = "CL";
                }

                string Asignado = null;
                string[] usrAsig = ddlAsignar.SelectedValue.Split('|');
                if (ddlAsignar.SelectedIndex != 0)
                    Asignado = usrAsig[0];

                string[] cliente = ddlCliente.SelectedValue.Split('|');
                Guardar(hdnHoraInicial.Value, cliente[0], cliente[1], ddlReporto.SelectedValue, txtTelefono.Text.Trim().ToUpper(),
                    txtEmail.Text.Trim().ToUpper(), ddlAplicacion.SelectedValue, ddlMenu.SelectedValue, ddlModulo.SelectedValue,
                    ddlSubModulo.SelectedValue, null /*ddlVersion.SelectedValue*/, ddlAsunto.SelectedValue, null/*estado*/,
                    ddlManosde.SelectedValue/*encargado*/, txtSolicitamos.Text.Trim()/*solicita*/, Session["Nick"].ToString(), ddlClasificacion.SelectedValue, ddlPrioridad.SelectedValue,
                    DateTime.Now.ToShortTimeString(), idAsunto, txtDescripcion.Text.Trim().ToUpper()
                    , ddlEstado.SelectedValue, ddlConsulto.SelectedValue, txtInformacion.Text.Trim().ToUpper(), hdnHoraInicialDetalle.Value,
                    DateTime.Now.ToShortTimeString(), chkRecibido.Checked, txtFecha.Text.Trim(), ddlRecibio.SelectedValue, ddlManosde.SelectedValue, Asignado
                    );
            }
        }
        else if (sender.Equals(imgCancelar) || sender.Equals(imgNuevo))
            Response.Redirect("addAsunto.aspx");
        else if (sender.Equals(imgActualizar))
            Response.Redirect("addAsunto.aspx?id=A");
    }

}