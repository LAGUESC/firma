﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcRegActividades.aspx.cs" Inherits="Movimientos_srcRegActividades" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
    <table align="center">
        <tr>     
            <td>
                <table align="center">
                    <tr>
                        <td>Semana:</td>
                        <td style="white-space:nowrap">
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>                                                       
                                    <asp:Label ID="lblSemana" runat="server"></asp:Label>
                                    <asp:TextBox Width="1" style="visibility:hidden" Height="1"  ID="txtSemana" runat="server" CssClass="form_input"></asp:TextBox>
                                    <ajaxToolkit:CalendarExtender Format="dd/MM/yyyy" ID="clnFecha"
                                        runat="server" TargetControlID="txtSemana" PopupButtonID="imgCalendar" 
                                        OnClientDateSelectionChanged="Semana"></ajaxToolkit:CalendarExtender>
                                        <img style="cursor:pointer" src="../icons/calendar.png" runat="server" id="imgCalendar" />  
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger ControlID="lnkSemana" EventName="click" />
                                </Triggers>
                            </asp:UpdatePanel>                                    
                            <asp:LinkButton ID="lnkSemana" runat="server" onclick="lnkSemana_Click"></asp:LinkButton>
                        </td>
                        <td> &nbsp;Responsable: </td>
                        <td>
                            <asp:DropDownList ID="ddlResponsable" runat="server" AutoPostBack="true"
                                onselectedindexchanged="ddlResponsable_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>       

        </tr>
        <tr>
            <td>
                <asp:UpdatePanel ID="upnlGrid" UpdateMode="Conditional" runat="server">
                    <ContentTemplate>
                        <asp:GridView AutoGenerateColumns="False" ID="gvRegistro" runat="server"
                        CellPadding="4" ForeColor="#333333" GridLines="None" Width="750px" 
                            onrowdatabound="gvRegistro_RowDataBound">                   
                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <RowStyle CssClass="normalrow" />
                            <AlternatingRowStyle CssClass="alterrow" />
                            <PagerStyle CssClass="cabeza" ForeColor="White"  />
                            <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                            <Columns>     
                                 <asp:BoundField DataField="APLICACION" HeaderText="Aplicación" />
                                 <asp:BoundField DataField="PROGRAMA" HeaderText="Programa" />
                                 <asp:BoundField DataField="ACTIVIDAD" HeaderText="Actividad" ItemStyle-Wrap="false" />
                                 <asp:BoundField DataField="FECHA" HeaderText="Fecha" />
                                 <asp:BoundField DataField="TIEMPO" HeaderText="Tiempo" />
                                 <asp:BoundField DataField="TINVERTIDO" HeaderText="Tiempo Invertido" HeaderStyle-Wrap="false" ItemStyle-HorizontalAlign="Center" />                                 
                                 <asp:CheckBoxField DataField="FINALIZADO" HeaderText="Finalizada" />                        
                                 <asp:BoundField DataField="PORCENTAJE" HeaderText="%" />
                                 <asp:BoundField DataField="FECHA_FIN" HeaderText="Fecha Fin" HeaderStyle-Wrap="false" />                                                                                                                      
                            </Columns>
                        </asp:GridView> 
                    </ContentTemplate>
                    <Triggers>                        
                        <asp:AsyncPostBackTrigger ControlID="lnkSemana" EventName="click" />
                        <asp:AsyncPostBackTrigger ControlID="ddlResponsable" EventName="SelectedIndexChanged" />                        
                    </Triggers>
                </asp:UpdatePanel>
                
            </td>
        </tr>
    </table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

