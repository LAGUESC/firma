﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="infAsuntoExcel.aspx.cs" Inherits="Movimientos_infAsuntoExcel" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="../Style/azul.css" rel="stylesheet" type="text/css" />
    <title>Informe Soporte</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <asp:Panel runat="server" ID="pnlExcelini">    
            <table cellspacing="0" cellpadding="3" rules="all" border="1" style="color:#333333;border-collapse:collapse;">
                <tr runat="server" id="cabezaArriba" class="cabeza">
                    <td align="center" colspan="7"><strong>INFORME LINEA SOPORTE</strong></td>                
                </tr>
                <tr>
                    <td rowspan="5" colspan="3"><asp:Literal ID="litImage" runat="server"></asp:Literal></td>
                </tr>
                <tr class="normalrow">
                    <td colspan="2">Comercializador:</td>
                    <td colspan="2"><asp:Label ID="lblComercializador" runat="server"></asp:Label></td>
                </tr>
                 <tr >
                    <td colspan="2">Asunto:</td>
                    <td colspan="2"><asp:Label ID="lblAsunto" runat="server"></asp:Label></td>
                </tr>
                <tr class="normalrow">
                    <td colspan="2">Clasificación:</td>
                    <td colspan="2"><asp:Label ID="lblClasificacion" runat="server"></asp:Label></td>
                </tr>
                <tr>
                    <td colspan="2">Caso:</td>
                    <td colspan="2"><asp:Label ID="lblCaso" runat="server"></asp:Label></td>
                </tr>
                <tr class="normalrow">
                <td colspan="3"></td>
                    <td colspan="2">Fecha del Reporte:</td>
                    <td colspan="2"><asp:Label ID="lblFecha" runat="server"></asp:Label></td>
                </tr>
            </table>
            <br />
            <table align="center">
                <tr>
                    <td>
                        <asp:GridView AutoGenerateColumns="False" ID="gvInforme" runat="server" 
                        CellPadding="3" ForeColor="#333333">                   
                            <FooterStyle BackColor="#F7DFB5" />
                            <RowStyle CssClass="normalrow"/>
                            <AlternatingRowStyle CssClass="alterrow" />
                            <HeaderStyle CssClass="cabeza" Font-Bold="True"  />                
                            <Columns>      
                                 <asp:BoundField DataField="CONSECUTIVO" HeaderText="Consecutivo" />
                                 <asp:BoundField DataField="FECHA" HeaderText="Fecha" />
                                 <asp:BoundField DataField="HORAFIN" HeaderText="Hora" />                      
                                 <asp:BoundField DataField="INFORMACION" HeaderText="Información" />                      
                                 <asp:BoundField DataField="IDUSUCLI" HeaderText="Contacto" />                      
                                 <asp:BoundField DataField="ESTADO" HeaderText="Estado" />                      
                                 <asp:BoundField DataField="ENCARGADO" HeaderText="Encargado" />                      
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
    </form>
</body>
</html>
