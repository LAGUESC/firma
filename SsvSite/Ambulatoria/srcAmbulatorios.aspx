﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcAmbulatorios.aspx.cs" Inherits="Admin_srcCursos" Title="Ambulatorios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="center">
        <tr>
            <td><asp:ImageButton runat="server" ID="imgCurso" ToolTip="Crear Curso" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
            <td>&nbsp;</td>
            <td><asp:ImageButton runat="server" ID="imgBuscar" ToolTip="Buscar" ImageUrl="../icons/magnify.png" onclick="Images_Click" /></td>
        </tr>     
    </table>
    <hr />
    <asp:UpdatePanel ID="UpdatePanel1" runat="server">
        <ContentTemplate>
            <table align="center">
 
                <tr>
                    <td>Filtro:</td>
                    <td><asp:DropDownList onkeydown="tabular(event,this)"  ID="ddlFlitro" runat="server">
                        <asp:ListItem Value="Estado">Estado</asp:ListItem>
                        <asp:ListItem Value="Curso">Procedimiento</asp:ListItem>
                        <asp:ListItem Value="Capacitador">Nombre</asp:ListItem>
                        <asp:ListItem Value="Convenio">Convenio</asp:ListItem>
                        <asp:ListItem Value="Cedula">Cedula</asp:ListItem>
                        
                        
                        </asp:DropDownList></td>    
                    <td><asp:LinkButton ID="lnkAgregar" runat="server" Text="Agregar" onclick="lnkAgregar_Click"></asp:LinkButton></td> 
                     <td>
                         
                    </td>
                </tr>    
                <tr>
                    <asp:Panel ID="pnlUsuario" runat="server" Visible="false">
                        <td>Procedimiento:</td>
                        <td colspan="2"><asp:TextBox CssClass="form_input" ID="txtBusquedaUsuario" runat="server"></asp:TextBox>
                        <asp:LinkButton ID="lnkKitarUsuario" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                        </td>  
                    </asp:Panel>
                </tr>
                <tr>
                    <asp:Panel ID="pnlNombre" runat="server" Visible="false">
                        <td>Nombre:</td>
                        <td colspan="2"><asp:TextBox CssClass="form_input" ID="txtBusquedaNombre" runat="server"></asp:TextBox>
                        <asp:LinkButton ID="lnkKitarNombre" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                        </td>  
                    </asp:Panel>
                </tr>
                <tr>
                    <asp:Panel ID="pnlFecha" runat="server" Visible="false">
                        <td>Convenio:</td>
                        <td colspan="2"><asp:TextBox CssClass="form_input" ID="txtBusquedaConvenio" runat="server"></asp:TextBox>
                        <asp:LinkButton ID="lnkKitarConvenio" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                        </td>  
                    </asp:Panel>
                </tr>
                <tr>
                    <asp:Panel ID="pnlCedula" runat="server" Visible="false">
                        <td>Cedula:</td>
                        <td colspan="2"><asp:TextBox CssClass="form_input" ID="txtCedula" runat="server"></asp:TextBox>
                        <asp:LinkButton ID="lnkKitarCedula" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                        </td>  
                    </asp:Panel>
                </tr>


               <tr>
                    <asp:Panel ID="pnlEstado" runat="server" Visible="false">
                        <td>Estado:</td>
                        <td colspan="2">
                            <asp:DropDownList ID="ddlEstado" runat="server"></asp:DropDownList>
                            <asp:HiddenField ID="txtEstado" runat="server" />
                            <%--<asp:TextBox CssClass="form_input" ID="txtBusquedaClasificacion" runat="server"></asp:TextBox>--%>
                        <asp:LinkButton ID="lnkKitarEstado" runat="server" Text="Quitar" onclick="lnkAgregar_Click"></asp:LinkButton>
                        </td>  
                    </asp:Panel>
                </tr>
            </table>
        </ContentTemplate>
    </asp:UpdatePanel>
    <table align="center">
        <tr>
            <td>
                <asp:GridView AutoGenerateColumns="False" ID="gvUsr" runat="server" AllowPaging="True"
                    CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" PageSize="20"
                    onrowdatabound="gvUsr_RowDataBound" 
                    onpageindexchanging="gvUsr_PageIndexChanging" EnableModelValidation="True" >
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle CssClass="normalrow" />
                    <AlternatingRowStyle CssClass="alterrow" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" Wrap="false"  />
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" Wrap="false" />
                    <Columns> 
                         <asp:BoundField DataField="ID" HeaderText="#" ItemStyle-Wrap="false" >     
<ItemStyle Wrap="False"></ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="IDENTIFICACION" HeaderText="IDENTIFICACION" ItemStyle-Wrap="false" >                                                               
<ItemStyle Wrap="False"></ItemStyle>
                         </asp:BoundField>
                         <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE"  >

                         </asp:BoundField>
                         <asp:BoundField DataField="PRE_PRE_DESCRIPCIO" HeaderText="PROCEDIMIENTO"   >  

                         </asp:BoundField>
                         <asp:BoundField DataField="TIPO_SOLICITUD" HeaderText="TIPO SOLICITUD" ItemStyle-Wrap="false"  ><ItemStyle Wrap="False"></ItemStyle>

                         </asp:BoundField>
                         <asp:BoundField DataField="CON_DESCRIP" HeaderText="CONVENIO" />
                         <asp:BoundField DataField="FECAPRC" HeaderText="FECHA" />
                        <asp:BoundField DataField="ESTADOSOLICITUD" HeaderText="" ItemStyle-Wrap="false" > 
<ItemStyle Wrap="False"></ItemStyle>
                         </asp:BoundField>
                        <asp:TemplateField HeaderText="Estado">
                                <ItemTemplate>
                                    <asp:Image ID="ImgSemaforo" runat="server" CausesValidation="False"  Height="16px" Width="16px" ></asp:Image>
                                </ItemTemplate>
                                <ItemStyle/>
                            </asp:TemplateField>       
                    </Columns>
                </asp:GridView> 
            </td>
        </tr>
    </table>
<div id="div1" runat="server" >
            <asp:Panel ID="Panel1" runat="server" CssClass="modalPopup" Style="display: none;
                text-align: center;" Width="400px" Height="150px" BackColor="White" ForeColor="Black" BorderStyle="Solid">
                <br />
    <table align="center">
        <tr>
            <td colspan="4" align="center" style="height: 24px"><h3><span><legend class="label">DISTRIBUCION EVENTOS</legend></span></h3></td>
        </tr>
        <tr>
            <td colspan="4" align="center"></td>
        </tr>
        <tr>
            <td style="width: 41px">Desde:</td>
            <td>
                <asp:TextBox ID="txtFecha" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="60px" ontextchanged="txtFecha_TextChanged"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                        runat="server" TargetControlID="txtFecha" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" ID="CalendarExtender1" OnClientDateSelectionChanged="ValidaSemana"
                    runat="server" TargetControlID="txtFecha" PopupButtonID="img1"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img1" />
            </td>
            <td>Hasta:</td>
            <td>
                <asp:TextBox ID="txtFechaF" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="60px" ontextchanged="txtFechaF_TextChanged1"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="txtFechaF_FilteredTextBoxExtender" 
                        runat="server" TargetControlID="txtFechaF" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" 
                    ID="txtFechaF_CalendarExtender" OnClientDateSelectionChanged="ValidaSemana"
                    runat="server" TargetControlID="txtFechaF" PopupButtonID="img2"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img2" />
                </td>
            </tr>
            <tr>
                <td colspan="2" align="right">
                    <asp:Button ID="btnConsultar" runat="server" Text="Distribuir" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn" onclick="Click_Botones" />
                </td>
                <td colspan="2" align="left">
                    <asp:Button ID="Button1" runat="server" Text="Cancelar" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn"  />
                </td>
            </tr>
    </table>             
                
<%--<asp:Button ID="OkButton" runat="server" Text="Aceptar" Width="68px" />--%>
            </asp:Panel>
            <br />
 
            &nbsp;
        </div>


    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

