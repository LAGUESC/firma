﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="AddAmbulatorios.aspx.cs" Inherits="Formulario_AddAmbulatorio" Title="Ambulatorios" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table align="center">
 
        <tr>
            <td align="right"  style="width:331px; height: 32px;">Id Paciente :
                <asp:Label ID="lblValidaIdPaciente" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
            </td>
            <td colspan="2" style="width: 268435584px; height: 32px;">
                <asp:TextBox  ID="txtIdentificacion" runat="server" CssClass="form_input" onblur="comprobarRed(this.value,this.id)" MaxLength="15"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtIdentificacion" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom"></ajaxToolkit:FilteredTextBoxExtender>
            <asp:LinkButton ID="lnkComprobar" runat="server" CausesValidation="false" onclick="Click_Botones"></asp:LinkButton>
            &nbsp;</td>




            </td>
            <td align="right" class="auto-style17" style="width: 254px; height: 32px;">Nombre :
                <asp:Label ID="Label11" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
            </td>
            <td colspan="1" style="height: 32px">
                <asp:TextBox ID="txtNombre" runat="server" CssClass="form_input" MaxLength="200" Width="341px" ></asp:TextBox>
            </td>
        </tr>

                <tr>
            <td align="right"  style="width:331px; height: 32px;">Asegurador :
                <asp:Label ID="lblValidaAsegurador" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
            </td>
            <td colspan="4" style="height: 32px;">
                <asp:DropDownList ID="ddlAsegurador" runat="server" Height="24px" Width="459px">
                </asp:DropDownList>
            </td>
           
        </tr>



        </table>

     <table align="center" style="width: 789px">
        <tr>
            <td align="right" class="auto-style17" style="height: 24px;" colspan="6"></td>
        </tr>

        <tr>
            <td align="right" class="auto-style17" style="width: 292px; height: 61px;">Tipo Solicitud : <asp:Label ID="LblValidaTipoSolicitud" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
            </td>
            <td colspan="2" style="width: 367px; height: 61px">
                <asp:DropDownList ID="ddlTipoSol" runat="server" Height="24px" Width="153px">
                    <asp:ListItem Value="0">.::No Aplica::.</asp:ListItem>
                    <asp:ListItem Value="1">Descargar Orden</asp:ListItem>
                    <asp:ListItem Value="2">Solicitud de Cambio</asp:ListItem>
                    <asp:ListItem Value="3">Solicitud de radicación</asp:ListItem>
                    <asp:ListItem Value="4">Solicitud de Código</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="right" class="auto-style17" style="width: 211px; height: 61px;" >&nbsp;</td>
            <td colspan="2" style="height: 61px; width: 268435488px;">
               
                &nbsp;</tr>
        <tr>
            <td align="right" class="auto-style17" style="width: 292px; height: 26px;">Soporte enviado : <asp:Label ID="Label13" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
            </td>
            <td colspan="2" style="height: 26px; width: 367px;">
                <asp:DropDownList ID="ddlSopEnviado" runat="server">
                    <asp:ListItem Selected="True" Value="0">.::Elegir::.</asp:ListItem>
                    <asp:ListItem Value="1">Central MOS</asp:ListItem>
                    <asp:ListItem Value="2">CNA</asp:ListItem>
                    <asp:ListItem Value="3">Oftalmologia Reg</asp:ListItem>
                    <asp:ListItem Value="4">Regional</asp:ListItem>
                    <asp:ListItem Value="5">No Aplica</asp:ListItem>
                    <asp:ListItem Value="6">ARL</asp:ListItem>
                    <asp:ListItem Value="7">Pólizas Escolares</asp:ListItem>
                    <asp:ListItem Value="8">EPM</asp:ListItem>
                    <asp:ListItem Value="9">Medicina Prepagada</asp:ListItem>
                    <asp:ListItem Value="10">Plan Complementario</asp:ListItem>
                    <asp:ListItem Value="11">Poliza de Salud</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="right" class="auto-style17" style="height: 26px; width: 211px;">No Evento :<asp:Label ID="Label14" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>&nbsp;</td>
            <td colspan="2" style="height: 26px; width: 268435488px;">
                                                <asp:TextBox ID="txtNrEvento" runat="server" CssClass="form_input" MaxLength="100" Width="100px"  ></asp:TextBox>
                                                </td>
        </tr>
        <tr>
            <td align="right" class="auto-style17" colspan="3">Fecha posible respuesta :
                <asp:Label ID="Label3" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="2">
   
                                <asp:TextBox ID="txtFecPosResp" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="60px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                        runat="server" TargetControlID="txtFecPosResp" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" ID="CalendarExtender1" 
                    runat="server" TargetControlID="txtFecPosResp" PopupButtonID="img2"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img2" __designer:mapid="766" />

                
                
                    </td>
        </tr>
        <tr>
            <td align="right" class="auto-style17" colspan="3">Tipo de servicio :
                <asp:Label ID="Label2" runat="server" ForeColor="Blue" Text="(*)"></asp:Label></td>
            <td colspan="2">
                <asp:DropDownList ID="ddlTipoSolicitud" runat="server">
                    <asp:ListItem Value="0">.:Elegir:.</asp:ListItem>
                    <asp:ListItem Value="1">Consulta</asp:ListItem>
                    <asp:ListItem Value="2">Cambio de Orden</asp:ListItem>
                    <asp:ListItem Value="3">Lab. Clinicos</asp:ListItem>
                    <asp:ListItem Value="4">Ayudas Diagnosticas</asp:ListItem>
                    <asp:ListItem Value="5">Procedimiento Medicos</asp:ListItem>
                    <asp:ListItem Value="6">Procedimiento Quirurgicos</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="right" class="auto-style17" colspan="6">&nbsp;</td>
        </tr>
       <tr>
           <td align="right" class="auto-style17" colspan="3">Especialidad requerida : <asp:Label ID="lblValidaEspecialidad" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
           </td>
           <td colspan="3">
               <asp:DropDownList ID="ddlEspecialidad" runat="server" OnSelectedIndexChanged="ddlEspecialidad_SelectedIndexChanged" AutoPostBack="true">
               </asp:DropDownList>
           </td>
       </tr>
       <tr>
          <td align="right" class="auto-style17" colspan="3">Procedimiento Indicado :
              <asp:Label ID="lblValidaProcIndicado" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
          </td>
          <td colspan="2">
               <asp:DropDownList ID="ddlProcIndicado" AutoPostBack="false" runat="server" Width="400px">
               </asp:DropDownList>
          </td>
       </tr>

       <tr>
           <td align="right" class="auto-style17" colspan="3">Procedimiento Autorizado :
            <asp:Label ID="lblValidaProcAutorizado" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
            </td>
            <td colspan="2">
               <asp:DropDownList ID="ddlProcAutorizado" AutoPostBack="false" runat="server" Width="400px"></asp:DropDownList>
              
            </td>
       </tr>

       <tr>
           <td align="right" class="auto-style17" colspan="3">Prestador Externo :
               <asp:Label ID="Label7" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
           </td>
           <td colspan="2">
               <asp:TextBox ID="txtPrestador" runat="server" CssClass="form_input" MaxLength="200" Width="200px"  ></asp:TextBox>
           </td>
       </tr>
     </table>

     <table align="center">
       <tr>
           <td align="right" class="auto-style17" style="width: 200px" >F.Consulta:
              <asp:Label ID="Label8" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
           </td>
           <td style="width: 105px">

               
                    <asp:TextBox ID="txtFecProcedimiento" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="60px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" 
                        runat="server" TargetControlID="txtFecProcedimiento" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" ID="CalendarExtender5" 
                    runat="server" TargetControlID="txtFecProcedimiento" PopupButtonID="img5"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img5" __designer:mapid="766" />


             </td>
            <td align="right" class="auto-style17" style="width: 40px">&nbsp;</td>
           <td align="right" class="auto-style17" style="width: 100px" >Fecha Cx:
              <asp:Label ID="Label15" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
           </td>
           <td style="width: 87px">
             
                     <asp:TextBox ID="txtFechaCx" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="60px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                        runat="server" TargetControlID="txtFechaCx" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" ID="CalendarExtender3" 
                    runat="server" TargetControlID="txtFechaCx" PopupButtonID="img1"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img1" __designer:mapid="766" />
                </td>
       </tr>

       <tr>
           <td align="right" class="auto-style17" colspan="2"> Observaciones :
               <asp:Label ID="Label10" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
           </td>
           <td colspan="3">
              <asp:TextBox ID="txtObservaciones" runat="server" CssClass="form_input" MaxLength="1000" Width="300px" Height="47px" TextMode="MultiLine"  ></asp:TextBox>
           </td>
       </tr>
</table>
       </br>
   

     <table align="center">
       <tr>
           <td align="right" class="auto-style17" style="width: 200px" colspan="3">Estado:
              <asp:Label ID="Label9" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
           </td>
           <td style="width: 105px" colspan="3">
               <asp:DropDownList ID="ddlEstado" runat="server" OnSelectedIndexChanged="ddlEstado_SelectedIndexChanged">
                   
               </asp:DropDownList>
           </td>
       </tr>
       <tr>
           <td align="right" class="auto-style17" > &nbsp;</td>
           <td align="right" class="auto-style17" > 
               &nbsp;</td>
           <td style="width: 40px">&nbsp;</td>
           <td >&nbsp;</td>
           <td >
                &nbsp;</td>
           <td >&nbsp;</td>
       </tr>

       <tr>
           <td align="right" class="auto-style17" > Alerta :
               <asp:Label ID="lblValidaAlerta" runat="server" ForeColor="Blue" Text="(*)"></asp:Label>
           </td>
           <td >Tiempo:</td>
           <td >
                <asp:TextBox ID="txtTiempo" runat="server" CssClass="form_input" MaxLength="100" Width="38px" ></asp:TextBox>
            </td>
           <td ><asp:DropDownList ID="ddlTiempo" runat="server">
               <asp:ListItem Value="1">Minutos</asp:ListItem>
               <asp:ListItem Value="2">Horas</asp:ListItem>
               <asp:ListItem Value="3">Dias</asp:ListItem>
               </asp:DropDownList>
           </td>
       </tr>
       <tr>
           <td align="right" class="auto-style17" > &nbsp;</td>
           <td align="right" class="auto-style17" > 
               &nbsp;</td>
           <td style="width: 40px">&nbsp;</td>
           <td >&nbsp;</td>
           <td >
                &nbsp;</td>
           <td >&nbsp;</td>
       </tr>

</table>
    <table align="center">                                                                         
        <tr>
            <td align="center" colspan="4">
                <asp:Button ID="btnAgregar" runat="server" Text="Guardar" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn" onclick="Click_Botones" />
                <asp:Button ID="btnNuevo" runat="server" Text="Actualizar" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn" onclick="Click_Botones" />
               
                <asp:Button ID="btnCancelar" runat="server" Text="Retornar" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn" onclick="Click_Botones" />
            </td>
        </tr>
    </table>

 
 <table align="center">
        <tr>
            <td align="center">
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="Label1" runat="server"></asp:Label>
            </td>
        </tr>
    </table>

    <table align="center">
        <tr>
            <td align="center">
                <asp:Label ID="lblDescripcion" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
    <asp:HiddenField ID="hdnAccion" runat="server" />
</asp:Content>

