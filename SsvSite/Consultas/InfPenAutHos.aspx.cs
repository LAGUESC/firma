﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Consultas_InfPenAutHos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Informe de Autorizaciones Pendientes en Pacientes Hospitalizados", this.Page);
            Consultar();
        }
        
        litScript.Text = string.Empty;
    }

    

    private void Consultar()
    {


        DataTable dtInforme = new ClinicaCES.Logica.LConsultas().ConsultarPendientes();
        DataTable dtPagina = Procedimientos.dtFiltrado("ORDNUMERO", "", dtInforme);
        string[] campo = { "ORDNUMERO" };


        if (dtInforme.Rows.Count > 0)
        {
            ViewState["dtInformes"] = dtInforme;
            ViewState["dtPaginas"] = dtPagina;
            Procedimientos.LlenarGrid(dtInforme, gvInforme);
            pnlInforme.Visible = true;
            string[] campos = { "ORDNUMERO" };
        }
        else
        {
            pnlInforme.Visible = false;
            Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
        }
        

    }

    protected void Excel_Click(object sender, ImageClickEventArgs e)
    {
        Procedimientos.ExportarDataTable(dtExcel(), "Informe de Autorizaciones Pendientes en Pacientes Hospitalizados");
    }

    public DataTable dtExcel()
    {

        string[] titulo = { "ORDNOMBRE", "ORDNUMERO", "PRECOD", "PRENOM", "FECHASOLIC", "CODESTADO", "OBSERVACION", "MTVCORRELATIVO", "ID", "CAMASERVICIO", "NOMBRES", "CONVENIO" };
        string[] columnas = { "ORDNOMBRE", "ORDNUMERO", "PRECOD", "PRENOM", "FECHASOLIC", "CODESTADO", "OBSERVACION", "MTVCORRELATIVO", "ID", "CAMASERVICIO", "NOMBRES", "CONVENIO" };
        string[] valores = new string[12];


        DataTable dt = new DataTable();
        DataTable dtPagina = (DataTable)ViewState["dtPaginas"];

        foreach (DataRow row in dtPagina.Rows)
        {
            for (int i = 0; i < titulo.Length; i++)
            {
                valores[i] = row[columnas[i]].ToString();
            }
            Procedimientos.CrearDatatable(titulo, valores, dt);
        }
        return dt;
    }

    protected void gvInforme_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInforme.PageIndex = e.NewPageIndex;
        gvInforme.DataSource = ViewState["dtPaginas"];
        gvInforme.DataBind();
    }

    protected void gvInforme_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");
           
           
            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

        }
    }
}