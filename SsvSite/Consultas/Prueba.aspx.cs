﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Consultas_Prueba : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Informe de Autorizaciones Pendientes en Pacientes Hospitalizados", this.Page);
           // Consultar(txtFecha.Text, txtFechaF.Text);
        }

        litScript.Text = string.Empty;
    }
    protected void Click_Botones(object sender, EventArgs e)
    {
        Consultar(txtFecha.Text, txtFechaF.Text);
    }


    private void Consultar(string FechaI, string FechaF)
    {

        if (FechaI == "" | FechaF == "")
        {
            pnlInforme.Visible = false;
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
        }

        else
        {
            DataTable dtGrid = new DataTable();
            dtGrid.Columns.Add(new DataColumn("CODPRESTACION", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("NOMPRESTACION", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("PAC_PAC_NUMERO", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("IDENTIFICACION", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("NOMBREPAC", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("CON_CON_CODIGO", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("NOMCONVENIO", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("RESPONSABLE", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("CODESTADO", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("DESESTADO", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("OBSERVACION", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("FECPENDGESTION", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("FECPROAUT", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("USUPROAUT", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("FECHA_APRO_NEG", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("USUA_APRO_NEG", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("FECSOLICITUD", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("FECCANCELA", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("USUCANCELA", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("FECDEVUELTA", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("USUDEVUELTA", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("EVENTO", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("ORDNUMERO", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("HORASREALES", System.Type.GetType("System.Decimal")));
            dtGrid.Columns.Add(new DataColumn("HORASEFECTIVAS", System.Type.GetType("System.Decimal")));

            DataTable dtInforme = new ClinicaCES.Logica.LConsultas().ConsultarPrueba(FechaI, FechaF);
            for (int i = 0; i < dtInforme.Rows.Count; i++)
            {
                DataRow fila = dtGrid.NewRow();
                DataRow dr = dtInforme.Rows[i];
                //TraerHoras(Convert.ToDateTime( dr[24].ToString()), Convert.ToDateTime( dr[25].ToString()));
                fila["CODPRESTACION"] = dtInforme.Rows[i]["CODPRESTACION"].ToString();
                fila["NOMPRESTACION"] = dtInforme.Rows[i]["NOMPRESTACION"];
                fila["PAC_PAC_NUMERO"] = dtInforme.Rows[i]["PAC_PAC_NUMERO"].ToString();
                fila["IDENTIFICACION"] = dtInforme.Rows[i]["IDENTIFICACION"].ToString();
                fila["NOMBREPAC"] = dtInforme.Rows[i]["NOMBREPAC"];
                fila["CON_CON_CODIGO"] = dtInforme.Rows[i]["CON_CON_CODIGO"].ToString();
                fila["NOMCONVENIO"] = dtInforme.Rows[i]["NOMCONVENIO"].ToString();
                fila["RESPONSABLE"] = dtInforme.Rows[i]["RESPONSABLE"];
                fila["CODESTADO"] = dtInforme.Rows[i]["CODESTADO"].ToString();
                fila["DESESTADO"] = dtInforme.Rows[i]["DESESTADO"].ToString();
                fila["OBSERVACION"] = dtInforme.Rows[i]["OBSERVACION"];
                fila["FECPENDGESTION"] = dtInforme.Rows[i]["FECPENDGESTION"].ToString();
                fila["FECPROAUT"] = dtInforme.Rows[i]["FECPROAUT"].ToString();
                fila["USUPROAUT"] = dtInforme.Rows[i]["USUPROAUT"];
                fila["FECHA_APRO_NEG"] = dtInforme.Rows[i]["FECHA_APRO_NEG"].ToString();
                fila["USUA_APRO_NEG"] = dtInforme.Rows[i]["USUA_APRO_NEG"].ToString();
                fila["FECSOLICITUD"] = dtInforme.Rows[i]["FECSOLICITUD"];
                fila["FECCANCELA"] = dtInforme.Rows[i]["FECCANCELA"].ToString();
                fila["USUCANCELA"] = dtInforme.Rows[i]["USUCANCELA"].ToString();
                fila["FECDEVUELTA"] = dtInforme.Rows[i]["FECDEVUELTA"];
                fila["USUDEVUELTA"] = dtInforme.Rows[i]["USUDEVUELTA"].ToString();
                fila["EVENTO"] = dtInforme.Rows[i]["EVENTO"].ToString();
                fila["ORDNUMERO"] = dtInforme.Rows[i]["ORDNUMERO"];
                fila["HORASREALES"] = dtInforme.Rows[i]["HORASREALES"];
                fila["HORASEFECTIVAS"] = TraerHoras(Convert.ToDateTime(dr[17].ToString()), Convert.ToDateTime(dr[13].ToString()));


                dtGrid.Rows.Add(fila);




            }

            //DataTable dtPagina = Procedimientos.dtFiltrado("ORDNUMERO", "", dtInforme);
            //string[] campo = { "ORDNUMERO" };


            if (dtGrid.Rows.Count > 0)
            {
                ViewState["dtPaginas"] = dtGrid;
                DataTable dtInformeFinal = dtGrid.Clone();
                dtInformeFinal = GroupBy("NOMCONVENIO|NOMPRESTACION", "CON_CON_CODIGO", "HORASREALES", "HORASEFECTIVAS", dtGrid);
                //    ViewState["dtInformes"] = dtGrid;
                ViewState["dtPaginas2"] = dtInformeFinal;
                Procedimientos.LlenarGrid(dtInformeFinal, gvInforme);
                pnlInforme.Visible = true;
                //    string[] campos = { "ORDNUMERO" };
            }
            //else
            //{
            //    pnlInforme.Visible = false;
            //    Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
            //}
        }

    }
    public DataTable GroupBy(string i_sGroupByColumn, string i_sAggregateColumn, string numeratorReal, string numeratorEfectic, DataTable i_dSourceTable)
    {

        DataView dv = new DataView(i_dSourceTable);
        string[] datos;
        datos = i_sGroupByColumn.Split('|');
        //getting distinct values for group column
        DataTable dtGroup = dv.ToTable(true, datos);

        //adding column for the row count
       
        dtGroup.Columns.Add("NUMERADORREAL", typeof(double));
        dtGroup.Columns.Add("NUMERADOREFECTIVO", typeof(double));
        dtGroup.Columns.Add("DENOMINADOR", typeof(int));
        dtGroup.Columns.Add("INDICADORREAL", typeof(double));
        dtGroup.Columns.Add("INDICADOREFECTIVO", typeof(double));
       
        //looping thru distinct values for the group, counting
        foreach (DataRow dr in dtGroup.Rows)
        {
            dr["NUMERADORREAL"] = i_dSourceTable.Compute("Sum(" + numeratorReal + ")", datos[0] + " = '" + dr[datos[0]] + "' and " + datos[1] + " = '" + dr[datos[1]] + "' ");
            dr["NUMERADOREFECTIVO"] = i_dSourceTable.Compute("Sum(" + numeratorEfectic + ")", datos[0] + " = '" + dr[datos[0]] + "' and " + datos[1] + " = '" + dr[datos[1]] + "' ");
            dr["DENOMINADOR"] = i_dSourceTable.Compute("Count(" + i_sAggregateColumn + ")", datos[0] + " = '" + dr[datos[0]] + "' and " + datos[1] + " = '" + dr[datos[1]] + "' ");
            dr["INDICADORREAL"] = Convert.ToDouble( dr["NUMERADORREAL"]) / Convert.ToDouble( dr["DENOMINADOR"]);
            dr["INDICADOREFECTIVO"] = Convert.ToDouble(dr["NUMERADOREFECTIVO"]) / Convert.ToDouble(dr["DENOMINADOR"]);
        }

        //returning grouped/counted result
        return dtGroup;
    }
    protected double TraerHoras(DateTime start_date, DateTime end_date)
    {
        TimeSpan HoraIni = new TimeSpan(07, 30, 00);
        TimeSpan HoraFin = new TimeSpan(18, 30, 00);
        TimeSpan HoraFinS = new TimeSpan(13, 30, 00);
        DateTime new_star_date= start_date;
        DateTime new_end_date = end_date;
        TimeSpan difDias = end_date - start_date;
        TimeSpan Horas = new TimeSpan(00, 00, 00);
        bool bandera=false;
        while (bandera==false)

        //if (Convert.ToInt32(difDias.Days) > 0)
        {
            //for (int i = 0; i <= Convert.ToInt32(difDias.Days); i++)
            //{
                
                if (Convert.ToInt32(new_star_date.DayOfWeek) == 6)
                {
                    if (new_star_date.TimeOfDay < HoraIni)
                    {
                        new_star_date = new_star_date.Date + HoraIni;
                    }
                    if (new_star_date.TimeOfDay > HoraFinS)
                    {
                        new_star_date = new_star_date.AddDays(2) + HoraIni;
                    }
                    if (new_star_date.Date == end_date.Date)
                    {
                        Horas = Horas + (end_date - new_star_date);
                        bandera = true;
                    }
                    else
                    {
                        Horas = Horas + ((new_star_date.Date + HoraFinS) - (new_star_date));
                        new_star_date = new_star_date.Date.AddDays(1) + HoraIni;

                    }
                    //new_star_date = new_star_date.AddDays(1);
                }
                if (Convert.ToInt32(new_star_date.DayOfWeek) == 0)
                {
                    new_star_date = new_star_date.Date.AddDays(1) + HoraIni;
                }
                if (new ClinicaCES.Logica.LConsultas().ConsultarFestivos( new_star_date.ToShortDateString()).Rows.Count > 0)
                {
                    new_star_date = new_star_date.Date.AddDays(1) + HoraIni;
                }
                //if (Convert.ToInt32(new_end_date.DayOfWeek) == 6)
                //{
                //    if (new_end_date.TimeOfDay < HoraIni)
                //    {
                //        new_end_date = new_end_date.Date + HoraFin;
                //    }
                //    if (new_end_date.TimeOfDay > HoraFin)
                //    {
                //        new_end_date = new_end_date.AddDays(2) + HoraFin;
                //    }
                //    Horas = Horas + ((new_end_date.Date + HoraFinS) - (new_end_date));
                //    new_star_date = new_end_date.AddDays(1);
                //}
                //if (Convert.ToInt32(new_end_date.DayOfWeek) == 0)
                //{
                //    new_end_date = new_end_date.Date.AddDays(1) + HoraFin;
                //}
                //if (new ClinicaCES.Logica.LConsultas().ConsultarFestivos(new_end_date.ToShortDateString()).Rows.Count > 0)
                //{
                //    new_end_date = new_end_date.Date.AddDays(1) + HoraFin;
                //}
                if (new_star_date.TimeOfDay < HoraIni)
                {
                    new_star_date = new_star_date.Date + HoraIni;
                }
                //if (new_star_date.TimeOfDay > HoraFinS)
                //{
                //    new_star_date = new_star_date.AddDays(1) + HoraIni;
                //}
                //if (new_end_date.TimeOfDay < HoraIni)
                //{
                //    new_end_date = new_end_date.Date + HoraFin;
                //}
                if (new_end_date.TimeOfDay > HoraFin)
                {
                    new_end_date = new_end_date.AddDays(1) + HoraFin;
                }
                if (new_star_date.Date <= end_date.Date)
                {
                    if (new_star_date.Date == end_date.Date & bandera == false)
                    {
                        Horas = Horas + (end_date - new_star_date);
                        bandera = true;
                    }
                    else
                    {
                        if (bandera == false)
                        {
                            Horas = Horas + ((new_star_date.Date + HoraFin) - new_star_date);
                            new_star_date = new_star_date.Date.AddDays(1) + HoraIni;
                        }
                    }
                }
                
            //}

        }
        //double tothoras = Horas.TotalHours;
        //double totalminutes = (double)Horas.Minutes / 60;
        //double totthoras = (double)Horas.Hours / 24 + (double)Horas.Minutes / 60 + (double)Horas.Seconds / 3600;
        //return double.Parse((Horas.TotalHours * 24 + Horas.TotalMinutes * 60 + Horas.TotalSeconds * 3600).ToString());
        return Horas.TotalHours;
;

        //
     
    }


    protected void Excel_Click(object sender, ImageClickEventArgs e)
    {
        Procedimientos.ExportarDataTable(dtExcel(), "Informe detallado de Indicador Oportunidad");
    }

    public DataTable dtExcel()
    {

        string[] titulo = { "CODPRESTACION", "NOMPRESTACION", "PAC_PAC_NUMERO", "IDENTIFICACION", "NOMBREPAC", "CON_CON_CODIGO", "NOMCONVENIO", "RESPONSABLE", "CODESTADO", "DESESTADO", "OBSERVACION", "FECPENDGESTION", "FECPROAUT", "USUPROAUT", "FECHA_APRO_NEG", "USUA_APRO_NEG", "FECSOLICITUD", "FECCANCELA", "USUCANCELA", "FECDEVUELTA", "USUDEVUELTA", "EVENTO", "ORDNUMERO", "HORASREALES1", "HORASEFECTIVAS1"  };
        string[] columnas = { "CODPRESTACION", "NOMPRESTACION", "PAC_PAC_NUMERO", "IDENTIFICACION", "NOMBREPAC", "CON_CON_CODIGO", "NOMCONVENIO", "RESPONSABLE", "CODESTADO", "DESESTADO", "OBSERVACION", "FECPENDGESTION", "FECPROAUT", "USUPROAUT", "FECHA_APRO_NEG", "USUA_APRO_NEG", "FECSOLICITUD", "FECCANCELA", "USUCANCELA", "FECDEVUELTA", "USUDEVUELTA", "EVENTO", "ORDNUMERO", "HORASREALES1", "HORASEFECTIVAS1" };
        string[] valores = new string[25];


        DataTable dt = new DataTable();
        DataTable dtPagina = (DataTable)ViewState["dtPaginas"];
        dtPagina.Columns.Add(new DataColumn("HORASREALES1", System.Type.GetType("System.String")));
        dtPagina.Columns.Add(new DataColumn("HORASEFECTIVAS1", System.Type.GetType("System.String")));
        foreach (DataRow row in dtPagina.Rows)
        {
            row["HORASREALES1"] = row["HORASREALES"].ToString().Replace(',','-');
            row["HORASEFECTIVAS1"] = row["HORASEFECTIVAS"].ToString().Replace(',', '-');
            for (int i = 0; i < titulo.Length; i++)
            {
                valores[i] = row[columnas[i]].ToString();
            }
            Procedimientos.CrearDatatable(titulo, valores, dt);
        }
        return dt;
    }

    protected void gvInforme_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInforme.PageIndex = e.NewPageIndex;
        gvInforme.DataSource = ViewState["dtPaginas"];
        gvInforme.DataBind();
    }

    protected void gvInforme_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");


            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

        }
    }
    private bool ValidaSemana()
    {
        bool valido = true;

        if ((txtFecha.Text != "") && (txtFechaF.Text != ""))
        {
            DateTime FechaIni = Convert.ToDateTime(txtFecha.Text);
            DateTime FechaFin = Convert.ToDateTime(txtFechaF.Text);

            if (FechaIni.CompareTo(FechaFin) == 1)
            {
                valido = false;
            }


            if (!valido)
            {
                txtFecha.Text = string.Empty;
                txtFechaF.Text = string.Empty;
            }

        }

        return valido;
    }

    protected void txtFechaF_TextChanged1(object sender, EventArgs e)
    {
        if (!ValidaSemana())
        {
            Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
            pnlInforme.Visible = false;
        }
    }

    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtFechaF.Text.Trim()))
            if (!ValidaSemana())
            {
                Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
                pnlInforme.Visible = false;
            }
    }
}