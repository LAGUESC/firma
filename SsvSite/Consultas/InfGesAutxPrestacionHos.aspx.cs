﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Consultas_InfGesAutxPrestacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Informe de autorizaciones realizadas por prestación y rango de fechas", this.Page);
        }
        this.Form.DefaultButton = btnConsultar.UniqueID;
        litScript.Text = string.Empty;
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        Consultar(txtFecha.Text, txtFechaF.Text);
    }

    private void Consultar(string FechaI, string FechaF)
    {

        if (FechaI == "" | FechaF == "")
        {
            pnlInforme.Visible = false;
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
        }

        else
        {
            
            DataSet ds = new ClinicaCES.Logica.LConsultas().ConsultarGestion(FechaI, FechaF);
            DataRow[] PR = ds.Tables[0].Select("(CODESTADO=3 or CODESTADO=4 or CODESTADO=5 or CODESTADO=6)");
            DataTable dtInforme = ds.Tables[0].Clone();
            foreach (DataRow element in PR)
            {
                dtInforme.ImportRow(element);
            }
            dtInforme = GroupBy("CODPRESTACION|NOMPRESTACION|DESESTADO|NOMRESPONSABLE", "CODPRESTACION", dtInforme);

          

            DataTable dtPagina = Procedimientos.dtFiltrado("CODPRESTACION", "", dtInforme);
            string[] campo = { "CODPRESTACION" };


            if (dtInforme.Rows.Count > 0)
            {
                ViewState["dtInformes"] = dtInforme;
                ViewState["dtPaginas"] = dtPagina;
                Procedimientos.LlenarGrid(dtInforme, gvInforme);
                pnlInforme.Visible = true;
                string[] campos = { "CODPRESTACION" };
            }
            else
            {
                pnlInforme.Visible = false;
                Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
            }
        }

    }



    public DataTable GroupBy(string i_sGroupByColumn, string i_sAggregateColumn, DataTable i_dSourceTable)
    {

        DataView dv = new DataView(i_dSourceTable);
        string[] datos;
        datos = i_sGroupByColumn.Split('|');
        //getting distinct values for group column
        DataTable dtGroup = dv.ToTable(true, datos);

        //adding column for the row count
        dtGroup.Columns.Add("CANTIDAD", typeof(int));

        //looping thru distinct values for the group, counting
        foreach (DataRow dr in dtGroup.Rows)
        {
            dr["CANTIDAD"] = i_dSourceTable.Compute("Count(" + i_sAggregateColumn + ")", datos[0] + " = '" + dr[datos[0]] + "' and " + datos[2] + " = '" + dr[datos[2]] + "' and " + datos[3] + " = '" + dr[datos[3]] + "' ");
        }

        //returning grouped/counted result
        return dtGroup;
    }

    protected void Excel_Click(object sender, ImageClickEventArgs e)
    {
        Procedimientos.ExportarDataTable(dtExcel(), "Informe Prestaciones entre Fechas");
    }

    public DataTable dtExcel()
    {

        string[] titulo = { "CODPRESTACION", "NOMPRESTACION", "DESESTADO", "NOMRESPONSABLE", "CANTIDAD" };
        string[] columnas = { "CODPRESTACION", "NOMPRESTACION", "DESESTADO", "NOMRESPONSABLE", "CANTIDAD" };
        string[] valores = new string[5];


        DataTable dt = new DataTable();
        DataTable dtPagina = (DataTable)ViewState["dtPaginas"];

        foreach (DataRow row in dtPagina.Rows)
        {
            for (int i = 0; i < titulo.Length; i++)
            {
                valores[i] = row[columnas[i]].ToString();
            }
            Procedimientos.CrearDatatable(titulo, valores, dt);
        }
        return dt;
    }


    private bool ValidaSemana()
    {
        bool valido = true;

        if ((txtFecha.Text != "") && (txtFechaF.Text != ""))
        {
            DateTime FechaIni = Convert.ToDateTime(txtFecha.Text);
            DateTime FechaFin = Convert.ToDateTime(txtFechaF.Text);

            if (FechaIni.CompareTo(FechaFin) == 1)
            {
                valido = false;
            }


            if (!valido)
            {
                txtFecha.Text = string.Empty;
                txtFechaF.Text = string.Empty;
            }

        }

        return valido;
    }

    protected void txtFechaF_TextChanged1(object sender, EventArgs e)
    {
        if (!ValidaSemana())
        {
            Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
            pnlInforme.Visible = false;
        }
    }

    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtFechaF.Text.Trim()))
            if (!ValidaSemana())
            {
                Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
                pnlInforme.Visible = false;
            }
    }

    protected void gvInforme_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInforme.PageIndex = e.NewPageIndex;
        gvInforme.DataSource = ViewState["dtPaginas"];
        gvInforme.DataBind();
    }

    protected void gvInforme_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

        }
    }
}
