﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="InfGesAutxEstadosHos.aspx.cs" Inherits="Consultas_InfGesAutxEstadosHos" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="InfGesAutxEstadosHos.aspx.cs" Inherits="Consultas_InfGesAutxEstadosHos" Title="Informe de estados por rango de fechas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   

    <br />
    <table align="center">
        <tr>
            <td style="width: 41px">
                Desde:
            </td>
            <td>
                    <asp:TextBox ID="txtFecha" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="60px" ontextchanged="txtFecha_TextChanged"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                        runat="server" TargetControlID="txtFecha" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" ID="CalendarExtender1" OnClientDateSelectionChanged="ValidaSemana"
                    runat="server" TargetControlID="txtFecha" PopupButtonID="img1"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img1" __designer:mapid="766" />
            </td>
            <td>
                Hasta:
            </td>
            <td>
                    <asp:TextBox ID="txtFechaF" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="60px" ontextchanged="txtFechaF_TextChanged1"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="txtFechaF_FilteredTextBoxExtender" 
                        runat="server" TargetControlID="txtFechaF" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" 
                    ID="txtFechaF_CalendarExtender" OnClientDateSelectionChanged="ValidaSemana"
                    runat="server" TargetControlID="txtFechaF" PopupButtonID="img2"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img2" __designer:mapid="766" /></td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn" onclick="Click_Botones" />
            </td>
        </tr>
    </table>
    
    <table align="center" style="width: 605px">
        <tr>
            <td align="center">
    <asp:Panel ID="pnlInforme" runat="server" Visible="false">
    
    <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="../icons/icon_excel.gif" 
            onclick="Excel_Click" style="height: 16px" />
        <table border="1">
        </table>
        <table align="center" style="width: 590px">
            <tr>
                <td>
                    <asp:GridView AutoGenerateColumns="False" ID="gvInforme" runat="server" AllowPaging="True" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="20" pa
                        onrowdatabound="gvInforme_RowDataBound"
                        onpageindexchanging="gvInforme_PageIndexChanging" Width="570px">                   
                        <FooterStyle BackColor="#F7DFB5" />
                        <RowStyle CssClass="normalrow"/>
                        <AlternatingRowStyle CssClass="alterrow" />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White"  />                
                        <Columns>      
                             <asp:BoundField DataField="DESESTADO" HeaderText="DESESTADO" />
                             <asp:BoundField DataField="NOMRESPONSABLE" HeaderText="NOMRESPONSABLE" />
                             <asp:BoundField DataField="CANTIDAD" HeaderText="CANTIDAD" />                                                       
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
            </td>
        </tr>
    </table>
        <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>
