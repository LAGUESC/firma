﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Masters_MasterNew : System.Web.UI.MasterPage
{
    int Id_Master = 666;
    string Dispositivo = "";
    #region "Formulario"

    protected void Page_Load(object sender, EventArgs e)
    {
       

        if (!Page.IsPostBack)
        {
            CargarMenuPPal();
        }
    }
    protected void lnkSalir_Click(object sender, EventArgs e)
    {
        Session.Clear();
        Response.Redirect("~/General/LogIn.aspx");
    }
    //protected void lnkDirectorio_Click(object sender, EventArgs e)
    //{
    //    Response.Redirect("~/Formularios/Principal/frmDirectorio.aspx");
    //}
    //protected void imguser_DataBinding(object sender, EventArgs e)
    //{
    //    cEmpleados_Activos objEmpleados_Activos = new cEmpleados_Activos();
    //    objEmpleados_Activos.E_MAIL = Session["EmailUsuario"].ToString();
    //    DataTable dtDatos = objEmpleados_Activos.ListarPorCorreo();

    //    Session["EMPLEADO"] = dtDatos.Rows[0]["EMPLEADO"].ToString();
    //}

    #endregion
    #region "Funciones"

    private bool CargarMenuPPal()
    {
        //lblProyecto.Text =Session["Codigo_Proyecto"].ToString()+"  "+ Session["Proyecto"].ToString();
        if (Session["Nick"] != null)
        {
            PermisosFormulario(Session["Nick"].ToString(), Id_Master, menu);

            lblNombreUsuario.Text = Session["Nick"].ToString();
            lblNombre.Text = Session["Nombre"].ToString().ToLower();
            lblNombre_Cargo.Text = Session["Cargo"].ToString();
            imguser.ImageUrl = "~/img/User.svg.png";

            imguserpeq.ImageUrl = "~/img/User.svg.png";
            imguserdir.ImageUrl = "~/img/User.svg.png";
        }
        else
        {
            Response.Redirect("~/General/LogIn.aspx");
        }
        //lblFuncionario.Text = "Funcionario desde el " + Convert.ToDateTime(dtDatos.Rows[0]["F_INGRESO"]).ToLongDateString().ToString();
        //Session["EMPLEADO"] = dtDatos.Rows[0]["EMPLEADO"].ToString();

        //imguser.ImageUrl = "~/ImageHandler.ashx?EMPLEADO=" + Session["EMPLEADO"].ToString();
        //imguserdir.ImageUrl = "~/ImageHandler.ashx?EMPLEADO=" + Session["EMPLEADO"].ToString();
        //imguserpeq.ImageUrl = "~/ImageHandler.ashx?EMPLEADO=" + Session["EMPLEADO"].ToString();

        return true;

    }
    private bool PermisosFormulario(string Usuario, int Id_MasterPage ,  Control control)
    {
        DataTable dt = new ClinicaCES.Logica.LMenu().PermisosFormularios(Usuario,Id_MasterPage);

        foreach (DataRow row in dt.Rows)
        {
            foreach (Control lih in control.Controls)
            {
                if (lih.ID == row[3].ToString())
                {
                    lih.Visible = true;

                    foreach (Control li in lih.Controls)
                    {
                        if (li.ID == row[7].ToString())
                        {
                            li.Visible = true;
                        }
                    }

                }
            }
        }

        return true;
    }


    #endregion

}
