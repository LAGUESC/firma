﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Xml.Linq;
using ClinicaCES.DA;

public partial class Masters_Master : System.Web.UI.MasterPage
{
    protected void Page_Load(object sender, EventArgs e)
    {        
        //Procedimientos.ValidarSession(this.Page);
        //if (!IsPostBack)
        //{
        //    GenerarMenu(Session["Perfil1"].ToString());
        //    if (Session["Tema1"].ToString().Equals("dark_blue"))
        //        tvMenu.ImageSet = TreeViewImageSet.Arrows;
        //    else
        //        tvMenu.ImageSet = TreeViewImageSet.XPFileExplorer;
        //    lblCreditos.Text = "&nbsp;(<em><a target=_blank href=http://www.clinicaces.com>www.clinicaces.com</a></em>)";
        //    lblUsuario.Text = "&nbsp;<span style=\"text-decoration: underline\">" + Session["Nombre1"].ToString() + "</span> ( <em>" + Session["PerfilN1"].ToString() + "</em> )&nbsp;";
        //    llenarPendientes();
        //}
    }

    protected void lnkCerrar_Click(object sender, EventArgs e)
    {
        HttpCookie SIDGVCookie = Request.Cookies["SIDGV1"];

        SIDGVCookie["Save1"] = "false";
        SIDGVCookie["Nombre1"] = Session["Nombre1"].ToString();
        SIDGVCookie["Perfil"] = Session["Perfil1"].ToString();
        SIDGVCookie["Nick1"] = Session["Nick1"].ToString();

        SIDGVCookie.Expires = DateTime.Now.AddDays(10d);
        Response.Cookies.Add(SIDGVCookie);
        //Response.Cookies.Add(SIDGVCookie);
        Session.Clear();
        Response.Redirect("../General/LoginNew.aspx");
    }

    private void GenerarMenu(string Nivel)
    {
        string[] nomParam = { "@Nivel" };
        object[] valParam = { Nivel };

        DataTable dtMenu = new DA().getDataTable("uspGeneraMenu", nomParam, valParam);
        DataTable dtMenuPadres = Procedimientos.dtFiltrado(null, "ID_PADRE = -1 ", dtMenu);

        foreach (DataRow row in dtMenuPadres.Rows)
        {
            TreeNode node = new TreeNode();
            node.Text = row["ETIQUETAMENU"].ToString();
            node.NavigateUrl = row["URL"].ToString();
            tvMenu.Nodes.Add(node);
            AgregarHijos(row["ID_MENU"].ToString(), node, dtMenu);
        }
    }

    private void AgregarHijos(string MenuID, TreeNode ParentNode, DataTable dtMenu)
    {
        DataTable dtHijos = Procedimientos.dtFiltrado("ORDEN", "ID_PADRE = " + MenuID, dtMenu);

        foreach (DataRow row in dtHijos.Rows)
        {
            TreeNode node = new TreeNode();
            node.Text = row["ETIQUETAMENU"].ToString();
            node.NavigateUrl = row["URL"].ToString();
            ParentNode.ChildNodes.Add(node);
            AgregarHijos(row["ID_MENU"].ToString(), node, dtMenu);
            //tvMenu.Nodes.Add(node); 


        }
    }
    //protected void Timer1_Tick(object sender, EventArgs e)
    //{
    //    DataTable dt = new ClinicaCES.Logica.LMaestros().ConsultarAlertas(Session["Nick1"].ToString());
    //    if (dt.Rows.Count > 0)
    //    {

    //    }
    //}
    protected void Timer1_Tick(object sender, EventArgs e)
    {
        llenarPendientes();
    }
    protected void llenarPendientes()
    {
        try
        {
            DataTable dt = new ClinicaCES.Logica.LMaestros().ConsultarAlertas(Session["Nick1"].ToString(), Convert.ToInt32(Session["Perfil1"].ToString()));
            if (dt.Rows.Count > 0)
            {
                BulletedList1.DataSource = crearDataTable(dt, Convert.ToInt32(Session["Perfil1"].ToString()));

                BulletedList1.DataValueField = "ID";
                BulletedList1.DataTextField = "LISTA";
                BulletedList1.DataBind();
                // ModalPopupExtender1.Show();

            }
        }
        catch (Exception ex)
        {

        }
    }
    protected DataTable crearDataTable(DataTable dt, int Perfil)
    {

        int suma = 0;
        DataTable dtGrid = new DataTable();
        if (dt.Rows.Count > 0)
        {
            dtGrid.Columns.Add(new DataColumn("ID", System.Type.GetType("System.String")));
            dtGrid.Columns.Add(new DataColumn("LISTA", System.Type.GetType("System.String")));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow fila = dtGrid.NewRow();
                if (Perfil >= 3)
                {
                    if (dt.Rows[i]["TIPO"].ToString() == "1")
                    {
                        fila["ID"] = "../Maestros/srcAutPendientes.aspx?cookiePendientes=" + Procedimientos.cifrar(dt.Rows[i]["ID"].ToString()) + "";
                        //fila["ID"] = dt.Rows[i]["ROWNUM"];
                        fila["LISTA"] = "Tiene pendiente gestionar la autorización de la prestación " + dt.Rows[i]["PRESTACION"] + " para el paciente " + dt.Rows[i]["ID"] + " - " + dt.Rows[i]["NOMBRE"] + " ubicado en la cama " + dt.Rows[i]["CAMA"];
                    }
                    if (dt.Rows[i]["TIPO"].ToString() == "2")
                    {
                        fila["ID"] = "http://www.google.com.co";
                        //fila["ID"] = dt.Rows[i]["ROWNUM"];
                        fila["LISTA"] = "Tiene pendiente gestionar la autorización de la prestación " + dt.Rows[i]["PRESTACION"] + " para el paciente " + dt.Rows[i]["ID"] + " - " + dt.Rows[i]["NOMBRE"] + " en " + dt.Rows[i]["CAMA"];
                    }
                }
                else
                {
                    if (dt.Rows[i]["TIPO"].ToString() == "1")
                    {
                        fila["ID"] = "../Maestros/srcAutPendientes.aspx?cookiePendientes=" + Procedimientos.cifrar(dt.Rows[i]["ID"].ToString()) + "";
                        //fila["ID"] = dt.Rows[i]["ROWNUM"];
                        fila["LISTA"] = "El usuario " + dt.Rows[i]["RESPONSABLE"] + " tiene pendiente gestionar la autorización de la prestación " + dt.Rows[i]["PRESTACION"] + " para el paciente " + dt.Rows[i]["ID"] + " - " + dt.Rows[i]["NOMBRE"] + " ubicado en la cama " + dt.Rows[i]["CAMA"];
                    }
                    if (dt.Rows[i]["TIPO"].ToString() == "2")
                    {
                        fila["ID"] = "http://www.google.com.co";
                        //fila["ID"] = dt.Rows[i]["ROWNUM"];
                        fila["LISTA"] = "El usuario " + dt.Rows[i]["RESPONSABLE"] + " tiene pendiente gestionar la autorización de la prestación " + dt.Rows[i]["PRESTACION"] + " para el paciente " + dt.Rows[i]["ID"] + " - " + dt.Rows[i]["NOMBRE"] + " en " + dt.Rows[i]["CAMA"];
                    }
                }
               
                dtGrid.Rows.Add(fila);
                suma = suma + 22;
            }
        }
        Panel1.Height = 160 + suma;
        return dtGrid;
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();
    }
}
