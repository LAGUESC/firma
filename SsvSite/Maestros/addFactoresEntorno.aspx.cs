﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addFactoresEntorno : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Factores Entorno", this.Page);

            string script = "if(trim(document.getElementById('" + txtCodigo.ClientID + "').value)!= '') { " +
                " __doPostBack('" + lnkComprobar.UniqueID + "','') };";

            txtCodigo.Attributes.Add("onblur", script);

            //imgBuscarCriterio.Attributes.Add("onclick", "abrirVentana('srcCriterioEntorno.aspx?pop=" + txtCriterio.ClientID + "&nom=" + hdnNombre.ClientID + "')");

            if (Request.QueryString["id"] != null)
                Buscar(Request.QueryString["id"]);
            else
                Procedimientos.Script("SeleccionarText('" + txtCodigo.ClientID + "')", litScript);
        }
        litScript.Text = string.Empty;
    }
    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
        {
            if (ValidaGuardar())
                Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(), txtDescripcion.Text.Trim(), txtPeso.Text.Trim());
        }
        else if (sender.Equals(imgCancelar) || sender.Equals(imgNuevo))
            Response.Redirect("addFactoresEntorno.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false, Session["Nick1"].ToString());
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true, Session["Nick1"].ToString());
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        {
            if (ValidaGuardar())
                Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString(), txtDescripcion.Text.Trim(), txtPeso.Text.Trim());   
        }
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addFactoresEntorno.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false, Session["Nick1"].ToString());
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true, Session["Nick1"].ToString());
        //else if (sender.Equals(btnAgregar))
        //{ 
        //    if(ValidarFila())
        //        insertarFila(hdnNombre.Value, txtCriterio.Text.Trim());
        //}


            
    }

    private void CambiarEstado(string Codigo, bool? Estado, string usr)
    {
        FactoresEntorno est = new FactoresEntorno();
        est.Codigo = Codigo;
        est.Estado = Estado;
        est.usr = usr;
        string msg = "3";

        if (new ClinicaCES.Logica.LFactoresEntorno().FactoresEntornoEstado(est))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addFactoresEntorno.aspx?id=" + Codigo + "')", litScript);
    }

    private void Buscar(string Codigo)
    {
        FactoresEntorno est = new FactoresEntorno();
        est.Codigo = Codigo;

        DataSet ds = new ClinicaCES.Logica.LFactoresEntorno().FactoresEntornoConsultar(est);
        DataTable dt = ds.Tables[0];

        txtCodigo.Enabled = false;
        txtNombre.Enabled = true;
        txtPeso.Enabled = true;

        //txtCriterio.Enabled = true;
        //imgBuscarCriterio.Visible = true;
        //btnAgregar.Visible = true;

        txtDescripcion.Enabled = true;
        btnGuardar.Enabled = true;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";
        btnRetirar.Enabled = false;

        if (Request.QueryString["id"] != null)
            txtCodigo.Text = Request.QueryString["id"];

        if (dt.Rows.Count > 0)
        {
            DataRow row = dt.Rows[0];

            txtNombre.Text = row["NOMBRE"].ToString();
            txtPeso.Text = row["PESO"].ToString();
            txtDescripcion.Text = row["DESCRIPCION"].ToString();
            txtCodigo.Text = row["CODIGO"].ToString();

            bool estado = bool.Parse(row["ESTADO"].ToString());

            btnGuardar.Enabled = estado;
            imgGuardar.Enabled = estado;
            txtNombre.Enabled = estado;
            txtPeso.Enabled = estado;
            txtDescripcion.Enabled = estado;
            //txtCriterio.Enabled = estado;
            //imgBuscarCriterio.Visible = estado;
            //btnAgregar.Visible = estado;
            //pnlGrid.Visible = estado;
            imgGuardar.Enabled = estado;
            btnRetirar.Enabled = estado;
            btnReactivar.Visible = !estado;
            btnRetirar.Visible = estado;
            imgReactivar.Visible = !estado;
            imgRetirar.Visible = estado;
            imgRetirar.Enabled = estado;

            if (!estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }

            //if (ds.Tables[1].Rows.Count > 0)
            //{
            //    Procedimientos.LlenarGrid(ds.Tables[1], gvCriterioEntorno);
            //    ViewState["dtCategorias"] = ds.Tables[1];
            //}
        }

        Procedimientos.Script("SeleccionarText('" + txtNombre.ClientID + "')", litScript);
    }

    private bool ValidaGuardar()
    {
        bool Valido = true;
        double Peso;
        if (string.IsNullOrEmpty(txtCodigo.Text.Trim()))
        {
            txtCodigo.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtCodigo.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            txtNombre.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtNombre.CssClass = "form_input";
        }


        if (string.IsNullOrEmpty(txtDescripcion.Text.Trim()))
        {
            txtDescripcion.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtDescripcion.CssClass = "form_input";
        }

        if (double.TryParse(txtPeso.Text.Trim().Replace('.',','), out Peso))
        {
            txtPeso.CssClass = "form_input";
            txtPeso.Text = Peso.ToString();
        }
        else
        {
            Valido = false;
            txtPeso.CssClass = "invalidtxt";
        }

        return Valido;

    }

    private void Guardar(string Codigo, string Nombre, string Usr, string Descripcion, string Peso)
    {        

        FactoresEntorno est = new FactoresEntorno();
        est.Codigo = Codigo;
        est.Nombre = Nombre;
        est.Descripcion = Descripcion;
        est.usr = Usr;
        //est.xml = xmlCriterio();
        est.Peso = Peso;

        string msg = "3";

        if (new ClinicaCES.Logica.LFactoresEntorno().FactoresEntornoActualizar(est))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addFactoresEntorno.aspx?id=" + Codigo + "')", litScript);
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Buscar(txtCodigo.Text.Trim());
    }

    private void insertarFila(string nombre, string codigo)
    {        
        DataTable dtCategorias = new DataTable();
        if (ViewState["dtCategorias"] != null)
        {
            dtCategorias = (DataTable)ViewState["dtCategorias"];
        }
        else
        {
            dtCategorias.Columns.Add("NOMBRE");
            dtCategorias.Columns.Add("CODIGO");
        }

        DataRow dr;

        dr = dtCategorias.NewRow();
        dr["NOMBRE"] = nombre;
        dr["CODIGO"] = codigo;


        foreach (DataRow row in dtCategorias.Select("CODIGO = '" + codigo + "'"))
        {
            dtCategorias.Rows.Remove(row);
        }

        dtCategorias.Rows.Add(dr);

        ViewState["dtCategorias"] = dtCategorias;

        //gvCriterioEntorno.DataSource = dtCategorias;
        //gvCriterioEntorno.DataBind();


        //txtCriterio.Text = string.Empty;
        //hdnNombre.Value = string.Empty;

        //Procedimientos.Script("selecttext", "SeleccionarText('" + txtcodCategoria.ClientID + "')", this.Page);
    }

    private void EliminarFila(string codigo)
    {
        DataTable dtCategorias = (DataTable)ViewState["dtCategorias"];
        foreach (DataRow row in dtCategorias.Select("CODIGO = '" + codigo + "'"))
        {
            dtCategorias.Rows.Remove(row);
        }

        ViewState["dtCategorias"] = dtCategorias;

        //gvCriterioEntorno.DataSource = dtCategorias;
        //gvCriterioEntorno.DataBind();

    }

    ////private bool ValidarFila()
    ////{
    ////    bool valido = true;
    ////    if (string.IsNullOrEmpty(txtCriterio.Text.Trim()))
    ////    {
    ////        valido = false;
    ////        txtCriterio.CssClass = "invalidtxt";
    ////    }
    ////    else
    ////    {
    ////        txtCriterio.CssClass = "form_input";
    ////        CriterioEntorno est = new CriterioEntorno();
    ////        est.Codigo = txtCriterio.Text.Trim();
    ////        DataTable dt = new ClinicaCES.Logica.LCriterioEntorno().CriterioEntornoConsultar(est);

    ////        if (dt.Rows.Count > 0)
    ////        {
    ////            hdnNombre.Value = dt.Rows[0]["NOMBRE"].ToString();
    ////            txtCriterio.CssClass = "form_input";
    ////        }
    ////        else
    ////        {
    ////            valido = false;
    ////            Procedimientos.Script("Mensaje(21)", litScript);
    ////            txtCriterio.CssClass = "invalidtxt";
    ////        }
    ////    }


    ////    return valido;
    ////}

    //private string xmlCriterio()
    //{
    //    if (gvCriterioEntorno.Rows.Count <= 0)
    //        return null;

    //    string cod;
    //    string xmlCrit = "<Raiz>";

    //    for (int i = 0; i < gvCriterioEntorno.Rows.Count; i++)
    //    {
    //        GridViewRow row = gvCriterioEntorno.Rows[i];
    //        if (row.Visible)
    //        {
    //            cod = row.Cells[1].Text.Trim();
    //            xmlCrit += "<Datos CODIGO='" + cod + "' />";
    //        }            
    //    }

    //    return xmlCrit += "</Raiz>";
    //}
    //protected void gvCriterioEntorno_RowCreated(object sender, GridViewRowEventArgs e)
    //{
    //    if (e.Row.RowType == DataControlRowType.DataRow)
    //    {
    //        ImageButton imgEliminarCriterio = (ImageButton)e.Row.FindControl("imgEliminarCriterio");
    //        imgEliminarCriterio.CommandArgument = e.Row.RowIndex.ToString();
    //    }
    //}


    //protected void gvCriterioEntorno_RowCommand(object sender, GridViewCommandEventArgs e)
    //{
    //    if (e.CommandName == "eliminar")
    //    {
    //        int fila = int.Parse(e.CommandArgument.ToString());
    //        GridViewRow row = gvCriterioEntorno.Rows[fila];
    //        //row.Visible = false;
    //        EliminarFila(row.Cells[1].Text);
    //    }
    //}
}
