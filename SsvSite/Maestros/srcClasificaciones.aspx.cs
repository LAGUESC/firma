﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcClasificaciones : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Clasificaciones", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("CLASIFICACION", "Clasificacion", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void Consultar()
    {
        Clasificaciones clasi = new Clasificaciones();
        DataTable dtClasi = new ClinicaCES.Logica.LClasificaciones().ClasiConsultar(clasi); 
        ViewState["dtClasi"] = dtClasi;
        ViewState["dtPaginas"] = dtClasi;
        Procedimientos.LlenarGrid(dtClasi, gvClasificaciones);
    }

    protected void gvClasificaciones_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 1; i < gvClasificaciones.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addClasificaciones.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
            }

            e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkClasi = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvClasificaciones.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkClasi.Enabled = false;

            chkClasi.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlClasi, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LClasificaciones().ClasisRetirar(xmlClasi, Usuario))
        {
            msg = "7";

            Clasificaciones clasi = new Clasificaciones();
            DataTable dtClasi = new ClinicaCES.Logica.LClasificaciones().ClasiConsultar(clasi);
            ViewState["dtClasi"] = dtClasi;
            Filtrar();
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND CLASIFICACION LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtClasi = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtClasi"]);
        Procedimientos.LlenarGrid(dtClasi, gvClasificaciones);

        ViewState["dtPaginas"] = dtClasi;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("CLASIFICACION", "Clasificacion", pnlNombre, txtBusquedaNombre);
    }

    protected void gvClasificaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvClasificaciones.PageIndex = e.NewPageIndex;
        gvClasificaciones.DataSource = ViewState["dtPaginas"];
        gvClasificaciones.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlClasi(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addClasificaciones.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    private string xmlClasi()
    {
        string clasi;
        string xmlClasi = "<Raiz>";

        for (int i = 0; i < gvClasificaciones.Rows.Count; i++)
        {
            GridViewRow row = gvClasificaciones.Rows[i];
            CheckBox chkClasi = (CheckBox)row.FindControl("chkEliminar");
            if (chkClasi.Checked)
            {
                clasi = row.Cells[1].Text.Trim();
                xmlClasi += "<Datos CODIGO='" + clasi + "' />";
            }
        }

        return xmlClasi += "</Raiz>";
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("CLASIFICACION", "Clasificacion", pnlNombre, txtBusquedaNombre);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "CLASIFICACION":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
