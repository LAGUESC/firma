﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addAppXCli : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Aplicaciones X Cliente", this.Page);
            llenarCombos();
            txtFecha.Enabled = false;
            ddlUsrs.Enabled = false;

            string scritp = 
                "if(document.getElementById('"  + ddlAplicacion.ClientID + "').value != '-1' && document.getElementById('"  + ddlCliente.ClientID + "').value != '-1') { " +
                    "__doPostBack('" + lnkComprobar.UniqueID + "',''); }";

            ddlAplicacion.Attributes.Add("onblur", scritp);
            ddlCliente.Attributes.Add("onblur", scritp);

            if (Request.QueryString["nit"] != null)
            {
                Buscar(Request.QueryString["nit"], Request.QueryString["neg"], Request.QueryString["app"]);
            }
        }
    }

    private void llenarCombos()
    {
        Procedimientos.LlenarCombos(ddlCliente, new ClinicaCES.Logica.LClientes().ClienteConsultar(), "NIT", "NOMCOMCIAL");
        Procedimientos.LlenarCombos(ddlAplicacion, new ClinicaCES.Logica.LMenu().AplicacionListar(), "CODIGO", "NOMBRE");
        Procedimientos.LlenarCombos(ddlUsrs, new ClinicaCES.Logica.LUsuarios().UsuarioConsultar(), "USUARIO", "NOMBRE");
    }

    private void CambiarEstado(string nit, string negocio, string app, string usr, bool? estado)
    { 
        AppXCli axc = new AppXCli();

        axc.Nit = nit;
        axc.Negocio = negocio;
        axc.Aplicacion = app;
        axc.Estado = estado;
        axc.Usr = usr;
        string msg = new ClinicaCES.Logica.LAppXCli().AppXCliCambiarEstado(axc).Rows[0]["MSG"].ToString();

        if (msg != "1")
            Procedimientos.Script("Mensaje(39,'" + msg + "'); redirect('addAppXcli.aspx?nit=" + nit + "&neg=" + negocio + "&app=" + app + "')", litScript);                    
        else
            Procedimientos.Script("Mensaje(" + msg + "); redirect('addAppXcli.aspx?nit=" + nit + "&neg=" + negocio + "&app=" + app + "')", litScript);            


    }

    private void Buscar(string nit,string negocio,string app)
    { 
        AppXCli axc = new AppXCli();

        string cliente = nit + "|" + negocio;

        axc.Nit = nit;
        axc.Negocio = negocio;
        axc.Aplicacion = app;

        ddlAplicacion.SelectedValue = app;
        ddlCliente.SelectedValue = cliente;

        DataTable dtApp = new ClinicaCES.Logica.LAppXCli().AppXCliBuscar(axc);

        btnGuardar.Enabled = true;
        txtFecha.Enabled = true;
        ddlUsrs.Enabled = true;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";
        ddlCliente.Enabled = false;
        ddlAplicacion.Enabled = false;

        if (dtApp.Rows.Count > 0)
        {
            txtFecha.Text = dtApp.Rows[0]["FECHA"].ToString();
            ddlUsrs.SelectedValue = dtApp.Rows[0]["USUARIO"].ToString();

            if (ddlAplicacion.SelectedIndex == 0)
                ddlAplicacion.Items.Add(new ListItem(dtApp.Rows[0]["APLICACION"].ToString(), app));

            if(ddlCliente.SelectedIndex == 0)
                ddlCliente.Items.Add(new ListItem(dtApp.Rows[0]["NOMCOMCIAL"].ToString(), cliente));
                

            bool estado = bool.Parse(dtApp.Rows[0]["ESTADO"].ToString());

            btnGuardar.Enabled = estado;
            imgGuardar.Enabled = estado;
            btnRetirar.Enabled = estado;
            btnReactivar.Visible = !estado;
            btnRetirar.Visible = estado;
            imgReactivar.Visible = !estado;
            imgRetirar.Visible = estado;
            imgRetirar.Enabled = estado;
            imgCalendar.Visible = estado;
            txtFecha.Enabled = estado;
            ddlUsrs.Enabled = estado;

            if (!estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
                //ControlesAbajo(estado);
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }
        }
        
    }

    private void Guardar(string Nit, string Negocio, string Aplicacion, string UsrIns, string Usr, string FecIns)
    {
        DateTime fecha;
        if (!DateTime.TryParse(FecIns, out fecha))
        {
            txtFecha.CssClass = "invalidtxt";
            return;
        }
        else
            txtFecha.CssClass = "format_input";

        AppXCli axc = new AppXCli();

        axc.Nit = Nit;
        axc.Negocio = Negocio;
        axc.Aplicacion = Aplicacion;
        axc.UsrInstala = UsrIns;
        axc.Usr = Usr;
        axc.FecIns = FecIns;

        string msg = "3";

        if (new ClinicaCES.Logica.LAppXCli().AppXCliActualizar(axc))
        {
            msg = "1";            
        }

        Procedimientos.Script("Mensaje(" + msg + "); redirect('addAppXcli.aspx?nit=" + Nit + "&neg=" + Negocio + "&app=" + Aplicacion + "')", litScript);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        string[] cliente = ddlCliente.SelectedValue.Split('|');
        string nit = cliente[0];
        string negocio = cliente[1];
        if (sender.Equals(btnGuardar))
            Guardar(nit, negocio, ddlAplicacion.SelectedValue, ddlUsrs.SelectedValue, Session["Nick1"].ToString(), txtFecha.Text.Trim());
        else if (sender.Equals(btnReactivar))
            CambiarEstado(nit, negocio, ddlAplicacion.SelectedValue, Session["Nick1"].ToString(), true);
        else if (sender.Equals(btnRetirar))
            CambiarEstado(nit, negocio, ddlAplicacion.SelectedValue, Session["Nick1"].ToString(), false);
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addAppXCli.aspx");
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        string[] cliente = ddlCliente.SelectedValue.Split('|');
        string nit = cliente[0];
        string negocio = cliente[1];
        Buscar(nit, negocio, ddlAplicacion.SelectedValue);
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        string[] cliente = ddlCliente.SelectedValue.Split('|');
        string nit = cliente[0];
        string negocio = cliente[1];
        if (sender.Equals(imgGuardar))
            Guardar(nit, negocio, ddlAplicacion.SelectedValue, ddlUsrs.SelectedValue, Session["Nick1"].ToString(), txtFecha.Text.Trim());
        else if (sender.Equals(imgReactivar))
            CambiarEstado(nit, negocio, ddlAplicacion.SelectedValue, Session["Nick1"].ToString(), true);
        else if (sender.Equals(imgRetirar))
            CambiarEstado(nit, negocio, ddlAplicacion.SelectedValue, Session["Nick1"].ToString(), false);
        else if (sender.Equals(imgCancelar) || sender.Equals(imgNuevo))
            Response.Redirect("addAppXCli.aspx");
    }
}
