﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addCompanias : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Compañias", this.Page);
            txtCodigo.Focus();

            if (Request.QueryString["id"] != null)
                Buscar(Request.QueryString["id"]);
            else
                Procedimientos.Script("SeleccionarText('" + txtCodigo.ClientID + "')", litScript);
        }
        litScript.Text = string.Empty;
    }

    private void Buscar(string Codigo)
    {
        Companias cia = new Companias();
        cia.Codigo = Codigo;

        DataTable dt = new ClinicaCES.Logica.LCompanias().CiaConsultar(cia);

        txtCodigo.Enabled = false;
        txtNombre.Enabled = true;
        btnGuardar.Enabled = true;
        btnRetirar.Enabled = false;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";


        if (Request.QueryString["id"] != null)
            txtCodigo.Text = Request.QueryString["id"];

        if (dt.Rows.Count > 0)
        {
            DataRow row = dt.Rows[0];

            txtNombre.Text = row["COMPANIA"].ToString();
            txtCodigo.Text = row["CODIGO"].ToString();

            bool estado = bool.Parse(row["ESTADO"].ToString());

            btnGuardar.Enabled = estado;
            txtNombre.Enabled = estado;
            imgGuardar.Enabled = estado;
            btnRetirar.Enabled = estado;
            btnReactivar.Visible = !estado;
            btnRetirar.Visible = estado;
            imgReactivar.Visible = !estado;
            imgRetirar.Visible = estado;
            imgRetirar.Enabled = estado;

            if (!estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }
        }
        txtNombre.Focus();
        Procedimientos.Script("SeleccionarText('" + txtNombre.ClientID + "')", litScript);
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(imgCancelar) || sender.Equals(imgNuevo))
            Response.Redirect("addCompanias.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true);
    }

    private void Guardar(string Codigo, string Nombre, string Usr)
    {
        if (!ValidaGuardar())
            return;

        Companias cia = new Companias();
        cia.Usuario = Usr;
        cia.Codigo = Codigo;
        cia.Nombre = Nombre;

        string msg = "3";

        if (new ClinicaCES.Logica.LCompanias().CiaGuardar(cia))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addCompanias.aspx?id=" + Codigo + "')", litScript);
    }

    private bool ValidaGuardar()
    {
        bool Valido = true;
        if (string.IsNullOrEmpty(txtCodigo.Text.Trim()))
        {
            txtCodigo.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtCodigo.CssClass = "form_input";
        }

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            txtNombre.CssClass = "invalidtxt";
            Valido = false;
        }
        else
        {
            txtNombre.CssClass = "form_input";
        }

        return Valido;

    }

    private void CambiarEstado(string Codigo, bool? Estado)
    {
        Companias cia = new Companias();
        cia.Codigo = Codigo;
        cia.Estado = Estado;
        string msg = "1";

        if (!new ClinicaCES.Logica.LCompanias().CiaEstado(cia))
            msg = "3";

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addCompanias.aspx?id=" + Codigo + "')", litScript);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addCompanias.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), false);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), true);
    }

}
