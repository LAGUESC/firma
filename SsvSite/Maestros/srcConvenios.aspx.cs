﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcConvenios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Convenios que requieren anexo", this.Page);
            Procedimientos.LlenarCombosBlanco(ddlFiltro, new ClinicaCES.Logica.LMaestros().ListaEmpresas(), "CODEMPRESA", "DESCRIPCION");
            
        }
        litScript.Text = string.Empty;
        
    }
    protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
    {
        consultarConvenios(ddlFiltro.SelectedValue.ToString());
    }
    private void consultarConvenios(string CodEmpresa)
    {
        DataTable dtGrid = llenardt(CodEmpresa);
        if (dtGrid.Rows.Count > 0)
        {
            Panel1.Visible = true;
            Procedimientos.LlenarGrid(dtGrid, gvConvenios);
            ViewState["dtPaginas"] = dtGrid;
            ViewState["dtPaginas2"] = dtGrid;
        }
        else
        {
            Panel1.Visible = false;
        }
    }
    protected DataTable llenardt(string CodEmpresa)
    {
        DataTable dtGrid = new ClinicaCES.Logica.LMaestros().consultarConveniosxEmpresa(CodEmpresa); 
        ViewState["dtPaginas"] = dtGrid;
        ViewState["dtPaginas2"] = dtGrid;
        return dtGrid;
    }
    protected void gvConvenios_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chkEstado = (CheckBox)e.Row.FindControl("chkPages");
            chkEstado.Checked = bool.Parse(gvConvenios.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
        }
    }
    protected void gvConvenios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvConvenios.PageIndex = e.NewPageIndex;
        gvConvenios.DataSource = ViewState["dtPaginas"];
        gvConvenios.DataBind();
    }
    
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        //string msg = "";
        //msg = gvPrestaciones.PageIndex.ToString();
        //Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        string msg = "";
        for (int i = 0; i < gvConvenios.Rows.Count; i++)
        {
            CheckBox check = (CheckBox)gvConvenios.Rows[i].FindControl("chkPages");
            if (new ClinicaCES.Logica.LMaestros().ConveniosActualizar(gvConvenios.Rows[i].Cells[1].Text, check.Checked, Session["Nick1"].ToString()))
            {
                msg = "1";
            }
            else
            {
                msg = "3";
            }
            //}
        }
        if (msg == "1")
        {
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            //gvPrestaciones.PageIndex = gvPrestaciones.PageIndex;
            //gvPrestaciones.DataSource = llenardt();
            //gvPrestaciones.DataBind();
        }
        else
        {
            if (msg == "3")
            { Procedimientos.Script("Mensaje(" + msg + ")", litScript); }
        }
    }
}