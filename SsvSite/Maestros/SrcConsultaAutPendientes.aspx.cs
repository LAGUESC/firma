﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_SrcConsultaAutPendientes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["cookiePendientes"] == null)
            {
                Procedimientos.Titulo("Consulta de Gestión de Autorizaciones", this.Page);
                Procedimientos.LlenarCombos(ddlEstados, new ClinicaCES.Logica.LMaestros().EstadosConsultar(), "CODIGO", "DESCESTADO");
                ConsultarAsigCamas();
            }
            else
            {
                Filtrar();
            }


        }
        litScript.Text = string.Empty;
    }
    private void ConsultarAsigCamas()
    {
        try
        {
            DataView view;
            view = new DataView(ConsultarAsigCamaResponsable(llenardt(), Convert.ToInt32(Session["Perfil1"].ToString()), Session["Nick1"].ToString()));
            DataTable dtGrid = view.ToTable(true, "CAMASERVICIO", "CAMA", "RESPONSABLE", "MTVCORRELATIVO", "PAC_PAC_NUMERO");
            Procedimientos.LlenarGrid(dtGrid, gvAsigCamas);
            ViewState["dtPaginas"] = dtGrid;

        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }

    }
    protected DataTable llenardt()
    {
        try
        {
            DataTable dtGrid = new ClinicaCES.Logica.LMaestros().ConsultarAsigCamaServiciosResVTres(); //tipo 2 son las prestaciones no vigentes en oracle
            //ViewState["dtPaginas2"] = dtGrid;
            //ViewState["dtPaginas2"] = dtGrid;

            return dtGrid;
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            return null;
        }

    }
    protected void gvAsigCamas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                DataView view = new DataView((DataTable)ViewState["dtPaginas"]);
                DataTable dt = view.ToTable(true, "CAMASERVICIO", "CAMA", "RESPONSABLE", "MTVCORRELATIVO", "PAC_PAC_NUMERO");
                DataRow dr = dt.Rows[e.Row.RowIndex];
                string mtvCorrelativo = dr["MTVCORRELATIVO"].ToString();
                string Pac_pac_Numero = dr["PAC_PAC_NUMERO"].ToString();
                GridView gvOrders = e.Row.FindControl("gvOrders") as GridView;
                //DataTable dtGrid = new ClinicaCES.Logica.LMaestros().ConsultarPrestacionesPendientes(mtvCorrelativo, Pac_pac_Numero);
                DataTable dtGrid = Procedimientos.dtFiltrado("PAC_PAC_NUMERO", "PAC_PAC_NUMERO = '" + Pac_pac_Numero + "' AND MTVCORRELATIVO='" + mtvCorrelativo + "'", (DataTable)ViewState["dtPaginas3"]);
                ViewState["dtPaginas2"] = dtGrid;
                Procedimientos.LlenarGrid(dtGrid, gvOrders);

                gvOrders.DataSource = dtGrid;
                gvOrders.DataBind();
            }
            catch
            {
                string msg = "83";
                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            }

        }
    }
    protected void gvOrders_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            try
            {
                GridView gvOrders = (GridView)sender;
                DataTable dt = (DataTable)ViewState["dtPaginas2"];
                DataRow dr = dt.Rows[e.Row.RowIndex];
                DropDownList ddlEstados = e.Row.FindControl("ddlEstado") as DropDownList;
                int tiempo = Convert.ToInt32(dr["VALORALERTA"].ToString());
                if (dr["CODESTADO"].ToString() == "1")
                {
                    Procedimientos.LlenarCombos(ddlEstados, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '1' or CODIGO='2'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                }
                if (dr["CODESTADO"].ToString() == "2")
                {
                    Procedimientos.LlenarCombos(ddlEstados, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '2' or CODIGO='3' or CODIGO='4'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                }
                if (dr["CODESTADO"].ToString() == "3")
                {
                    Procedimientos.LlenarCombos(ddlEstados, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '3'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                }
                if (dr["CODESTADO"].ToString() == "4")
                {
                    Procedimientos.LlenarCombos(ddlEstados, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '4'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                }
                if (dr["CODESTADO"].ToString() == "5")
                {
                    Procedimientos.LlenarCombos(ddlEstados, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '5'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");

                }
                if (dr["CODESTADO"].ToString() == "6")
                {
                    Procedimientos.LlenarCombos(ddlEstados, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '6' OR CODIGO = '2'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");

                }
                ddlEstados.SelectedValue = dr["CODESTADO"].ToString();
                CheckBox chkEstado = (CheckBox)e.Row.FindControl("ckbALerta");
                if (dr["ALERTA"].ToString() != "")
                {
                    if (dr["ALERTA"].ToString() == "1")
                    {
                        chkEstado.Checked = true;
                    }
                }
                TextBox txtTiempo = (TextBox)e.Row.FindControl("txtTiempo");
                if (dr["CODTIEMPOALERTA"].ToString() == "2")
                {
                    tiempo = Convert.ToInt32(dr["VALORALERTA"].ToString()) / 60;
                }
                if (dr["CODTIEMPOALERTA"].ToString() == "3")
                {
                    tiempo = Convert.ToInt32(dr["VALORALERTA"].ToString()) / 1440;
                }
                txtTiempo.Text = tiempo.ToString();
                DropDownList ddlUnidadTiempo = e.Row.FindControl("ddlUnidadTiempo") as DropDownList;
                ddlUnidadTiempo.SelectedValue = dr["CODTIEMPOALERTA"].ToString();
                Image imgSemaforo = e.Row.FindControl("imgSemaforo") as Image;
                if (dr["CODESTADO"].ToString() == "1") { imgSemaforo.ImageUrl = "../img/Rojo.png"; }
                if (dr["CODESTADO"].ToString() == "2") { imgSemaforo.ImageUrl = "../img/Amarillo.png"; }
                if (dr["CODESTADO"].ToString() == "3") { imgSemaforo.ImageUrl = "../img/Verde.png"; }
                if (dr["CODESTADO"].ToString() == "4") { imgSemaforo.ImageUrl = "../img/Naranja.png"; }
                if (dr["CODESTADO"].ToString() == "5") { imgSemaforo.ImageUrl = "../img/Gris.png"; }
                if (dr["CODESTADO"].ToString() == "6") { imgSemaforo.ImageUrl = "../img/Azul.png"; }

                //if (dt.Rows.Count - 1 == e.Row.RowIndex)
                //{
                //    ImageButton imgbtnCrear = e.Row.FindControl("imgbtnCrear") as ImageButton;
                //    imgbtnCrear.Visible = true;

                //}
                //LinkButton Editar = e.Row.FindControl("Editar") as LinkButton;
                if (dr["ORDNOMBRE"].ToString() == "" && dr["ORDNUMERO"].ToString() == "" && dr["PRECOD"].ToString() == "")
                {
                    e.Row.Cells[5].Visible = false;
                    e.Row.Cells[6].Visible = false;
                    e.Row.Cells[17].Visible = false;
                    ddlEstados.Visible = false;
                    chkEstado.Visible = false;
                    txtTiempo.Visible = false;
                    ddlUnidadTiempo.Visible = false;
                    imgSemaforo.Visible = false;
                    //Editar.Visible = false;
                }
                else
                {
                    e.Row.Cells[5].Visible = true;
                    e.Row.Cells[6].Visible = true;
                    e.Row.Cells[17].Visible = true;
                    ddlEstados.Visible = true;
                    chkEstado.Visible = true;
                    txtTiempo.Visible = true;
                    ddlUnidadTiempo.Visible = true;
                    imgSemaforo.Visible = true;
                    //Editar.Visible = true;
                }

                //DataTable dt = (DataTable)ViewState["dtPaginas2"];
                //DataRow dr = dt.Rows[e.Row.RowIndex];
                //if (dr["CODESTADO"].ToString() != "")
                //{
                //    ddlEstados.SelectedValue = dr["CODESTADO"].ToString();
                //}

                //gvOrders.DataSource = dtGrid;
                //gvOrders.DataBind();
            }
            catch
            {
                string msg = "83";
                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            }
        }
    }
    protected void cargarGrid(DataTable dtGrid, GridView gvOrders)
    {
        try
        {
            foreach (GridViewRow row in gvOrders.Rows)
            {
                DropDownList ddlResponsable = row.FindControl("ddlEstados") as DropDownList;
                Procedimientos.LlenarCombos(ddlResponsable, new ClinicaCES.Logica.LMaestros().EstadosConsultar(), "ID", "NOMBRE");

                //if (gvFormu.DataKeys[e.Row.RowIndex]["IDRECIBE"].ToString() != "")
                //{
                //    ddlResponsable.SelectedValue = valor["RESPONSABLE"].ToString();
                //}
            }
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }

    protected void gvOrders_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int tiempo = 0;
            string[] css = { "form_input", "invalidtxt" };
            ddlTiempoEdit.CssClass = css[0];
            txtTiempoEdit.CssClass = css[0];
            GridView gvOrders = (GridView)sender;
            //DataTable dt = new ClinicaCES.Logica.LMaestros().ConsultarPrestacionesPendientes(gvOrders.Rows[e.RowIndex].Cells[5].Text, gvOrders.DataKeys[e.RowIndex]["PAC_PAC_NUMERO"].ToString());
            DataTable dt = Procedimientos.dtFiltrado("PAC_PAC_NUMERO", "PAC_PAC_NUMERO = '" + gvOrders.DataKeys[e.RowIndex]["PAC_PAC_NUMERO"].ToString() + "' AND MTVCORRELATIVO='" + gvOrders.Rows[e.RowIndex].Cells[6].Text + "'", (DataTable)ViewState["dtPaginas3"]);
            DataRow dr = dt.Rows[e.RowIndex];
            txtOrden.Text = dr["ORDNUMERO"].ToString();
            txtResponsableEdit0.Text = dr["RESPONSABLE"].ToString();
            txtCodPrestacionEdit.Text = gvOrders.Rows[e.RowIndex].Cells[3].Text;
            txtPrestacionEdit.Text = textoTilde(gvOrders.Rows[e.RowIndex].Cells[4].Text);
            DataRow drConvenio = new ClinicaCES.Logica.LMaestros().ConsultarConvenioPac(dr["MTVCORRELATIVO"].ToString(), dr["PAC_PAC_NUMERO"].ToString()).Rows[0];
            txtCodConvenioEdit.Text = drConvenio["CODCONVENIO"].ToString();
            txtConvenioEdit.Text = drConvenio["CONVENIO"].ToString();
            if (dr["CODESTADO"].ToString() == "1")
            {
                Procedimientos.LlenarCombos(ddlEstadoEdit, Procedimientos.dtFiltrado("CODIGO", "CODIGO='0' OR CODIGO='2' OR CODIGO='5'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                ddlEstadoEdit.SelectedValue = "-1";
                habilitarControles(dr["CODESTADO"].ToString());
            }
            if (dr["CODESTADO"].ToString() == "2")
            {
                Procedimientos.LlenarCombos(ddlEstadoEdit, Procedimientos.dtFiltrado("CODIGO", "CODIGO='0' OR CODIGO = '2' or CODIGO='3' or CODIGO='4' OR CODIGO='5' OR CODIGO='6'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                ddlEstadoEdit.SelectedValue = "-1";
                habilitarControles(dr["CODESTADO"].ToString());
            }
            if (dr["CODESTADO"].ToString() == "3")
            {
                Procedimientos.LlenarCombos(ddlEstadoEdit, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '3' ", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                ddlEstadoEdit.SelectedValue = dr["CODESTADO"].ToString();
                inhabilitarControles();
            }
            if (dr["CODESTADO"].ToString() == "4")
            {
                Procedimientos.LlenarCombos(ddlEstadoEdit, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '4'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                ddlEstadoEdit.SelectedValue = dr["CODESTADO"].ToString();
                inhabilitarControles();
            }
            if (dr["CODESTADO"].ToString() == "5")
            {
                Procedimientos.LlenarCombos(ddlEstadoEdit, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '5'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                ddlEstadoEdit.SelectedValue = dr["CODESTADO"].ToString();
                inhabilitarControles();
            }
            if (dr["CODESTADO"].ToString() == "6")
            {
                Procedimientos.LlenarCombos(ddlEstadoEdit, Procedimientos.dtFiltrado("CODIGO", "CODIGO = '6' OR CODIGO = '2'", new ClinicaCES.Logica.LMaestros().EstadosConsultar()), "CODIGO", "DESCESTADO");
                ddlEstadoEdit.SelectedValue = dr["CODESTADO"].ToString();
                //inhabilitarControles();
            }
            txtObservacionesEdit.Text = dr["OBSERVACION"].ToString();
            if (dr["ALERTA"].ToString() != "")
            {
                if (dr["ALERTA"].ToString() == "1")
                {

                    chkAlerta.Checked = true;
                }

            }
            else { chkAlerta.Checked = false; }
            if (dr["CODESTADO"].ToString() == "1")
            {
                chkAlerta.Checked = true;
                chkAlerta.Enabled = false;
            }
            if (dr["CODTIEMPOALERTA"].ToString() == "2")
            {
                tiempo = Convert.ToInt32(dr["VALORALERTA"].ToString()) / 60;
            }
            if (dr["CODTIEMPOALERTA"].ToString() == "3")
            {
                tiempo = Convert.ToInt32(dr["VALORALERTA"].ToString()) / 1440;
            }
            txtTiempoEdit.Text = tiempo.ToString();

            ViewState["FECHASOLIC"] = dr["FECHASOLIC"];
            lblPac_pac_numero.Text = dr["PAC_PAC_NUMERO"].ToString() + "|" + dr["MTVCORRELATIVO"].ToString() + "|" + dr["FECHASOLIC"];
            if (dr["CODTIEMPOALERTA"].ToString() == "")
            {
                ddlTiempoEdit.SelectedValue = "0";
            }
            else
            {
                ddlTiempoEdit.SelectedValue = dr["CODTIEMPOALERTA"].ToString();
            }

            ModalPopupExtender1.Show();
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected string textoTilde(string texto)
    {
        try
        {
            string nuevoTexto = "";
            string valor = "";
            for (int i = 0; i < texto.Length; i++)
            {
                valor = texto.Substring(i, 1);
                if (valor == "&")
                {
                    valor = texto.Substring(i, 5);
                    if (valor == "&#225") { valor = "á"; i = i + 5; } else if (valor == "&#193") { valor = "Á"; i = i + 5; }
                    if (valor == "&#233") { valor = "é"; i = i + 5; } else if (valor == "&#201") { valor = "É"; i = i + 5; }
                    if (valor == "&#237") { valor = "í"; i = i + 5; } else if (valor == "&#205") { valor = "Í"; i = i + 5; }
                    if (valor == "&#243") { valor = "ó"; i = i + 5; } else if (valor == "&#211") { valor = "Ó"; i = i + 5; }
                    if (valor == "&#250") { valor = "ú"; i = i + 5; } else if (valor == "&#218") { valor = "Ú"; i = i + 5; }
                    if (valor == "&#241") { valor = "ñ"; i = i + 5; } else if (valor == "&#209") { valor = "Ñ"; i = i + 5; }
                }
                nuevoTexto = nuevoTexto + valor;
            }
            return nuevoTexto;
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            return "";
        }
    }
    protected void inhabilitarControles()
    {
        ddlEstadoEdit.Enabled = false;
        txtObservacionesEdit.Enabled = false;
        chkAlerta.Enabled = false;
        txtTiempoEdit.Enabled = false;
        ddlTiempoEdit.Enabled = false;
        btnGuardar.Enabled = false;

    }
    protected void habilitarControles(string Estado)
    {
        try
        {
            ddlEstadoEdit.Enabled = true;
            txtObservacionesEdit.Enabled = true;
            if (Estado == "2")
            {
                chkAlerta.Enabled = true;
            }
            txtTiempoEdit.Enabled = true;
            ddlTiempoEdit.Enabled = true;
            btnGuardar.Enabled = true;
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected void btnRetornar_Click(object sender, EventArgs e)
    {
        ModalPopupExtender1.Hide();
    }
    protected DataTable ConsultarAsigCamaResponsable(DataTable dtOracle, int Perfil, string Usuario)
    {
        try
        {
            ViewState["dtPaginas3"] = dtOracle;
            //DataTable dtSql = new ClinicaCES.Logica.LMaestros().ConsultarAsigCamaObjetosSql();
            DataTable dtGrid = new DataTable();
            if (dtOracle.Rows.Count > 0)
            {
                dtGrid.Columns.Add(new DataColumn("CAMASERVICIO", System.Type.GetType("System.String")));
                dtGrid.Columns.Add(new DataColumn("CAMA", System.Type.GetType("System.String")));
                dtGrid.Columns.Add(new DataColumn("RESPONSABLE", System.Type.GetType("System.String")));
                dtGrid.Columns.Add(new DataColumn("MTVCORRELATIVO", System.Type.GetType("System.String")));
                dtGrid.Columns.Add(new DataColumn("PAC_PAC_NUMERO", System.Type.GetType("System.String")));
                dtGrid.Columns.Add(new DataColumn("USUARIO", System.Type.GetType("System.String")));
                int cantidad;
                for (int i = 0; i < dtOracle.Rows.Count; i++)
                {

                    DataRow fila = dtGrid.NewRow();
                    cantidad = Convert.ToInt32(Procedimientos.dtFiltrado("CODESTADO", "CAMA = '" + dtOracle.Rows[i]["CAMA"].ToString().Trim() + "' AND (CODESTADO=1 OR CODESTADO=2 OR CODESTADO=6)", dtOracle).Rows.Count.ToString()) - 1;
                    if (dtOracle.Rows[i]["RESPONSABLE"].ToString() != "")
                    {
                        fila["CAMASERVICIO"] = dtOracle.Rows[i]["CAMASERVICIO"] + " - " + dtOracle.Rows[i]["NOMBRES"] + " - " + cantidad + " - " + dtOracle.Rows[i]["CONVENIO"];
                    }
                    else
                    {
                        //Procedimientos.dtFiltrado("CODESTADO", "CAMA = '" + dtOracle.Rows[i]["CAMA"].ToString().Trim() + "' AND (CODESTADO=1 OR CODESTADO=2 OR CODESTADO=6)", dtOracle).Rows.Count.ToString()- 1
                        fila["CAMASERVICIO"] = dtOracle.Rows[i]["CAMASERVICIO"] + " - " + cantidad + " - " + dtOracle.Rows[i]["CONVENIO"];
                    }


                    fila["CAMA"] = dtOracle.Rows[i]["CAMA"].ToString();
                    fila["RESPONSABLE"] = dtOracle.Rows[i]["NOMBRES"];
                    fila["MTVCORRELATIVO"] = dtOracle.Rows[i]["MTVCORRELATIVO"].ToString();
                    fila["PAC_PAC_NUMERO"] = dtOracle.Rows[i]["PAC_PAC_NUMERO"].ToString();
                    fila["USUARIO"] = dtOracle.Rows[i]["RESPONSABLE"];
                    dtGrid.Rows.Add(fila);
                }
            }

            if (Perfil == 8)
            {
                dtGrid = Procedimientos.dtFiltrado("SER_SER_CODIGO", "SER_SER_CODIGO LIKE '*" + Usuario + "*'", dtOracle);
            }
            ViewState["dtPaginas"] = dtGrid;
            ViewState["dtPaginas2"] = dtGrid;
            return dtGrid;
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            return null;
        }

    }

    protected DataTable ConsultarPendientesxCama(string customerId)
    {
        DataTable dtOracle = new ClinicaCES.Logica.LMaestros().ConsultarAsigCamaObjetos(customerId);

        return dtOracle;
    }


    protected void gvAsigCamas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvAsigCamas.PageIndex = e.NewPageIndex;
        gvAsigCamas.DataSource = ViewState["dtPaginas"];
        gvAsigCamas.DataBind();
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {

        try
        {

            string msg = "";
            if (validar())
            {
                //DateTime fechaSolic = new DateTime();

                string fechaSolic = "";
                string valorFecha = "";
                int tiempo = Convert.ToInt32(txtTiempoEdit.Text);
                string[] paciente_correlativo_Fecsoli = lblPac_pac_numero.Text.Split('|');
                int alerta = 0;
                if (chkAlerta.Checked == true) { alerta = 1; }
                string[] fecha = paciente_correlativo_Fecsoli[2].ToString().Split(' ');
                string val = paciente_correlativo_Fecsoli[2].ToString().Substring(19, 4);

                if (val.Trim() == "a.m." || val.Trim() == "a.m" || val.Trim() == "a. m")
                {
                    fechaSolic = fecha[0].ToString() + " " + fecha[1].ToString() + " A.M.";
                    valorFecha = "AM";
                }
                if (val.Trim() == "p.m." || val.Trim() == "p.m" || val.Trim() == "p. m")
                {
                    fechaSolic = fecha[0].ToString() + " " + fecha[1].ToString() + " P.M.";
                    valorFecha = "PM";
                }
                if (ddlTiempoEdit.SelectedValue == "2")
                {
                    tiempo = Convert.ToInt32(txtTiempoEdit.Text) * 60;
                }
                if (ddlTiempoEdit.SelectedValue == "3")
                {
                    tiempo = Convert.ToInt32(txtTiempoEdit.Text) * 1440;

                }
                DateTime fechaConvertir = Convert.ToDateTime(paciente_correlativo_Fecsoli[2].ToString());

                if ((new ClinicaCES.Logica.LMaestros().gestionAutActualizar(paciente_correlativo_Fecsoli[0].ToString().Trim(), paciente_correlativo_Fecsoli[1].ToString().Trim(), txtOrden.Text, txtCodPrestacionEdit.Text, txtCodConvenioEdit.Text, txtResponsableEdit0.Text, ddlEstadoEdit.SelectedValue.ToString(), txtObservacionesEdit.Text, alerta, tiempo, ddlTiempoEdit.SelectedValue.ToString(), fecha[1].ToString(), valorFecha, fechaConvertir, Session["Nick1"].ToString())))
                {
                    msg = "1";
                }
                else
                {
                    msg = "3";
                }

                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                ConsultarAsigCamas();
            }
            else
            {
                msg = "82";
                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                ModalPopupExtender1.Show();
            }
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected bool validar()
    {
        bool Valido = true;

        System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
        string[] css = { "form_input", "invalidtxt" };


        if (string.IsNullOrEmpty(ddlEstadoEdit.SelectedValue.ToString().Trim()))
        {
            Valido = false;

            ddlEstadoEdit.CssClass = css[1];
        }
        else
        {
            ddlEstadoEdit.CssClass = css[0];
            if (ddlEstadoEdit.SelectedValue.ToString().Trim() == "-1")
            {
                Valido = false;
                ddlEstadoEdit.CssClass = css[1];
            }
            else
            {
                ddlEstadoEdit.CssClass = css[0];
            }
        }
        if (ddlEstadoEdit.SelectedValue.ToString() != "5")
        {
            if (chkAlerta.Checked == true)
            {
                if (string.IsNullOrEmpty(ddlTiempoEdit.SelectedValue.ToString().Trim()) || ddlTiempoEdit.SelectedValue.ToString().Trim() == "0")
                {
                    Valido = false;

                    ddlTiempoEdit.CssClass = css[1];
                }
                else
                {
                    ddlTiempoEdit.CssClass = css[0];
                }
                if (string.IsNullOrEmpty(txtTiempoEdit.Text.Trim()) || txtTiempoEdit.Text.Trim() == "0")
                {
                    Valido = false;
                    txtTiempoEdit.CssClass = css[1];
                }
                else
                {
                    txtTiempoEdit.CssClass = css[0];

                }
            }
        }
        return Valido;
    }
    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        try
        {
            imgBuscar.Enabled = true;
            imgBuscar.Visible = true;
            if (sender.Equals(lnkAgregar))
                AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
            else if (sender.Equals(lnkKitarProc))
                kitarFiltro("1", "Procedimiento", pnlProc, txtBusquedaProc);
            else if (sender.Equals(lnkKitarPaciente))
                kitarFiltro("2", "Paciente", pnlPaciente, txtBusquedaPaciente);
            else if (sender.Equals(lnkKitarEstados))
                kitarFiltroEstado("3", "Estado", pnlEstados, ddlEstados);
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    private void Filtrar()
    {
        try
        {
            DataView view = null;
            if (Request.QueryString["cookiePendientes"] == null)
            {

                if (ddlFlitro.SelectedValue == "1")
                {
                    view = new DataView(ConsultarAsigCamaResponsable(Procedimientos.dtFiltrado("PRENOM", "PRENOM LIKE '*" + txtBusquedaProc.Text.Trim() + "*'", (DataTable)ViewState["dtPaginas3"]), Convert.ToInt32(Session["Perfil1"].ToString()), Session["Nick1"].ToString()));
                }
                if (ddlFlitro.SelectedValue == "2")
                {
                    view = new DataView(ConsultarAsigCamaResponsable(Procedimientos.dtFiltrado("ID", "ID LIKE '*" + txtBusquedaPaciente.Text.Trim() + "*'", (DataTable)ViewState["dtPaginas3"]), Convert.ToInt32(Session["Perfil1"].ToString()), Session["Nick1"].ToString()));
                }
                if (ddlFlitro.SelectedValue == "3")
                {
                    view = new DataView(ConsultarAsigCamaResponsable(Procedimientos.dtFiltrado("CODESTADO", "CODESTADO = " + ddlEstados.SelectedValue.ToString() + " ", (DataTable)ViewState["dtPaginas3"]), Convert.ToInt32(Session["Perfil1"].ToString()), Session["Nick1"].ToString()));
                }
            }
            else
            {
                view = new DataView(ConsultarAsigCamaResponsable(Procedimientos.dtFiltrado("ID", "ID = '" + Procedimientos.descifrar(Request.QueryString["cookiePendientes"]).ToString().Trim() + "'", llenardt()), Convert.ToInt32(Session["Perfil1"].ToString()), Session["Nick1"].ToString()));
            }
            DataTable dtGrid = view.ToTable(true, "CAMASERVICIO", "CAMA", "RESPONSABLE", "MTVCORRELATIVO", "PAC_PAC_NUMERO");
            Procedimientos.LlenarGrid(dtGrid, gvAsigCamas);
            ViewState["dtPaginas"] = dtGrid;
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "1":
                {
                    //ddlFlitro.Items.RemoveAt(index);
                    pnlProc.Visible = true;
                    break;
                }
            case "2":
                {
                    //ddlFlitro.Items.RemoveAt(index);
                    pnlPaciente.Visible = true;
                    break;
                }
            case "3":
                {
                    //ddlFlitro.Items.RemoveAt(index);
                    pnlEstados.Visible = true;
                    break;
                }
            default:
                {
                    //ddlFlitro.Items.RemoveAt(index);
                    //pnlRESPONSABLE.Visible = true;
                    break;
                }
        }
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        //ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
        imgBuscar.Enabled = false;
        imgBuscar.Visible = false;
        ConsultarAsigCamas();
    }
    private void kitarFiltroEstado(string value, string text, Panel pnl, DropDownList ddl)
    {
        //ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        ddl.SelectedValue = "-1";
        imgBuscar.Enabled = false;
        imgBuscar.Visible = false;
        ConsultarAsigCamas();
    }
    protected void imgBuscar_Click(object sender, ImageClickEventArgs e)
    {
        Filtrar();
    }


    protected void gvOrders_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            GridView gvOrders = (GridView)sender;
            llenarInfo(gvOrders, e.RowIndex);
            ModalPopupExtender2.Show();
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }

    }
    protected void llenarInfo(GridView gvOrders, int Index)
    {
        try
        {
            Procedimientos.LlenarCombos(ddlEspecialidad, new ClinicaCES.Logica.LMaestros().ConsultarEspecialidades(), "ORDTIPO", "ORDNOMBRE");
            txtPac.Text = gvOrders.Rows[Index].Cells[5].Text;
            txtEvento.Text = gvOrders.Rows[Index].Cells[6].Text;
            lblPac_pac_numero.Text = gvOrders.DataKeys[Index]["PAC_PAC_NUMERO"].ToString();
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }

    protected void ddlEspecialidad_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Procedimientos.LlenarCombos(ddlPrestacion, new ClinicaCES.Logica.LMaestros().ConsultarPrestaciones(ddlEspecialidad.SelectedValue.ToString()), "CODIGO", "PRESTACION");
            ModalPopupExtender2.Show();
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected void GuardarPrestacion_Click(object sender, EventArgs e)
    {
        try
        {
            string msg = "";
            if (validarPrestacion())
            {
                if (new ClinicaCES.Logica.LMaestros().prestacionAutActualizar(ddlEspecialidad.SelectedItem.ToString().ToUpper(), "NO MEDICA", ddlPrestacion.SelectedValue.ToString().ToUpper(), "SYSDATE", ddlEspecialidad.SelectedValue.ToString().ToUpper(), txtPac.Text.ToUpper(), lblPac_pac_numero.Text.ToUpper(), txtEvento.Text.ToUpper()))
                {
                    msg = "1";
                }
                else
                {
                    msg = "3";
                }

                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                LimpiarControles();
                ConsultarAsigCamas();
            }
            else
            {
                msg = "82";
                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                ModalPopupExtender2.Show();
            }
        }
        catch
        {
            string msg = "83";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected void LimpiarControles()
    {
        ddlPrestacion.SelectedValue = "-1";
    }

    protected bool validarPrestacion()
    {
        bool Valido = true;

        System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
        string[] css = { "form_input", "invalidtxt" };


        if (string.IsNullOrEmpty(ddlEspecialidad.SelectedValue.ToString().Trim()))
        {
            Valido = false;

            ddlEspecialidad.CssClass = css[1];
        }
        else
        {
            ddlEspecialidad.CssClass = css[0];
            if (ddlEspecialidad.SelectedValue.ToString().Trim() == "-1")
            {
                Valido = false;
                ddlEspecialidad.CssClass = css[1];
            }
            else
            {
                ddlEspecialidad.CssClass = css[0];
            }
            if (string.IsNullOrEmpty(ddlPrestacion.SelectedValue.ToString().Trim()))
            {
                Valido = false;

                ddlPrestacion.CssClass = css[1];
            }
            else
            {
                if (ddlPrestacion.SelectedValue.ToString().Trim() == "-1")
                {
                    Valido = false;
                    ddlPrestacion.CssClass = css[1];
                }
                else
                {
                    ddlPrestacion.CssClass = css[0];
                }
            }
        }


        return Valido;
    }

}