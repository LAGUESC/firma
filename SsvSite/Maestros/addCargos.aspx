﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addCargos.aspx.cs" Inherits="Maestros_addCargos" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"></asp:ScriptManager>
<table width="100%">
    <tr>
        <td>
            <table align="left">
                <tr>
                    <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" 
                            ImageUrl="../icons/nuevo.png" onclick="Images_Click" TabIndex="1" /></td>
                    <td><asp:ImageButton runat="server" ID="imgRetirar"  ToolTip="Retirar" 
                            ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" 
                            OnClientClick="return Mensaje(11)" TabIndex="2" /></td>
                    <td><asp:ImageButton runat="server" Visible="false" ID="imgReactivar" 
                            ToolTip="Reactivar" ImageUrl="../icons/refresh.png" onclick="Images_Click" 
                            TabIndex="3"  /></td>                
                    <td><asp:ImageButton runat="server" ID="imgGuardar" ToolTip="Guardar" 
                            OnClientClick="return Mensaje(13)" ImageUrl="../icons/16_save_d.png" 
                            onclick="Images_Click" TabIndex="4" /></td>                
                    <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" 
                            ImageUrl="../icons/cancel.png" onclick="Images_Click" TabIndex="5" /></td>
                    <td><img onclick="redirect('srcCargo.aspx')" src="../icons/magnify.png" id="imgBuscar" runat="server" style="cursor:pointer" /></td>
                </tr>     
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>
                    <td>Area:</td>
                    <td><asp:DropDownList ID="ddlArea" runat="server" TabIndex="6"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td>Codigo:</td>                        
                    <td><asp:TextBox onkeypress="tabular(event,this); event.returnValue=LetrasyNum(event)" onkeydown="tabular(event,this)"  CssClass="form_input" 
                            ID="txtCodigo" runat="server" Width="77px" MaxLength="10" 
                            TabIndex="7"></asp:TextBox>
                    <asp:LinkButton ID="lnkComprobar" runat="server" onclick="lnkComprobar_Click"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>Nombre:</td>
                    <td><asp:TextBox onkeypress="tabular(event,this); event.returnValue=LetrasyNum(event)" Enabled="false" CssClass="form_input" ID="txtNombre" 
                            runat="server" MaxLength="50" TabIndex="8" Width="209px"></asp:TextBox></td>                    
                </tr>
                <tr>
                    <td>Valor Hora:</td>
                    <td><asp:TextBox onkeypress="tabular(event,this); event.returnValue=SoloNumeros(event)" Enabled="false" CssClass="form_input" ID="txtVlrHora" 
                            runat="server" Width="50px" MaxLength="10" TabIndex="9"></asp:TextBox></td>                    
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" TabIndex="10"   /></td>
                    <td><asp:Button Visible="false" ID="btnReactivar" runat="server" Text="Reactivar" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" TabIndex="11"   /></td>
                    <td><asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" 
                            OnClientClick="return Mensaje(11)" onmouseover="this.className='btnhov'" 
                            onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" 
                            TabIndex="12"  /></td>
                    <td><asp:Button ID="btnGuardar" Enabled="false" runat="server" Text="Guardar" 
                            OnClientClick="return Mensaje(13)" onmouseover="this.className='btnhov'" 
                            onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" 
                            TabIndex="13" /></td>                                                                                
                    <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" TabIndex="14"  /></td>                     
                    <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcCargo.aspx')" /> </td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<br />
</asp:Content>

