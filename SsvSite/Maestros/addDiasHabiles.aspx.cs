﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;

public partial class Maestros_addDiasHabiles : System.Web.UI.Page
{
    DataTable dtFechaNoLab = null;
    bool ControlFecha = true;

    protected void Page_Load(object sender, EventArgs e)
    {
        //clsSession.ValidarSession(this.Page);
    }

    protected void Calendar1_DayRender(object sender, DayRenderEventArgs e)
    {
        if (!e.Day.IsOtherMonth)
        {
            if (e.Day.Date.DayOfWeek.ToString() == "Sunday")
            {
                e.Cell.Text = "<input type=checkbox value=" + e.Day.Date.ToShortDateString() + " checked disabled />" + e.Day.Date.Day;
                hdnMes.Value = e.Day.Date.Month.ToString();
                e.Cell.BorderStyle = BorderStyle.Solid;
                e.Cell.BorderWidth = new Unit(1);
                e.Cell.BorderColor = System.Drawing.Color.Black;
                //Script("alertin", "alert('" + hdnMes.Value + "')");
            }
            else
            {
                e.Cell.Text = "<input type=checkbox value=" + e.Day.Date.ToShortDateString() + " />" + e.Day.Date.Day;
            }
            if (ControlFecha)
            {
                int Mes = e.Day.Date.Month;
                int Year = e.Day.Date.Year;
                dtFechaNoLab = new ClinicaCES.Logica.LAsunto().FestivosConsultar(Mes, Year);
                ControlFecha = false;
            }
            for (int x = 0; x < dtFechaNoLab.Rows.Count; x++)
            {
                if (dtFechaNoLab.Rows[x]["FECHA"].ToString() == e.Day.Date.ToShortDateString() && e.Day.Date.DayOfWeek.ToString() != "Sunday")
                {
                    e.Cell.BorderStyle = BorderStyle.Solid;
                    e.Cell.BorderWidth = new Unit(1);
                    e.Cell.BorderColor = System.Drawing.Color.Black;
                    e.Cell.Text = "<input type=checkbox value=" + e.Day.Date.ToShortDateString() + " checked />" + e.Day.Date.Day;
                    break;
                }
            }

        }
        else
        {
            e.Cell.Text = e.Day.Date.Day.ToString();
            ControlFecha = true;
        }
    }

    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        Guardar();
    }

    private void Guardar()
    {
        string Xml = hdnXML.Value;
        string msg = "3";
            if (new ClinicaCES.Logica.LAsunto().FestivosActualizar(Xml))
            {
                //OPERACION EXITOSA
                //Procedimientos.Script("alertin", "Mensaje(1)",this.Page);
                msg = "1";
            }
            Procedimientos.Script("alertin", "Mensaje(" + msg + ")", this.Page);
           
    }
}
