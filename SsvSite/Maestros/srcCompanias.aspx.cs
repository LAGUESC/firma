﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcCompanias : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Compañias", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("COMPANIA", "Nombre", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void Consultar()
    {
        Companias cia = new Companias();
        DataTable dtCias = new ClinicaCES.Logica.LCompanias().CiaConsultar(cia);
        ViewState["dtCias"] = dtCias;
        ViewState["dtPaginas"] = dtCias;
        Procedimientos.LlenarGrid(dtCias, gvCompanias);
    }

    protected void gvCompanias_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvCompanias.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addCompanias.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
            }

            e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvCompanias.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlCia, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LCompanias().CiasRetirar(xmlCia, Usuario))
        {
            msg = "7";

            Companias cia = new Companias();
            DataTable dtCias = new ClinicaCES.Logica.LCompanias().CiaConsultar(cia);
            ViewState["dtCias"] = dtCias;
            Filtrar();
        }
        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND COMPANIA LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtCias = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtCias"]);
        Procedimientos.LlenarGrid(dtCias, gvCompanias);

        ViewState["dtPaginas"] = dtCias;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("COMPANIA", "Nombre", pnlNombre, txtBusquedaNombre);
    }

    private string xmlCia()
    {
        string cia;
        string xmlCia = "<Raiz>";

        for (int i = 0; i < gvCompanias.Rows.Count; i++)
        {
            GridViewRow row = gvCompanias.Rows[i];
            CheckBox chkCia = (CheckBox)row.FindControl("chkEliminar");
            if (chkCia.Checked)
            {
                cia = row.Cells[1].Text.Trim();
                xmlCia += "<Datos COMPANIA='" + cia + "' />";
            }
        }

        return xmlCia += "</Raiz>";
    }

    protected void gvCompanias_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCompanias.PageIndex = e.NewPageIndex;
        gvCompanias.DataSource = ViewState["dtPaginas"];
        gvCompanias.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlCia(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addCompanias.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("COMPANIA", "Nombre", pnlNombre, txtBusquedaNombre);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "COMPANIA":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
