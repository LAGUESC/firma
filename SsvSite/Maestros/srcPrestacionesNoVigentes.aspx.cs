﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcPrestacionesNoVigentes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Prestaciones No vigentes con autorización", this.Page);
            ConsultarPrestaciones();
        }
        litScript.Text = string.Empty;
    }
    protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
    {
        AgregarFiltro(ddlFiltro.SelectedValue, ddlFiltro.SelectedIndex);
    }
    private void ConsultarPrestaciones()
    {
        string msg = "3";
        DataTable dtGrid = llenardt();
        if (dtGrid.Rows.Count > 0)
        {
            Procedimientos.LlenarGrid(dtGrid, gvPrestaciones);
            ViewState["dtPaginas"] = dtGrid;
            ViewState["dtPaginas2"] = dtGrid;
        }
        else
        {
            
            ddlFiltro.Enabled = false;
            btnGuardar.Enabled = false;
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected DataTable llenardt()
    {
        DataTable dtGrid = new ClinicaCES.Logica.LMaestros().ConsultarPrestacionesOracle(2); //tipo 2 son las prestaciones no vigentes en oracle
        ViewState["dtPaginas"] = dtGrid;
        ViewState["dtPaginas2"] = dtGrid;
        return dtGrid;
    }
    protected void gvPrestaciones_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            CheckBox chkEstado = (CheckBox)e.Row.FindControl("chkPages");
            //chkEstado.Checked = bool.Parse(gvPrestaciones.DataKeys[e.Row.RowIndex]["ESTADO_AUT"].ToString());
            chkEstado.Checked = bool.Parse(gvPrestaciones.DataKeys[e.Row.RowIndex]["ESTADO_AUT"].ToString());
        }
    }
    protected void gvPrestaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPrestaciones.PageIndex = e.NewPageIndex;
        gvPrestaciones.DataSource = ViewState["dtPaginas"];
        gvPrestaciones.DataBind();
    }
    protected void txtFiltro_TextChanged(object sender, EventArgs e)
    {
        DataTable dtNew = Procedimientos.dtFiltrado("DESCRIPCION", "CODPRESTACION LIKE '*" + txtFiltro.Text.Trim() + "*' OR DESCRIPCION LIKE '*" + txtFiltro.Text.Trim() + "*'", (DataTable)ViewState["dtPaginas2"]);
        Procedimientos.LlenarGrid(dtNew, gvPrestaciones);
    }
    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "1":
                {
                    pnlBusqueda.Visible = true;
                    break;
                }
            case "2":
                {
                    pnlBusqueda.Visible = false;
                    txtFiltro.Text = "";
                    DataTable dtNew = Procedimientos.dtFiltrado("ESTADO_AUT", "ESTADO_AUT LIKE '*true*'", (DataTable)ViewState["dtPaginas2"]);
                    Procedimientos.LlenarGrid(dtNew, gvPrestaciones);
                    ViewState["dtPaginas"] = dtNew;
                    break;
                }
            case "3":
                {
                    pnlBusqueda.Visible = false;
                    txtFiltro.Text = "";
                    DataTable dtNew = Procedimientos.dtFiltrado("ESTADO_AUT", "ESTADO_AUT LIKE '*false*'", (DataTable)ViewState["dtPaginas2"]);
                    Procedimientos.LlenarGrid(dtNew, gvPrestaciones);
                    ViewState["dtPaginas"] = dtNew;
                    break;
                }
        }
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        //string msg = "";
        //msg = gvPrestaciones.PageIndex.ToString();
        //Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        Prestaciones prestacion = new Prestaciones();
        string msg = "";
        for (int i = 0; i < gvPrestaciones.Rows.Count; i++)
        {
            CheckBox check = (CheckBox)gvPrestaciones.Rows[i].FindControl("chkPages");
            //if (check.Checked == true)
            //{
            prestacion.Estado = check.Checked;
            prestacion.CodPrestacion = gvPrestaciones.Rows[i].Cells[1].Text;
            prestacion.Usuario = Session["Nick1"].ToString();
            if (new ClinicaCES.Logica.LMaestros().PrestacionesActualizar(prestacion.CodPrestacion, prestacion.Estado, prestacion.Usuario))
            {
                msg = "1";
            }
            else
            {
                msg = "3";
            }
            //}
        }
        if (msg == "1")
        {
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            gvPrestaciones.PageIndex = gvPrestaciones.PageIndex;
            gvPrestaciones.DataSource = llenardt();
            gvPrestaciones.DataBind();
        }
        else
        {
            if (msg == "3")
            { Procedimientos.Script("Mensaje(" + msg + ")", litScript); }
        }
    }
}


//using System;
//using System.Collections;
//using System.Configuration;
//using System.Data;
//using System.Linq;
//using System.Web;
//using System.Web.Security;
//using System.Web.UI;
//using System.Web.UI.HtmlControls;
//using System.Web.UI.WebControls;
//using System.Web.UI.WebControls.WebParts;
//using System.Xml.Linq;
//using ClinicaCES.Entidades;

//public partial class Maestros_srcPrestacionesNoVigentes : System.Web.UI.Page
//{
//    protected void Page_Load(object sender, EventArgs e)
//    {
//        if (!IsPostBack)
//        {
//            Procedimientos.Titulo("Prestaciones No vigentes con autorización", this.Page);
//            ConsultarPrestaciones();
//        }
//        litScript.Text = string.Empty;
//    }
//    protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
//    {
//        AgregarFiltro(ddlFiltro.SelectedValue, ddlFiltro.SelectedIndex);
//    }
//    private void ConsultarPrestaciones()
//    {
//        string msg = "3";
//        DataTable dtGrid = llenardt();
//        if (dtGrid.Rows.Count > 0)
//        {
//            Procedimientos.LlenarGrid(dtGrid, gvPrestaciones);
//            ViewState["dtPaginas"] = dtGrid;
//            ViewState["dtPaginas2"] = dtGrid;
//        }
//        else
//        {
            
//            ddlFiltro.Enabled = false;
//            btnGuardar.Enabled = false;
//            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
//        }
//    }
//    protected DataTable llenardt()
//    {
//        DataTable dtOracle = new ClinicaCES.Logica.LMaestros().ConsultarPrestacionesOracle(2); //tipo 2 son las prestaciones no vigentes en oracle
//        DataTable dtSql = new ClinicaCES.Logica.LMaestros().ConsultarPrestacionesSql(2);
//        DataTable dtGrid = new DataTable(); ;
     
//        if (dtSql.Rows.Count > 0)
//        {
//            ddlFiltro.Enabled = true;
//            btnGuardar.Enabled = true;
    
//            dtGrid.Columns.Add(new DataColumn("ESTADO_AUT", System.Type.GetType("System.String")));
//            dtGrid.Columns.Add(new DataColumn("CODPRESTACION", System.Type.GetType("System.String")));
//            dtGrid.Columns.Add(new DataColumn("DESCRIPCION", System.Type.GetType("System.String")));
//            for (int i = 0; i < dtOracle.Rows.Count; i++)
//            {
//                DataRow fila = dtGrid.NewRow();
//                foreach (DataRow fila2 in dtSql.Rows)
//                {
//                    if (dtOracle.Rows[i]["CODPRESTACION"].ToString() == fila2[0].ToString())
//                    {
//                        fila["ESTADO_AUT"] = fila2[1].ToString();
//                        fila["CODPRESTACION"] = dtOracle.Rows[i]["CODPRESTACION"];
//                        fila["DESCRIPCION"] = dtOracle.Rows[i]["DESCRIPCION"];
//                        dtGrid.Rows.Add(fila);
//                        break;
//                    }
//                    //else
//                    //{
//                    //    fila["ESTADO_AUT"] = dtOracle.Rows[i]["ESTADO_AUT"];
//                    //    fila["CODPRESTACION"] = dtOracle.Rows[i]["CODPRESTACION"];
//                    //    fila["DESCRIPCION"] = dtOracle.Rows[i]["DESCRIPCION"];
//                    //}
//                }
                
//            }
            
            
//        }
       
//        ViewState["dtPaginas"] = dtGrid;
//        ViewState["dtPaginas2"] = dtGrid;
//        return dtGrid;

//    }
//    protected void gvPrestaciones_RowDataBound(object sender, GridViewRowEventArgs e)
//    {
//        if (e.Row.RowType == DataControlRowType.DataRow)
//        {
//            CheckBox chkEstado = (CheckBox)e.Row.FindControl("chkPages");
//            chkEstado.Checked = bool.Parse(gvPrestaciones.DataKeys[e.Row.RowIndex]["ESTADO_AUT"].ToString());
//        }
//    }
//    protected void gvPrestaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
//    {
//        gvPrestaciones.PageIndex = e.NewPageIndex;
//        gvPrestaciones.DataSource = ViewState["dtPaginas"];
//        gvPrestaciones.DataBind();
//    }
//    protected void txtFiltro_TextChanged(object sender, EventArgs e)
//    {
//        DataTable dtNew = Procedimientos.dtFiltrado("DESCRIPCION", "CODPRESTACION LIKE '*" + txtFiltro.Text.Trim() + "*' OR DESCRIPCION LIKE '*" + txtFiltro.Text.Trim() + "*'", (DataTable)ViewState["dtPaginas2"]);
//        Procedimientos.LlenarGrid(dtNew, gvPrestaciones);
//    }
//    private void AgregarFiltro(string Filtro, int index)
//    {
//        switch (Filtro)
//        {
//            case "1":
//                {
//                    pnlBusqueda.Visible = true;
//                    break;
//                }
//            case "2":
//                {
//                    pnlBusqueda.Visible = false;
//                    txtFiltro.Text = "";
//                    DataTable dtNew = Procedimientos.dtFiltrado("ESTADO_AUT", "ESTADO_AUT LIKE '*true*'", (DataTable)ViewState["dtPaginas2"]);
//                    Procedimientos.LlenarGrid(dtNew, gvPrestaciones);
//                    ViewState["dtPaginas"] = dtNew;
//                    break;
//                }
//            case "3":
//                {
//                    pnlBusqueda.Visible = false;
//                    txtFiltro.Text = "";
//                    DataTable dtNew = Procedimientos.dtFiltrado("ESTADO_AUT", "ESTADO_AUT LIKE '*false*'", (DataTable)ViewState["dtPaginas2"]);
//                    Procedimientos.LlenarGrid(dtNew, gvPrestaciones);
//                    ViewState["dtPaginas"] = dtNew;
//                    break;
//                }
//        }
//    }
//    protected void btnGuardar_Click(object sender, EventArgs e)
//    {
//        //string msg = "";
//        //msg = gvPrestaciones.PageIndex.ToString();
//        //Procedimientos.Script("Mensaje(" + msg + ")", litScript);
//        Prestaciones prestacion = new Prestaciones();
//        string msg = "";
//        for (int i = 0; i < gvPrestaciones.Rows.Count; i++)
//        {
//            CheckBox check = (CheckBox)gvPrestaciones.Rows[i].FindControl("chkPages");
//            //if (check.Checked == true)
//            //{
//            prestacion.Estado = check.Checked;
//            prestacion.CodPrestacion = gvPrestaciones.Rows[i].Cells[1].Text;
//            prestacion.Usuario = Session["Nick"].ToString();
//            if (new ClinicaCES.Logica.LMaestros().PrestacionesActualizar(prestacion.CodPrestacion, prestacion.Estado, prestacion.Usuario))
//            {
//                msg = "1";
//            }
//            else
//            {
//                msg = "3";
//            }
//            //}
//        }
//        if (msg == "1")
//        {
//            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
//            gvPrestaciones.PageIndex = gvPrestaciones.PageIndex;
//            gvPrestaciones.DataSource = llenardt();
//            gvPrestaciones.DataBind();
//        }
//        else
//        {
//            if (msg == "3")
//            { Procedimientos.Script("Mensaje(" + msg + ")", litScript); }
//        }
//    }
//}