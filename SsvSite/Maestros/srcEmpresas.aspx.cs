﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;
public partial class Maestros_srcEmpresas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Correos Empresas ", this.Page);
            ConsultarConvenios();
        }
        litScript.Text = string.Empty;
    }
    protected void ddlFiltro_SelectedIndexChanged(object sender, EventArgs e)
    {
        AgregarFiltro(ddlFiltro.SelectedValue, ddlFiltro.SelectedIndex);
    }
    private void ConsultarConvenios()
    {
        DataTable dtGrid = llenardt();
        Procedimientos.LlenarGrid(dtGrid, gvEmpresas);
        ViewState["dtPaginas"] = dtGrid;
        ViewState["dtPaginas2"] = dtGrid;
    }
    protected DataTable llenardt()
    {
        DataTable dtGrid = new ClinicaCES.Logica.LMaestros().ConsultarEmpresasOracle(); //tipo 2 son las prestaciones no vigentes en oracle
        ViewState["dtPaginas"] = dtGrid;
        ViewState["dtPaginas2"] = dtGrid;
        return dtGrid;
    }
    protected void gvEmpresas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            TextBox txtCorreo = (TextBox)e.Row.FindControl("txtCorreo");
            //LinkButton Lb = (LinkButton)e.Row.FindControl("LinkButton1");
            ////txtCorreo.Text = gvConvenios.DataKeys[e.Row.RowIndex]["CORREO"].ToString();
            //Lb.Attributes.Add("onclick", "javascript:return confirm('¿Está seguro de que desea actualizar el registro con ID=" + DataBinder.Eval(e.Row.DataItem, "id") + "?')");
        }
    }
    protected void gvEmpresas_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvEmpresas.PageIndex = e.NewPageIndex;
        gvEmpresas.DataSource = ViewState["dtPaginas"];
        gvEmpresas.DataBind();
    }
    protected void txtFiltro_TextChanged(object sender, EventArgs e)
    {
        DataTable dtNew = Procedimientos.dtFiltrado("DESCRIPCION", "CODEMPRESA LIKE '*" + txtFiltro.Text.Trim() + "*' OR DESCRIPCION LIKE '*" + txtFiltro.Text.Trim() + "*'", (DataTable)ViewState["dtPaginas2"]);
        Procedimientos.LlenarGrid(dtNew, gvEmpresas);
    }
    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "1":
                {
                    pnlBusqueda.Visible = true;
                    break;
                }
            case "2":
                {
                    pnlBusqueda.Visible = false;
                    txtFiltro.Text = "";
                    DataTable dtNew = Procedimientos.dtFiltrado("ESTADO_AUT", "ESTADO_AUT LIKE '*true*'", (DataTable)ViewState["dtPaginas2"]);
                    Procedimientos.LlenarGrid(dtNew, gvEmpresas);
                    ViewState["dtPaginas"] = dtNew;
                    break;
                }
            case "3":
                {
                    pnlBusqueda.Visible = false;
                    txtFiltro.Text = "";
                    DataTable dtNew = Procedimientos.dtFiltrado("ESTADO_AUT", "ESTADO_AUT LIKE '*false*'", (DataTable)ViewState["dtPaginas2"]);
                    Procedimientos.LlenarGrid(dtNew, gvEmpresas);
                    ViewState["dtPaginas"] = dtNew;
                    break;
                }
        }
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        //string msg = "";
        //msg = gvPrestaciones.PageIndex.ToString();
        //Procedimientos.Script("Mensaje(" + msg + ")", litScript);
       
            string msg = "";
            for (int i = 0; i < gvEmpresas.Rows.Count; i++)
            {
                TextBox txtCorreo = (TextBox)gvEmpresas.Rows[i].FindControl("txtCorreo");
                if (txtCorreo.Text != "")
                {
                    if (validar(txtCorreo) == true)
                    {
                        if (new ClinicaCES.Logica.LMaestros().EmpresasActualizar(gvEmpresas.Rows[i].Cells[0].Text, txtCorreo.Text, Session["Nick1"].ToString()))
                        {
                            msg = "1";
                        }
                        else
                        {
                            msg = "3";
                        }
                        //}
                    }
                    else
                    {
                        msg = "66";
                        break;
                    }
                }
            }
            if (msg == "1")
            {
                gvEmpresas.PageIndex = gvEmpresas.PageIndex;
                gvEmpresas.DataSource = llenardt();
                gvEmpresas.DataBind();
                ddlFiltro.SelectedIndex = 0;
                txtFiltro.Text = "";
                pnlBusqueda.Visible = false;
                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                
            }
            else
            {
                if (msg == "3")
                { Procedimientos.Script("Mensaje(" + msg + ")", litScript); }
                else 
                {
                    
                    Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                }
            }
    }

    private bool validar(TextBox txtCorreo)
    {
        bool Valido = true;
        System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
        string[] css = { "form_input", "invalidtxt" };
        if (string.IsNullOrEmpty(txtCorreo.Text.Trim()))
        {
            Valido = false;
            //lblValidaMail.ForeColor = color[1];
            txtCorreo.CssClass = css[1];
        }
        else
        {

            txtCorreo.CssClass = css[0];

            if (!Procedimientos.regularExpression(txtCorreo.Text.Trim(), "^(([^<;>;()[\\]\\\\.,;:\\s@\\\"]+" + "(\\.[^<;>;()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@" + "((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}" + "\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+" + "[a-zA-Z]{2,}))$"))
            {
                Valido = false;
                //lblValidaMail.ForeColor = color[1];
                txtCorreo.CssClass = css[1];
            }
            else
            {
                //lblValidaMail.ForeColor = color[0];
                txtCorreo.CssClass = css[0];
            }
        }
        return Valido;
    }
    //protected void gvEmpresas_RowEditing(object sender, GridViewEditEventArgs e)
    //{
    //    string msg = "";

    //    TextBox txtCorreo = (TextBox)gvEmpresas.Rows[e.NewEditIndex].FindControl("txtCorreo");
       
    //    //if (validar(txtCorreo) == true)
    //    //{
    //    //    if (new ClinicaCES.Logica.LMaestros().EmpresasActualizarUnitaria(gvEmpresas.Rows[e.NewEditIndex].Cells[0].Text, txtCorreo.Text, Session["Nick"].ToString()))
    //    //    {
    //    //        msg = "1";
    //    //    }
    //    //    else
    //    //    {
    //    //        msg = "3";
    //    //    }
    //    //    //}
    //    //}
    //    //else
    //    //{
    //    //    msg = "66";
            
    //    //}
    //    //ddlFiltro.SelectedIndex = 0;
    //    //txtFiltro.Text = "";
    //    //pnlBusqueda.Visible = false;
    //    //Procedimientos.Script("Mensaje(" + msg + ")", litScript);
   
        
    //}
    
   
    protected void gvEmpresas_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        string msg = "";
        TextBox txtCorreo = (TextBox)gvEmpresas.Rows[e.RowIndex].FindControl("txtCorreo");
        if (new ClinicaCES.Logica.LMaestros().EmpresasActualizar(gvEmpresas.Rows[e.RowIndex].Cells[0].Text, txtCorreo.Text, Session["Nick1"].ToString()))
        {
            msg = "1";
        }
        else
        {
            msg = "3";
        }
        ddlFiltro.SelectedIndex = 0;
        txtFiltro.Text = "";
        pnlBusqueda.Visible = false;
        gvEmpresas.PageIndex = gvEmpresas.PageIndex;
        gvEmpresas.DataSource = llenardt();
        gvEmpresas.DataBind();
        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }
}