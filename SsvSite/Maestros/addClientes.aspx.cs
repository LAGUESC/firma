﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addClientes : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ViewState["dtRegional"] == null)
            ConsultarRegional();

        if (!IsPostBack)
        {
            Procedimientos.Titulo("Clientes", this.Page);
            Cargarpaises();
            CargarTipo();
            ControlesAbajo(false);
            ControlesArriba(true);

            string script =
                "if(trim(document.getElementById('" + txtNit.ClientID + "').value) != '' && document.getElementById('" + ddlTipo.ClientID + "').value != '-1') { " +
                    "__doPostBack('" + lnkComprobar.UniqueID + "',''); }";

            txtNit.Attributes.Add("onblur", script);
            ddlTipo.Attributes.Add("onblur", script);

            if (Request.QueryString["tip"] != null)
                Consultar(Request.QueryString["nit"], Request.QueryString["tip"], Request.QueryString["neg"]);
        }

        litScript.Text = string.Empty;
    }

    private void ConsultarRegional()
    {
        ViewState["dtRegional"] = new ClinicaCES.Logica.LRegionales().RegionalListar();
    }

    private void CargarTipo()
    {
        Procedimientos.LlenarCombos(ddlTipo, new ClinicaCES.Logica.LClientes().TipoConsultar(), "CODIGO", "DESCRIPCION");
    }

    private void Cargarpaises()
    { 
        Paises p = new Paises();
        DataTable dtpaises = new ClinicaCES.Logica.LPaises().PaisesConsultar(p);

        Procedimientos.LlenarCombos(ddlPais, dtpaises, "CODIGO", "DESCRIPCION");
        Procedimientos.comboEstadoInicial(ddlDpto);
        Procedimientos.comboEstadoInicial(ddlMpio);
    }

    private void ControlesArriba(bool estad)
    {
        ddlTipo.Enabled = estad;
        txtNit.Enabled = estad;
    }

    private void ControlesAbajo(bool estado)
    {
        txtRazonSocial.Enabled = estado;
        txtNomComercial.Enabled = estado;
        txtDireccion.Enabled = estado;
        txtTelefono.Enabled = estado;
        txtFax.Enabled = estado;
        ddlPais.Enabled = estado;
        ddlDpto.Enabled = estado;
        ddlMpio.Enabled = estado;
        txtEmail.Enabled = estado;
        txtCelular.Enabled = estado;       
        chkRemoto.Enabled = estado;
        chkInternet.Enabled = estado;
        gvCias.Visible = estado;
    }

    private void Consultar(string Nit, string Tipo)
    {
        if (string.IsNullOrEmpty(Nit) || Tipo == "-1")
            return;

        Clientes cli = new Clientes();
        cli.nit = Nit;
        cli.tipo = Tipo;

        DataSet dsCliente = new ClinicaCES.Logica.LClientes().ClienteConsultar(cli);
        DataTable dtClientes = dsCliente.Tables[0];

        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";

        imgRetirar.Enabled = true;
        imgRetirar.ImageUrl = "../icons/eliminar.gif";


        if (dtClientes.Rows.Count == 1)
        {
            ControlesAbajo(true);
            ControlesArriba(false);

            txtNit.Text = Nit;
            ddlTipo.SelectedValue = Tipo;

            btnGuardar.Enabled = true;

            DataRow row = dtClientes.Rows[0];
            bool estado = bool.Parse(row["ACTIVO"].ToString());
            hdnNegocio.Value = row["NEGOCIO"].ToString();
            txtRazonSocial.Text = row["RAZSOCIAL"].ToString();
            txtNomComercial.Text = row["NOMCOMCIAL"].ToString();
            txtDireccion.Text = row["DIRECCION"].ToString();
            txtTelefono.Text = row["TELEFONO"].ToString();
            txtFax.Text = row["FAX"].ToString();
            ddlPais.SelectedValue = row["IDPAIS"].ToString();
            CargarDptos(ddlPais.SelectedValue);
            ddlDpto.SelectedValue = row["IDDEPARTAMENTO"].ToString();
            CargarMpios(ddlPais.SelectedValue, ddlDpto.SelectedValue);
            ddlMpio.SelectedValue = row["IDMUNICIPIO"].ToString();
            txtEmail.Text = row["EMAIL"].ToString();
            txtCelular.Text = row["CELULAR"].ToString();
            //ddlTipo.SelectedValue = row["TIPO"].ToString();

            chkRemoto.Checked = bool.Parse(row["SOPREMOTO"].ToString());
            chkInternet.Checked = bool.Parse(row["INTERNET"].ToString());


            btnGuardar.Enabled = estado;
            imgGuardar.Enabled = estado;
            btnRetirar.Enabled = estado;
            btnReactivar.Visible = !estado;
            btnRetirar.Visible = estado;
            imgReactivar.Visible = !estado;
            imgRetirar.Visible = estado;
            gvCias.Visible = estado;

            if (!estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
                ControlesAbajo(estado);
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }
        }
        if (dtClientes.Rows.Count > 0)
        {
            Procedimientos.LlenarGrid(dtClientes, gvNegocios);
            MPE.Show();
            if (Tipo == "C")
            {
                Procedimientos.LlenarGrid(dsCliente.Tables[1], gvCias);
                hdnFilas.Value = dsCliente.Tables[1].Rows.Count.ToString();
            }
            else
                gvCias.Visible = false;
        }
        else
        {
            Nuevo();
        }

    }

    private void Consultar(string Nit, string Tipo, string Negocio)
    { 
        if(string.IsNullOrEmpty(Nit) || Tipo == "-1")
            return;

        Clientes cli = new Clientes();
        cli.nit = Nit;
        cli.tipo = Tipo;
        cli.negocio = Negocio;

        DataSet dsCliente = new ClinicaCES.Logica.LClientes().ClienteConsultar(cli);
        DataTable dtClientes = dsCliente.Tables[0];

        ControlesAbajo(true);
        ControlesArriba(false);

        txtNit.Text = Nit;
        ddlTipo.SelectedValue = Tipo;

        btnGuardar.Enabled = true;

        if (dtClientes.Rows.Count == 1)
        {
            DataRow row = dtClientes.Rows[0];
            bool estado = bool.Parse(row["ACTIVO"].ToString());
            hdnNegocio.Value = row["NEGOCIO"].ToString();
            txtRazonSocial.Text = row["RAZSOCIAL"].ToString();
            txtNomComercial.Text = row["NOMCOMCIAL"].ToString();
            txtDireccion.Text = row["DIRECCION"].ToString();
            txtTelefono.Text  = row["TELEFONO"].ToString();
            txtFax.Text = row["FAX"].ToString();
            ddlPais.SelectedValue = row["IDPAIS"].ToString();
            CargarDptos(ddlPais.SelectedValue);
            ddlDpto.SelectedValue = row["IDDEPARTAMENTO"].ToString();
            CargarMpios(ddlPais.SelectedValue, ddlDpto.SelectedValue);
            ddlMpio.SelectedValue = row["IDMUNICIPIO"].ToString();
            txtEmail.Text = row["EMAIL"].ToString();
            txtCelular.Text = row["CELULAR"].ToString();
            //ddlTipo.SelectedValue = row["TIPO"].ToString();
           
            chkRemoto.Checked =  bool.Parse(row["SOPREMOTO"].ToString());
            chkInternet.Checked =  bool.Parse(row["INTERNET"].ToString());


            btnGuardar.Enabled = estado;
            imgGuardar.Enabled = estado;
            btnRetirar.Enabled = estado;
            btnReactivar.Visible = !estado;
            btnRetirar.Visible = estado;
            imgReactivar.Visible = !estado;
            imgRetirar.Visible = estado;
            gvCias.Visible = estado;
            
            if (!estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
                ControlesAbajo(estado);
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }

            
        }

        if (Tipo == "C")
        {
            Procedimientos.LlenarGrid(dsCliente.Tables[1], gvCias);
            hdnFilas.Value = dsCliente.Tables[1].Rows.Count.ToString();
        }
        else
            gvCias.Visible = false;
        
        
    }

    private void CargarDptos(string Pais)
    {
        DataTable dtDptos = new ClinicaCES.Logica.LClientes().DptoConsultar(Pais);

        Procedimientos.LlenarCombos(ddlDpto, dtDptos, "CODIGO", "DESCRIPCION");
    }

    private void CargarMpios(string Pais,string Dpto)
    {
        DataTable dtMpios = new ClinicaCES.Logica.LClientes().MpioConsultar(Pais,Dpto);

        Procedimientos.LlenarCombos(ddlMpio, dtMpios, "CODIGO", "DESCRIPCION");
    }

    private void Guardar
    (
        string nit ,
        string negocio ,
        string razsocial ,
        string nomcomcial,
        string direccion ,
        string telefono ,
        string fax,
        string idpais ,
        string iddepartamento,
        string idmunicipio,
        string email ,
        string celular,
        string tipo ,       
        bool sopremoto,
        bool internet,
        string usr,
        string xmlCia
    )
    {
        if (!ValidaGuardar())
            return;

        Clientes cli = new Clientes();

        cli.nit = nit;
        cli.negocio = negocio;
        cli.razsocial = razsocial;
        cli.nomcomcial = nomcomcial;
        cli.direccion = direccion;
        cli.telefono = telefono;
        cli.fax = fax;
        cli.idpais = idpais;
        cli.iddepartamento = iddepartamento;
        cli.idmunicipio = idmunicipio;
        cli.email = email;
        cli.celular= celular;
        cli.tipo = tipo;        
        cli.sopremoto = sopremoto;
        cli.internet = internet;
        cli.usr = usr;
        cli.xmlCia = xmlCia;

        string msg = "3";
        string ng = negocio;
        if (new ClinicaCES.Logica.LClientes().ClientesActualizar(cli))
        {
            msg = "1";
            if(string.IsNullOrEmpty(negocio))
                ng = new ClinicaCES.Logica.LClientes().NegocioConsultar(nit, tipo);
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addClientes.aspx?nit=" + nit + "&tip=" + tipo + "&neg=" + ng + "')", litScript);
    }

    protected void SelectedIndexChanged(object sender, EventArgs e)
    {
        if (sender.Equals(ddlPais))
        {
            Procedimientos.comboEstadoInicial(ddlMpio);
            if (ddlPais.SelectedIndex != 0)            
                CargarDptos(ddlPais.SelectedValue);            
            else
                Procedimientos.comboEstadoInicial(ddlDpto);
        }
        else if (sender.Equals(ddlDpto))
        {
            if (ddlDpto.SelectedIndex != 0)
                CargarMpios(ddlPais.SelectedValue, ddlDpto.SelectedValue);
            else
                Procedimientos.comboEstadoInicial(ddlMpio);
        }
    }

    private void CambiarEstado(bool Estado, string Tipo, string Nit, string Negocio)
    {
        Clientes cli = new Clientes();

        cli.nit = Nit;
        cli.negocio = Negocio;
        cli.tipo = Tipo;
        cli.estado = Estado;

        string msg = "1";

        if (!new ClinicaCES.Logica.LClientes().ClientesEstado(cli))
            msg = "3";

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addClientes.aspx?nit=" + Nit + "&tip=" + Tipo + "&neg=" + Negocio + "')", litScript);

    }

    private void Nuevo()
    {
        ControlesAbajo(true);
        ControlesArriba(false);
        btnGuardar.Enabled = true;
        MPE.Hide();
        hdnNegocio.Value = string.Empty;

        txtRazonSocial.Text = string.Empty;
        txtNomComercial.Text = string.Empty;
        txtDireccion.Text = string.Empty;
        txtTelefono.Text = string.Empty;
        txtCelular.Text = string.Empty;
        txtFax.Text = string.Empty;
        txtEmail.Text = string.Empty;
        chkInternet.Checked = false;
        chkRemoto.Checked = false;
        Procedimientos.comboEstadoInicial(ddlMpio);
        Procedimientos.comboEstadoInicial(ddlDpto);
        ddlPais.SelectedIndex = 0;

        txtRazonSocial.Focus();

        Clientes cli = new Clientes();
        DataSet dsCliente = new ClinicaCES.Logica.LClientes().ClienteConsultar(cli);
        if (ddlTipo.SelectedValue == "C")
        {
            Procedimientos.LlenarGrid(dsCliente.Tables[1], gvCias);
            hdnFilas.Value = dsCliente.Tables[1].Rows.Count.ToString();
        }
        else
            gvCias.Visible = false;

    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        {
            string negocio = null;

            if (!string.IsNullOrEmpty(hdnNegocio.Value))
                negocio = hdnNegocio.Value;

            bool estado = true;
            string xmlCias = xmlCia(ref estado,ddlTipo.SelectedValue);

            if (estado)
            {
                Guardar(txtNit.Text.Trim(),
                negocio, txtRazonSocial.Text.Trim().ToUpper(),
                txtNomComercial.Text.Trim().ToUpper(), txtDireccion.Text.Trim().ToUpper(),
                txtTelefono.Text.Trim().ToUpper(), txtFax.Text.Trim().ToUpper(),
                ddlPais.SelectedValue, ddlDpto.SelectedValue, ddlMpio.SelectedValue,
                txtEmail.Text.Trim(), txtCelular.Text.Trim(), ddlTipo.SelectedValue,
                chkRemoto.Checked, chkInternet.Checked, Session["Nick1"].ToString(), xmlCias);
            }


        }
        else if (sender.Equals(btnCancelar) || sender.Equals(btnNuevo))
            Response.Redirect("addClientes.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(false, ddlTipo.SelectedValue, txtNit.Text.Trim(), hdnNegocio.Value);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(true, ddlTipo.SelectedValue, txtNit.Text.Trim(), hdnNegocio.Value);
        else if (sender.Equals(btnNuevoNegocio))
            Nuevo();

    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Consultar(txtNit.Text.Trim(), ddlTipo.SelectedValue);
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
        {
            string negocio = null;

            if (!string.IsNullOrEmpty(hdnNegocio.Value))
                negocio = hdnNegocio.Value;

            bool estado = true;
            string xmlCias = xmlCia(ref estado, ddlTipo.SelectedValue);

            if (estado)
            {
                Guardar(txtNit.Text.Trim(),
                negocio, txtRazonSocial.Text.Trim().ToUpper(),
                txtNomComercial.Text.Trim().ToUpper(), txtDireccion.Text.Trim().ToUpper(),
                txtTelefono.Text.Trim().ToUpper(), txtFax.Text.Trim().ToUpper(),
                ddlPais.SelectedValue, ddlDpto.SelectedValue, ddlMpio.SelectedValue,
                txtEmail.Text.Trim(), txtCelular.Text.Trim(), ddlTipo.SelectedValue,
                chkRemoto.Checked, chkInternet.Checked, Session["Nick1"].ToString(), xmlCias);
            }
            

        }
        else if (sender.Equals(imgCancelar) || sender.Equals(imgNuevo))
            Response.Redirect("addClientes.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(false, ddlTipo.SelectedValue, txtNit.Text.Trim(), hdnNegocio.Value);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(true, ddlTipo.SelectedValue, txtNit.Text.Trim(), hdnNegocio.Value);
    }

    private string xmlCia(ref bool estado, string tipo)
    {

        if (tipo == "C")
        {
            string xml = "<Raiz>";
            string Cia;
            foreach (GridViewRow row in gvCias.Rows)
            {
                Cia = gvCias.DataKeys[row.RowIndex]["CODIGO"].ToString();
                CheckBox chkCia = (CheckBox)row.FindControl("chkCia");
                DropDownList ddlRegional = (DropDownList)row.FindControl("ddlRegional");
                if (chkCia.Checked && ddlRegional.SelectedIndex == 0)
                {
                    ddlRegional.CssClass = "invalidtxt";
                    chkCia.CssClass = string.Empty;
                    estado = false;
                }
                else if (!chkCia.Checked && ddlRegional.SelectedIndex != 0)
                {
                    ddlRegional.CssClass = string.Empty;
                    chkCia.CssClass = "invalidtxt";
                    estado = false;
                }
                else if (chkCia.Checked && ddlRegional.SelectedIndex != 0)
                {
                    xml += "<Datos Cia='" + Cia + "' Reg='" + ddlRegional.SelectedValue + "' />";
                    ddlRegional.CssClass = string.Empty;
                    chkCia.CssClass = string.Empty;
                }
            }

            return xml + "</Raiz>";
        }
        else
            return (null);

        
    }
    protected void gvCias_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            string codigo = gvCias.DataKeys[e.Row.RowIndex]["CODIGO"].ToString();
            DataTable dt = Procedimientos.dtFiltrado("REGIONAL", "COMPANIA = '" + codigo + "'", (DataTable)ViewState["dtRegional"]);
            DropDownList ddlRegional = (DropDownList)e.Row.FindControl("ddlRegional");
            Procedimientos.LlenarCombos(ddlRegional, dt, "CODIGO", "REGIONAL");
            string Regional = gvCias.DataKeys[e.Row.RowIndex]["REGIONAL"].ToString();
            ddlRegional.SelectedValue = Regional;
        }
    }
    protected void gvNegocios_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onclick", "redirect('addClientes.aspx?nit=" + e.Row.Cells[0].Text.Trim() + "&neg=" + gvNegocios.DataKeys[e.Row.RowIndex]["NEGOCIO"].ToString() + "&tip=" + e.Row.Cells[3].Text + "')");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
        }

    }

    private bool ValidaGuardar()
    {
        TextBox[] txt = 
        {
            txtNit,txtRazonSocial,txtNomComercial,txtDireccion,txtTelefono, txtEmail
        };

        DropDownList[] ddl = 
        {
            ddlDpto,ddlMpio,ddlPais,ddlTipo
        };

        bool valido = Procedimientos.ValidaGuardar(txt, ddl);
        if (valido)
        {
            valido = Procedimientos.regularExpression(txtEmail.Text.Trim(), "^(([^<;>;()[\\]\\\\.,;:\\s@\\\"]+" + "(\\.[^<;>;()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@" + "((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}" + "\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+" + "[a-zA-Z]{2,}))$");
            if (!valido)
                txtEmail.CssClass = "invalidtxt";
            else
                txtEmail.CssClass = "form_input";
            
        }

        int control = 0;
        if (valido && ddlTipo.SelectedValue == "C")
        {
            foreach (GridViewRow item in gvCias.Rows)
            {
                CheckBox chkCia = (CheckBox)item.FindControl("chkCia");
                DropDownList ddlRegional = (DropDownList)item.FindControl("ddlRegional");

                if (chkCia.Checked && ddlRegional.SelectedIndex > 0)
                {
                    control++;
                    //break;
                }
            }

            if (control == 0)
            {
                Procedimientos.Script("Mensaje(60)", litScript);
                valido = false;                
            }
        }

        if (ddlTipo.SelectedValue == "I")
            gvCias.Visible = false;

        return valido;
    }
}