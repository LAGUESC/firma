﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Prueba.aspx.cs" Inherits="Maestros_Prueba" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
        <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
        .Grid td
        {
            background-color: #A1DCF2;
            color: black;
            font-size: 10pt;
            line-height:200%
        }
        .Grid th
        {
            background-color: #3AC0F2;
            color: White;
            font-size: 10pt;
            line-height:200%
        }
        .ChildGrid td
        {
            background-color: #eee !important;
            color: black;
            font-size: 10pt;
            line-height:200%
        }
        .ChildGrid th
        {
            background-color: #6C6C6C !important;
            color: White;
            font-size: 10pt;
            line-height:200%
        }
            .auto-style1 {
                width: 129px;
            }
            .auto-style2 {
                width: 166px;
            }
            .auto-style3 {
                width: 4px;
            }
    </style>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $("[src*=plus]").live("click", function () {
            $(this).closest("tr").after("<tr><td></td><td colspan = '999'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("src", "../img/minus.png");
        });
        $("[src*=minus]").live("click", function () {
            $(this).attr("src", "../img/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>

</head>
<body>
    <form id="form1" runat="server">
        <table style="width: 435px"  >
            <tr>
                <td  align="center"  colspan="6" >
                    <div class="TipoInformacion">
                        <B>Listado de Responsables</B></div>
                </td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="Label1" runat="server" Text="Cama:" Font-Bold="True" Font-Size="Small"></asp:Label>
                </td>
                <td align="left">
                    <asp:Label ID="lblCama" runat="server" Text="Label" Font-Size="Small"></asp:Label>
                </td>
                <td align="left" class="auto-style1">
                    &nbsp;</td>
                <td align="left" class="auto-style1">
                    &nbsp;</td>
                <td  align="left">
                    &nbsp;</td>
                <td  >
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="left" >
                    <asp:Label ID="lblResActual" runat="server" Text="Responsable Actual:" Font-Bold="True" Font-Size="Small" Visible="False"></asp:Label>
                </td>
                <td align="left" >
                    <asp:Label ID="lblResponsable" runat="server" Text="Label" Font-Size="Small" Visible="False"></asp:Label>
                </td>
                <td align="leff" >
                    &nbsp;</td>
                <td align="leff" >
                    &nbsp;</td>
                <td align="right">
                    &nbsp;</td>
                <td  >
                    &nbsp;</td>
            </tr>
            <tr>
                <td align="left" colspan="6" >
                    <asp:GridView ID="gvInforme" runat="server" AllowPaging="True" AutoGenerateColumns="False" CellPadding="4" EnableModelValidation="True" ForeColor="#333333" GridLines="None" 
                        PageSize="15" Width="551px"  OnRowUpdating="gvInforme_RowUpdating">
                        <FooterStyle BackColor="#F7DFB5" />
                        <PagerSettings Visible="False" />
                        <RowStyle CssClass="normalrow" />
                        <AlternatingRowStyle CssClass="alterrow" />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />
                        <Columns>
                            <asp:BoundField DataField="ID" HeaderText="USUARIO" />
                            <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" />
                             <asp:TemplateField>    
                                 <ItemTemplate>
                                 <asp:LinkButton ID="Editar" Runat="server" OnClientClick="return confirm('¿Desea actualizar el registro?');" CommandName="Update">Actualizar</asp:LinkButton>
                                 </ItemTemplate>
                             </asp:TemplateField> 
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center"  colspan="6">
                    &nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
