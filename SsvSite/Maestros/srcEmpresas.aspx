﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="srcConvenios.aspx.cs" Inherits="Maestros_srcConvenios" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcEmpresas.aspx.cs" Inherits="Maestros_srcEmpresas" Title="Correos Empresas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    &nbsp;<table align="center">
        <tr>
            <td>Filtro:</td>
            <td><asp:DropDownList ID="ddlFiltro" AutoPostBack="true" runat="server" 
                    onselectedindexchanged="ddlFiltro_SelectedIndexChanged">
                <asp:ListItem>--- Seleccione ---</asp:ListItem>
                <asp:ListItem Value="1">Codigo o Descripción</asp:ListItem>
                </asp:DropDownList></td>
            <td>
                <asp:Panel ID="pnlBusqueda" runat="server" Visible="False">
                    <asp:TextBox ID="txtFiltro" runat="server" Width="179px" AutoPostBack="True" OnTextChanged="txtFiltro_TextChanged"></asp:TextBox>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center" colspan="3">
                <br />
            </td>
        </tr>
        </table>
    <table align="center">
        <tr>
            <td>
                <asp:GridView AutoGenerateColumns="False" ID="gvEmpresas" runat="server" DataKeyNames="CODEMPRESA" 
                CellPadding="4" ForeColor="#333333" GridLines="None"  PageSize="20" Width="100%" EnableModelValidation="True" OnRowDataBound="gvEmpresas_RowDataBound" AllowPaging="True" EnableTheming="True" OnPageIndexChanging="gvEmpresas_PageIndexChanging" OnRowUpdating="gvEmpresas_RowUpdating" 
                     >
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#dcdcdc" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle  ForeColor="White" HorizontalAlign="Center" CssClass="cabeza" />
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>        
                                            
                                                                          
                         <asp:BoundField DataField="CODEMPRESA" HeaderText="CODEMPRESA"  />
                    
                         <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION" />
                         <%--<asp:BoundField DataField="ESTADO" HeaderText="Estado" Visible="false" />--%>
                        <asp:TemplateField>
                             <ItemTemplate>
                                 <%--<asp:CheckBox ID="chkPages" runat="server" Text='<%# Bind("ESTADO_AUT") %>' />--%>
                                 <asp:TextBox ID="txtCorreo" runat="server" Width="246px" Text='<%# Bind("CORREO") %>' ></asp:TextBox>
                                 <asp:RegularExpressionValidator ID="TxtEmailRegEx" runat="server" 
                                    ErrorMessage="Ingrese un Email correcto"
                                    ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*" 
                                    ControlToValidate="txtCorreo" />
                             </ItemTemplate>
                             <ItemStyle HorizontalAlign="Left" />
                             
                             <HeaderTemplate>
                                CORREO
                             </HeaderTemplate>
                         </asp:TemplateField>  
                                                                          
                        <asp:TemplateField>
<ItemTemplate>
<asp:LinkButton ID="delete" Runat="server" OnClientClick="return confirm('¿Desea actualizar el registro?');" CommandName="Update">Editar
</asp:LinkButton>
</ItemTemplate>
</asp:TemplateField> 
                       
                                                                          
                    </Columns>
                </asp:GridView>
               

            </td>
        </tr>
        <tr>
            <td>
                &nbsp;</td>
        </tr>
        <tr>
            <td align="center">
                <asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick="return Mensaje(2)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" OnClick="btnGuardar_Click" Visible="False" />
                </td>
        </tr>
    </table>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>
