﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addAplicacion.aspx.cs" Inherits="Maestros_addAplicacion" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>
<table width="100%">
    <tr>
        <td>
            <table align="left">
                <tr>
                    <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" 
                            ImageUrl="../icons/nuevo.png" onclick="Images_Click" TabIndex="1" /></td>
                    <td><asp:ImageButton runat="server" ID="imgRetirar"  ToolTip="Retirar" 
                            ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" 
                            OnClientClick="return Mensaje(50)" TabIndex="2" /></td>
                    <td><asp:ImageButton runat="server" Visible="false" ID="imgReactivar" 
                            ToolTip="Reactivar" ImageUrl="../icons/refresh.png" onclick="Images_Click" 
                            TabIndex="3"  /></td>                
                    <td><asp:ImageButton runat="server" ID="imgGuardar" ToolTip="Guardar" 
                            OnClientClick="return Mensaje(13)" ImageUrl="../icons/16_save_d.png" 
                            onclick="Images_Click" TabIndex="4" /></td>                
                    <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" 
                            ImageUrl="../icons/cancel.png" onclick="Images_Click" TabIndex="5" /></td>
                    <td><img onclick="redirect('srcAplicacion.aspx')" src="../icons/magnify.png" id="imgBuscar" runat="server" style="cursor:pointer" /></td>
                </tr>     
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>
                    <td>Codigo:</td>                        
                    <td><asp:TextBox onblur="if(trim(this.value) != ''){redirect('addAplicacion.aspx?id=' + this.value)}" CssClass="form_input" ID="txtCodigo" runat="server" Width="60px" MaxLength="15" TabIndex="6"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtCodigo" FilterType="UppercaseLetters, LowercaseLetters, Numbers"  ></ajaxToolkit:FilteredTextBoxExtender>                    
                            
                            </td>
                </tr>
                <tr>
                    <td>Nombre:</td>
                    <td><asp:TextBox Enabled="false" CssClass="form_input" ID="txtNombre" runat="server" MaxLength="100" TabIndex="7" Width="389px"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtNombre" FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>                    
                            </td>                    
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" TabIndex="8"   /></td>
                    <td><asp:Button Visible="false" ID="btnReactivar" runat="server" Text="Reactivar" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" TabIndex="9"   /></td>
                    <td><asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" 
                            OnClientClick="return Mensaje(50)" onmouseover="this.className='btnhov'" 
                            onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" 
                            TabIndex="10"  /></td>
                    <td><asp:Button ID="btnGuardar" Enabled="false" runat="server" Text="Guardar" 
                            OnClientClick="return Mensaje(13)" onmouseover="this.className='btnhov'" 
                            onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" 
                            TabIndex="11" /></td>                                                                                
                    <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" 
                            onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                            CssClass="btn" onclick="Click_Botones" TabIndex="12"  /></td>                     
                    <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcAplicacion.aspx')" /> </td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<br />
</asp:Content>

