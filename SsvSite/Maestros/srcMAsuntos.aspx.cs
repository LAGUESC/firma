﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcMAsuntos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Asuntos", this.Page);
            EstadoInicial();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        litScript.Text = string.Empty;
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "Asunto", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void Consultar()
    {
        MAsuntos masuntos = new MAsuntos();
        DataTable dtMasunto = new ClinicaCES.Logica.LMAsuntos().MAsuntoConsultar(masuntos);
        ViewState["dtMasunto"] = dtMasunto;
        ViewState["dtPaginas"] = dtMasunto;
        Procedimientos.LlenarGrid(dtMasunto, gvMAsuntos);
    }

    protected void gvMAsuntos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 1; i < gvMAsuntos.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addMAsuntos.aspx?id=" + e.Row.Cells[1].Text.Trim() + "')");
            }

            e.Row.Cells[3].Text = e.Row.Cells[3].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkMAsunto = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvMAsuntos.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkMAsunto.Enabled = false;

            chkMAsunto.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlMAsunto, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LMAsuntos().MAsuntosRetirar(xmlMAsunto, Usuario))
        {
            msg = "7";

            MAsuntos masuntos = new MAsuntos();
            DataTable dtMasunto = new ClinicaCES.Logica.LMAsuntos().MAsuntoConsultar(masuntos);
            ViewState["dtMasunto"] = dtMasunto;
            Filtrar();
        }
        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND NOMBRE LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtMasunto = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtMasunto"]);
        Procedimientos.LlenarGrid(dtMasunto, gvMAsuntos);

        ViewState["dtPaginas"] = dtMasunto;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "Asunto", pnlNombre, txtBusquedaNombre);
    }

    protected void gvMAsuntos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvMAsuntos.PageIndex = e.NewPageIndex;
        gvMAsuntos.DataSource = ViewState["dtPaginas"];
        gvMAsuntos.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlMAsunto(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addMAsuntos.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    private string xmlMAsunto()
    {
        string masuntos;
        string xmlMAsunto = "<Raiz>";

        for (int i = 0; i < gvMAsuntos.Rows.Count; i++)
        {
            GridViewRow row = gvMAsuntos.Rows[i];
            CheckBox chkMasunto = (CheckBox)row.FindControl("chkEliminar");
            if (chkMasunto.Checked)
            {
                masuntos = row.Cells[1].Text.Trim();
                xmlMAsunto += "<Datos CODIGO='" + masuntos + "' />";
            }
        }

        return xmlMAsunto += "</Raiz>";
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("NOMBRE", "Asunto", pnlNombre, txtBusquedaNombre);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NOMBRE":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
