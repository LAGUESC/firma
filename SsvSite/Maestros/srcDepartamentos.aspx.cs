﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcDepartamentos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        litScript.Text = string.Empty;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Departamentos", this.Page);
            EstadoInicial();
            string script = "document.getElementById('" + imgRetirar.ClientID + "').disabled = true;";
            Procedimientos.Script(script, litScript);
            ListarPais();
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        
    }

    private void ListarPais()
    {
        Procedimientos.LlenarCombos(ddlPais, new ClinicaCES.Logica.LDepartamentos().PaisListar(), "CODIGO", "DESCRIPCION");
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Departamento", pnlNombre, txtBusquedaNombre);
        if (pnlPais.Visible)
            kitarFiltro("COD_PAIS", "Pais", pnlPais, ddlFlitro);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }

    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }

    private void kitarFiltro(string value, string text, Panel pnl, DropDownList txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.SelectedIndex = 0;
    }

    private void Consultar()
    {
        Departamentos dep = new Departamentos();
        DataTable dtDep = new ClinicaCES.Logica.LDepartamentos().DeparConsultar(dep);
        ViewState["dtDep"] = dtDep;
        ViewState["dtPaginas"] = dtDep;
        Procedimientos.LlenarGrid(dtDep, gvDepartamentos);
    }

    protected void gvDepartamentos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            for (int i = 1; i < gvDepartamentos.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addDepartamentos.aspx?dep=" + e.Row.Cells[1].Text.Trim() + "&pais=" + e.Row.Cells[3].Text.Trim() + "')");
            }

            e.Row.Cells[4].Text = e.Row.Cells[4].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvDepartamentos.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlDepartamento, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LDepartamentos().DepRetirar(xmlDepartamento, Usuario))
        {
            msg = "7";

            Departamentos dep = new Departamentos();
            DataTable dtDep = new ClinicaCES.Logica.LDepartamentos().DeparConsultar(dep);
            ViewState["dtDep"] = dtDep;
            Filtrar();
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND DESCRIPCION LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        if (ddlPais.SelectedIndex != 0)
        {
            filtro += " AND COD_PAIS = '" + ddlPais.SelectedValue + "'";
        }
        DataTable dtDep = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtDep"]);
        Procedimientos.LlenarGrid(dtDep, gvDepartamentos);

        ViewState["dtPaginas"] = dtDep;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Departamento", pnlNombre, txtBusquedaNombre);
        if (pnlPais.Visible)
            kitarFiltro("COD_PAIS", "Pais", pnlPais, ddlPais);
    }

    private string xmlDepartamento()
    {
        string dep;
        string pais;
        string xmlDepartamento = "<Raiz>";

        for (int i = 0; i < gvDepartamentos.Rows.Count; i++)
        {
            GridViewRow row = gvDepartamentos.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                dep = row.Cells[1].Text.Trim();
                pais = row.Cells[3].Text.Trim();
                xmlDepartamento += "<Datos DEPARTAMENTO='" + dep + "' PAIS='" + pais + "' />";
            }
        }

        return xmlDepartamento += "</Raiz>";
    }

    protected void gvDepartamentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvDepartamentos.PageIndex = e.NewPageIndex;
        gvDepartamentos.DataSource = ViewState["dtPaginas"];
        gvDepartamentos.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlDepartamento(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addDepartamentos.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        else if (sender.Equals(lnkNombreKitar))
            kitarFiltro("DESCRIPCION", "Departamento", pnlNombre, txtBusquedaNombre);
        else if (sender.Equals(lnkPaisKitar))
            kitarFiltro("COD_PAIS", "Pais", pnlPais, ddlPais);
    }

    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "DESCRIPCION":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            case "COD_PAIS":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlPais.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
