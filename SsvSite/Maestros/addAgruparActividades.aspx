﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addAgruparActividades.aspx.cs" Inherits="Maestros_addAgruparActividades" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
&nbsp;<table align="center">
        <tr>
            <td>Etapa:</td>
            <td><asp:DropDownList ID="ddlEtapas" AutoPostBack="true" runat="server" 
                    onselectedindexchanged="ddlEtapas_SelectedIndexChanged"></asp:DropDownList></td>
        </tr>
        <tr>
            <td><asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick="return Mensaje(2)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>
        </tr>
    </table>
    <table align="center">
        <tr>
            <td>
                <asp:GridView AutoGenerateColumns="False" ID="gvActividades" runat="server" DataKeyNames="Estado,ACTIVIDAD_NOMBRE"
                CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
                    onrowdatabound="gvActividades_RowDataBound">
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#dcdcdc" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>        
                         <asp:TemplateField>
                             <ItemTemplate>
                                 <asp:CheckBox ID="chkPages" runat="server" Text='<%# Bind("ACTIVIDAD_NOMBRE") %>' />
                             </ItemTemplate>
                             <ItemStyle HorizontalAlign="Left" />
                             <HeaderTemplate>
                                Pagina
                             </HeaderTemplate>
                         </asp:TemplateField>                     
                                                                          
                    </Columns>
                </asp:GridView> 
            </td>
        </tr>
    </table>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

