﻿using System;
using System.Collections;
using System.Configuration;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addMunicipios : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Municipio", this.Page);
            Procedimientos.comboEstadoInicial(ddlDepartamento);
            ListarPais();
            if (Request.QueryString["id"] != null)
                Consultar(Request.QueryString["pais"], Request.QueryString["dep"], Request.QueryString["id"]);

            string script =
                "if(document.getElementById('" + ddlPais.ClientID + "').value != '-1' && document.getElementById('" + ddlDepartamento.ClientID + "').value != '-1' && trim(document.getElementById('" + txtCodigo.ClientID + "').value) != ''){" +
                    "__doPostBack('" + lnkComprobar.UniqueID + "',''); }";

            ddlPais.Attributes.Add("onblur", script);
            ddlDepartamento.Attributes.Add("onblur", script);
            txtCodigo.Attributes.Add("onblur", script);
        }
        litScript.Text = string.Empty;
    }

    private void ListarPais()
    {
        Procedimientos.LlenarCombos(ddlPais, new ClinicaCES.Logica.LMunicipios().PaisListar(), "CODIGO", "DESCRIPCION");
    }

    private void Consultar(string pais, string dep, string codigo)
    {
        Municipios mun = new Municipios();
        mun.Pais = pais;
        mun.Depar = dep;
        mun.Codigo = codigo;

        DataTable dt = new ClinicaCES.Logica.LMunicipios().MunConsultar(mun);

        txtCodigo.Text = codigo;
        ddlPais.SelectedValue = pais;
        ListarDepartamento(ddlPais.SelectedValue);
        ddlDepartamento.SelectedValue = dep;


        btnGuardar.Enabled = true;
        btnRetirar.Enabled = false;
        btnRetirar.Visible = true;
        btnReactivar.Visible = false;
        txtNombre.Enabled = true;
        txtCodigo.Enabled = false;
        ddlPais.Enabled = false;
        ddlDepartamento.Enabled = false;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";

        if (dt.Rows.Count > 0)
        {
            txtNombre.Text = dt.Rows[0]["DESCRIPCION"].ToString();
            bool Estado = bool.Parse(dt.Rows[0]["ESTADO"].ToString());

            btnGuardar.Enabled = Estado;
            btnRetirar.Enabled = Estado;
            btnRetirar.Visible = Estado;
            btnReactivar.Visible = !Estado;
            imgReactivar.Visible = !Estado;
            imgRetirar.Visible = Estado;
            imgGuardar.Enabled = Estado;
            txtNombre.Enabled = Estado;
            imgRetirar.Enabled = Estado;

            if (!Estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }

            txtNombre.Focus();
        }
    }

    private void ListarDepartamento(string Pais)
    {
        Departamentos dep = new Departamentos();
        dep.Pais = Pais;
        Procedimientos.LlenarCombos(ddlDepartamento, Procedimientos.dtFiltrado("DESCRIPCION","ESTADO = 1", new ClinicaCES.Logica.LDepartamentos().DeparConsultar(dep)), "CODIGO", "DESCRIPCION");
    }

    protected void ddlPais_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlPais.SelectedIndex != 0)
        {
            ListarDepartamento(ddlPais.SelectedValue);
        }
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Consultar(ddlPais.SelectedValue, ddlDepartamento.SelectedValue, txtCodigo.Text.Trim());
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(ddlPais.SelectedValue, ddlDepartamento.SelectedValue, txtCodigo.Text.Trim().ToUpper(),txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(btnNuevo) || sender.Equals(btnCancelar))
            Response.Redirect("addMunicipios.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlPais.SelectedValue, ddlDepartamento.SelectedValue, false);
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlPais.SelectedValue, ddlDepartamento.SelectedValue, true);
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(ddlPais.SelectedValue, ddlDepartamento.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick1"].ToString());
        else if (sender.Equals(imgNuevo) || sender.Equals(imgCancelar))
            Response.Redirect("addMunicipios.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlPais.SelectedValue, ddlDepartamento.SelectedValue, false);
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlPais.SelectedValue, ddlDepartamento.SelectedValue, true);
    }

    private void CambiarEstado(string Codigo, string Pais, string Dep, bool? Estado)
    {
        Municipios mun = new Municipios();


        mun.Estado = Estado;
        mun.Pais = Pais;
        mun.Depar = Dep;
        mun.Codigo = Codigo;
        string msg = "1";

        if (!new ClinicaCES.Logica.LMunicipios().MunEstado(mun))
            msg = "3";

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addMunicipios.aspx?id=" + Codigo + "&pais=" + Pais + "&dep=" + Dep + "')", litScript);
    }

    private void Guardar(string Pais, string Dep, string Codigo, string Desc, string Usr)
    {
        if (!ValidaGuardar())
            return;

        Municipios mun = new Municipios();

        mun.Pais = Pais;
        mun.Depar = Dep;
        mun.Codigo = Codigo;
        mun.Municipio = Desc;
        mun.Usr = Usr;

        string msg = "3";

        if (new ClinicaCES.Logica.LMunicipios().MunicipioActualizar(mun))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addMunicipios.aspx?id=" + Codigo + "&pais=" + Pais + "&dep=" + Dep + "')", litScript);

    }

    private bool ValidaGuardar()
    {
        bool Guarda = true;

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            Guarda = false;
            txtNombre.CssClass = "invalidtxt";
        }
        else
        {
            txtNombre.CssClass = "form_input";
        }

        return Guarda;
    }
}
