﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_addRegionales : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Regionales", this.Page);
            ListarCompania();

            if (Request.QueryString["reg"] != null)
                Consultar(Request.QueryString["comp"], Request.QueryString["reg"]);

            string script =
                "if(document.getElementById('" + ddlCompania.ClientID + "').value != '-1' && trim(document.getElementById('" + txtCodigo.ClientID + "').value) != ''){" +
                    "__doPostBack('" + lnkComprobar.UniqueID + "',''); }";

            ddlCompania.Attributes.Add("onblur", script);
            txtCodigo.Attributes.Add("onblur", script);
        }
        litScript.Text = string.Empty;
    }

    private void Guardar(string Compania, string Codigo, string Desc, string Usr)
    {
        if (!ValidaGuardar())
            return;

        Regionales men = new Regionales();

        men.Compania = Compania;
        men.Codigo = Codigo;
        men.Regional = Desc;
        men.Usr = Usr;

        string msg = "3";

        if (new ClinicaCES.Logica.LRegionales().RegionalesActualizar(men))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addRegionales.aspx?reg=" + Codigo + "&comp=" + Compania + "')", litScript);

    }

    private void CambiarEstado(string Codigo, string Cia, bool? Estado, string Usr)
    {
        Regionales men = new Regionales();

        men.Estado = Estado;
        men.Compania = Cia;
        men.Codigo = Codigo;
        men.Usr = Usr;

        string msg = "3";

        if (new ClinicaCES.Logica.LRegionales().RegionalesCambiarEstado(men))
        {
            msg = "1";
        }

        Procedimientos.Script("Mensaje(" + msg + ");redirect('addRegionales.aspx?reg=" + Codigo + "&comp=" + Cia + "')", litScript);
    }

    private void Consultar(string Compania, string codigo)
    {
        Regionales men = new Regionales();
        men.Compania = Compania;
        men.Codigo = codigo;

        DataTable dt = new ClinicaCES.Logica.LRegionales().RegionalesConsultar(men);

        txtCodigo.Text = codigo;
        ddlCompania.SelectedValue = Compania;

        btnGuardar.Enabled = true;
        imgGuardar.Enabled = true;
        imgGuardar.ImageUrl = "../icons/16_save.png";
        btnRetirar.Enabled = false;
        btnRetirar.Visible = true;
        btnReactivar.Visible = false;
        txtNombre.Enabled = true;
        txtCodigo.Enabled = false;
        ddlCompania.Enabled = false;



        if (dt.Rows.Count > 0)
        {
            txtNombre.Text = dt.Rows[0]["REGIONAL"].ToString();
            bool Estado = bool.Parse(dt.Rows[0]["ESTADO"].ToString());

            btnGuardar.Enabled = Estado;
            btnRetirar.Enabled = Estado;
            btnRetirar.Visible = Estado;
            btnReactivar.Visible = !Estado;
            imgReactivar.Visible = !Estado;
            imgRetirar.Visible = Estado;

            imgGuardar.Enabled = Estado;
            imgRetirar.Enabled = Estado;

            if (!Estado)
            {
                imgGuardar.ImageUrl = "../icons/16_save_d.png";
                imgRetirar.ImageUrl = "../icons/eliminar_d.gif";
            }
            else
            {
                imgGuardar.ImageUrl = "../icons/16_save.png";
                imgRetirar.ImageUrl = "../icons/eliminar.gif";
            }

            txtNombre.Focus();
        }
    }

    private void ListarCompania()
    {
        Procedimientos.LlenarCombos(ddlCompania, new ClinicaCES.Logica.LRegionales().CompaniaListar(), "CODIGO", "NOMBRE");
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgGuardar))
            Guardar(ddlCompania.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick"].ToString());
        else if (sender.Equals(imgNuevo) || sender.Equals(imgCancelar))
            Response.Redirect("addRegionales.aspx");
        else if (sender.Equals(imgRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlCompania.SelectedValue, false, Session["Nick"].ToString());
        else if (sender.Equals(imgReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlCompania.SelectedValue, true, Session["Nick"].ToString());
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
            Guardar(ddlCompania.SelectedValue, txtCodigo.Text.Trim().ToUpper(), txtNombre.Text.Trim().ToUpper(), Session["Nick"].ToString());
        else if (sender.Equals(btnNuevo) || sender.Equals(btnCancelar))
            Response.Redirect("addRegionales.aspx");
        else if (sender.Equals(btnRetirar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlCompania.SelectedValue, false, Session["Nick"].ToString());
        else if (sender.Equals(btnReactivar))
            CambiarEstado(txtCodigo.Text.Trim(), ddlCompania.SelectedValue, true, Session["Nick"].ToString());
    }

    protected void lnkComprobar_Click(object sender, EventArgs e)
    {
        Consultar(ddlCompania.SelectedValue, txtCodigo.Text.Trim());
    }

    private bool ValidaGuardar()
    {
        TextBox[] txt = { txtCodigo, txtNombre };
        DropDownList[] ddl = { ddlCompania };

        return Procedimientos.ValidaGuardar(txt, ddl);
    }
}
