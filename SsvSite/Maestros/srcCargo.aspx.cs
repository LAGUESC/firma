﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcCargo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        litScript.Text = string.Empty;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de Cargos", this.Page);
            EstadoInicial();
            string script = "document.getElementById('" + imgRetirar.ClientID + "').disabled = true;";
            Procedimientos.Script(script, litScript);
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
    }

    private void Consultar()
    {
        Cargos mod = new Cargos();
        DataTable dtCargo = new ClinicaCES.Logica.LCargos().CargoConsultar(mod);
        ViewState["dtCargo"] = dtCargo;
        ViewState["dtPaginas"] = dtCargo;
        Procedimientos.LlenarGrid(dtCargo, gvCargo);
    }

    protected void gvCargo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvCargo.Columns.Count; i++)
            {
                string area = gvCargo.DataKeys[e.Row.RowIndex]["AREA"].ToString();
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addCargos.aspx?id=" + e.Row.Cells[1].Text.Trim() + "&area=" + area + "')");
                e.Row.Cells[5].Text = e.Row.Cells[5].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            }

            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvCargo.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPais, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LCargos().CargoRetirar(xmlPais, Usuario))
        {
            msg = "7";

            Cargos MEN = new Cargos();
            DataTable dtCargo = new ClinicaCES.Logica.LCargos().CargoConsultar(MEN);
            ViewState["dtCargo"] = dtCargo;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlCargo()
    {
        string area;
        string Cargo;
        string xmlCargo = "<Raiz>";

        for (int i = 0; i < gvCargo.Rows.Count; i++)
        {
            GridViewRow row = gvCargo.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                Cargo = row.Cells[1].Text.Trim();
                area = gvCargo.DataKeys[i]["AREA"].ToString();//row.Cells[3].Text.Trim();
                xmlCargo += "<Datos AREA='" + area + "' CARGO='" + Cargo + "' />";
            }
        }

        return xmlCargo += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "CODIGO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND DESCRIPCION LIKE '*" + txtBusquedaNombre.Text.Trim() + "*'";
        DataTable dtCargo = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtCargo"]);
        Procedimientos.LlenarGrid(dtCargo, gvCargo);

        ViewState["dtPaginas"] = dtCargo;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    protected void gvCargo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvCargo.PageIndex = e.NewPageIndex;
        gvCargo.DataSource = ViewState["dtPaginas"];
        gvCargo.DataBind();
    }

    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlCargo(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addCargos.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }

    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Codigo", pnlCodigo, txtBusquedaCod);
        if (sender.Equals(lnkNombreKitar))
            kitarFiltro("DESCRIPCION", "Nombre", pnlNombre, txtBusquedaNombre);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "DESCRIPCION":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}
