﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="srcAutPendientes.aspx.cs" Inherits="Maestros_srcAutPendientes" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcAutPendientes.aspx.cs" Inherits="Maestros_srcAutPendientes" Title="Autorizaciones Pendientes" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <head>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script type="text/javascript">
        $("[src*=plus]").live("click", function () {
            $(this).closest("tr").after("<tr><td></td><td colspan = '777'>" + $(this).next().html() + "</td></tr>")
            $(this).attr("src", "../img/minus.png");
        });
        $("[src*=minus]").live("click", function () {
            $(this).attr("src", "../img/plus.png");
            $(this).closest("tr").next().remove();
        });
    </script>
    </head>
<table style="width: auto">
    <tr>
        <td>
            <table align="left" style="width: 1234px">
                <tr>
                    <td>

                    </td>
                </tr>
                <tr>
                    <td>

                        <table align="center">
                            <tr>
                                <td>Filtro:</td>
                                <td>
                                    <asp:DropDownList ID="ddlFlitro" runat="server" onkeydown="tabular(event,this)">
                                        <asp:ListItem Value="1">Procedimiento</asp:ListItem>
                                        <asp:ListItem Value="2">Paciente</asp:ListItem>
                                        <asp:ListItem Value="3">Estado</asp:ListItem>
                                    </asp:DropDownList>
                                    </td>
                                </td>
                                <td>
                                    <asp:LinkButton ID="lnkAgregar" runat="server"  Text="Agregar" onclick="lnkAgregar_Click"></asp:LinkButton>
                                </td>
                                <td>

                                    <asp:ImageButton runat="server" ID="imgBuscar" ToolTip="Buscar" ImageUrl="../icons/magnify.png" OnClick="imgBuscar_Click" Enabled="False" Visible="False" />

                                </td>
                            </tr>
                            
                            <tr>
                                <asp:Panel ID="pnlProc" runat="server" Visible="false">
                                    <td>Procedimiento:</td>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtBusquedaProc" runat="server" CssClass="form_input"></asp:TextBox>
                                        <asp:LinkButton ID="lnkKitarProc" runat="server" onclick="lnkAgregar_Click" Text="Quitar"></asp:LinkButton>
                                    </td>
                                </asp:Panel>
                            </tr>
                            <tr>
                                <asp:Panel ID="pnlPaciente" runat="server" Visible="false">
                                    <td>Paciente:</td>
                                    <td colspan="2">
                                        <asp:TextBox ID="txtBusquedaPaciente" runat="server" CssClass="form_input"></asp:TextBox>
                                        <asp:LinkButton ID="lnkKitarPaciente" runat="server" onclick="lnkAgregar_Click" Text="Quitar"></asp:LinkButton>
                                    </td>
                                </asp:Panel>
                            </tr>
                            <tr>
                                <asp:Panel ID="pnlEstados" runat="server" Visible="false">
                                    <td>Estado:</td>
                                    <td colspan="2">
                                        <asp:DropDownList ID="ddlEstados" runat="server" CssClass="form_input">
                                        </asp:DropDownList>
                                        <asp:LinkButton ID="lnkKitarEstados" runat="server" onclick="lnkAgregar_Click" Text="Quitar"></asp:LinkButton>
                                    </td>
                                </asp:Panel>
                            </tr>
                        </table>

                    </td>
                </tr>
                <tr>
                    <td>
                <asp:GridView AutoGenerateColumns="False" ID="gvAsigCamas" runat="server" DataKeyNames="CAMA" CssClass="Grid"
                CellPadding="1" ForeColor="#333333" GridLines="None" Width="100%" EnableModelValidation="True" EnableTheming="True" OnRowDataBound="gvAsigCamas_RowDataBound" OnPageIndexChanging="gvAsigCamas_PageIndexChanging" PageSize="20"
                     >
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#dcdcdc" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <PagerSettings PageButtonCount="20" />
                    <PagerStyle  ForeColor="White" HorizontalAlign="Center" CssClass="cabeza" />
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <AlternatingRowStyle BackColor="White" HorizontalAlign="Left" VerticalAlign="Middle" />
                    <Columns>        
                        <asp:TemplateField>
                            <ItemTemplate>
                                <img alt = "" style="cursor: pointer" src="../img/plus.png" />
                                <asp:Panel ID="pnlOrders" runat="server" Style="display: none">
                                    <asp:GridView ID="gvOrders" runat="server" AutoGenerateColumns="false" CssClass = "ChildGrid" DataKeyNames="PAC_PAC_NUMERO" OnRowUpdating="gvOrders_RowUpdating" OnRowDataBound="gvOrders_RowDataBound" OnRowDeleting="gvOrders_RowDeleting"
                                        CellPadding="1" ForeColor="#333333" GridLines="None"  PageSize="40" Width="100%" EnableModelValidation="True" AllowPaging="True" EnableTheming="True" >
                                        
                                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                        <RowStyle BackColor="#dcdcdc" />
                                        <EditRowStyle BackColor="#2461BF" />
                                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                        <PagerStyle  ForeColor="White" HorizontalAlign="Center" CssClass="cabeza" />
                                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />
                                        <AlternatingRowStyle BackColor="White" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="imgbtnCrear" runat="server" CommandName="Delete" Visible="false" ImageUrl="~/img/add.png" />
                                            <%--<asp:LinkButton ID="Crear" Runat="server" CommandName="Delete" Visible="false">+</asp:LinkButton>--%>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <%--<asp:BoundField ItemStyle-Width="150px" DataField="CODIGO" HeaderText="CODIGO" />--%>
                                        <asp:BoundField ItemStyle-Width="150px" DataField="ORDNOMBRE" HeaderText="ESPECIALIDAD"  />
                                        <asp:BoundField ItemStyle-Width="80px" DataField="ORDNUMERO" HeaderText="ORDNUMERO" />
                                        
                                        <asp:BoundField ItemStyle-Width="80px" DataField="PRECOD" HeaderText="CODIGO" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                       
                                        <asp:BoundField ItemStyle-Width="250px" DataField="PRENOM" HeaderText="PRESTACION" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField ItemStyle-Width="250px" DataField="ID" HeaderText="IDPACIENTE" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField ItemStyle-Width="250px" DataField="MTVCORRELATIVO" HeaderText="EVENTO" ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" />
                                        <asp:BoundField ItemStyle-Width="80px" DataField="CODESTADO" HeaderText="ESTADO" Visible="false"/>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                             <ItemTemplate>
                                                 <asp:DropDownList ID="ddlEstado" runat="server" DataTextField="CODESTADO" Width="130px" Enabled="false" ></asp:DropDownList>
                                             </ItemTemplate>
                                            
                                             <HeaderTemplate>
                                                ESTADO
                                             </HeaderTemplate>
                                         </asp:TemplateField>  
                                        <asp:BoundField ItemStyle-Width="250px" DataField="OBSERVACION" HeaderText="OBSERVACION" Visible="false" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle" Visible="false">
                                             <ItemTemplate>
                                                 <asp:TextBox ID="txtObservaciones" runat="server" Text='<%# Bind("OBSERVACION") %>' Width="200px" Enabled="false" ></asp:TextBox>
                                             </ItemTemplate>
                                            
                                             <HeaderTemplate>
                                                OBSERVACIONES
                                             </HeaderTemplate>
                                         </asp:TemplateField>
                                        <asp:BoundField ItemStyle-Width="50px" HeaderText="ALERTA" Visible="false" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                             <ItemTemplate>
                                                 <asp:CheckBox ID="ckbALerta" runat="server" Enabled="false"/>
                                             </ItemTemplate>
                                             
                                             <HeaderTemplate>
                                                ALERTA
                                             </HeaderTemplate>
                                         </asp:TemplateField>
                                        <asp:BoundField ItemStyle-Width="50px" DataField="VALORALERTA" HeaderText="TIEMPO" VISIBLE ="false"/>
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                             <ItemTemplate>
                                                 <asp:TextBox ID="txtTiempo" runat="server" Text='<%# Bind("VALORALERTA") %>' Width="25px" Enabled="false"></asp:TextBox>
                                             </ItemTemplate>
                                            
                                             <HeaderTemplate>
                                                TIEMPO
                                             </HeaderTemplate>
                                         </asp:TemplateField>
                                        <asp:BoundField ItemStyle-Width="30px" DataField="CODTIEMPOALERTA" HeaderText="UNIDAD" Visible="false" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                             <ItemTemplate>
                                                 <asp:DropDownList ID="ddlUnidadTiempo" runat="server" DataTextField="CODTIEMPOALERTA"  Width="90px" Enabled="false" >
                                                     <asp:ListItem Value="0">.::No Aplica::.</asp:ListItem>
                                                     <asp:ListItem Value="1">Minutos</asp:ListItem>
                                                     <asp:ListItem Value="2">Horas</asp:ListItem>
                                                     <asp:ListItem Value="3">Dias</asp:ListItem>
                                                 </asp:DropDownList>
                                             </ItemTemplate>
                                            
                                             <HeaderTemplate>
                                                UNIDAD
                                             </HeaderTemplate>
                                         </asp:TemplateField>
                                        <asp:BoundField ItemStyle-Width="80px" DataField="FechaSolic" HeaderText="FEC SOLICITUD" />
                                        <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-VerticalAlign="Middle">
                                             <ItemTemplate>
                                                 <asp:Image ID="imgSemaforo" runat="server" Height="16px" Width="16px" />
                                             </ItemTemplate>
                                             
                                             <HeaderTemplate>
                                                
                                             </HeaderTemplate>
                                         </asp:TemplateField>  
                                        <%--<asp:BoundField ItemStyle-Width="150px" DataField="COLOR" HeaderText="COLOR" />--%>
                                        <%--<asp:TemplateField>
                                             <ItemTemplate>
                                                 
                                                 <asp:DropDownList ID="ddlResponsable" runat="server" DataTextField="RESPONSABLE" Width="155px" AutoPostBack="True" OnSelectedIndexChanged="ddlResponsable_SelectedIndexChanged" ></asp:DropDownList>
                                                 
                                             </ItemTemplate>
                                             <ItemStyle HorizontalAlign="Left" />
                             
                                             <HeaderTemplate>
                                                RESPONSABLE
                                             </HeaderTemplate>
                                         </asp:TemplateField>  --%>
                                        
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                            <asp:LinkButton ID="Editar" Runat="server" CommandName="Update">Seleccionar</asp:LinkButton>
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        
                                    </Columns>
                                    </asp:GridView>
                                </asp:Panel>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                            <ItemStyle HorizontalAlign="Left" VerticalAlign="Middle" />
                        </asp:TemplateField>  
                                                                          
                         <%--<asp:BoundField DataField="CODIGO" HeaderText="CODIGO"  />--%>
                    
                         <asp:BoundField DataField="CAMASERVICIO" HeaderText="SERVICIO - RESPONSABLE" >
                            
                         <HeaderStyle HorizontalAlign="Center" VerticalAlign="Middle" />
                        </asp:BoundField>
                         <%--<asp:BoundField DataField="ESTADO" HeaderText="Estado" Visible="false" />--%>
                       
                       
                                                                          
                    </Columns>
                </asp:GridView>
               

                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td>
        <asp:Literal ID="litScript" runat="server"></asp:Literal>
                    </td>
                    <td>&nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>                                                                                
                    <td>&nbsp;</td>                     
                    <td>
                        &nbsp;</td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
            <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender1" runat="server" 
	 okcontrolid="btnOkay" 
	targetcontrolid="txtResponsableEdit0" popupcontrolid="Panel1" 
	popupdraghandlecontrolid="PopupHeader" drag="true" DropShadow="true"
	backgroundcssclass="modalBackground">
</ajaxToolkit:ModalPopupExtender>

<asp:panel id="Panel1" runat="server" CssClass="modalPopup" Style="display: none;
                text-align: left;" Width="445px" Height="500px" BackColor="White" ForeColor="Black" BorderStyle="Solid">
	<div class="HellowWorldPopup">
               <%-- <div class="PopupHeader" id="PopupHeader"><b>Formulario de diligenciamiento de clientes</b></div>
                <div class="PopupBody">--%>
                 <table style="width: 435px"  >
            <tr>
                <td  align="center"  colspan="2" >
                    <div class="TipoInformacion">
                        <B>EDITAR</B></div>
                </td>
            </tr>
                     <tr>
                <td align="left">
                    <b>No Orden:</b></td>
                <td align="left">
                    <asp:TextBox ID="txtOrden" runat="server" Width="268px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Usuario:</b></td>
                <td align="left">
                    <asp:TextBox ID="txtResponsableEdit0" runat="server" Width="268px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
           
            <tr>
                <td align="left">
                    <b>Codigo Prestación:</b></td>
                <td align="left">
                    <asp:TextBox ID="txtCodPrestacionEdit" runat="server" Width="268px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Prestación:</b></td>
                <td align="left">
                    <asp:TextBox ID="txtPrestacionEdit" runat="server" Height="60px" TextMode="MultiLine" Width="268px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Codigo Convenio:</b></td>
                <td align="left">
                    <asp:TextBox ID="txtCodConvenioEdit" runat="server" Width="268px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Convenio:</b></td>
                <td align="left">
                    <asp:TextBox ID="txtConvenioEdit" runat="server" Height="60px" TextMode="MultiLine" Width="268px" Enabled="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Estado:</b></td>
                <td align="left">
                    <asp:DropDownList ID="ddlEstadoEdit" runat="server" Height="23px" Width="273px">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Observaciones:</b></td>
                <td align="left">
                    <asp:TextBox ID="txtObservacionesEdit" runat="server" Height="60px" TextMode="MultiLine" Width="268px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Alerta:</b></td>
                <td align="left">
                    <asp:CheckBox ID="chkAlerta" runat="server" />
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Tiempo:</b></td>
                <td align="left">
                    <%--<asp:TextBox ID="txtTiempoEdit" runat="server" Width="268px"></asp:TextBox>--%>
                    <asp:TextBox ID="txtTiempoEdit" runat="server" Width="65px" style="text-align:center" Font-Size="Small" MaxLength="3" Height="16px" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender87" runat="server" TargetControlID="txtTiempoEdit" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
                </td>
            </tr>
            <tr>
                <td align="left">
                   <b> Unidad</b></td>
                <td align="left">
                    <asp:DropDownList ID="ddlTiempoEdit" runat="server" Height="23px" Width="273px">
                        <asp:ListItem Value="0">.::No Aplica::.</asp:ListItem>
                        <asp:ListItem Value="1">Minutos</asp:ListItem>
                        <asp:ListItem Value="2">Horas</asp:ListItem>
                        <asp:ListItem Value="3">Dias</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
                      <tr>
                <td align="left">
                    &nbsp;</td>
                <td align="left">
                    <asp:Label ID="lblPac_pac_numero" runat="server" Text="Label" Visible="False"></asp:Label>
                </td>
            </tr>
                     <tr>
                <td align="center">
                    <div class="Controls" >
                    <%--<input id="btnOkay" type="button" value="Retornar" />--%>  
                        <asp:Button ID="btnOkay" runat="server" OnClick="btnRetornar_Click" Text="Retornar" />                   
		</div></td>
                <td align="center">
                    <asp:Button ID="btnGuardar" runat="server" OnClick="btnGuardar_Click" Text="Guardar" />
                         </td>
            </tr>
        </table>
                   

              
                
        </div>
</asp:panel>


    <ajaxToolkit:ModalPopupExtender id="ModalPopupExtender2" runat="server" 
	 okcontrolid="btnOk" 
	targetcontrolid="txtResponsableEdit0" popupcontrolid="Panel2" 
	popupdraghandlecontrolid="PopupHeader2" drag="true" DropShadow="true"
	backgroundcssclass="modalBackground">
</ajaxToolkit:ModalPopupExtender>

<asp:panel id="Panel2" runat="server" CssClass="modalPopup" Style="display: none;
                text-align: left;" Width="535px" Height="210px" BackColor="White" ForeColor="Black" BorderStyle="Solid">
	<div class="HellowWorldPopup">
               <%-- <div class="PopupHeader" id="PopupHeader"><b>Formulario de diligenciamiento de clientes</b></div>
                <div class="PopupBody">--%>
                 <table style="width: 300px"  >
            <tr>
                <td  align="center" colspan="2" >
                    <div class="TipoInformacion">
                        <B>AGREGAR PRESTACIÓN</B></div>
                </td>
            </tr>
            <tr>
                <td  align="center" colspan="2" >
                    &nbsp;</td>
            </tr>
        <tr>
                <td align="left">
                   <b> Especialidad:</B></td>
                <td align="left">
                    <asp:DropDownList ID="ddlEspecialidad" runat="server" Width="250px" AutoPostBack="True" OnSelectedIndexChanged="ddlEspecialidad_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
        <tr>
                <td align="left">
                    <B>Prestación:</B></td>
                <td align="left">
                    <asp:DropDownList ID="ddlPrestacion" runat="server" Width="450px" >
                    </asp:DropDownList>
                </td>
            </tr>
        <tr>
                <td align="left">
                    <B>Paciente:</B></td>
                <td align="left">
                    <asp:TextBox ID="txtPac" runat="server" Enabled="False" Width="104px"></asp:TextBox>
                </td>
            </tr>
        <tr>
                <td align="left">
                    <B>Evento</B></td>
                <td align="left">
                    <asp:TextBox ID="txtEvento" runat="server" Enabled="False" Width="104px"></asp:TextBox>
                </td>
            </tr>
        <tr>
                <td align="left" colspan="2">
                    <asp:Label ID="Label1" runat="server" Text="Label" Visible="False"></asp:Label>
                </td>
            </tr>
                     </table>
        
        <table style="width: 400px"  >
                     <tr>
                <td align="center" class="auto-style1">
                    <asp:Button ID="btnOk" runat="server" Text="Retornar" />
		        </td>
                <td align="center" class="auto-style1">
                    <asp:Button ID="GuardarPrestacion" runat="server" Text="Guardar" OnClick="GuardarPrestacion_Click" />
                </td>
            </tr>
        </table>
                   

                
                
     </div>
</asp:panel>

</asp:Content>