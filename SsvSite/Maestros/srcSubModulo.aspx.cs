﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Maestros_srcSubModulo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Procedimientos.ValidarSession(this.Page);
        MaintainScrollPositionOnPostBack = true;
        litScript.Text = string.Empty;
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Búsqueda de SubModulos", this.Page);
            EstadoInicial();
            string script = "document.getElementById('" + imgRetirar.ClientID + "').disabled = true;";
            Procedimientos.Script(script, litScript);
        }
        this.Form.DefaultButton = imgBuscar.UniqueID;
        
    }

    private void Consultar()
    {
        SubModulo mod = new SubModulo();
        DataTable dtSubModulo = new ClinicaCES.Logica.LSubModulo().SubModuloConsultar(mod);
        ViewState["dtSubModulo"] = dtSubModulo;
        ViewState["dtPaginas"] = dtSubModulo;
        Procedimientos.LlenarGrid(dtSubModulo, gvSubModulo);
    }

    protected void gvSubModulo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            //Label lblRed = (Label)e.Row.FindControl("lblRed");

            for (int i = 1; i < gvSubModulo.Columns.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("onclick", "redirect('addSubModulo.aspx?id=" + e.Row.Cells[1].Text.Trim() + "&app=" + e.Row.Cells[5].Text.Trim() + "&men=" + gvSubModulo.DataKeys[e.Row.RowIndex]["MENU"].ToString() + "&mod=" + gvSubModulo.DataKeys[e.Row.RowIndex]["MODULO"].ToString() + "')");
            }

            e.Row.Cells[6].Text = e.Row.Cells[6].Text.Replace("True", "Activo").Replace("False", "Inactivo");
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");

            CheckBox chkPais = (CheckBox)e.Row.FindControl("chkEliminar");
            bool activo = bool.Parse(gvSubModulo.DataKeys[e.Row.RowIndex]["ESTADO"].ToString());
            //vlrChk BD
            if (!activo)
                chkPais.Enabled = false;

            chkPais.Attributes.Add("onclick", "HabilitarBotonBorrarCheck('" + imgRetirar.ClientID + "',this.checked)");
        }
    }

    private void Retirar(string xmlPais, string Usuario)
    {
        string msg = "3";
        if (new ClinicaCES.Logica.LSubModulo().SubModuloRetirar(xmlPais, Usuario))
        {
            msg = "7";

            SubModulo MEN = new SubModulo();
            DataTable dtSubModulo = new ClinicaCES.Logica.LSubModulo().SubModuloConsultar(MEN);
            ViewState["dtSubModulo"] = dtSubModulo;
            Filtrar();//Filtrar(ddlFlitro.SelectedValue);
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
    }

    private string xmlSubModulo()
    {
        string SubModulo;
        string apl;
        string Menu;
        string Modulo;
        string xmlSubModulo = "<Raiz>";

        for (int i = 0; i < gvSubModulo.Rows.Count; i++)
        {
            GridViewRow row = gvSubModulo.Rows[i];
            CheckBox chkPais = (CheckBox)row.FindControl("chkEliminar");
            if (chkPais.Checked)
            {
                SubModulo = row.Cells[1].Text.Trim();
                apl = row.Cells[5].Text.Trim();
                Menu = gvSubModulo.DataKeys[i]["MENU"].ToString();
                Modulo = gvSubModulo.DataKeys[i]["MODULO"].ToString();

                xmlSubModulo += "<Datos SubModulo='" + SubModulo + "' Aplicacion='" + apl + "' Menu='" + Menu + "' Modulo='" + Modulo + "' />";
            }
        }

        return xmlSubModulo += "</Raiz>";
    }


    private void Filtrar()
    {
        string filtro = "NOMMODULO LIKE '*" + txtBusquedaCod.Text.Trim() + "*' AND SubModulo LIKE '*" + txtBusquedaNombre.Text.Trim() + "*' AND NOMMENU LIKE '*" + txtBusquedaMenu.Text.Trim() + "*' AND APLICACION LIKE '*" + txtBusquedaApp.Text.Trim() + "*'";
        DataTable dtSubModulo = Procedimientos.dtFiltrado("Orden", filtro, (DataTable)ViewState["dtSubModulo"]);
        Procedimientos.LlenarGrid(dtSubModulo, gvSubModulo);

        ViewState["dtPaginas"] = dtSubModulo;

        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Modulo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "SubModulo", pnlNombre, txtBusquedaNombre);
        if (pnlMenu.Visible)
            kitarFiltro("NOMMENU", "Menu", pnlMenu, txtBusquedaMenu);
        if (pnlAplicacion.Visible)
            kitarFiltro("APLICACION", "Aplicación", pnlAplicacion, txtBusquedaApp);
    }


    protected void gvSubModulo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvSubModulo.PageIndex = e.NewPageIndex;
        gvSubModulo.DataSource = ViewState["dtPaginas"];
        gvSubModulo.DataBind();
    }


    protected void Images_Click(object sender, ImageClickEventArgs e)
    {
        if (sender.Equals(imgRetirar))
            Retirar(xmlSubModulo(), Session["Nick"].ToString());
        else if (sender.Equals(imgBuscar))
            Filtrar();
        else if (sender.Equals(imgNuevo))
            Response.Redirect("addSubModulo.aspx");
        else if (sender.Equals(imgCancelar))
        {
            EstadoInicial();
        }
    }


    private void EstadoInicial()
    {
        if (pnlCodigo.Visible)
            kitarFiltro("CODIGO", "Modulo", pnlCodigo, txtBusquedaCod);
        if (pnlNombre.Visible)
            kitarFiltro("NOMBRE", "SubModulo", pnlNombre, txtBusquedaNombre);
        if (pnlMenu.Visible)
            kitarFiltro("NOMMENU", "Menu", pnlMenu, txtBusquedaMenu);
        if (pnlAplicacion.Visible)
            kitarFiltro("APLICACION", "Aplicación", pnlAplicacion, txtBusquedaApp);

        Consultar();
        ddlFlitro.SelectedIndex = 0;
    }


    protected void lnkAgregar_Click(object sender, EventArgs e)
    {
        if (sender.Equals(lnkAgregar))
            AgregarFiltro(ddlFlitro.SelectedValue, ddlFlitro.SelectedIndex);
        else if (sender.Equals(lnkCodigoKitar))
            kitarFiltro("CODIGO", "Modulo", pnlCodigo, txtBusquedaCod);
        else if (sender.Equals(lnkNombreKitar))
            kitarFiltro("NOMBRE", "SubModulo", pnlNombre, txtBusquedaNombre);
        else if (sender.Equals(lnkMenuKitar))
            kitarFiltro("NOMMENU", "Menu", pnlMenu, txtBusquedaMenu);
        else if (sender.Equals(lnkAppKitar))
            kitarFiltro("APLICACION", "Aplicación", pnlAplicacion, txtBusquedaApp);
    }


    private void kitarFiltro(string value, string text, Panel pnl, TextBox txt)
    {
        ddlFlitro.Items.Insert(0, new ListItem(text, value));
        pnl.Visible = false;
        txt.Text = string.Empty;
    }


    private void AgregarFiltro(string Filtro, int index)
    {
        switch (Filtro)
        {
            case "NOMBRE":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlNombre.Visible = true;
                    break;
                }
            case "NOMMENU":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlMenu.Visible = true;
                    break;
                }
            case "APLICACION":
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlAplicacion.Visible = true;
                    break;
                }
            default:
                {
                    ddlFlitro.Items.RemoveAt(index);
                    pnlCodigo.Visible = true;
                    break;
                }
        }
    }
}