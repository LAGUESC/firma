﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addEspecialidad.aspx.cs" Inherits="Maestros_addEspecialidad" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table align="center">
        <tr>
            <td>Codigo:</td>
            <td colspan="2">
                <asp:TextBox onkeypress="tabular(event,this); event.returnValue=SoloNumeros(event)" 
                    Width="20px" CssClass="form_input" onblur="comprobarRed(this.value,this.id)" 
                    ID="txtCodigo" runat="server" MaxLength="2"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtCodigo" FilterType="Numbers"></ajaxToolkit:FilteredTextBoxExtender>                                                             
            <asp:LinkButton ID="lnkComprobar" runat="server" CausesValidation="false" onclick="Click_Botones"></asp:LinkButton>
            </td>
        </tr>
        <tr>
            <td>Especialidad:</td>
            <td colspan="2">
                <asp:TextBox Width="200px" onkeypress="tabular(event,this)"  CssClass="form_input" ID="txtEspecialidad" runat="server" MaxLength="30"></asp:TextBox>
                <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="txtEspecialidad" FilterType="UppercaseLetters, LowercaseLetters" ></ajaxToolkit:FilteredTextBoxExtender>                                                             
            </td>
        </tr>
        <tr>
            <td align="center"><asp:Button ID="btnGuardar" runat="server" Text="Guardar" OnClientClick="return Mensaje(2)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>
            <td align="center"><asp:Button OnClientClick="return Mensaje(2)" ID="btnRetirar" runat="server" Text="Retirar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>
            <td align="center"><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>
        </tr>
    </table>
    
    <table align="center">
        <tr>
            <td>
                <asp:GridView AutoGenerateColumns="false" ID="gvEspecialidad" runat="server" 
                CellPadding="4" ForeColor="#333333" GridLines="None" Width="100%" 
                    onrowcreated="gvEspecialidad_RowCreated" onrowcommand="gvEspecialidad_RowCommand">
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />                
                    <PagerStyle CssClass="cabeza" ForeColor="White"  />
                    <RowStyle CssClass="normalrow" />
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle CssClass="alterrow" />
                    <Columns>        
                         <asp:TemplateField>
                             <ItemTemplate>
                                 <asp:LinkButton ID="lnkEspecialidad" runat="server" CommandName="Comprobar"></asp:LinkButton>
                                 <asp:Label ID="lblEspecialidad" runat="server" Text='<%# Bind("CODIGO") %>'></asp:Label>
                             </ItemTemplate>
                             <HeaderTemplate>
                                 Codigo
                             </HeaderTemplate>
                             <ItemStyle HorizontalAlign="Center" />
                         </asp:TemplateField>                     
                         <asp:BoundField DataField="ESPECIALIDAD" HeaderText="Especialidad" />                                                        
                    </Columns>
                </asp:GridView> 
           </td>
        </tr>
    </table>
    <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>

