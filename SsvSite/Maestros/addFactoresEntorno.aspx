﻿<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="addFactoresEntorno.aspx.cs" Inherits="Maestros_addFactoresEntorno" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server">
    </asp:ScriptManager>
    <table width="100%">
    <tr>
        <td>
            <table align="left">
                <tr>
                    <td><asp:ImageButton runat="server" ID="imgNuevo"  ToolTip="Nuevo" ImageUrl="../icons/nuevo.png" onclick="Images_Click" /></td>
                    <td><asp:ImageButton Enabled="false" runat="server" ID="imgRetirar"  ToolTip="Retirar" ImageUrl="../icons/eliminar_d.gif" onclick="Images_Click" OnClientClick="return Mensaje(11)" /></td>
                    <td><asp:ImageButton runat="server" Visible="false" ID="imgReactivar" ToolTip="Reactivar" ImageUrl="../icons/refresh.png" onclick="Images_Click"  /></td>                
                    <td><asp:ImageButton Enabled="false" runat="server" ID="imgGuardar" ToolTip="Guardar" OnClientClick="return Mensaje(13)" ImageUrl="../icons/16_save_d.png" onclick="Images_Click" /></td>                
                    <td><asp:ImageButton runat="server" ID="imgCancelar" ToolTip="Cancelar" ImageUrl="../icons/cancel.png" onclick="Images_Click" /></td>
                    <td><img onclick="redirect('srcFactoresEntorno.aspx')" src="../icons/magnify.png" id="imgBuscar" runat="server" style="cursor:pointer" /></td>
                </tr>     
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>
                    <td>Codigo:</td>                        
                    <td><asp:TextBox onkeydown="tabular(event,this)" CssClass="form_input" 
                            ID="txtCodigo" runat="server" Width="30px" MaxLength="10"></asp:TextBox>
                        <asp:LinkButton ID="lnkComprobar" runat="server" onclick="lnkComprobar_Click"></asp:LinkButton>
                    </td>
                </tr>
                <tr>
                    <td>Nombre:</td>
                    <td><asp:TextBox Enabled="false" CssClass="form_input" ID="txtNombre" 
                            runat="server" MaxLength="80" Width="350px"></asp:TextBox></td>                    
                </tr>
                <tr>
                    <td>Peso:</td>
                    <td><asp:TextBox Enabled="false" Width="30px" CssClass="form_input" ID="txtPeso" 
                            runat="server" MaxLength="3"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtPeso" FilterType="Custom, Numbers" ValidChars="-,."></ajaxToolkit:FilteredTextBoxExtender> 
                                                                                </td>                    
                </tr>
                                                                            <tr>
                    <td>Descripción:</td>
                    <td><asp:TextBox Enabled="false" TextMode="MultiLine" onkeydown="mulmaxlength(this,300)" CssClass="form_input" 
                            ID="txtDescripcion" runat="server" Height="80px" Width="350px"></asp:TextBox> 
                    </td>                    
                </tr>
                </table>
        </td>
    </tr>
    <tr>
        <td>
            <table align="center">
                <tr>                       
                    <td><asp:Button ID="btnNuevo" runat="server" Text="Nuevo" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                    <td><asp:Button Visible="false" ID="btnReactivar" runat="server" Text="Reactivar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"   /></td>
                    <td><asp:Button Enabled="false" ID="btnRetirar" runat="server" Text="Retirar" OnClientClick="return Mensaje(11)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>
                    <td><asp:Button ID="btnGuardar" Enabled="false" runat="server" Text="Guardar" OnClientClick="return Mensaje(13)" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones" /></td>                                                                                
                    <td><asp:Button ID="btnCancelar" runat="server" Text="Cancelar" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" CssClass="btn" onclick="Click_Botones"  /></td>                     
                    <td><input type="button" value="Buscar" class="btn" onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" onclick="redirect('srcFactoresEntorno.aspx')" /> </td>                     
                </tr>                    
            </table>
        </td>
    </tr>
</table>
<asp:Literal ID="litScript" runat="server"></asp:Literal>
<br />
</asp:Content>

