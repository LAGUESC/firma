﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="Maestros_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 476px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <table style="width: 435px"  >
            <tr>
                <td  align="center" colspan="2" >
                    <div class="TipoInformacion">
                        <B>AGREGAR PRESTACIÓN</B></div>
                </td>
            </tr>
            <tr>
                <td  align="center" colspan="2" >
                    &nbsp;</td>
            </tr>
        <tr>
                <td align="left">
                    Especialidad:</td>
                <td align="left">
                    <asp:DropDownList ID="ddlEspecialidad" runat="server" Width="381px" AutoPostBack="True" OnSelectedIndexChanged="ddlEspecialidad_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
        <tr>
                <td align="left">
                    Prestación:</td>
                <td align="left">
                    <asp:DropDownList ID="ddlPrestacion" runat="server" Width="381px" >
                    </asp:DropDownList>
                </td>
            </tr>
        <tr>
                <td align="left">
                    Paciente:</td>
                <td align="left">
                    <asp:TextBox ID="txtPac" runat="server" Enabled="False" Width="104px"></asp:TextBox>
                </td>
            </tr>
        <tr>
                <td align="left">
                    Evento</td>
                <td align="left">
                    <asp:TextBox ID="txtEvento" runat="server" Enabled="False" Width="104px"></asp:TextBox>
                </td>
            </tr>
        <tr>
                <td align="left" colspan="2">
                    <asp:Label ID="lblPac_pac_numero" runat="server" Text="Label" Visible="False"></asp:Label>
                </td>
            </tr>
                     </table>
        
        <table style="width: 486px"  >
                     <tr>
                <td align="center" class="auto-style1">
                    <asp:Button ID="Button2" runat="server" Text="Button" />
		        </td>
                <td align="center" class="auto-style1">
                    <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click" />
                </td>
            </tr>
        </table>



    </div>
    </form>
</body>
</html>
