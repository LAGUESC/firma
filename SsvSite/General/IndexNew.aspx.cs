﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class General_IndexNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            TotalConsentimientos();
        }
    }

    protected void TotalConsentimientos()
    {
        DataTable dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_COUNT();
        DataRow row = dt.Rows[0];
        lblConsentimientos.Text = row["TOTAL"].ToString();

        decimal Resma = Convert.ToInt16(lblConsentimientos.Text) / Convert.ToDecimal(500);
        lblResmas.Text = Resma.ToString();

        decimal AhorroImpresion = Convert.ToInt16(lblConsentimientos.Text) * Convert.ToDecimal(60);
        lblDineroAhorro.Text = "$"+" "+ AhorroImpresion.ToString();

        DataTable dtMes = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_COUNT_MES();
        DataRow rowMes = dtMes.Rows[0];
        lblConsentimientosMes.Text = rowMes["ConsentimientosMes"].ToString();
    }


}