﻿using System;
using System.Data;
using System.Web;
using System.Web.UI;


public partial class General_LogIn : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (this.IsPostBack)
        {

            lblDispositivo.Text = Request.Form[hfName.UniqueID];
            if (lblDispositivo.Text != "")
            {
                Session["Dispositivo"] = lblDispositivo.Text;
            }
        }
    }

    protected void btnIniciar_Click(object sender, EventArgs e)
    {
        IniciarSession(txtUsuario.Text.Trim(), Encripcion.clsEncriptar.Encriptar(txtPass.Text.Trim().ToUpper()));

    }
    private void IniciarSession(string usr, string contrasena)
    {
        DataSet ds = new ClinicaCES.Logica.LUsuarios().InicioSesion(usr);
        DataTable dtInforme = ds.Tables[0];
        DataTable dtPagina = Procedimientos.dtFiltrado("IDENTIFICACION", "", dtInforme);
        string[] campo = { "IDENTIFICACION" };
        if (dtInforme.Rows.Count > 0)
        {
            DataRow row = dtInforme.Rows[0];
//           
            string Pass = Encripcion.clsEncriptar.Encriptar(row["Pass"].ToString().Trim().ToUpper());

            string TipoIdentificacion = row["TipoId"].ToString();
            string Identificacion = row["Identificacion"].ToString();
            string Nombre = row["Nombre"].ToString();
            string Cargo = row["Cargo"].ToString();
            string Registro = row["Registro"].ToString();
            string Nick = usr.Trim().ToUpper();

            if (Pass != contrasena)
            {
                Interno.InnerHtml = "Usuario y/o Clave Incorrectos";
                warning.Visible = true;
                txtUsuario.Focus();
            }
            else {
                Session["TipoId"] = TipoIdentificacion;
                Session["Identificacion"] = Identificacion;
                Session["Nombre"] = Nombre;
                Session["Cargo"] = Cargo;
                Session["Registro"] = Registro;
                Session["Nick"] = Nick;

            Response.Cookies.Remove("SIDGV1");
                HttpCookie SidgvCookie;
                if (Request.Cookies["SIDGV1"] == null)
                {
                    SidgvCookie = new HttpCookie("SIDGV1");
                }
                else
                {
                    SidgvCookie = Request.Cookies["SIDGV1"];
                }

                SidgvCookie["TipoId"] = TipoIdentificacion;
                SidgvCookie["Identificacion"] = Identificacion;
                SidgvCookie["Nombre"] = Nombre;
                SidgvCookie["Cargo"] = Cargo;
                SidgvCookie["Registro"] = Registro;
                SidgvCookie["Nick"] = Nick;

                SidgvCookie.Expires = DateTime.Now.AddDays(10d);
                Response.Cookies.Add(SidgvCookie);

                if (!string.IsNullOrEmpty(Request.QueryString["redirect"]))
                    Response.Redirect(Request.QueryString["redirect"]);

                Response.Redirect("IndexNew.aspx");
            }
        }
            else
            {
                Interno.InnerHtml = "Usuario y/o Clave Incorrectos";
                warning.Visible = true;
                txtUsuario.Focus();
            }
        }

    }

