﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Movimientos_addAsunto : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        MaintainScrollPositionOnPostBack = true;        
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Asunto", this.Page);
            Procedimientos.ValidarSession(this.Page);
            ListarClientes();
            ListarAsunto();
            hdnHoraInicial.Value = DateTime.Now.ToShortTimeString();

            if (Request.QueryString["id"] != null)
            {
                Consultar(Request.QueryString["id"]);
            }
        }

        litScript.Text = string.Empty;
    }

    private void ListarClientes()
    {
        Procedimientos.LlenarCombos(ddlCliente, new ClinicaCES.Logica.LClientes().ClienteConsultar(), "NIT", "NOMCOMCIAL");       
    }

    private void ListarReporta(string nit,string negocio)
    {
        Procedimientos.LlenarCombos(ddlReporto, new ClinicaCES.Logica.LClientes().UsuarioReporta(nit, negocio), "USUARIO", "NOMBRE");
        Procedimientos.LlenarCombos(ddlConsulto, new ClinicaCES.Logica.LClientes().UsuarioReporta(nit, negocio), "USUARIO", "NOMBRE");
        Procedimientos.LlenarCombos(ddlRecibio, new ClinicaCES.Logica.LClientes().UsuarioReporta(nit, negocio), "USUARIO", "NOMBRE");        
    }

    private void ListarAppXClie(string nit, string negocio)
    {
        AppXCli axc = new AppXCli();
        axc.Nit = nit;
        axc.Negocio = negocio;
        Procedimientos.LlenarCombos(ddlAplicacion, new ClinicaCES.Logica.LAppXCli().AppXCliBuscar(axc), "IDAPLICACION", "NOMBRE");                
    }


    private void Guardar
    (
        string horaini,
        string idcliente,
        string negocio,
        string idusuario,
        string telefono,
        string email,
        string idaplicacion,
        string idmenu,
        string idmodulo,
        string idsubmodulo,
        string version,
        string asunto,
        string idestado,
        string idencargado,
        string solicita,
        string idatendio,
        string idclasificacion,
        string idprioridad,
        string horagra,
        string IdAsunto,
        string Descripcion,

        string idEstado,
        string idUsuClie,
        string Informacion ,
        string HoraIni,
        string HoraFin,
        bool? Recibieron ,
        string FRecibieron ,
        string IdUsuarioRecibio
    )
    { 
        Asunto asu = new Asunto();

        string msg = "3";

        asu.horaini = horaini;
        asu.idcliente = idcliente;
        asu.negocio = negocio;
        asu.idusuario = idusuario;
        asu.telefono = telefono;
        asu.email = email;
        asu.idaplicacion = idaplicacion;
        asu.idmenu = idmenu;
        asu.idmodulo = idmodulo;
        asu.idsubmodulo = idsubmodulo;
        asu.version = version;
        asu.asunto = asunto;
        asu.idestado = idestado;
        asu.idencargado = idencargado;
        asu.solicita = solicita;
        asu.idatendio = idatendio;
        asu.idclasificacion = idclasificacion;
        asu.idprioridad = idprioridad;
        asu.horagra = horagra;
        asu.IdAsunto = IdAsunto;
        asu.Descripcion = Descripcion;

        

        IdAsunto = new ClinicaCES.Logica.LAsunto().AsuntoActualizar(asu);
        if (!string.IsNullOrEmpty(IdAsunto))
        {
            if (Request.QueryString["id"] != null)
                IdAsunto = Request.QueryString["id"];
            hdnIdAsunto.Value = IdAsunto;
            lblCodigo.Text = IdAsunto;
            msg = "1";

            if (pnlDetalle.Visible)
            {
                asu.idEstado = idEstado;//ddlEstado.SelectedValue
	           // asun.idEncargado = 
	            asu.idUsuClie = idUsuClie; //ddlConsulto.SelectedValue;
	            asu.Informacion = Informacion;//txtInformacion
	            //asun.IdUsuario = idatendio;
	            //asun.Fecha ,
	            asu.HoraIni = HoraIni;//hdnHoraInicialDetalle.Value;
	            asu.HoraFin = HoraFin; //DateTime.Now.ToShortTimeString();
	            asu.Recibieron = Recibieron;//chkRecibido.Checked;
	            asu.FRecibieron = FRecibieron;//txtFecha.Text;
	            asu.IdUsuarioRecibio = IdUsuarioRecibio;//ddlRecibio.SelectedValue
                if (!new ClinicaCES.Logica.LAsunto().AsuntoDetalleActualizar(asu))
                {
                    msg = "3";
                }


            }

            
        }

        Procedimientos.Script("Mensaje(" + msg + "); redirect('addAsunto.aspx?id=" + IdAsunto + "')", litScript);

    }

    private void ListarAsunto()
    {
        Procedimientos.LlenarCombos(ddlAsunto, new ClinicaCES.Logica.LAsunto().AsuntoListar(), "IDCLASUNTO", "CLASUNTO");
        Procedimientos.LlenarCombos(ddlManosde, new ClinicaCES.Logica.LAsunto().EncargadoListar(), "USUARIO", "NOMBRE");
        Procedimientos.LlenarCombos(ddlPrioridad, new ClinicaCES.Logica.LAsunto().PrioridadListar(), "IDPRIORIDAD", "PRIORIDAD");
        Procedimientos.LlenarCombos(ddlClasificacion, new ClinicaCES.Logica.LAsunto().ClasificacionListar(), "IDCLASIFICACION", "CLASIFICACION");
        Procedimientos.LlenarCombos(ddlEstado, new ClinicaCES.Logica.LAsunto().EstadosListar(), "IDESTADO", "DESCESTADO");
        Procedimientos.comboEstadoInicial(ddlAplicacion);
        Procedimientos.comboEstadoInicial(ddlMenu);
        Procedimientos.comboEstadoInicial(ddlModulo);
        Procedimientos.comboEstadoInicial(ddlReporto);
        Procedimientos.comboEstadoInicial(ddlSubModulo);
        //Procedimientos.comboEstadoInicial(ddlVersion);

        ddlManosde.SelectedValue = Session["Nick1"].ToString();



    }

    protected void ddlCliente_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCliente.SelectedIndex != 0)
        {
            string[] cliente = ddlCliente.SelectedValue.Split('|');
            ListarReporta(cliente[0], cliente[1]);
            ListarAppXClie(cliente[0], cliente[1]);
        }
    }

    private void ListarMenu(string App)
    {
        MMenu men = new MMenu();
        men.Aplicacion = App;
        Procedimientos.LlenarCombos(ddlMenu, new ClinicaCES.Logica.LMenu().MenuConsultar(men), "CODIGO", "MENU");                
    }

    private void ListarModulo(string App)
    {
        Modulo men = new Modulo();
        men.Aplicacion = App;
        Procedimientos.LlenarCombos(ddlModulo, new ClinicaCES.Logica.LModulo().ModuloConsultar(men), "CODIGO", "MODULO");                
        
    }

    private void ListarSubModulo(string Modulo)
    {
        SubModulo sub = new SubModulo();
        sub.Mod = Modulo;
        Procedimientos.LlenarCombos(ddlSubModulo, new ClinicaCES.Logica.LSubModulo().SubModuloConsultar(sub), "CODIGO", "SUBMODULO");                
        
    }

    protected void ddlAplicacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlAplicacion.SelectedIndex != 0)
        {
            ListarMenu(ddlAplicacion.SelectedValue);
            ListarModulo(ddlAplicacion.SelectedValue);
        }
    }
    protected void ddlModulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlModulo.SelectedIndex != 0)
        {
            ListarSubModulo(ddlModulo.SelectedValue);
        }
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(btnGuardar))
        { 
            string idAsunto = null;

            if (!string.IsNullOrEmpty(hdnIdAsunto.Value))
                idAsunto = hdnIdAsunto.Value;

            string[] cliente = ddlCliente.SelectedValue.Split('|');
            Guardar(hdnHoraInicial.Value, cliente[0], cliente[1], ddlReporto.SelectedValue, txtTelefono.Text.Trim(),
                txtEmail.Text.Trim(), ddlAplicacion.SelectedValue, ddlMenu.SelectedValue, ddlModulo.SelectedValue,
                ddlSubModulo.SelectedValue, null /*ddlVersion.SelectedValue*/, ddlAsunto.SelectedValue, null/*estado*/,
                ddlManosde.SelectedValue/*encargado*/, txtSolicitamos.Text.Trim()/*solicita*/, Session["Nick1"].ToString(), ddlClasificacion.SelectedValue, ddlPrioridad.SelectedValue,
                DateTime.Now.ToShortTimeString(), idAsunto,txtDescripcion.Text.Trim()
                , ddlEstado.SelectedValue, ddlConsulto.SelectedValue, txtInformacion.Text.Trim(), hdnHoraInicialDetalle.Value,
                DateTime.Now.ToShortTimeString(), chkRecibido.Checked, txtFecha.Text.Trim(), ddlRecibio.SelectedValue
                );
        }
    }

    private void Consultar(string idAsun)
    {
        DataSet dsAsunto = new ClinicaCES.Logica.LAsunto().AsuntoConsultar(idAsun);

        DataTable dtAsunto = dsAsunto.Tables[0];
        DataTable dtDetalle = dsAsunto.Tables[1];

        if (dtAsunto.Rows.Count > 0)
        {
            DataRow row = dtAsunto.Rows[0];
            lblCodigo.Text = row["IDASUNTO"].ToString();
            hdnIdAsunto.Value = row["IDASUNTO"].ToString();
            ddlCliente.SelectedValue = row["CLIENTE"].ToString();

            string[] cliente = ddlCliente.SelectedValue.Split('|');
            ListarReporta(cliente[0], cliente[1]);
            ListarAppXClie(cliente[0], cliente[1]);

            ddlReporto.SelectedValue = row["IDUSUARIO"].ToString();

            ConsultarDatosUsuario(ddlReporto.SelectedValue);

            ddlAplicacion.SelectedValue = row["IDAPLICACION"].ToString();

            ListarMenu(ddlAplicacion.SelectedValue);
            ListarModulo(ddlAplicacion.SelectedValue);

            ddlMenu.SelectedValue = row["IDMENU"].ToString();
            ddlModulo.SelectedValue = row["IDMODULO"].ToString();

            ListarSubModulo(ddlModulo.SelectedValue);

            ddlSubModulo.SelectedValue = row["IDSUBMODULO"].ToString();
            ddlPrioridad.SelectedValue = row["IDPRIORIDAD"].ToString();
            ddlAsunto.SelectedValue = row["ASUNTO"].ToString();
            ddlClasificacion.SelectedValue = row["IDCLASIFICACION"].ToString();
            txtDescripcion.Text = row["DESCRIPCION"].ToString();
            ddlManosde.SelectedValue = row["IDENCARGADO"].ToString();
            txtSolicitamos.Text = row["SOLICITA"].ToString();
            hdnHoraInicial.Value = row["HORAINI"].ToString();

            pnlDetalle.Visible = true;

            lblFecha.Text = row["FECHA"].ToString();
            lblHora.Text = row["HORAINI"].ToString() + " - " + row["HORAFIN"].ToString();

            hdnHoraInicialDetalle.Value = DateTime.Now.ToShortTimeString();

            if (dtDetalle.Rows.Count > 0)
            {
                Procedimientos.LlenarGrid(dtDetalle, gvDetalle);
            }

        }
    }

    private void ConsultarDatosUsuario(string usr)
    { 
        Usuarios user = new Usuarios();
        user.Usuario = usr;
        DataTable dt = new ClinicaCES.Logica.LUsuarios().UsuarioConsultar(user);

        if (dt.Rows.Count > 0)
        {
            txtTelefono.Text = dt.Rows[0]["TELEFONO"].ToString();
            txtEmail.Text = dt.Rows[0]["EMAIL"].ToString();
        }
    }

    protected void ddlReporto_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlReporto.SelectedIndex != 0)
            ConsultarDatosUsuario(ddlReporto.SelectedValue);
        else
        {
            txtEmail.Text = string.Empty;
            txtTelefono.Text = string.Empty;
        }
        
    }
    protected void gvDetalle_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        { 
            string info = e.Row.Cells[3].Text;
            if (info.Length > 100)
            {
                e.Row.Cells[3].Text = e.Row.Cells[3].Text.Substring(0, 100) + " ...";
                e.Row.ToolTip = info;
            }
        }
    }
}