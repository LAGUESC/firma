﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/MasterNew.master" AutoEventWireup="true" CodeFile="IndexNew.aspx.cs" Inherits="General_IndexNew" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <section class="content-header">
        <h1>¡ Bienvenido !&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Clínica CES&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <small>La educación es el arma más poderosa que puedes usar para cambiar el mundo.&nbsp; - Nelson Mandela.</small>
        </h1>
    </section>
    <section class="content">
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>
                            <asp:Label ID="lblConsentimientos" runat="server" Text="0"></asp:Label></h3>

                        <p>Total consentimientos</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-folder-open"></i>
                    </div>
                    <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            <asp:Label ID="lblConsentimientosMes" runat="server" Text="0"></asp:Label></h3>

                        <p>Consentimientos mes actual</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-calendar"></i>
                    </div>
                    <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            <asp:Label ID="lblDineroAhorro" runat="server" Text="0"></asp:Label></h3>
                        <p>Ahorro impresiones</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-money"></i>
                    </div>
                    <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            <asp:Label ID="lblResmas" runat="server" Text="0"></asp:Label></h3>

                        <p>Ahorro resmas de papel</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-usd"></i>
                    </div>
                    <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
            </div>
            <!-- ./col -->
        </div>
</section>
    <!--div slider-->
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
            </div>
        </div>
        <div class="col-md-8">
            <div class="box box-solid">
                <div class="box-body">
                    <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                            <li data-target="#carousel-example-generic" data-slide-to="1" class=""></li>
                            <li data-target="#carousel-example-generic" data-slide-to="2" class=""></li>
                        </ol>
                        <div class="carousel-inner">
                            <div class="item active">
                                <img src="../img/banners-ppales-enero-01.jpg" alt="First slide">
                                <div class="carousel-caption">
                                    First Slide
                                </div>
                            </div>
                            <div class="item">
                                <img src="../img/banners-ppales-enero-02.jpg" alt="Second slide">

                                <div class="carousel-caption">
                                    Second Slide
                                </div>
                            </div>
                            <div class="item">
                                <img src="../img/banners-ppales-enero-03.jpg" alt="Third slide">

                                <div class="carousel-caption">
                                    Third Slide
                                </div>
                            </div>
                        </div>
                        <a class="left carousel-control" href="#carousel-example-generic" data-slide="prev">
                            <span class="fa fa-angle-left"></span>
                        </a>
                        <a class="right carousel-control" href="#carousel-example-generic" data-slide="next">
                            <span class="fa fa-angle-right"></span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
            </div>
        </div>
    </div>
    <!--div historia-->
    <div class="row">
        <div class="col-md-2">
            <div class="form-group">
            </div>
        </div>
        <div class="col-md-8">
            <div class="form-group">
                <h2>Historia</h2>
                <p>
                    En junio de 1980 la Clínica CES nació como una entidad sin ánimo de lucro comprometida con la excelencia, producto de un sueño creado a partir de una idea apasionada. Desde sus inicios, su propósito fue brindar servicios de salud con alta calidad humana, ética y científica, articulando la Docencia – Servicio en la atención.
                    Los comisionados por el CES para realizar la adquisición de la Clínica fueron el Dr. Hernán Vélez Atehortúa y el Dr. Luis Carlos Muñoz Uribe, bajo la rectoría del Dr. Gonzalo Calle Vélez, y por parte de la Caja de Compensación COMFAMA el Dr. Oscar Uribe, el Dr. Luis Enrique Echeverri U. y el Dr. Ramiro Mejía. 
                    Su visión y su mandato misional se fueron enriqueciendo a medida que todo el equipo de trabajo se comprometía con el propósito de contar con un campo de práctica propio para la Facultad de Medicina de la Universidad CES, que recién iniciaba en el año 1977. 
                    La Clínica ha logrado un crecimiento significativo en recurso humano, espacio físico, dotación y renovación tecnológica, y gracias a esto hoy cuenta con un importante reconocimiento social.
                </p>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
            </div>
        </div>
    </div>
</asp:Content>

