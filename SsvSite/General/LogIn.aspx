﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="LogIn.aspx.cs" Inherits="General_LogIn" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>CES | Login</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport" />
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="../bootstrap/css/bootstrap.min.css" />
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />
    <!-- Theme style -->
    <%--<link rel="stylesheet" href="../dist/css/AdminLTE.min.css" />--%>
    <link href="../dist/css/mediaquery.css" rel="stylesheet" />
    <!-- iCheck -->
    <link rel="stylesheet" href="../plugins/iCheck/square/blue.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->
</head>
<body class="hold-transition login-page">
    <div class="login-box">
        <!-- /.login-logo -->
        <div class="login-box-body" >
            <asp:Image ID="Image2" runat="server" Width="360" Height="250" ImageUrl="~/img/CES.png" CssClass="img-responsive" />
            <form id="form1" runat="server">
                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtUsuario" runat="server" class="form-control" placeholder="Usuario"></asp:TextBox>
                    <span class="glyphicon glyphicon-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <asp:TextBox ID="txtPass" runat="server" class="form-control" placeholder="Password" TextMode="Password"></asp:TextBox>
                    <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                </div>
                <div class="row">
                    
                    <!-- /.col -->
                    <div class="col-xs-12" style="height: 34px">
                        <asp:Button ID="btnIniciar" runat="server" class="btn btn-primary btn-block btn-flat" Text="Entrar" OnClick="btnIniciar_Click" />
                    </div>
                    <!-- /.col -->
                </div>
                <asp:Label ID = "lblDispositivo" runat="server" ForeColor="White" />
                        <asp:HiddenField ID ="hfName" runat="server"/>
                <br />
                <div id="warning" class="alert alert-warning alert-dismissible" visible="false" runat="server">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                        &times;</button>
                    <h4><i class="icon fa fa-warning"></i>Advertencia!</h4>
                    <div id="Interno" runat="server">
                    </div>
                </div>
            </form>
        </div>
        <!-- /.login-box-body -->
    </div>
    <!-- /.login-box -->
    <!-- jQuery 2.2.0 -->
    <script src="<%= Page.ResolveUrl("~/plugins/jQuery/jQuery-2.2.0.min.js") %>"></script>
    <!-- Bootstrap 3.3.6 -->
    <script src="<%= Page.ResolveUrl("~/bootstrap/js/bootstrap.min.js") %>"></script>
    <!-- iCheck -->
    <script src="<%= Page.ResolveUrl("~/plugins/iCheck/icheck.min.js") %>"></script>
    <script type="text/javascript">
        if (
            window.addEventListener('touchstart', function () {
                var label = document.getElementById("<%=lblDispositivo.ClientID %>");

                //Set the value of Label.
                label.innerHTML = "TABLET";

                document.getElementById("<%=hfName.ClientID %>").value = label.innerHTML;
                //document.write("your device is a touch device");
            }));
        else {
            var label = document.getElementById("<%=lblDispositivo.ClientID %>");

                //Set the value of Label.
                label.innerHTML = "DESKTOP";

                document.getElementById("<%=hfName.ClientID %>").value = label.innerHTML;

        }
        </script>
    <script>
        $(function () {
            $('input').iCheck({
                checkboxClass: 'icheckbox_square-blue',
                radioClass: 'iradio_square-blue',
                increaseArea: '20%' // optional
            });
        });
    </script>
</body>
</html>
