﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="srcCerrarAnexo.aspx.cs" Inherits="Anexos_srcCerrarAnexo" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="srcCerrarAnexo.aspx.cs" Inherits="Anexos_srcCerrarAnexo" Title="Busqueda paciente para generar Anexo1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <br />
    <table align="center" style="width: 328px">
        <tr>
            <td align="left">
                No Anexo:</td>
            <td align="left">
                <asp:TextBox ID="txtId" runat="server" Width="143px" OnTextChanged="txtId_TextChanged" AutoPostBack="True"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center">
                <%--<asp:Button ID="btnConsultar" runat="server" Text="Consultar" 
                    onmouseover="this.className='btnhov'" onmouseout="this.className='btn'" 
                    CssClass="btn" onclick="Click_Botones" />--%>
            </td>
        </tr>
    </table>
    
    <table align="center">
        <tr>
            <td align="center">
    <asp:Panel ID="pnlInforme" runat="server" Visible="false">
    
        <table border="1">
        </table>
        <table align="center">
            <tr>
                <td><asp:GridView AutoGenerateColumns="False" ID="gvAnexos" runat="server" DataKeyNames="ANEXO" 
                CellPadding="4" ForeColor="#333333" GridLines="None"  PageSize="20" Width="100%" EnableModelValidation="True"  AllowPaging="True" EnableTheming="True" OnRowUpdating="gvAnexos_RowUpdating" 
                     >
                    <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#dcdcdc" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle  ForeColor="White" HorizontalAlign="Center" CssClass="cabeza" />
                    <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>                            
                         <asp:BoundField DataField="ANEXO" HeaderText="ANEXO"  />
                         <asp:BoundField DataField="TIPOANEXO" HeaderText="TIPO ANEXO" />
                         <asp:BoundField DataField="IDPACIENTE" HeaderText="ID PACIENTE" />
                         <asp:BoundField DataField="PACIENTE" HeaderText="PACIENTE" />
                         <%--<asp:BoundField DataField="ESTADO" HeaderText="Estado" Visible="false" />--%>
                        
                                                                          
                        <asp:TemplateField>
<ItemTemplate>
<asp:LinkButton ID="Editar" Runat="server" OnClientClick="return confirm('¿Desea actualizar el registro?');" CommandName="Update">Finalizar</asp:LinkButton>
</ItemTemplate>
</asp:TemplateField> 
                       
                                                                          
                    </Columns>
                </asp:GridView></td>
            </tr>
        </table>
    </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblDescripcion" runat="server"></asp:Label>
            </td>
        </tr>
    </table>
        <asp:Literal ID="litScript" runat="server"></asp:Literal>
</asp:Content>