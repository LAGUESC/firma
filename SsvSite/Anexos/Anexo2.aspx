﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="Anexo2.aspx.cs" Inherits="Anexos_Anexo2" Title="Anexo 2" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
           <table style="width: 1003px; height: 96px;">
    <tr>
        <td rowspan="5" style="width: 63px">
            <asp:Image ID="Image1" runat="server" Height="77px" ImageUrl="~/img/escudocol.png" Width="102px" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="6"><B>MINISTERIO DE LA PROTECCION SOCIAL</B></td>
    </tr>
    <tr>
        <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" colspan="6"><B><font SIZE=4>INFORME DE LA ATENCION INICIAL DE URGENCIA</font></B></td>
    </tr>
    <tr>
        <td align="center" style="width: auto"><b>NUMERO ATENCION</b></td>
        <td align="center" style="width: auto; margin-left: 40px;">
            <asp:TextBox ID="TextBox1" runat="server" Width="104px" style="text-align:center" Font-Size="Small" CssClass="form_input" Enabled="False" ></asp:TextBox>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
        </td>
        <td align="center" style="width: auto"><b>Fecha:</b></td>
        <td align="center" style="width: auto">
            <asp:TextBox ID="TextBox10" runat="server" Width="125px" style="text-align:center" Font-Size="Small" CssClass="form_input" Enabled="False"></asp:TextBox>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
        </td>
        <td align="center" style="width: auto"><b>Hora:</b></td>
        <td align="center" style="width: auto">
            <asp:TextBox ID="TextBox20" runat="server" Width="67px" style="text-align:center" Font-Size="Small" CssClass="form_input" Enabled="False" ></asp:TextBox>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>  </td>
    </tr>
</table>
   
    <table style="width: 975px">
    <tr>
        <td colspan="6"><b>INFORMACION DEL PRESTADOR</b></td>
    </tr>
    <tr>
        <td style="width: auto"><b>Nombre</b></td>
        <td><b>NIT</b></td>
        <td>
            <asp:TextBox ID="TextBox25" runat="server" Width="16px" ReadOnly="True" Font-Underline="False" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">X</asp:TextBox>
        </td>
        <td>&nbsp;&nbsp;&nbsp;</td>
        <td colspan="2">
            <asp:TextBox ID="TextBox26" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox27" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">9</asp:TextBox>
            <asp:TextBox ID="TextBox28" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox29" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">9</asp:TextBox>
            <asp:TextBox ID="TextBox30" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox31" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox32" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">6</asp:TextBox>
            <asp:TextBox ID="TextBox33" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox34" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox35" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox36" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">-</asp:TextBox>
            <asp:TextBox ID="TextBox37" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBox38" runat="server" Enabled="False" ReadOnly="True" Width="548px" CssClass="form_input">CORPORACION PARA ESTUDIOS EN SALUD CLINICA CES</asp:TextBox>
        </td>
        <td><b>CC</b></td>
        <td>
            <asp:TextBox ID="TextBox39" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input" Enabled="False"></asp:TextBox>
            </td>
        <td>&nbsp;</td>
        <td>Numero:</td>
        <td>
            <asp:TextBox ID="TextBox41" runat="server" Width="226px" CssClass="form_input" Enabled="False"></asp:TextBox>
            <asp:Label ID="Label7" runat="server" Text="DV" Font-Bold="False" Font-Size="Medium"></asp:Label>
        </td>
    </tr>
</table>
  
    <table style="width: 975px">
    <tr>
        <td style="width: 64px"><b>Codigo:</b></td>
        <td style="width: 388px">
            <asp:TextBox ID="TextBox42" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox43" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">5</asp:TextBox>
            <asp:TextBox ID="TextBox44" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox45" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox46" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
            <asp:TextBox ID="TextBox47" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox48" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox49" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
            <asp:TextBox ID="TextBox50" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox51" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">4</asp:TextBox>
            <asp:TextBox ID="TextBox52" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox53" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            </td>
        <td><b>Dirección Prestador:</b></td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td rowspan="2" style="width: 64px"><b>Teléfono:</b></td>
        <td style="width: 239px">
            <asp:TextBox ID="TextBox54" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">4</asp:TextBox>
            <asp:TextBox ID="TextBox55" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox56" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox57" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox58" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            </td>
        <td style="width: 359px">
            <asp:TextBox ID="TextBox59" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">5</asp:TextBox>
            <asp:TextBox ID="TextBox60" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox61" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">6</asp:TextBox>
            <asp:TextBox ID="TextBox62" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox63" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox64" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox65" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            </td>
        <td colspan="9">
            <asp:TextBox ID="TextBox66" runat="server" Width="541px" CssClass="form_input" Enabled="False">CALLE 58 # 50C-2 PRADO CENTRO</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 239px" align="center">Indicativo</td>
        <td style="width: 359px" align="center">Número</td>
        <td style="width: auto"><b>Departamento:</b></td>
        <td>
            <asp:TextBox ID="TextBox67" runat="server" Enabled="False" ReadOnly="True" Width="130px" CssClass="form_input">ANTIOQUIA</asp:TextBox>
        </td>
        <td bgcolor="Gray">
            &nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td><b>Municipio:</b></td>
        <td>
            <asp:TextBox ID="TextBox68" runat="server" Enabled="False" ReadOnly="True" Width="130px" CssClass="form_input">MEDELLIN</asp:TextBox>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
    </tr>
    </table>

    <table style="width: 975px">
    <tr>
        <td style="height: 26px;"><b>ENTIDAD A LA QUE SE LE SOLICITA (Pagador)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
        <td style="height: 26px">&nbsp;<b>CODIGO:</b></td>
        <td style="height: 26px">
            <asp:TextBox ID="TextBox69" runat="server" Width="99px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input" Enabled="False">05001021240</asp:TextBox>
            <%--            <asp:TextBox ID="TextBox2" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox3" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox4" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox5" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox6" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox7" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox8" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox9" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
    </tr>
    </table>
   
    <table style="width: 975px">
    <tr>
        <td colspan="8" align="center"><B>DATOS DEL PACIENTE</b></td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBox75" runat="server" Width="233px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBox76" runat="server" Width="233px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td colspan="3">
            <asp:TextBox ID="TextBox77" runat="server" Width="233px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td colspan="3">
            <asp:TextBox ID="TextBox78" runat="server" Width="233px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px"><b>Primer Apellido</b></td>
        <td style="height: 23px"><b>Segundo Apellido</b></td>
        <td style="height: 23px" colspan="3"><b>Primer Nombre</b></td>
        <td style="height: 23px" colspan="3"><b>Segundo Nombre</b></td>
    </tr>
    <tr>
        <td colspan="8"><b>Tipo Documento de Identificación</b></td>
    </tr>
    <tr>
        <td colspan="2" rowspan="4">
            <asp:RadioButtonList ID="rblTipoId" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Width="473px" CssClass="form_input" Enabled="False">
                <asp:ListItem Value="2">Registro Civil</asp:ListItem>
                <asp:ListItem Value="P">Pasaporte</asp:ListItem>
                <asp:ListItem Value="3">Tarjeta de Identidad</asp:ListItem>
                <asp:ListItem Value="A">Adulto sin Identificación</asp:ListItem>
                <asp:ListItem Value="4">Cédula de Ciudadania</asp:ListItem>
                <asp:ListItem Value="M">Menor sin Idenficaci&#243;n</asp:ListItem>
                <asp:ListItem Value="5">Cédula de Extranjería</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        <td>&nbsp;</td>
        <td colspan="5">
            <asp:TextBox ID="TextBox81" runat="server" Width="286px"  Font-Size="Small" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="txtDocOrigen_FilteredTextBoxExtender" runat="server" TargetControlID="TextBox81" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--            <asp:TextBox ID="TextBox11" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox12" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox13" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label3" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox14" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox15" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label4" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox16" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox17" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
       
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center" colspan="3">Número Documento de Identificación</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><b>Fecha de Nacimiento:</b></td>
        <td colspan="4">
            <asp:TextBox ID="TextBox102" runat="server" Width="141px" style="text-align:center" Font-Size="Small" MaxLength="10" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="txtFechaOrigen_FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox102"  FilterType="Numbers, Custom"  ValidChars="/"  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--      <asp:TextBox ID="TextBox21" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
             <asp:Label ID="Label5" runat="server" Text=":" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox23" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox24" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
      --%>
            </td>
    </tr>
    <tr>
        <td colspan="2">
            &nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="4">
            &nbsp;</td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td><b>Dirección de Residencia Habitual:</b></td>
        <td>
            <asp:TextBox ID="TextBox112" runat="server" Width="419px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td><b>Teléfono:</b></td>
        <td>
            <asp:TextBox ID="TextBox113" runat="server" Width="152px"  Font-Size="Small" MaxLength="10" CssClass="form_input" Enabled="False"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="txtTelefonoOrigen_FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox113"  FilterType="Numbers"  ValidChars="/"  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--<asp:TextBox ID="TextBox70" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox71" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox72" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox73" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox74" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td><b>Departamento:</b></td>
        <td>
            <asp:TextBox ID="TextBox123" runat="server" Width="240px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td><b>Municipio:</b></td>
        <td>
            <asp:TextBox ID="TextBox124" runat="server" Width="240px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
    </tr>
    </table>
   
    <table style="width: 975px">
    </table>
   
    <table style="width: 975px">
    <tr>
        <td style="height: 23px"><b>Cobertura en Salud</b></td>
    </tr>
    <tr>
        <td>
            <asp:RadioButtonList ID="rblCobertura" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" CssClass="form_input" Width="975px">
                <asp:ListItem Value="1">Regimen Contributivo</asp:ListItem>
                <asp:ListItem Value="2">Regimen Subsidiado - Parcial</asp:ListItem>
                <asp:ListItem Value="3">Población Pobre No Asegurada Sin SISBEN</asp:ListItem>
                <asp:ListItem Value="4">Plan Adicional de Salud</asp:ListItem>
                <asp:ListItem Value="5">Regimen Subsidiado - Total</asp:ListItem>
                <asp:ListItem Value="6">Población Pobre No Asegurada Con SISBEN</asp:ListItem>
                <asp:ListItem Value="7">Desplazado</asp:ListItem>
                <asp:ListItem Value="8">Otro</asp:ListItem>
            </asp:RadioButtonList>
            </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px" align="center" colspan="8"><B>INFORMACIÓN DE LA ATENCIÓN</B></td>
    </tr>
    <tr>
        <td style="height: 23px"><b>Origen de la atención</b></td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px" colspan="2">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
    </tr>
    <tr>
        <td align="left" colspan="5" rowspan="2">
            <asp:RadioButtonList ID="rblAtencion" runat="server" RepeatColumns="3" RepeatDirection="Horizontal" CssClass="form_input">
                <asp:ListItem Value="13">Enfermedad General</asp:ListItem>
                <asp:ListItem Value="01">Accidente de Trabajo</asp:ListItem>
                <asp:ListItem Value="06">Evento Catastrófico</asp:ListItem>
                <asp:ListItem Value="14">Enfermedad Profesional</asp:ListItem>
                <asp:ListItem Value="02">Accidente de Tránsito</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        <td style="height: 23px">
            &nbsp;</td>
        <td style="height: 23px"><b>Clasificación triage</b></td>
        <td>
            <asp:RadioButtonList ID="rblTriage" runat="server" CssClass="form_input" Enabled="False" RepeatDirection="Horizontal">
                <asp:ListItem Value="1">I</asp:ListItem>
                <asp:ListItem Value="2">II</asp:ListItem>
                <asp:ListItem Value="3">III</asp:ListItem>
                <asp:ListItem Value="4">IV</asp:ListItem>
                <asp:ListItem Value="5">V</asp:ListItem>
            </asp:RadioButtonList>
            </td>
    </tr>
    <tr>
        <td style="height: 23px">
            &nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td>
            &nbsp;</td>
    </tr>
    

    </table>

      <table style="width: 975px">
    <tr>
        <td style="height: 23px" colspan="6"><b>Ingreso a Urgencias</b></td>
    </tr>
    <tr>
        <td align="center" style="width: auto"><b>Fecha:</b></td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox18" runat="server" Width="141px" style="text-align:center" Font-Size="Small" MaxLength="10" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox18"  FilterType="Numbers, Custom"  ValidChars="/"  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--            <asp:TextBox ID="TextBox82" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox83" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox84" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox85" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox86" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox87" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox88" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox89" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox90" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox91" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox92" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox93" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox94" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox95" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox96" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
        <td align="center" style="width: auto"><b>Hora:</b></td>
        <td align="center" style="width: auto">
            <asp:TextBox ID="TextBox19" runat="server" Width="67px" style="text-align:center" Font-Size="Small" MaxLength="5" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="TextBox19"  FilterType="Numbers, Custom"  ValidChars=":"  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--            <asp:TextBox ID="TextBox103" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox104" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox105" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label15" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox106" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox107" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label24" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox108" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox109" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
        </td>
        <td style="height: 23px" align="right">Paciente viene remitido</td>
        <td style="height: 23px">
            <asp:RadioButtonList ID="rblRemitido" runat="server" AutoPostBack="True" CssClass="form_input" OnSelectedIndexChanged="rblRemitido_SelectedIndexChanged" RepeatDirection="Horizontal">
                <asp:ListItem>SI</asp:ListItem>
                <asp:ListItem>NO</asp:ListItem>
            </asp:RadioButtonList>
         </td>

    </tr>
    <tr>
        <td style="height: 23px" colspan="3">Nombre del prestador de servicio de salud que remite</td>
        
        <td style="height: 23px" align="right" colspan="1">Codigo:</td>
        <td style="height: 23px" colspan="2">
            <asp:TextBox ID="TextBox449" runat="server" Width="160px" style="text-align:center" Font-Size="Small" MaxLength="15" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="TextBox449"  FilterType="Numbers"  ValidChars=""  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--<asp:TextBox ID="TextBox114" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox115" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox116" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox117" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox118" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox119" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox120" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox121" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
       
    </tr>
    <tr>
        <td style="height: 23px" colspan="6">
            <asp:TextBox ID="TextBox461" runat="server" Width="854px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    </table>


    <table style="width: 975px">
    <tr>
        <td><b>Departamento:</b></td>
        <td>
            <asp:DropDownList ID="dllDepartamento" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dllDepartamento_SelectedIndexChanged" Width="236px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td><b>Municipio:</b></td>
        <td>
            <asp:DropDownList ID="ddlMunicipio" runat="server" Width="236px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
    </tr>
    </table>

    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px" colspan="8"><b>Motivo de consulta</b></td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="12">
            <asp:TextBox ID="TextBox462" runat="server" Width="951px" Height="57px" TextMode="MultiLine" CssClass="form_input"></asp:TextBox>
            </td>

    </tr>
</table>
   


    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px"><b>Impresión Diagnóstica</b></td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">Código CIE10</td>
        <td style="height: 23px">Descripción</td>
    </tr>
    <tr>
        <td style="height: 23px">Diagnóstico Principal</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox387" runat="server" Width="85px" style="text-align:center" Font-Size="Small" MaxLength="4" CssClass="form_input" Enabled="False"></asp:TextBox>
       
            <%--            <asp:TextBox ID="TextBox131" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox132" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox133" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label1" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox134" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox135" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label2" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox136" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox148" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%></td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox399" runat="server" Width="672px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">Diagnóstico Relacionado 1</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox391" runat="server" Width="85px" style="text-align:center" Font-Size="Small" MaxLength="4" CssClass="form_input" Enabled="False"></asp:TextBox>
        
            <%--            <asp:TextBox ID="TextBox40" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
             <asp:Label ID="Label6" runat="server" Text=":" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox152" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox153" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox400" runat="server" Width="672px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px">Diagnóstico Relacionado 2</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox395" runat="server" Width="85px" style="text-align:center" Font-Size="Small" MaxLength="4" CssClass="form_input" Enabled="False"></asp:TextBox>
        
            <%--            <asp:TextBox ID="TextBox450" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox451" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox452" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox453" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox454" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox455" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox456" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox457" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox458" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox459" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox460" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox401" runat="server" Width="672px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
   <tr>
        <td style="height: 23px">Diagnóstico Relacionado 3</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox127" runat="server" Width="85px" style="text-align:center" Font-Size="Small" MaxLength="4" CssClass="form_input" Enabled="False"></asp:TextBox>
        
            <%--<asp:TextBox ID="TextBox388" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox389" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox390" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            --%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox156" runat="server" Width="672px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
 
    </table>
      <table style="width: 975px">
    <tr>
        <td style="height: 23px"><b>Destino del paciente</b></td>
        <td style="height: 23px"></td>
        <td style="height: 23px"></td>
        <td style="height: 23px"></td>
        <td style="height: 23px"></td>
       
     </tr>
    <tr>
        <td align="justify" colspan="5">
            <asp:RadioButtonList ID="rblDestino" runat="server" CssClass="form_input" RepeatColumns="3" RepeatDirection="Horizontal" Width="943px">
                <asp:ListItem Value="1">Domicilio</asp:ListItem>
                <asp:ListItem Value="2">Internación</asp:ListItem>
                <asp:ListItem Value="3">Contraremisión</asp:ListItem>
                <asp:ListItem Value="4">Observación</asp:ListItem>
                <asp:ListItem Value="5">Remisión</asp:ListItem>
                <asp:ListItem Value="6">Otro</asp:ListItem>
            </asp:RadioButtonList>
        </td>
        

    </tr>
    <tr>
        <td style="height: 23px" colspan="5">&nbsp;</td>
       

    </tr>


    </table>
    <table style="width: 975px">
    <tr>
        <td style="height: 23px" colspan="12" align="center"><B>INFORMACIÓN DE LA PERSONA QUE INFORMA</B></td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2">Nombre de quien informa</td>
        <td rowspan="3">Teléfono</td>
        <td style="height: 23px; width: 111px;">
            <asp:TextBox ID="txtIndicativoSolic" runat="server" Width="91px" style="text-align:left" Font-Size="Small" CssClass="form_input" Enabled="False" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="txtIndicativoSolic" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--            <asp:TextBox ID="TextBox391" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox392" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox393" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox394" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="txtNumeroSolic" runat="server" Width="126px" style="text-align:left" Font-Size="Small" CssClass="form_input" Enabled="False" ></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="txtNumeroSolic" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--            <asp:TextBox ID="TextBox395" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox396" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox397" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox398" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="txtExtSolic" runat="server" Width="85px" style="text-align:center" Font-Size="Small" CssClass="form_input" Enabled="False" ></asp:TextBox>
            <%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="txtExtSolic" FilterType="Numbers"  ValidChars="-"  ></ajaxToolkit:FilteredTextBoxExtender>--%>

            <%--            <asp:TextBox ID="TextBox127" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox128" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox130" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox155" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2">
            <asp:TextBox ID="TextBox402" runat="server" Width="476px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td style="height: 23px; width: 111px;" align="center">Indicativo</td>
        <td style="height: 23px" align="center">Número</td>
        <td style="height: 23px" align="center">Extensión</td>
    </tr>
    <tr>
        <td style="height: 23px">Cargo o Actvidad:</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox419" runat="server" Width="359px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td style="height: 23px; width: 111px;" align="center">Teléfono Celular:</td>
        <td style="height: 23px" colspan="2" align="center">
            <asp:TextBox ID="txtCelularSolicita" runat="server" Width="200px"  Font-Size="Small" CssClass="form_input" Enabled="False" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="txtCelularSolicita" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--            <asp:TextBox ID="TextBox543" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox544" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox545" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label22" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox546" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox547" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label23" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox548" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox549" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
    </tr>
    <tr>
        <td style="height: 23px; font-size: xx-small;" colspan="6">MPS-SAS V5.0 2008-07-11</td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="6" align="center">
            <asp:Button ID="Button1" runat="server" Text="Generar PDF" OnClick="Button1_Click" />
        &nbsp;<asp:Button ID="Button2" runat="server" Text="Imprimir PDF" OnClick="Button2_Click" Enabled="False" Visible="False" />
        </td>
    </tr>
</table>
   <asp:Literal ID="litScript" runat="server"></asp:Literal>
   
    
</asp:Content>