﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/MasterNew.master" AutoEventWireup="true" CodeFile="ConsentimientosInformadosListar1.aspx.cs" Inherits="Anexos_ConsentimientosInformadosListar1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

        <%--<link rel="stylesheet" href="../dist/css/css_styles.css" />--%>
        <script src="<%= Page.ResolveUrl("~/js/jquery-1.8.3.js") %>"></script>
        <%--<script src="<%= Page.ResolveUrl("~/js/Scripts_jquery.metadata.js") %>"></script>--%>
        <script src="<%= Page.ResolveUrl("~/js/Scripts_jquery.dataTables.min.js") %>"></script>

        <script type="text/javascript">

        $(document).ready(function () {

            // Setup Metadata plugin
            $.metadata.setType("class");

            // Setup GridView
            $("table.grid").each(function () {
                var jTbl = $(this);

                if (jTbl.find("tbody>tr>th").length > 0) {
                    jTbl.find("tbody").before("<thead><tr></tr></thead>");
                    jTbl.find("thead:first tr").append(jTbl.find("th"));
                    jTbl.find("tbody tr:first").remove();
                }

                // If GridView has the 'sortable' class and has more than 10 rows
                if (jTbl.hasClass("sortable") && jTbl.find("tbody:first > tr").length > 5) {

                    // Run DataTable on the GridView
                    jTbl.dataTable({
                        sPaginationType: "full_numbers",
                        sDom: '<"top"lf>rt<"bottom"ip>',
                        oLanguage: {
                            sInfoFiltered: "(from _MAX_ entries)",
                            sSearch: ""
                        },
                        aoColumnDefs: [
                            { bSortable: false, aTargets: jTbl.metadata().disableSortCols }
                        ]
                    });
                }
            });
        });

    </script>
     <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box box-primary">
                    <!--div Título-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3 align="center" runat="server" class="box-title">Buscar consentimientos Informados</h3>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!--div campo Identificación-->
<%--                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:TextBox ID="txtId" runat="server" class="form-control" placeholder="Identificación a buscar" Visible="false"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4" style="left: 0px; top: 0px">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>--%>
                    <!--div botón Consultar-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-6">
                           <%-- <div class="form-group">
                                <div class="row">
                                    <div class="col-md-3 col-sm-4 col-xs-6 demo-col">
                                        <div class="icheck-material-red">
                                            <input type="radio" id="red1" name="red"  />
                                            <label for="red1">Todos los registros</label>
                                        </div>
                                        <div class="icheck-material-red">
                                            <input type="radio" checked id="red2" name="red" />
                                            <label for="red2">Cédula paciente</label>
                                        </div>
                                        <div class="icheck-material-red">
                                            <input type="radio" checked id="red3" name="red" />
                                            <label for="red3">Cédula especialista</label>
                                        </div>
                                    </div>
                                </div>
                                <div class="icheck-material-pink">
                                    <asp:RadioButtonList ID="filtro" runat="server" RepeatDirection="Horizontal" AutoPostBack="True" OnSelectedIndexChanged="filtro_SelectedIndexChanged" Style="font-family: Arial">
                                        <asp:ListItem Value="1">&nbsp;&nbsp;&nbsp;Todos los registros&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="2">&nbsp;&nbsp;&nbsp;Cédula paciente&nbsp;&nbsp;&nbsp;</asp:ListItem>
                                        <asp:ListItem Value="3">&nbsp;&nbsp;&nbsp;Cédula especialista</asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <br />--%>
                                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="btn btn-primary" OnClick="btnConsultar_Click" />&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                    <!--******************************************************************************************************************************************************-->
                </div>
            </div>
        </div>

    <div class="box box-primary" id="divtable" runat="server" visible="false">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    </div>
                </div>
            </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <asp:GridView ID="gvConsentimientos" runat="server" AutoGenerateColumns="False" CssClass="grid sortable {disableSortCols: [3]}" DataKeyNames="ID_CONSENTIMIENTO_PDF" Width="100%" EnableModelValidation="True">
                        <Columns>
                            <asp:BoundField DataField="ID_CONSENTIMIENTO_PDF" HeaderText="ID" Visible="False" />
                            <asp:BoundField DataField="IDENTIFICACION_PACIENTE" HeaderText="Identificación Paciente" />
                            <asp:BoundField DataField="NOMBRE_PACIENTE" HeaderText="Nombre Paciente" />
                            <asp:BoundField DataField="IDENTIFICACION_USUARIO" HeaderText="Identificación Especialista" />
                            <asp:BoundField DataField="NOMBRE_USUARIO" HeaderText="Nombre Especialista" />
                            <asp:BoundField DataField="FECHA_REGISTRO" HeaderText="Fecha Registro" />
                            <asp:BoundField DataField="CONSENTIMIENTO_HASH" HeaderText="Archivo" />
                            <asp:TemplateField HeaderText="Ver PDF">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgPDF" runat="server" Height="31px" ImageUrl="~/img/Imagenes_pdf.svg" Width="42px" OnClick="ImgPDF_Click" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Aprobación médico" Visible="False">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckAprobacionMedico" runat="server" Checked='<%# Eval("APROBACION_MEDICO") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:GridView>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar cambios" CssClass="btn btn-primary" OnClick="btnGuardar_Click" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

