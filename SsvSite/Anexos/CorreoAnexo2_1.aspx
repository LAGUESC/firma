﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CorreoAnexo2_1.aspx.cs" Inherits="Anexos_CorreoAnexo2_1" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">


 p.MsoNormal
	{margin-bottom:.0001pt;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	        margin-left: 0cm;
            margin-right: 0cm;
            margin-top: 0cm;
        }
        .auto-style1 {
            width: 100%;
        }
    table.MsoNormalTable
	{font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	}
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        
        <p class="MsoNormal">
            <a name="OLE_LINK1"><span style="mso-bookmark:OLE_LINK2"><span style="mso-bookmark:
OLE_LINK3"><span style="mso-bookmark:OLE_LINK4"><span style="mso-bookmark:OLE_LINK5"><span style="mso-bookmark:OLE_LINK6">Cordial saludo,<o:p></o:p></span></span></span></span></span></a></p>
        <p class="MsoNormal">
            <span style="mso-bookmark:OLE_LINK1"><span style="mso-bookmark:
OLE_LINK2"><span style="mso-bookmark:OLE_LINK3"><span style="mso-bookmark:OLE_LINK4"><span style="mso-bookmark:OLE_LINK5"><span style="mso-bookmark:OLE_LINK6"><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>
        <p class="MsoNormal">
            <span style="font-size:11.0pt;line-height:107%;
font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:
Calibri;mso-fareast-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;
mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;
mso-ansi-language:ES-CO;mso-fareast-language:EN-US;mso-bidi-language:AR-SA">Dando cumplimiento al decreto 4747 de 2007 y la resolución 3047 de 2008, se envía anexo técnico N° 2 solicitando autorización para la atención inicial de urgencias del paciente relacionado en el asunto.</span></p>
        <p class="MsoNormal">
            <span style="mso-bookmark:OLE_LINK1"><span style="mso-bookmark:
OLE_LINK2"><span style="mso-bookmark:OLE_LINK3"><span style="mso-bookmark:OLE_LINK4"><span style="mso-bookmark:OLE_LINK5"><span style="mso-bookmark:OLE_LINK6"><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>
        <p class="MsoNormal">
            Quedamos atentos a su gestión.<o:p></o:p></p>
        <p class="MsoNormal">
            <o:p></o:p>
        </p>
        <p class="MsoNormal">
            <o:p></o:p></p>
        <p class="MsoNormal">
            <span style="mso-bookmark:OLE_LINK1"><span style="mso-bookmark:
OLE_LINK2"><span style="mso-bookmark:OLE_LINK3"><span style="mso-bookmark:OLE_LINK4"><span style="mso-bookmark:OLE_LINK5"><span style="mso-bookmark:OLE_LINK6">Cordialmente,<o:p></o:p></span></span></span></span></span></span></p>
        <p class="MsoNormal">
            <o:p></o:p>
        </p>
        <p class="MsoNormal">
            <o:p></o:p>
        </p>
        <p class="MsoNormal">
            <o:p></o:p>
        </p>
            <o:p></o:p>
        <table class="auto-style1">
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
            </tr>
        </table>
        <p class="MsoNormal">
            <o:p></o:p>
        </p>
        <p class="MsoNormal">
            <table border="0" cellpadding="0" cellspacing="0" class="MsoNormalTable">
                <tr style="mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;
  height:61.9pt">
                    <td style="width:68.25pt;padding:0cm 5.4pt 0cm 5.4pt;
  height:61.9pt" valign="top" width="91">
                        <p class="MsoNormal">
                            <span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
  mso-fareast-theme-font:minor-fareast;mso-fareast-language:ES-CO;mso-no-proof:
  yes"><![if !vml]>
                                <img src=cid:companylogo style="height: 79px; width: 58px"><![endif]></span><o:p></o:p></p>
                    </td>
                    <td style="width:366.75pt;padding:0cm 5.4pt 0cm 5.4pt;height:61.9pt" width="489">
                        <p class="MsoNormal">
                            <b><span style="font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
  &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#00B0F0;
  mso-no-proof:yes">Autorizaciones</span></b><o:p></o:p></p>
  <u1:p></u1:p>
                        <p class="MsoNormal">
                            <span style="font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
  &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;
  mso-no-proof:yes">Central de Autorizaciones </span><span style="font-family:
  &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:
  minor-fareast;color:#00B0F0;mso-no-proof:yes">|</span><span style="font-family:
  &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:
  minor-fareast;color:#1F4E79;mso-no-proof:yes"> Clínica CES<u1:p></u1:p></span><o:p></o:p></p>
                        <p class="MsoNormal">
                            <span style="font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:
  &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;
  mso-no-proof:yes">Tel: 5767272 ext 7470</span><span style="font-family:&quot;Arial&quot;,sans-serif;
  mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;
  color:#00B0F0;mso-no-proof:yes"> |</span><span style="font-family:&quot;Arial&quot;,sans-serif;
  mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;
  color:#1F4E79;mso-no-proof:yes"> Carrera 50c #58-12 </span><span style="font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
  mso-fareast-theme-font:minor-fareast;color:#00B0F0;mso-no-proof:yes">|</span><span style="font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;
  mso-fareast-theme-font:minor-fareast;color:#1F4E79;mso-no-proof:yes"> Medellín, Colombia<u1:p></u1:p></span><o:p></o:p></p>
                        <p class="MsoNormal">
                            <span style="mso-fareast-font-family:&quot;Times New Roman&quot;;
  mso-fareast-theme-font:minor-fareast;mso-no-proof:yes"><a href="http://www.clinicaces.com/"><i><span style="font-family:&quot;Arial&quot;,sans-serif;
  color:#00B0F0">www.clinicaces.com</span></i></a></span><o:p></o:p></p>
                    </td>
                </tr>
            </table>
        </p>
    
    </div>
    </form>
</body>
</html>
