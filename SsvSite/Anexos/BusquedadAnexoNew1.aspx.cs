﻿using ClinicaCES.Entidades;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Windows.Forms;

[WebService]
public partial class Anexos_BusquedadAnexoNew1 : System.Web.UI.Page
{
    string pathDescarga = "";
    string firma = "";
    DataTable dtFirma = null;
    
    

    protected void Page_Load(object sender, EventArgs e)
    {

        StreamReader reader = new StreamReader(Request.InputStream);
        String signature = Server.UrlDecode(reader.ReadToEnd());
        reader.Close();

        if (signature.StartsWith("bmpObj="))
        {
            drawimgFirm(signature);
        }

        if (!IsPostBack)
        {
            Procedimientos.Titulo("Consentimientos Informados", this.Page);
            //Procedimientos.LlenarCombos(ddlTipoId, new ClinicaCES.Logica.LMaestros().ListaTiposIdentificacion(), "ID", "ID");
            //drawimgFirm(signature);
        }
        this.Form.DefaultButton = btnConsultar.UniqueID;
        //litScript.Text = string.Empty;
    }

    #region "FUNCIONES"
    public class Encrypt
    {
        public static string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
    }
    private void Consultar(string Id)
    {
        if ((CheckPaciente.Checked) | (CheckRepresentante.Checked))
        {
            if (Id == "")
            {
                //pnlInforme.Visible = false;
                divtable.Visible = false;
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
            }
            else
            {
                DataSet ds = new ClinicaCES.Logica.LBusquedaPacientes().BusquedaPaciente(Id);
                DataTable dtInforme = ds.Tables[0];
                DataTable dtPagina = Procedimientos.dtFiltrado("IDENTIFICACION", "", dtInforme);
                string[] campo = { "IDENTIFICACION" };
                if (dtInforme.Rows.Count > 0)
                {
                    DataRow row = dtInforme.Rows[0];
                    LblNombre.Text = row["PACIENTE"].ToString();
                    if (CheckPaciente.Checked)
                    {
                        chkcons1.Visible = false;
                        chkcons6.Visible = true;
                    }
                    else
                    {
                        chkcons1.Visible = true;
                        chkcons6.Visible = true;
                    }
                    divtable.Visible = true;

                }
                else
                {
                    //pnlInforme.Visible = true;
                    divtable.Visible = true;
                    Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
                }
            }
        }
        else
        {
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
        }
    }
    private void GuardarPDF(string Cedula, string CedUser, string NameUser)
    {


        ConsentimientosInformados oPDF = new ConsentimientosInformados();

        string FilePath = Server.MapPath(pathDescarga);

        WebClient User = new WebClient();

        Byte[] FileBuffer = User.DownloadData(FilePath);

        oPDF.Cedula = Cedula;
        oPDF.NombrePaciente = LblNombre.Text;
        oPDF.CedulaUsuario = CedUser;
        oPDF.NombreUsuario = NameUser;
        oPDF.FechaRegistro = Convert.ToDateTime(DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"));
        //if (CheckAprobacionMedico.Checked)
        //{
        //    oPDF.AprobacionMedico = CheckAprobacionMedico.Checked = true;
        //}
        //else
        //{
            oPDF.AprobacionMedico = CheckAprobacionMedico.Checked = false;
        //}

        int startIndex = 10;
        string NewpathDescarga = pathDescarga.Substring(startIndex).Replace(".pdf", "");

        oPDF.Hash = NewpathDescarga;
        oPDF.Consentimiento_PDF = FileBuffer;

        if (new ClinicaCES.Logica.LConsentimientosInformados().InsertPDFDB(oPDF))
        {
            Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
        }

    }
    public void drawimgFirm(string base64)
    {
        if ((base64 != ""))
        {
            int startIndex = 7;
            string Firma = base64.Substring(startIndex);

            ConsentimientosInformados oFirma = new ConsentimientosInformados();

            byte[] imagepath = Convert.FromBase64String(Firma);

            if (Session["CedulaAutorizado"].ToString() != "")
            {
                oFirma.Cedula = Session["CedulaAutorizado"].ToString();
            }
            else
            {
                oFirma.Cedula = Session["Cedula"].ToString();
            }

            oFirma.UserFirmPath = imagepath;

            if (new ClinicaCES.Logica.LConsentimientosInformados().InsertFirmDB(oFirma))
            {
                Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
            }

            Session["CedulaAutorizado"] = null;
        }

    }
    public void Limpiar()
    {
        LblNombre.Text = "";
        txtConsideraciones.Text = "";
        txtId.Text = "";
        txtIdAut.Text = "";
        txtNomAut.Text = "";
        CheckAnestesiaConductiva.Checked = false;
        CheckAnestesiaGeneral.Checked = false;
        CheckAnestesiaLocal.Checked = false;
        CheckAnestesiaRegional.Checked = false;
        chkcons1.Checked = false;
        chkcons2.Checked = false;
        chkcons3.Checked = false;
        //chkcons4.Checked = false;
        //chkcons5.Checked = false;
        //ddlTipoId.SelectedValue = "-1";
        divConsideraciones.Visible = false;
        divtable.Visible = false;
        divAnestesia.Visible = false;
        chkcons2.Enabled = true;
        chkcons3.Enabled = true;
        //chkcons4.Enabled = true;
        CheckPaciente.Checked = false;
        CheckRepresentante.Checked = false;

    }
    protected void GuardarEvento(string ced)
    {
        ConsentimientosInformados oEvento = new ConsentimientosInformados();
        if (chkcons1.Checked)
        {   //string Autorizado = Session["AUTORIZACION"].ToString();
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-2";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);

        }
        if (chkcons2.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-13";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        if (chkcons3.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-7";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        //if (chkcons4.Checked)
        //{
        //    oEvento.Cedula = ced;
        //    oEvento.CodConsentimiento = "F-HC-40";
        //    ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
        //    o.InsertEvento(oEvento);
        //}
        //if (chkcons5.Checked)
        //{
        //    oEvento.Cedula = ced;
        //    oEvento.CodConsentimiento = "F-HC-4";
        //    ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
        //    o.InsertEvento(oEvento);
        //}
        if (chkcons6.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-SI-5";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
    }
    #endregion

    #region "CONSENTIMIENTOS VARIOS"
    //protected string retornarHtml()
    //{

    //    DataTable dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_VARIOS();
    //    DataRow row = dt.Rows[0];

    //    string PiePagina1 = row["PIEPAGINA1"].ToString();
    //    string PiePagina2 = row["PIEPAGINA2"].ToString();
    //    string PiePagina3 = row["PIEPAGINA3"].ToString();


    //    string Encabezado = "", Representacion = "", ConsultaHC = "", Procedimientosenfemermeria = "", PiePagina = "", html = "", AcompanantePermanente = "";
    //    try
    //    {

    //        string path = Server.MapPath("../img/Logo.png");

    //        Encabezado = row["ENCABEZADO"].ToString();
    //        string Encabe = "", Representa = "", Procedimientosenfeme = "", AcompanantePerman = "", PiePag3 = "";
    //        Encabe = Encabezado.Replace("FechaNotificacion", DateTime.Today.ToString("MMMM dd, yyyy"))
    //                           .Replace("LblNombre", LblNombre.Text)
    //                           .Replace("ddlTipoId", ddlTipoId.SelectedValue.ToString())
    //                           .Replace("txtId", txtId.Text.Trim());

    //        DataTable dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(txtId.Text);

    //        if (dtFirma.Rows.Count > 0)
    //        {

    //            firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
    //            PiePag3 = PiePagina3.Replace("firma", firma)
    //                                .Replace("txtIdAut", txtIdAut.Text.Trim());

    //        }
    //        else
    //        {
    //            PiePag3 = PiePagina3.Replace("firma", "").Replace("txtIdAut", "");
    //        }

    //        if (chkcons1.Checked)
    //        {
    //            Representacion = row["REPRESENTACION"].ToString();
    //            Representa = Representacion.Replace("txtNomAut", txtNomAut.Text.Trim().ToUpper())
    //                                       .Replace("txtIdAut", txtIdAut.Text.Trim());
    //        }

    //        if (chkcons2.Checked)
    //        {
    //            ConsultaHC = row["CONSULTAHC"].ToString();
    //        }

    //        if (chkcons3.Checked)
    //        {
    //            Procedimientosenfemermeria = row["PROCEDIMIENTOSENFERMERIA"].ToString();
    //            Procedimientosenfeme = Procedimientosenfemermeria.Replace("txtConsideraciones", txtConsideraciones.Text.Trim());
    //        }

    //        if (chkcons4.Checked)
    //        {
    //            AcompanantePermanente = row["ACOMPANANTEPERMANENTE"].ToString();
    //            AcompanantePerman = AcompanantePermanente.Replace("LblNombre", LblNombre.Text.ToUpper());

    //        }

    //        PiePagina = row["PIEPAGINA"].ToString();

    //        if (chkcons1.Checked)
    //        {

    //            PiePagina = PiePagina + PiePagina1;

    //        }
    //        else
    //        {

    //            PiePagina = PiePagina + PiePagina2;

    //        }

    //        PiePagina = PiePagina + PiePag3;


    //        html = Encabe + Representa + ConsultaHC + Procedimientosenfeme + AcompanantePerman + PiePagina;
    //        return html;

    //    }

    //    catch
    //    { return html = ""; }

    //}
    #endregion

    protected string retornarHtml()
    {
        string html = "", NEW_CUERPO_DOCENCIA = "", ENCABEZADO = "", AUTORIZADO = "", REPRESENTACION = "", NEW_CUERPO_HC = "", FIRMA = "", NEW_CUERPO_ENFERMERIA = "", NEW_CUERPO_ACOMPANANTE_PERMANENTE = "", NEW_CUERPO_TRATAMIENTO_DATOS = "";
        /******************************************/

        #region "Datos"
        DataTable dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_CARGAR_TODOS();
        DataRow row = dt.Rows[0];

        
        /********ENCABEZADO*******************************************************************/
        string CUERPO_ENCABEZADO = dt.Rows[9]["CUERPO_CONSENTIMIENTO"].ToString();

        /********AUTORIZACIÓN DE DOCENCIA****************************************/
        string NOMBRE_DOCENCIA = dt.Rows[17]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_DOCENCIA = dt.Rows[17]["CODIGO_CONSENTIMIENTO"].ToString();
        int VERSION_DOCENCIA = Convert.ToInt16(dt.Rows[17]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_DOCENCIA = dt.Rows[17]["CUERPO_CONSENTIMIENTO"].ToString();

        /********AUTORIZACIÓN DE REPRESENTACIÓN***********************************************/
        string NOMBRE_REPRESENTACION = dt.Rows[1]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_REPRESENTACION = dt.Rows[1]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_REPRESENTACION"] = CODIGO_REPRESENTACION;
        int VERSION_REPRESENTACION = Convert.ToInt16(dt.Rows[1]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_REPRESENTACION = dt.Rows[1]["CUERPO_CONSENTIMIENTO"].ToString();

        /********AUTORIZACIÓN PARA CONSULTA DE HISTORIA CLINICA POR PERSONAL ADMINISTRATIVO***/
        string NOMBRE_HC = dt.Rows[4]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_HC = dt.Rows[4]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_HC"] = CODIGO_HC;
        int VERSION_HC = Convert.ToInt16(dt.Rows[4]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_HC = dt.Rows[4]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO INFORMADO SOBRE PROCEDIMIENTO DE ENFERMERÍA********************/
        string NOMBRE_PROCEDIMIENTO_ENFERMERIA = dt.Rows[5]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTO_ENFERMERIA = dt.Rows[5]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_PROCEDIMIENTO_ENFERMERIA"] = CODIGO_PROCEDIMIENTO_ENFERMERIA;
        int VERSION_PROCEDIMIENTO_ENFERMERIA = Convert.ToInt16(dt.Rows[5]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_PROCEDIMIENTO_ENFERMERIA = dt.Rows[5]["CUERPO_CONSENTIMIENTO"].ToString();

        /********NOTIFICACIÓN DE ACOMPAÑANTE PERMANENTE PARA PACIENTES HOSPITALIZADOS***********/
        string NOMBRE_ACOMPANANTE_PERMANENTE = dt.Rows[6]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_ACOMPANANTE_PERMANENTE = dt.Rows[6]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_ACOMPANANTE_PERMANENTE"] = CODIGO_ACOMPANANTE_PERMANENTE;
        int VERSION_ACOMPANANTE_PERMANENTE = Convert.ToInt16(dt.Rows[6]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_ACOMPANANTE_PERMANENTE = dt.Rows[6]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CALIDAD EN QUE SE OTORGAN LOS CONSENTIMIENTOS COMO PACIENTE***********/
        string CUERPO_CONSENTIMIENTOS_PACIENTE = dt.Rows[8]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CALIDAD EN QUE SE OTORGAN LOS CONSENTIMIENTOS COMO RESPONSABLE***********/
        string CUERPO_CONSENTIMIENTOS_RESPONSABLE = dt.Rows[2]["CUERPO_CONSENTIMIENTO"].ToString();

        /********TRATAMIENTO DE DATOS PERSONALES***********/
        string CUERPO_DATOS_PERSONALES = dt.Rows[3]["CUERPO_CONSENTIMIENTO"].ToString();

        /********FIRMA AUTORIZADO*********************************************************/
        string CUERPO_FIRMA_AUTORIZADO = dt.Rows[10]["CUERPO_CONSENTIMIENTO"].ToString();

        /********AUTORIZACIÓN PARA EL TRATAMIENTO DE DATOS PERSONALES***********/
        string NOMBRE_TRATAMIENTO_DATOS = dt.Rows[7]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_TRATAMIENTO_DATOS = dt.Rows[7]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_TRATAMIENTO_DATOS"] = CODIGO_TRATAMIENTO_DATOS;
        int VERSION_TRATAMIENTO_DATOS = Convert.ToInt16(dt.Rows[7]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_TRATAMIENTO_DATOS = dt.Rows[7]["CUERPO_CONSENTIMIENTO"].ToString();
        #endregion

        try
        {

            string path = Server.MapPath("../img/Logo.png"), NombreAutorizado = "";
            NombreAutorizado = LblNombre.Text;

            /****************ENCABEZADO*********************************/
            ENCABEZADO = CUERPO_ENCABEZADO
                               .Replace("lblPaciente", LblNombre.Text)
                               .Replace("lblDocumento", txtId.Text);
            /****************AUTORIZACIÓN*******************************/
            if (chkcons1.Checked)
            {
                dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(txtIdAut.Text);
                if (dtFirma.Rows.Count > 0)
                {
                    firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firma = "";
                }

                NombreAutorizado = txtNomAut.Text;
                AUTORIZADO = CUERPO_CONSENTIMIENTOS_RESPONSABLE;
                REPRESENTACION = CUERPO_REPRESENTACION
                                       .Replace("NombreConsentimientoAutorizacion", NOMBRE_REPRESENTACION)
                                       .Replace("CodigoConsentimientoAutorizacion", CODIGO_REPRESENTACION)
                                       .Replace("VersionadoAutorizacion", VERSION_REPRESENTACION.ToString())
                                       .Replace("txtNomAut", txtNomAut.Text)
                                       .Replace("txtIdAut", txtIdAut.Text);
            }
            else
            {
                dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(txtId.Text);
                if (dtFirma.Rows.Count > 0)
                {
                    firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firma = "";
                }
                AUTORIZADO = CUERPO_CONSENTIMIENTOS_PACIENTE;
                REPRESENTACION = "";
            }
            /****************HISTORIA CLINICA***************************/
            if (chkcons2.Checked)
            {
                NEW_CUERPO_HC = CUERPO_HC
                               .Replace("NombreConsentimientoHC", NOMBRE_HC)
                               .Replace("CodigoConsentimientoHC", CODIGO_HC)
                               .Replace("VersionadoHC", VERSION_HC.ToString());
            }
            /****************PROCEDIMIENTO ENFERMERIA*******************/
            if (chkcons3.Checked)
            {
                NEW_CUERPO_ENFERMERIA = CUERPO_PROCEDIMIENTO_ENFERMERIA
                                        .Replace("NombreConsentimientoEnfermeria", NOMBRE_PROCEDIMIENTO_ENFERMERIA)
                                        .Replace("CodigoConsentimientoEnfermeria", CODIGO_PROCEDIMIENTO_ENFERMERIA)
                                        .Replace("VersionadoEnfermeria", VERSION_PROCEDIMIENTO_ENFERMERIA.ToString())
                                        .Replace("lblOtrasConsideraciones", txtConsideraciones.Text);
            }
            /****************ACOMPAÑANTE PERMANENTE*********************/
            if (chkcons4.Checked)
            {
                NEW_CUERPO_ACOMPANANTE_PERMANENTE = CUERPO_ACOMPANANTE_PERMANENTE
                            .Replace("NombreConsentimientoAutorizacion", NOMBRE_ACOMPANANTE_PERMANENTE)
                            .Replace("CodigoConsentimientoAutorizacion", CODIGO_ACOMPANANTE_PERMANENTE)
                            .Replace("VersionadoAutorizacion", VERSION_ACOMPANANTE_PERMANENTE.ToString())
                            .Replace("LblNombre", LblNombre.Text)
                            .Replace("LblCama", "");
            }

            /****************TRATAMIENTO DE DATOS*********************/
            if (chkcons6.Checked)
            {
                NEW_CUERPO_TRATAMIENTO_DATOS = CUERPO_TRATAMIENTO_DATOS
                            .Replace("NombreConsentimientoTD", NOMBRE_TRATAMIENTO_DATOS)
                            .Replace("CodigoConsentimientoTD", CODIGO_TRATAMIENTO_DATOS)
                            .Replace("VersionadoTD", VERSION_TRATAMIENTO_DATOS.ToString());
            }
            /******************AUTORIZACIÓN DE DOCENCIA*****************/
            if (CheckAutorizacionDocencia.Checked)
            {
                NEW_CUERPO_DOCENCIA = CUERPO_DOCENCIA
                            .Replace("NombreConsentimiento", NOMBRE_DOCENCIA)
                            .Replace("CodigoConsentimiento", CODIGO_DOCENCIA)
                            .Replace("Versionado", VERSION_DOCENCIA.ToString());
            }
            /****************FIRMA**************************************/
            FIRMA = CUERPO_FIRMA_AUTORIZADO
                            .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                            .Replace("txtIdAut", txtIdAut.Text)
                            .Replace("firma", firma)
                            .Replace("lblUsuarioInforma", Session["Nombre"].ToString())
                            .Replace("lblPaciente", NombreAutorizado);

            //if (chkcons5.Checked)
            //{
            //    html = NEW_CUERPO_ANESTESIA + CUERPO_DATOS_PERSONALES;
            //}
            //else
            //{
                html = ENCABEZADO + NEW_CUERPO_HC + NEW_CUERPO_ENFERMERIA + NEW_CUERPO_ACOMPANANTE_PERMANENTE + NEW_CUERPO_DOCENCIA + NEW_CUERPO_TRATAMIENTO_DATOS + AUTORIZADO  + FIRMA + CUERPO_DATOS_PERSONALES;
            //}

            return html;

        }

        catch
        { return html = ""; }

    }

    #region "CONTROLES"
    protected void chkcons1_CheckedChanged(object sender, EventArgs e)
    {
        if (chkcons1.Checked)
        {
            divAutorizado.Visible = true;
        }

        else
        {
            divAutorizado.Visible = false;
            txtIdAut.Text = "";
            txtNomAut.Text = "";
        }
    }
    protected void chkcons3_CheckedChanged(object sender, EventArgs e)
    {
        if (chkcons3.Checked)
        {
            divConsideraciones.Visible = true;
           
        }

        else
        {
            divConsideraciones.Visible = false;
            txtConsideraciones.Text = "";
            
        }
    }
    protected void CheckPaciente_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckPaciente.Checked)
        {
            chkcons1.Visible = false;
            
            chkcons6.Visible = true;
            chkcons1.Checked = false;
            CheckRepresentante.Checked = false;
            txtIdAut.Text = "";
            txtNomAut.Text = "";
            divAutorizado.Visible = false;
            btnCaptureFirm.Visible = false;
            
        }

        else
        {
            chkcons1.Visible = true;         
            chkcons6.Visible = true;
        }
    }
    protected void CheckRepresentante_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckRepresentante.Checked)
        {
            chkcons1.Visible = true;
            chkcons1.Checked = true;
            chkcons1.Enabled = false;
            chkcons6.Visible = true;
            CheckPaciente.Checked = false;
            divAutorizado.Visible = true;
            btnCaptureFirm.Visible = false;
        }

        else
        {
            chkcons1.Visible = false;
            chkcons1.Checked = false;;
            chkcons6.Visible = true;
        }
    }
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        btnGuardar.Visible = false;
        btnCaptureFirm.Visible = false;
        Consultar(txtId.Text);
        Session["Cedula"] = txtId.Text;

    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        string CedUser = Session["Identificacion"].ToString();
        string NombreUser = Session["Nombre"].ToString();


        string cadenaEncriptada = Encrypt.GetMD5(DateTime.Now.ToString());

        string html = retornarHtml();
        var htmlContent = String.Format(html);
        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        pathDescarga = "../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf";


        htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf"));

        GuardarPDF(txtId.Text, CedUser, NombreUser);
        GuardarEvento(txtId.Text);


        //DirectoryInfo di = new DirectoryInfo("C:\\Users\\LUISAGUDELO\\Desktop\\REPOSCES\\FIRMA\\SsvSite\\AUTPDF\\");
        DirectoryInfo di = new DirectoryInfo("C:\\inetpub\\wwwroot\\Firmas\\AUTPDF");
        foreach (FileInfo file in di.GetFiles())
        {
            file.Delete();
        }
        foreach (DirectoryInfo dir in di.GetDirectories())
        {
            dir.Delete(true);
        }

        divtable.Visible = false;
        Limpiar();


    }
    protected void btnReview_Click(object sender, EventArgs e)
    {
        Session["CedulaAutorizado"] = txtIdAut.Text;
        string PDF = "";
        string cadenaEncriptada = Encrypt.GetMD5(DateTime.Now.ToString());

        string html = retornarHtml();
        var htmlContent = String.Format(html);
        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        pathDescarga = "../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf";


        htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf"));

        PDF = pathDescarga;

        //Session["PDF"] = PDF;



        string url = "VerPDF.aspx?PDF=" + PDF + "&Origen1=Busquedad";
        Response.Write("<script type='text/javascript'>window.open('" + url + "', 'window','resizable=no,location=1,status=1,scrollbars=1,width=1200,height=800,left=400,top=90');</script>");

        if (firma != "")
        {
            btnGuardar.Visible = true;
            //CheckAprobacionMedico.Visible = true;
        }
        else
        {
            btnCaptureFirm.Visible = true;
        }
    }
    protected void Capture_Firma_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "captsig", "Capture();", true);
    }
    #endregion

}