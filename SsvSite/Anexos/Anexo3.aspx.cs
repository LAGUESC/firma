﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClinicaCES.Correo;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;
using System.IO;


public partial class Anexos_Anexo3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //traerCorreo();
            datosIniciales();
        }
    }
    protected void datosIniciales()
    { 
        //generarSolicitud()   
        if (Request.QueryString["cookie3"] != null)
        {
            TraerDatos(Procedimientos.descifrar(Request.QueryString["cookie3"]));
            
        }
        TextBox10.Text = DateTime.Now.ToShortDateString();
        TextBox20.Text = DateTime.Now.ToShortTimeString();
    }
    protected void TraerDatos(string Busqueda)
    {
        string msg = "";
        try
        {
            string[] Filtros = Busqueda.Split('|');
            DataTable dtGrid = new ClinicaCES.Logica.LBusquedaPacientes().consultarDatosAnexo3(Filtros[0], Filtros[1], Filtros[2]);
            DataRow registro = dtGrid.Rows[0];
            TextBox75.Text = registro[0].ToString();
            TextBox76.Text = registro[1].ToString();
            TextBox77.Text = registro[2].ToString();
            TextBox78.Text = registro[3].ToString();
            TextBox81.Text = registro[4].ToString();
            rblTipoId.SelectedValue = registro[5].ToString();
            // TraerIndentificacion(registro[5].ToString());
            TextBox102.Text = registro[6].ToString();
            TextBox112.Text = registro[7].ToString();
            TextBox113.Text = registro[8].ToString();
            TextBox123.Text = registro[9].ToString();
            TextBox124.Text = registro[10].ToString();
            TextBox125.Text = registro[11].ToString();
            TextBox136.Text = registro[12].ToString();
            rblAtencion.SelectedValue = registro[13].ToString();
            //TraerContingencia(registro[13].ToString());
            rblUbicacion.SelectedValue = Filtros[2];
            //TraerUbicacion();
            traerDiagnosticos(dtGrid);
            TextBox402.Text = registro[16].ToString();
            TextBox419.Text = registro[17].ToString();
            TextBox403.Text = "4";
            TextBox408.Text = "5767272";
            TextBox415.Text = "7258";
            ViewState["tipoId"] = registro[18].ToString();
            TextBox420.Text = registro[19].ToString();
            TextBox164.Text = registro[21].ToString();
            TextBox157.Text = registro[20].ToString();
            TextBox449.Text = registro[22].ToString();
            CargarPrestaciones();
        }
        catch
        {
            msg = "69";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
        
    }
    protected void CargarPrestaciones()
    {
        string msg = "";
        try
        {
            DataTable dtGrid = new ClinicaCES.Logica.LBusquedaPacientes().consultarPrestxDefecto();
            if (dtGrid.Rows.Count > 0)
            {
                for (int i = 0; i < dtGrid.Rows.Count; i++)
                {
                    DataRow registro = dtGrid.Rows[i];
                    if (i == 0) { TextBox166.Text = registro[0].ToString(); TextBox173.Text = registro[1].ToString(); }
                    if (i == 1) { TextBox177.Text = registro[0].ToString(); TextBox450.Text = registro[1].ToString(); }
                    
                }
            }
        }
        catch
        {
            msg = "77";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected void traerDiagnosticos(DataTable dt)
    {
        string msg = "";
        try
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow registro = dt.Rows[i];
                if (i == 0) { TextBox387.Text = registro[14].ToString(); TextBox399.Text = registro[15].ToString(); }
                if (i == 1) { TextBox391.Text = registro[14].ToString(); TextBox400.Text = registro[15].ToString(); }
                if (i == 2) { TextBox395.Text = registro[14].ToString(); TextBox401.Text = registro[15].ToString(); }
            }
        }
        catch
        {
            msg = "70";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string msg = "";
        try
        {
            if (validar())
            {
                Button1.Enabled = false;
                Button2.Visible = true;
                Button2.Enabled = true;
                string convenio = traerCorreo();
                if (convenio != "")
                {
                    DataRow Cargo = new ClinicaCES.Logica.LMaestros().generarSolicitudAne3().Rows[0];
                    TextBox1.Text = Cargo[0].ToString();
                    string html = retornarHtml() + retonrarHtml2();
                    var htmlContent = String.Format(html);
                    var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                    string pApellido = "";
                    string sApellido = "";
                    string pNombre = "";
                    string sNombre = "";
                    if (TextBox75.Text != "") { pApellido = TextBox75.Text.Substring(0, TextBox75.Text.Length - TextBox75.Text.Length + 1).Trim(); }
                    if (TextBox76.Text != "") { sApellido = TextBox76.Text.Substring(0, TextBox76.Text.Length - TextBox76.Text.Length + 1).Trim(); }
                    if (TextBox77.Text != "") { pNombre = TextBox77.Text.Substring(0, TextBox77.Text.Length - TextBox77.Text.Length + 1).Trim(); }
                    if (TextBox78.Text != "") { sNombre = TextBox78.Text.Substring(0, TextBox78.Text.Length - TextBox78.Text.Length + 1).Trim(); }
                    //string prueba =   TextBox76.Text.Substring(0, TextBox76.Text.Length - TextBox76.Text.Length + 1).Trim() + TextBox77.Text.Substring(0, TextBox77.Text.Length - TextBox77.Text.Length + 1).Trim() + TextBox78.Text.Substring(0, TextBox78.Text.Length - TextBox78.Text.Length + 1).Trim();
                    string archivo = ViewState["tipoId"].ToString() + "-" + TextBox81.Text.Trim() + "-" + pApellido + sApellido + pNombre + sNombre + "-" + TextBox1.Text;

                    string Mensaje = construirCorreo();
                    string Mensaje2 = construirCorreo2();
                    htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + archivo + "_Anexo3.pdf"));
                    if (enviar(convenio, Mensaje, archivo + "_Anexo3.pdf - Envio No: " + 1, Server.MapPath("../AUTPDF/" + archivo + "_Anexo3.pdf")))
                    {
                        string[] datos = agrupar().Split(';');
                        string[] Filtros = Procedimientos.descifrar(Request.QueryString["cookie3"]).Split('|');
                        if (new ClinicaCES.Logica.LAnexos().AnexoActualizar(Filtros[0], Filtros[1], TextBox1.Text, convenio, Mensaje, Mensaje2, "3", archivo + "_Anexo3.pdf", Server.MapPath("../AUTPDF/" + archivo + "_Anexo3.pdf"), datos[0], datos[1], Session["Nick1"].ToString(), Filtros[2]))
                        {
                            msg = "1";
                            inactivarControles();
                            ViewState["Archivo3"] = archivo + "_Anexo3.pdf";

                            //Response.Redirect("../General/Index.aspx");
                        }
                        else
                        {
                            msg = "3";
                        }
                    }
                    Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                }
                else
                {
                    msg = "78";
                    Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                }
            }
            else
            {
                msg = "68";
                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            }

        }
        catch
        {
            Button1.Enabled = false;
            msg = "71";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }

    }
    protected bool validar()
    {
        bool Valido = true;

        System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
        string[] css = { "form_input", "invalidtxt" };

        if (string.IsNullOrEmpty(rblCobertura.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            rblCobertura.CssClass = css[1];
        }
        else
        {
            rblCobertura.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(rblAtencion.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            rblAtencion.CssClass = css[1];
        }
        else
        {
            rblAtencion.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(rblServicio.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            rblServicio.CssClass = css[1];
        }
        else
        {
            rblServicio.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(rblPrioridad.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            rblPrioridad.CssClass = css[1];
        }
        else
        {
            rblPrioridad.CssClass = css[0];
        }
        //if (string.IsNullOrEmpty(TextBox165.Text.Trim()))
        //{
        //    Valido = false;
        //    TextBox165.CssClass = css[1];
        //}
        //else
        //{
        //    TextBox165.CssClass = css[0];
        //}
        if (string.IsNullOrEmpty(TextBox166.Text.Trim()) && string.IsNullOrEmpty(TextBox173.Text.Trim()) && string.IsNullOrEmpty(TextBox174.Text.Trim()))
        {
            TextBox166.CssClass = css[0];
            TextBox173.CssClass = css[0];
            TextBox174.CssClass = css[0];
        }
        else
        {
            if (TextBox166.Text.Trim() != "")
            {
                TextBox166.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox166.CssClass = css[1];
            }
            if (TextBox173.Text.Trim() != "")
            {
                TextBox173.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox173.CssClass = css[1];
            }
            if (TextBox174.Text.Trim() != "")
            {
                TextBox174.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox174.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox177.Text.Trim()) && string.IsNullOrEmpty(TextBox184.Text.Trim()) && string.IsNullOrEmpty(TextBox450.Text.Trim()))
        {
            TextBox177.CssClass = css[0];
            TextBox184.CssClass = css[0];
            TextBox450.CssClass = css[0];
        }
        else
        {
            if (TextBox177.Text.Trim() != "")
            {
                TextBox177.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox177.CssClass = css[1];
            }
            if (TextBox184.Text.Trim() != "")
            {
                TextBox184.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox184.CssClass = css[1];
            }
            if (TextBox450.Text.Trim() != "")
            {
                TextBox450.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox450.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox188.Text.Trim()) && string.IsNullOrEmpty(TextBox314.Text.Trim()) && string.IsNullOrEmpty(TextBox431.Text.Trim()))
        {
            TextBox188.CssClass = css[0];
            TextBox314.CssClass = css[0];
            TextBox431.CssClass = css[0];
        }
        else
        {
            if (TextBox188.Text.Trim() != "")
            {
                TextBox188.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox188.CssClass = css[1];
            }
            if (TextBox314.Text.Trim() != "")
            {
                TextBox314.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox314.CssClass = css[1];
            }
            if (TextBox431.Text.Trim() != "")
            {
                TextBox431.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox431.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox195.Text.Trim()) && string.IsNullOrEmpty(TextBox317.Text.Trim()) && string.IsNullOrEmpty(TextBox432.Text.Trim()))
        {
            TextBox195.CssClass = css[0];
            TextBox317.CssClass = css[0];
            TextBox432.CssClass = css[0];
        }
        else
        {
            if (TextBox195.Text.Trim() != "")
            {
                TextBox195.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox195.CssClass = css[1];
            }
            if (TextBox317.Text.Trim() != "")
            {
                TextBox317.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox317.CssClass = css[1];
            }
            if (TextBox432.Text.Trim() != "")
            {
                TextBox432.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox432.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox202.Text.Trim()) && string.IsNullOrEmpty(TextBox320.Text.Trim()) && string.IsNullOrEmpty(TextBox433.Text.Trim()))
        {
            TextBox202.CssClass = css[0];
            TextBox320.CssClass = css[0];
            TextBox433.CssClass = css[0];
        }
        else
        {
            if (TextBox202.Text.Trim() != "")
            {
                TextBox202.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox202.CssClass = css[1];
            }
            if (TextBox320.Text.Trim() != "")
            {
                TextBox320.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox320.CssClass = css[1];
            }
            if (TextBox433.Text.Trim() != "")
            {
                TextBox433.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox433.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox209.Text.Trim()) && string.IsNullOrEmpty(TextBox323.Text.Trim()) && string.IsNullOrEmpty(TextBox434.Text.Trim()))
        {
            TextBox209.CssClass = css[0];
            TextBox323.CssClass = css[0];
            TextBox434.CssClass = css[0];
        }
        else
        {
            if (TextBox209.Text.Trim() != "")
            {
                TextBox209.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox209.CssClass = css[1];
            }
            if (TextBox323.Text.Trim() != "")
            {
                TextBox323.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox323.CssClass = css[1];
            }
            if (TextBox434.Text.Trim() != "")
            {
                TextBox434.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox434.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox216.Text.Trim()) && string.IsNullOrEmpty(TextBox326.Text.Trim()) && string.IsNullOrEmpty(TextBox435.Text.Trim()))
        {
            TextBox216.CssClass = css[0];
            TextBox326.CssClass = css[0];
            TextBox435.CssClass = css[0];
        }
        else
        {
            if (TextBox216.Text.Trim() != "")
            {
                TextBox216.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox216.CssClass = css[1];
            }
            if (TextBox326.Text.Trim() != "")
            {
                TextBox326.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox326.CssClass = css[1];
            }
            if (TextBox435.Text.Trim() != "")
            {
                TextBox435.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox435.CssClass = css[1];
            }
        }
        
        if (string.IsNullOrEmpty(TextBox223.Text.Trim()) && string.IsNullOrEmpty(TextBox329.Text.Trim()) && string.IsNullOrEmpty(TextBox436.Text.Trim()))
        {
            TextBox223.CssClass = css[0];
            TextBox329.CssClass = css[0];
            TextBox436.CssClass = css[0];
        }
        else
        {
            if (TextBox223.Text.Trim() != "")
            {
                TextBox223.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox223.CssClass = css[1];
            }
            if (TextBox329.Text.Trim() != "")
            {
                TextBox329.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox329.CssClass = css[1];
            }
            if (TextBox436.Text.Trim() != "")
            {
                TextBox436.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox436.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox230.Text.Trim()) && string.IsNullOrEmpty(TextBox332.Text.Trim()) && string.IsNullOrEmpty(TextBox437.Text.Trim()))
        {
            TextBox230.CssClass = css[0];
            TextBox332.CssClass = css[0];
            TextBox437.CssClass = css[0];
        }
        else
        {
            if (TextBox230.Text.Trim() != "")
            {
                TextBox230.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox230.CssClass = css[1];
            }
            if (TextBox332.Text.Trim() != "")
            {
                TextBox332.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox332.CssClass = css[1];
            }
            if (TextBox437.Text.Trim() != "")
            {
                TextBox437.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox437.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox237.Text.Trim()) && string.IsNullOrEmpty(TextBox335.Text.Trim()) && string.IsNullOrEmpty(TextBox438.Text.Trim()))
        {
            TextBox237.CssClass = css[0];
            TextBox335.CssClass = css[0];
            TextBox438.CssClass = css[0];
        }
        else
        {
            if (TextBox237.Text.Trim() != "")
            {
                TextBox237.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox237.CssClass = css[1];
            }
            if (TextBox335.Text.Trim() != "")
            {
                TextBox335.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox335.CssClass = css[1];
            }
            if (TextBox438.Text.Trim() != "")
            {
                TextBox438.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox438.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox244.Text.Trim()) && string.IsNullOrEmpty(TextBox338.Text.Trim()) && string.IsNullOrEmpty(TextBox439.Text.Trim()))
        {
            TextBox244.CssClass = css[0];
            TextBox338.CssClass = css[0];
            TextBox439.CssClass = css[0];
        }
        else
        {
            if (TextBox244.Text.Trim() != "")
            {
                TextBox244.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox244.CssClass = css[1];
            }
            if (TextBox338.Text.Trim() != "")
            {
                TextBox338.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox338.CssClass = css[1];
            }
            if (TextBox439.Text.Trim() != "")
            {
                TextBox439.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox439.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox251.Text.Trim()) && string.IsNullOrEmpty(TextBox341.Text.Trim()) && string.IsNullOrEmpty(TextBox440.Text.Trim()))
        {
            TextBox251.CssClass = css[0];
            TextBox341.CssClass = css[0];
            TextBox440.CssClass = css[0];
        }
        else
        {
            if (TextBox251.Text.Trim() != "")
            {
                TextBox251.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox251.CssClass = css[1];
            }
            if (TextBox341.Text.Trim() != "")
            {
                TextBox341.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox341.CssClass = css[1];
            }
            if (TextBox440.Text.Trim() != "")
            {
                TextBox440.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox440.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox258.Text.Trim()) && string.IsNullOrEmpty(TextBox344.Text.Trim()) && string.IsNullOrEmpty(TextBox441.Text.Trim()))
        {
            TextBox258.CssClass = css[0];
            TextBox344.CssClass = css[0];
            TextBox441.CssClass = css[0];
        }
        else
        {
            if (TextBox258.Text.Trim() != "")
            {
                TextBox258.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox258.CssClass = css[1];
            }
            if (TextBox344.Text.Trim() != "")
            {
                TextBox344.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox344.CssClass = css[1];
            }
            if (TextBox441.Text.Trim() != "")
            {
                TextBox441.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox441.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox265.Text.Trim()) && string.IsNullOrEmpty(TextBox347.Text.Trim()) && string.IsNullOrEmpty(TextBox442.Text.Trim()))
        {
            TextBox265.CssClass = css[0];
            TextBox347.CssClass = css[0];
            TextBox442.CssClass = css[0];
        }
        else
        {
            if (TextBox265.Text.Trim() != "")
            {
                TextBox265.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox265.CssClass = css[1];
            }
            if (TextBox347.Text.Trim() != "")
            {
                TextBox347.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox347.CssClass = css[1];
            }
            if (TextBox442.Text.Trim() != "")
            {
                TextBox442.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox442.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox272.Text.Trim()) && string.IsNullOrEmpty(TextBox350.Text.Trim()) && string.IsNullOrEmpty(TextBox443.Text.Trim()))
        {
            TextBox272.CssClass = css[0];
            TextBox350.CssClass = css[0];
            TextBox443.CssClass = css[0];
        }
        else
        {
            if (TextBox272.Text.Trim() != "")
            {
                TextBox272.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox272.CssClass = css[1];
            }
            if (TextBox350.Text.Trim() != "")
            {
                TextBox350.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox350.CssClass = css[1];
            }
            if (TextBox443.Text.Trim() != "")
            {
                TextBox443.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox443.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox279.Text.Trim()) && string.IsNullOrEmpty(TextBox353.Text.Trim()) && string.IsNullOrEmpty(TextBox444.Text.Trim()))
        {
            TextBox279.CssClass = css[0];
            TextBox353.CssClass = css[0];
            TextBox444.CssClass = css[0];
        }
        else
        {
            if (TextBox279.Text.Trim() != "")
            {
                TextBox279.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox279.CssClass = css[1];
            }
            if (TextBox353.Text.Trim() != "")
            {
                TextBox353.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox353.CssClass = css[1];
            }
            if (TextBox444.Text.Trim() != "")
            {
                TextBox444.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox444.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox286.Text.Trim()) && string.IsNullOrEmpty(TextBox356.Text.Trim()) && string.IsNullOrEmpty(TextBox445.Text.Trim()))
        {
            TextBox286.CssClass = css[0];
            TextBox356.CssClass = css[0];
            TextBox445.CssClass = css[0];
        }
        else
        {
            if (TextBox286.Text.Trim() != "")
            {
                TextBox286.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox286.CssClass = css[1];
            }
            if (TextBox356.Text.Trim() != "")
            {
                TextBox356.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox356.CssClass = css[1];
            }
            if (TextBox445.Text.Trim() != "")
            {
                TextBox445.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox445.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox293.Text.Trim()) && string.IsNullOrEmpty(TextBox359.Text.Trim()) && string.IsNullOrEmpty(TextBox446.Text.Trim()))
        {
            TextBox293.CssClass = css[0];
            TextBox359.CssClass = css[0];
            TextBox446.CssClass = css[0];
        }
        else
        {
            if (TextBox293.Text.Trim() != "")
            {
                TextBox293.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox293.CssClass = css[1];
            }
            if (TextBox359.Text.Trim() != "")
            {
                TextBox359.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox359.CssClass = css[1];
            }
            if (TextBox446.Text.Trim() != "")
            {
                TextBox446.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox446.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox300.Text.Trim()) && string.IsNullOrEmpty(TextBox362.Text.Trim()) && string.IsNullOrEmpty(TextBox447.Text.Trim()))
        {
            TextBox300.CssClass = css[0];
            TextBox362.CssClass = css[0];
            TextBox447.CssClass = css[0];
        }
        else
        {
            if (TextBox300.Text.Trim() != "")
            {
                TextBox300.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox300.CssClass = css[1];
            }
            if (TextBox362.Text.Trim() != "")
            {
                TextBox362.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox362.CssClass = css[1];
            }
            if (TextBox447.Text.Trim() != "")
            {
                TextBox447.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox447.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox307.Text.Trim()) && string.IsNullOrEmpty(TextBox365.Text.Trim()) && string.IsNullOrEmpty(TextBox448.Text.Trim()))
        {
            TextBox307.CssClass = css[0];
            TextBox365.CssClass = css[0];
            TextBox448.CssClass = css[0];
        }
        else
        {
            if (TextBox307.Text.Trim() != "")
            {
                TextBox307.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox307.CssClass = css[1];
            }
            if (TextBox365.Text.Trim() != "")
            {
                TextBox365.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox365.CssClass = css[1];
            }
            if (TextBox448.Text.Trim() != "")
            {
                TextBox448.CssClass = css[0];
            }
            else
            {
                Valido = false;
                TextBox448.CssClass = css[1];
            }
        }
        if (string.IsNullOrEmpty(TextBox451.Text.Trim()))
        {
            Valido = false;
            TextBox451.CssClass = css[1];
        }
        else
        {
            TextBox451.CssClass = css[0];
        }

        if (TextBox166.Text == "" && TextBox174.Text=="" || TextBox173.Text=="" )
        {
            Valido = false;
            TextBox166.CssClass = css[1];
            TextBox174.CssClass = css[1];
            TextBox173.CssClass = css[1];
        }
        else
        {
            TextBox166.CssClass = css[0];
            TextBox174.CssClass = css[0];
            TextBox173.CssClass = css[0];
        }
        return Valido;
    }
    protected void AgregarPrintScript(string ruta)
    {
        string msg = "";
        try
        {
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", ruta));
            Response.ContentType = "application/pdf";
            Response.WriteFile(Server.MapPath(Path.Combine("~/AUTPDF", ruta)));
            Response.End();
        }
        catch
        {
            Button1.Enabled = false;
            msg = "74";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected string construirCorreo()
    {
        string nomPaciente = TextBox451.Text.ToUpper().Trim();
        string html1 = " <!DOCTYPE html>  " +
" <html xmlns='http://www.w3.org/1999/xhtml'>  " +
" <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title>  " +
" </title>  " +
" <style type='text/css'>  " +
" p.MsoNormal  " +
" 	{margin-bottom:.0001pt;  " +
" 	font-size:11.0pt;  " +
" 	font-family:'Calibri',sans-serif;  " +
"	        margin-left: 0cm;  " +
" margin-right: 0cm;  " +
" margin-top: 0cm;  " +
" }  " +
" .auto-style1 {  " +
" width: 100%;  " +
" }  " +
" table.MsoNormalTable  " +
" 	{font-size:11.0pt;  " +
" 	font-family:'Calibri',sans-serif;  " +
" 	}  " +
" </style>  " +
" </head>  " +
" <body>  " +
" <form name='form2' method='post' action='CorreoAnexo3_1.aspx' id='form2'>  " +
" <div>  " +
" <input type='hidden' name='__VIEWSTATE' id='__VIEWSTATE' value='/wEPDwUKLTEzNDM3NzkxOWRktFqUC1uh54j6j8uP3USpOlPZPsc=' />  " +
" </div>  " +
" <div>  " +
" 	<input type='hidden' name='__VIEWSTATEGENERATOR' id='__VIEWSTATEGENERATOR' value='8B02451E' />  " +
" </div>  " +
" <div>  " +
" <p class='MsoNormal'>  " +
" <a name='OLE_LINK1'><span style='mso-bookmark:OLE_LINK2'><span style='mso-bookmark:  " +
" OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordial saludo,<o:p></o:p></span></span></span></span></span></a></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-size:11.0pt;line-height:107%;  " +
" font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:  " +
" Calibri;mso-fareast-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;  " +
" mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;  " +
" mso-ansi-language:ES-CO;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'>Dando cumplimiento al decreto 4747 de 2007 y la resolución 3047 de 2008, se envía anexo técnico N° 3 solicitando autorización para " + nomPaciente + " del paciente relacionado en el asunto.</span></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" Quedamos atentos a su gestión.<o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordialmente,<o:p></o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <o:p></o:p>  " +
" <table class='auto-style1'>  " +
" <tr>  " +
" <td>&nbsp;</td>  " +
" <td>&nbsp;</td>  " +
" </tr>  " +
" <tr>  " +
" <td>&nbsp;</td>  " +
" <td>&nbsp;</td>  " +
" </tr>  " +
" </table>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <table border='0' cellpadding='0' cellspacing='0' class='MsoNormalTable'>  " +
" <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;  " +
" height:61.9pt'>  " +
" <td style='width:68.25pt;padding:0cm 5.4pt 0cm 5.4pt;  " +
" height:61.9pt' valign='top' width='91'>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;mso-fareast-language:ES-CO;mso-no-proof:  " +
" yes'><![if !vml]>  " +
" <img src=cid:companylogo style='height: 79px; width: 58px'><![endif]></span><o:p></o:p></p>  " +
" </td>  " +
" <td style='width:366.75pt;padding:0cm 5.4pt 0cm 5.4pt;height:61.9pt' width='489'>  " +
" <p class='MsoNormal'>  " +
" <b><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#00B0F0;  " +
" mso-no-proof:yes'>Autorizaciones</span></b><o:p></o:p></p>  " +
" <u1:p></u1:p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;  " +
" mso-no-proof:yes'>Central de Autorizaciones </span><span style='font-family:  " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:  " +
" minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:  " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:  " +
" minor-fareast;color:#1F4E79;mso-no-proof:yes'> Clínica CES<u1:p></u1:p></span><o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;  " +
" mso-no-proof:yes'>Tel: 5767272 ext 7630</span><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;  " +
" color:#00B0F0;mso-no-proof:yes'> |</span><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;  " +
" color:#1F4E79;mso-no-proof:yes'> Carrera 50c #58-12 </span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;color:#1F4E79;mso-no-proof:yes'> Medellín, Colombia<u1:p></u1:p></span><o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;mso-no-proof:yes'><a href='http://www.clinicaces.com/'><i><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" color:#00B0F0'>www.clinicaces.com</span></i></a></span><o:p></o:p></p>  " +
" </td>  " +
" </tr>  " +
" <u1:p></u1:p>  " +
" </table>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" </div>  " +
" </form>  " +
" </body>  " +
" </html>  ";
        return textoTilde(html1);

    }
    protected string construirCorreo2()
    {
        string nomPaciente = TextBox451.Text.ToUpper().Trim();
        string html2=" <!DOCTYPE html>  " +
" <html xmlns='http://www.w3.org/1999/xhtml'>  " +
" <head id='Head1'><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title>  " +
" </title>  " +
" <style type='text/css'>  " +
" p.MsoNormal  " +
" 	{margin-bottom:.0001pt;  " +
" 	font-size:11.0pt;  " +
" 	font-family:'Calibri',sans-serif;  " +
" 	        margin-left: 0cm;  " +
" margin-right: 0cm;  " +
" margin-top: 0cm;  " +
" }  " +
" .auto-style1 {  " +
" width: 100%;  " +
" }  " +
" table.MsoNormalTable  " +
" 	{font-size:11.0pt;  " +
" 	font-family:'Calibri',sans-serif;  " +
" 	}  " +
" </style>  " +
" </head>  " +
" <body>  " +
" <form name='form2' method='post' action='CorreoAnexo3_2.aspx' id='form2'>  " +
" <div>  " +
" <input type='hidden' name='__VIEWSTATE' id='__VIEWSTATE' value='/wEPDwUKLTEzNDM3NzkxOWRk6XQ33Nj/m6Y4Tpi0YWt/0sgWDZw=' />  " +
" </div>  " +
" <div>  " +
" 	<input type='hidden' name='__VIEWSTATEGENERATOR' id='__VIEWSTATEGENERATOR' value='C65AAC44' />  " +
" </div>  " +
" <div>  " +
" <p class='MsoNormal'>  " +
" <a name='OLE_LINK1'><span style='mso-bookmark:OLE_LINK2'><span style='mso-bookmark:  " +
" OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordial saludo,<o:p></o:p></span></span></span></span></span></a></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-size:11.0pt;line-height:107%;  " +
" font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:  " +
" Calibri;mso-fareast-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;  " +
" mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;  " +
" mso-ansi-language:ES-CO;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'>Dando cumplimiento al decreto 4747 de 2007 y la resolución 3047 de 2008, se envía anexo técnico N° 3 solicitando autorización para " + nomPaciente + " del paciente relacionado en el asunto.</span></p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" <p class='MsoNormal'>  " +
" Con soportes de envío se continuará con la atención del paciente; correo que a su vez será enviado a ente territorial como lo menciona la norma.<o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" Quedamos atentos a su gestión.<o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordialmente,<o:p></o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <o:p></o:p>  " +
" <table class='auto-style1'>  " +
" <tr>  " +
" <td>&nbsp;</td>  " +
" <td>&nbsp;</td>  " +
" </tr>  " +
" <tr>  " +
" <td>&nbsp;</td>  " +
" <td>&nbsp;</td>  " +
" </tr>  " +
" </table>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <table border='0' cellpadding='0' cellspacing='0' class='MsoNormalTable'>  " +
" <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;  " +
" height:61.9pt'>  " +
" <td style='width:68.25pt;padding:0cm 5.4pt 0cm 5.4pt;  " +
" height:61.9pt' valign='top' width='91'>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;mso-fareast-language:ES-CO;mso-no-proof:  " +
" yes'><![if !vml]>  " +
" <img src=cid:companylogo style='height: 79px; width: 58px'><![endif]></span><o:p></o:p></p>  " +
" </td>  " +
" <td style='width:366.75pt;padding:0cm 5.4pt 0cm 5.4pt;height:61.9pt' width='489'>  " +
" <p class='MsoNormal'>  " +
" <b><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#00B0F0;  " +
" mso-no-proof:yes'>Autorizaciones</span></b><o:p></o:p></p>  " +
" <u1:p></u1:p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;  " +
" mso-no-proof:yes'>Central de Autorizaciones </span><span style='font-family:  " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:  " +
" minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:  " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:  " +
" minor-fareast;color:#1F4E79;mso-no-proof:yes'> Clínica CES<u1:p></u1:p></span><o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;  " +
" mso-no-proof:yes'>Tel: 5767272 ext 7630</span><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;  " +
" color:#00B0F0;mso-no-proof:yes'> |</span><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;  " +
" color:#1F4E79;mso-no-proof:yes'> Carrera 50c #58-12 </span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;color:#1F4E79;mso-no-proof:yes'> Medellín, Colombia<u1:p></u1:p></span><o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;mso-no-proof:yes'><a href='http://www.clinicaces.com/'><i><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" color:#00B0F0'>www.clinicaces.com</span></i></a></span><o:p></o:p></p>  " +
" </td>  " +
" </tr>  " +
" <u1:p></u1:p>  " +
" </table>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" </div>  " +
" </form>  " +
" </body>  " +
" </html>  ";
        return textoTilde(html2);
    }
    protected string traerCorreo()
    {
        string msg = "";
        try
        {
            string[] Filtros = Procedimientos.descifrar(Request.QueryString["cookie3"]).Split('|');
            DataRow correo = new ClinicaCES.Logica.LBusquedaPacientes().traerCorreo(Filtros[0], Filtros[1], Filtros[2]).Rows[0];
            return correo[0].ToString();
        }
        catch
        {
            msg = "72";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            return "";
        }
    }
    protected string  agrupar()
    {
        string Codigo = "";
        string Descripcion = "";
        if (TextBox166.Text != "") { Codigo = TextBox166.Text; Descripcion = TextBox173.Text; }
        if (TextBox177.Text != "") { Codigo = Codigo + "|" + TextBox177.Text; Descripcion = Descripcion + "|" + TextBox450.Text; }
        if (TextBox188.Text != "") { Codigo = Codigo + "|" + TextBox188.Text; Descripcion = Descripcion + "|" + TextBox431.Text; }
        if (TextBox195.Text != "") { Codigo = Codigo + "|" + TextBox195.Text; Descripcion = Descripcion + "|" + TextBox432.Text; }
        if (TextBox202.Text != "") { Codigo = Codigo + "|" + TextBox202.Text; Descripcion = Descripcion + "|" + TextBox433.Text; }
        if (TextBox209.Text != "") { Codigo = Codigo + "|" + TextBox209.Text; Descripcion = Descripcion + "|" + TextBox434.Text; }
        if (TextBox216.Text != "") { Codigo = Codigo + "|" + TextBox216.Text; Descripcion = Descripcion + "|" + TextBox435.Text; }
        if (TextBox223.Text != "") { Codigo = Codigo + "|" + TextBox223.Text; Descripcion = Descripcion + "|" + TextBox436.Text; }
        if (TextBox230.Text != "") { Codigo = Codigo + "|" + TextBox230.Text; Descripcion = Descripcion + "|" + TextBox437.Text; }
        if (TextBox237.Text != "") { Codigo = Codigo + "|" + TextBox237.Text; Descripcion = Descripcion + "|" + TextBox438.Text; }
        if (TextBox244.Text != "") { Codigo = Codigo + "|" + TextBox244.Text; Descripcion = Descripcion + "|" + TextBox439.Text; }
        if (TextBox251.Text != "") { Codigo = Codigo + "|" + TextBox251.Text; Descripcion = Descripcion + "|" + TextBox440.Text; }
        if (TextBox258.Text != "") { Codigo = Codigo + "|" + TextBox258.Text; Descripcion = Descripcion + "|" + TextBox441.Text; }
        if (TextBox265.Text != "") { Codigo = Codigo + "|" + TextBox265.Text; Descripcion = Descripcion + "|" + TextBox442.Text; }
        if (TextBox272.Text != "") { Codigo = Codigo + "|" + TextBox272.Text; Descripcion = Descripcion + "|" + TextBox443.Text; }
        if (TextBox279.Text != "") { Codigo = Codigo + "|" + TextBox279.Text; Descripcion = Descripcion + "|" + TextBox444.Text; }
        if (TextBox286.Text != "") { Codigo = Codigo + "|" + TextBox286.Text; Descripcion = Descripcion + "|" + TextBox445.Text; }
        if (TextBox293.Text != "") { Codigo = Codigo + "|" + TextBox293.Text; Descripcion = Descripcion + "|" + TextBox446.Text; }
        if (TextBox300.Text != "") { Codigo = Codigo + "|" + TextBox300.Text; Descripcion = Descripcion + "|" + TextBox447.Text; }
        if (TextBox307.Text != "") { Codigo = Codigo + "|" + TextBox307.Text; Descripcion = Descripcion + "|" + TextBox448.Text; }
        return Codigo + ";" + Descripcion;
    }
    protected bool enviar(string Para, string Mensaje, string Asunto, string ruta)
    {
        string msg = "";
        bool bandera = false;
        try
        {
            Correo correo = new Correo();
            correo.Para = Para;
            correo.Mensaje = Mensaje;
            correo.Asunto = Asunto;
            //correo.De = ConfigurationManager.AppSettings["mailSoporte"];
            //string path = Server.MapPath(@"..\AUTPDF\anexo2.pdf"); ;
            string[] result = new string[] { ruta };
            correo.Adjuntos = result;
            if (correo.Enviar())
            { bandera = true; }
            return bandera;
        }
        catch
        {
            msg = "73";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            return bandera;
        }
    }
    protected string textoTilde(string texto)
    {
        string nuevoTexto = "";
        string valor = "";
        for (int i = 0; i < texto.Length; i++)
        {
            valor = texto.Substring(i, 1);
            if (valor == "á") { valor = "&#225"; } else if (valor == "Á") { valor = "&#193"; }
            if (valor == "é") { valor = "&#233"; } else if (valor == "É") { valor = "&#201"; }
            if (valor == "í") { valor = "&#237"; } else if (valor == "Í") { valor = "&#205"; }
            if (valor == "ó") { valor = "&#243"; } else if (valor == "Ó") { valor = "&#211"; }
            if (valor == "ú") { valor = "&#250"; } else if (valor == "Ú") { valor = "&#218"; }
            if (valor == "ñ") { valor = "&#241"; } else if (valor == "Ñ") { valor = "&#209"; }
            nuevoTexto = nuevoTexto + valor;
        }
        return nuevoTexto;
    }
    protected string valorTamano()
    {
        int tam = 20;
        int maxCaracter = 100;
        if (TextBox449.Text.Length > maxCaracter)
        {
            while (TextBox449.Text.Length > maxCaracter)
            {
                maxCaracter = maxCaracter + 100;
                tam = tam + 20;
            }
        }
        return tam.ToString();
    }
    protected string retornarHtml()
    {
        string tam = valorTamano();
        string[] tipoId = { "", "", "", "", "", "", "" };
        string[] cobertura = { "", "", "", "", "", "", "", "" };
        string[] Atencion = { "", "", "", "", "" };
        string[] Servicio = { "", "" };
        string[] Prioridad = { "", "" };
        string[] Ubicacion = { "", "", "" };
        if (rblTipoId.SelectedValue == "2") { tipoId[0] = "X"; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "3") { tipoId[0] = ""; tipoId[1] = "X"; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "4") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = "X"; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "5") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = "X"; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "P") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = "X"; tipoId[5] = ""; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "A") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = "X"; tipoId[6] = ""; }
        if (rblTipoId.SelectedValue == "M") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = "X"; }
        if (rblCobertura.SelectedValue == "1") { cobertura[0] = "X"; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "2") { cobertura[0] = ""; cobertura[1] = "X"; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "3") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = "X"; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "4") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = "X"; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "5") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = "X"; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "6") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = "X"; cobertura[6] = ""; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "7") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = "X"; cobertura[7] = ""; }
        if (rblCobertura.SelectedValue == "8") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = "X"; }
        if (rblAtencion.SelectedValue == "13") { Atencion[0] = "X"; Atencion[1] = ""; Atencion[2] = ""; Atencion[3] = ""; Atencion[4] = ""; }
        if (rblAtencion.SelectedValue == "01") { Atencion[0] = ""; Atencion[1] = "X"; Atencion[2] = ""; Atencion[3] = ""; Atencion[4] = ""; }
        if (rblAtencion.SelectedValue == "06") { Atencion[0] = ""; Atencion[1] = ""; Atencion[2] = "X"; Atencion[3] = ""; Atencion[4] = ""; }
        if (rblAtencion.SelectedValue == "14") { Atencion[0] = ""; Atencion[1] = ""; Atencion[2] = ""; Atencion[3] = "X"; Atencion[4] = ""; }
        if (rblAtencion.SelectedValue == "02") { Atencion[0] = ""; Atencion[1] = ""; Atencion[2] = ""; Atencion[3] = ""; Atencion[4] = "X"; }
        if (rblServicio.SelectedValue=="1") { Servicio[0]="X"; Servicio[1]=""; }
        if (rblServicio.SelectedValue=="2") { Servicio[0]=""; Servicio[1]="X"; }
        if (rblPrioridad.SelectedValue == "1") { Prioridad[0] = "X"; Prioridad[1] = ""; }
        if (rblPrioridad.SelectedValue == "2") { Prioridad[0] = ""; Prioridad[1] = "X"; }
        if (rblUbicacion.SelectedValue == "2") { Ubicacion[0] = "X"; Ubicacion[1] = ""; Ubicacion[2] = ""; }
        if (rblUbicacion.SelectedValue == "3") { Ubicacion[0] = ""; Ubicacion[1] = "X"; Ubicacion[2] = ""; }
        if (rblUbicacion.SelectedValue == "1") { Ubicacion[0] = ""; Ubicacion[1] = ""; Ubicacion[2] = "X"; }
        string path = Server.MapPath("../img/escudocol.png");
        string html = " <table style='width: 975px; height: 96px;'> " +
        "     <tr>  " +
        "         <td rowspan='5' style='width: 63px'>  " +

        "             <img src='" + path + "' style='height:84px;width:121px;' />  " +
        //"             <img id='Image1' src='~/img/escudocol.png' style='height:77px;width:102px;' />  " +
        "         </td>  " +
        "     </tr>  " +
        "     <tr>  " +
        "         <td align='center' colspan='6'><B>MINISTERIO DE LA PROTECCI&#211N SOCIAL</B></td>  " +
        "     </tr>  " +
        "     <tr>  " +
        "         <td colspan='6'>&nbsp;</td>  " +
        "     </tr>  " +
        "     <tr>  " +
        "         <td align='center' colspan='6'><B><font SIZE=4>SOLICITUD DE AUTORIZACI&#211N DE SERVICIOS DE SALUD</font></B></td>  " +
        "     </tr>  " +
        "     <tr>  " +
        "         <td align='center' style='width: auto'><b>N&#218MERO DE SOLICITUD</b></td>  " +
        "         <td align='center' style='width: auto'>  " +
        "             <input name='TextBox1' type='text' maxlength='1' id='TextBox1' style='width:173px;text-align:center' value='" + TextBox1.Text + "' /> " +
        //"             <input name='TextBox2' type='text' maxlength='1' id='TextBox2' style='width:16px;text-align:center' value='" + TextBox2.Text + "' /> " +
        //"             <input name='TextBox3' type='text' maxlength='1' id='TextBox3' style='width:16px;text-align:center' value='" + TextBox3.Text + "' /> " +
        //"             <input name='TextBox4' type='text' maxlength='1' id='TextBox4' style='width:16px;text-align:center' value='" + TextBox4.Text + "' /> " +
        //"             <input name='TextBox5' type='text' maxlength='1' id='TextBox5' style='width:16px;text-align:center' value='" + TextBox5.Text + "' /> " +
        //"            <input name='TextBox6' type='text' maxlength='1' id='TextBox6' style='width:16px;text-align:center'  value='" + TextBox6.Text + "' /> " +
        //"             <input name='TextBox7' type='text' maxlength='1' id='TextBox7' style='width:16px;text-align:center' value='" + TextBox7.Text + "' /> " +
        //"             <input name='TextBox8' type='text' maxlength='1' id='TextBox8' style='width:16px;text-align:center' value='" + TextBox8.Text + "' /> " +
        //"             <input name='TextBox9' type='text' maxlength='1' id='TextBox9' style='width:16px;text-align:center' value='" + TextBox9.Text + "' /> " +
        "         </td> " +
        "         <td align='center' style='width: auto'><b>Fecha:</b></td> " +
        "         <td align='center' style='width: auto'> " +
        "             <input name='TextBox10' type='text' maxlength='1' id='TextBox10' style='width:135px;text-align:center' value='" + TextBox10.Text + "' /> " +
        //"             <input name='TextBox11' type='text' maxlength='1' id='TextBox11' style='width:16px;text-align:center' value='" + TextBox11.Text + "' /> " +
        //"             <input name='TextBox12' type='text' maxlength='1' id='TextBox12' style='width:16px;text-align:center' value='" + TextBox12.Text + "'/> " +
        //"             <input name='TextBox13' type='text' maxlength='1' id='TextBox13' style='width:16px;text-align:center' value='" + TextBox13.Text + "'/> " +
        //"             <input name='TextBox14' type='text' maxlength='1' id='TextBox14' style='width:16px;text-align:center' value='" + TextBox14.Text + "'/> " +
        //"             <input name='TextBox15' type='text' maxlength='1' id='TextBox15' style='width:16px;text-align:center' value='" + TextBox15.Text + "'/> " +
        //"             <input name='TextBox16' type='text' maxlength='1' id='TextBox16' style='width:16px;text-align:center' value='" + TextBox16.Text + "'/> " +
        //"             <input name='TextBox17' type='text' maxlength='1' id='TextBox17' style='width:16px;text-align:center' value='" + TextBox17.Text + "'/> " +
        //"             <input name='TextBox18' type='text' maxlength='1' id='TextBox18' style='width:16px;text-align:center' value='" + TextBox18.Text + "'/> " +
        //"             <input name='TextBox19' type='text' maxlength='1' id='TextBox19' style='width:16px;text-align:center' value='" + TextBox19.Text + "'/> " +
        "         </td> " +
        "         <td align='center' style='width: auto'><b>Hora:</b></td> " +
        "         <td align='center' style='width: auto'> " +
        "             <input name='TextBox20' type='text' maxlength='1' id='TextBox20' style='width:90px;text-align:center' value='" + TextBox20.Text + "' /> " +
        //"             <input name='TextBox21' type='text' maxlength='1' id='TextBox21' style='width:16px;text-align:center' value='" + TextBox21.Text + "' /> " +
        //"             <input name='TextBox22' type='text' maxlength='1' id='TextBox22' style='width:16px;text-align:center' value='" + TextBox22.Text + "' /> " +
        //"             <input name='TextBox23' type='text' maxlength='1' id='TextBox23' style='width:16px;text-align:center' value='" + TextBox23.Text + "' /> " +
        //"             <input name='TextBox24' type='text' maxlength='1' id='TextBox24' style='width:16px;text-align:center' value='" + TextBox24.Text + "' /> " +
        "         </td> " +
        "     </tr> " +
        " </table>" +
        " <table style='width: 975px'> " +
"     <tr>  " +
"         <td colspan='6'><b>INFORMACI&#211N DEL PRESTADOR (Solicitante)</b></td> " +
"     </tr> " +
"     <tr> " +
"         <td style='width: auto'><b>Nombre</b></td> " +
"         <td><b>NIT</b></td> " +
"         <td> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox25' type='text' value='X' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox25' disabled='disabled' style='text-decoration:none;width:16px;text-align:center' /> " +
"         </td>  " +
"         <td>&nbsp;&nbsp;&nbsp;</td> " +
"         <td colspan='2'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox26' type='text' value='8' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox26' disabled='disabled' style='width:16px;text-align:center' />  " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox27' type='text' value='9' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox27' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox28' type='text' value='0' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox28' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox29' type='text' value='9' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox29' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox30' type='text' value='8' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox30' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox31' type='text' value='2' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox31' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox32' type='text' value='6' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox32' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox33' type='text' value='0' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox33' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox34' type='text' value='8' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox34' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox35' type='text' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox35' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox36' type='text' value='-' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox36' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox37' type='text' value='1' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox37' disabled='disabled' style='width:16px;text-align:center' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox38' type='text' value='CORPORACI&#211N PARA ESTUDIOS EN SALUD CLINICA CES' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox38' disabled='disabled' style='width:566px;' /> " +
"         </td> " +
"         <td><b>CC</b></td> " +
"         <td> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox39' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox39' style='width:16px;text-align:center' readonly='readonly' disabled='disabled' /> " +
"             </td> " +
"         <td>&nbsp;</td> " +
"         <td>Numero:</td> " +
"         <td> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox41' type='text' id='ctl00_ContentPlaceHolder1_TextBox41' style='width:249px;' readonly='readonly' disabled='disabled' /> " +
"         </td> " +
"     </tr> " +
" </table>" +
" <table style='width: 975px'> " +
"     <tr> " +
"         <td style='width: 64px'><b>C&#243digo:</b></td> " +
"         <td style='width: 326px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox42' type='text' value='0' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox42' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox43' type='text' value='5' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox43' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox44' type='text' value='0' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox44' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox45' type='text' value='0' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox45' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox46' type='text' value='1' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox46' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox47' type='text' value='0' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox47' disabled='disabled' style='width:16px;text-align:center' /> " +
 "            <input name='ctl00$ContentPlaceHolder1$TextBox48' type='text' value='2' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox48' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox49' type='text' value='1' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox49' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox50' type='text' value='2' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox50' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox51' type='text' value='4' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox51' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox52' type='text' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox52' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox53' type='text' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox53' disabled='disabled' style='width:16px;text-align:center' /> " +
"             </td> " +
"         <td><b>Direcci&#243n Prestador:</b></td> " +
"     </tr> " +
"     </table>   " +
"     <table style='width: 975px'> " +
"     <tr> " +
"         <td rowspan='2' style='width: 64px'><b>Tel&#233fono:</b></td> " +
 "        <td style='width: 191px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox54' type='text' value='4' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox54' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox55' type='text' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox55' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox56' type='text' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox56' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox57' type='text' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox57' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox58' type='text' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox58' disabled='disabled' style='width:16px;text-align:center' /> " +
"             </td> " +
"         <td style='width: 277px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox59' type='text' value='5' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox59' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox60' type='text' value='7' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox60' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox61' type='text' value='6' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox61' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox62' type='text' value='7' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox62' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox63' type='text' value='2' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox63' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox64' type='text' value='7' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox64' disabled='disabled' style='width:16px;text-align:center' /> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox65' type='text' value='2' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox65' disabled='disabled' style='width:16px;text-align:center' /> " +
"             </td> " +
"         <td colspan='9'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox66' type='text' value='" + TextBox66.Text + "' id='ctl00_ContentPlaceHolder1_TextBox66' style='width:563px;' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='width: 191px' align='center'>Indicativo</td> " +
"         <td style='width: 277px' align='center'>N&#250mero</td> " +
"         <td style='width: auto'><b>Departamento:</b></td> " +
"         <td> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox67' type='text' value='ANTIOQUIA' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox67' disabled='disabled' style='width:130px;' /> " +
"         </td> " +
"         <td bgcolor='Gray'> " +
"             &nbsp;</td> " +
"         <td bgcolor='Gray'>&nbsp;</td> " +
"         <td><b>Municipio:</b></td> " +
"         <td> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox68' type='text' value='MEDELL&#205N' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox68' disabled='disabled' style='width:130px;' /> " +
"         </td> " +
"        <td bgcolor='Gray'>&nbsp;</td> " +
"        <td bgcolor='Gray'>&nbsp;</td> " +
"        <td bgcolor='Gray'>&nbsp;</td> " +
"    </tr> " +
"    </table>  " +
 "   <table style='width: 975px'> " +
"    <tr> " +
"        <td style='height: 26px;'><b>ENTIDAD A LA QUE SE LE SOLICITA (Pagador)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td> " +
"        <td style='height: 26px'>&nbsp;<b>C&#211DIGO:</b></td> " +
"        <td style='height: 26px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox69' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox69' style='width:99px;text-align:center' value='" + textoTilde(TextBox69.Text) + "' />  " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox70' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox70' style='width:16px;text-align:center' value='" + TextBox70.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox71' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox71' style='width:16px;text-align:center' value='" + TextBox71.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox72' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox72' style='width:16px;text-align:center' value='" + TextBox72.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox73' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox73' style='width:16px;text-align:center' value='" + TextBox73.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox74' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox74' style='width:16px;text-align:center' value='" + TextBox74.Text + "' /> " +
"            </td> " +
"    </tr> " +
"    </table> " +
"    <table style='width: 975px'> " +
"    <tr> " +
"        <td colspan='12' align='center'><B>DATOS DEL PACIENTE</b></td> " +
"    </tr> " +
"    <tr> " +
"        <td colspan='3'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox75' type='text' id='ctl00_ContentPlaceHolder1_TextBox75' style='width:233px;' value='" + textoTilde(TextBox75.Text) + "' /> " +
"        </td> " +
"        <td colspan='3'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox76' type='text' id='ctl00_ContentPlaceHolder1_TextBox76' style='width:233px;' value='" + textoTilde(TextBox76.Text) + "' /> " +
"        </td> " +
"        <td colspan='3'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox77' type='text' id='ctl00_ContentPlaceHolder1_TextBox77' style='width:233px;' value='" + textoTilde(TextBox77.Text) + "' /> " +
"        </td> " +
"        <td colspan='3'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox78' type='text' id='ctl00_ContentPlaceHolder1_TextBox78' style='width:233px;' value='" + textoTilde(TextBox78.Text) + "' /> " +
"        </td> " +
"    </tr> " +
 "   <tr> " +
 "       <td style='height: 23px' colspan='3'><b>Primer Apellido</b></td> " +
"        <td style='height: 23px' colspan='3'><b>Segundo Apellido</b></td> " +
"        <td style='height: 23px' colspan='3'><b>Primer Nombre</b></td> " +
"        <td style='height: 23px' colspan='3'><b>Segundo Nombre</b></td> " +
"    </tr> " +
"    <tr> " +
"        <td colspan='12'><b>Tipo Documento de Identificaci&#243n</b></td> " +
"    </tr> " +
"    <tr> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox79' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox79' style='width:16px;text-align:center' value='" + tipoId[0] + "' /> " +
"            </td> " +
"        <td style='width: 145px'>Registro Civil</td> " +
"        <td colspan='2'>&nbsp;</td> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox80' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox80' style='width:16px;text-align:center' value='" + tipoId[4] + "' /> " +
"            </td> " +
"        <td>Pasaporte</td> " +
 "       <td>&nbsp;</td> " +
 "       <td colspan='3'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox81' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox81' style='width:280px;text-align:center' value='" + TextBox81.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox82' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox82' style='width:16px;text-align:center' value='" + TextBox82.Text + "' /> " +
// "           <input name='ctl00$ContentPlaceHolder1$TextBox83' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox83' style='width:16px;text-align:center' value='" + TextBox83.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox84' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox84' style='width:16px;text-align:center' value='" + TextBox84.Text + "' /> " +
// "           <input name='ctl00$ContentPlaceHolder1$TextBox85' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox85' style='width:16px;text-align:center' value='" + TextBox85.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox86' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox86' style='width:16px;text-align:center' value='" + TextBox86.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox87' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox87' style='width:16px;text-align:center' value='" + TextBox87.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox88' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox88' style='width:16px;text-align:center' value='" + TextBox88.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox89' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox89' style='width:16px;text-align:center' value='" + TextBox89.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox90' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox90' style='width:16px;text-align:center' value='" + TextBox90.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox91' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox91' style='width:16px;text-align:center' value='" + TextBox91.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox92' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox92' style='width:16px;text-align:center' value='" + TextBox92.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox93' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox93' style='width:16px;text-align:center' value='" + TextBox93.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox94' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox94' style='width:16px;text-align:center' value='" + TextBox94.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox95' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox95' style='width:16px;text-align:center' value='" + TextBox95.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox96' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox96' style='width:16px;text-align:center' value='" + TextBox96.Text + "' /> " +
"            </td> " +
"        <td>&nbsp;</td> " +
"        <td>&nbsp;</td> " +
"    </tr> " +
"    <tr> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox97' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox97' style='width:16px;text-align:center'  value='" + tipoId[1] + "' /> " +
"            </td> " +
"        <td style='width: 145px'>Tarjeta de Identidad</td> " +
"        <td colspan='2'>&nbsp;</td> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox98' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox98' style='width:16px;text-align:center'  value='" + tipoId[5] + "' /> " +
"            </td> " +
"        <td>Adulto sin Identificaci&#243n</td> " +
"        <td>&nbsp;</td> " +
"        <td align='center' colspan='3'>N&#250mero Documento de Identificaci&#243n</td> " +
"        <td>&nbsp;</td> " +
"        <td>&nbsp;</td> " +
"    </tr> " +
"    <tr> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox99' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox99' style='width:16px;text-align:center'  value='" + tipoId[2] + "' /> " +
"            </td> " +
"        <td style='width: 145px'>C&#233dula de Ciudadan&#237a</td> " +
"        <td colspan='2'>&nbsp;</td> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox100' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox100' style='width:16px;text-align:center' value='" + tipoId[6] + "' /> " +
"            </td> " +
"        <td>Menor sin Identificaci&#243n</td> " +
"        <td>&nbsp;</td> " +
"        <td>&nbsp;</td> " +
"        <td colspan='2'>&nbsp;</td> " +
"        <td>&nbsp;</td> " +
"        <td>&nbsp;</td> " +
"    </tr> " +
"    <tr> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox101' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox101' style='width:16px;text-align:center' value='" + tipoId[3] + "' /> " +
"            </td> " +
"        <td style='width: 145px'>C&#233dula de Extranjer&#237a</td> " +
"        <td colspan='2'>&nbsp;</td> " +
"        <td>&nbsp;</td> " +
"        <td>&nbsp;</td> " +
"        <td>&nbsp;</td> " +
"        <td><b>Fecha de Nacimiento:</b></td> " +
"        <td colspan='2'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox102' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox102' style='width:163px;text-align:center' value='" + TextBox102.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox103' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox103' style='width:16px;text-align:center' value='" + TextBox103.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox104' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox104' style='width:16px;text-align:center' value='" + TextBox104.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox105' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox105' style='width:16px;text-align:center' value='" + TextBox105.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox106' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox106' style='width:16px;text-align:center' value='" + TextBox106.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox107' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox107' style='width:16px;text-align:center' value='" + TextBox107.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox108' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox108' style='width:16px;text-align:center' value='" + TextBox108.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox109' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox109' style='width:16px;text-align:center' value='" + TextBox109.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox110' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox110' style='width:16px;text-align:center' value='" + TextBox110.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox111' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox111' style='width:16px;text-align:center' value='" + TextBox111.Text + "' /> " +
"            </td> " +
"        <td>&nbsp;</td> " +
"        <td>&nbsp;</td> " +
"    </tr> " +
"    </table>     " +
"    <table style='width: 975px'> " +
"    <tr> " +
"        <td><b>Direcci&#243n de Residencia Habitual:</b></td> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox112' type='text' id='ctl00_ContentPlaceHolder1_TextBox112' style='width:419px;' value='" + TextBox112.Text + "' /> " +
"        </td> " +
"        <td><b>Tel&#233fono:</b></td> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox113' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox113' style='width:160px;text-align:center' value='" + TextBox113.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox114' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox114' style='width:16px;text-align:center' value='" + TextBox114.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox115' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox115' style='width:16px;text-align:center' value='" + TextBox115.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox116' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox116' style='width:16px;text-align:center' value='" + TextBox116.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox117' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox117' style='width:16px;text-align:center' value='" + TextBox117.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox118' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox118' style='width:16px;text-align:center' value='" + TextBox118.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox119' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox119' style='width:16px;text-align:center' value='" + TextBox119.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox120' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox120' style='width:16px;text-align:center' value='" + TextBox120.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox121' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox121' style='width:16px;text-align:center' value='" + TextBox121.Text + "' /> " +
"            </td> " +
"    </tr> " +
"    </table>     " +
"    <table style='width: 975px'> " +
"    <tr> " +
"        <td><b>Departamento:</b></td> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox123' type='text' id='ctl00_ContentPlaceHolder1_TextBox123' style='width:240px;' value='" + TextBox123.Text + "' /> " +
"        </td> " +
"        <td bgcolor='Gray'>&nbsp;</td> " +
"        <td bgcolor='Gray'>&nbsp;</td> " +
"        <td><b>Municipio:</b></td> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox124' type='text' id='ctl00_ContentPlaceHolder1_TextBox124' style='width:240px;' value='" + TextBox124.Text + "' /> " +
"        </td> " +
"        <td bgcolor='Gray'>&nbsp;</td> " +
"        <td bgcolor='Gray'>&nbsp;</td> " +
"        <td bgcolor='Gray'>&nbsp;</td> " +
"    </tr> " +
"    </table>    " +
"    <table style='width: 975px'> " +
"    <tr> " +
"        <td><b>Tel&#233fono Celular:</b></td> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox125' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox125' style='width:186px;text-align:center' value='" + TextBox125.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox126' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox126' style='width:16px;text-align:center' value='" + TextBox126.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox127' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox127' style='width:16px;text-align:center' value='" + TextBox127.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox128' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox128' style='width:16px;text-align:center' value='" + TextBox128.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox129' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox129' style='width:16px;text-align:center' value='" + TextBox129.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox130' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox130' style='width:16px;text-align:center' value='" + TextBox130.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox131' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox131' style='width:16px;text-align:center' value='" + TextBox131.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox132' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox132' style='width:16px;text-align:center' value='" + TextBox132.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox133' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox133' style='width:16px;text-align:center' value='" + TextBox133.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox134' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox134' style='width:16px;text-align:center' value='" + TextBox134.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox135' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox135' style='width:16px;text-align:center' value='" + TextBox135.Text + "' /> " +
"            </td> " +
"        <td><b>Correo Electr&#243nico:</b></td> " +
"        <td> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox136' type='text' id='ctl00_ContentPlaceHolder1_TextBox136' style='width:434px;'  value='" + TextBox136.Text + "' /> " +
"        </td> " +
"    </tr> " +
"    </table>    " +
"    <table style='width: 975px'> " +
"    <tr> " +
"        <td colspan='8' style='height: 23px'><b>Cobertura en Salud</b></td> " +
"    </tr> " +
"    <tr> " +
"       <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox137' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox137' style='width:16px;text-align:center' value='" + cobertura[0] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>R&#233gimen Contributivo</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox138' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox138' style='width:16px;text-align:center' value='" + cobertura[1] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>R&#233gimen Subsidiado - Parcial</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox139' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox139' style='width:16px;text-align:center' value='" + cobertura[2] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Poblaci&#243n Pobre No Asegurada Sin SISBEN</td> " +
"        <td style='height: 23px'> " +
"           <input name='ctl00$ContentPlaceHolder1$TextBox140' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox140' style='width:16px;text-align:center' value='" + cobertura[3] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Plan Adicional de Salud</td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox141' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox141' style='width:16px;text-align:center' value='" + cobertura[4] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>R&#233gimen Subsidiado - Total</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox142' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox142' style='width:16px;text-align:center' value='" + cobertura[5] + "' /> " +
  "          </td> " +
  "      <td style='height: 23px'>Poblaci&#243n Pobre No Asegurada Con SISBEN</td> " +
  "      <td style='height: 23px'> " +
    "        <input name='ctl00$ContentPlaceHolder1$TextBox143' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox143' style='width:16px;text-align:center' value='" + cobertura[6] + "' /> " +
   "         </td> " +
"        <td style='height: 23px'>Desplazado</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox144' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox144' style='width:16px;text-align:center' value='" + cobertura[7] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Otro</td> " +
"    </tr> " +
"    </table>     " +
"    <table style='width: 975px'> " +
"    <tr> " +
"        <td style='height: 23px' align='center' colspan='10'><B>INFORMACI&#211N DE LA ATENCI&#211N Y SERVICIOS SOLICITADOS</B></td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px' colspan='2'><b>Origen de la atenci&#243n</b></td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px' colspan='2'><b>Tipo de servicios solicitados</b></td> " +
"        <td style='height: 23px' colspan='2'><b>Prioridad de la atenci&#243n</b></td> " +
"    </tr> " +
 "   <tr> " +
 "       <td style='height: 23px'> " +
 "           <input name='ctl00$ContentPlaceHolder1$TextBox145' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox145' style='width:16px;text-align:center' value='" + Atencion[0] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Enfermedad General</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox146' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox146' style='width:16px;text-align:center' value='" + Atencion[1] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Accidente de trabajo</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox147' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox147' style='width:16px;text-align:center' value='" + Atencion[2] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Evento Catastr&#243fico</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox148' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox148' style='width:16px;text-align:center' value='" + Servicio[0] + "' /> " +
"            &nbsp;</td> " +
"        <td style='height: 23px'>Posterior a la atenci&#243n inicial de urgencias</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox149' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox149' style='width:16px;text-align:center' value='" + Prioridad[0] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Prioritaria</td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox150' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox150' style='width:16px;text-align:center' value='" + Atencion[3] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Enfermedad profesional</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox151' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox151' style='width:16px;text-align:center' value='" + Atencion[4] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Accidente de Tr&#225nsito</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox153' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox153' style='width:16px;text-align:center' value='" + Servicio[1] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Servicios Electivos</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox154' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox154' style='width:16px;text-align:center' value='" + Prioridad[1] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>No Prioritaria</td> " +
"    </tr> " +
"    </table>     " +
"    <table style='width: 975px'> " +
"    <tr> " +
"        <td style='height: 23px' colspan='8'><b>Ubicaci&#243n del paciente al momento de la solicitud de autorizacion</b></td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox155' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox155' style='width:16px;text-align:center'  value='" + Ubicacion[0] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Consulta Externa</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox156' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox156' style='width:16px;text-align:center'  value='" + Ubicacion[1] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Hospitalizaci&#243n</td> " +
"        <td style='height: 23px'>Servicio</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox164' type='text' id='ctl00_ContentPlaceHolder1_TextBox164' style='width:286px;'  value='" + TextBox164.Text + "' /> " +
"        </td> " +
"        <td style='height: 23px'>Cama</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox157' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox157' style='width:72px;text-align:center' value='" + TextBox157.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox158' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox158' style='width:16px;text-align:center' value='" + TextBox158.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox159' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox159' style='width:16px;text-align:center' value='" + TextBox159.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox160' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox160' style='width:16px;text-align:center' value='" + TextBox160.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox161' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox161' style='width:16px;text-align:center' value='" + TextBox161.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox162' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox162' style='width:16px;text-align:center' value='" + TextBox162.Text + "' /> " +
"            </td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox163' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox163' style='width:16px;text-align:center' value='" + Ubicacion[2] + "' /> " +
"            </td> " +
"        <td style='height: 23px'>Urgencias</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"    </tr> " +
"    </table>   " +
"    <table style='width: 975px'> " +
"    <tr> " +
"        <td style='height: 23px'><b></b>Manejo integral segun gu&#237a de:</b></td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox165' type='text' id='ctl00_ContentPlaceHolder1_TextBox165' style='width:766px;'  value='" + TextBox165.Text.ToUpper().Trim() + "' /> " +
"        </td> " +
"    </tr> " +
"    </table> " +
"    <table style='width: 975px'> " +
"    <tr> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'><b>C&#243digo CUPS</b></td> " +
"        <td style='height: 23px'><b>Cantidad</b></td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'><b>Descripci&#243n</b></td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'>1</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox166' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox166' style='width:160px;text-align:center' value='" + TextBox166.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox167' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox167' style='width:16px;text-align:center' value='" + TextBox167.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox168' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox168' style='width:16px;text-align:center' value='" + TextBox168.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox169' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox169' style='width:16px;text-align:center' value='" + TextBox169.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox170' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox170' style='width:16px;text-align:center' value='" + TextBox170.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox171' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox171' style='width:16px;text-align:center' value='" + TextBox171.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox172' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox172' style='width:16px;text-align:center' value='" + TextBox172.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox174' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox174' style='width:65px;text-align:center' value='" + TextBox174.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox175' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox175' style='width:16px;text-align:center' value='" + TextBox175.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox176' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox176' style='width:16px;text-align:center' value='" + TextBox176.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox173' type='text' id='ctl00_ContentPlaceHolder1_TextBox173' style='width:674px;' value='" + TextBox173.Text.ToUpper().Trim() + "' /> " +
"        </td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'>2</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox177' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox177' style='width:160px;text-align:center' value='" + TextBox177.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox178' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox178' style='width:16px;text-align:center' value='" + TextBox178.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox179' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox179' style='width:16px;text-align:center' value='" + TextBox179.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox180' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox180' style='width:16px;text-align:center' value='" + TextBox180.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox181' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox181' style='width:16px;text-align:center' value='" + TextBox181.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox182' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox182' style='width:16px;text-align:center' value='" + TextBox182.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox183' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox183' style='width:16px;text-align:center' value='" + TextBox183.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox184' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox184' style='width:65px;text-align:center' value='" + TextBox184.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox185' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox185' style='width:16px;text-align:center' value='" + TextBox185.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox186' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox186' style='width:16px;text-align:center' value='" + TextBox186.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox450' type='text' id='ctl00_ContentPlaceHolder1_TextBox450' style='width:674px;' value='" + TextBox450.Text.ToUpper().Trim() + "' /> " +
"        </td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'>3</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox188' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox188' style='width:160px;text-align:center' value='" + TextBox188.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox189' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox189' style='width:16px;text-align:center' value='" + TextBox189.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox190' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox190' style='width:16px;text-align:center' value='" + TextBox190.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox191' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox191' style='width:16px;text-align:center' value='" + TextBox191.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox192' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox192' style='width:16px;text-align:center' value='" + TextBox192.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox193' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox193' style='width:16px;text-align:center' value='" + TextBox193.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox194' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox194' style='width:16px;text-align:center' value='" + TextBox194.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox314' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox314' style='width:65px;text-align:center' value='" + TextBox314.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox315' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox315' style='width:16px;text-align:center' value='" + TextBox315.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox316' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox316' style='width:16px;text-align:center' value='" + TextBox316.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox431' type='text' id='ctl00_ContentPlaceHolder1_TextBox431' style='width:674px;' value='" + TextBox431.Text.ToUpper().Trim() + "' /> " +
"        </td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'>4</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox195' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox195' style='width:160px;text-align:center' value='" + TextBox195.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox196' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox196' style='width:16px;text-align:center' value='" + TextBox196.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox197' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox197' style='width:16px;text-align:center' value='" + TextBox197.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox198' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox198' style='width:16px;text-align:center' value='" + TextBox198.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox199' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox199' style='width:16px;text-align:center' value='" + TextBox199.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox200' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox200' style='width:16px;text-align:center' value='" + TextBox200.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox201' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox201' style='width:16px;text-align:center' value='" + TextBox201.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox317' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox317' style='width:65px;text-align:center' value='" + TextBox317.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox318' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox318' style='width:16px;text-align:center' value='" + TextBox318.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox319' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox319' style='width:16px;text-align:center' value='" + TextBox319.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox432' type='text' id='ctl00_ContentPlaceHolder1_TextBox432' style='width:674px;' value='" + TextBox432.Text.ToUpper().Trim() + "' /> " +
"        </td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'>5</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox202' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox202' style='width:160px;text-align:center' value='" + TextBox202.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox203' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox203' style='width:16px;text-align:center' value='" + TextBox203.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox204' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox204' style='width:16px;text-align:center' value='" + TextBox204.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox205' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox205' style='width:16px;text-align:center' value='" + TextBox205.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox206' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox206' style='width:16px;text-align:center' value='" + TextBox206.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox207' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox207' style='width:16px;text-align:center' value='" + TextBox207.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox208' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox208' style='width:16px;text-align:center' value='" + TextBox208.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox320' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox320' style='width:65px;text-align:center' value='" + TextBox320.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox321' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox321' style='width:16px;text-align:center' value='" + TextBox321.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox322' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox322' style='width:16px;text-align:center' value='" + TextBox322.Text + "' /> " +
"            </td> " +
"        <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox433' type='text' id='ctl00_ContentPlaceHolder1_TextBox433' style='width:674px;' value='" + TextBox433.Text.ToUpper().Trim() + "' /> " +
"        </td> " +
"    </tr> " +
"    <tr> " +
"        <td style='height: 23px'>6</td> " +
"        <td style='height: 23px'> " +
"            <input name='ctl00$ContentPlaceHolder1$TextBox209' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox209' style='width:160px;text-align:center' value='" + TextBox209.Text.ToUpper().Trim() + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox210' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox210' style='width:16px;text-align:center' value='" + TextBox210.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox211' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox211' style='width:16px;text-align:center' value='" + TextBox211.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox212' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox212' style='width:16px;text-align:center' value='" + TextBox212.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox213' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox213' style='width:16px;text-align:center' value='" + TextBox213.Text + "' /> " +
//"            <input name='ctl00$ContentPlaceHolder1$TextBox214' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox214' style='width:16px;text-align:center' value='" + TextBox214.Text + "' /> " +
// "           <input name='ctl00$ContentPlaceHolder1$TextBox215' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox215' style='width:16px;text-align:center' value='" + TextBox215.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox323' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox323' style='width:65px;text-align:center' value='" + TextBox323.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox324' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox324' style='width:16px;text-align:center' value='" + TextBox324.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox325' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox325' style='width:16px;text-align:center' value='" + TextBox325.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox434' type='text' id='ctl00_ContentPlaceHolder1_TextBox434' style='width:674px;' value='" + TextBox434.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>7</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox216' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox216' style='width:160px;text-align:center' value='" + TextBox216.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox217' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox217' style='width:16px;text-align:center' value='" + TextBox217.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox218' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox218' style='width:16px;text-align:center' value='" + TextBox218.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox219' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox219' style='width:16px;text-align:center' value='" + TextBox219.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox220' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox220' style='width:16px;text-align:center' value='" + TextBox220.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox221' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox221' style='width:16px;text-align:center' value='" + TextBox221.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox222' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox222' style='width:16px;text-align:center' value='" + TextBox222.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox326' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox326' style='width:65px;text-align:center' value='" + TextBox326.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox327' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox327' style='width:16px;text-align:center' value='" + TextBox327.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox328' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox328' style='width:16px;text-align:center' value='" + TextBox328.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox435' type='text' id='ctl00_ContentPlaceHolder1_TextBox435' style='width:674px;' value='" + TextBox435.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>8</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox223' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox223' style='width:160px;text-align:center' value='" + TextBox223.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox224' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox224' style='width:16px;text-align:center' value='" + TextBox224.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox225' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox225' style='width:16px;text-align:center' value='" + TextBox225.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox226' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox226' style='width:16px;text-align:center' value='" + TextBox226.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox227' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox227' style='width:16px;text-align:center' value='" + TextBox227.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox228' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox228' style='width:16px;text-align:center' value='" + TextBox228.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox229' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox229' style='width:16px;text-align:center' value='" + TextBox229.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox329' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox329' style='width:65px;text-align:center' value='" + TextBox329.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox330' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox330' style='width:16px;text-align:center' value='" + TextBox330.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox331' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox331' style='width:16px;text-align:center' value='" + TextBox331.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox436' type='text' id='ctl00_ContentPlaceHolder1_TextBox436' style='width:674px;' value='" + TextBox436.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>9</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox230' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox230' style='width:160px;text-align:center' value='" + TextBox230.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox231' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox231' style='width:16px;text-align:center' value='" + TextBox231.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox232' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox232' style='width:16px;text-align:center' value='" + TextBox232.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox233' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox233' style='width:16px;text-align:center' value='" + TextBox233.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox234' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox234' style='width:16px;text-align:center' value='" + TextBox234.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox235' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox235' style='width:16px;text-align:center' value='" + TextBox235.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox236' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox236' style='width:16px;text-align:center' value='" + TextBox236.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox332' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox332' style='width:65px;text-align:center' value='" + TextBox332.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox333' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox333' style='width:16px;text-align:center' value='" + TextBox333.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox334' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox334' style='width:16px;text-align:center' value='" + TextBox334.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox437' type='text' id='ctl00_ContentPlaceHolder1_TextBox437' style='width:674px;' value='" + TextBox437.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>10</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox237' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox237' style='width:160px;text-align:center' value='" + TextBox237.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox238' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox238' style='width:16px;text-align:center' value='" + TextBox238.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox239' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox239' style='width:16px;text-align:center' value='" + TextBox239.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox240' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox240' style='width:16px;text-align:center' value='" + TextBox240.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox241' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox241' style='width:16px;text-align:center' value='" + TextBox241.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox242' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox242' style='width:16px;text-align:center' value='" + TextBox242.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox243' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox243' style='width:16px;text-align:center' value='" + TextBox243.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox335' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox335' style='width:65px;text-align:center' value='" + TextBox335.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox336' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox336' style='width:16px;text-align:center' value='" + TextBox336.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox337' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox337' style='width:16px;text-align:center' value='" + TextBox337.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox438' type='text' id='ctl00_ContentPlaceHolder1_TextBox438' style='width:674px;'  value='" + TextBox438.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>11</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox244' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox244' style='width:160px;text-align:center' value='" + TextBox244.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox245' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox245' style='width:16px;text-align:center' value='" + TextBox245.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox246' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox246' style='width:16px;text-align:center' value='" + TextBox246.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox247' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox247' style='width:16px;text-align:center' value='" + TextBox247.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox248' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox248' style='width:16px;text-align:center' value='" + TextBox248.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox249' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox249' style='width:16px;text-align:center' value='" + TextBox249.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox250' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox250' style='width:16px;text-align:center' value='" + TextBox250.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox338' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox338' style='width:65px;text-align:center' value='" + TextBox338.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox339' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox339' style='width:16px;text-align:center' value='" + TextBox339.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox340' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox340' style='width:16px;text-align:center' value='" + TextBox340.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox439' type='text' id='ctl00_ContentPlaceHolder1_TextBox439' style='width:674px;'  value='" + TextBox439.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>12</td> " +
 "        <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox251' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox251' style='width:160px;text-align:center' value='" + TextBox251.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox252' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox252' style='width:16px;text-align:center' value='" + TextBox252.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox253' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox253' style='width:16px;text-align:center' value='" + TextBox253.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox254' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox254' style='width:16px;text-align:center' value='" + TextBox254.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox255' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox255' style='width:16px;text-align:center' value='" + TextBox255.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox256' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox256' style='width:16px;text-align:center' value='" + TextBox256.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox257' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox257' style='width:16px;text-align:center' value='" + TextBox257.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox341' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox341' style='width:65px;text-align:center' value='" + TextBox341.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox342' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox342' style='width:16px;text-align:center' value='" + TextBox342.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox343' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox343' style='width:16px;text-align:center' value='" + TextBox343.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox440' type='text' id='ctl00_ContentPlaceHolder1_TextBox440' style='width:674px;' value='" + TextBox440.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>13</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox258' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox258' style='width:160px;text-align:center' value='" + TextBox258.Text.ToUpper().Trim() + "' /> " +
// "            <input name='ctl00$ContentPlaceHolder1$TextBox259' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox259' style='width:16px;text-align:center' value='" + TextBox259.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox260' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox260' style='width:16px;text-align:center' value='" + TextBox260.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox261' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox261' style='width:16px;text-align:center' value='" + TextBox261.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox262' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox262' style='width:16px;text-align:center' value='" + TextBox262.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox263' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox263' style='width:16px;text-align:center' value='" + TextBox263.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox264' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox264' style='width:16px;text-align:center' value='" + TextBox264.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox344' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox344' style='width:65px;text-align:center' value='" + TextBox344.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox345' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox345' style='width:16px;text-align:center' value='" + TextBox345.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox346' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox346' style='width:16px;text-align:center' value='" + TextBox346.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"        <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox441' type='text' id='ctl00_ContentPlaceHolder1_TextBox441' style='width:674px;' value='" + TextBox441.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>14</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox265' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox265' style='width:160px;text-align:center' value='" + TextBox265.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox266' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox266' style='width:16px;text-align:center' value='" + TextBox266.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox267' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox267' style='width:16px;text-align:center' value='" + TextBox267.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox268' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox268' style='width:16px;text-align:center' value='" + TextBox268.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox269' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox269' style='width:16px;text-align:center' value='" + TextBox269.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox270' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox270' style='width:16px;text-align:center' value='" + TextBox270.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox271' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox271' style='width:16px;text-align:center' value='" + TextBox271.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox347' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox347' style='width:65px;text-align:center' value='" + TextBox347.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox348' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox348' style='width:16px;text-align:center' value='" + TextBox348.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox349' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox349' style='width:16px;text-align:center' value='" + TextBox349.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox442' type='text' id='ctl00_ContentPlaceHolder1_TextBox442' style='width:674px;' value='" + TextBox442.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>15</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox272' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox272' style='width:160px;text-align:center' value='" + TextBox272.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox273' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox273' style='width:16px;text-align:center' value='" + TextBox273.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox274' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox274' style='width:16px;text-align:center' value='" + TextBox274.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox275' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox275' style='width:16px;text-align:center' value='" + TextBox275.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox276' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox276' style='width:16px;text-align:center' value='" + TextBox276.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox277' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox277' style='width:16px;text-align:center' value='" + TextBox277.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox278' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox278' style='width:16px;text-align:center' value='" + TextBox278.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox350' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox350' style='width:65px;text-align:center' value='" + TextBox350.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox351' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox351' style='width:16px;text-align:center' value='" + TextBox351.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox352' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox352' style='width:16px;text-align:center' value='" + TextBox352.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
 "        <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox443' type='text' id='ctl00_ContentPlaceHolder1_TextBox443' style='width:674px;' value='" + TextBox443.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>16</td> " +
 "        <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox279' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox279' style='width:160px;text-align:center' value='" + TextBox279.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox280' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox280' style='width:16px;text-align:center' value='" + TextBox280.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox281' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox281' style='width:16px;text-align:center' value='" + TextBox281.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox282' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox282' style='width:16px;text-align:center' value='" + TextBox282.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox283' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox283' style='width:16px;text-align:center' value='" + TextBox283.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox284' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox284' style='width:16px;text-align:center' value='" + TextBox284.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox285' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox285' style='width:16px;text-align:center' value='" + TextBox285.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox353' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox353' style='width:65px;text-align:center' value='" + TextBox353.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox354' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox354' style='width:16px;text-align:center' value='" + TextBox354.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox355' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox355' style='width:16px;text-align:center' value='" + TextBox355.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox444' type='text' id='ctl00_ContentPlaceHolder1_TextBox444' style='width:674px;' value='" + TextBox444.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>17</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox286' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox286' style='width:160px;text-align:center' value='" + TextBox286.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox287' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox287' style='width:16px;text-align:center' value='" + TextBox287.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox288' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox288' style='width:16px;text-align:center' value='" + TextBox288.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox289' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox289' style='width:16px;text-align:center' value='" + TextBox289.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox290' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox290' style='width:16px;text-align:center' value='" + TextBox290.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox291' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox291' style='width:16px;text-align:center' value='" + TextBox291.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox292' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox292' style='width:16px;text-align:center' value='" + TextBox292.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox356' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox356' style='width:65px;text-align:center' value='" + TextBox356.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox357' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox357' style='width:16px;text-align:center' value='" + TextBox357.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox358' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox358' style='width:16px;text-align:center' value='" + TextBox358.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox445' type='text' id='ctl00_ContentPlaceHolder1_TextBox445' style='width:674px;' value='" + TextBox445.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>18</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox293' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox293' style='width:160px;text-align:center' value='" + TextBox293.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox294' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox294' style='width:16px;text-align:center' value='" + TextBox294.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox295' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox295' style='width:16px;text-align:center' value='" + TextBox295.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox296' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox296' style='width:16px;text-align:center' value='" + TextBox296.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox297' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox297' style='width:16px;text-align:center' value='" + TextBox297.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox298' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox298' style='width:16px;text-align:center' value='" + TextBox298.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox299' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox299' style='width:16px;text-align:center' value='" + TextBox299.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox359' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox359' style='width:65px;text-align:center' value='" + TextBox359.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox360' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox360' style='width:16px;text-align:center' value='" + TextBox360.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox361' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox361' style='width:16px;text-align:center' value='" + TextBox361.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox446' type='text' id='ctl00_ContentPlaceHolder1_TextBox446' style='width:674px;' value='" + TextBox446.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>19</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox300' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox300' style='width:160px;text-align:center' value='" + TextBox300.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox301' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox301' style='width:16px;text-align:center' value='" + TextBox301.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox302' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox302' style='width:16px;text-align:center' value='" + TextBox302.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox303' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox303' style='width:16px;text-align:center' value='" + TextBox303.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox304' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox304' style='width:16px;text-align:center' value='" + TextBox304.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox305' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox305' style='width:16px;text-align:center' value='" + TextBox305.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox306' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox306' style='width:16px;text-align:center' value='" + TextBox306.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox362' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox362' style='width:65px;text-align:center' value='" + TextBox362.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox363' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox363' style='width:16px;text-align:center' value='" + TextBox363.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox364' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox364' style='width:16px;text-align:center' value='" + TextBox364.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox447' type='text' id='ctl00_ContentPlaceHolder1_TextBox447' style='width:674px;' value='" + TextBox447.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>20</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox307' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox307' style='width:160px;text-align:center' value='" + TextBox307.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox308' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox308' style='width:16px;text-align:center' value='" + TextBox308.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox309' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox309' style='width:16px;text-align:center' value='" + TextBox309.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox310' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox310' style='width:16px;text-align:center' value='" + TextBox310.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox311' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox311' style='width:16px;text-align:center' value='" + TextBox311.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox312' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox312' style='width:16px;text-align:center' value='" + TextBox312.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox313' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox313' style='width:16px;text-align:center' value='" + TextBox313.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox365' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox365' style='width:65px;text-align:center' value='" + TextBox365.Text.ToUpper().Trim() + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox366' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox366' style='width:16px;text-align:center' value='" + TextBox366.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox367' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox367' style='width:16px;text-align:center' value='" + TextBox367.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox448' type='text' id='ctl00_ContentPlaceHolder1_TextBox448' style='width:674px;' value='" + TextBox448.Text.ToUpper().Trim() + "' /> " +
"         </td> " +
"     </tr> " +
"     </table>     " + 
"     <table style='width: 975px'> " +
"     <tr> " +
"         <td style='height: 23px'><b>Justificaci&#243n Cl&#237nica</b></td> " +
"     </tr> " +
"     <tr> " +
 "        <td style='height: 23px'> " +
"             <textarea name='ctl00$ContentPlaceHolder1$TextBox449' rows='2' cols='20' id='ctl00_ContentPlaceHolder1_TextBox449' style='height:" + tam + "px;width:955px;'>" + textoTilde(TextBox449.Text.ToUpper().Trim().ToUpper()) + "</textarea> " +
"         </td> " +
"     </tr> " +
"     </table>    " +
   "     <table style='width: 975px'> " +
"     <tr> " +
"         <td style='height: 23px'><b>Impresi&#243n Diagn&#243stica</b></td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'>C&#243digo CIE10</td> " +
"         <td style='height: 23px'>Descripci&#243n</td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>Diagn&#243stico Principal</td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox387' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox387' style='width:85px;text-align:center' value='" + TextBox387.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox388' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox388' style='width:16px;text-align:center' value='" + TextBox388.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox389' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox389' style='width:16px;text-align:center' value='" + TextBox389.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox390' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox390' style='width:16px;text-align:center' value='" + TextBox390.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox399' type='text' id='ctl00_ContentPlaceHolder1_TextBox399' style='width:672px;'  value='" + TextBox399.Text + "' /> " +
 "        </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>Diagn&#243stico Relacionado 1</td> " +
 "        <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox391' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox391' style='width:85px;text-align:center' value='" + TextBox391.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox392' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox392' style='width:16px;text-align:center' value='" + TextBox392.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox393' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox393' style='width:16px;text-align:center' value='" + TextBox393.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox394' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox394' style='width:16px;text-align:center' value='" + TextBox394.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox400' type='text' id='ctl00_ContentPlaceHolder1_TextBox400' style='width:672px;'  value='" + TextBox400.Text + "' /> " +
"         </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>Diagn&#243stico Relacionado 2</td> " +
"         <td style='height: 23px'>&nbsp;</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox395' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox395' style='width:85px;text-align:center' value='" + TextBox395.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox396' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox396' style='width:16px;text-align:center' value='" + TextBox396.Text + "' /> " +
// "            <input name='ctl00$ContentPlaceHolder1$TextBox397' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox397' style='width:16px;text-align:center' value='" + TextBox397.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox398' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox398' style='width:16px;text-align:center' value='" + TextBox398.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox401' type='text' id='ctl00_ContentPlaceHolder1_TextBox401' style='width:672px;'  value='" + TextBox401.Text + "' /> " +
"         </td> " +
"     </tr> " +
"     </table> " 
;
        return html;
    }
    protected string retonrarHtml2()
    {
       string html2= "     <table style='width: 975px'> " +
"     <tr> " +
"         <td style='height: 23px' colspan='6' align='center'><B>INFORMACION DE LA PERSONA QUE SOLICITA</B></td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px' colspan='2'>Nombre de la persona que solicita</td> " +
 "        <td rowspan='3'>Tel&#233fono</td> " +
"         <td style='height: 23px' align='center'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox403' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox403' style='width:91px;text-align:center' value='" + TextBox403.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox404' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox404' style='width:16px;text-align:center' value='" + TextBox404.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox405' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox405' style='width:16px;text-align:center' value='" + TextBox405.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox406' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox406' style='width:16px;text-align:center' value='" + TextBox406.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox407' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox407' style='width:16px;text-align:center' value='" + TextBox407.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px' align='center'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox408' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox408' style='width:126px;text-align:center' value='" + TextBox408.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox409' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox409' style='width:16px;text-align:center' value='" + TextBox409.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox410' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox410' style='width:16px;text-align:center' value='" + TextBox410.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox411' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox411' style='width:16px;text-align:center' value='" + TextBox411.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox412' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox412' style='width:16px;text-align:center' value='" + TextBox412.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox413' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox413' style='width:16px;text-align:center' value='" + TextBox413.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox414' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox414' style='width:16px;text-align:center' value='" + TextBox414.Text + "' /> " +
"             </td> " +
"         <td style='height: 23px' align='center'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox415' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox415' style='width:85px;text-align:center' value='" + TextBox415.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox416' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox416' style='width:16px;text-align:center' value='" + TextBox416.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox417' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox417' style='width:16px;text-align:center' value='" + TextBox417.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox418' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox418' style='width:16px;text-align:center' value='" + TextBox418.Text + "' /> " +
"             </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px' colspan='2'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox402' type='text' id='ctl00_ContentPlaceHolder1_TextBox402' style='width:476px;'  value='" + TextBox402.Text + "' /> " +
"         </td> " +
"         <td style='height: 23px' align='center'>Indicativo</td> " +
"         <td style='height: 23px' align='center'>N&#250mero</td> " +
"         <td style='height: 23px' align='center'>Extensi&#243n</td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px'>Cargo o Actvidad:</td> " +
"         <td style='height: 23px'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox419' type='text' id='ctl00_ContentPlaceHolder1_TextBox419' style='width:359px;'  value='" + TextBox419.Text + "' /> " +
"         </td> " +
"         <td style='height: 23px' align='center'>Tel&#233fono Celular:</td> " +
"         <td style='height: 23px' colspan='2' align='center'> " +
"             <input name='ctl00$ContentPlaceHolder1$TextBox420' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox420' style='width:200px;text-align:center' value='" + TextBox420.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox421' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox421' style='width:16px;text-align:center' value='" + TextBox421.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox422' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox422' style='width:16px;text-align:center' value='" + TextBox422.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox423' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox423' style='width:16px;text-align:center' value='" + TextBox423.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox424' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox424' style='width:16px;text-align:center' value='" + TextBox424.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox425' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox425' style='width:16px;text-align:center' value='" + TextBox425.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox426' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox426' style='width:16px;text-align:center' value='" + TextBox426.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox427' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox427' style='width:16px;text-align:center' value='" + TextBox427.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox428' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox428' style='width:16px;text-align:center' value='" + TextBox428.Text + "' /> " +
//"             <input name='ctl00$ContentPlaceHolder1$TextBox429' type='text' maxlength='1' id='ctl00_ContentPlaceHolder1_TextBox429' style='width:16px;text-align:center' value='" + TextBox429.Text + "' /> " +
"            </td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px;font-size: xx-small;' colspan='6'>MPS-SAS V5.0 2008-07-11</td> " +
"     </tr> " +
"     <tr> " +
"         <td style='height: 23px' colspan='6' align='center'> " +
"         </td> " +
"     </tr> " +
" </table>";
       return html2;
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        AgregarPrintScript(ViewState["Archivo3"].ToString());
    }
    protected void inactivarControles()
    {
        Button1.Enabled = false;
        rblCobertura.Enabled = false;
        rblServicio.Enabled = false;
        rblPrioridad.Enabled = false;
        TextBox1.Enabled = false;
        TextBox165.Enabled = false;
        TextBox166.Enabled = false;
        TextBox174.Enabled = false;
        TextBox173.Enabled = false;
        TextBox177.Enabled = false;
        TextBox184.Enabled = false;
        TextBox450.Enabled = false;
        TextBox188.Enabled = false;
        TextBox314.Enabled = false;
        TextBox431.Enabled = false;
        TextBox195.Enabled = false;
        TextBox317.Enabled = false;
        TextBox432.Enabled = false;
        TextBox202.Enabled = false;
        TextBox320.Enabled = false;
        TextBox433.Enabled = false;
        TextBox209.Enabled = false;
        TextBox323.Enabled = false;
        TextBox434.Enabled = false;
        TextBox216.Enabled = false;
        TextBox326.Enabled = false;
        TextBox435.Enabled = false;
        TextBox223.Enabled = false;
        TextBox329.Enabled = false;
        TextBox436.Enabled = false;
        TextBox230.Enabled = false;
        TextBox332.Enabled = false;
        TextBox437.Enabled = false;
        TextBox237.Enabled = false;
        TextBox335.Enabled = false;
        TextBox438.Enabled = false;
        TextBox244.Enabled = false;
        TextBox338.Enabled = false;
        TextBox439.Enabled = false;
        TextBox251.Enabled = false;
        TextBox341.Enabled = false;
        TextBox440.Enabled = false;
        TextBox258.Enabled = false;
        TextBox344.Enabled = false;
        TextBox441.Enabled = false;
        TextBox265.Enabled = false;
        TextBox347.Enabled = false;
        TextBox442.Enabled = false;
        TextBox272.Enabled = false;
        TextBox350.Enabled = false;
        TextBox443.Enabled = false;
        TextBox279.Enabled = false;
        TextBox353.Enabled = false;
        TextBox444.Enabled = false;
        TextBox286.Enabled = false;
        TextBox356.Enabled = false;
        TextBox445.Enabled = false;
        TextBox293.Enabled = false;
        TextBox359.Enabled = false;
        TextBox446.Enabled = false;
        TextBox300.Enabled = false;
        TextBox362.Enabled = false;
        TextBox447.Enabled = false;
        TextBox307.Enabled = false;
        TextBox365.Enabled = false;
        TextBox448.Enabled = false;
        TextBox449.Enabled = false;
        TextBox451.Enabled = false;
        TextBox449.Enabled = false;
        rblAtencion.Enabled = false;
    }
}

//protected void TraerUbicacion(string ValUbicacion)
//{
//    if (ValUbicacion == "1") { TextBox163.Text = "X"; } else { if (ValUbicacion == "2") { TextBox156.Text = "X"; } }
//}
//protected void TraerIndentificacion(string ValTipoId)
//{
//    if (ValTipoId == "2") { TextBox78.Text = "X"; }
//    if (ValTipoId == "3") { TextBox97.Text = "X"; }
//    if (ValTipoId == "4") { TextBox99.Text = "X"; }
//    if (ValTipoId == "5") { TextBox101.Text = "X"; }
//    if (ValTipoId == "P") { TextBox80.Text = "X"; }
//    if (ValTipoId == "A") { TextBox98.Text = "X"; }
//    if (ValTipoId == "M") { TextBox100.Text = "X"; }
//}
//protected void TraerContingencia(string ValContingencia)
//{
//    if (ValContingencia == "13") { TextBox145.Text = "X"; }
//    if (ValContingencia == "14") { TextBox150.Text = "X"; }
//    if (ValContingencia == "01") { TextBox146.Text = "X"; }
//    if (ValContingencia == "02") { TextBox151.Text = "X"; }
//    if (ValContingencia == "06") { TextBox147.Text = "X"; }
//}