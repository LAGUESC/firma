﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/MasterNew.master" AutoEventWireup="true" CodeFile="ConsentimientosStandar.aspx.cs" Inherits="Anexos_BusquedadAnexoNew1" MaintainScrollPositionOnPostback="true" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
        <script src="<%= Page.ResolveUrl("~/js/signatureStandar.js") %>"></script>
        <script type="text/javascript">
            function FirmaTablet() {
                //guardo la referencia de la ventana para poder utilizarla luego
                ventana_secundaria = window.open("FirmaTablet.html", "miventana", "width=419px,height=300px,menubar=no,left=750,top=400")
            }
    </script>
        <asp:Label ID="lblIdentifier" runat="server" Visible="false"></asp:Label>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box box-primary">
                        <!--div Título-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-4">
                                <h3 align="center" runat="server" class="box-title">Consentimientos Informados Enfermería Urgencias</h3> </div>
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <asp:LinkButton ID="LinkButton1" runat="server" style ="float:right;font-size:large;" OnClientClick="REPRODUCTOR();" ><h3 class="fa fa-info-circle"></h3>&nbsp;Ayuda&nbsp;&nbsp;&nbsp;&nbsp;</asp:LinkButton>
                                </div>
                            </div>
                        </div>
                    </div>
                    
        <!-- Small boxes (Stat box) -->
        <div class="row">
            <div class="col-md-12">
            <div class="col-lg-3 col-xs-6">
                <div class="form-group">
                <!-- small box -->
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3>
                            <asp:Label ID="lblF_HC_13" runat="server" Text="0"></asp:Label></h3>

                        <p>F-HC-13 Consulta historia clínica</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-pencil-square-o"></i>
                    </div>
                    <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                    </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="form-group">
                <div class="small-box bg-aqua">
                    <div class="inner">
                        <h3>
                            <asp:Label ID="lblF_HC_7" runat="server" Text="0"></asp:Label></h3>

                        <p>F-HC-7 Procedimientos de enfermería</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-pencil-square-o"></i>
                    </div>
                    <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                    </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <!-- small box -->
                <div class="form-group">
                <div class="small-box bg-yellow">
                    <div class="inner">
                        <h3>
                            <asp:Label ID="lblF_HC_16" runat="server" Text="0"></asp:Label></h3>
                        <p>F-HC-16 Autorización de docencia</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-pencil-square-o"></i>
                    </div>
                    <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                    </div>
            </div>
            <!-- ./col -->
            <div class="col-lg-3 col-xs-6">
                <div class="form-group">
                <!-- small box -->
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3>
                            <asp:Label ID="lblF_SH_4" runat="server" Text="0"></asp:Label></h3>

                        <p>F-SH-4 Notificación Acompañante Permanente</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-pencil-square-o"></i>
                    </div>
                    <a href="#" class="small-box-footer">Mas info <i class="fa fa-arrow-circle-right"></i></a>
                </div>
                    </div>
            </div>
            <!-- ./col -->
        </div>
            </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!--div campo Tipo identificación-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <%--<h5>Tipo de identificación</h5>
                                    <asp:DropDownList ID="ddlTipoId" runat="server" AutoPostBack="True" class="form-control"> </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                        </div>
                        <!--div campo Identificación-->
                        <div class="row">
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                     <div class="col-md-5">
                        <div class="form-group">
                           <h5>Identificación del paciente</h5>
                           <asp:TextBox ID="txtId" runat="server" class="form-control" placeholder="Identificación del paciente" MaxLength="13"></asp:TextBox>
                           <ajaxToolkit:FilteredTextBoxExtender ID="txtId_FilteredTextBoxExtender" runat="server" FilterInterval="17" TargetControlID="txtId" ValidChars="1234567890"> </ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group"> </div>
                     </div>
                  </div>
                        <!--div botón Consultar-->
                        <div class="row">
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label>
                              <asp:CheckBox ID="CheckPaciente" runat="server" Text="Firma cómo paciente" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckPaciente_CheckedChanged" /> </label>
                           <br />
                           <label>
                              <asp:CheckBox ID="CheckRepresentante" runat="server" Text="Firma cómo representante" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckRepresentante_CheckedChanged" /> </label>
                           <br />
                           <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="btn btn-primary" OnClick="btnConsultar_Click" /> </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                  </div>
                    </div>
                </div>
            </div>
        </div>
        <!--div tabla-->
        <div class="box box-primary" id="divtable" runat="server" visible="false">
            <div class="row">
            <div class="box-header with-border">
               <div class="col-md-1">
                  <div class="form-group"> </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <h5>Nombre Paciente :</h5> </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">
                     <div>
                        <asp:TextBox ID="LblNombre" runat="server" READONLY="true" CausesValidation="True" class="form-control"></asp:TextBox>
                     </div>
                  </div>
               </div>
               <div class="col-md-1">
                  <div class="form-group"> </div>
               </div>
            </div>
         </div>
            <!--*****************************************************************-->
            <div class="row">
                <div class="box-header with-border">
                <div class="col-md-1">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <%--<h5>Consentiento :</h5>--%> </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div>
                            <div class="icheck-material-blue">
                                <label>
                                    <asp:CheckBox ID="CheckHClinica" runat="server" Text="F-HC-13 Consulta historia clínica" CssClass="icheck-material-blue" AutoPostBack="True"  Visible="true" OnCheckedChanged="CheckHClinica_CheckedChanged"/> </label>
                                <br />
                                <br />
                                <label>
                                <asp:CheckBox ID="CheckPEnfermeria" runat="server" Text="F-HC-7 Procedimientos de enfermería" CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" OnCheckedChanged="CheckPEnfermeria_CheckedChanged"/> </label>
                                <br />
                                <br />
                                <label>
                                <asp:CheckBox ID="CheckAutorizacionDocencia" runat="server" Text="F-HC-16 Autorización de docencia" CssClass="icheck-material-blue" AutoPostBack="True"  Visible="true" OnCheckedChanged="CheckAutorizacionDocencia_CheckedChanged" /> </label>
                                <br />
                                <br />
                                <label>
                                <asp:CheckBox ID="CheckAcompañante" runat="server" Text="F-SH-4 Notificación Acompañante Permanente" CssClass="icheck-material-blue" AutoPostBack="True"  Visible="true" OnCheckedChanged="CheckAcompañante_CheckedChanged"/> </label>
                                <br />
                                <br />
                                <label>
                                <asp:CheckBox ID="CheckDatosPersonales" runat="server" Text="F-SI-5 Autorización para tratamientos de datos personales" CssClass="icheck-material-blue" AutoPostBack="True"  Visible="true" OnCheckedChanged="CheckDatosPersonales_CheckedChanged"/> </label>
                                <br />
                                <br />
                                <label>
                                <asp:CheckBox ID="CheckAutorizado" runat="server" Text="F-HC-2 Autorización de representante" CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" Enabled="false" class="form-control" /> </label>
                                </div>
                            </div>
                        </div>
                    </div>                
                <div class="col-md-4">
                    <div class="form-group"> </div>
                </div>
            </div>
            </div>
            <!--*****************************************************************-->
            <div id="divAutorizado" runat="server" visible="false">
            <div class="box-header with-border">
               <div class="row">
                  <div class="col-md-1">
                     <div class="form-group"> </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <h5>Cédula Autorizado :</h5> </div>
                  </div>
                  <div class="col-md-7">
                     <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="Campos para autorizado" ForeColor="#0066FF"></asp:Label>
                        <asp:TextBox ID="txtIdAut" runat="server" class="form-control" placeholder="Cédula Autorizado" MaxLength="13"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="txtIdAut_FilteredTextBoxExtender" runat="server" TargetControlID="txtIdAut" ValidChars="1234567890"> </ajaxToolkit:FilteredTextBoxExtender>
                     </div>
                  </div>
                  <div class="col-md-1">
                     <div class="form-group"> </div>
                  </div>
               </div>
            <!--*****************************************************************-->
            <div class="row">
                  <div class="col-md-1">
                     <div class="form-group"> </div>
                  </div>
                  <div class="col-md-3">
                     <div class="form-group">
                        <h5>Nombre Autorizado :</h5> </div>
                  </div>
                  <div class="col-md-7">
                     <div class="form-group">
                           <asp:TextBox ID="txtNomAut" runat="server" class="form-control" placeholder="Nombre Autorizado" MaxLength="100"></asp:TextBox>
                           <ajaxToolkit:FilteredTextBoxExtender ID="txtNomAut_FilteredTextBoxExtender" runat="server" TargetControlID="txtNomAut" ValidChars="abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ´ "> </ajaxToolkit:FilteredTextBoxExtender>
                     </div>
                  </div>
                  <div class="col-md-1">
                     <div class="form-group"> </div>
                  </div>
               </div>
            </div>
         </div>
            <!--*****************************************************************-->
            <div class="row" id="divConsideraciones" runat="server" visible="false">
            <div class="box-header with-border">
               <div class="col-md-1">
                  <div class="form-group"> 
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <h5>Consideraciones :</h5> </div>
               </div>
               <div class="col-md-7">
                  <div class="form-group">
                     <div>
                        <asp:Label ID="lblConsideraciones" runat="server" ForeColor="#0066FF"></asp:Label>
                        <br />
                        <asp:TextBox ID="txtConsideraciones" runat="server" CausesValidation="True" Height="83px" TextMode="MultiLine" class="form-control required"></asp:TextBox>
                     </div>
                  </div>
               </div>
               <div class="col-md-1">
                  <div class="form-group"> </div>
               </div>
            </div>
         </div>
            <!--*****************************************************************-->
            <div class="row" id="divAnestesia" runat="server" visible="false">
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <h5>Anestesias :</h5> </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="icheck-material-blue">
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaGeneral" runat="server" Text="ANESTESIA GENERAL" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaRegional" runat="server" Text="ANESTESIA REGIONAL O BLOQUEO" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaConductiva" runat="server" Text="ANESTESIA CONDUCTIVA" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaLocal" runat="server" Text="ANESTESIA LOCAL ASISTIDA" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br /> </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <br />
            <div class="box-header with-border">
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group"></div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                            <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-primary" Text="Aceptar" OnClick="btnGuardar_Click" visible="false"/>
                            <asp:Button ID="btnReview" runat="server" CssClass="btn btn-primary" Text="Previsualizar" OnClick="btnReview_Click" Visible="true" />
                            <asp:Button ID="btnCaptureFirm" type="submit" class="btn btn-danger" runat="server" OnClick="Capture_Firma_Click" Text="Capturar firma" Visible="false" /> 
                            <asp:Button ID="btnCaptureFirmTablet" class="btn btn-danger" runat="server" OnclientClick ="FirmaTablet()" Text="Capturar firma" Visible="false" /> &nbsp;&nbsp;
                            <label>
                                <asp:CheckBox ID="CheckAprobacionMedico" runat="server" Text="Aprobación médico" CssClass="icheck-material-blue" visible="false" /> </label>
                            <br />
                            <br /> 
                            <label>
                                <asp:CheckBox ID="CheckUtilizarFirmaBD" runat="server" Text="Capturar nueva firma" CssClass="icheck-material-blue" AutoPostBack="True" Visible="false" class="form-control" OnCheckedChanged="CheckUtilizarFirmaBD_CheckedChanged" /> </label>
                            <br />
                            <br />
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group"> 
                        <div id="imageBox" class="boxed" style="height:37mm;width:72mm" ;></div>
                    </div>
                </div>
                </div>
            </div>
        </div>       
    </asp:Content>