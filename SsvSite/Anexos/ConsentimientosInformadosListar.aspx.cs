﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Anexos_ConsentimientosInformadosListar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
 

    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarConsentimientos();
        divtable.Visible = true;
    }
    public void CargarConsentimientos()
    {
        
        if (CheckIdentificacion.Checked)
        {
            DataTable dt = null;
            string Cedula = txtId.Text;
            dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_LISTAR_X_CEDULAPA(Cedula);
            if (dt.Rows.Count>0)
            {
                Session["dtTodosRegistros"] = dt;
                ViewState["dtPaginas"] = dt;
                Procedimientos.LlenarGrid(dt, gvConsentimientos);
                foreach (GridViewRow row in gvConsentimientos.Rows)
                {
                    int ID_CONSENTIMIENTO_PDF = Convert.ToInt32(gvConsentimientos.DataKeys[row.RowIndex].Values[0]);
                    bool APROBACION_MEDICO = (row.FindControl("CheckAprobacionMedico") as CheckBox).Checked;

                }
            }
            else
            {
                Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
                divtable.Visible = false;
                gvConsentimientos.DataBind();
            }

        }
        else if (CheckRango.Checked)
        {

            DateTime FechaInicio = Convert.ToDateTime(txtInicio.Text);
            DateTime FechaFin = Convert.ToDateTime(txtFin.Text);

            string FechaActual = DateTime.Now.ToString();
            if (FechaInicio <= DateTime.Parse(FechaActual))
            {
                if (FechaFin <= DateTime.Parse(FechaActual))
                {
                    if (FechaInicio <= FechaFin)
                    {
                        DataTable dt = null;
                        dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_LISTAR_X_RANGOS_FECHA(FechaInicio.ToString(),FechaFin.ToString());
                        Session["dtTodosRegistros"] = dt;
                        ViewState["dtPaginas"] = dt;
                        Procedimientos.LlenarGrid(dt, gvConsentimientos);
                        foreach (GridViewRow row in gvConsentimientos.Rows)
                        {
                            int ID_CONSENTIMIENTO_PDF = Convert.ToInt32(gvConsentimientos.DataKeys[row.RowIndex].Values[0]);
                            bool APROBACION_MEDICO = (row.FindControl("CheckAprobacionMedico") as CheckBox).Checked;

                        }

                    }
                    else
                    {
                        Procedimientos.Script("mensajini", "Mensaje(85)", this.Page);
                    }
                }
                else
                {
                    Procedimientos.Script("mensajini", "Mensaje(84)", this.Page);
                }

            }
            else
            {
                Procedimientos.Script("mensajini", "Mensaje(86)", this.Page);
            }
        }
        else
        {
            string Cedula = txtUsuario.Text;
            DataTable dt = null;
            dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_LISTAR_X_CEDULA(Cedula);
            if (dt.Rows.Count > 0)
            {
                Session["dtTodosRegistros"] = dt;
                ViewState["dtPaginas"] = dt;
                Procedimientos.LlenarGrid(dt, gvConsentimientos);
                foreach (GridViewRow row in gvConsentimientos.Rows)
                {
                    int ID_CONSENTIMIENTO_PDF = Convert.ToInt32(gvConsentimientos.DataKeys[row.RowIndex].Values[0]);
                    bool APROBACION_MEDICO = (row.FindControl("CheckAprobacionMedico") as CheckBox).Checked;

                }
            }
            else
            {
                Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
                divtable.Visible = false;
                gvConsentimientos.DataBind();
            }

        }
    }

    protected void ImgPDF_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        GridViewRow row = (GridViewRow)btn.NamingContainer;
        int i = Convert.ToInt32(row.RowIndex);

        DataTable Docs = (DataTable)Session["dtTodosRegistros"];
        //DataTable Resultado = Docs.Select("Archivo = '" + gvDocumentos.Rows[i].Cells[1].Text + "'").CopyToDataTable();
        DataTable Resultado = Docs.Select("CONSENTIMIENTO_HASH = '" + Docs.Rows[i][6].ToString() + "'").CopyToDataTable();

        Session["Datos"] = Resultado;

        string url = "VerPDF.aspx";
        Response.Write("<script type='text/javascript'>window.open('" + url + "', 'window','resizable=no,location=1,status=1,scrollbars=1,width=1200,height=900,left=400,top=50');</script>");
    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in gvConsentimientos.Rows)
        {
            int ID_CONSENTIMIENTO_PDF = Convert.ToInt32(gvConsentimientos.DataKeys[row.RowIndex].Values[0]);
            bool APROBACION_MEDICO = (row.FindControl("CheckAprobacionMedico") as CheckBox).Checked;

            DataTable dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_ACTUALIZAR_APROBACION_MEDICO(ID_CONSENTIMIENTO_PDF, APROBACION_MEDICO);
        }
        Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
    }
    protected void CheckIdentificacion_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckIdentificacion.Checked)
        {
            divtable.Visible = false;
            gvConsentimientos.DataBind();
            txtInicio.Text = "";
            txtFin.Text = "";
            txtUsuario.Text = "";
            //ddlMes.Enabled = false;
            //ddlAnio.Enabled = false;
            //ddlMes.SelectedValue = "0";
            //ddlAnio.SelectedValue = "0";
            txtId.ReadOnly = false;
            txtInicio.ReadOnly = true;
            txtFin.ReadOnly = true;
            txtUsuario.ReadOnly = true;
            //CheckMesAnio.Checked = false;
            CheckRango.Checked = false;
            CheckUsuario.Checked = false;
        }
        else
        {
            gvConsentimientos.DataBind();
            txtId.ReadOnly = true;
            txtId.Text = "";
        }

    }

    protected void CheckRango_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckRango.Checked)
        {
            divtable.Visible = false;
            gvConsentimientos.DataBind();
            txtId.Text = "";
            txtUsuario.Text = "";
            //ddlMes.SelectedValue = "0";
            //ddlAnio.SelectedValue = "0";
            //ddlMes.Enabled = false;
            //ddlAnio.Enabled = false;
            txtInicio.ReadOnly = false;
            txtFin.ReadOnly = false;
            txtId.ReadOnly = true;
            CheckIdentificacion.Checked = false;
            CheckUsuario.Checked = false;
            txtUsuario.ReadOnly = true;
            //CheckMesAnio.Checked = false;
        }
        else
        {
            gvConsentimientos.DataBind();
            txtInicio.ReadOnly = true;
            txtFin.ReadOnly = true;
            txtInicio.Text = "";
            txtFin.Text = "";
        }
    }

    //protected void CheckMesAnio_CheckedChanged(object sender, EventArgs e)
    //{
    //    if (CheckMesAnio.Checked)
    //    {
    //        txtInicio.Text = "";
    //        txtFin.Text = "";
    //        txtId.Text = "";
    //        ddlMes.Enabled = true;
    //        ddlAnio.Enabled = true;
    //        txtInicio.Enabled = false;
    //        txtFin.Enabled = false;
    //        txtId.ReadOnly = true;
    //        CheckIdentificacion.Checked = false;
    //        CheckRango.Checked = false;
    //    }
    //    else
    //    {
    //        ddlMes.Enabled = false;
    //        ddlAnio.Enabled = false;
    //        ddlMes.SelectedValue = "0";
    //        ddlAnio.SelectedValue = "0";
    //    }
    //}
    protected void CheckUsuario_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckUsuario.Checked)
        {
            divtable.Visible = false;
            gvConsentimientos.DataBind();
            txtInicio.Text = "";
            txtFin.Text = "";
            txtId.Text = "";
            //ddlMes.Enabled = false;
            //ddlAnio.Enabled = false;
            //ddlMes.SelectedValue = "0";
            //ddlAnio.SelectedValue = "0";
            txtId.ReadOnly = true;
            txtInicio.ReadOnly = true;
            txtFin.ReadOnly = true;
            //CheckMesAnio.Checked = false;
            CheckRango.Checked = false;
            CheckIdentificacion.Checked = false;
            txtUsuario.ReadOnly = false;
        }
        else
        {
            gvConsentimientos.DataBind();
            txtUsuario.ReadOnly = true;
            txtId.Text = "";
        }

    }
}