﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="Anexo1.aspx.cs" Inherits="Anexos_Anexo1" Title="Anexo 1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
   
        <table style="width: 1003px; height: 96px;">
    <tr>
        <td rowspan="5" style="width: 63px">
            <asp:Image ID="Image1" runat="server" Height="77px" ImageUrl="~/img/escudocol.png" Width="102px" />
        </td>
    </tr>
    <tr>
        <td align="center" colspan="6"><B>MINISTERIO DE LA PROTECCION SOCIAL</B></td>
    </tr>
    <tr>
        <td colspan="6">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" colspan="6"><B><font SIZE=4>
            <asp:Label ID="Label1" runat="server" Text="INFORME DE POSIBLES INCONSISTENCIAS EN LA BASE DE DATOS DE LA ENTIDAD RESPONSABLE DE PAGO" Font-Size="Medium"></asp:Label></font></B></td>
    </tr>
    <tr>
        <td align="center" style="width: auto">
            <asp:Label ID="Label2" runat="server" Text="NUMERO INFORME" Font-Bold="True"></asp:Label></td>
        <td align="center" style="width: auto; margin-left: 40px;">
            <asp:TextBox ID="txtNumInforme" runat="server" Width="104px" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="txtNumInforme_FilteredTextBoxExtender" runat="server" TargetControlID="txtNumInforme" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
                           
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
        </td>
        <td align="center" style="width: auto"><b>Fecha:</b></td>
        <td align="center" style="width: auto">
            <asp:TextBox ID="txtFecha" runat="server" Width="125px" style="text-align:center" Font-Size="Small" MaxLength="1" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
            
           
        </td>
        <td align="center" style="width: auto"><b>Hora:</b></td>
        <td align="center" style="width: auto">
            <asp:TextBox ID="txtHora" runat="server" Width="67px" style="text-align:center" Font-Size="Small" MaxLength="1" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
        </td>
    </tr>
</table>
   
    <table style="width: 975px">
    <tr>
        <td colspan="6" style="height: 23px"><b>INFORMACION DEL PRESTADOR (Solicitante)</b></td>
    </tr>
    <tr>
        <td style="width: auto"><b>Nombre</b></td>
        <td><b>NIT</b></td>
        <td align="center">
            <asp:TextBox ID="TextBox25" runat="server" Width="16px" ReadOnly="True" Font-Underline="False" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">X</asp:TextBox>
        </td>
        <td>&nbsp;&nbsp;&nbsp;</td>
        <td colspan="2" align="center">
            <asp:TextBox ID="TextBox26" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox27" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">9</asp:TextBox>
            <asp:TextBox ID="TextBox28" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox29" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">9</asp:TextBox>
            <asp:TextBox ID="TextBox30" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox31" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox32" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">6</asp:TextBox>
            <asp:TextBox ID="TextBox33" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox34" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox35" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox37" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBox38" runat="server" Enabled="False" ReadOnly="True" Width="566px" CssClass="form_input">CORPORACION PARA ESTUDIOS EN SALUD CLINICA CES</asp:TextBox>
        </td>
        <td><b>CC</b></td>
        <td>
            <asp:TextBox ID="TextBox39" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" Enabled="False" CssClass="form_input"></asp:TextBox>
            </td>
        <td>&nbsp;</td>
        <td>Numero:</td>
        <td>
            <asp:TextBox ID="TextBox41" runat="server" Width="226px" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<asp:Label ID="Label7" runat="server" Text="DV" Font-Bold="False" Font-Size="Medium"></asp:Label>--%>
            
        </td>
    </tr>
</table>
  
    <table style="width: 975px">
    <tr>
        <td style="width: 64px"><b>Codigo:</b></td>
        <td style="width: 349px">
            <asp:TextBox ID="TextBox42" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox43" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">5</asp:TextBox>
            <asp:TextBox ID="TextBox44" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox45" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox46" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
            <asp:TextBox ID="TextBox47" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox48" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox49" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
            <asp:TextBox ID="TextBox50" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox51" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">4</asp:TextBox>
            <asp:TextBox ID="TextBox52" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox53" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            </td>
        <td><b>Direcciòn Prestador:</b></td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td rowspan="2" style="width: 64px"><b>Teléfono:</b></td>
        <td style="width: 191px">
            <asp:TextBox ID="TextBox54" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">4</asp:TextBox>
            <asp:TextBox ID="TextBox55" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox56" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox57" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox58" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            </td>
        <td style="width: 277px">
            <asp:TextBox ID="TextBox59" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">5</asp:TextBox>
            <asp:TextBox ID="TextBox60" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox61" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">6</asp:TextBox>
            <asp:TextBox ID="TextBox62" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox63" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox64" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox65" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            </td>
        <td colspan="9">
            <asp:TextBox ID="TextBox66" runat="server" Width="543px" Enabled="False" CssClass="form_input">CALLE 58 # 50C-2 PRADO CENTRO</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="width: 191px" align="center">Indicativo</td>
        <td style="width: 277px" align="center">Número</td>
        <td style="width: auto"><b>Departamento:</b></td>
        <td>
            <asp:TextBox ID="TextBox67" runat="server" Enabled="False" ReadOnly="True" Width="130px" CssClass="form_input">ANTIOQUIA</asp:TextBox>
        </td>
        <td bgcolor="Gray">
            &nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td><b>Municipio:</b></td>
        <td>
            <asp:TextBox ID="TextBox68" runat="server" Enabled="False" ReadOnly="True" Width="130px" CssClass="form_input">MEDELLIN</asp:TextBox>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
    </tr>
    </table>

    <table style="width: 975px">
    <tr>
        <td style="height: 26px;"><b>ENTIDAD A LA QUE SE LE INFORMA (Pagador)</b>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
        <td style="height: 26px">&nbsp;<b>CODIGO:</b></td>
        <td style="height: 26px">
            <asp:TextBox ID="TextBox69" runat="server" Width="99px" style="text-align:center" Font-Size="Small" MaxLength="1" Enabled="False" CssClass="form_input">05001021240</asp:TextBox>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
            </td>
    </tr>
    </table>
   
    <table style="width: 975px">
    <tr>
        <td align="center"><asp:Label ID="Label8" runat="server" Text="Tipo de inconsistencia" Font-Size="Medium" Font-Bold="True"></asp:Label></td>
        <td align="left">
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%><%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
            <asp:RadioButtonList ID="rblInconsistencia" runat="server" Width="812px" CssClass="form_input">
                <asp:ListItem Value="1">El paciente no existe en la base de datos</asp:ListItem>
                <asp:ListItem Value="2">Los datos del paciente no corresponde con los del documento de identificacion presentado</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    </table>
       
        <table style="width: 975px">
    <tr>
        <td style="height: 23px" colspan="8" align="center"><asp:Label ID="Label11" runat="server" Text="DATOS DEL USUARIO (Como aparece en la base de datos)" Font-Bold="True"></asp:Label></td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBox75" runat="server" Width="233px" CssClass="form_input" Enabled="False" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="TextBox75" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td>
            <asp:TextBox ID="TextBox76" runat="server" Width="233px" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="TextBox76" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td colspan="3" style="width: 195px">
            <asp:TextBox ID="TextBox77" runat="server" Width="233px" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" runat="server" TargetControlID="TextBox77" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td colspan="3">
            <asp:TextBox ID="TextBox78" runat="server" Width="233px" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" runat="server" TargetControlID="TextBox78" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
    </tr>
    <tr>
        <td style="height: 23px" align="center"><b>Primer Apellido</b></td>
        <td style="height: 23px" align="center"><b>Segundo Apellido</b></td>
        <td style="height: 23px; width: 195px;" colspan="3" align="center"><b>Primer Nombre</b></td>
        <td style="height: 23px" colspan="3" align="center"><b>Segundo Nombre</b></td>
    </tr>




    <tr>
        <td colspan="8"><b>Tipo Documento de Identificación</b></td>
    </tr>
    <tr>
        <td colspan="2" rowspan="4">
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%><%--<asp:TextBox ID="TextBox1" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%><%--<asp:Label ID="Label9" runat="server" Text="El paciente no existe en la base de datos"></asp:Label>&nbsp;--%><%--<asp:TextBox ID="TextBox79" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox79" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<asp:TextBox ID="TextBox80" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%><%--<asp:TextBox ID="TextBox97" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%><%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="TextBox97" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>--%><%--<asp:TextBox ID="TextBox98" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            <asp:RadioButtonList ID="rblTipoId" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Width="473px" CssClass="form_input" Enabled="False">
                <asp:ListItem Value="2">Registro Civil</asp:ListItem>
                <asp:ListItem Value="P">Pasaporte</asp:ListItem>
                <asp:ListItem Value="3">Tarjeta de Identidad</asp:ListItem>
                <asp:ListItem Value="A">Adulto sin Identificación</asp:ListItem>
                <asp:ListItem Value="4">Cédula de Ciudadania</asp:ListItem>
                <asp:ListItem Value="M">Menor sin Idenficaci&#243;n</asp:ListItem>
                <asp:ListItem Value="5">Cédula de Extranjería</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        <td>&nbsp;</td>
        <td colspan="5">
            <asp:TextBox ID="txtDocOrigen" runat="server" Width="286px" Font-Size="Small" CssClass="form_input" Enabled="False" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="txtDocOrigen_FilteredTextBoxExtender" runat="server" TargetControlID="txtDocOrigen" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            
            <%--<asp:TextBox ID="TextBox99" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="TextBox99" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
       
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center" colspan="3">Número Documento de Identificación</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td style="width: 341px">&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td style="width: 341px"><b>Fecha de Nacimiento:(yyyy/mm/dd)</b></td>
        <td colspan="4">
            <asp:TextBox ID="txtFechaOrigen" runat="server" Width="141px" style="text-align:center" Font-Size="Small" MaxLength ="10" CssClass="form_input" Enabled="False"  ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="txtFechaOrigen_FilteredTextBoxExtender1" runat="server" TargetControlID="txtFechaOrigen"  FilterType="Numbers, Custom"  ValidChars="/"  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--<asp:TextBox ID="TextBox100" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
       
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td><b>Dirección de Residencia Habitual:</b></td>
        <td>
            <asp:TextBox ID="TextBox112" runat="server" Width="419px" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" runat="server" TargetControlID="TextBox112" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ #-0123456789"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td><b>Teléfono:</b></td>
        <td>
            <asp:TextBox ID="txtTelefonoOrigen" runat="server" Width="152px" style="text-align:center" Font-Size="Small" MaxLength="10" CssClass="form_input" Enabled="False"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="txtTelefonoOrigen_FilteredTextBoxExtender1" runat="server" TargetControlID="txtTelefonoOrigen"  FilterType="Numbers"  ValidChars=""  ></ajaxToolkit:FilteredTextBoxExtender>

            
            <%--<asp:TextBox ID="TextBox101" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td><b>Departamento:</b></td>
        <td>
        &nbsp;
            <asp:DropDownList ID="dllDepartamento" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dllDepartamento_SelectedIndexChanged" Width="236px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td><b>Municipio:</b></td>
        <td>
        &nbsp;
            <asp:DropDownList ID="ddlMunicipio" runat="server" Width="236px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
    </tr>
    </table>
   
    <table style="width: 975px">
    </table>
   
    <table style="width: 975px">
    <tr>
        <td style="height: 23px"><b>Cobertura en Salud</b></td>
    </tr>
    <tr>
        <td align="left">
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%><%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%><%--<asp:TextBox ID="TextBox137" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%><%--<asp:TextBox ID="TextBox138" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%><%--<asp:TextBox ID="TextBox139" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%><%--<asp:TextBox ID="TextBox140" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%><%--<asp:TextBox ID="TextBox141" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%><%--<asp:TextBox ID="TextBox142" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            <asp:RadioButtonList ID="rblCobertura" runat="server" RepeatColumns="4" RepeatDirection="Horizontal" CssClass="form_input" Width="975px">
                <asp:ListItem Value="1">Regimen Contributivo</asp:ListItem>
                <asp:ListItem Value="2">Regimen Subsidiado - Parcial</asp:ListItem>
                <asp:ListItem Value="3">Población Pobre No Asegurada Sin SISBEN</asp:ListItem>
                <asp:ListItem Value="4">Plan Adicional de Salud</asp:ListItem>
                <asp:ListItem Value="5">Regimen Subsidiado - Total</asp:ListItem>
                <asp:ListItem Value="6">Población Pobre No Asegurada Con SISBEN</asp:ListItem>
                <asp:ListItem Value="7">Desplazado</asp:ListItem>
                <asp:ListItem Value="8">Otro</asp:ListItem>
            </asp:RadioButtonList>
            </td>
    </tr>
    </table>
    
    <table style="width: 975px">
    <tr>
        <td style="height: 23px" align="center" colspan="12">
            <asp:Label ID="Label12" runat="server" Text="INFORMACION DE LA POSIBLE INCONSISTENCIA" Font-Bold="True"></asp:Label></td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="4"><asp:Label ID="Label13" runat="server" Text="VARIABLE PRESUNTAMENTE INCORRECTA" Font-Bold="True" Font-Size="Small"></asp:Label></td>
        <td style="height: 23px" colspan="8"><asp:Label ID="Label14" runat="server" Text="DATOS SEGUN DOCUMENTODE IDENTIFICACION (fisico)" Font-Bold="True" Font-Size="Small"></asp:Label></td>
       
      
    </tr>
    <tr>
        <td style="height: 10px" width="2"><asp:TextBox ID="TextBox145" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="TextBox145" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td style="height: AUTO"  colspan="1" align="left">Primer Apellido</td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 10px" colspan="2">Primer Apellido</td>
        <td style="height: 10px" colspan="6">
            <asp:TextBox ID="txtPrimerApellidoDest" runat="server" Width="427px"  Font-Size="Small" CssClass="form_input" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" runat="server" TargetControlID="txtPrimerApellidoDest" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox143" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
       
    </tr>
    <tr>
        <td style="height: 10px"  width="2" colspan="1"><asp:TextBox ID="TextBox3" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox3" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td style="height: 23px" colspan="1" align="left">Segundo Apellido</td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 10px" colspan="1"></td>

        <td style="height: 23px" colspan="2"><asp:Label ID="Label16" runat="server" Text="Segundo Apellido"></asp:Label></td>
        <td style="height: 10px" colspan="6">
            <asp:TextBox ID="txtSegundoApellidoDest" runat="server" Width="427px"  Font-Size="Small" CssClass="form_input" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" runat="server" TargetControlID="txtSegundoApellidoDest" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox144" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
       
    </tr>
    <tr>
        <td style="height: 10px" width="2" colspan="1"><asp:TextBox ID="TextBox148" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="TextBox148" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td style="height: 23px" colspan="1" align="left">Primer Nombre</td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 23px" colspan="2"><asp:Label ID="Label17" runat="server" Text="Primer Nombre"></asp:Label></td>
        <td style="height: 10px" colspan="6">
            <asp:TextBox ID="txtPrimerNombreDest" runat="server" Width="427px"  Font-Size="Small" CssClass="form_input" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" runat="server" TargetControlID="txtPrimerNombreDest" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
            </td>
       
    </tr>
    <tr>
        <td style="height: 10px"   width="2" colspan="1"><asp:TextBox ID="TextBox472" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="TextBox472" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td style="height: 23px" colspan="1" align="left">Segundo Nombre</td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 23px" colspan="2"><asp:Label ID="Label18" runat="server" Text="Segundo nombre"></asp:Label></td>
        <td style="height: 10px" colspan="6">
            <asp:TextBox ID="txtSegundoNombreDest" runat="server" Width="427px"  Font-Size="Small" CssClass="form_input" Height="23px" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" runat="server" TargetControlID="txtSegundoNombreDest" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
            </td>
       
    </tr>
    <tr>
        <td style="height: 10px"  width="2" colspan="1"><asp:TextBox ID="TextBox495" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="TextBox495" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td style="height: 23px" colspan="1" align="left">Tipo Documento de Identificación</td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 23px" colspan="2"><asp:Label ID="Label19" runat="server" Text="Tipo documento"></asp:Label>&nbsp;de Identificación</td>
        <td style="height: 10px" colspan="6">
                <asp:DropDownList ID="ddlTipoId" runat="server" Width="268px" CssClass="form_input">
                </asp:DropDownList>
            <%--            <asp:TextBox ID="TextBox7" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox8" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox9" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
       
    </tr>
    <tr>
        <td style="height: 10px"  width="2" colspan="1"><asp:TextBox ID="TextBox518" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="TextBox518" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td style="height: 23px" colspan="1" align="left">Numero Documento de Identificación</td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 23px" colspan="2"><asp:Label ID="Label20" runat="server" Text="Numero de documento"></asp:Label>&nbsp;de Identificación</td>
        <td style="height: 10px" colspan="6">
            <asp:TextBox ID="txtNumDocDest" runat="server" Width="257px"  Font-Size="Small" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender80" runat="server" TargetControlID="txtNumDocDest" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>


            <%--            <asp:TextBox ID="TextBox11" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox12" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox13" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label3" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox14" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox15" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label4" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox16" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox17" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
       
    </tr>

    <tr>
        <td style="height: 10px" width="2" colspan="1"><asp:TextBox ID="TextBox541" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="TextBox541" ValidChars="Xx"  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
        <td style="height: 23px" colspan="1" align="left">Fecha de nacimiento</td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 10px" colspan="1"></td>
        <td style="height: 23px" colspan="2"><asp:Label ID="Label21" runat="server" Text="Fecha de nacimiento"></asp:Label></td>
        <td style="height: 23px" colspan="6" align="left">
            <asp:TextBox ID="txtFechaNacDest" runat="server" Width="140px"  Font-Size="Small" CssClass="form_input" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="txtFechaNacDest" FilterType="Numbers,Custom"  ValidChars="/"  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--<asp:TextBox ID="TextBox70" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox71" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox72" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox73" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox74" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            &nbsp;
            <asp:Label ID="Label22" runat="server" Font-Bold="True" Text="yyyy/mm/dd"></asp:Label>
            </td>
       
    </tr>
    </table>
    <table style="width: 975px">
    <tr>
        <td style="height: 23px"><b>Observaciones</b></td>
    </tr>
    <tr>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox386" runat="server" Height="67px" TextMode="MultiLine" Width="955px" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" runat="server" TargetControlID="TextBox386" ValidChars="ABCDEFGHIJKLMNÑOPQRSTUVWXYZabcdefghijklmnñopqrstuvwxyzüÜáéíóúÁÉÍÓ 0123456789-/*();,:."  ></ajaxToolkit:FilteredTextBoxExtender>
        </td>
    </tr>
    </table>
    <table style="width: 975px">
    <tr>
        <td style="height: 23px" colspan="6" align="center"><B>INFORMACION DE LA PERSONA QUE REPORTA</B></td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2">Nombre de quien reporta</td>
        <td rowspan="3">Teléfono</td>
        <td style="height: 23px; width: 111px;">
            <asp:TextBox ID="txtIndicativoSolic" runat="server" Width="91px" style="text-align:left" Font-Size="Small" Enabled="False" CssClass="form_input" >4</asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="txtIndicativoSolic" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--  <asp:TextBox ID="TextBox21" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
             <asp:Label ID="Label5" runat="server" Text=":" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox23" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox24" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="txtNumeroSolic" runat="server" Width="126px" style="text-align:left" Font-Size="Small" Enabled="False" CssClass="form_input" ></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="txtNumeroSolic" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--            <asp:TextBox ID="TextBox82" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox83" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox84" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox85" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox86" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox87" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox88" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox89" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox90" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox91" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox92" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox93" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox94" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox95" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox96" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
        <td style="height: 23px">
            <asp:TextBox ID="txtExtSolic" runat="server" Width="85px" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" runat="server" TargetControlID="txtExtSolic" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--            <asp:TextBox ID="TextBox103" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox104" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox105" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label15" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox106" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox107" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:Label ID="Label24" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox108" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox109" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2">
            <asp:TextBox ID="TextBox402" runat="server" Width="476px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td style="height: 23px; width: 111px;" align="center">Indicativo</td>
        <td style="height: 23px" align="center">Número</td>
        <td style="height: 23px" align="center">Extensión</td>
    </tr>
    <tr>
        <td style="height: 23px">Cargo o Actvidad:</td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox419" runat="server" Width="359px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td style="height: 23px; width: 111px;" align="center">Teléfono Celular:</td>
        <td style="height: 23px" colspan="2" align="center">
            <asp:TextBox ID="txtCelularSolicita" runat="server" Width="200px"  Font-Size="Small" CssClass="form_input" ></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" runat="server" TargetControlID="txtCelularSolicita" FilterType="Numbers"  ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>

            <%--            <asp:TextBox ID="TextBox114" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox115" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox116" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox117" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox118" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox119" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox120" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <asp:TextBox ID="TextBox121" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>--%>
            </td>
    </tr>
    <tr>
        <td style="height: 23px; font-size: xx-small;" colspan="6">MPS-SAS V5.0 2008-07-11</td>
    </tr>
    </table>
        
        <table style="width: 975px">
    <tr>
        <td style="height: 23px; width: 235px;" align="center">
            <b>Convenio del paciente:</b></td>
        <td style="height: 23px" align="left">
            <asp:DropDownList ID="ddlConvenio" runat="server" Width="653px" CssClass="form_input">
            </asp:DropDownList>
        </td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2" align="center">
            <asp:Button ID="Button1" runat="server" Text="Generar PDF" OnClick="Button1_Click" />
        &nbsp;<asp:Button ID="Button2" runat="server" Text="Imprimir PDF" OnClick="Button2_Click" Enabled="False" Visible="False" />
        </td>
    </tr>
</table>
      <asp:Literal ID="litScript" runat="server"></asp:Literal>
    
</asp:Content>