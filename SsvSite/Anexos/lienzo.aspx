﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="lienzo.aspx.cs" Inherits="Anexos_lienzo" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">

    <style>
.drop-shadow {
	margin:2em 20% 4em;
}
canvas {
	border-style:solid;
	border-color:#000000;
	border-radius:5px;
	border-width:1px;
}
</style>
</head>
 
<body>
    <form id="form1" runat="server">
    <div>
  <body onload="prepareCanvas();">
	<canvas id="canvasEjemplo" width="800" height="600"></canvas>
	<br><input type="button" id="borrador" value="Borrar Todo" />
 
	<script>

	    var miLienzo; // el canvas
	    var contexto; // el contexto
	    var canvasLimites; // los margenes del canvas
	    var flagPaint = false; // nos dice si pintar o no
	    var actualPos; // la posición actual donde hice click

	    function prepareCanvas() {
	        miLienzo = document.getElementById("canvasEjemplo");
	        contexto = miLienzo.getContext("2d"); // obtenemos el contexto ( dibujar en 2d)
	        canvasLimites = miLienzo.getBoundingClientRect(); // obtenemos los limites del canvas
	        miLienzo.addEventListener('mousedown', cambiarEstado, false);
	        miLienzo.addEventListener('mouseup', cambiarEstado, false);
	        miLienzo.addEventListener('mousemove', pintarLinea, false);
	        miLienzo.style.cursor = "hand";

	        borrador = document.getElementById("borrador");
	        borrador.addEventListener('click', erase, false);
	    }

	    function cambiarEstado() {
	        flagPaint = !flagPaint;
	        actualPos = obtenerCoordenadas(event);
	    }

	    function pintarLinea(event) {

	        if (flagPaint) {
	            var coordenadas = obtenerCoordenadas(event);
	            contexto.beginPath(); // comenzamos a dibujar
	            contexto.moveTo(actualPos.x, actualPos.y); // ubicamos el cursor en la posicion (10,10)
	            contexto.lineTo(coordenadas.x, coordenadas.y);
	            actualPos = {
	                x: coordenadas.x,
	                y: coordenadas.y
	            };
	            contexto.strokeStyle = "#000"; // color de la linea
	            contexto.stroke(); // dibujamos la linea
	        }
	    }

	    function obtenerCoordenadas(event) {
	        var posX;
	        var posY;

	        if (event.pageX || event.pageY) {
	            posX = event.pageX - canvasLimites.left;
	            posY = event.pageY - canvasLimites.top;
	        } else {
	            posX = event.clientX - canvasLimites.left;
	            posY = event.clientY - canvasLimites.top;
	        }

	        return {
	            x: posX,
	            y: posY
	        };
	    }
	    function erase() {
	        contexto.clearRect(0, 0, miLienzo.width, miLienzo.height);
	    }

	</script>
    </div>
    </form>
</body>
</html>
