﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/MasterNew.master" AutoEventWireup="true" CodeFile="ConsentimientosConsultaExterna.aspx.cs" Inherits="Anexos_ConsentimientosConsultaExterna" MaintainScrollPositionOnPostback="true" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <%--<script src="<%= Page.ResolveUrl(" ~/js/signatureConsultaExterna.js ") %>"></script>--%>
        <script src="../js/signatureConsultaExterna.js"></script>
            <script type="text/javascript">
                     function REPRODUCTOR() {
                         var mywindow = window.open("http://cessrvapp1/TutorialesCES/ConsultaExterna.mp4", "mywindow", "resizable=no,location=1,status=1,scrollbars=1,width=1000,height=700");
                     mywindow.opener = self;
                     mywindow.moveTo(500, 200);
                     mywindow.focus();
                 }
 </script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box box-primary">
                        <!--div Título-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h3 align="center" runat="server" class="box-title">Consentimientos Informados Consulta Externa</h3>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> 
                                    <asp:LinkButton ID="LinkButton1" runat="server" style ="float:right;font-size:large;" OnClientClick="REPRODUCTOR();" ><h3 class="fa fa-info-circle"></h3>&nbsp;Ayuda&nbsp;&nbsp;&nbsp;&nbsp;</asp:LinkButton>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!--div campo Tipo identificación-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <%--<h5>Tipo de identificación</h5>
                                    <asp:DropDownList ID="ddlTipoId" runat="server" AutoPostBack="True" class="form-control"> </asp:DropDownList>--%> </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                        </div>
                        <!--div campo Identificación-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <h5>Identificación del paciente</h5>
                                    <asp:TextBox ID="txtId" runat="server" class="form-control required" placeholder="Identificación del paciente" MaxLength="13"></asp:TextBox>
                                    <ajaxToolkit:FilteredTextBoxExtender ID="txtId_FilteredTextBoxExtender" runat="server" FilterInterval="17" TargetControlID="txtId" ValidChars="1234567890"> </ajaxToolkit:FilteredTextBoxExtender>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group"> </div>
                            </div>
                        </div>
                        <!--div botón Consultar-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-5">
                                <div class="form-group">
                                    <label>
                                        <asp:CheckBox ID="CheckPaciente" runat="server" Text="Firma cómo paciente" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckPaciente_CheckedChanged" /> </label>
                                    <br />
                                    <label>
                                        <asp:CheckBox ID="CheckRepresentante" runat="server" Text="Firma cómo representante" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckRepresentante_CheckedChanged" /> </label>
                                    <br />
                                    <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="btn btn-primary" OnClick="btnConsultar_Click" /> </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group"> </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--div tabla-->
        <div class="box box-primary" id="divtable" runat="server" visible="false">
            <div class="row">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Nombre Paciente :</h5> </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <div>
                                <asp:TextBox ID="LblNombre" runat="server" READONLY="true" CausesValidation="True" class="form-control"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <%--<h5>Consentiento :</h5>--%> </div>
                    </div>
                    <div class="col-md-7">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
                                        Anestesia
                                        </a>
                                    </h4> </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">  <label>
                                    <asp:CheckBox ID="chkconsAnestesia" runat="server" Text="F-HC-4 Consentimiento o disentimiento informado de anestesia." CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsAnestesia_CheckedChanged" /> </label>
                                <br /> </div>
                                   
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Cirugía y procedimientos médicos
                                    </a>
                                    </h4> </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                     <div class="panel-body">
                                        <label>
                                            <asp:CheckBox ID="chkconsCirugia" runat="server" Text="F-HC-3 Consentimiento o disentimiento informado para cirugías y procedimientos médicos. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugia_CheckedChanged" /> </label>
                                        <br /> 
                                        <label>
                                            <asp:CheckBox ID="chkconsCirugiaApendicectomia" runat="server" Text="F-HC-56 Consentimiento o disentimiento informado para apendicectomía. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugiaApendicectomia_CheckedChanged"/> </label>
                                        <br /> 
                                        <label>
                                            <asp:CheckBox ID="chkconsCirugiaColecistectomia" runat="server" Text="F-HC-57 Consentimiento o disentimiento informado para colecistectomía. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugiaColecistectomia_CheckedChanged" /> </label>
                                        <br /> 
                                        <label>
                                            <asp:CheckBox ID="chkconsCirugiaeventrorrafiasohernia" runat="server" Text="F-HC-58 Consentimiento o disentimiento informado para eventrorrafias o hernia umbilical. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugiaeventrorrafiasohernia_CheckedChanged" /> </label>
                                        <br /> 
                                        <label>
                                            <asp:CheckBox ID="chkconsCirugiaHerniorrafiainguinal" runat="server" Text="F-HC-59 Consentimiento o disentimiento informado para herniorrafia inguinal. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugiaHerniorrafiainguinal_CheckedChanged" /> </label>
                                        <br /> 
                                        <label>
                                            <asp:CheckBox ID="chkconsCirugialaparoscopiadiagnostica" runat="server" Text="F-HC-60 Consentimiento o disentimiento informado para laparoscopia diagnóstica. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugialaparoscopiadiagnostica_CheckedChanged" /> </label>
                                        <br /> 
                                        <label>
                                            <asp:CheckBox ID="chkconsCirugialaparotomiaexploratoria" runat="server" Text="F-HC-61 Consentimiento o disentimiento informado para laparotomía exploratoria. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugialaparotomiaexploratoria_CheckedChanged" /> </label>
                                        <br /> 
                                        <label>
                                            <asp:CheckBox ID="chkconsCirugialavadoperitonealterapeutico" runat="server" Text="F-HC-62 Consentimiento o disentimiento informado para lavado peritoneal terapéutico. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugialavadoperitonealterapeutico_CheckedChanged" /> </label>
                                        <br /> 
                                        <label>
                                            <asp:CheckBox ID="chkconsCirugiatoracostomia" runat="server" Text="F-HC-70 Consentimiento o disentimiento informado para toracostomia. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugiatoracostomia_CheckedChanged" /> </label>
                                        <br /> 

                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Ortopedia
                                    </a>
                                    </h4> </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body"> 
                                        <label>
                                    <asp:CheckBox ID="chkconsOrtopedia" runat="server" Text="F-HC-43 Consentimiento o disentimiento informado para procedimientos de ortopedia. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsOrtopedia_CheckedChanged" /> </label>
                                <br /> 
                                        <label>
                                    <asp:CheckBox ID="chkconsCirugiaortopedia" runat="server" Text="F-HC-71 Consentimiento o disentimiento informado para cirugías de ortopedia. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsCirugiaortopedia_CheckedChanged" /> </label>
                                <br />
                                    </div>
                                </div>
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFor">
                                    <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFor" aria-expanded="false" aria-controls="collapseFor">
                                    Urología
                                    </a>
                                    </h4> </div>
                                <div id="collapseFor" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFor">
                                    <div class="panel-body"> <label>
                                    <asp:CheckBox ID="chkconsUrologianefrectomias" runat="server" Text="F-HC-49 Consentimiento o disentimiento informado para nefrectomías. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsUrologianefrectomias_CheckedChanged" /> </label>
                                <br />
                                        <label>
                                    <asp:CheckBox ID="chkconsUrologianefrolitotomiapercutanea" runat="server" Text="F-HC-50 Consentimiento o disentimiento informado para nefrolitotomía percutánea. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsUrologianefrolitotomiapercutanea_CheckedChanged"  /> </label>
                                <br />
                                        <label>
                                    <asp:CheckBox ID="chkconsUrologiaendoscopicosurologia" runat="server" Text="F-HC-51 Consentimiento o disentimiento informado para procedimientos endoscópicos de urología. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsUrologiaendoscopicosurologia_CheckedChanged" /> </label>
                                <br />
                                        <label>
                                    <asp:CheckBox ID="chkconsUrologiacirugiaprostata" runat="server" Text="F-HC-52 Consentimiento o disentimiento informado para cirugías de próstata. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsUrologiacirugiaprostata_CheckedChanged" /> </label>
                                <br />
                                </div>
                                </div>
                            </div>
                            
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFive">
                                    <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                    Dermatología
                                    </a>
                                    </h4> </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="panel-body"> <label>
                                    <asp:CheckBox ID="chkconsDermatologia" runat="server" Text="F-HC-34 Consentimiento o disentimiento informado para dermatología. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsDermatologia_CheckedChanged" /> </label>
                                <br /> </div>
                                </div>
                            </div>

                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingSix">
                                    <h4 class="panel-title">
                                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSix" aria-expanded="false" aria-controls="collapseSix">
                                    Neurocirugía
                                    </a>
                                    </h4> </div>
                                <div id="collapseSix" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingSix">
                                    <div class="panel-body"> <label>
                                    <asp:CheckBox ID="chkconsNeurocirugiabloqueocolumna" runat="server" Text="F-HC-53 Consentimiento o disentimiento informado para bloqueo de columna. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsNeurocirugiabloqueocolumna_CheckedChanged" /> </label>
                                <br /> 
                                        <label>
                                    <asp:CheckBox ID="chkconsNeurocirugiacirugiacolumna" runat="server" Text="F-HC-54 Consentimiento o disentimiento informado para para cirugía de columna. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsNeurocirugiacirugiacolumna_CheckedChanged" /> </label>
                                <br /> 
                                        <label>
                                    <asp:CheckBox ID="chkconsNeurocirugiaresecciontumorcerebral" runat="server" Text="F-HC-55 Consentimiento o disentimiento informado para para para resección de tumor cerebral. " CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" OnCheckedChanged="chkconsNeurocirugiaresecciontumorcerebral_CheckedChanged" /> </label>
                                <br /> 
                                    </div>
                                </div>
                            </div>

                            <asp:HiddenField ID="PaneName" runat="server" />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div id="divAutorizado" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="row">
                        <div class="col-md-1">
                            <div class="form-group"> </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h5>Cédula Autorizado :</h5> </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <asp:Label ID="Label2" runat="server" Text="Campos para autorizado" ForeColor="#0066FF"></asp:Label>
                                <asp:TextBox ID="txtIdAut" runat="server" class="form-control" placeholder="Cédula Autorizado" MaxLength="13"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="txtIdAut_FilteredTextBoxExtender" runat="server" TargetControlID="txtIdAut" ValidChars="1234567890"> </ajaxToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group"> </div>
                        </div>
                    </div>
                    <!--*****************************************************************-->
                    <div class="row">
                        <div class="col-md-1">
                            <div class="form-group"> </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group">
                                <h5>Nombre Autorizado :</h5> </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <asp:TextBox ID="txtNomAut" runat="server" class="form-control" placeholder="Nombre Autorizado" MaxLength="100"></asp:TextBox>
                                <ajaxToolkit:FilteredTextBoxExtender ID="txtNomAut_FilteredTextBoxExtender" runat="server" TargetControlID="txtNomAut" ValidChars="abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ´ "> </ajaxToolkit:FilteredTextBoxExtender>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="form-group"> </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divConsideraciones" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Consideraciones :</h5> </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <div>
                                <asp:Label ID="lblConsideraciones" runat="server" ForeColor="#0066FF"></asp:Label>
                                <br />
                                <asp:TextBox ID="txtConsideraciones" runat="server" CausesValidation="True" Height="83px" TextMode="MultiLine" class="form-control required"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divconsentmientoDisentimiento" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Autorización :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckConsentimiento" runat="server" Text="Consentimiento" CssClass="icheck-material-blue" OnCheckedChanged="CheckConsentimiento_CheckedChanged" AutoPostBack="True" /> </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckDisentimiento" runat="server" Text="Disentimiento" CssClass="icheck-material-blue" OnCheckedChanged="CheckDisentimiento_CheckedChanged" AutoPostBack="True" /> </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divcirugia" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Tipo de intervención :</h5> </div>
                    </div>
                    <div class="col-md-7">
                        <div class="form-group">
                            <div>
                                <asp:TextBox ID="txtTipoIntervencion" runat="server" CausesValidation="True" class="form-control required" placeholder="Tipo de intervención"></asp:TextBox>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divAnestesia" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Tipos anestesias :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaGeneral" runat="server" Text="Anestesia general" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaRegional" runat="server" Text="Anestesia regional o bloqueo" CssClass="icheck-material-blue" /> </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaConductiva" runat="server" Text="Anestesia conductiva" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaLocal" runat="server" Text="Anestesia local asistida" CssClass="icheck-material-blue" /> </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divOrtopedia" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Tipos intervención :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckInfiltracion" runat="server" Text="Infiltración" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckCambioYeso" runat="server" Text="Cambio de yeso" CssClass="icheck-material-blue" /> </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckOnicectomia" runat="server" Text="Onicectomía" CssClass="icheck-material-blue" /> </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divUrologia" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Tipos intervención :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckNefrectomiaParcialAbierta" runat="server" Text="Nefrectomía parcial abierta" CssClass="icheck-material-blue" /> </label>
                            <br />
                           
                            <label>
                                <asp:CheckBox ID="CheckNefrectomiaSimpleLaparoscopica" runat="server" Text="Nefrectomía simple laparoscópica" CssClass="icheck-material-blue" /> </label>
                             <br />
                            <label>
                                <asp:CheckBox ID="CheckNefrectomiaParcialLaparoscopica" runat="server" Text="Nefrectomía parcial laparoscópica" CssClass="icheck-material-blue" /> </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckNefrectomiaRadicalAbierta" runat="server" Text="Nefrectomía radical abierta" CssClass="icheck-material-blue" /> </label>
                            <br />
                          
                            <label>
                                <asp:CheckBox ID="CheckNefrectomiaSimpleAbierta" runat="server" Text="Nefrectomía simple abierta" CssClass="icheck-material-blue" /> </label>
                             <br />
                            <label>
                                <asp:CheckBox ID="CheckNefrectomiaRadicalLaparoscopica" runat="server" Text="Nefrectomía radical laparoscópica" CssClass="icheck-material-blue" /> </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divUrologia1" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Tipos intervención :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckNefroUreterolitotomiaendoscopica" runat="server" Text="Nefro-Ureterolitotomía endoscópica (con o sin láser)" CssClass="icheck-material-blue" /> </label>
                            <br />
                           
                            <label>
                                <asp:CheckBox ID="CheckUreteropielorrenoscopia" runat="server" Text="Ureteropielorrenoscopia " CssClass="icheck-material-blue" /> </label>
                             <br />
                            <label>
                                <asp:CheckBox ID="CheckCistoscopia" runat="server" Text="Cistoscopia con extracción de cuerpo extraño" CssClass="icheck-material-blue" /> </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckEndopielotomia" runat="server" Text="Endopielotomía con láser " CssClass="icheck-material-blue" /> </label>
                            <br />
                          
                            <label>
                                <asp:CheckBox ID="CheckLesionvesical" runat="server" Text="Resección o fulguración de lesión vesical" CssClass="icheck-material-blue" /> </label>
                             <br />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divUrologia2" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Tipos intervención :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="Checkprostataradical" runat="server" Text="Resección de próstata abierta radical" CssClass="icheck-material-blue" /> </label>
                            <br />
                           
                            <label>
                                <asp:CheckBox ID="Checkprostatasimple" runat="server" Text="Resección de próstata abierta simple " CssClass="icheck-material-blue" /> </label>
                             <br />
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="Checklaparoscopicaprostata" runat="server" Text="Resección laparoscópica de próstata " CssClass="icheck-material-blue" /> </label>
                            <br />
                          
                            <label>
                                <asp:CheckBox ID="Checktransuretalprostata" runat="server" Text="Resección transuretral de próstata (no se saca toda la próstata, solo la parte que lo obstruye)" CssClass="icheck-material-blue" /> </label>
                             <br />
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divNeurocirugia" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Tipos intervención :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="Checkcolmnacervical" runat="server" Text="Bloqueo de columna cervical" CssClass="icheck-material-blue" /> </label>
                            <br />
                           
                            <label>
                                <asp:CheckBox ID="Checkcolumnadorsal" runat="server" Text="Bloqueo de columna dorsal " CssClass="icheck-material-blue" /> </label>
                             <br />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="Checkcolumnalumbar" runat="server" Text="Bloque de columna lumbar " CssClass="icheck-material-blue" /> </label>
                            <br />
                          
                            <label>
                                <asp:CheckBox ID="Checkcolumnasacra" runat="server" Text="Bloqueo de columna sacra" CssClass="icheck-material-blue" /> </label>
                             <br />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divNeurocirugia1" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Tipos intervención :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="Checklaminectomiacervical" runat="server" Text="Laminectomía cervical" CssClass="icheck-material-blue" /> </label>
                            <br />                          
                            <label>
                                <asp:CheckBox ID="Checkmicrodiscectomialumbar" runat="server" Text="Microdiscectomía lumbar " CssClass="icheck-material-blue" /> </label>
                             <br />
                            <label>
                                <asp:CheckBox ID="Checklaminectomiatoracica" runat="server" Text="Laminectomía torácica " CssClass="icheck-material-blue" /> </label>
                             <br />
                            <label>
                                <asp:CheckBox ID="Checkartrodesiscolumnacervical" runat="server" Text="Artrodesis de columna cervical " CssClass="icheck-material-blue" /> </label>
                             <br />
                            <label>
                                <asp:CheckBox ID="Checklaminectomialumbar" runat="server" Text="Laminectomía lumbar " CssClass="icheck-material-blue" /> </label>
                             <br />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="Checkartrodesiscolumnatoracica" runat="server" Text="Artrodesis de columna torácica " CssClass="icheck-material-blue" /> </label>
                            <br />                         
                            <label>
                                <asp:CheckBox ID="Checkmicrodiscectomia" runat="server" Text="Microdiscectomía cervical" CssClass="icheck-material-blue" /> </label>
                             <br />
                            <label>
                                <asp:CheckBox ID="Checkartrodesiscolumnalumbar" runat="server" Text="Artrodesis de columna lumbar " CssClass="icheck-material-blue" /> </label>
                             <br />
                            <label>
                                <asp:CheckBox ID="Checkmicrodiscectomiatoracica" runat="server" Text="Microdiscectomía torácica " CssClass="icheck-material-blue" /> </label>
                             <br />
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divExplicacion" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Explicación del procedimiento :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckDiagnostico" runat="server" Text="Aclaración del diagnóstico" CssClass="icheck-material-blue" /> </label>
                            <br />
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="CheckPatologia" runat="server" Text="Mejorar parcial o definitivamente la patología" CssClass="icheck-material-blue" /> </label>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divEVENTRORRAFIAS" runat="server" visible="false">
                <div class="box-header with-border">
                    <div class="col-md-1">
                        <div class="form-group"> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <h5>Tipo de intervención :</h5> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="Checkeventrorrafias" runat="server" Text="Herniorrafía Umbilical" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br /> </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>
                                <asp:CheckBox ID="Checkhernia" runat="server" Text="Eventrorrafía" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br /> </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group"> </div>
                    </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <br />
            <div class="box-header with-border">
            <div class="row">
                <div class="col-md-1">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group"></div>
                </div>
                <div class="col-md-7">
                    <div class="form-group">
                      
                            <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-primary" Text="Aceptar" OnClick="btnGuardar_Click" visible="false" />
                            <asp:Button ID="btnReview" runat="server" CssClass="btn btn-primary" Text="Previsualizar" OnClick="btnReview_Click" Visible="false" />
                            <asp:Button ID="btnCaptureFirm" type="submit" class="btn btn-danger" runat="server" OnClick="Capture_Firma_Click" Text="Capturar firma" Visible="false" />
                            <asp:Button ID="btnCaptureFirmMedico" class="btn btn-danger" runat="server" Text="Firma especialista" Visible="false" OnClick="btnCaptureFirmMedico_Click" />&nbsp;&nbsp;
                            <label>
                                <asp:CheckBox ID="CheckAprobacionMedico" runat="server" Text="Aprobación médico" CssClass="icheck-material-blue" visible="false" /> </label>
                            <br />
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckUtilizarFirmaBD" runat="server" Text="Capturar nueva firma" CssClass="icheck-material-blue" AutoPostBack="True" Visible="false" class="form-control" OnCheckedChanged="CheckUtilizarFirmaBD_CheckedChanged" /> </label>
                            <br />
                            <br />
                            <%--<label>
                                <asp:CheckBox ID="CheckNueva" runat="server" Text="Utilizar nueva firma" CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" class="form-control" /> </label>
                            <br />--%>
                            <%--<label>
                                <asp:CheckBox ID="chkcons6" runat="server" Text="F-SI-5 Autorización para tratamientos de datos personales" CssClass="icheck-material-blue" AutoPostBack="True" Visible="true" Checked="true" class="form-control" /> </label>--%>
                            <br />
                            <br /> </div>
                </div>
                <div class="col-md-1">
                    <div class="form-group">
                        <div id="imageBox" class="boxed" style="height:37mm;width:72mm" ;></div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript">
    $(function () {
        var paneName = $("[id*=PaneName]").val() != "" ? $("[id*=PaneName]").val() : "collapseOne";
             
        //Remove the previous selected Pane.
        $("#accordion .in").removeClass("in");
             
        //Set the selected Pane.
        $("#" + paneName).collapse("show");
             
        //When Pane is clicked, save the ID to the Hidden Field.
        $(".panel-heading a").click(function () {
            $("[id*=PaneName]").val($(this).attr("href").replace("#", ""));
        });
    });
</script>
    </asp:Content>