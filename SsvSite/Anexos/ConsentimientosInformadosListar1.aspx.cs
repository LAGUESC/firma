﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Anexos_ConsentimientosInformadosListar1 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Consentimientos Informados", this.Page);
        }
    }

    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        CargarConsentimientos();
        divtable.Visible = true;
    }
    public void CargarConsentimientos()
    {
        //if (filtro.SelectedValue == "1")
        //{
            DataTable dtTodosRegistros = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_LISTAR();
            Session["dtTodosRegistros"] = dtTodosRegistros;
            ViewState["dtPaginas"] = dtTodosRegistros;
            Procedimientos.LlenarGrid(dtTodosRegistros, gvConsentimientos);

            foreach (GridViewRow row in gvConsentimientos.Rows)
            {
                int ID_CONSENTIMIENTO_PDF = Convert.ToInt32(gvConsentimientos.DataKeys[row.RowIndex].Values[0]);
                bool APROBACION_MEDICO = (row.FindControl("CheckAprobacionMedico") as CheckBox).Checked;

            }

        //}
        //else if (filtro.SelectedValue == "3")
        //{
        //    txtId.Visible = false;
        //    string Cedula = Session["Identificacion"].ToString();
        //    DataTable dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_LISTAR_X_CEDULA(Cedula);
        //    Session["dtTodosRegistros"] = dt;
        //    ViewState["dtPaginas"] = dt;
        //    Procedimientos.LlenarGrid(dt, gvConsentimientos);
        //    foreach (GridViewRow row in gvConsentimientos.Rows)
        //    {
        //        int ID_CONSENTIMIENTO_PDF = Convert.ToInt32(gvConsentimientos.DataKeys[row.RowIndex].Values[0]);
        //        bool APROBACION_MEDICO = (row.FindControl("CheckAprobacionMedico") as CheckBox).Checked;

        //    }

        //}
        //else
        //{
        //    DataTable dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_LISTAR_X_CEDULAPA(txtId.Text);
        //    Session["dtTodosRegistros"] = dt;
        //    ViewState["dtPaginas"] = dt;
        //    Procedimientos.LlenarGrid(dt, gvConsentimientos);
        //    foreach (GridViewRow row in gvConsentimientos.Rows)
        //    {
        //        int ID_CONSENTIMIENTO_PDF = Convert.ToInt32(gvConsentimientos.DataKeys[row.RowIndex].Values[0]);
        //        bool APROBACION_MEDICO = (row.FindControl("CheckAprobacionMedico") as CheckBox).Checked;

        //    }
        //}
    }

    protected void ImgPDF_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton btn = (ImageButton)sender;
        GridViewRow row = (GridViewRow)btn.NamingContainer;
        int i = Convert.ToInt32(row.RowIndex);

        DataTable Docs = (DataTable)Session["dtTodosRegistros"];
        //DataTable Resultado = Docs.Select("Archivo = '" + gvDocumentos.Rows[i].Cells[1].Text + "'").CopyToDataTable();
        DataTable Resultado = Docs.Select("CONSENTIMIENTO_HASH = '" + Docs.Rows[i][6].ToString() + "'").CopyToDataTable();

        Session["Datos"] = Resultado;

        string url = "VerPDF.aspx";
        Response.Write("<script type='text/javascript'>window.open('" + url + "', 'window','resizable=no,location=1,status=1,scrollbars=1,width=1200,height=900,left=400,top=50');</script>");
    }


    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        foreach (GridViewRow row in gvConsentimientos.Rows)
        {
            int ID_CONSENTIMIENTO_PDF = Convert.ToInt32(gvConsentimientos.DataKeys[row.RowIndex].Values[0]);
            bool APROBACION_MEDICO = (row.FindControl("CheckAprobacionMedico") as CheckBox).Checked;

            DataTable dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_ACTUALIZAR_APROBACION_MEDICO(ID_CONSENTIMIENTO_PDF, APROBACION_MEDICO);
        }
        Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
    }
}
