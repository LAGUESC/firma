﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;
using System.IO;

public partial class Anexos_srcImprimirAnexo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Busqueda de Anexos Generados para cerrar el evento", this.Page);
        }
        litScript.Text = string.Empty;

    }
    protected void txtId_TextChanged(object sender, EventArgs e)
    {
        consultarConvenios(txtId.Text.Trim());
    }

    private void consultarConvenios(string IdPaciente)
    {
        DataTable dtSql = new ClinicaCES.Logica.LAnexos().AnexosBuscarImprimirSql(IdPaciente);
        if (dtSql.Rows.Count > 0)
        {
            pnlInforme.Visible = true;
            Procedimientos.LlenarGrid(dtSql, gvAnexos);
            ViewState["dtPaginas"] = dtSql;
            ViewState["dtPaginas2"] = dtSql;
            txtId.Text = "";
        }
        else
        {
            pnlInforme.Visible = false;
            Procedimientos.Script("mensajini", "Mensaje(76)", this.Page);
        }
    }
    protected DataTable llenardt(DataTable dt)
    {
        DataTable dtGrid = dt;
        ViewState["dtPaginas"] = dtGrid;
        ViewState["dtPaginas2"] = dtGrid;
        return dtGrid;
    }
    protected void gvAnexos_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        
        string msg = "";
        string ruta = gvAnexos.Rows[e.RowIndex].Cells[1].Text;
        try
        {
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", ruta));
            Response.ContentType = "application/pdf";
            Response.WriteFile(Server.MapPath(Path.Combine("~/AUTPDF", ruta)));
            Response.End();
        }
        catch
        {
            msg = "74";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    
}