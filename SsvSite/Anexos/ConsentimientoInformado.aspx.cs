﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography;
using System.Text;
using System.IO;


public partial class Anexos_BusquedaAnexo : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblDescripcion.Text = "Consentimientos Informados";
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Consentimientos Informados", this.Page);
            Procedimientos.LlenarCombos(ddlTipoId, new ClinicaCES.Logica.LMaestros().ListaTiposIdentificacion(), "ID", "ID");
        }
        this.Form.DefaultButton = btnConsultar.UniqueID;
        litScript.Text = string.Empty;
    }

    public class Encrypt
    {
        public static string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
    }

    protected void Click_Botones(object sender, EventArgs e)
    {

        string cadenaEncriptada = Encrypt.GetMD5(DateTime.Now.ToString());

        //if (sender.Equals(btnGeneraPDF))
        //{
            
        //    string html = retornarHtml();
        //    var htmlContent = String.Format(html);
        //    var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        //    htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf"));
        //    string path = "\\\\CESSRVAPP1\\AUTPDF\\firma.BMP";
        //    File.Delete(path);
        //}
        //else if (sender.Equals(btnConsultar))
        //    //Consultar(ddlTipoId.SelectedValue.ToString(), txtId.Text);
        //else if (sender.Equals(btnFirmar))
        //{
        //    //string path = "C:\\Program Files\\WinRAR\\WinRAR.exe";
        //    string path = "C:\\COMPILADOS\\FIRMAS_TABLET\\SETUP.EXE";
        //    System.Diagnostics.Process.Start(path, "");
        //}
        
    }
    private void Consultar(string Id)
    {
        if (Id == "")
        {
            pnlInforme.Visible = false;
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
        }
        else
        {
            DataSet ds = new ClinicaCES.Logica.LBusquedaPacientes().BusquedaPaciente(Id);
            DataTable dtInforme = ds.Tables[0];
            DataTable dtPagina = Procedimientos.dtFiltrado("IDENTIFICACION", "", dtInforme);
            string[] campo = { "IDENTIFICACION" };
            if (dtInforme.Rows.Count > 0)
            {
                DataRow row = dtInforme.Rows[0];
                LblNombre.Text = row["PACIENTE"].ToString();
                LblCama.Text = row["CAMA"].ToString();
                pnlInforme.Visible = true;
            }
            else
            {
                pnlInforme.Visible = false;
                Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
            }
        }
    }
   


    protected string retornarHtml()
    {
        string Encabezado = "", Representacion = "", ConsultaHC = "", Procedimientosenfemermeria = "", PiePagina = "", html = "", AcompanantePermanente="";
        try
        {
 
            string path = Server.MapPath("../img/logo.jpg");
            string firma = Server.MapPath("../AUTPDF/firma.bmp");


            Encabezado = "<table style='width: 985px; height: 96px;'> " +
            "<tbody> " +
            "<tr> " +
            "<td style='width: 309px;'><IMG SRC='" + path + "' WIDTH=80 HEIGHT=80 ></td> " +
            "<td style='width: 855px;' align='center'> " +
            "<p><strong>CONSENTIMIENTOS INFORMADOS</strong></p> " +
            "</td> " +
            "<td style='width: 336px;' align='center'>&nbsp;</td> " +
            "</tr> " +
             " <tr> " +
            "<td style='width: 309px; text-align: right;' align='center'>&nbsp;</td> " +
            "<td style='width: 855px; text-align: left;' align='center'>&nbsp;</td> " +
            "<td style='width: 336px;' align='center'>&nbsp;</td> " +
            "</tr>	" +
            "<tr>" +
            "<td style='width: 309px; text-align: right;' align='left'><strong>Fecha de notificacion :</strong></td> " +
            "<td style='width: 855px; text-align: left;' align='left'><big><strong>" + DateTime.Today.ToString("MMMM dd, yyyy") + "</strong></big></td> " +
            "<td style='width: 336px;' align='center'>&nbsp;</td>" +
            "</tr> " +
            "<tr> " +
            "<td style='width: 309px; text-align: justify;' colspan='3' align='center'>" +
            "<p>Yo,&nbsp;&nbsp; <big><strong>" + LblNombre.Text + "</strong></big> &nbsp;&nbsp;, identificado con &nbsp;&nbsp; <big>" + ddlTipoId.SelectedValue.ToString() + "</big>  &nbsp;&nbsp; No.  &nbsp;&nbsp;<strong>" + txtId.Text.Trim() + "&nbsp;&nbsp;</strong> , obrando en calidad de: Paciente Responsable del paciente (Padre o Madre si es menor; representante legal, familiar o representante u otras personas que figuren como tales en la H. C.) , hago las siguientes declaraciones:</p> " +
            "<p>&nbsp;</p> " +
            "</td> " +
            "</tr>" +
            "</tbody> " +
            "</table>";


            if (chkcons1.Checked)
            {

                Representacion = "<table style='width: 985px; height: 96px;'> " +
                        "<tbody>" +
                        "<tr>" +
                        "<td style='width: 309px;'>&nbsp;</td>" +
                        "<td style='width: 855px;'>&nbsp;</td>" +
                        "<td style='width: 336;'>&nbsp;</td>" +
                        "</tr>" +

                        "<tr>" +
                        "<td style='width: 309px;'>&nbsp;</td>" +
                        "<td style='width: 855px;' align='center'>" +
                        "<p><strong>AUTORIZACION DE REPRESENTACION</strong></p>" +
                        "</td>" +
                        "<td style='width: 336px;' align='center'>" +
                        "<p><strong>F-HC-2</strong></p>" +
                        "<p><strong>versi&oacute;n 3</strong></p>" +
                        "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px; text-align: right;' align='center'>&nbsp;</td>" +
                        "<td style='width: 855px; text-align: left;' align='center'>&nbsp;</td>" +
                        "<td style='width: 336px;' align='center'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px; text-align: justify;' colspan='3' align='center'>" +
                        "<p>Por medio del presente documento, en forma libre, en pleno uso de mis facultades mentales y sin limitaciones o impedimentos de car&aacute;cter m&eacute;dico o legal, considerando que mi historia cl&iacute;nica es un documento privado sometido a reserva, y que &uacute;nicamente puede ser conocido por terceros con mi previa autorizaci&oacute;n, AUTORIZO a &nbsp;&nbsp; <big><strong> " + txtNomAut.Text.Trim().ToUpper() + ".</strong></big> &nbsp;&nbsp; con Identificación &nbsp;&nbsp; <big><strong> " + txtIdAut.Text.Trim() + ". </strong></big> &nbsp;&nbsp;  a acceder a mi historia cl&iacute;nica o a solicitar copia de esta, como tambi&eacute;n a tomar decisiones sobre mi conducta m&eacute;dica en caso de que yo tenga minusval&iacute;a mental durante esta hospitalizaci&oacute;n.</p>" +
                        "</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>";
            }


            if (chkcons2.Checked)
            {

                ConsultaHC = "<table style='width: 985px; height: 96px;'> " +
                        "<tbody>" +
                        "<tr>" +    
                        "<td style='width: 309px;'>&nbsp;</td>" +
                        "<td style='width: 855px;'>&nbsp;</td>" +
                        "<td style='width: 336;'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px;'>&nbsp;</td>" +
                        "<td style='width: 855px;' align='center'>" +
                        "<p><strong>AUTORIZACION PARA CONSULTA DE HISTORIA CLINICA</strong></p> " +
                        "<p><strong>POR PERSONAL ADMINISTRATIVO</strong></p> " +
                        "</td>" +
                        "<td style='width: 336px;' align='center'>" +
                        "<p><strong>F-HC-13</strong></p>" +
                        "<p><strong>versi&oacute;n 3</strong></p>" +
                        "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px; text-align: right;' align='center'>&nbsp;</td>" +
                        "<td style='width: 855px; text-align: left;' align='center'>&nbsp;</td>" +
                        "<td style='width: 336px;' align='center'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px; text-align: justify;' colspan='3' align='center'>" +
                        "<p>considerando que la historia cl&iacute;nica es un documento privado sometido a reserva, y que &uacute;nicamente puede ser conocido por terceros previa autorizaci&oacute;n, los abajo firmantes obrando de manera consciente y voluntaria autorizamos expresa y permanentemente a la CLINICA CES para que al obrar como Instituci&oacute;n Prestadora de Servicios de Salud acceda a las historias cl&iacute;nicas o su fotocopia, as&iacute; como la informaci&oacute;n en ellas incorporada relativa a mi estado de salud. De la misma manera, autorizamos a esta instituci&oacute;n y a los profesionales de la Salud, para suministrar y dar a conocer a la Empresa Administradora de Planes de Beneficios tanto las historias cl&iacute;nicas como la informaci&oacute;n en ellas incorporada relativa a mi estado de salud.<br />Esta autorizaci&oacute;n se imparte para que CLINICA CES pueda realizar actividades tales como la auditor&iacute;a m&eacute;dica que considere pertinente, evaluar el proceso de atenci&oacute;n, efectuar el cobro a Entidades Administradoras de Riesgos Profesionales, a otras Entidades Promotoras de Salud, a la Aseguradora que ha emitido el Seguro Obligatorio de Accidentes de Tr&aacute;nsito o al Fondo de Solidaridad y Garant&iacute;a, aportarla como prueba a un proceso judicial, entre otros. Lo anterior para efectos de lo establecido en la Ley 23 de 1981, su Decreto reglamentario 3380 de 1981 y dem&aacute;s normas que los modifiquen o adicionen.</p>" +
                        "</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>";
            }

            if (chkcons3.Checked)
            {
                Procedimientosenfemermeria = "<table style='width: 985px; height: 96px;'> " +
                        "<tbody>" +
                        "<tr>" +
                        "<td style='width: 309px;'>&nbsp;</td>" +
                        "<td style='width: 855px;'>&nbsp;</td>" +
                        "<td style='width: 336;'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px;'>&nbsp;</td>" +
                        "<td style='width: 855px;' align='center'>" +
                        "<p><strong>CONSENTIMIENTO INFORMADO SOBRE PROCEDIMIENTO DE ENFERMERIA</strong></p> " +
                        "</td>" +
                        "<td style='width: 336px;' align='center'>" +
                        "<p><strong>F-HC-7</strong></p>" +
                        "<p><strong>versi&oacute;n 3</strong></p>" +
                        "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px; text-align: right;' align='center'>&nbsp;</td>" +
                        "<td style='width: 855px; text-align: left;' align='center'>&nbsp;</td>" +
                        "<td style='width: 336px;' align='center'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px; text-align: justify;' colspan='3' align='center'>" +
                        "<p>1. Por medio del presente documento, en forma libre, en pleno uso de mis facultades mentales y sin limitaciones o impedimentos de car&aacute;cter m&eacute;dico o legal, habiendo recibido informaci&oacute;n por parte del m&eacute;dico tratante o par m&eacute;dico, otorgo mi consentimiento para que EL PERSONAL DE ENFERMERIA DE LA CL&Iacute;NICA CES me practique LOS PROCEDIMIENTOS DE ENFERMER&Iacute;A ordenados por el m&eacute;dico o asociados al cuidado. <br />2. Algunos de los procedimientos son: Toma de muestras de laboratorio, aplicaci&oacute;n de medicamentos venosos y orales, paso de sondas, canalizaci&oacute;n venosa, curaciones, y otros que el m&eacute;dico ordene <br />3. Aunque son procedimientos seguros, pueden presentarse complicaciones como: Hematomas, infecciones, dolores locales o sangrados y otros impredecibles. 4. Declaro que he sido advertido(a) por el personal de enfermer&iacute;a sobre los riesgos y beneficios de los procedimientos, y que la pr&aacute;ctica de los mismos compromete una actividad de medio en el campo diagn&oacute;stico y/o terap&eacute;utico, pero no de resultado. <br />5. Certifico que el presente documento ha sido le&iacute;do y entendido en su integridad por m&iacute; y que las dudas e interrogantes que he formulado me han sido resueltos mediante explicaciones claras sobre los asuntos o temas de mi inter&eacute;s.</p> " +
                        "<p>OTRAS CONSIDERACIONES<br /> <big>" + txtConsideraciones.Text.Trim() +
                        "</big></td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>";
            }


            if (chkcons4.Checked)
            {
                AcompanantePermanente = "<table style='width: 985px; height: 96px;'> " +
                        "<tbody>" +
                        "<tr>" +
                        "<td style='width: 309px;'>&nbsp;</td>" +
                        "<td style='width: 855px;'>&nbsp;</td>" +
                        "<td style='width: 336;'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px;'>&nbsp;</td>" +
                        "<td style='width: 855px;' align='center'>" +
                        "<p><strong>NOTIFICACION DE ACOMPANANATE PERMANENTE</strong></p> " +
                        "<p><strong>PARA PACIENTES HOSPITALIZADOS</strong></p> " +
                        "</td>" +
                        "<td style='width: 336px;' align='center'>" +
                        "<p><strong>F-HC-4</strong></p>" +
                        "<p><strong>versi&oacute;n 2</strong></p>" +
                        "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px; text-align: right;' align='center'>&nbsp;</td>" +
                        "<td style='width: 855px; text-align: left;' align='center'>&nbsp;</td>" +
                        "<td style='width: 336px;' align='center'>&nbsp;</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<td style='width: 309px; text-align: justify;' colspan='3' align='center'>" +
                        "<p>LA CLINICA CES, Institucion prestadora de servicios de salud. preocupada por el bienestar y seguridad de sus usuarios, le notifica la obligacion de permanecer las 24 horas acompananado al paciente &nbsp;&nbsp; <big><B>" + LblNombre.Text.ToUpper() + "</B> </p> <p>HABITACION : &nbsp;&nbsp; " + LblCama.Text + " &nbsp;&nbsp;  </p>" +
                        "</td>" +
                        "</tr>" +
                        "</tbody>" +
                        "</table>";
            }



            PiePagina = "</br></br></br><table style='width: 985px; height: 96px;'> " +
                    "<tbody><tr> " +
                    "<td style='width: 855px;' align='left' colspan='4'>" +
                    "<p><strong>CALIDAD EN QUE SE OTORGAN LOS CONSENTIMIENTOS</strong></p> " +
                    "</td></tr><tr>" ;
                    
                    if (chkcons1.Checked) {

                        PiePagina = PiePagina + "<td style='width: 855px;' align='left' colspan='4'>" +
                        "<p>Como Paciente : _____ </p> " +
                        "<p>Como Responsable del paciente (Padre o Madre si es menor; representante legal, familiar o representante u otras personas que figuren como tales en la HC): __X__  </p> " +
                        "</td>";

                    } else
                    {

                        PiePagina = PiePagina + "<td style='width: 855px;' align='left' colspan='4'>" +
                        "<p>Como Paciente : __X__ </p> " +
                        "<p>Como Responsable del paciente (Padre o Madre si es menor; representante legal, familiar o representante u otras personas que figuren como tales en la HC): _____ </p> " +
                        "</td>";

                    }

                    PiePagina = PiePagina + "</tr><td style='width: 309px;'>&nbsp;</td>" +
                    "<td style='width: 855px;'>&nbsp;</td>" +
                    "<td style='width: 336;'>&nbsp;</td>" +
                    "<tr> " +
                    "<td style='width: 309px; text-align: right;' align='center'>&nbsp;</td> " +
                    "<td style='width: 855px; text-align: left;' align='center'>&nbsp;</td> " +
                    "<td style='width: 336px;' align='center'>&nbsp;</td> " +
                    "</tr> " +
                    "<tr> " +
                    "<td style='width: 309px; text-align: justify;' colspan='3' align='center'> " +
                    

                    "<p><IMG SRC='" + firma + "' WIDTH=256 HEIGHT=131 ></p> " +
                    "<p>Firma del paciente o representante:</p> " +
                    "<p>Identificacion : &nbsp;&nbsp; <big><strong> " + txtIdAut.Text.Trim() + ". </strong></big> &nbsp;&nbsp;  </p> " +

                    "<p>Firma de la persona que informa la autorizaci&oacute;n:&nbsp;</p> " +
                    "<p>Firma de la persona que recibe el consentimiento:&nbsp;</p> " +
                    "</td> " +
                    "</tr>" +
                    "<td style='width: 855px;' align='left' colspan='4'>" +
                    "</br></br></br></br></br></br></br>" +
                    "<p>TRATAMIENTO DE DATOS PERSONALES: LA CLINICA CES le informa que los datos personales recopilados en este documento, se utilizan unicamente para las finalidades aqui descritas y las establecidas en su Politica de tratamiento y proteccion de datos personales y manual de aplicacion. Todo en concordancia con lo ordenado por la ley 1581 de 2012 y el decreto 1377 de 2013.</p> " +
                        "</td>" +

                    
                    
                    "</tbody></table> ";
                    
                    
            html = Encabezado + Representacion + ConsultaHC + Procedimientosenfemermeria + AcompanantePermanente + PiePagina;               
            return html;
        }

        catch
        { return html = ""; }

    }

    protected void chkcons1_CheckedChanged(object sender, EventArgs e)
    {
        if (chkcons1.Checked)
            pnlAcompanante.Visible = true;
        else
        {
            pnlAcompanante.Visible = false;
            txtIdAut.Text = "";
            txtNomAut.Text = "";
        }
    }
    protected void chkcons3_CheckedChanged(object sender, EventArgs e)
    {
        if (chkcons3.Checked)
            pnlEnfermeria.Visible = true;
        else
        {
            pnlEnfermeria.Visible = false;
            txtConsideraciones.Text = "";
        }
    }



    protected void Abrir_Click(object sender, EventArgs e)
    {
        string vtn = "window.open('lienzo.aspx','Dates','scrollbars=yes,resizable=no','height=300', 'width=300')";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "popup", vtn, true);
    }
}