﻿using ClinicaCES.Entidades;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Windows.Forms;

[WebService]
public partial class Anexos_ConsentimientosConsultaExterna : System.Web.UI.Page
{
    #region "ATRIBUTOS"
    string pathDescarga = "";
    string pathDescargaTratamiento = "";
    string firma = "";
    int Id_Firma = 0;
    string firmaMedico = "";
    DataTable dtFirma = null;
    DataTable dtFirmaMedico = null;
    Byte[] FileBufferTratamientoDatos = null;
    string Validar = "";
    #endregion

    #region "CONTROLES"
    protected void Page_Load(object sender, EventArgs e)
    {

        StreamReader reader = new StreamReader(Request.InputStream);
        String signature = Server.UrlDecode(reader.ReadToEnd());
        reader.Close();

        if (signature.StartsWith("bmpObj="))
        {
            if (Session["Validar"] != null)
            {
                drawimgFirmMedico(signature);
                Session["Validar"] = null;
            }
            else
            {
                drawimgFirm(signature);
            }

        }

    }
    protected void CheckPaciente_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckPaciente.Checked)
        {
            CheckRepresentante.Checked = false;
            txtIdAut.Text = "";
            txtNomAut.Text = "";
            divAutorizado.Visible = false;
            //btnCaptureFirm.Visible = false;
        }
    }
    protected void CheckRepresentante_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckRepresentante.Checked)
        {
            CheckPaciente.Checked = false;
            divAutorizado.Visible = true;
            //btnCaptureFirm.Visible = false;
        }
        else
        {
            divAutorizado.Visible = false;
            txtIdAut.Text = "";
            txtNomAut.Text = "";

        }
    }
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        btnGuardar.Visible = false;
        //btnCaptureFirm.Visible = false;
        Consultar(txtId.Text);
        Session["Cedula"] = txtId.Text;

    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (ValidarDatosHTML())
        {
            try
            {

                string CedUser = Session["Identificacion"].ToString();
                string NombreUser = Session["Nombre"].ToString();


                string cadenaEncriptada = Encrypt.GetMD5(DateTime.Now.ToString());

                string html = retornarHtml();
                var htmlContent = String.Format(html);
                var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                pathDescarga = "../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf";


                htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf"));

                GuardarEvento(txtId.Text);
                GuardarPDF(txtId.Text, CedUser, NombreUser);

                divtable.Visible = false;
                Limpiar();
                LimpiarCheck();

                //DirectoryInfo di = new DirectoryInfo("C:\\Users\\LUISAGUDELO\\Desktop\\LUNES\\FIRMA\\SsvSite\\AUTPDF\\");
                DirectoryInfo di = new DirectoryInfo("C:\\inetpub\\wwwroot\\Firmas\\AUTPDF");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }


                //Response.Redirect("~/Anexos/ConsentimientosConsultaExterna.aspx");
            }
            catch (Exception)
            {

                Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
            }
        }
    }
    protected void btnReview_Click(object sender, EventArgs e)
    {
        if (ValidarDatosHTML())
        {
            try
            {
                Session["CedulaAutorizado"] = txtIdAut.Text;
                string PDF = "";
                string cadenaEncriptada = Encrypt.GetMD5(DateTime.Now.ToString());
                //if (ValidarCampos())
                //{
                string html = retornarHtml();
                var htmlContent = String.Format(html);
                var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                pathDescarga = "../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf";


                htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf"));

                PDF = pathDescarga;


                string url = "VerPDF.aspx?PDF=" + PDF + "&Origen1=Busquedad";
                Response.Write("<script type='text/javascript'>window.open('" + url + "', 'window','resizable=no,location=1,status=1,scrollbars=1,width=1200,height=800,left=400,top=90');</script>");

                if (firma != "")
                {
                    btnGuardar.Visible = true;
                    btnCaptureFirm.Visible = false;
                }
                else
                {
                    btnCaptureFirm.Visible = true;
                }
                if (firmaMedico == "")
                {
                    btnCaptureFirmMedico.Visible = true;
                }
                else
                {
                    btnGuardar.Visible = true;
                    btnCaptureFirmMedico.Visible = false;
                }
                //}
            }
            catch (Exception)
            {

                Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
            }

        }
    }
    protected void Capture_Firma_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "captsig", "Capture();", true);
    }
    #endregion

    #region "FUNCIONES"
    public class Encrypt
    {
        public static string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
    }
    private void Consultar(string Id)
    {
        if ((CheckPaciente.Checked) | (CheckRepresentante.Checked))
        {
            if (Id == "")
            {
                divtable.Visible = false;
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
            }
            else
            {
                DataSet ds = new ClinicaCES.Logica.LBusquedaPacientes().BusquedaPaciente(Id);
                DataTable dtInforme = ds.Tables[0];
                DataTable dtPagina = Procedimientos.dtFiltrado("IDENTIFICACION", "", dtInforme);
                string[] campo = { "IDENTIFICACION" };
                if (dtInforme.Rows.Count > 0)
                {
                    DataRow row = dtInforme.Rows[0];
                    LblNombre.Text = row["PACIENTE"].ToString();
                    string Edad = row["EDAD"].ToString();
                    Session["EDAD"] = Edad;
                    divtable.Visible = true;

                }
                else
                {
                    divtable.Visible = false;
                    Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
                }
            }
        }
        else
        {
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
        }
    }
    private void GuardarPDF(string Cedula, string CedUser, string NameUser)
    {
        try
        {
            if (divAutorizado.Visible == true)
            {
                if ((txtIdAut.Text != "") && (txtNomAut.Text != ""))
                {
                    TratamientoDatosPDF();
                    ConsentimientosInformados oPDF = new ConsentimientosInformados();

                    string FilePath = Server.MapPath(pathDescarga);

                    WebClient User = new WebClient();

                    Byte[] FileBuffer = User.DownloadData(FilePath);

                    oPDF.Cedula = Cedula;
                    oPDF.NombrePaciente = LblNombre.Text;
                    oPDF.CedulaUsuario = CedUser;
                    oPDF.NombreUsuario = NameUser;
                    oPDF.FechaRegistro = Convert.ToDateTime(DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"));
                    oPDF.AprobacionMedico = CheckAprobacionMedico.Checked = false;
                    oPDF.BodyHtml = Session["BODY_HTML"].ToString();
                    oPDF.PDF_Tratamiento = TratamientoDatosPDF();


                    int startIndex = 10;
                    string NewpathDescarga = pathDescarga.Substring(startIndex).Replace(".pdf", "");

                    oPDF.Hash = NewpathDescarga;
                    oPDF.Consentimiento_PDF = FileBuffer;

                    if (new ClinicaCES.Logica.LConsentimientosInformados().InsertPDFDB(oPDF))
                    {
                        Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                    }
                }
                else
                {
                    Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                }
            }
            else
            {

                ConsentimientosInformados oPDF = new ConsentimientosInformados();

                string FilePath = Server.MapPath(pathDescarga);

                WebClient User = new WebClient();

                Byte[] FileBuffer = User.DownloadData(FilePath);

                oPDF.Cedula = Cedula;
                oPDF.NombrePaciente = LblNombre.Text;
                oPDF.CedulaUsuario = CedUser;
                oPDF.NombreUsuario = NameUser;
                oPDF.FechaRegistro = Convert.ToDateTime(DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"));
                oPDF.AprobacionMedico = CheckAprobacionMedico.Checked = false;
                oPDF.BodyHtml = Session["BODY_HTML"].ToString();
                oPDF.PDF_Tratamiento = TratamientoDatosPDF();

                int startIndex = 10;
                string NewpathDescarga = pathDescarga.Substring(startIndex).Replace(".pdf", "");

                oPDF.Hash = NewpathDescarga;
                oPDF.Consentimiento_PDF = FileBuffer;

                if (new ClinicaCES.Logica.LConsentimientosInformados().InsertPDFDB(oPDF))
                {
                    Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                }
                else
                {
                    Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                }
            }
        }
        catch (Exception)
        {

            Procedimientos.Script("mensajini", "Mensaje(3)", this.Page);
        }


    }
    public void drawimgFirm(string base64)
    {
        if ((base64 != ""))
        {
            if (Session["CedulaAutorizado"].ToString() != "")
            {
                dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(Session["CedulaAutorizado"].ToString());

                if (dtFirma.Rows.Count > 0)
                {
                    firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                    Id_Firma = Convert.ToInt16(dtFirma.Rows[0]["Id_Firma"]);

                    try
                    {
                        int startIndex = 7;
                        string Firma = base64.Substring(startIndex);

                        ConsentimientosInformados oFirma = new ConsentimientosInformados();

                        byte[] imagepath = Convert.FromBase64String(Firma);


                        oFirma.Cedula = Session["CedulaAutorizado"].ToString();
                        oFirma.UserFirmPath = imagepath;
                        oFirma.Id_Firma = Id_Firma;

                        if (new ClinicaCES.Logica.LConsentimientosInformados().UpdateFirmDB(oFirma))
                        {
                            Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        }

                        Session["CedulaAutorizado"] = null;
                    }
                    catch (Exception)
                    {

                        Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
                    }
                }
                else
                {
                    try
                    {
                        int startIndex = 7;
                        string Firma = base64.Substring(startIndex);

                        ConsentimientosInformados oFirma = new ConsentimientosInformados();

                        byte[] imagepath = Convert.FromBase64String(Firma);


                        oFirma.Cedula = Session["CedulaAutorizado"].ToString();
                        oFirma.UserFirmPath = imagepath;

                        if (new ClinicaCES.Logica.LConsentimientosInformados().InsertFirmDB(oFirma))
                        {
                            Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        }

                        Session["CedulaAutorizado"] = null;
                    }
                    catch (Exception)
                    {

                        Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
                    }
                }

            }
            else
            {
                dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(Session["Cedula"].ToString());

                if (dtFirma.Rows.Count > 0)
                {
                    firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                    Id_Firma = Convert.ToInt16(dtFirma.Rows[0]["Id_Firma"]);

                    try
                    {
                        int startIndex = 7;
                        string Firma = base64.Substring(startIndex);

                        ConsentimientosInformados oFirma = new ConsentimientosInformados();

                        byte[] imagepath = Convert.FromBase64String(Firma);


                        oFirma.Cedula = Session["Cedula"].ToString();
                        oFirma.UserFirmPath = imagepath;
                        oFirma.Id_Firma=Id_Firma;

                        if (new ClinicaCES.Logica.LConsentimientosInformados().UpdateFirmDB(oFirma))
                        {
                            Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        }

                        Session["CedulaAutorizado"] = null;
                    }
                    catch (Exception)
                    {

                        Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
                    }
                }
                else
                {
                    try
                    {
                        int startIndex = 7;
                        string Firma = base64.Substring(startIndex);

                        ConsentimientosInformados oFirma = new ConsentimientosInformados();

                        byte[] imagepath = Convert.FromBase64String(Firma);


                        oFirma.Cedula = Session["Cedula"].ToString();
                        oFirma.UserFirmPath = imagepath;

                        if (new ClinicaCES.Logica.LConsentimientosInformados().InsertFirmDB(oFirma))
                        {
                            Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        }

                        Session["CedulaAutorizado"] = null;
                    }
                    catch (Exception)
                    {

                        Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
                    }
                }
            }

        }

    }
    public void drawimgFirmMedico(string base64)
    {
        try
        {
            if ((base64 != ""))
            {
                int startIndex = 7;
                string Firma = base64.Substring(startIndex);

                ConsentimientosInformados oFirma = new ConsentimientosInformados();

                byte[] imagepath = Convert.FromBase64String(Firma);

                if (Session["Identificacion"].ToString() != "")
                {
                    oFirma.Cedula = Session["Identificacion"].ToString();
                }
                oFirma.UserFirmPath = imagepath;

                if (new ClinicaCES.Logica.LConsentimientosInformados().InsertFirmDB(oFirma))
                {
                    Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                }

            }
        }
        catch (Exception)
        {

            Procedimientos.Script("mensajini", "Mensaje(3)", this.Page);
        }
    }
    public void Limpiar()
    {
        txtTipoIntervencion.Text = "";
        LblNombre.Text = "";
        txtId.Text = "";
        txtIdAut.Text = "";
        txtNomAut.Text = "";
        divtable.Visible = false;
        CheckPaciente.Checked = false;
        CheckRepresentante.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsAnestesia.Checked = false;
        chkconsUrologianefrectomias.Checked = false;
        chkconsOrtopedia.Checked = false;
        chkconsDermatologia.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
    }
    protected void GuardarEvento(string ced)
    {
        ConsentimientosInformados oEvento = new ConsentimientosInformados();
        if (chkconsAnestesia.Checked)
        {   //string Autorizado = Session["AUTORIZACION"].ToString();
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-4";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugia.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-3";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugiaApendicectomia.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-56";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugiaColecistectomia.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-57";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugiaeventrorrafiasohernia.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-58";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugiaHerniorrafiainguinal.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-59";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugialaparoscopiadiagnostica.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-60";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugialaparotomiaexploratoria.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-61";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugialavadoperitonealterapeutico.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-62";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugiatoracostomia.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-70";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsOrtopedia.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-43";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsCirugiaortopedia.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-71";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsUrologianefrectomias.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-49";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsUrologianefrolitotomiapercutanea.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-50";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsUrologiaendoscopicosurologia.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-51";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsUrologiacirugiaprostata.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-52";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsDermatologia.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-34";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsNeurocirugiabloqueocolumna.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-53";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsNeurocirugiacirugiacolumna.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-54";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
        else if (chkconsNeurocirugiaresecciontumorcerebral.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-55";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
    }
    protected string retornarHtml()
    {
        string html = "", NEW_CUERPO_ANESTESIA = "", AUTORIZADO = "", REPRESENTACION = "", NEW_CUERPO_CIRUGIA_PROCEDIMIENTOS_MEDICOS = "", NEW_CUERPO_CIRUGIA_APENDICECTOMIA = "",
                          NEW_CUERPO_CIRUGIA_COLECISTECTOMIA = "", NEW_CUERPO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL = "", NEW_CUERPO_CIRUGIA_HERNIORRAFIA_INGUINAL = "", NEW_CUERPO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA = "",
                          NEW_CUERPO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA = "", NEW_CUERPO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO = "", NEW_CUERPO_CIRUGIA_TORACOSTOMIA = "", NEW_CIRUGIAS_DE_ORTOPEDIA = "",
                          NEW_CUERPO_PROCEDIMIENTOS_DERMATOLOGIA = "", NEW_CUERPO_PROCEDIMIENTOS_ORTOPEDIA = "",
                          NEW_CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS = "", NEW_CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA = "", NEW_CUERPO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA = "", NEW_CUERPO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA = "",
                          NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA = "", NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA ="", NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL="",
                          NEW_CUERPO_TRATAMIENTO_DATOS = "";
        /******************************************/

        #region "Datos"
        DataTable dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_CARGAR_TODOS();
        DataRow row = dt.Rows[0];

        #region "Cirugía"
        /********CONSENTIMIENTO INFORMADO PARA CIRUGÍA Y PROCEDIMIENTOS MEDICOS************************/
        string NOMBRE_CIRUGIA_PROCEDIMIENTOS_MEDICOS = dt.Rows[13]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIA_PROCEDIMIENTOS_MEDICOS = dt.Rows[13]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIA_PROCEDIMIENTOS_MEDICOS = dt.Rows[13]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIA_PROCEDIMIENTOS_MEDICOS = dt.Rows[13]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO INFORMADO PARA CIRUGÍA Y PROCEDIMIENTOS MEDICOS APENDICECTOMIA*****************/
        string NOMBRE_CIRUGIA_APENDICECTOMIA = dt.Rows[18]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIA_APENDICECTOMIA = dt.Rows[18]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIA_APENDICECTOMIA = dt.Rows[18]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIA_APENDICECTOMIA = dt.Rows[18]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO INFORMADO PARA CIRUGÍA Y PROCEDIMIENTOS MEDICOS COLECISTECTOMÍA***************/
        string NOMBRE_CIRUGIA_COLECISTECTOMÍA = dt.Rows[19]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIA_COLECISTECTOMÍA = dt.Rows[19]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIA_COLECISTECTOMÍA = dt.Rows[19]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIA_COLECISTECTOMÍA = dt.Rows[19]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO INFORMADO PARA CIRUGÍA Y PROCEDIMIENTOS MEDICOS EVENTRORRAFIAS_O_HERNIA_UMBILICAL****/
        string NOMBRE_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL = dt.Rows[20]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL = dt.Rows[20]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL = dt.Rows[20]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL = dt.Rows[20]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO INFORMADO PARA CIRUGÍA Y PROCEDIMIENTOS MEDICOS HERNIORRAFIA_INGUINAL*********/
        string NOMBRE_CIRUGIA_HERNIORRAFIA_INGUINAL = dt.Rows[21]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIA_HERNIORRAFIA_INGUINAL = dt.Rows[21]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIA_HERNIORRAFIA_INGUINAL = dt.Rows[21]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIA_HERNIORRAFIA_INGUINAL = dt.Rows[21]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO INFORMADO PARA CIRUGÍA Y PROCEDIMIENTOS MEDICOS LAPAROSCOPIA_DIAGNOSTICA******/
        string NOMBRE_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA = dt.Rows[22]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA = dt.Rows[22]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA = dt.Rows[22]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA = dt.Rows[22]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA LAPAROTOMÍA EXPLORATORIA*********************/
        string NOMBRE_CIRUGIA_LAPAROTOMIA_EXPLORATORIA = dt.Rows[23]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA = dt.Rows[23]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIA_LAPAROTOMIA_EXPLORATORIA = dt.Rows[23]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA = dt.Rows[23]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA LAVADO PERITONEAL TERAPÉUTICO*********************/
        string NOMBRE_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO = dt.Rows[24]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO = dt.Rows[24]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO = dt.Rows[24]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO = dt.Rows[24]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA TORACOSTOMIA*********************/
        string NOMBRE_CIRUGIA_TORACOSTOMIA = dt.Rows[25]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIA_TORACOSTOMIA = dt.Rows[25]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIA_TORACOSTOMIA = dt.Rows[25]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIA_TORACOSTOMIA = dt.Rows[25]["CUERPO_CONSENTIMIENTO"].ToString();
        #endregion

        #region "Ortopedia"
        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE ORTOPEDIA*************/
        string NOMBRE_PROCEDIMIENTOS_ORTOPEDIA = dt.Rows[14]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTOS_ORTOPEDIA = dt.Rows[14]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_PROCEDIMIENTOS_ORTOPEDIA = dt.Rows[14]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_PROCEDIMIENTOS_ORTOPEDIA = dt.Rows[14]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA CIRUGÍAS DE ORTOPEDIA*************/
        string NOMBRE_CIRUGIAS_DE_ORTOPEDIA = dt.Rows[26]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CIRUGIAS_DE_ORTOPEDIA = dt.Rows[26]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CIRUGIAS_DE_ORTOPEDIA = dt.Rows[26]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CIRUGIAS_DE_ORTOPEDIA = dt.Rows[26]["CUERPO_CONSENTIMIENTO"].ToString();
        #endregion

        #region "Anestesia"
        /********CONSENTIMIENTO INFORMADO DE ANESTESIA****************************************/
        string NOMBRE_ANESTESIA = dt.Rows[0]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_ANESTESIA = dt.Rows[0]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_ANESTESIA"] = CODIGO_ANESTESIA;
        int VERSION_ANESTESIA = Convert.ToInt16(dt.Rows[0]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_ANESTESIA = dt.Rows[0]["CUERPO_CONSENTIMIENTO"].ToString();
        #endregion

        #region "Urología"
        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE UROLOGÍA NEFRECTOMIAS *************/
        string NOMBRE_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS = dt.Rows[15]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS = dt.Rows[15]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS = dt.Rows[15]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS = dt.Rows[15]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE UROLOGÍA NEFROLITOTOMIA_PERCUTANEA*************/
        string NOMBRE_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA = dt.Rows[27]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA = dt.Rows[27]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA = dt.Rows[27]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA = dt.Rows[27]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE UROLOGÍA ENDOSCOPICOS_DE_UROLOGIA*************/
        string NOMBRE_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA = dt.Rows[28]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA = dt.Rows[28]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA = dt.Rows[28]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA = dt.Rows[28]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE CIRUGÍAS DE PRÓSTATA*************/
        string NOMBRE_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA = dt.Rows[29]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA = dt.Rows[29]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA = dt.Rows[29]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA = dt.Rows[29]["CUERPO_CONSENTIMIENTO"].ToString();
        #endregion

        #region "Dermatología"
        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE DERMATOLOGÍA*************/
        string NOMBRE_PROCEDIMIENTOS_DERMATOLOGIA = dt.Rows[16]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTOS_DERMATOLOGIA = dt.Rows[16]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_PROCEDIMIENTOS_DERMATOLOGIA = dt.Rows[16]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_PROCEDIMIENTOS_DERMATOLOGIA = dt.Rows[16]["CUERPO_CONSENTIMIENTO"].ToString();
        #endregion

        #region "Neurocirugia"
        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS NEUROCIRUGÍA_BLOQUEO_DE_COLUMNA*************/
        string NOMBRE_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA = dt.Rows[30]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA = dt.Rows[30]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA = dt.Rows[30]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA = dt.Rows[30]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS NEUROCIRUGÍA_BLOQUEO_DE_COLUMNA*************/
        string NOMBRE_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA = dt.Rows[31]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA = dt.Rows[31]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA = dt.Rows[31]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA = dt.Rows[31]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS NEUROCIRUGÍA_RESECCIÓN DE TUMOR CEREBRAL*************/
        string NOMBRE_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL = dt.Rows[32]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL = dt.Rows[32]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL = dt.Rows[32]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL = dt.Rows[32]["CUERPO_CONSENTIMIENTO"].ToString();
        #endregion

        #region "Varios"

        /********AUTORIZACIÓN DE REPRESENTACIÓN***********************************************/
        string NOMBRE_REPRESENTACION = dt.Rows[1]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_REPRESENTACION = dt.Rows[1]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_REPRESENTACION"] = CODIGO_REPRESENTACION;
        int VERSION_REPRESENTACION = Convert.ToInt16(dt.Rows[1]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_REPRESENTACION = dt.Rows[1]["CUERPO_CONSENTIMIENTO"].ToString();


        /********CALIDAD EN QUE SE OTORGAN LOS CONSENTIMIENTOS COMO PACIENTE***********/
        string CUERPO_CONSENTIMIENTOS_PACIENTE = dt.Rows[8]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CALIDAD EN QUE SE OTORGAN LOS CONSENTIMIENTOS COMO RESPONSABLE***********/
        string CUERPO_CONSENTIMIENTOS_RESPONSABLE = dt.Rows[2]["CUERPO_CONSENTIMIENTO"].ToString();

        /********TRATAMIENTO DE DATOS PERSONALES***********/
        string CUERPO_DATOS_PERSONALES = dt.Rows[3]["CUERPO_CONSENTIMIENTO"].ToString();

        /********FIRMA AUTORIZADO*********************************************************/
        string CUERPO_FIRMA_AUTORIZADO = dt.Rows[10]["CUERPO_CONSENTIMIENTO"].ToString();

        /********AUTORIZACIÓN PARA EL TRATAMIENTO DE DATOS PERSONALES***********/
        string NOMBRE_TRATAMIENTO_DATOS = dt.Rows[7]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_TRATAMIENTO_DATOS = dt.Rows[7]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_TRATAMIENTO_DATOS"] = CODIGO_TRATAMIENTO_DATOS;
        int VERSION_TRATAMIENTO_DATOS = Convert.ToInt16(dt.Rows[7]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_TRATAMIENTO_DATOS = dt.Rows[7]["CUERPO_CONSENTIMIENTO"].ToString();

        #endregion

        #endregion



        try
        {

            string path = Server.MapPath("../img/CES.png");
            string NombreAutorizado = "";
            NombreAutorizado = LblNombre.Text;

            /****************ENCABEZADO*********************************/
            //ENCABEZADO = CUERPO_ENCABEZADO
            //                   .Replace("lblPaciente", LblNombre.Text)
            //                   .Replace("lblDocumento", txtId.Text);
            /****************AUTORIZACIÓN REPRESENTANTE*******************************/
            if (CheckRepresentante.Checked)
            {
                if (ValidarCamposAut())
                {
                    dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(txtIdAut.Text);

                    if (dtFirma.Rows.Count > 0)
                    {
                        CheckUtilizarFirmaBD.Visible = true;
                        firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                            
                    }
                    else
                    {
                        firma = "";
                        btnCaptureFirm.Visible = true;

                    }

                    NombreAutorizado = txtNomAut.Text;
                    AUTORIZADO = CUERPO_CONSENTIMIENTOS_RESPONSABLE;
                    REPRESENTACION = CUERPO_REPRESENTACION
                                           .Replace("NombreConsentimientoAutorizacion", NOMBRE_REPRESENTACION)
                                           .Replace("CodigoConsentimientoAutorizacion", CODIGO_REPRESENTACION)
                                           .Replace("VersionadoAutorizacion", VERSION_REPRESENTACION.ToString())
                                           .Replace("txtNomAut", txtNomAut.Text)
                                           .Replace("txtIdAut", txtIdAut.Text);
                }
            }
            else
            {
                dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(txtId.Text);
                if (dtFirma.Rows.Count > 0)
                {
                    CheckUtilizarFirmaBD.Visible = true;
                    firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firma = "";
                    btnCaptureFirm.Visible = true;
                }
                AUTORIZADO = CUERPO_CONSENTIMIENTOS_PACIENTE;
                REPRESENTACION = "";
            }


            #region "Anestesia"
            /****************ANESTESIA**********************************/
            if (chkconsAnestesia.Checked)
            {
                    #region "Tipos anestesia"
                    string Local = "", General = "", Conductiva = "", Regional = "", cons = "", dis = "";

                    if (CheckAnestesiaGeneral.Checked)
                    {
                        General = "__X__";
                    }
                    else
                    {
                        General = "";
                    }
                    if (CheckAnestesiaConductiva.Checked)
                    {
                        Conductiva = "__X__";
                    }
                    else
                    {
                        Conductiva = "";
                    }
                    if (CheckAnestesiaLocal.Checked)
                    {
                        Local = "__X__";
                    }
                    else
                    {
                        Local = "";
                    }
                    if (CheckAnestesiaRegional.Checked)
                    {
                        Regional = "__X__";
                    }
                    else
                    {
                        Regional = "";
                    }
                    #endregion
                    string fMedico = Session["Identificacion"].ToString();
                    dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                    if (dtFirmaMedico.Rows.Count > 0)
                    {
                        firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                    }
                    else
                    {
                        firmaMedico = "";
                    }
                    if (CheckDisentimiento.Checked)
                    {
                        dis = "__X__";
                    }
                    if (CheckConsentimiento.Checked)
                    {
                        cons = "__X__";
                    }
                    if (CheckPaciente.Checked)
                    {
                        NEW_CUERPO_ANESTESIA = CUERPO_ANESTESIA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_ANESTESIA)
                                   .Replace("CodigoConsentimiento", CODIGO_ANESTESIA)
                                   .Replace("Versionado", VERSION_ANESTESIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)

                                   .Replace("lblDoctor", Session["Nombre"].ToString())

                                   .Replace("lblGeneral", General)
                                   .Replace("lblConductiva", Conductiva)
                                   .Replace("lblLocal", Local)
                                   .Replace("lblRegional", Regional)


                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                    }
                    else
                    {
                        NEW_CUERPO_ANESTESIA = CUERPO_ANESTESIA
                                       .Replace("lblPaciente", txtNomAut.Text)
                                       .Replace("lblDocumento", txtIdAut.Text)
                                       .Replace("NombreConsentimiento", NOMBRE_ANESTESIA)
                                       .Replace("CodigoConsentimiento", CODIGO_ANESTESIA)
                                       .Replace("Versionado", VERSION_ANESTESIA.ToString())
                                       .Replace("lblcons", cons)
                                       .Replace("lbldisc", dis)

                                       .Replace("lblDoctor", Session["Nombre"].ToString())

                                       .Replace("lblGeneral", General)
                                       .Replace("lblConductiva", Conductiva)
                                       .Replace("lblLocal", Local)
                                       .Replace("lblRegional", Regional)

                                       .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                       .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                       .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                       .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                       .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                       .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                       .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                       .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                       .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                       .Replace("firmaPaciente", firma)
                                       .Replace("firmaMedico", firmaMedico);
                    }
            }
            #endregion

            #region "Cirugia"
            /****************CONSENTIMIENTO INFORMADO PARA CIRUGÍA Y PROCEDIMIENTOS MEDICOS***************************/
            if (chkconsCirugia.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_CIRUGIA_PROCEDIMIENTOS_MEDICOS = CUERPO_CIRUGIA_PROCEDIMIENTOS_MEDICOS
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_PROCEDIMIENTOS_MEDICOS)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_PROCEDIMIENTOS_MEDICOS)
                                   .Replace("Versionado", VERSION_CIRUGIA_PROCEDIMIENTOS_MEDICOS.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_CIRUGIA_PROCEDIMIENTOS_MEDICOS = CUERPO_CIRUGIA_PROCEDIMIENTOS_MEDICOS
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_PROCEDIMIENTOS_MEDICOS)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_PROCEDIMIENTOS_MEDICOS)
                                   .Replace("Versionado", VERSION_CIRUGIA_PROCEDIMIENTOS_MEDICOS.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            /****************CONSENTIMIENTO INFORMADO PARA CIRUGÍA APENDICECTOMIA***************************/
            if (chkconsCirugiaApendicectomia.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_CIRUGIA_APENDICECTOMIA = CUERPO_CIRUGIA_APENDICECTOMIA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_APENDICECTOMIA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_APENDICECTOMIA)
                                   .Replace("Versionado", VERSION_CIRUGIA_APENDICECTOMIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Apendicectomía")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_CIRUGIA_APENDICECTOMIA = CUERPO_CIRUGIA_APENDICECTOMIA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_APENDICECTOMIA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_APENDICECTOMIA)
                                   .Replace("Versionado", VERSION_CIRUGIA_APENDICECTOMIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Apendicectomía")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            /****************CONSENTIMIENTO INFORMADO PARA CIRUGÍA COLECISTECTOMÍA***************************/
            if (chkconsCirugiaColecistectomia.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_CIRUGIA_COLECISTECTOMIA = CUERPO_CIRUGIA_COLECISTECTOMÍA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_COLECISTECTOMÍA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_COLECISTECTOMÍA)
                                   .Replace("Versionado", VERSION_CIRUGIA_COLECISTECTOMÍA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Colecistectomía")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_CIRUGIA_COLECISTECTOMIA = CUERPO_CIRUGIA_COLECISTECTOMÍA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_COLECISTECTOMÍA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_COLECISTECTOMÍA)
                                   .Replace("Versionado", VERSION_CIRUGIA_COLECISTECTOMÍA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Colecistectomía")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            /****************CONSENTIMIENTO INFORMADO PARA CIRUGÍA EVENTRORRAFIAS_O_HERNIA_UMBILICAL***************************/
            if (chkconsCirugiaeventrorrafiasohernia.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "", eventrorrafias = "", hernia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                /*********************************/
                if (Checkeventrorrafias.Checked)
                {
                    eventrorrafias = "__X__";
                }
                else
                {
                    eventrorrafias = "";
                }
                if (Checkhernia.Checked)
                {
                    hernia = "__X__";
                }
                else
                {
                    hernia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL = CUERPO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL)
                                   .Replace("Versionado", VERSION_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lblHERNIORRAFIA", eventrorrafias)
                                   .Replace("lblEVENTRORRAFIA", hernia)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL = CUERPO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL)
                                   .Replace("Versionado", VERSION_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            /****************CONSENTIMIENTO INFORMADO PARA CIRUGÍA HERNIORRAFIA INGUINAL***************************/
            if (chkconsCirugiaHerniorrafiainguinal.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }

                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_CIRUGIA_HERNIORRAFIA_INGUINAL = CUERPO_CIRUGIA_HERNIORRAFIA_INGUINAL
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_HERNIORRAFIA_INGUINAL)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_HERNIORRAFIA_INGUINAL)
                                   .Replace("Versionado", VERSION_CIRUGIA_HERNIORRAFIA_INGUINAL.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Herniorrafía Inguinal")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_CIRUGIA_HERNIORRAFIA_INGUINAL = CUERPO_CIRUGIA_HERNIORRAFIA_INGUINAL
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_HERNIORRAFIA_INGUINAL)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_HERNIORRAFIA_INGUINAL)
                                   .Replace("Versionado", VERSION_CIRUGIA_HERNIORRAFIA_INGUINAL.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Herniorrafía Inguinal")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            /****************CONSENTIMIENTO INFORMADO PARA CIRUGÍA LAPAROSCOPIA_DIAGNOSTICA***************************/
            if (chkconsCirugialaparoscopiadiagnostica.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }

                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA = CUERPO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA)
                                   .Replace("Versionado", VERSION_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Laparoscopia diagnóstica")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA = CUERPO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA)
                                   .Replace("Versionado", VERSION_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Laparoscopia diagnóstica")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            /****************CONSENTIMIENTO INFORMADO PARA CIRUGÍA LAPAROTOMIA_EXPLORATORIA***************************/
            if (chkconsCirugialaparotomiaexploratoria.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }

                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA = CUERPO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_LAPAROTOMIA_EXPLORATORIA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA)
                                   .Replace("Versionado", VERSION_CIRUGIA_LAPAROTOMIA_EXPLORATORIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Laparoscopia exploratoria")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA = CUERPO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_LAPAROTOMIA_EXPLORATORIA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA)
                                   .Replace("Versionado", VERSION_CIRUGIA_LAPAROTOMIA_EXPLORATORIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Laparoscopia exploratoria")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            /****************CONSENTIMIENTO PARA CIRUGÍA LAVADO PERITONEAL TERAPÉUTICO*********************/
            if (chkconsCirugialavadoperitonealterapeutico.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }

                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO = CUERPO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO)
                                   .Replace("Versionado", VERSION_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Lavado peritoneal terapéutico")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO = CUERPO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO)
                                   .Replace("Versionado", VERSION_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Lavado peritoneal terapéutico")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            /****************CONSENTIMIENTO PARA CIRUGÍA TORACOSTOMIA*********************/
            if (chkconsCirugiatoracostomia.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }

                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_CIRUGIA_TORACOSTOMIA = CUERPO_CIRUGIA_TORACOSTOMIA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_TORACOSTOMIA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_TORACOSTOMIA)
                                   .Replace("Versionado", VERSION_CIRUGIA_TORACOSTOMIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Toracostomia")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_CIRUGIA_TORACOSTOMIA = CUERPO_CIRUGIA_TORACOSTOMIA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIA_TORACOSTOMIA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIA_TORACOSTOMIA)
                                   .Replace("Versionado", VERSION_CIRUGIA_TORACOSTOMIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "Toracostomia")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            #endregion

            #region "Ortopedia"
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE ORTOPEDIA****************/
            if (chkconsOrtopedia.Checked)
            {
                string Infiltracion = "", CambioYeso = "", Onicectomia = "", cons = "", dis = "", Diagnostico = "", Patologia = ""; ;

                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /************************************/
                if (CheckInfiltracion.Checked)
                {
                    Infiltracion = "__X__";
                }
                else
                {
                    Infiltracion = "";
                }
                if (CheckCambioYeso.Checked)
                {
                    CambioYeso = "__X__";
                }
                else
                {
                    CambioYeso = "";
                }
                if (CheckOnicectomia.Checked)
                {
                    Onicectomia = "__X__";
                }
                else
                {
                    Onicectomia = "";
                }
                /**********************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }

                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_PROCEDIMIENTOS_ORTOPEDIA = CUERPO_PROCEDIMIENTOS_ORTOPEDIA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_ORTOPEDIA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_ORTOPEDIA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_ORTOPEDIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lblinfiltracion", Infiltracion)
                                   .Replace("lblcambioYeso", CambioYeso)
                                   .Replace("lblOnicectomia", Onicectomia)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_PROCEDIMIENTOS_ORTOPEDIA = CUERPO_PROCEDIMIENTOS_ORTOPEDIA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_ORTOPEDIA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_ORTOPEDIA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_ORTOPEDIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lblinfiltracion", Infiltracion)
                                   .Replace("lblcambioYeso", CambioYeso)
                                   .Replace("lblOnicectomia", Onicectomia)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }

            }
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA CIRUGÍAS DE ORTOPEDIA****************/
            if (chkconsCirugiaortopedia.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CIRUGIAS_DE_ORTOPEDIA = CUERPO_CIRUGIAS_DE_ORTOPEDIA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIAS_DE_ORTOPEDIA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIAS_DE_ORTOPEDIA)
                                   .Replace("Versionado", VERSION_CIRUGIAS_DE_ORTOPEDIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CIRUGIAS_DE_ORTOPEDIA = CUERPO_CIRUGIAS_DE_ORTOPEDIA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_CIRUGIAS_DE_ORTOPEDIA)
                                   .Replace("CodigoConsentimiento", CODIGO_CIRUGIAS_DE_ORTOPEDIA)
                                   .Replace("Versionado", VERSION_CIRUGIAS_DE_ORTOPEDIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }

            #endregion

            #region "Urología"
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE UROLOGIA_NEFRECTOMIAS****************/
            if (chkconsUrologianefrectomias.Checked)
            {
                string NefrectomiaParcialAbierta = "", NefrectomiaSimpleLaparoscopica = "", NefrectomiaParcialLaparoscopica = "", NefrectomiaRadicalAbierta = "", NefrectomiaSimpleAbierta = "", NefrectomiaRadicalLaparoscopica = "", cons = "", dis = "", Diagnostico = "", Patologia = "";

                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /****************************/
                if (CheckNefrectomiaParcialAbierta.Checked)
                {
                    NefrectomiaParcialAbierta = "__X__";
                }
                else
                {
                    NefrectomiaParcialAbierta = "";
                }
                if (CheckNefrectomiaSimpleLaparoscopica.Checked)
                {
                    NefrectomiaSimpleLaparoscopica = "__X__";
                }
                else
                {
                    NefrectomiaSimpleLaparoscopica = "";
                }
                if (CheckNefrectomiaParcialLaparoscopica.Checked)
                {
                    NefrectomiaParcialLaparoscopica = "__X__";
                }
                else
                {
                    NefrectomiaParcialLaparoscopica = "";
                }
                if (CheckNefrectomiaRadicalAbierta.Checked)
                {
                    NefrectomiaRadicalAbierta = "__X__";
                }
                else
                {
                    NefrectomiaRadicalAbierta = "";
                }
                if (CheckNefrectomiaSimpleAbierta.Checked)
                {
                    NefrectomiaSimpleAbierta = "__X__";
                }
                else
                {
                    NefrectomiaSimpleAbierta = "";
                }
                if (CheckNefrectomiaRadicalLaparoscopica.Checked)
                {
                    NefrectomiaRadicalLaparoscopica = "__X__";
                }
                else
                {
                    NefrectomiaRadicalLaparoscopica = "";
                }
                /*********************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS = CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblabierta", NefrectomiaParcialAbierta)
                                   .Replace("lbllaparoscopica", NefrectomiaSimpleLaparoscopica)
                                   .Replace("lbllaparcial", NefrectomiaParcialLaparoscopica)
                                   .Replace("lblraabierta", NefrectomiaRadicalAbierta)
                                   .Replace("lblsimpleabierta", NefrectomiaSimpleAbierta)
                                   .Replace("lblralaparos", NefrectomiaRadicalLaparoscopica)

                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS = CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblabierta", NefrectomiaParcialAbierta)
                                   .Replace("lbllaparoscopica", NefrectomiaSimpleLaparoscopica)
                                   .Replace("lbllaparcial", NefrectomiaParcialLaparoscopica)
                                   .Replace("lblraabierta", NefrectomiaRadicalAbierta)
                                   .Replace("lblsimpleabierta", NefrectomiaSimpleAbierta)
                                   .Replace("lblralaparos", NefrectomiaRadicalLaparoscopica)

                                   .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }

            }
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE UROLOGIA_NEFROLITOTOMIA_PERCUTANEA****************/
            if (chkconsUrologianefrolitotomiapercutanea.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA = CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA = CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE UROLOGIA_ENDOSCOPICOS_DE_UROLOGIA****************/
            if (chkconsUrologiaendoscopicosurologia.Checked)
            {
                string NefroUreterolitotomiaendoscopica = "", Ureteropielorrenoscopia = "", Cistoscopia = "", Endopielotomia = "", Lesionvesical = "", cons = "", dis = "", Diagnostico = "", Patologia = "";

                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /****************************/
                if (CheckNefroUreterolitotomiaendoscopica.Checked)
                {
                    NefroUreterolitotomiaendoscopica = "__X__";
                }
                else
                {
                    NefroUreterolitotomiaendoscopica = "";
                }
                if (CheckUreteropielorrenoscopia.Checked)
                {
                    Ureteropielorrenoscopia = "__X__";
                }
                else
                {
                    Ureteropielorrenoscopia = "";
                }
                if (CheckCistoscopia.Checked)
                {
                    Cistoscopia = "__X__";
                }
                else
                {
                    Cistoscopia = "";
                }
                if (CheckEndopielotomia.Checked)
                {
                    Endopielotomia = "__X__";
                }
                else
                {
                    Endopielotomia = "";
                }
                if (CheckLesionvesical.Checked)
                {
                    Lesionvesical = "__X__";
                }
                else
                {
                    Lesionvesical = "";
                }
                /*********************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA = CUERPO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblnefro", NefroUreterolitotomiaendoscopica)
                                   .Replace("lblreseccion", Lesionvesical)
                                   .Replace("lblurete", Ureteropielorrenoscopia)
                                   .Replace("lblendo", Endopielotomia)
                                   .Replace("lblcitoscopia", Cistoscopia)

                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA = CUERPO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblnefro", NefroUreterolitotomiaendoscopica)
                                   .Replace("lblreseccion", Lesionvesical)
                                   .Replace("lblurete", Ureteropielorrenoscopia)
                                   .Replace("lblendo", Endopielotomia)
                                   .Replace("lblcitoscopia", Cistoscopia)

                                   .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }

            }
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE UROLOGIA_CIRUGÍAS DE PRÓSTATA****************/
            if (chkconsUrologiacirugiaprostata.Checked)
            {
                string prostataradical = "", prostatasimple = "", laparoscopicaprostata = "", transuretalprostata = "", cons = "", dis = "", Diagnostico = "", Patologia = "";

                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /****************************/
                if (Checkprostataradical.Checked)
                {
                    prostataradical = "__X__";
                }
                else
                {
                    prostataradical = "";
                }
                if (Checkprostatasimple.Checked)
                {
                    prostatasimple = "__X__";
                }
                else
                {
                    prostatasimple = "";
                }
                if (Checklaparoscopicaprostata.Checked)
                {
                    laparoscopicaprostata = "__X__";
                }
                else
                {
                    laparoscopicaprostata = "";
                }
                if (Checktransuretalprostata.Checked)
                {
                    transuretalprostata = "__X__";
                }
                else
                {
                    transuretalprostata = "";
                }
                /*********************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA = CUERPO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblradical", prostataradical)
                                   .Replace("lbllaparoscopica", laparoscopicaprostata)
                                   .Replace("lblsimple", prostatasimple)
                                   .Replace("lblreseccion", transuretalprostata)

                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA = CUERPO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblradical", prostataradical)
                                   .Replace("lbllaparoscopica", laparoscopicaprostata)
                                   .Replace("lblsimple", prostatasimple)
                                   .Replace("lblreseccion", transuretalprostata)


                                   .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }

            }
            #endregion

            #region "Dermatología"
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS DE UROLOGÍA****************/
            if (chkconsDermatologia.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";

                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /*********************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_PROCEDIMIENTOS_DERMATOLOGIA = CUERPO_PROCEDIMIENTOS_DERMATOLOGIA
                               .Replace("lblPaciente", LblNombre.Text)
                               .Replace("lblDocumento", txtId.Text)
                               .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_DERMATOLOGIA)
                               .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_DERMATOLOGIA)
                               .Replace("Versionado", VERSION_PROCEDIMIENTOS_DERMATOLOGIA.ToString())
                               .Replace("lblcons", cons)
                               .Replace("lbldisc", dis)
                               .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                               .Replace("lblDiagnostico", Diagnostico)
                               .Replace("lblPatologia", Patologia)
                               .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                               .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                               .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                               .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                               .Replace("lblNombreMedico", Session["Nombre"].ToString())
                               .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                               .Replace("lblRegistroMedico", Session["Registro"].ToString())
                               .Replace("firmaPaciente", firma)
                               .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_PROCEDIMIENTOS_DERMATOLOGIA = CUERPO_PROCEDIMIENTOS_DERMATOLOGIA
                               .Replace("lblPaciente", txtNomAut.Text)
                               .Replace("lblDocumento", txtIdAut.Text)
                               .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_DERMATOLOGIA)
                               .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_DERMATOLOGIA)
                               .Replace("Versionado", VERSION_PROCEDIMIENTOS_DERMATOLOGIA.ToString())
                               .Replace("lblcons", cons)
                               .Replace("lbldisc", dis)
                               .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                               .Replace("lblDiagnostico", Diagnostico)
                               .Replace("lblPatologia", Patologia)
                               .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                               .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                               .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                               .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                               .Replace("lblNombreMedico", Session["Nombre"].ToString())
                               .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                               .Replace("lblRegistroMedico", Session["Registro"].ToString())
                               .Replace("firmaPaciente", firma)
                               .Replace("firmaMedico", firmaMedico);
                }
            }
            #endregion

            #region "Neurocirugia"
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS BLOQUEO DE COLUMNA****************/
            if (chkconsNeurocirugiabloqueocolumna.Checked)
            {
                string colmnacervical = "", columnadorsal = "", columnalumbar = "", columnasacra = "", cons = "", dis = "", Diagnostico = "", Patologia = "";

                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /****************************/
                if (Checkcolmnacervical.Checked)
                {
                    colmnacervical = "__X__";
                }
                else
                {
                    colmnacervical = "";
                }
                if (Checkcolumnadorsal.Checked)
                {
                    columnadorsal = "__X__";
                }
                else
                {
                    columnadorsal = "";
                }
                if (Checkcolumnalumbar.Checked)
                {
                    columnalumbar = "__X__";
                }
                else
                {
                    columnalumbar = "";
                }
                if (Checkcolumnasacra.Checked)
                {
                    columnasacra = "__X__";
                }
                else
                {
                    columnasacra = "";
                }
                /*********************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA = CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblcervical", colmnacervical)
                                   .Replace("lbldorsal", columnadorsal)
                                   .Replace("lbllumbar", columnalumbar)
                                   .Replace("lblsacra", columnasacra)


                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA = CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lblcervical", colmnacervical)
                                   .Replace("lbldorsal", columnadorsal)
                                   .Replace("lbllumbar", columnalumbar)
                                   .Replace("lblsacra", columnasacra)


                                   .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }

            }
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA PROCEDIMIENTOS CIRUGÍA DE COLUMNA****************/
            if (chkconsNeurocirugiacirugiacolumna.Checked)
            {
                string laminectomiacervical = "", microdiscectomialumbar = "", laminectomiatoracica = "", artrodesiscolumnacervical = "", laminectomialumbar = "", artrodesiscolumnatoracica = "", microdiscectomia = "", artrodesiscolumnalumbar = "", microdiscectomiatoracica = "",
                       cons = "", dis = "", Diagnostico = "", Patologia = "";

                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /****************************/
                if (Checklaminectomiacervical.Checked)
                {
                    laminectomiacervical = "__X__";
                }
                else
                {
                    laminectomiacervical = "";
                }
                if (Checkmicrodiscectomialumbar.Checked)
                {
                    microdiscectomialumbar = "__X__";
                }
                else
                {
                    microdiscectomialumbar = "";
                }
                if (Checklaminectomiatoracica.Checked)
                {
                    laminectomiatoracica = "__X__";
                }
                else
                {
                    laminectomiatoracica = "";
                }
                if (Checkartrodesiscolumnacervical.Checked)
                {
                    artrodesiscolumnacervical = "__X__";
                }
                else
                {
                    artrodesiscolumnacervical = "";
                }
                if (Checklaminectomialumbar.Checked)
                {
                    laminectomialumbar = "__X__";
                }
                else
                {
                    laminectomialumbar = "";
                }
                if (Checkartrodesiscolumnatoracica.Checked)
                {
                    artrodesiscolumnatoracica = "__X__";
                }
                else
                {
                    artrodesiscolumnatoracica = "";
                }
                if (Checkmicrodiscectomia.Checked)
                {
                    microdiscectomia = "__X__";
                }
                else
                {
                    microdiscectomia = "";
                }
                if (Checkartrodesiscolumnalumbar.Checked)
                {
                    artrodesiscolumnalumbar = "__X__";
                }
                else
                {
                    artrodesiscolumnalumbar = "";
                }
                if (Checkmicrodiscectomiatoracica.Checked)
                {
                    microdiscectomiatoracica = "__X__";
                }
                else
                {
                    microdiscectomiatoracica = "";
                }
                /*********************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA = CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lbllc", laminectomiacervical)
                                   .Replace("lblml", microdiscectomialumbar)
                                   .Replace("lbllt", laminectomiatoracica)
                                   .Replace("lblacc", artrodesiscolumnacervical)
                                   .Replace("lblll", laminectomialumbar)
                                   .Replace("lblact", artrodesiscolumnatoracica)
                                   .Replace("lblmc", microdiscectomia)
                                   .Replace("lblarcl", artrodesiscolumnalumbar)
                                   .Replace("lblmt", microdiscectomiatoracica)


                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA = CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)

                                   .Replace("lbllc", laminectomiacervical)
                                   .Replace("lblml", microdiscectomialumbar)
                                   .Replace("lbllt", laminectomiatoracica)
                                   .Replace("lblacc", artrodesiscolumnacervical)
                                   .Replace("lblll", laminectomialumbar)
                                   .Replace("lblact", artrodesiscolumnatoracica)
                                   .Replace("lblmc", microdiscectomia)
                                   .Replace("lblarcl", artrodesiscolumnalumbar)
                                   .Replace("lblmt", microdiscectomiatoracica)


                                   .Replace("lbltipoIntervencion", txtTipoIntervencion.Text)
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }

            }
            /****************CONSENTIMIENTO O DISENTIMIENTO INFORMADO PARA CIRUGÍA Y PROCEDIMIENTOS RESECCIÓN DE TUMOR CEREBRAL*****/
            if (chkconsNeurocirugiaresecciontumorcerebral.Checked)
            {
                string cons = "", dis = "", Diagnostico = "", Patologia = "";
                string fMedico = Session["Identificacion"].ToString();
                dtFirmaMedico = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(fMedico);

                if (dtFirmaMedico.Rows.Count > 0)
                {
                    firmaMedico = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirmaMedico.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firmaMedico = "";
                }
                /*******************************/
                if (CheckDisentimiento.Checked)
                {
                    dis = "__X__";
                }
                if (CheckConsentimiento.Checked)
                {
                    cons = "__X__";
                }
                /******************************/
                if (CheckDiagnostico.Checked)
                {
                    Diagnostico = "__X__";
                }
                else
                {
                    Diagnostico = "";
                }
                if (CheckPatologia.Checked)
                {
                    Patologia = "__X__";
                }
                else
                {
                    Patologia = "";
                }
                if (CheckPaciente.Checked)
                {
                    NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL = CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL
                                   .Replace("lblPaciente", LblNombre.Text)
                                   .Replace("lblDocumento", txtId.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "RESECCIÓN DE TUMOR CEREBRAL")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "")
                                   .Replace("lblCePa", "")
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
                else
                {
                    NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL = CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL
                                   .Replace("lblPaciente", txtNomAut.Text)
                                   .Replace("lblDocumento", txtIdAut.Text)
                                   .Replace("NombreConsentimiento", NOMBRE_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL)
                                   .Replace("CodigoConsentimiento", CODIGO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL)
                                   .Replace("Versionado", VERSION_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL.ToString())
                                   .Replace("lblcons", cons)
                                   .Replace("lbldisc", dis)
                                   .Replace("lblDiagnostico", Diagnostico)
                                   .Replace("lblPatologia", Patologia)
                                   .Replace("lbltipoIntervencion", "RESECCIÓN DE TUMOR CEREBRAL")
                                   .Replace("lblAurorizacionRepresentacion", REPRESENTACION)
                                   .Replace("lblCalidadEnLaQueSeOtorgaElConsentimiento", AUTORIZADO)
                                   .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                                   .Replace("lblOtrasConsideraciones", txtConsideraciones.Text)
                                   .Replace("lblRePa", "Nombre del paciente:" + " " + LblNombre.Text)
                                   .Replace("lblCePa", "con número de identificación" + " " + txtId.Text)
                                   .Replace("lblNombreMedico", Session["Nombre"].ToString())
                                   .Replace("lblCedulaMedico", Session["Identificacion"].ToString())
                                   .Replace("lblRegistroMedico", Session["Registro"].ToString())
                                   .Replace("firmaPaciente", firma)
                                   .Replace("firmaMedico", firmaMedico);
                }
            }
            #endregion


            /****************TRATAMIENTO DE DATOS*********************/
            NEW_CUERPO_TRATAMIENTO_DATOS = CUERPO_TRATAMIENTO_DATOS
                        .Replace("NombreConsentimientoTD", NOMBRE_TRATAMIENTO_DATOS)
                        .Replace("CodigoConsentimientoTD", CODIGO_TRATAMIENTO_DATOS)
                        .Replace("VersionadoTD", VERSION_TRATAMIENTO_DATOS.ToString());

            /**********************ANESTESIA***********************************/
            if (chkconsAnestesia.Checked)
            {
                html = NEW_CUERPO_ANESTESIA + CUERPO_DATOS_PERSONALES;
            }
            /***************************CIRUGIA*****************************************************/
            else if (chkconsCirugia.Checked)
            {
                html = NEW_CUERPO_CIRUGIA_PROCEDIMIENTOS_MEDICOS + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsCirugiaApendicectomia.Checked)
            {
                html = NEW_CUERPO_CIRUGIA_APENDICECTOMIA + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsCirugiaColecistectomia.Checked)
            {
                html = NEW_CUERPO_CIRUGIA_COLECISTECTOMIA + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsCirugiaeventrorrafiasohernia.Checked)
            {
                html = NEW_CUERPO_CIRUGIA_EVENTRORRAFIAS_O_HERNIA_UMBILICAL + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsCirugiaHerniorrafiainguinal.Checked)
            {
                html = NEW_CUERPO_CIRUGIA_HERNIORRAFIA_INGUINAL + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsCirugialaparoscopiadiagnostica.Checked)
            {
                html = NEW_CUERPO_CIRUGIA_LAPAROSCOPIA_DIAGNOSTICA + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsCirugialaparotomiaexploratoria.Checked)
            {
                html = NEW_CUERPO_CIRUGIA_LAPAROTOMIA_EXPLORATORIA + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsCirugialavadoperitonealterapeutico.Checked)
            {
                html = NEW_CUERPO_CIRUGIA_LAVADO_PERITONEAL_TERAPEUTICO + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsCirugiatoracostomia.Checked)
            {
                html = NEW_CUERPO_CIRUGIA_TORACOSTOMIA + CUERPO_DATOS_PERSONALES;
            }
            /***************ORTOPEDIA******************************************************************/
            else if (chkconsOrtopedia.Checked)
            {
                html = NEW_CUERPO_PROCEDIMIENTOS_ORTOPEDIA + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsCirugiaortopedia.Checked)
            {
                html = NEW_CIRUGIAS_DE_ORTOPEDIA + CUERPO_DATOS_PERSONALES;
            }
            /***************UROLOGÍA******************************************************************/
            else if (chkconsUrologianefrectomias.Checked)
            {
                html = NEW_CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFRECTOMIAS + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsUrologianefrolitotomiapercutanea.Checked)
            {
                html = NEW_CUERPO_PROCEDIMIENTOS_UROLOGIA_NEFROLITOTOMIA_PERCUTANEA + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsUrologiaendoscopicosurologia.Checked)
            {
                html = NEW_CUERPO_PROCEDIMIENTOS_ENDOSCOPICOS_DE_UROLOGIA + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsUrologiacirugiaprostata.Checked)
            {
                html = NEW_CUERPO_PROCEDIMIENTOS_CIRUGIA_DE_PROSTATA + CUERPO_DATOS_PERSONALES;
            }
            /***************DERMATOLOGÍA******************************************************************/
            else if (chkconsDermatologia.Checked)
            {
                html = NEW_CUERPO_PROCEDIMIENTOS_DERMATOLOGIA + CUERPO_DATOS_PERSONALES;
            }
            /***************NEUROCIRUGÍA******************************************************************/
            else if (chkconsNeurocirugiabloqueocolumna.Checked)
            {
                html = NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_BLOQUEO_DE_COLUMNA + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsNeurocirugiacirugiacolumna.Checked)
            {
                html = NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_CIRUGIA_DE_COLUMNA + CUERPO_DATOS_PERSONALES;
            }
            else if (chkconsNeurocirugiaresecciontumorcerebral.Checked)
            {
                html = NEW_CUERPO_PROCEDIMIENTOS_NEUROCIRUGIA_RESECCION_DE_TUMOR_CEREBRAL + CUERPO_DATOS_PERSONALES;
            }





            Session["BODY_HTML"] = html;
            return html;

        }

        catch
        { return html = ""; }

    }
    private byte[] TratamientoDatosPDF()
    {
        string cadenaEncriptada = Encrypt.GetMD5(DateTime.Now.ToString());

        string html = Session["BODY_HTML"].ToString();
        var htmlContent = String.Format(html);
        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        pathDescargaTratamiento = "../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf";


        htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf"));

        string FilePath = Server.MapPath(pathDescargaTratamiento);

        WebClient User = new WebClient();

        FileBufferTratamientoDatos = User.DownloadData(FilePath);

        return FileBufferTratamientoDatos;

    }
    #endregion
    protected void btnCaptureFirmMedico_Click(object sender, EventArgs e)
    {
        Validar = "ok";
        Session["Validar"] = Validar.ToString();
        ScriptManager.RegisterStartupScript(this, GetType(), "captsig", "Capture();", true);
    }

    #region "Check Cirugía"
    protected void chkconsCirugia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugia.Checked)
        {
            LimpiarCheck();
            LimpiarCheckCirugia();
            divcirugia.Visible = true;
        }
        else
        {
            LimpiarCheck();
        }

    }
    protected void chkconsCirugiaApendicectomia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugiaApendicectomia.Checked)
        {
            LimpiarCheck();
            LimpiarCheckCirugiaApendicectomia();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsCirugiaColecistectomia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugiaColecistectomia.Checked)
        {
            LimpiarCheck();
            LimpiarCheckCirugiaColecistectomia();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsCirugiaeventrorrafiasohernia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugiaeventrorrafiasohernia.Checked)
        {
            LimpiarCheck();
            LimpiarCheckCirugiaEventrorrafias();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsCirugiaHerniorrafiainguinal_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugiaHerniorrafiainguinal.Checked)
        {
            LimpiarCheck();
            LimpiarCheckCirugiaHerniorrafiainguinal();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsCirugialaparoscopiadiagnostica_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugialaparoscopiadiagnostica.Checked)
        {
            LimpiarCheck();
            LimpiarCirugialaparoscopiadiagnostica();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsCirugialaparotomiaexploratoria_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugialaparotomiaexploratoria.Checked)
        {
            LimpiarCheck();
            LimpiarCirugialaparotomiaexploratoria();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsCirugialavadoperitonealterapeutico_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugialavadoperitonealterapeutico.Checked)
        {
            LimpiarCheck();
            LimpiarCirugialavadoperitonealterapeutico();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsCirugiatoracostomia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugiatoracostomia.Checked)
        {
            LimpiarCheck();
            LimpiarCirugiatoracostomia();
        }
        else
        {
            LimpiarCheck();
        }
    }
    #endregion

    #region "Check Ortopedia"
    protected void chkconsOrtopedia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsOrtopedia.Checked)
        {
            LimpiarCheck();
            LimpiarCheckOrtopedia();
        }
        else
        {
            LimpiarCheck();
        }

    }
    protected void chkconsCirugiaortopedia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsCirugiaortopedia.Checked)
        {
            LimpiarCheck();
            LimpiarCheckCirugiaortopedia();
        }
        else
        {
            LimpiarCheck();
        }

    }
    #endregion

    #region "Check Urología"
    protected void chkconsUrologianefrectomias_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsUrologianefrectomias.Checked)
        {
            LimpiarCheck();
            LimpiarUrologianefrectomias();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsUrologianefrolitotomiapercutanea_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsUrologianefrolitotomiapercutanea.Checked)
        {
            LimpiarCheck();
            LimpiarUrologianefrolitotomiapercutanea();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsUrologiaendoscopicosurologia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsUrologiaendoscopicosurologia.Checked)
        {
            LimpiarCheck();
            LimpiarUrologiaendoscopicosurologia();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsUrologiacirugiaprostata_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsUrologiacirugiaprostata.Checked)
        {
            LimpiarCheck();
            LimpiarUrologiacirugiaprostata();
        }
        else
        {
            LimpiarCheck();
        }
    }
    #endregion

    #region "Check Neurocirugía"
    protected void chkconsNeurocirugiabloqueocolumna_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsNeurocirugiabloqueocolumna.Checked)
        {
            LimpiarCheck();
            LimpiarNeurocirugiabloqueocolumna();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsNeurocirugiacirugiacolumna_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsNeurocirugiacirugiacolumna.Checked)
        {
            LimpiarCheck();
            LimpiarNeurocirugiacirugiacolumna();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void chkconsNeurocirugiaresecciontumorcerebral_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsNeurocirugiaresecciontumorcerebral.Checked)
        {
            LimpiarCheck();
            LimpiarNeurocirugiaresecciontumorcerebral();
        }
        else
        {
            LimpiarCheck();
        }
    }
    #endregion

    protected void chkconsAnestesia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsAnestesia.Checked)
        {
            LimpiarCheck();
            LimpiarCheckAnestesia();
        }
        else
        {
            LimpiarCheck();
        }
    }
    protected void CheckConsentimiento_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckConsentimiento.Checked)
        {
            CheckDisentimiento.Checked = false;
        }
    }
    protected void CheckDisentimiento_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckDisentimiento.Checked)
        {
            CheckConsentimiento.Checked = false;
        }
    }
    
    protected void chkconsDermatologia_CheckedChanged(object sender, EventArgs e)
    {
        if (chkconsDermatologia.Checked)
        {
            LimpiarCheck();
            LimpiarCheckDermatologia();
        }
        else
        {
            LimpiarCheck();
        }
    }

    #region "Limpiar"

    #region "Limpiar Todo"
    protected void LimpiarCheck()
    {
        divOrtopedia.Visible = false;
        divConsideraciones.Visible = false;
        divExplicacion.Visible = false;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divconsentmientoDisentimiento.Visible = false;
        divAnestesia.Visible = false;
        divcirugia.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        divEVENTRORRAFIAS.Visible = false;


        txtConsideraciones.Text = "";
        txtTipoIntervencion.Text = "";


        btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        btnReview.Visible = false;

        CheckUtilizarFirmaBD.Visible = false;
        CheckAnestesiaConductiva.Checked = false;
        CheckAnestesiaGeneral.Checked = false;
        CheckAnestesiaLocal.Checked = false;
        CheckAnestesiaRegional.Checked = false;
        CheckCambioYeso.Checked = false;
        CheckNefrectomiaParcialAbierta.Checked = false;
        CheckNefrectomiaSimpleLaparoscopica.Checked = false;
        CheckConsentimiento.Checked = false;
        CheckDisentimiento.Checked = false;
        CheckNefrectomiaParcialLaparoscopica.Checked = false;
        CheckDiagnostico.Checked = false;
        CheckPatologia.Checked = false;
        CheckNefrectomiaRadicalAbierta.Checked = false;
        CheckNefrectomiaSimpleAbierta.Checked = false;
        CheckNefrectomiaRadicalLaparoscopica.Checked = false;
        CheckOnicectomia.Checked = false;
        CheckInfiltracion.Checked = false;
        Checkeventrorrafias.Checked = false;
        Checkhernia.Checked = false;
        CheckNefroUreterolitotomiaendoscopica.Checked = false;
        CheckUreteropielorrenoscopia.Checked = false;
        CheckCistoscopia.Checked = false;
        CheckEndopielotomia.Checked = false;
        CheckLesionvesical.Checked = false;
        Checkprostataradical.Checked = false;
        Checkprostatasimple.Checked = false;
        Checklaparoscopicaprostata.Checked = false;
        Checktransuretalprostata.Checked = false;
        Checkcolmnacervical.Checked = false;
        Checkcolumnadorsal.Checked = false;
        Checkcolumnalumbar.Checked = false;
        Checkcolumnasacra.Checked = false;
        Checklaminectomiacervical.Checked = false;
        Checkmicrodiscectomialumbar.Checked = false;
        Checklaminectomiatoracica.Checked = false;
        Checkartrodesiscolumnacervical.Checked = false;
        Checklaminectomialumbar.Checked = false;
        Checkartrodesiscolumnatoracica.Checked = false;
        Checkmicrodiscectomia.Checked = false;
        Checkartrodesiscolumnalumbar.Checked = false;
        Checkmicrodiscectomiatoracica.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;



    }
    #endregion

    #region "Limpiar Dermatología"
    protected void LimpiarCheckDermatologia()
    {
        btnReview.Visible = true;
        divcirugia.Visible = true;
        divExplicacion.Visible = true;
        divConsideraciones.Visible = true;
        divconsentmientoDisentimiento.Visible = true;


        lblConsideraciones.Text = "Campos para dermatología";


        divOrtopedia.Visible = false;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divAnestesia.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        divEVENTRORRAFIAS.Visible = false;

        chkconsCirugia.Checked = false;
        chkconsAnestesia.Checked = false;
        chkconsOrtopedia.Checked = false;
        chkconsUrologianefrectomias.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;



        txtTipoIntervencion.Text = "";
        txtConsideraciones.Text = "";

        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
    }
    #endregion

    #region "Limpiar Urología"
    protected void LimpiarUrologianefrectomias()
    {
        btnReview.Visible = true;
        divExplicacion.Visible = true;
        divUrologia.Visible = true;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        divConsideraciones.Visible = true;
        divconsentmientoDisentimiento.Visible = true;
        lblConsideraciones.Text = "Campos para urología nefrectomías";

        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsAnestesia.Checked = false;
        chkconsOrtopedia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;

        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;

        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;


        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        divcirugia.Visible = false;
        divEVENTRORRAFIAS.Visible = false;

        txtTipoIntervencion.Text = "";
        txtConsideraciones.Text = "";

        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
    }
    protected void LimpiarUrologianefrolitotomiapercutanea()
    {
        btnReview.Visible = true;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        divConsideraciones.Visible = true;
        divconsentmientoDisentimiento.Visible = true;
        lblConsideraciones.Text = "Campos para urología nefrolitotomía percutánea";

        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsAnestesia.Checked = false;
        chkconsOrtopedia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;

        chkconsUrologianefrectomias.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;

        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;

        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        divcirugia.Visible = false;
        divEVENTRORRAFIAS.Visible = false;

        txtTipoIntervencion.Text = "";
        txtConsideraciones.Text = "";

        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
    }
    protected void LimpiarUrologiaendoscopicosurologia()
    {
        btnReview.Visible = true;
        divExplicacion.Visible = true;
        divUrologia1.Visible = true;
        divUrologia.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        divConsideraciones.Visible = true;
        divconsentmientoDisentimiento.Visible = true;
        lblConsideraciones.Text = "Campos para endoscópicos de urología";

        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsAnestesia.Checked = false;
        chkconsOrtopedia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;

        chkconsUrologianefrectomias.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;



        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        divcirugia.Visible = false;
        divEVENTRORRAFIAS.Visible = false;

        txtTipoIntervencion.Text = "";
        txtConsideraciones.Text = "";

        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
    }
    protected void LimpiarUrologiacirugiaprostata()
    {
        btnReview.Visible = true;
        divExplicacion.Visible = true;
        divUrologia2.Visible = true;
        divUrologia1.Visible = false;
        divUrologia.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        divConsideraciones.Visible = true;
        divconsentmientoDisentimiento.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía de próstata";

        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsAnestesia.Checked = false;
        chkconsOrtopedia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;

        chkconsUrologianefrectomias.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;



        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        divcirugia.Visible = false;
        divEVENTRORRAFIAS.Visible = false;

        txtTipoIntervencion.Text = "";
        txtConsideraciones.Text = "";

        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
    }
    #endregion

    #region "Limpiar Anestesia"
    protected void LimpiarCheckAnestesia()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para anestesia";
        divConsideraciones.Visible = true;
        divAnestesia.Visible = true;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divOrtopedia.Visible = false;
        divcirugia.Visible = false;
        divEVENTRORRAFIAS.Visible = false;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        divExplicacion.Visible = false;
        txtTipoIntervencion.Text = "";
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    #endregion

    #region "Limpiar Ortopedia"
    protected void LimpiarCheckOrtopedia()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para ortopedia";
        divOrtopedia.Visible = true;
        divConsideraciones.Visible = true;
        txtConsideraciones.Text = "";
        chkconsCirugia.Checked = false;
        chkconsAnestesia.Checked = false;
        chkconsUrologianefrectomias.Checked = false;
        divAnestesia.Visible = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divEVENTRORRAFIAS.Visible = false;
        divExplicacion.Visible = true;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        txtTipoIntervencion.Text = "";
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarCheckCirugiaortopedia()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía ortopedia";
        divConsideraciones.Visible = true;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        //btnCaptureFirm.Visible = false;
        divEVENTRORRAFIAS.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
    }
    #endregion

    #region "Limpiar Cirugía"
    protected void LimpiarCheckCirugia()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía";
        divConsideraciones.Visible = true;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        //btnCaptureFirm.Visible = false;
        divEVENTRORRAFIAS.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarCheckCirugiaApendicectomia()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía apendicectomía";
        divConsideraciones.Visible = true;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        divEVENTRORRAFIAS.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarCheckCirugiaColecistectomia()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía colecistectomía";
        divConsideraciones.Visible = true;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        divEVENTRORRAFIAS.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarCheckCirugiaEventrorrafias()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía eventrorrafias o hernia umbilical";
        divConsideraciones.Visible = true;
        divEVENTRORRAFIAS.Visible = true;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        //btnCaptureFirm.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarCheckCirugiaHerniorrafiainguinal()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía herniorrafia inguinal";
        divConsideraciones.Visible = true;
        divEVENTRORRAFIAS.Visible = false;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarCirugialaparoscopiadiagnostica()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía laparoscopia diagnóstica";
        divConsideraciones.Visible = true;
        divEVENTRORRAFIAS.Visible = false;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarCirugialaparotomiaexploratoria()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía laparotomía exploratoria";
        divConsideraciones.Visible = true;
        divEVENTRORRAFIAS.Visible = false;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarCirugialavadoperitonealterapeutico()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía lavado peritoneal terapéutico";
        divConsideraciones.Visible = true;
        divEVENTRORRAFIAS.Visible = false;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugiatoracostomia.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarCirugiatoracostomia()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía toracostomia";
        divConsideraciones.Visible = true;
        divEVENTRORRAFIAS.Visible = false;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    #endregion

    #region "Limpiar Neurocirugía"
    protected void LimpiarNeurocirugiabloqueocolumna()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía bloqueo de columna";
        divConsideraciones.Visible = true;
        divNeurocirugia.Visible = true;
        divNeurocirugia1.Visible = false;
        divEVENTRORRAFIAS.Visible = false;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;



    }
    protected void LimpiarNeurocirugiacirugiacolumna()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para cirugía de columna";
        divConsideraciones.Visible = true;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = true;
        divEVENTRORRAFIAS.Visible = false;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiaresecciontumorcerebral.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
    }
    protected void LimpiarNeurocirugiaresecciontumorcerebral()
    {
        btnReview.Visible = true;
        lblConsideraciones.Text = "Campos para resección de tumor cerebral";
        divConsideraciones.Visible = true;
        divNeurocirugia.Visible = false;
        divNeurocirugia1.Visible = false;
        divEVENTRORRAFIAS.Visible = false;
        txtConsideraciones.Text = "";
        chkconsOrtopedia.Checked = false;
        chkconsAnestesia.Checked = false;
        divAnestesia.Visible = false;
        divOrtopedia.Visible = false;
        chkconsUrologianefrectomias.Checked = false;
        divconsentmientoDisentimiento.Visible = true;
        divcirugia.Visible = false;
        divExplicacion.Visible = true;
        divUrologia.Visible = false;
        divUrologia1.Visible = false;
        divUrologia2.Visible = false;
        //btnCaptureFirm.Visible = false;
        btnCaptureFirmMedico.Visible = false;
        btnGuardar.Visible = false;
        chkconsDermatologia.Checked = false;
        chkconsCirugia.Checked = false;
        chkconsCirugiaApendicectomia.Checked = false;
        chkconsCirugiaColecistectomia.Checked = false;
        chkconsCirugiaeventrorrafiasohernia.Checked = false;
        chkconsCirugiaHerniorrafiainguinal.Checked = false;
        chkconsCirugialaparoscopiadiagnostica.Checked = false;
        chkconsCirugialaparotomiaexploratoria.Checked = false;
        chkconsCirugialavadoperitonealterapeutico.Checked = false;
        chkconsCirugiaortopedia.Checked = false;
        chkconsUrologianefrolitotomiapercutanea.Checked = false;
        chkconsUrologiaendoscopicosurologia.Checked = false;
        chkconsUrologiacirugiaprostata.Checked = false;
        chkconsNeurocirugiabloqueocolumna.Checked = false;
        chkconsNeurocirugiacirugiacolumna.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;

    }
    #endregion
    #endregion

    protected void CheckUtilizarFirmaBD_CheckedChanged(object sender, EventArgs e)
    {
        if(CheckUtilizarFirmaBD.Checked)
        {
            btnCaptureFirm.Visible = true;
        }  
        else
        {
            btnCaptureFirm.Visible = false;
        }
    }

    protected bool ValidarCamposAut()
    {
        bool sw = true;
        if(txtIdAut.Text=="")
        {
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
            txtIdAut.Focus();
            sw = false;
        }
        if (txtNomAut.Text == "")
        {
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
            txtNomAut.Focus();
            sw = false;
        }
        return sw;
    }


    protected bool ValidarDatosHTML()
    {
        bool sw = true;
        if (CheckRepresentante.Checked)
        {
            if (txtNomAut.Text == "")
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                txtNomAut.Focus();
                sw = false;
            }
            if (txtIdAut.Text == "")
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                txtIdAut.Focus();
                sw = false;
            }
        }        
        /*VALIDACIÓN ANESTESIA*/
        if (chkconsAnestesia.Checked)
        {

            if ((CheckAnestesiaLocal.Checked == false) && (CheckAnestesiaRegional.Checked == false) && (CheckAnestesiaGeneral.Checked == false) && (CheckAnestesiaConductiva.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                txtIdAut.Focus();
                sw = false;
            }
        }
        else
        {
            if ((CheckConsentimiento.Checked == false) && (CheckDisentimiento.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckConsentimiento.Focus();
                sw = false;
            }
        }
        /*VALIDACIÓN CIRUGÍA*/
        if ((chkconsCirugia.Checked) | (chkconsDermatologia.Checked))
        { 
            if (txtTipoIntervencion.Text == "")
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                txtTipoIntervencion.Focus();
                sw = false;
            }
            if ((CheckDiagnostico.Checked == false) && (CheckPatologia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckDiagnostico.Focus();
                sw = false;
            }

        }
        if ((chkconsCirugiaApendicectomia.Checked) | (chkconsCirugiaColecistectomia.Checked) | (chkconsCirugiatoracostomia.Checked) | (chkconsCirugialavadoperitonealterapeutico.Checked) |
            (chkconsCirugialaparotomiaexploratoria.Checked) | (chkconsCirugialaparoscopiadiagnostica.Checked) | (chkconsCirugiaHerniorrafiainguinal.Checked) | (chkconsCirugiaortopedia.Checked) | (chkconsNeurocirugiaresecciontumorcerebral.Checked) |
            (chkconsUrologianefrolitotomiapercutanea.Checked))
        {
            if ((CheckDiagnostico.Checked == false) && (CheckPatologia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckDiagnostico.Focus();
                sw = false;
            }
        }
        if(chkconsCirugiaeventrorrafiasohernia.Checked)
        {
            if ((CheckDiagnostico.Checked == false) && (CheckPatologia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckDiagnostico.Focus();
                sw = false;
            }
            if ((Checkeventrorrafias.Checked == false) && (Checkhernia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                Checkeventrorrafias.Focus();
                sw = false;
            }
        }
        /*VALIDACIÓN ORTOPEDIA*/
        if(chkconsOrtopedia.Checked)
        {
            if ((CheckDiagnostico.Checked == false) && (CheckPatologia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckDiagnostico.Focus();
                sw = false;
            }
            if ((CheckInfiltracion.Checked == false) && (CheckCambioYeso.Checked == false) && (CheckOnicectomia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckInfiltracion.Focus();
                sw = false;
            }
        }
        /*VALIDACIÓN UROLOGÍA*/
        if (chkconsUrologianefrectomias.Checked)
        {
            if ((CheckDiagnostico.Checked == false) && (CheckPatologia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckDiagnostico.Focus();
                sw = false;
            }
            if ((CheckNefrectomiaParcialAbierta.Checked == false) && (CheckNefrectomiaSimpleLaparoscopica.Checked == false) && (CheckNefrectomiaParcialLaparoscopica.Checked == false) && (CheckNefrectomiaRadicalAbierta.Checked == false) && (CheckNefrectomiaSimpleAbierta.Checked == false) && (CheckNefrectomiaRadicalLaparoscopica.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckNefrectomiaParcialAbierta.Focus();
                sw = false;
            }
        }
        if (chkconsUrologiaendoscopicosurologia.Checked)
        {
            if ((CheckDiagnostico.Checked == false) && (CheckPatologia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckDiagnostico.Focus();
                sw = false;
            }
            if ((CheckNefroUreterolitotomiaendoscopica.Checked == false) && (CheckUreteropielorrenoscopia.Checked == false) && (CheckCistoscopia.Checked == false) && (CheckEndopielotomia.Checked == false) && (CheckLesionvesical.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckNefroUreterolitotomiaendoscopica.Focus();
                sw = false;
            }
        }
        if (chkconsUrologiacirugiaprostata.Checked)
        {
            if ((CheckDiagnostico.Checked == false) && (CheckPatologia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckDiagnostico.Focus();
                sw = false;
            }
            if ((Checkprostataradical.Checked == false) && (Checkprostatasimple.Checked == false) && (Checklaparoscopicaprostata.Checked == false) && (Checktransuretalprostata.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckNefroUreterolitotomiaendoscopica.Focus();
                sw = false;
            }
        }
        /*VALIDACIÓN NEUROCIRUGÍA*/
        if (chkconsNeurocirugiabloqueocolumna.Checked)
        {
            if ((CheckDiagnostico.Checked == false) && (CheckPatologia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckDiagnostico.Focus();
                sw = false;
            }
            if ((Checkcolmnacervical.Checked == false) && (Checkcolumnadorsal.Checked == false) && (Checkcolumnalumbar.Checked == false) && (Checkcolumnasacra.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckNefroUreterolitotomiaendoscopica.Focus();
                sw = false;
            }
        }
        if (chkconsNeurocirugiacirugiacolumna.Checked)
        {
            if ((CheckDiagnostico.Checked == false) && (CheckPatologia.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckDiagnostico.Focus();
                sw = false;
            }
            if ((Checklaminectomiacervical.Checked == false) && (Checkmicrodiscectomialumbar.Checked == false) && (Checklaminectomiatoracica.Checked == false) && (Checkartrodesiscolumnacervical.Checked == false) && (Checklaminectomialumbar.Checked == false) && (Checkartrodesiscolumnatoracica.Checked == false) && (Checkmicrodiscectomia.Checked == false) && (Checkartrodesiscolumnalumbar.Checked == false) && (Checkmicrodiscectomiatoracica.Checked == false))
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                CheckNefroUreterolitotomiaendoscopica.Focus();
                sw = false;
            }
        }
        return sw;
    }
}