﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Anexos_VerPDF : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (!IsPostBack)
        {
            if (Request.QueryString["Origen1"] == "Busquedad")
            {
                string result = Request.QueryString["PDF"].ToString();
                //string result = Session["PDF"].ToString();
                string FilePath = Server.MapPath(result);

                WebClient User = new WebClient();

                Byte[] FileBuffer = User.DownloadData(FilePath);

                if (FileBuffer != null)

                {
                    Response.Buffer = true;
                    Response.Cache.SetCacheability(HttpCacheability.NoCache);
                    Response.AddHeader("content-length", FileBuffer.Length.ToString());
                    Response.ContentType = "application/pdf";
                    Response.BinaryWrite(FileBuffer);
                    Response.End();

                }
            }
            else 
            {

                DataTable dt = Session["Datos"] as DataTable;

                Response.Clear();
                Response.ContentType = "application/pdf";
                Response.BinaryWrite((byte[])dt.Rows[0]["CONSENTIMIENTO_PDF"]);
                Response.End();

            }


        }
    }

}

