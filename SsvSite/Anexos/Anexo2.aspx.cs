﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClinicaCES.Correo;
using System.Data;
using System.IO;

public partial class Anexos_Anexo2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            datosIniciales();
        }
    }
    protected void datosIniciales()
    {
        //generarSolicitud()   
        if (Request.QueryString["cookie"] != null)
        {
            Procedimientos.LlenarCombos(dllDepartamento, new ClinicaCES.Logica.LMaestros().ListarDepartamentos(), "CODDEP", "DEP");
            TraerDatos(Procedimientos.descifrar(Request.QueryString["cookie"]));
        }
        TextBox10.Text = DateTime.Now.ToShortDateString();
        TextBox20.Text = DateTime.Now.ToShortTimeString();

    }
    protected void dllDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        Procedimientos.LlenarCombos(ddlMunicipio, new ClinicaCES.Logica.LMaestros().ListarMunicipios(dllDepartamento.SelectedValue), "CODMUN", "MUN");
    }
    protected void TraerDatos(string busqueda)
    {
        string msg = "";
        try
        {
            
            string Correlativo = "";
            string NumPaciente = "";
            string[] datos;
            datos = busqueda.Split('|');
            string cedula = datos[0].PadRight(13);
            string formulario = datos[1];
            DataSet dt = new ClinicaCES.Logica.LAnexo2().Anexo2Consultar(cedula, formulario);
            DataTable ds = dt.Tables[0];
            foreach (DataRow row in ds.Rows)
            {
                TextBox75.Text = Convert.ToString(row["PAC_PAC_APELLPATER"]);
                TextBox76.Text = Convert.ToString(row["PAC_PAC_APELLMATER"]);
                TextBox77.Text = Convert.ToString(row["PRINOMBRE"]);
                TextBox78.Text = Convert.ToString(row["SEGNOMBRE"]);
                rblTipoId.SelectedValue = Convert.ToString(row["PAC_PAC_TIPOIDENTCODIGO"]);
                TextBox81.Text = Convert.ToString(row["PAC_PAC_RUT"]);
                TextBox102.Text = Convert.ToString(row["PAC_PAC_FECHANACIM"]);
                TextBox112.Text = Convert.ToString(row["PAC_PAC_DIRECCIONGRALHABIT"]);
                TextBox113.Text = Convert.ToString(row["PAC_PAC_FONO"]);
                TextBox123.Text = Convert.ToString(row["DEPARTAMENTO"]);
                TextBox124.Text = Convert.ToString(row["CIUDAD"]);
                rblAtencion.SelectedValue = Convert.ToString(row["MTVCON_ORIGEN"]);
                rblTriage.SelectedValue = Convert.ToString(row["TRIAGECOD"]);
                TextBox18.Text = Convert.ToString(row["FECHAINGRESO"]);
                TextBox19.Text = Convert.ToString(row["HORAINGRESO"]);
                TextBox462.Text = caracteresEspeciales(Convert.ToString(row["TEXTO"]));
                Correlativo = Convert.ToString(row["MTVCORRELATIVO"]);
                NumPaciente = Convert.ToString(row["PAC_PAC_NUMERO"]);
                ViewState["tipoId"] = Convert.ToString(row["TIPOID"]);
                traerUsuario();
            }
            DataSet dia = new ClinicaCES.Logica.LAnexo2().Anexo2DiagnosticosConsultar(Correlativo, NumPaciente);
            traerDiagnosticos(dia.Tables[0]);
        }
        catch
        {
            msg = "69";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
      //  DataTable dtGrid = new ClinicaCES.Logica.LMaestros().ConsultarEmpresasOracle();
    }
    protected void traerDiagnosticos(DataTable dt)
    {
        string msg = "";
        try
        {
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow registro = dt.Rows[i];
                if (i == 0) { TextBox387.Text = registro[0].ToString(); TextBox399.Text = registro[1].ToString(); }
                if (i == 1) { TextBox391.Text = registro[0].ToString(); TextBox400.Text = registro[1].ToString(); }
                if (i == 2) { TextBox395.Text = registro[0].ToString(); TextBox401.Text = registro[1].ToString(); }
                if (i == 3) { TextBox127.Text = registro[0].ToString(); TextBox156.Text = registro[1].ToString(); }
            }
        }
        catch
        {
            msg = "70";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string msg = "";
        try
        {
            if (validar())
            {

                Button1.Enabled = false;
                Button2.Visible = true;
                Button2.Enabled = true;
                string convenio = traerCorreo();
                if (convenio != "")
                {
                    DataRow Nsolicitud = new ClinicaCES.Logica.LMaestros().generarSolicitudAne2().Rows[0];
                    TextBox1.Text = Nsolicitud[0].ToString();
                    //TextBox1.Text = "201606230004";
                    string html = retornarHtml();
                    var htmlContent = String.Format(html);
                    var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                    string pApellido = "";
                    string sApellido = "";
                    string pNombre = "";
                    string sNombre = "";
                    if (TextBox75.Text != "") { pApellido = TextBox75.Text.Substring(0, TextBox75.Text.Length - TextBox75.Text.Length + 1).Trim(); }
                    if (TextBox76.Text != "") { sApellido = TextBox76.Text.Substring(0, TextBox76.Text.Length - TextBox76.Text.Length + 1).Trim(); }
                    if (TextBox77.Text != "") { pNombre = TextBox77.Text.Substring(0, TextBox77.Text.Length - TextBox77.Text.Length + 1).Trim(); }
                    if (TextBox78.Text != "") { sNombre = TextBox78.Text.Substring(0, TextBox78.Text.Length - TextBox78.Text.Length + 1).Trim(); }
                    string archivo = ViewState["tipoId"].ToString() + "-" + TextBox81.Text.Trim() + "-" + pApellido + sApellido + pNombre + sNombre + "-" + TextBox1.Text;
                
                    string Mensaje = construirCorreo();
                    string Mensaje2 = construirCorreo2();
                    htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + archivo + "_Anexo2.pdf"));
                    if (enviar(convenio, Mensaje, archivo + "_Anexo2.pdf - Envio No: " + 1, Server.MapPath("../AUTPDF/" + archivo + "_Anexo2.pdf")))
                    {
                        string[] Filtros = Procedimientos.descifrar(Request.QueryString["cookie"]).Split('|');  
                        if (new ClinicaCES.Logica.LAnexo2().Anexo2Crear(Filtros[0], Filtros[1], TextBox1.Text, convenio, Mensaje, Mensaje2, "2", archivo + "_Anexo2.pdf", Server.MapPath("../AUTPDF/" + archivo + "_Anexo2.pdf"), Session["Nick1"].ToString()))
                        {
                            msg = "1";
                            inactivarControles();
                            ViewState["Archivo2"] = archivo + "_Anexo2.pdf";
                        }
                        else
                        {
                            msg = "3";
                        }
                    }
                    Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                }
                else
                {
                    msg = "78";
                    Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                }
                
            }
            else
            {
                msg = "68";
                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            }
        }
        catch (Exception ex)
        {
            msg = "71";
            
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected void AgregarPrintScript(string ruta)
    {
        string msg = "";
        try
        {
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", ruta));
            Response.ContentType = "application/pdf";
            Response.WriteFile(Server.MapPath(Path.Combine("~/AUTPDF", ruta)));
            Response.End();
        }
        catch
        {
            msg = "74";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected string traerCorreo()
    {
        string msg = "";
        try
        {
            string[] Filtros = Procedimientos.descifrar(Request.QueryString["cookie"]).Split('|');
            DataRow correo = new ClinicaCES.Logica.LBusquedaPacientes().traerCorreo(Filtros[0], Filtros[1], "1").Rows[0];
            return correo[0].ToString();
        }
        catch
        {
            msg = "72";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            return "";
        }
    }
    protected string construirCorreo()
    {
        
        string html1 =" <!DOCTYPE html> " +
" <html xmlns='http://www.w3.org/1999/xhtml'> " +
" <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title> " +
" </title> " +
" <style type='text/css'> " +
" p.MsoNormal " +
" 	{margin-bottom:.0001pt; " +
"	font-size:11.0pt; " +
" 	font-family:'Calibri',sans-serif; " +
" 	        margin-left: 0cm; " +
" margin-right: 0cm; " +
" margin-top: 0cm; " +
" } " +
" .auto-style1 { " +
" width: 100%; " +
" } " +
" table.MsoNormalTable " +
" 	{font-size:11.0pt; " +
" 	font-family:'Calibri',sans-serif; " +
" 	} " +
" </style> " +
" </head> " +
" <body> " +
" <form name='form1' method='post' action='CorreoAnexo2_1.aspx' id='form1'> " +
" <div> " +
" <input type='hidden' name='__VIEWSTATE' id='__VIEWSTATE' value='/wEPDwUKLTEzNDM3NzkxOWRkqxf2ot4JCKSK8Q/o8OsCemLTYF4=' /> " +
" </div> " +
" <div> " +
" 	<input type='hidden' name='__VIEWSTATEGENERATOR' id='__VIEWSTATEGENERATOR' value='B804CE55' /> " +
" </div> " +
" <div> " +
" <p class='MsoNormal'> " +
" <a name='OLE_LINK1'><span style='mso-bookmark:OLE_LINK2'><span style='mso-bookmark: " +
" OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordial saludo,<o:p></o:p></span></span></span></span></span></a></p> " +
" <p class='MsoNormal'> " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark: " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p> " +
" <p class='MsoNormal'> " +
" <span style='font-size:11.0pt;line-height:107%; " +
" font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family: " +
" Calibri;mso-fareast-theme-font:minor-latin;mso-hansi-theme-font:minor-latin; " +
" mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi; " +
" mso-ansi-language:ES-CO;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'>Dando cumplimiento al decreto 4747 de 2007 y la resolución 3047 de 2008, se envía anexo técnico N° 2 solicitando autorización para la atención inicial de urgencias del paciente relacionado en el asunto.</span></p> " +
" <p class='MsoNormal'> " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark: " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p> " +
" <p class='MsoNormal'> " +
" Quedamos atentos a su gestión.<o:p></o:p></p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p></p> " +
" <p class='MsoNormal'> " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark: " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordialmente,<o:p></o:p></span></span></span></span></span></span></p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <o:p></o:p> " +
" <table class='auto-style1'> " +
" <tr> " +
" <td>&nbsp;</td> " +
" <td>&nbsp;</td> " +
" </tr> " +
" <tr> " +
" <td>&nbsp;</td> " +
" <td>&nbsp;</td> " +
" </tr> " +
" </table> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <p class='MsoNormal'> " +
" <table border='0' cellpadding='0' cellspacing='0' class='MsoNormalTable'> " +
" <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes; " +
" height:61.9pt'> " +
" <td style='width:68.25pt;padding:0cm 5.4pt 0cm 5.4pt; " +
" height:61.9pt' valign='top' width='91'> " +
" <p class='MsoNormal'> " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;; " +
" mso-fareast-theme-font:minor-fareast;mso-fareast-language:ES-CO;mso-no-proof: " +
" yes'><![if !vml]> " +
" <img src=cid:companylogo style='height: 79px; width: 58px'><![endif]></span><o:p></o:p></p> " +
" </td> " +
" <td style='width:366.75pt;padding:0cm 5.4pt 0cm 5.4pt;height:61.9pt' width='489'> " +
" <p class='MsoNormal'> " +
" <b><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family: " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#00B0F0; " +
" mso-no-proof:yes'>Autorizaciones</span></b><o:p></o:p></p> " +
" <u1:p></u1:p> " +
" <p class='MsoNormal'> " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family: " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79; " +
" mso-no-proof:yes'>Central de Autorizaciones </span><span style='font-family: " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font: " +
" minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family: " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font: " +
" minor-fareast;color:#1F4E79;mso-no-proof:yes'> Clínica CES<u1:p></u1:p></span><o:p></o:p></p> " +
" <p class='MsoNormal'> " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family: " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79; " +
" mso-no-proof:yes'>Tel: 5767272 ext 7630</span><span style='font-family:&quot;Arial&quot;,sans-serif; " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast; " +
" color:#00B0F0;mso-no-proof:yes'> |</span><span style='font-family:&quot;Arial&quot;,sans-serif; " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast; " +
" color:#1F4E79;mso-no-proof:yes'> Carrera 50c #58-12 </span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;; " +
" mso-fareast-theme-font:minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;; " +
" mso-fareast-theme-font:minor-fareast;color:#1F4E79;mso-no-proof:yes'> Medellín, Colombia<u1:p></u1:p></span><o:p></o:p></p> " +
" <p class='MsoNormal'> " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;; " +
" mso-fareast-theme-font:minor-fareast;mso-no-proof:yes'><a href='http://www.clinicaces.com/'><i><span style='font-family:&quot;Arial&quot;,sans-serif; " +
" color:#00B0F0'>www.clinicaces.com</span></i></a></span><o:p></o:p></p> " +
" </td> " +
" </tr> " +
" </table> " +
" </p> " +
" </div> " +
" </form> " +
" </body> " +
" </html> " ;
        return textoTilde(html1);

    }
    protected string construirCorreo2()
    {
        string html2 = " <!DOCTYPE html> " +
" <html xmlns='http://www.w3.org/1999/xhtml'> " +
" <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title> " +
" </title> " +
" <style type='text/css'> " +
" p.MsoNormal " +
" 	{margin-bottom:.0001pt; " +
" 	font-size:11.0pt; " +
" 	font-family:'Calibri',sans-serif; " +
" 	        margin-left: 0cm; " +
" margin-right: 0cm; " +
" margin-top: 0cm; " +
" } " +
" .auto-style1 { " +
" width: 100%; " +
" } " +
" table.MsoNormalTable " +
" 	{font-size:11.0pt; " +
" 	font-family:'Calibri',sans-serif; " +
" 	} " +
" </style> " +
" </head> " +
" <body> " +
" <form name='form1' method='post' action='CorreoAnexo2_2.aspx' id='form1'> " +
" <div> " +
" <input type='hidden' name='__VIEWSTATE' id='__VIEWSTATE' value='/wEPDwUKLTEzNDM3NzkxOWRkHv9fceH7/zH0CLQSO+tk9mSc4HY=' /> " +
" </div> " +
" <div> " +
" 	<input type='hidden' name='__VIEWSTATEGENERATOR' id='__VIEWSTATEGENERATOR' value='B93E171B' /> " +
" </div> " +
" <div> " +
" <p class='MsoNormal'> " +
" <a name='OLE_LINK1'><span style='mso-bookmark:OLE_LINK2'><span style='mso-bookmark: " +
" OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordial saludo,<o:p></o:p></span></span></span></span></span></a></p> " +
" <p class='MsoNormal'> " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark: " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p> " +
" <p class='MsoNormal'> " +
" Dando cumplimiento al decreto 4747 de 2007 y la resolución 3047 de 2008, se envía anexo técnico N° 2 solicitando autorización para la atención inicial de urgencias del paciente relacionado en el asunto.<o:p></o:p></p> " +
" <p class='MsoNormal'> " +
" <o:p>&nbsp;</o:p></p> " +
" <span style='font-size:11.0pt;line-height:107%;font-family:&quot;Calibri&quot;,sans-serif; " +
" mso-ascii-theme-font:minor-latin;mso-fareast-font-family:Calibri;mso-fareast-theme-font: " +
" minor-latin;mso-hansi-theme-font:minor-latin;mso-bidi-font-family:&quot;Times New Roman&quot;; " +
" mso-bidi-theme-font:minor-bidi;mso-ansi-language:ES-CO;mso-fareast-language: " +
" EN-US;mso-bidi-language:AR-SA'>Con soportes de envío se continuará con la atención del paciente; correo que a su vez será enviado a ente territorial como lo menciona la norma.</span><p class='MsoNormal'> " +
" &nbsp;</p> " +
" <p class='MsoNormal'> " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark: " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p> " +
" <p class='MsoNormal'> " +
" Quedamos atentos a su gestión.<o:p></o:p></p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p></p> " +
" <p class='MsoNormal'> " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark: " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordialmente,<o:p></o:p></span></span></span></span></span></span></p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <o:p></o:p> " +
" <table class='auto-style1'> " +
" <tr> " +
" <td>&nbsp;</td> " +
" <td>&nbsp;</td> " +
" </tr> " +
" <tr> " +
" <td>&nbsp;</td> " +
" <td>&nbsp;</td> " +
" </tr> " +
" </table> " +
" <p class='MsoNormal'> " +
" <o:p></o:p> " +
" </p> " +
" <p class='MsoNormal'> " +
" <table border='0' cellpadding='0' cellspacing='0' class='MsoNormalTable'> " +
" <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes; " +
" height:61.9pt'> " +
" <td style='width:68.25pt;padding:0cm 5.4pt 0cm 5.4pt; " +
" height:61.9pt' valign='top' width='91'> " +
" <p class='MsoNormal'> " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;; " +
" mso-fareast-theme-font:minor-fareast;mso-fareast-language:ES-CO;mso-no-proof: " +
" yes'><![if !vml]> " +
" <img src=cid:companylogo style='height: 79px; width: 58px'><![endif]></span><o:p></o:p></p> " +
" </td> " +
" <td style='width:366.75pt;padding:0cm 5.4pt 0cm 5.4pt;height:61.9pt' width='489'> " +
" <p class='MsoNormal'> " +
" <b><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family: " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#00B0F0; " +
" mso-no-proof:yes'>Autorizaciones</span></b><o:p></o:p></p> " +
" <u1:p></u1:p> " +
" <p class='MsoNormal'> " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family: " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79; " +
" mso-no-proof:yes'>Central de Autorizaciones </span><span style='font-family: " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font: " +
" minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family: " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font: " +
" minor-fareast;color:#1F4E79;mso-no-proof:yes'> Clínica CES<u1:p></u1:p></span><o:p></o:p></p> " +
" <p class='MsoNormal'> " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family: " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79; " +
" mso-no-proof:yes'>Tel: 5767272 ext 7630</span><span style='font-family:&quot;Arial&quot;,sans-serif; " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast; " +
" color:#00B0F0;mso-no-proof:yes'> |</span><span style='font-family:&quot;Arial&quot;,sans-serif; " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast; " +
" color:#1F4E79;mso-no-proof:yes'> Carrera 50c #58-12 </span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;; " +
" mso-fareast-theme-font:minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;; " +
" mso-fareast-theme-font:minor-fareast;color:#1F4E79;mso-no-proof:yes'> Medellín, Colombia<u1:p></u1:p></span><o:p></o:p></p> " +
" <p class='MsoNormal'> " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;; " +
" mso-fareast-theme-font:minor-fareast;mso-no-proof:yes'><a href='http://www.clinicaces.com/'><i><span style='font-family:&quot;Arial&quot;,sans-serif; " +
" color:#00B0F0'>www.clinicaces.com</span></i></a></span><o:p></o:p></p> " +
" </td> " +
" </tr> " +
" </table> " +
" </p> " +
" </div> " +
" </form> " +
" </body> " +
" </html> " ;
        return textoTilde(html2);
    }
    protected bool enviar(string Para, string Mensaje, string Asunto, string ruta)
    {
        string msg = "";
        bool bandera = false;
        try
        {
            Correo correo = new Correo();
            correo.Para = Para;
            correo.Mensaje = Mensaje;
            correo.Asunto = Asunto;
            //correo.De = ConfigurationManager.AppSettings["mailSoporte"];
            //string path = Server.MapPath(@"..\AUTPDF\anexo2.pdf"); ;
            string[] result = new string[] { ruta };
            correo.Adjuntos = result;
            if (correo.Enviar())
            { bandera = true; }
            return bandera;
        }
        catch
        {
            msg = "73";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            return bandera;
        }
    }
    protected string caracteresEspeciales(string texto)
    {
        string comilla = "\"";
        string valor = "";
        string nuevoTexto = "";
        for (int i = 0; i < texto.Length; i++)
        {
            valor = texto.Substring(i, 1);
            if (valor == comilla) { valor = "&#34"; }
            nuevoTexto = nuevoTexto + valor;
        }
        return nuevoTexto;
    }
    protected string textoTilde(string texto)
    {
        string nuevoTexto = "";
        string valor = "";
        
        for (int i = 0; i < texto.Length; i++)
        {
            valor = texto.Substring(i, 1);
            if (valor == "á") { valor = "&#225"; } else if (valor == "Á") { valor = "&#193"; }
            if (valor == "é") { valor = "&#233"; } else if (valor == "É") { valor = "&#201"; }
            if (valor == "í") { valor = "&#237"; } else if (valor == "Í") { valor = "&#205"; }
            if (valor == "ó") { valor = "&#243"; } else if (valor == "Ó") { valor = "&#211"; }
            if (valor == "ú") { valor = "&#250"; } else if (valor == "Ú") { valor = "&#218"; }
            if (valor == "ñ") { valor = "&#241"; } else if (valor == "Ñ") { valor = "&#209"; }
            
            nuevoTexto = nuevoTexto + valor;
        }
        return nuevoTexto;
    }
    protected string valorTamano()
    {
        int tam = 20;
        int maxCaracter = 100;
        if (TextBox462.Text.Length > maxCaracter)
        {
            while (TextBox462.Text.Length > maxCaracter)
            {
                maxCaracter = maxCaracter + 100;
                tam = tam + 20;
            }
        }
        return tam.ToString();
    }
    protected string retornarHtml()
    {
        string html="";
        try
        {
            string tam = valorTamano();
            string[] tipoId = { "", "", "", "", "", "", "" };
            string[] cobertura = { "", "", "", "", "", "", "", "" };
            string[] Atencion = { "", "", "", "", "" };
            string[] Triage = { "", "", "", "", "" };
            string[] Remitido = { "", "" };
            string[] Destino = { "", "", "", "", "", "" };
            if (rblTipoId.SelectedValue == "2") { tipoId[0] = "X"; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
            if (rblTipoId.SelectedValue == "3") { tipoId[0] = ""; tipoId[1] = "X"; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
            if (rblTipoId.SelectedValue == "4") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = "X"; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
            if (rblTipoId.SelectedValue == "5") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = "X"; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
            if (rblTipoId.SelectedValue == "P") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = "X"; tipoId[5] = ""; tipoId[6] = ""; }
            if (rblTipoId.SelectedValue == "A") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = "X"; tipoId[6] = ""; }
            if (rblTipoId.SelectedValue == "M") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = "X"; }
            if (rblCobertura.SelectedValue == "1") { cobertura[0] = "X"; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
            if (rblCobertura.SelectedValue == "2") { cobertura[0] = ""; cobertura[1] = "X"; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
            if (rblCobertura.SelectedValue == "3") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = "X"; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
            if (rblCobertura.SelectedValue == "4") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = "X"; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
            if (rblCobertura.SelectedValue == "5") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = "X"; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = ""; }
            if (rblCobertura.SelectedValue == "6") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = "X"; cobertura[6] = ""; cobertura[7] = ""; }
            if (rblCobertura.SelectedValue == "7") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = "X"; cobertura[7] = ""; }
            if (rblCobertura.SelectedValue == "8") { cobertura[0] = ""; cobertura[1] = ""; cobertura[2] = ""; cobertura[3] = ""; cobertura[4] = ""; cobertura[5] = ""; cobertura[6] = ""; cobertura[7] = "X"; }
            if (rblAtencion.SelectedValue == "13") { Atencion[0] = "X"; Atencion[1] = ""; Atencion[2] = ""; Atencion[3] = ""; Atencion[4] = ""; }
            if (rblAtencion.SelectedValue == "01") { Atencion[0] = ""; Atencion[1] = "X"; Atencion[2] = ""; Atencion[3] = ""; Atencion[4] = ""; }
            if (rblAtencion.SelectedValue == "06") { Atencion[0] = ""; Atencion[1] = ""; Atencion[2] = "X"; Atencion[3] = ""; Atencion[4] = ""; }
            if (rblAtencion.SelectedValue == "14") { Atencion[0] = ""; Atencion[1] = ""; Atencion[2] = ""; Atencion[3] = "X"; Atencion[4] = ""; }
            if (rblAtencion.SelectedValue == "02") { Atencion[0] = ""; Atencion[1] = ""; Atencion[2] = ""; Atencion[3] = ""; Atencion[4] = "X"; }
            if (rblTriage.SelectedValue == "3") { Triage[0] = "X"; Triage[1] = ""; Triage[2] = ""; Triage[3] = ""; Triage[4] = ""; }
            if (rblTriage.SelectedValue == "2") { Triage[0] = ""; Triage[1] = "X"; Triage[2] = ""; Triage[3] = ""; Triage[4] = ""; }
            if (rblTriage.SelectedValue == "1") { Triage[0] = ""; Triage[1] = ""; Triage[2] = "X"; Triage[3] = ""; Triage[4] = ""; }
            if (rblTriage.SelectedValue == "4") { Triage[0] = ""; Triage[1] = ""; Triage[2] = ""; Triage[3] = "X"; Triage[4] = ""; }
            if (rblTriage.SelectedValue == "5") { Triage[0] = ""; Triage[1] = ""; Triage[2] = ""; Triage[3] = ""; Triage[4] = "X"; }
            if (rblRemitido.SelectedValue == "SI") { Remitido[0] = "X"; Remitido[1] = ""; }
            if (rblRemitido.SelectedValue == "NO") { Remitido[0] = ""; Remitido[1] = "X"; }
            if (rblDestino.SelectedValue == "1") { Destino[0] = "X"; Destino[1] = ""; Destino[2] = ""; Destino[3] = ""; Destino[4] = ""; Destino[5] = ""; }
            if (rblDestino.SelectedValue == "2") { Destino[0] = ""; Destino[1] = "X"; Destino[2] = ""; Destino[3] = ""; Destino[4] = ""; Destino[5] = ""; }
            if (rblDestino.SelectedValue == "3") { Destino[0] = ""; Destino[1] = ""; Destino[2] = "X"; Destino[3] = ""; Destino[4] = ""; Destino[5] = ""; }
            if (rblDestino.SelectedValue == "4") { Destino[0] = ""; Destino[1] = ""; Destino[2] = ""; Destino[3] = "X"; Destino[4] = ""; Destino[5] = ""; }
            if (rblDestino.SelectedValue == "5") { Destino[0] = ""; Destino[1] = ""; Destino[2] = ""; Destino[3] = ""; Destino[4] = "X"; Destino[5] = ""; }
            if (rblDestino.SelectedValue == "6") { Destino[0] = ""; Destino[1] = ""; Destino[2] = ""; Destino[3] = ""; Destino[4] = ""; Destino[5] = "X"; }
            string Departamento; 
            string Municipio;
            if (rblRemitido.SelectedValue == "NO")
            {
                Departamento = "";
                Municipio = "";
            }
            else
            {
                Departamento = dllDepartamento.SelectedItem.Text;
                Municipio = ddlMunicipio.SelectedItem.Text;
            }
            string path = Server.MapPath("../img/escudocol.png");
            html = "    <table style='width: 1003px; height: 96px;'>                                                                                                             													" +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td rowspan='5' style='width: 63px'>                                                                                                                                                                                           " +
     "             <img src='" + path + "' style='height:77px;width:102px;' />                                                                                                                                                              " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td align='center' colspan='6'><B>MINISTERIO DE LA PROTECCI&#211N SOCIAL</B></td>                                                                                                                                                  " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td colspan='6'>&nbsp;</td>                                                                                                                                                                                                    " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td align='center' colspan='6'><B><font SIZE=4>INFORME DE LA ATENCI&#211N INICIAL DE URGENCIA</font></B></td>                                                                                                                      " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td align='center' style='width: auto'><b>N&#218MERO ATENCI&#211N</b></td>                                                                                                                                                             " +
    "        <td align='center' style='width: auto; margin-left: 40px;'>                                                                                                                                                                    " +
    "            <input name='TextBox1' type='text' id='TextBox1' style='width:104px;text-align:center' value='" + TextBox1.Text + "'/>                                                                        " +
                //"            <input name='TextBox2' type='text' maxlength='1' id='TextBox2' style='width:16px;text-align:center' value='" + TextBox2.Text + "'/>                                                                        " +
                //"            <input name='TextBox3' type='text' maxlength='1' id='TextBox3' style='width:16px;text-align:center' value='" + TextBox3.Text + "'/>                                                                        " +
                //"            <input name='TextBox4' type='text' maxlength='1' id='TextBox4' style='width:16px;text-align:center' value='" + TextBox4.Text + "'/>                                                                        " +
                //"            <input name='TextBox5' type='text' maxlength='1' id='TextBox5' style='width:16px;text-align:center' value='" + TextBox5.Text + "'/>                                                                        " +
                //"            <input name='TextBox6' type='text' maxlength='1' id='TextBox6' style='width:16px;text-align:center' value='" + TextBox6.Text + "'/>                                                                        " +
                //"            <input name='TextBox7' type='text' maxlength='1' id='TextBox7' style='width:16px;text-align:center' value='" + TextBox7.Text + "'/>                                                                        " +
                //"            <input name='TextBox8' type='text' maxlength='1' id='TextBox8' style='width:16px;text-align:center' value='" + TextBox8.Text + "'/>                                                                        " +
                //"            <input name='TextBox9' type='text' maxlength='1' id='TextBox9' style='width:16px;text-align:center' value='" + TextBox9.Text + "'/>                                                                        " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td align='center' style='width: auto'><b>Fecha:</b></td>                                                                                                                                                                      " +
    "        <td align='center' style='width: auto'>                                                                                                                                                                                        " +
    "            <input name='TextBox10' type='text' id='TextBox10' style='width:125px;text-align:center' value='" + TextBox10.Text + "'/>                                                                     " +
                //"            <input name='TextBox11' type='text' maxlength='1' id='TextBox11' style='width:16px;text-align:center' value='" + TextBox11.Text + "'/>                                                                     " +
                //"            <input name='TextBox12' type='text' maxlength='1' id='TextBox12' style='width:16px;text-align:center' value='" + TextBox12.Text + "'/>                                                                     " +
                //"            <input name='TextBox13' type='text' maxlength='1' id='TextBox13' style='width:16px;text-align:center' value='" + TextBox13.Text + "'/>                                                                     " +
                //"            <span id='Label3' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                                    " +
                //"            <input name='TextBox14' type='text' maxlength='1' id='TextBox14' style='width:16px;text-align:center' value='" + TextBox14.Text + "'/>                                                                     " +
                //"            <input name='TextBox15' type='text' maxlength='1' id='TextBox15' style='width:16px;text-align:center' value='" + TextBox15.Text + "'/>                                                                     " +
                //"            <span id='Label4' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                                    " +
                //"            <input name='TextBox16' type='text' maxlength='1' id='TextBox16' style='width:16px;text-align:center' value='" + TextBox16.Text + "'/>                                                                     " +
                //"            <input name='TextBox17' type='text' maxlength='1' id='TextBox17' style='width:16px;text-align:center' value='" + TextBox17.Text + "'/>                                                                     " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td align='center' style='width: auto'><b>Hora:</b></td>                                                                                                                                                                       " +
    "        <td align='center' style='width: auto'>                                                                                                                                                                                        " +
    "            <input name='TextBox20' type='text' id='TextBox20' style='width:67px;text-align:center' value='" + TextBox20.Text + "'/>                                                                     " +
                //"            <input name='TextBox21' type='text' maxlength='1' id='TextBox21' style='width:16px;text-align:center' value='" + TextBox21.Text + "'/>                                                                     " +
                //"             <span id='Label5' style='font-size:XX-Large;font-weight:bold;'>:</span>                                                                                                                                                   " +
                //"            <input name='TextBox23' type='text' maxlength='1' id='TextBox23' style='width:16px;text-align:center' value='" + TextBox23.Text + "'/>                                                                     " +
                //"            <input name='TextBox24' type='text' maxlength='1' id='TextBox24' style='width:16px;text-align:center' value='" + TextBox24.Text + "'/>                                                                     " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "</table>                                                                                                                                                                                                                               " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td colspan='6'><b>INFORMACI&#211N DEL PRESTADOR</b></td>                                                                                                                                                            " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='width: auto'><b>Nombre</b></td>                                                                                                                                                                                     " +
    "        <td><b>NIT</b></td>                                                                                                                                                                                                            " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox25' type='text' value='X' readonly='readonly' id='TextBox25' disabled='disabled' style='text-decoration:none;width:16px;text-align:center' value='" + textoTilde(TextBox25.Text.ToUpper().Trim()) + "'/>            " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td>&nbsp;&nbsp;&nbsp;</td>                                                                                                                                                                                                    " +
    "        <td colspan='2'>                                                                                                                                                                                                               " +
    "            <input name='TextBox26' type='text' value='8' readonly='readonly' id='TextBox26' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox26.Text) + "'/>                                 " +
    "            <input name='TextBox27' type='text' value='9' readonly='readonly' id='TextBox27' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox27.Text) + "'/>                                 " +
    "            <input name='TextBox28' type='text' value='0' readonly='readonly' id='TextBox28' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox28.Text) + "'/>                                 " +
    "            <input name='TextBox29' type='text' value='9' readonly='readonly' id='TextBox29' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox29.Text) + "'/>                                 " +
    "            <input name='TextBox30' type='text' value='8' readonly='readonly' id='TextBox30' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox30.Text) + "'/>                                 " +
    "            <input name='TextBox31' type='text' value='2' readonly='readonly' id='TextBox31' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox31.Text) + "'/>                                 " +
    "            <input name='TextBox32' type='text' value='6' readonly='readonly' id='TextBox32' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox32.Text) + "'/>                                 " +
    "            <input name='TextBox33' type='text' value='0' readonly='readonly' id='TextBox33' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox33.Text) + "'/>                                 " +
    "            <input name='TextBox34' type='text' value='8' readonly='readonly' id='TextBox34' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox34.Text) + "'/>                                 " +
    "            <input name='TextBox35' type='text' readonly='readonly' id='TextBox35' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox35.Text) + "'/>                                           " +
    "            <input name='TextBox36' type='text' value='-' readonly='readonly' id='TextBox36' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox36.Text) + "'/>                                 " +
    "            <input name='TextBox37' type='text' value='1' readonly='readonly' id='TextBox37' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox37.Text) + "'/>                                 " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox38' type='text' value='CORPORACION PARA ESTUDIOS EN SALUD CLINICA CES' readonly='readonly' id='TextBox38' disabled='disabled' style='width:566px;' />                                                  " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td><b>CC</b></td>                                                                                                                                                                                                             " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox39' type='text' maxlength='1' id='TextBox39' style='width:16px;text-align:center' value='" + textoTilde(TextBox39.Text) + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td>Numero:</td>                                                                                                                                                                                                               " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox41' type='text' id='TextBox41' style='width:226px;' value='" + textoTilde(TextBox41.Text) + "'/>                                                                                                                   " +
    "            <span id='Label7' style='font-size:Medium;font-weight:normal;'></span>                                                                                                                                                   " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "</table>                                                                                                                                                                                                                               " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='width: 64px'><b>C&#243digo:</b></td>                                                                                                                                                                                    " +
    "        <td style='width: 326px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox42' type='text' value='0' readonly='readonly' id='TextBox42' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox42.Text) + "'/>                                 " +
    "            <input name='TextBox43' type='text' value='5' readonly='readonly' id='TextBox43' disabled='disabled' style='width:16px;text-align:center' value='" + textoTilde(TextBox43.Text) + "'/>                                 " +
    "            <input name='TextBox44' type='text' value='0' readonly='readonly' id='TextBox44' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox44.Text + "'/>                                 " +
    "            <input name='TextBox45' type='text' value='0' readonly='readonly' id='TextBox45' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox45.Text + "'/>                                 " +
    "            <input name='TextBox46' type='text' value='1' readonly='readonly' id='TextBox46' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox46.Text + "' />                                " +
    "            <input name='TextBox47' type='text' value='0' readonly='readonly' id='TextBox47' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox47.Text + "'/>                                 " +
    "            <input name='TextBox48' type='text' value='2' readonly='readonly' id='TextBox48' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox48.Text + "'/>                                 " +
    "            <input name='TextBox49' type='text' value='1' readonly='readonly' id='TextBox49' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox49.Text + "'/>                                 " +
    "            <input name='TextBox50' type='text' value='2' readonly='readonly' id='TextBox50' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox50.Text + "'/>                                 " +
    "            <input name='TextBox51' type='text' value='4' readonly='readonly' id='TextBox51' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox51.Text + "'/>                                 " +
    "            <input name='TextBox52' type='text' readonly='readonly' id='TextBox52' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox52.Text + "'/>                                           " +
    "            <input name='TextBox53' type='text' readonly='readonly' id='TextBox53' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox53.Text + "'/>                                           " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td><b>Direcci&#243n Prestador:</b></td>                                                                                                                                                                                           " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td rowspan='2' style='width: 64px'><b>Tel&#233fono:</b></td>                                                                                                                                                                      " +
    "        <td style='width: 191px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox54' type='text' value='4' readonly='readonly' id='TextBox54' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox54.Text + "'/>                                 " +
    "            <input name='TextBox55' type='text' readonly='readonly' id='TextBox55' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox55.Text + "'/>                                           " +
    "            <input name='TextBox56' type='text' readonly='readonly' id='TextBox56' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox56.Text + "'/>                                           " +
    "            <input name='TextBox57' type='text' readonly='readonly' id='TextBox57' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox57.Text + "'/>                                           " +
    "            <input name='TextBox58' type='text' readonly='readonly' id='TextBox58' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox58.Text + "'/>                                           " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='width: 277px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox59' type='text' value='5' readonly='readonly' id='TextBox59' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox59.Text + "'/>                                 " +
    "            <input name='TextBox60' type='text' value='7' readonly='readonly' id='TextBox60' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox60.Text + "'/>                                 " +
    "            <input name='TextBox61' type='text' value='6' readonly='readonly' id='TextBox61' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox61.Text + "'/>                                 " +
    "            <input name='TextBox62' type='text' value='7' readonly='readonly' id='TextBox62' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox62.Text + "'/>                                 " +
    "            <input name='TextBox63' type='text' value='2' readonly='readonly' id='TextBox63' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox63.Text + "'/>                                 " +
    "            <input name='TextBox64' type='text' value='7' readonly='readonly' id='TextBox64' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox64.Text + "'/>                                 " +
    "            <input name='TextBox65' type='text' value='2' readonly='readonly' id='TextBox65' disabled='disabled' style='width:16px;text-align:center' value='" + TextBox65.Text + "'/>                                 " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td colspan='9'>                                                                                                                                                                                                               " +
    "            <input name='TextBox66' type='text' id='TextBox66' style='width:563px;' value='" + TextBox66.Text + "'/>                                                                                                                   " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='width: 191px' align='center'>Indicativo</td>                                                                                                                                                                        " +
    "        <td style='width: 277px' align='center'>N&#250mero</td>                                                                                                                                                                            " +
    "        <td style='width: auto'><b>Departamento:</b></td>                                                                                                                                                                              " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox67' type='text' value='ANTIOQUIA' readonly='readonly' id='TextBox67' disabled='disabled' style='width:130px;' value='" + textoTilde(TextBox67.Text) + "'/>                                                         " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td bgcolor='Gray'>                                                                                                                                                                                                            " +
    "            &nbsp;</td>                                                                                                                                                                                                                " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td><b>Municipio:</b></td>                                                                                                                                                                                                     " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox68' type='text' value='MEDELL&#205N' readonly='readonly' id='TextBox68' disabled='disabled' style='width:130px;' value='" + textoTilde(TextBox68.Text) + "'/>                                                          " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 26px;'><b>ENTIDAD A LA QUE SE LE SOLICITA (Pagador)</b></td>                                                                                                                                                " +
    "        <td style='height: 26px'>&nbsp;<b>C&#211DIGO:</b></td>                                                                                                                                                                             " +
    "        <td style='height: 26px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox69' type='text' maxlength='1' id='TextBox69' style='width:99px;text-align:center' value='" + TextBox69.Text + "'/>                                                                     " +
                //"            <input name='TextBox70' type='text' maxlength='1' id='TextBox70' style='width:16px;text-align:center' value='" + TextBox70.Text + "'/>                                                                     " +
                //"            <input name='TextBox71' type='text' maxlength='1' id='TextBox71' style='width:16px;text-align:center' value='" + TextBox71.Text + "'/>                                                                     " +
                //"            <input name='TextBox72' type='text' maxlength='1' id='TextBox72' style='width:16px;text-align:center' value='" + TextBox72.Text + "'/>                                                                     " +
                //"            <input name='TextBox73' type='text' maxlength='1' id='TextBox73' style='width:16px;text-align:center' value='" + TextBox73.Text + "'/>                                                                     " +
                //"            <input name='TextBox74' type='text' maxlength='1' id='TextBox74' style='width:16px;text-align:center' value='" + TextBox74.Text + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td colspan='12' align='center'><B>DATOS DEL PACIENTE</b></td>                                                                                                                                                                 " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td colspan='3'>                                                                                                                                                                                                               " +
    "            <input name='TextBox75' type='text' id='TextBox75' style='width:233px;' value='" + textoTilde(TextBox75.Text) + "'/>                                                                                                                   " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td colspan='3'>                                                                                                                                                                                                               " +
    "            <input name='TextBox76' type='text' id='TextBox76' style='width:233px;' value='" + textoTilde(TextBox76.Text) + "'/>                                                                                                                   " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td colspan='3'>                                                                                                                                                                                                               " +
    "            <input name='TextBox77' type='text' id='TextBox77' style='width:233px;' value='" + textoTilde(TextBox77.Text) + "'/>                                                                                                                   " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td colspan='3'>                                                                                                                                                                                                               " +
    "            <input name='TextBox78' type='text' id='TextBox78' style='width:233px;' value='" + TextBox78.Text + "'/>                                                                                                                   " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='3'><b>Primer Apellido</b></td>                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='3'><b>Segundo Apellido</b></td>                                                                                                                                                              " +
    "        <td style='height: 23px' colspan='3'><b>Primer Nombre</b></td>                                                                                                                                                                 " +
    "        <td style='height: 23px' colspan='3'><b>Segundo Nombre</b></td>                                                                                                                                                                " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td colspan='12'><b>Tipo Documento de Identificaci&#243n</b></td>                                                                                                                                                                  " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox79' type='text' maxlength='1' id='TextBox79' style='width:16px;text-align:center' value='" + tipoId[0] + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='width: 145px'>Registro Civil</td>                                                                                                                                                                                   " +
    "        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                                    " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox80' type='text' maxlength='1' id='TextBox80' style='width:16px;text-align:center' value='" + tipoId[4] + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td>Pasaporte</td>                                                                                                                                                                                                             " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td colspan='5'>                                                                                                                                                                                                               " +
    "            <input name='TextBox81' type='text' id='TextBox81' style='width:286px' value='" + TextBox81.Text + "'/>                                                                     " +
                //"            <input name='TextBox82' type='text' maxlength='1' id='TextBox82' style='width:16px;text-align:center' value='" + TextBox82.Text + "'/>                                                                     " +
                //"            <input name='TextBox83' type='text' maxlength='1' id='TextBox83' style='width:16px;text-align:center' value='" + TextBox83.Text + "'/>                                                                     " +
                //"            <input name='TextBox84' type='text' maxlength='1' id='TextBox84' style='width:16px;text-align:center' value='" + TextBox84.Text + "'/>                                                                     " +
                //"            <input name='TextBox85' type='text' maxlength='1' id='TextBox85' style='width:16px;text-align:center' value='" + TextBox85.Text + "'/>                                                                     " +
                //"            <input name='TextBox86' type='text' maxlength='1' id='TextBox86' style='width:16px;text-align:center' value='" + TextBox86.Text + "'/>                                                                     " +
                //"            <input name='TextBox87' type='text' maxlength='1' id='TextBox87' style='width:16px;text-align:center' value='" + TextBox87.Text + "'/>                                                                     " +
                //"            <input name='TextBox88' type='text' maxlength='1' id='TextBox88' style='width:16px;text-align:center' value='" + TextBox88.Text + "'/>                                                                     " +
                //"            <input name='TextBox89' type='text' maxlength='1' id='TextBox89' style='width:16px;text-align:center' value='" + TextBox89.Text + "'/>                                                                     " +
                //"            <input name='TextBox90' type='text' maxlength='1' id='TextBox90' style='width:16px;text-align:center' value='" + TextBox90.Text + "'/>                                                                     " +
                //"            <input name='TextBox91' type='text' maxlength='1' id='TextBox91' style='width:16px;text-align:center' value='" + TextBox91.Text + "'/>                                                                     " +
                //"            <input name='TextBox92' type='text' maxlength='1' id='TextBox92' style='width:16px;text-align:center' value='" + TextBox92.Text + "'/>                                                                     " +
                //"            <input name='TextBox93' type='text' maxlength='1' id='TextBox93' style='width:16px;text-align:center' value='" + TextBox93.Text + "'/>                                                                     " +
                //"            <input name='TextBox94' type='text' maxlength='1' id='TextBox94' style='width:16px;text-align:center' value='" + TextBox94.Text + "'/>                                                                     " +
                //"            <input name='TextBox95' type='text' maxlength='1' id='TextBox95' style='width:16px;text-align:center' value='" + TextBox95.Text + "'/>                                                                     " +
                //"            <input name='TextBox96' type='text' maxlength='1' id='TextBox96' style='width:16px;text-align:center' value='" + TextBox96.Text + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "                                                                                                                                                                                                                                       " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox97' type='text' maxlength='1' id='TextBox97' style='width:16px;text-align:center' value='" + tipoId[1] + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='width: 145px'>Tarjeta de Identidad</td>                                                                                                                                                                             " +
    "        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                                    " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox98' type='text' maxlength='1' id='TextBox98' style='width:16px;text-align:center' value='" + tipoId[5] + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td>Adulto sin Identificaci&#243n</td>                                                                                                                                                                                             " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td align='center' colspan='3'>N&#250mero Documento de Identificaci&#243n</td>                                                                                                                                                         " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox99' type='text' maxlength='1' id='TextBox99' style='width:16px;text-align:center' value='" + tipoId[2] + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='width: 145px'>C&#233dula de Ciudadania</td>                                                                                                                                                                             " +
    "        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                                    " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox100' type='text' maxlength='1' id='TextBox100' style='width:16px;text-align:center' value='" + tipoId[6] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td>Menor sin Identificaci&#243n</td>                                                                                                                                                                                              " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                                    " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox101' type='text' maxlength='1' id='TextBox101' style='width:16px;text-align:center' value='" + tipoId[3] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='width: 145px'>C&#233dula de Extranjer&#237a</td>                                                                                                                                                                             " +
    "        <td colspan='2'>&nbsp;</td>                                                                                                                                                                                                    " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td>&nbsp;</td>                                                                                                                                                                                                                " +
    "        <td><b>Fecha de Nacimiento:</b></td>                                                                                                                                                                                           " +
    "        <td colspan='5'>                                                                                                                                                                                                               " +
    "            <input name='TextBox102' type='text' id='TextBox102' style='width:141px' value='" + TextBox102.Text + "'/>                                                                  " +
                //"            <input name='TextBox103' type='text' maxlength='1' id='TextBox103' style='width:16px;text-align:center' value='" + TextBox103.Text + "'/>                                                                  " +
                //"            <input name='TextBox104' type='text' maxlength='1' id='TextBox104' style='width:16px;text-align:center' value='" + TextBox104.Text + "'/>                                                                  " +
                //"            <input name='TextBox105' type='text' maxlength='1' id='TextBox105' style='width:16px;text-align:center' value='" + TextBox105.Text + "'/>                                                                  " +
                //"            <span id='Label15' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                                   " +
                //"            <input name='TextBox106' type='text' maxlength='1' id='TextBox106' style='width:16px;text-align:center' value='" + TextBox106.Text + "'/>                                                                  " +
                //"            <input name='TextBox107' type='text' maxlength='1' id='TextBox107' style='width:16px;text-align:center' value='" + TextBox107.Text + "'/>                                                                  " +
                //"            <span id='Label24' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                                   " +
                //"            <input name='TextBox108' type='text' maxlength='1' id='TextBox108' style='width:16px;text-align:center' value='" + TextBox108.Text + "'/>                                                                  " +
                //"            <input name='TextBox109' type='text' maxlength='1' id='TextBox109' style='width:16px;text-align:center' value='" + TextBox109.Text + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td><b>Direcci&#243n de Residencia Habitual:</b></td>                                                                                                                                                                              " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox112' type='text' id='TextBox112' style='width:419px;' value='" + textoTilde(TextBox112.Text) + "'/>                                                                                                                " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td><b>Tel&#233fono:</b></td>                                                                                                                                                                                                      " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox113' type='text' maxlength='1' id='TextBox113' style='width:152px' value='" + TextBox113.Text + "'/>                                                                  " +
                //"            <input name='TextBox114' type='text' maxlength='1' id='TextBox114' style='width:16px;text-align:center' value='" + TextBox114.Text + "'/>                                                                  " +
                //"            <input name='TextBox115' type='text' maxlength='1' id='TextBox115' style='width:16px;text-align:center' value='" + TextBox115.Text + "'/>                                                                  " +
                //"            <input name='TextBox116' type='text' maxlength='1' id='TextBox116' style='width:16px;text-align:center' value='" + TextBox116.Text + "'/>                                                                  " +
                //"            <input name='TextBox117' type='text' maxlength='1' id='TextBox117' style='width:16px;text-align:center' value='" + TextBox117.Text + "'/>                                                                  " +
                //"            <input name='TextBox118' type='text' maxlength='1' id='TextBox118' style='width:16px;text-align:center' value='" + TextBox118.Text + "'/>                                                                  " +
                //"            <input name='TextBox119' type='text' maxlength='1' id='TextBox119' style='width:16px;text-align:center' value='" + TextBox118.Text + "'/>                                                                  " +
                //"            <input name='TextBox120' type='text' maxlength='1' id='TextBox120' style='width:16px;text-align:center' value='" + TextBox120.Text + "'/>                                                                  " +
                //"            <input name='TextBox121' type='text' maxlength='1' id='TextBox121' style='width:16px;text-align:center' value='" + TextBox121.Text + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td><b>Departamento:</b></td>                                                                                                                                                                                                  " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox123' type='text' id='TextBox123' style='width:240px;' value='" + textoTilde(TextBox123.Text) + "'/>                                                                                                                " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td><b>Municipio:</b></td>                                                                                                                                                                                                     " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox124' type='text' id='TextBox124' style='width:240px;' value='" + textoTilde(TextBox124.Text) + "'/>                                                                                                                " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td colspan='8' style='height: 23px'><b>Cobertura en Salud</b></td>                                                                                                                                                            " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox137' type='text' maxlength='1' id='TextBox137' style='width:16px;text-align:center' value='" + cobertura[0] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>R&#233gimen Contributivo</td>                                                                                                                                                                             " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox138' type='text' maxlength='1' id='TextBox138' style='width:16px;text-align:center' value='" + cobertura[1] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>R&#233gimen Subsidiado - Parcial</td>                                                                                                                                                                     " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox139' type='text' maxlength='1' id='TextBox139' style='width:16px;text-align:center' value='" + cobertura[2] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Poblaci&#243n Pobre No Asegurada Sin SISBEN</td>                                                                                                                                                          " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox140' type='text' maxlength='1' id='TextBox140' style='width:16px;text-align:center' value='" + cobertura[3] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Plan Adicional de Salud</td>                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox141' type='text' maxlength='1' id='TextBox141' style='width:16px;text-align:center' value='" + cobertura[4] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>R&#233gimen Subsidiado - Total</td>                                                                                                                                                                       " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox142' type='text' maxlength='1' id='TextBox142' style='width:16px;text-align:center' value='" + cobertura[5] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Poblaci&#243n Pobre No Asegurada Con SISBEN</td>                                                                                                                                                          " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox143' type='text' maxlength='1' id='TextBox143' style='width:16px;text-align:center' value='" + cobertura[6] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Desplazado</td>                                                                                                                                                                                       " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox144' type='text' maxlength='1' id='TextBox144' style='width:16px;text-align:center' value='" + cobertura[7] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Otro</td>                                                                                                                                                                                             " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px' align='center' colspan='10'><B>INFORMACI&#211N DE LA ATENCI&#211N</B></td>                                                                                                                                    " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='2'><b>Origen de la atenci&#243n</b></td>                                                                                                                                                         " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px' colspan='2'>&nbsp;</td>                                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='2'>&nbsp;</td>                                                                                                                                                                               " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox145' type='text' maxlength='1' id='TextBox145' style='width:16px;text-align:center' value='" + Atencion[0] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Enfermedad General</td>                                                                                                                                                                               " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox146' type='text' maxlength='1' id='TextBox146' style='width:16px;text-align:center' value='" + Atencion[1] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Accidente de trabajo</td>                                                                                                                                                                             " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox147' type='text' maxlength='1' id='TextBox147' style='width:16px;text-align:center' value='" + Atencion[2] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Evento Catastr&#243fico</td>                                                                                                                                                                              " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            &nbsp;</td>                                                                                                                                                                                                                " +
    "        <td style='height: 23px'><b>Clasificaci&#243n triage</b></td>                                                                                                                                                                  " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox149' type='text' maxlength='1' id='TextBox149' style='width:16px;text-align:center' value='" + Triage[0] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'><B>I</B></td>                                                                                                                                                                                          " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox154' type='text' maxlength='1' id='TextBox154' style='width:16px;text-align:center' value='" + Triage[1] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'><B>II</B></td>    " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox22' type='text' maxlength='1' id='TextBox22' style='width:16px;text-align:center' value='" + Triage[2] + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'><B>III</B></td>                                                                                                                                                                                         " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='txtTriage4' type='text' maxlength='1' id='txtTriage4' style='width:16px;text-align:center' value='" + Triage[3] + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'><B>IV</B></td>                                                                                                                                                                                         " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='txtTriage5' type='text' maxlength='1' id='txtTriage5' style='width:16px;text-align:center' value='" + Triage[4] + "'/>                                                                     " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'><B>V</B></td>                                                                                                                                                                                         " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox150' type='text' maxlength='1' id='TextBox150' style='width:16px;text-align:center' value='" + Atencion[3] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Enfermedad profesional</td>                                                                                                                                                                           " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox151' type='text' maxlength='1' id='TextBox151' style='width:16px;text-align:center' value='" + Atencion[4] + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>Accidente de Tr&#225nsito</td>                                                                                                                                                                            " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            &nbsp;</td>                                                                                                                                                                                                                " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "    </tr>                                                                                                                                                                                                                              " +
    "                                                                                                                                                                                                                                       " +
    "                                                                                                                                                                                                                                       " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "      <table style='width: 975px'>                                                                                                                                                                                                     " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='8'><b>Ingreso a UrgenciaS</b></td>                                                                                                                                                            " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td align='center' style='width: auto'><b>Fecha:</b></td>                                                                                                                                                                      " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox18' type='text' id='TextBox18' style='width:141px;text-align:center' value='" + TextBox18.Text + "'/>                                                                     " +
                //"            <input name='TextBox131' type='text' maxlength='1' id='TextBox131' style='width:16px;text-align:center' value='" + TextBox131.Text + "'/>                                                                  " +
                //"            <input name='TextBox132' type='text' maxlength='1' id='TextBox132' style='width:16px;text-align:center' value='" + TextBox132.Text + "'/>                                                                  " +
                //"            <input name='TextBox133' type='text' maxlength='1' id='TextBox133' style='width:16px;text-align:center' value='" + TextBox133.Text + "'/>                                                                  " +
                //"            <span id='Label1' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                                    " +
                //"            <input name='TextBox134' type='text' maxlength='1' id='TextBox134' style='width:16px;text-align:center' value='" + TextBox134.Text + "'/>                                                                  " +
                //"            <input name='TextBox135' type='text' maxlength='1' id='TextBox135' style='width:16px;text-align:center' value='" + TextBox135.Text + "'/>                                                                  " +
                //"            <span id='Label2' style='font-size:XX-Large;font-weight:bold;'>-</span>                                                                                                                                                    " +
                //"            <input name='TextBox136' type='text' maxlength='1' id='TextBox136' style='width:16px;text-align:center' value='" + TextBox136.Text + "'/>                                                                  " +
                //"            <input name='TextBox148' type='text' maxlength='1' id='TextBox148' style='width:16px;text-align:center' value='" + TextBox148.Text + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td align='center' style='width: auto'><b>Hora:</b></td>                                                                                                                                                                       " +
    "        <td align='center' style='width: auto'>                                                                                                                                                                                        " +
    "            <input name='TextBox19' type='text' id='TextBox19' style='width:67px;text-align:center' value='" + TextBox19.Text + "'/>                                                                     " +
                //"            <input name='TextBox40' type='text' maxlength='1' id='TextBox40' style='width:16px;text-align:center' value='" + TextBox40.Text + "'/>                                                                     " +
                //"             <span id='Label6' style='font-size:XX-Large;font-weight:bold;'>:</span>                                                                                                                                                   " +
                //"            <input name='TextBox152' type='text' maxlength='1' id='TextBox152' style='width:16px;text-align:center' value='" + TextBox152.Text + "'/>                                                                  " +
                //"            <input name='TextBox153' type='text' maxlength='1' id='TextBox153' style='width:16px;text-align:center' value='" + TextBox153.Text + "'/>                                                                  " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td style='height: 23px' align='right'>Paciente viene remitido</td>                                                                                                                                                            " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox129' type='text' maxlength='1' id='TextBox129' style='width:16px;text-align:center' value='" + Remitido[0] + "'/>                                                                  " +
    "         </td>                                                                                                                                                                                                                         " +
    "         <td style='height: 23px'>SI</td>                                                                                                                                                                                              " +
    "                <td style='height: 23px'>                                                                                                                                                                                              " +
    "            <input name='TextBox122' type='text' maxlength='1' id='TextBox122' style='width:16px;text-align:center' value='" + Remitido[1] + "'/>                                                                  " +
    "         </td>                                                                                                                                                                                                                         " +
    "         <td style='height: 23px'>NO</td>                                                                                                                                                                                              " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='3'>Nombre del prestador de servicio de salud que remite</td>                                                                                                                                 " +
    "                                                                                                                                                                                                                                       " +
    "        <td style='height: 23px' align='right' colspan='1'>C&#243digo:</td>                                                                                                                                                                " +
    "        <td style='height: 23px' colspan='7'>                                                                                                                                                                                          " +
    "            <input name='TextBox449' type='text' id='TextBox449' style='width:160px' value='" + TextBox449.Text + "'/>                                                                  " +
                //"            <input name='TextBox450' type='text' maxlength='1' id='TextBox450' style='width:16px;text-align:center' value='" + TextBox450.Text + "'/>                                                                  " +
                //"            <input name='TextBox451' type='text' maxlength='1' id='TextBox451' style='width:16px;text-align:center' value='" + TextBox451.Text + "'/>                                                                  " +
                //"            <input name='TextBox452' type='text' maxlength='1' id='TextBox452' style='width:16px;text-align:center' value='" + TextBox452.Text + "'/>                                                                  " +
                //"            <input name='TextBox453' type='text' maxlength='1' id='TextBox453' style='width:16px;text-align:center' value='" + TextBox453.Text + "'/>                                                                  " +
                //"            <input name='TextBox454' type='text' maxlength='1' id='TextBox454' style='width:16px;text-align:center' value='" + TextBox454.Text + "'/>                                                                  " +
                //"            <input name='TextBox455' type='text' maxlength='1' id='TextBox455' style='width:16px;text-align:center' value='" + TextBox455.Text + "'/>                                                                  " +
                //"            <input name='TextBox456' type='text' maxlength='1' id='TextBox456' style='width:16px;text-align:center' value='" + TextBox456.Text + "'/>                                                                  " +
                //"            <input name='TextBox457' type='text' maxlength='1' id='TextBox457' style='width:16px;text-align:center' value='" + TextBox457.Text + "'/>                                                                  " +
                //"            <input name='TextBox458' type='text' maxlength='1' id='TextBox458' style='width:16px;text-align:center' value='" + TextBox458.Text + "'/>                                                                  " +
                //"            <input name='TextBox459' type='text' maxlength='1' id='TextBox459' style='width:16px;text-align:center' value='" + TextBox459.Text + "'/>                                                                  " +
                //"            <input name='TextBox460' type='text' maxlength='1' id='TextBox460' style='width:16px;text-align:center' value='" + TextBox460.Text + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "                                                                                                                                                                                                                                       " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='12'>                                                                                                                                                                                         " +
    "            <input name='TextBox461' type='text' id='TextBox461' style='width:854px;' value='" + textoTilde(TextBox461.Text.ToUpper().Trim()) + "'/>                                                                                                                " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td><b>Departamento:</b></td>                                                                                                                                                                                                  " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox125' type='text' id='TextBox125' style='width:240px;' value='" + textoTilde(Departamento) + "'/>                                                                                                                " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td><b>Municipio:</b></td>                                                                                                                                                                                                     " +
    "        <td>                                                                                                                                                                                                                           " +
    "            <input name='TextBox126' type='text' id='TextBox126' style='width:240px;' value='" + textoTilde(Municipio) + "'/>                                                                                                                " +
    "        </td>                                                                                                                                                                                                                          " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "        <td bgcolor='Gray'>&nbsp;</td>                                                                                                                                                                                                 " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "                                                                                                                                                                                                                                       " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='8'><b>Motivo de consulta</b></td>                                                                                                                                                            " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='12'>                                                                                                                                                                                         " +
    "            <textarea name='TextBox462' rows='2' cols='20' id='TextBox462' style='height:" + tam + "px;width:951px;'value=''>" + textoTilde(TextBox462.Text.ToUpper().Trim()) + "</textarea>                                                                                 " +
    "            </td>                                                                                                                                                                                                                      " +
    "                                                                                                                                                                                                                                       " +
    "    </tr>                                                                                                                                                                                                                              " +
    "</table>                                                                                                                                                                                                                               " +
    "                                                                                                                                                                                                                                       " +
    "                                                                                                                                                                                                                                       " +
    "                                                                                                                                                                                                                                       " +
    "                                                                                                                                                                                                                                       " +
    "    <table style='width: 975px'>                                                                                                                                                                                                       " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'><b>Impresi&#243n Diagn&#243stica</b></td>                                                                                                                                                                     " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>C&#243digo CIE10</td>                                                                                                                                                                                     " +
    "        <td style='height: 23px'>Descripci&#243n</td>                                                                                                                                                                                      " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'>Diagn&#243stico Principal</td>                                                                                                                                                                            " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox387' type='text'  id='TextBox387' style='width:85px' value='" + textoTilde(TextBox387.Text) + "'/>                                                                  " +
                //"            <input name='TextBox388' type='text' maxlength='1' id='TextBox388' style='width:16px;text-align:center' value='" + TextBox388.Text + "'/>                                                                  " +
                //"            <input name='TextBox389' type='text' maxlength='1' id='TextBox389' style='width:16px;text-align:center' value='" + TextBox389.Text + "'/>                                                                  " +
                //"            <input name='TextBox390' type='text' maxlength='1' id='TextBox390' style='width:16px;text-align:center' value='" + TextBox390.Text + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox399' type='text' id='TextBox399' style='width:672px;' value='" + textoTilde(TextBox399.Text) + "'/>                                                                                                                " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'>Diagn&#243stico Relacionado 1</td>                                                                                                                                                                        " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox391' type='text' id='TextBox391' style='width:85px' value='" + textoTilde(TextBox391.Text) + "'/>                                                                  " +
                //"            <input name='TextBox392' type='text' maxlength='1' id='TextBox392' style='width:16px;text-align:center' value='" + TextBox392.Text + "'/>                                                                  " +
                //"            <input name='TextBox393' type='text' maxlength='1' id='TextBox393' style='width:16px;text-align:center' value='" + TextBox393.Text + "'/>                                                                  " +
                //"            <input name='TextBox394' type='text' maxlength='1' id='TextBox394' style='width:16px;text-align:center' value='" + TextBox394.Text + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox400' type='text' id='TextBox400' style='width:672px;' value='" + textoTilde(TextBox400.Text) + "' />                                                                                                                                               " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'>Diagn&#243stico Relacionado 2</td>                                                                                                                                                                        " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox395' type='text'id='TextBox395' style='width:85px' value='" + textoTilde(TextBox395.Text) + "'/>                                                                  " +
                //"            <input name='TextBox396' type='text' maxlength='1' id='TextBox396' style='width:16px;text-align:center' value='" + TextBox396.Text + "'/>                                                                  " +
                //"            <input name='TextBox397' type='text' maxlength='1' id='TextBox397' style='width:16px;text-align:center' value='" + TextBox397.Text + "'/>                                                                  " +
                //"            <input name='TextBox398' type='text' maxlength='1' id='TextBox398' style='width:16px;text-align:center' value='" + TextBox398.Text + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox401' type='text' id='TextBox401' style='width:672px;' value='" + textoTilde(TextBox401.Text) + "'/>                                                                                                                " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "   <tr>                                                                                                                                                                                                                                " +
    "        <td style='height: 23px'>Diagn&#243stico Relacionado 3</td>                                                                                                                                                                        " +
    "        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox127' type='text' id='TextBox127' style='width:85px' value='" + textoTilde(TextBox127.Text) + "'/>                                                                  " +
                //"            <input name='TextBox128' type='text' maxlength='1' id='TextBox128' style='width:16px;text-align:center' value='" + TextBox128.Text + "'/>                                                                  " +
                //"            <input name='TextBox130' type='text' maxlength='1' id='TextBox130' style='width:16px;text-align:center' value='" + TextBox130.Text + "'/>                                                                  " +
                //"            <input name='TextBox155' type='text' maxlength='1' id='TextBox155' style='width:16px;text-align:center' value='" + TextBox155.Text + "'/>                                                                  " +
    "            </td>                                                                                                                                                                                                                      " +
    "        <td style='height: 23px'>                                                                                                                                                                                                      " +
    "            <input name='TextBox156' type='text' id='TextBox156' style='width:672px;' value='" + textoTilde(TextBox156.Text) + "'/>                                                                                                                " +
    "        </td>                                                                                                                                                                                                                          " +
    "    </tr>                                                                                                                                                                                                                              " +
    "                                                                                                                                                                                                                                       " +
    "    </table>                                                                                                                                                                                                                           " +
    "      <table style='width: 975px'>                                                                                                                                                                                                     " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px' colspan='2'><b>Destino del paciente</b></td>                                                                                                                                                          " +
    "        <td style='height: 23px'></td>                                                                                                                                                                                                 " +
    "        <td style='height: 23px'></td>                                                                                                                                                                                                 " +
    "        <td style='height: 23px'></td>                                                                                                                                                                                                 " +
    "        <td style='height: 23px'></td>                                                                                                                                                                                                 " +
    "                                                                                                                                                                                                                                       " +
    "     </tr>                                                                                                                                                                                                                             " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'><input name='TextBox157' type='text' maxlength='1' id='TextBox157' style='width:16px;text-align:center' value='" + Destino[0] + "'/></td>                                        " +
    "        <td style='height: 23px'>Domicilio</td>                                                                                                                                                                                        " +
    "        <td style='height: 23px'><input name='TextBox158' type='text' maxlength='1' id='TextBox158' style='width:16px;text-align:center' value='" + Destino[1] + "'/></td>                                        " +
    "        <td style='height: 23px'>Internaci&#243n</td>                                                                                                                                                                                      " +
    "        <td style='height: 23px'><input name='TextBox159' type='text' maxlength='1' id='TextBox159' style='width:16px;text-align:center' value='" + Destino[2] + "'/></td>                                        " +
    "        <td style='height: 23px'>Contraremisi&#243n</td>                                                                                                                                                                                   " +
    "                                                                                                                                                                                                                                       " +
    "                                                                                                                                                                                                                                       " +
    "    </tr>                                                                                                                                                                                                                              " +
    "    <tr>                                                                                                                                                                                                                               " +
    "        <td style='height: 23px'><input name='TextBox161' type='text' maxlength='1' id='TextBox161' style='width:16px;text-align:center' value='" + Destino[3] + "'/></td>                                        " +
    "        <td style='height: 23px'>Observaci&#243n</td>                                                                                                                                                                                      " +
    "        <td style='height: 23px'><input name='TextBox162' type='text' maxlength='1' id='TextBox162' style='width:16px;text-align:center' value='" + Destino[4] + "'/></td>                                        " +
    "        <td style='height: 23px'>Remisi&#243n</td>                                                                                                                                                                                         " +
    "        <td style='height: 23px'><input name='TextBox463' type='text' maxlength='1' id='TextBox463' style='width:16px;text-align:center' value='" + Destino[5] + "'/></td>                                        " +
    "        <td style='height: 23px'>Otro</td>                                                                                                                                                                                             " +
    "                                                                                                                                                                                                                                       " +
    "                                                                                                                                                                                                                                       " +
    "    </tr>                                                                                                                                                                                                                              " +
    //"    <tr>                                                                                                                                                                                                                               " +
    //"        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    //"        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    //"        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    //"        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    //"        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    //"        <td style='height: 23px'>&nbsp;</td>                                                                                                                                                                                           " +
    //"    </tr>                                                                                                                                                                                                                              " +
    "    </table>                                                                                                                                                                                                                           " +
    "    <table style='width: 975px'>                                                                                                                                                                                            " +
    "    <tr>                                                                                                                                                                                                                    " +
    "        <td style='height: 23px' colspan='12' align='center'><B>INFORMACI&#211N DE LA PERSONA QUE INFORMA</B></td>                                                                                                             " +
    "    </tr>                                                                                                                                                                                                                   " +
    "    <tr>                                                                                                                                                                                                                    " +
    "        <td style='height: 23px' colspan='2'>Nombre de quien informa</td>                                                                                                                                         " +
    "        <td rowspan='3'>Tel&#233fono</td>                                                                                                                                                                                       " +
    "        <td style='height: 23px; width: 111px;'>                                                                                                                                                                            " +
    "            <input name='txtIndicativoSolic' type='text' id='txtIndicativoSolic' style='width:91px;text-align:left' value='" + txtIndicativoSolic.Text + "'/>                                                       " +
    "            </td>                                                                                                                                                                                                           " +
    "        <td style='height: 23px'>                                                                                                                                                                                           " +
    "            <input name='txtNumeroSolic' type='text'  id='txtNumeroSolic' style='width:126px;text-align:left' value='" + txtNumeroSolic.Text + "'/>                                                       " +
    "            </td>                                                                                                                                                                                                           " +
    "        <td style='height: 23px'>                                                                                                                                                                                           " +
    "            <input name='txtExtSolic' type='text' id='txtExtSolic' style='width:85px;text-align:left' value='" + txtExtSolic.Text + "'/>                                                       " +
    "            </td>                                                                                                                                                                                                           " +
    "    </tr>                                                                                                                                                                                                                   " +
    "    <tr>                                                                                                                                                                                                                    " +
    "        <td style='height: 23px' colspan='2'>                                                                                                                                                                               " +
    "            <input name='TextBox402' type='text' id='TextBox402' style='width:476px;' value='" + TextBox402.Text + "'/>                                                                                                     " +
    "        </td>                                                                                                                                                                                                               " +
    "        <td style='height: 23px; width: 111px;' align='center'>Indicativo</td>                                                                                                                                              " +
    "        <td style='height: 23px' align='center'>N&#250mero</td>                                                                                                                                                                 " +
    "        <td style='height: 23px' align='center'>Extensi&#243n</td>                                                                                                                                                              " +
    "    </tr>                                                                                                                                                                                                                   " +
    "    <tr>                                                                                                                                                                                                                    " +
    "        <td style='height: 23px'>Cargo o Actvidad:</td>                                                                                                                                                                     " +
    "        <td style='height: 23px'>                                                                                                                                                                                           " +
    "            <input name='TextBox419' type='text' id='TextBox419' style='width:359px;' value='" + TextBox419.Text + "'/>                                                                                                     " +
    "        </td>                                                                                                                                                                                                               " +
    "        <td style='height: 23px; width: 111px;' align='center'>Tel&#233fono Celular:</td>                                                                                                                                       " +
    "        <td style='height: 23px' colspan='2' align='center'>                                                                                                                                                                " +
    "            <input name='txtCelularSolicita' type='text' id='txtCelularSolicita' style='width:200px;text-align:left' value='" + txtCelularSolicita.Text + "'/>                                                       " +
    "            </td>                                                                                                                                                                                                           " +
    "    </tr>                                                                                                                                                                                                                   " +
    "    <tr>                                                                                                                                                                                                                    " +
    "        <td style='height: 23px;font-size: xx-small;' colspan='6'>MPS-SAS V5.0 2008-07-11</td>                                                                                                                                                   " +
    "    </tr>                                                                                                                                                                                                                   " +
    "</table>";
            return html;
        }
            
        catch
        { return html="";}
        
    }
    protected bool validar()
    {
        bool Valido = true;

        System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
        string[] css = { "form_input", "invalidtxt" };

        if (string.IsNullOrEmpty(rblCobertura.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            //lblValidaNombres.ForeColor = color[1];
            rblCobertura.CssClass = css[1];
        }
        else
        {
            //lblValidaNombres.ForeColor = color[0];
            rblCobertura.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(rblAtencion.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            //lblValidaNombres.ForeColor = color[1];
            rblAtencion.CssClass = css[1];
        }
        else
        {
            //lblValidaNombres.ForeColor = color[0];
            rblAtencion.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(rblRemitido.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            //lblValidaNombres.ForeColor = color[1];
            rblRemitido.CssClass = css[1];
        }
        else
        {
            //lblValidaNombres.ForeColor = color[0];
            rblRemitido.CssClass = css[0];
            if (rblRemitido.SelectedValue == "SI")
            {
                if (string.IsNullOrEmpty(TextBox449.Text.Trim()))
                {
                    Valido = false;
                    //lblValidaApellido.ForeColor = color[1];
                    TextBox449.CssClass = css[1];
                }
                else
                {
                    //lblValidaApellido.ForeColor = color[0];
                    TextBox449.CssClass = css[0];
                }
                if (string.IsNullOrEmpty(TextBox461.Text.Trim()))
                {
                    Valido = false;
                    //lblValidaApellido.ForeColor = color[1];
                    TextBox461.CssClass = css[1];
                }
                else
                {
                    //lblValidaApellido.ForeColor = color[0];
                    TextBox461.CssClass = css[0];
                }
                if (dllDepartamento.SelectedIndex == 0)
                {
                    Valido = false;
                    dllDepartamento.CssClass = css[1];
                }
                else
                {
                    dllDepartamento.CssClass = css[0];
                }
                if (ddlMunicipio.SelectedIndex == 0)
                {
                    Valido = false;
                    ddlMunicipio.CssClass = css[1];
                }
                else
                {
                    ddlMunicipio.CssClass = css[0];
                }
            }
        }
        
        if (string.IsNullOrEmpty(rblDestino.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            //lblValidaNombres.ForeColor = color[1];
            rblDestino.CssClass = css[1];
        }
        else
        {
            //lblValidaNombres.ForeColor = color[0];
            rblDestino.CssClass = css[0];
        }
        return Valido;
    }
    protected void rblRemitido_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (rblRemitido.SelectedValue == "SI")
        {
            TextBox449.Enabled = true;
            TextBox461.Enabled = true;
            dllDepartamento.Enabled = true;
            ddlMunicipio.Enabled = true;
        }
        if (rblRemitido.SelectedValue == "NO")
        {
            TextBox449.Enabled = false;
            TextBox461.Enabled = false;
            dllDepartamento.Enabled = false;
            ddlMunicipio.Enabled = false;
        }
    }
    protected void traerUsuario()
    {
        string msg = "";
        try
        {
            DataRow Fila = new ClinicaCES.Logica.LAnexo2().TraerUsuario(Session["Nick1"].ToString()).Tables[0].Rows[0];
            TextBox402.Text = Fila[0].ToString();
            TextBox419.Text = Fila[1].ToString();
            txtIndicativoSolic.Text = "4";
            txtNumeroSolic.Text = "5767272";
            txtExtSolic.Text = "7258-7377";
        }
        catch
        {
            msg = "75";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        AgregarPrintScript(ViewState["Archivo2"].ToString());
    }
    protected void inactivarControles()
    {
        Button1.Enabled = false;
        rblCobertura.Enabled = false;
        TextBox449.Enabled = false;
        TextBox461.Enabled = false;
        dllDepartamento.Enabled = false;
        ddlMunicipio.Enabled = false;
        rblDestino.Enabled = false;
        rblRemitido.Enabled = false;
        rblAtencion.Enabled = false;
    }
}