﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/MasterNew.master" AutoEventWireup="true" CodeFile="BusquedadAnexoNew1.aspx.cs" Inherits="Anexos_BusquedadAnexoNew1" MaintainScrollPositionOnPostback="true" %>
    <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
        <div class="row">
            <div class="col-md-12">
                <div class="box">
                    <div class="box box-primary">
                        <!--div Título-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-4">
                                <h3 align="center" runat="server" class="box-title">Consentimientos Informados</h3> </div>
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <!--div campo Tipo identificación-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <%--<h5>Tipo de identificación</h5>
                                    <asp:DropDownList ID="ddlTipoId" runat="server" AutoPostBack="True" class="form-control"> </asp:DropDownList>--%>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                        </div>
                        <!--div campo Identificación-->
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <h5>Identificación del paciente</h5>
                                    <asp:TextBox ID="txtId" runat="server" class="form-control" placeholder="Identificación del paciente"></asp:TextBox>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group"> </div>
                            </div>
                        </div>
                        <!--div botón Consultar-->
                        <div class="row">
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label>
                              <asp:CheckBox ID="CheckPaciente" runat="server" Text="Firma cómo paciente" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckPaciente_CheckedChanged" /> </label>
                           <br />
                           <label>
                              <asp:CheckBox ID="CheckRepresentante" runat="server" Text="Firma cómo representante" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckRepresentante_CheckedChanged" /> </label>
                           <br />
                           <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="btn btn-primary" OnClick="btnConsultar_Click" /> </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                  </div>
                    </div>
                </div>
            </div>
        </div>
        <!--div tabla-->
        <div class="box box-primary" id="divtable" runat="server" visible="false">
            <div class="row">
            <div class="box-header with-border">
               <div class="col-md-2">
                  <div class="form-group"> </div>
               </div>
               <div class="col-md-2">
                  <div class="form-group">
                     <h5>Nombre Paciente :</h5> </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <div>
                        <asp:TextBox ID="LblNombre" runat="server" READONLY="true" CausesValidation="True" class="form-control"></asp:TextBox>
                     </div>
                  </div>
               </div>
               <div class="col-md-5">
                  <div class="form-group"> </div>
               </div>
            </div>
         </div>
            <!--*****************************************************************-->
            <div class="row">
                <div class="box-header with-border">
                <div class="col-md-2">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <%--<h5>Consentiento :</h5>--%> </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                            <div class="icheck-material-blue">
                                <label>
                                    <asp:CheckBox ID="chkcons2" runat="server" Text="F-HC-13 Consulta historia clínica" CssClass="icheck-material-blue" AutoPostBack="True"  Visible="true"/> </label>
                                <br />
                                <br />
                                <label>
                                    <asp:CheckBox ID="chkcons3" runat="server" Text="F-HC-7 Procedimientos de enfermería" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="chkcons3_CheckedChanged" Visible="true"/> </label>
                                <br />
                                <br />
                                <label>
                                    <asp:CheckBox ID="chkcons6" runat="server" Text="F-SI-5 Autorización para tratamientos de datos personales" CssClass="icheck-material-blue" AutoPostBack="True"  Visible="true"/> </label>
                                <br />
                                <br />
                                <label>
                                <asp:CheckBox ID="chkcons4" runat="server" Text="F-SH-4 Notificacion Acompañante Permanente" CssClass="icheck-material-blue" AutoPostBack="True"  Visible="true"/> </label>
                                <br />
                                <br />
                                <label>
                                <asp:CheckBox ID="CheckAutorizacionDocencia" runat="server" Text="F-HC-16 Autorización de docencia" CssClass="icheck-material-blue" AutoPostBack="True"  Visible="true" /> </label>
                                </div>
                        </div>
                    </div>                
                <div class="col-md-4">
                    <div class="form-group"> </div>
                </div>
            </div>
            </div>
            <!--*****************************************************************-->
            <div id="divAutorizado" runat="server" visible="false">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <h5>Cédula Autorizado :</h5> </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div>
                            <asp:TextBox ID="txtIdAut" runat="server" class="form-control" placeholder="Cédula Autorizado"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <h5>Nombre Autorizado :</h5> </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div>
                            <asp:TextBox ID="txtNomAut" runat="server" class="form-control" placeholder="Nombre Autorizado"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
            </div>
                </div>
            <!--*****************************************************************-->
            <div class="row" id="divConsideraciones" runat="server" visible="false">
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <h5>Consideraciones :</h5> </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div>
                            <asp:TextBox ID="txtConsideraciones" runat="server" CausesValidation="True" Height="83px" TextMode="MultiLine" class="form-control"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row" id="divAnestesia" runat="server" visible="false">
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <h5>Anestesias :</h5> </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div class="icheck-material-blue">
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaGeneral" runat="server" Text="ANESTESIA GENERAL" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaRegional" runat="server" Text="ANESTESIA REGIONAL O BLOQUEO" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaConductiva" runat="server" Text="ANESTESIA CONDUCTIVA" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br />
                            <label>
                                <asp:CheckBox ID="CheckAnestesiaLocal" runat="server" Text="ANESTESIA LOCAL ASISTIDA" CssClass="icheck-material-blue" /> </label>
                            <br />
                            <br /> </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
            </div>
            <!--*****************************************************************-->
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group"></div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <div>
                            <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-primary" Text="Aceptar" OnClick="btnGuardar_Click" visible="false"/>
                            <asp:Button ID="btnReview" runat="server" CssClass="btn btn-primary" Text="Ver" OnClick="btnReview_Click" />
                            <asp:Button ID="btnCaptureFirm" type="submit" class="btn btn-danger" runat="server" OnClick="Capture_Firma_Click" Text="Capturar firma" Visible="false" /> &nbsp;&nbsp;
                            <label>
                                <asp:CheckBox ID="CheckAprobacionMedico" runat="server" Text="Aprobación médico" CssClass="icheck-material-blue" visible="false" /> </label>
                            <br />
                            <br /> 
                            <label>
                                    <asp:CheckBox ID="chkcons1" runat="server" Text="F-HC-2 Autorización de representante" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="chkcons1_CheckedChanged" /> </label>
                                <br />
                                <br />
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group"> </div>
                </div>
            </div>
        </div>
        <div id="imageBox" class="boxed" style="height:37mm;width:72mm" ;></div>
        <asp:Literal ID="litScript" runat="server"></asp:Literal>
    </asp:Content>