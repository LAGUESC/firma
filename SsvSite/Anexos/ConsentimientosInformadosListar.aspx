﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/MasterNew.master" AutoEventWireup="true" CodeFile="ConsentimientosInformadosListar.aspx.cs" Inherits="Anexos_ConsentimientosInformadosListar" MaintainScrollPositionOnPostback="true" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">





    <script src="<%= Page.ResolveUrl("~/js/jquery-1.8.3.js") %>"></script>
    <script src="<%= Page.ResolveUrl("~/js/Scripts_jquery.dataTables.min.js") %>"></script>

    <script type="text/javascript">

        $(document).ready(function () {

            // Setup Metadata plugin
            $.metadata.setType("class");

            // Setup GridView
            $("table.grid").each(function () {
                var jTbl = $(this);

                if (jTbl.find("tbody>tr>th").length > 0) {
                    jTbl.find("tbody").before("<thead><tr></tr></thead>");
                    jTbl.find("thead:first tr").append(jTbl.find("th"));
                    jTbl.find("tbody tr:first").remove();
                }

                // If GridView has the 'sortable' class and has more than 10 rows
                if (jTbl.hasClass("sortable") && jTbl.find("tbody:first > tr").length > 10) {

                    // Run DataTable on the GridView
                    jTbl.dataTable({
                        sPaginationType: "full_numbers",
                        sDom: '<"top"lf>rt<"bottom"ip>',
                        oLanguage: {
                            sInfoFiltered: "(from _MAX_ entries)",
                            sSearch: ""
                        },
                        aoColumnDefs: [
                            { bSortable: false, aTargets: jTbl.metadata().disableSortCols }
                        ]
                    });
                }
            });
        });

    </script>
    <script>
        $(function () {

            //Date picker
            $( "#<%= txtInicio.ClientID %>" ).datepicker({
                autoclose: true
            });

             //Date picker
            $( "#<%= txtFin.ClientID %>" ).datepicker({
                autoclose: true
            });

            //iCheck for checkbox and radio inputs
            $('input[type="checkbox"].minimal, input[type="radio"].minimal').iCheck({
                checkboxClass: 'icheckbox_minimal-blue',
                radioClass: 'iradio_minimal-blue'
            });
            //Red color scheme for iCheck
            $('input[type="checkbox"].minimal-red, input[type="radio"].minimal-red').iCheck({
                checkboxClass: 'icheckbox_minimal-red',
                radioClass: 'iradio_minimal-red'
            });
            //Flat red color scheme for iCheck
            $('input[type="checkbox"].flat-red, input[type="radio"].flat-red').iCheck({
                checkboxClass: 'icheckbox_flat-blue',
                radioClass: 'iradio_flat-blue'
            });

            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();

            //Timepicker
            $(".timepicker").timepicker({
                showInputs: false
            });
        });
</script>
    <div class="row">
        <div class="col-md-12">
            <div class="box">
                <div class="box box-primary">
                    <!--div Título-->
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <h3 align="center" runat="server" class="box-title">Buscar consentimientos Informados</h3>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:LinkButton ID="LinkButton1" runat="server" style ="float:right;font-size:large;" OnClientClick="REPRODUCTOR();" ><h3 class="fa fa-info-circle"></h3>&nbsp;Ayuda&nbsp;&nbsp;&nbsp;&nbsp;</asp:LinkButton>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <!--div campo Identificación-->
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    <asp:CheckBox ID="CheckIdentificacion" runat="server" Text="Paciente" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckIdentificacion_CheckedChanged" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:TextBox ID="txtId" runat="server" class="form-control" placeholder="Identificación a buscar" Visible="true" ReadOnly="true"></asp:TextBox>
                            </div>
                        </div>
                        <div class="col-md-4" style="left: 0px; top: 0px">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                    <!--div campo usuario del sistema-->
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    <asp:CheckBox ID="CheckUsuario" runat="server" Text="Usuario del sistema" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckUsuario_CheckedChanged" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <asp:TextBox ID="txtUsuario" runat="server" class="form-control" placeholder="Identificación a buscar" Visible="true" ReadOnly="true"></asp:TextBox>                            
                            </div>
                        </div>
                        <div class="col-md-4" style="left: 0px; top: 0px">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>
                    <!--div botón Consultar-->
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    <asp:CheckBox ID="CheckRango" runat="server" Text="Rango de fecha" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckRango_CheckedChanged" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtInicio" runat="server" class="form-control pull-right" ReadOnly="true" placeholder="Fecha Inicio"></asp:TextBox>                                   
                                </div>
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <asp:TextBox ID="txtFin" runat="server" class="form-control pull-right" ReadOnly="true" placeholder="Fecha Fin"></asp:TextBox>
                                </div>
                                 <br />
                                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="btn btn-primary" OnClick="btnConsultar_Click" />&nbsp;&nbsp;&nbsp;
                                <!-- /.input group -->
                            </div>
                        </div>
                        <div class="col-md-4">
                             <div class="form-group">
                                 </div>
            </div>

                    </div>
                    <!--*******************************************************************************-->
                    <%--<div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    <asp:CheckBox ID="CheckMesAnio" runat="server" Text="Mes y Año" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckMesAnio_CheckedChanged" />
                                </label>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">

                                <asp:DropDownList ID="ddlMes" runat="server" CssClass="form-control" Enabled="false">
                                    <asp:ListItem Value="0">----Seleccione----</asp:ListItem>
                                    <asp:ListItem Value="1">Enero</asp:ListItem>
                                    <asp:ListItem Value="2">Febrero</asp:ListItem>
                                    <asp:ListItem Value="3">Marzo</asp:ListItem>
                                    <asp:ListItem Value="4">Abril</asp:ListItem>
                                    <asp:ListItem Value="5">Mayo</asp:ListItem>
                                    <asp:ListItem Value="6">Junio</asp:ListItem>
                                    <asp:ListItem Value="7">Julio</asp:ListItem>
                                    <asp:ListItem Value="8">Agosto</asp:ListItem>
                                    <asp:ListItem Value="9">Septiembre</asp:ListItem>
                                    <asp:ListItem Value="10">Octubre</asp:ListItem>
                                    <asp:ListItem Value="11">Noviembre</asp:ListItem>
                                    <asp:ListItem Value="12">Diciembre</asp:ListItem>
                                </asp:DropDownList>
                                <br />

                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <asp:DropDownList ID="ddlAnio" runat="server" CssClass="form-control" Enabled="false">
                                    <asp:ListItem Value="0">----Seleccione----</asp:ListItem>
                                    <asp:ListItem Value="2019">2019</asp:ListItem>
                                    <asp:ListItem Value="2020">2020</asp:ListItem>
                                    <asp:ListItem Value="2021">2021</asp:ListItem>
                                </asp:DropDownList>
                                <br />
                                <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="btn btn-primary" OnClick="btnConsultar_Click" />&nbsp;&nbsp;&nbsp;
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                            </div>
                        </div>
                    </div>--%>
                     
                    <!--******************************************************************************************************************************************************-->
                </div>
            </div>
        </div>
    </div>
    <div class="box box-primary" id="divtable" runat="server" visible="false">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <asp:GridView ID="gvConsentimientos" runat="server" AutoGenerateColumns="False" CssClass="grid sortable {disableSortCols: [3]}" DataKeyNames="ID_CONSENTIMIENTO_PDF" Width="100%" EnableModelValidation="True">
                        <Columns>
                            <asp:BoundField DataField="ID_CONSENTIMIENTO_PDF" HeaderText="ID" />
                            <asp:BoundField DataField="IDENTIFICACION_PACIENTE" HeaderText="Identificación Paciente" />
                            <asp:BoundField DataField="NOMBRE_PACIENTE" HeaderText="Nombre Paciente" />
                            <asp:BoundField DataField="IDENTIFICACION_USUARIO" HeaderText="Identificación creador" Visible="False" />
                            <asp:BoundField DataField="NOMBRE_USUARIO" HeaderText="Elaborado por:" />
                            <asp:BoundField DataField="FECHA_REGISTRO" HeaderText="Fecha Registro" />
                            <asp:BoundField DataField="CONSENTIMIENTO_HASH" HeaderText="Archivo" Visible="False" />
                            <asp:TemplateField HeaderText="Ver PDF">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox1" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:ImageButton ID="ImgPDF" runat="server" Height="31px" ImageUrl="~/img/Imagenes_pdf.svg" Width="42px" OnClick="ImgPDF_Click" />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Aprobación médico" Visible="False">
                                <EditItemTemplate>
                                    <asp:TextBox ID="TextBox2" runat="server"></asp:TextBox>
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:CheckBox ID="CheckAprobacionMedico" runat="server" Checked='<%# Eval("APROBACION_MEDICO") %>' />
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Center" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle Font-Bold="True" HorizontalAlign="Center" />
                    </asp:GridView>
                    <br />
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                &nbsp;&nbsp;&nbsp;
                                <asp:Button ID="btnGuardar" runat="server" Text="Guardar cambios" CssClass="btn btn-primary" OnClick="btnGuardar_Click" Visible="false" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>

