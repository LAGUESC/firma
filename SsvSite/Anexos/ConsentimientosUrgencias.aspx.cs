﻿using ClinicaCES.Entidades;
using System;
using System.Data;
using System.IO;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Windows.Forms;

[WebService]
public partial class Anexos_ConsentimientosUrgencias : System.Web.UI.Page
{
    #region "ATRIBUTOS"
    string pathDescarga = "";
    string pathDescargaTratamiento = "";
    int Id_Firma = 0;
    string firma = "";
    DataTable dtFirma = null;
    Byte[] FileBufferTratamientoDatos = null;
    #endregion

    #region "CONTROLES"
    protected void Page_Load(object sender, EventArgs e)
    {

        StreamReader reader = new StreamReader(Request.InputStream);
        String signature = Server.UrlDecode(reader.ReadToEnd());
        reader.Close();

        if (signature.StartsWith("bmpObj="))
        {
            drawimgFirm(signature);
        }

    }
    protected void CheckPaciente_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckPaciente.Checked)
        {

            chkcons6.Visible = false;
            CheckRepresentante.Checked = false;
            txtIdAut.Text = "";
            txtNomAut.Text = "";
            divAutorizado.Visible = false;
            btnCaptureFirm.Visible = false;
            btnGuardar.Visible = false;
            CheckUtilizarFirmaBD.Checked = false;
            CheckUtilizarFirmaBD.Visible = false;
        }
        else
        {
            chkcons6.Visible = false;
        }
    }
    protected void CheckRepresentante_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckRepresentante.Checked)
        {

            chkcons6.Visible = false;
            CheckPaciente.Checked = false;
            divAutorizado.Visible = true;
            btnCaptureFirm.Visible = false;
            btnGuardar.Visible = false;
            CheckUtilizarFirmaBD.Checked = false;
            CheckUtilizarFirmaBD.Visible = false;
        }

        else
        {
            chkcons6.Visible = false;
        }
    }
    protected void btnConsultar_Click(object sender, EventArgs e)
    {
        btnGuardar.Visible = false;
        btnCaptureFirm.Visible = false;
        Consultar(txtId.Text);
        Session["Cedula"] = txtId.Text;

    }
    protected void btnGuardar_Click(object sender, EventArgs e)
    {
        if (ValidarDatosHTML())
        {
            try
            {
                string CedUser = Session["Identificacion"].ToString();
                string NombreUser = Session["Nombre"].ToString();


                string cadenaEncriptada = Encrypt.GetMD5(DateTime.Now.ToString());

                string html = retornarHtml();
                var htmlContent = String.Format(html);
                var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                pathDescarga = "../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf";


                htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf"));

                GuardarEvento(txtId.Text);
                GuardarPDF(txtId.Text, CedUser, NombreUser);


                //DirectoryInfo di = new DirectoryInfo("C:\\Users\\LUISAGUDELO\\Desktop\\REPOSCES\\FIRMA\\SsvSite\\AUTPDF\\");
                DirectoryInfo di = new DirectoryInfo("C:\\inetpub\\wwwroot\\Firmas\\AUTPDF");
                foreach (FileInfo file in di.GetFiles())
                {
                    file.Delete();
                }
                foreach (DirectoryInfo dir in di.GetDirectories())
                {
                    dir.Delete(true);
                }

                divtable.Visible = false;
                Limpiar();
            }
            catch (Exception)
            {

                Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
            }

        }

    }
    protected void btnReview_Click(object sender, EventArgs e)
    {
        if (ValidarDatosHTML())
        {
            Session["CedulaAutorizado"] = txtIdAut.Text;
            string PDF = "";
            string cadenaEncriptada = Encrypt.GetMD5(DateTime.Now.ToString());

            string html = retornarHtml();
            var htmlContent = String.Format(html);
            var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
            pathDescarga = "../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf";


            htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf"));

            PDF = pathDescarga;


            string url = "VerPDF.aspx?PDF=" + PDF + "&Origen1=Busquedad";
            Response.Write("<script type='text/javascript'>window.open('" + url + "', 'window','resizable=no,location=1,status=1,scrollbars=1,width=1200,height=800,left=400,top=90');</script>");

            if (firma != "")
            {
                btnGuardar.Visible = true;
                btnCaptureFirm.Visible = false;
            }
            else
            {
                btnCaptureFirm.Visible = true;
            }
        }
    }
    protected void Capture_Firma_Click(object sender, EventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, GetType(), "captsig", "Capture();", true);
    }
    #endregion

    #region "FUNCIONES"
    public class Encrypt
    {
        public static string GetMD5(string str)
        {
            MD5 md5 = MD5CryptoServiceProvider.Create();
            ASCIIEncoding encoding = new ASCIIEncoding();
            byte[] stream = null;
            StringBuilder sb = new StringBuilder();
            stream = md5.ComputeHash(encoding.GetBytes(str));
            for (int i = 0; i < stream.Length; i++) sb.AppendFormat("{0:x2}", stream[i]);
            return sb.ToString();
        }
    }
    private void Consultar(string Id)
    {
        if ((CheckPaciente.Checked) | (CheckRepresentante.Checked))
        {
            if (Id == "")
            {
                divtable.Visible = false;
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
            }
            else
            {
                DataSet ds = new ClinicaCES.Logica.LBusquedaPacientes().BusquedaPaciente(Id);
                DataTable dtInforme = ds.Tables[0];
                DataTable dtPagina = Procedimientos.dtFiltrado("IDENTIFICACION", "", dtInforme);
                string[] campo = { "IDENTIFICACION" };
                if (dtInforme.Rows.Count > 0)
                {
                    DataRow row = dtInforme.Rows[0];
                    LblNombre.Text = row["PACIENTE"].ToString();
                    string Edad = row["EDAD"].ToString();
                    Session["EDAD"] = Edad;
                    divtable.Visible = true;

                }
                else
                {
                    divtable.Visible = false;
                    Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
                }
            }
        }
        else
        {
            Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
        }
    }
    private void GuardarPDF(string Cedula, string CedUser, string NameUser)
    {
        try
        {


            if (divAutorizado.Visible == true)
            {
                //if ((txtIdAut.Text != "") && (txtNomAut.Text != "") && (txtEmpresa.Text != ""))
                if ((txtIdAut.Text != "") && (txtNomAut.Text != ""))
                {
                    TratamientoDatosPDF();
                    ConsentimientosInformados oPDF = new ConsentimientosInformados();

                    string FilePath = Server.MapPath(pathDescarga);

                    WebClient User = new WebClient();

                    Byte[] FileBuffer = User.DownloadData(FilePath);

                    oPDF.Cedula = Cedula;
                    oPDF.NombrePaciente = LblNombre.Text;
                    oPDF.CedulaUsuario = CedUser;
                    oPDF.NombreUsuario = NameUser;
                    oPDF.FechaRegistro = Convert.ToDateTime(DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"));
                    oPDF.AprobacionMedico = CheckAprobacionMedico.Checked = false;
                    oPDF.BodyHtml = Session["BODY_HTML"].ToString();

                    int startIndex = 10;
                    string NewpathDescarga = pathDescarga.Substring(startIndex).Replace(".pdf", "");

                    oPDF.Hash = NewpathDescarga;
                    oPDF.Consentimiento_PDF = FileBuffer;

                    if (new ClinicaCES.Logica.LConsentimientosInformados().InsertPDFDB(oPDF))
                    {
                        Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        Limpiar();
                    }
                }
                else
                {
                    Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                }
            }
            else
            {
                //if (txtEmpresa.Text != "")
                //{
                    ConsentimientosInformados oPDF = new ConsentimientosInformados();

                    string FilePath = Server.MapPath(pathDescarga);

                    WebClient User = new WebClient();

                    Byte[] FileBuffer = User.DownloadData(FilePath);

                    oPDF.Cedula = Cedula;
                    oPDF.NombrePaciente = LblNombre.Text;
                    oPDF.CedulaUsuario = CedUser;
                    oPDF.NombreUsuario = NameUser;
                    oPDF.FechaRegistro = Convert.ToDateTime(DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"));
                    oPDF.AprobacionMedico = CheckAprobacionMedico.Checked = false;
                    oPDF.BodyHtml = Session["BODY_HTML"].ToString();

                    int startIndex = 10;
                    string NewpathDescarga = pathDescarga.Substring(startIndex).Replace(".pdf", "");

                    oPDF.Hash = NewpathDescarga;
                    oPDF.Consentimiento_PDF = FileBuffer;

                    if (new ClinicaCES.Logica.LConsentimientosInformados().InsertPDFDB(oPDF))
                    {
                        Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        Limpiar();
                    }
                //}
                //else
                //{
                //    Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                //}
            }
        }
        catch (Exception)
        {

            Procedimientos.Script("mensajini", "Mensaje(3)", this.Page);
        }

    }
    public void drawimgFirm(string base64)
    {
        if ((base64 != ""))
        {
            if (Session["CedulaAutorizado"].ToString() != "")
            {
                dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(Session["CedulaAutorizado"].ToString());

                if (dtFirma.Rows.Count > 0)
                {
                    firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                    Id_Firma = Convert.ToInt16(dtFirma.Rows[0]["Id_Firma"]);

                    try
                    {
                        int startIndex = 7;
                        string Firma = base64.Substring(startIndex);

                        ConsentimientosInformados oFirma = new ConsentimientosInformados();

                        byte[] imagepath = Convert.FromBase64String(Firma);

                        if (Session["CedulaAutorizado"].ToString() != "")
                        {
                            oFirma.Cedula = Session["CedulaAutorizado"].ToString();
                        }
                        else
                        {
                            oFirma.Cedula = Session["Cedula"].ToString();
                        }


                        oFirma.UserFirmPath = imagepath;
                        oFirma.Id_Firma = Id_Firma;

                        if (new ClinicaCES.Logica.LConsentimientosInformados().UpdateFirmDB(oFirma))
                        {
                            Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        }

                        Session["CedulaAutorizado"] = null;
                    }
                    catch (Exception)
                    {

                        Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
                    }
                }
                else
                {
                    try
                    {
                        int startIndex = 7;
                        string Firma = base64.Substring(startIndex);

                        ConsentimientosInformados oFirma = new ConsentimientosInformados();

                        byte[] imagepath = Convert.FromBase64String(Firma);

                        if (Session["CedulaAutorizado"].ToString() != "")
                        {
                            oFirma.Cedula = Session["CedulaAutorizado"].ToString();
                        }
                        else
                        {
                            oFirma.Cedula = Session["Cedula"].ToString();
                        }


                        oFirma.UserFirmPath = imagepath;

                        if (new ClinicaCES.Logica.LConsentimientosInformados().InsertFirmDB(oFirma))
                        {
                            Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        }

                        Session["CedulaAutorizado"] = null;
                    }
                    catch (Exception)
                    {

                        Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
                    }
                }

            }
            else
            {
                dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(Session["Cedula"].ToString());

                if (dtFirma.Rows.Count > 0)
                {
                    firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                    Id_Firma = Convert.ToInt16(dtFirma.Rows[0]["Id_Firma"]);

                    try
                    {
                        int startIndex = 7;
                        string Firma = base64.Substring(startIndex);

                        ConsentimientosInformados oFirma = new ConsentimientosInformados();

                        byte[] imagepath = Convert.FromBase64String(Firma);

                        if (Session["CedulaAutorizado"].ToString() != "")
                        {
                            oFirma.Cedula = Session["CedulaAutorizado"].ToString();
                        }
                        else
                        {
                            oFirma.Cedula = Session["Cedula"].ToString();
                        }


                        oFirma.UserFirmPath = imagepath;
                        oFirma.Id_Firma = Id_Firma;

                        if (new ClinicaCES.Logica.LConsentimientosInformados().UpdateFirmDB(oFirma))
                        {
                            Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        }

                        Session["CedulaAutorizado"] = null;
                    }
                    catch (Exception)
                    {

                        Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
                    }
                }
                else
                {
                    try
                    {
                        int startIndex = 7;
                        string Firma = base64.Substring(startIndex);

                        ConsentimientosInformados oFirma = new ConsentimientosInformados();

                        byte[] imagepath = Convert.FromBase64String(Firma);

                        if (Session["CedulaAutorizado"].ToString() != "")
                        {
                            oFirma.Cedula = Session["CedulaAutorizado"].ToString();
                        }
                        else
                        {
                            oFirma.Cedula = Session["Cedula"].ToString();
                        }


                        oFirma.UserFirmPath = imagepath;

                        if (new ClinicaCES.Logica.LConsentimientosInformados().InsertFirmDB(oFirma))
                        {
                            Procedimientos.Script("mensajini", "Mensaje(1)", this.Page);
                        }

                        Session["CedulaAutorizado"] = null;
                    }
                    catch (Exception)
                    {

                        Procedimientos.Script("mensajini", "Mensaje(2)", this.Page);
                    }
                }
            }

        }

    }
    public void Limpiar()
    {
        LblNombre.Text = "";
        txtEmpresa.Text = "";
        txtId.Text = "";
        txtIdAut.Text = "";
        txtNomAut.Text = "";
        divtable.Visible = false;
        CheckPaciente.Checked = false;
        CheckRepresentante.Checked = false;
        CheckUtilizarFirmaBD.Checked = false;
        CheckUtilizarFirmaBD.Visible = false;

    }
    protected void GuardarEvento(string ced)
    {
        ConsentimientosInformados oEvento = new ConsentimientosInformados();
        if (CheckRepresentante.Checked)
        {   //string Autorizado = Session["AUTORIZACION"].ToString();
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-HC-2";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);

        }
        if (chkcons6.Checked)
        {
            oEvento.Cedula = ced;
            oEvento.CodConsentimiento = "F-SI-5";
            ClinicaCES.Logica.LConsentimientosInformados o = new ClinicaCES.Logica.LConsentimientosInformados();
            o.InsertEvento(oEvento);
        }
    }
    protected string retornarHtml()
    {
        string html = "", ENCABEZADO = "", AUTORIZADO = "", FIRMA = "", NEW_CUERPO_CONSTANCIA_ATENCION = "", NEW_CUERPO_TRATAMIENTO_DATOS = "", ID = "", htmlTratamientoDatos = "", REPRESENTACION="";
        /******************************************/

        #region "Datos"
        DataTable dt = new ClinicaCES.Logica.LConsentimientosInformados().C_INFORMADO_CARGAR_TODOS();
        DataRow row = dt.Rows[0];

        /********CONSTANCIA DE ATENCIÓN*******************************************************/
        string NOMBRE_CONSTANCIA_ATENCION = dt.Rows[12]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_CONSTANCIA_ATENCION = dt.Rows[12]["CODIGO_CONSENTIMIENTO"].ToString();
        string VERSION_CONSTANCIA_ATENCION = dt.Rows[12]["VERSION_CONSENTIMIENTO"].ToString();
        string CUERPO_CONSTANCIA_ATENCION = dt.Rows[12]["CUERPO_CONSENTIMIENTO"].ToString();

        /********ENCABEZADO*******************************************************************/
        string CUERPO_ENCABEZADO = dt.Rows[9]["CUERPO_CONSENTIMIENTO"].ToString();

        /********AUTORIZACIÓN DE REPRESENTACIÓN***********************************************/
        string NOMBRE_REPRESENTACION = dt.Rows[1]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_REPRESENTACION = dt.Rows[1]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_REPRESENTACION"] = CODIGO_REPRESENTACION;
        int VERSION_REPRESENTACION = Convert.ToInt16(dt.Rows[1]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_REPRESENTACION = dt.Rows[1]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CALIDAD EN QUE SE OTORGAN LOS CONSENTIMIENTOS COMO PACIENTE***********/
        string CUERPO_CONSENTIMIENTOS_PACIENTE = dt.Rows[8]["CUERPO_CONSENTIMIENTO"].ToString();

        /********CALIDAD EN QUE SE OTORGAN LOS CONSENTIMIENTOS COMO RESPONSABLE***********/
        string CUERPO_CONSENTIMIENTOS_RESPONSABLE = dt.Rows[2]["CUERPO_CONSENTIMIENTO"].ToString();

        /********TRATAMIENTO DE DATOS PERSONALES***********/
        string CUERPO_DATOS_PERSONALES = dt.Rows[3]["CUERPO_CONSENTIMIENTO"].ToString();

        /********FIRMA AUTORIZADO*********************************************************/
        string CUERPO_FIRMA_AUTORIZADO = dt.Rows[10]["CUERPO_CONSENTIMIENTO"].ToString();

        /********AUTORIZACIÓN PARA EL TRATAMIENTO DE DATOS PERSONALES***********/
        string NOMBRE_TRATAMIENTO_DATOS = dt.Rows[7]["NOMBRE_CONSENTIMIENTO"].ToString();
        string CODIGO_TRATAMIENTO_DATOS = dt.Rows[7]["CODIGO_CONSENTIMIENTO"].ToString();
        Session["CODIGO_TRATAMIENTO_DATOS"] = CODIGO_TRATAMIENTO_DATOS;
        int VERSION_TRATAMIENTO_DATOS = Convert.ToInt16(dt.Rows[7]["VERSION_CONSENTIMIENTO"]);
        string CUERPO_TRATAMIENTO_DATOS = dt.Rows[7]["CUERPO_CONSENTIMIENTO"].ToString();
        #endregion

        try
        {

            string path = Server.MapPath("../img/Logo.png"), NombreAutorizado = "";
            NombreAutorizado = LblNombre.Text;

            /****************ENCABEZADO*********************************/
            ENCABEZADO = CUERPO_ENCABEZADO
                               .Replace("lblPaciente", LblNombre.Text)
                               .Replace("lblDocumento", txtId.Text);
            /****************AUTORIZACIÓN*******************************/
            if (CheckRepresentante.Checked)
            {
                dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(txtIdAut.Text);
                if (dtFirma.Rows.Count > 0)
                {
                    CheckUtilizarFirmaBD.Visible = true;
                    firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firma = "";
                }

                NombreAutorizado = txtNomAut.Text;
                ID = txtIdAut.Text;
                AUTORIZADO = CUERPO_CONSENTIMIENTOS_RESPONSABLE;
                REPRESENTACION = CUERPO_REPRESENTACION
                                       .Replace("NombreConsentimientoAutorizacion", NOMBRE_REPRESENTACION)
                                       .Replace("CodigoConsentimientoAutorizacion", CODIGO_REPRESENTACION)
                                       .Replace("VersionadoAutorizacion", VERSION_REPRESENTACION.ToString())
                                       .Replace("txtNomAut", txtNomAut.Text)
                                       .Replace("txtIdAut", txtIdAut.Text);
            }
            else
            {
                dtFirma = new ClinicaCES.Logica.LConsentimientosInformados().SearchFirm(txtId.Text);
                if (dtFirma.Rows.Count > 0)
                {
                    CheckUtilizarFirmaBD.Visible = true;
                    firma = "data:image/png;base64," + Convert.ToBase64String((byte[])dtFirma.Rows[0]["UserFirmPath"]);
                }
                else
                {
                    firma = "";
                }
                AUTORIZADO = CUERPO_CONSENTIMIENTOS_PACIENTE;
                ID = txtId.Text;
                REPRESENTACION = "";

            }
            /****************TRATAMIENTO DE DATOS*********************/
            NEW_CUERPO_TRATAMIENTO_DATOS = CUERPO_TRATAMIENTO_DATOS
                        .Replace("NombreConsentimientoTD", NOMBRE_TRATAMIENTO_DATOS)
                        .Replace("CodigoConsentimientoTD", CODIGO_TRATAMIENTO_DATOS)
                        .Replace("VersionadoTD", VERSION_TRATAMIENTO_DATOS.ToString());

            /****************FIRMA**************************************/
            FIRMA = CUERPO_FIRMA_AUTORIZADO
                            .Replace("lblFecha", DateTime.Now.ToString("MMMM dd/yyyy h:mm tt"))
                            .Replace("txtIdAut", ID)
                            .Replace("firma", firma)
                            .Replace("lblUsuarioInforma", Session["Nombre"].ToString())
                            .Replace("lblPaciente", NombreAutorizado);

            /****************CONSTANCIA DE ATENCIÓN**************************************/
            NEW_CUERPO_CONSTANCIA_ATENCION = CUERPO_CONSTANCIA_ATENCION
                            .Replace("NombreConsentimientoCA", NOMBRE_CONSTANCIA_ATENCION)
                            .Replace("CodigoConsentimientoCA", CODIGO_CONSTANCIA_ATENCION)
                            .Replace("VersionadoCA", VERSION_CONSTANCIA_ATENCION.ToString())
                            .Replace("lblDocumento", txtId.Text)
                            .Replace("lblPaciente", LblNombre.Text)
                            .Replace("lblEdad", Session["EDAD"].ToString())
                            .Replace("lblServicio", "Urgencias")
                            .Replace("lblEmpresa", txtEmpresa.Text);

            html = ENCABEZADO + NEW_CUERPO_TRATAMIENTO_DATOS + REPRESENTACION + AUTORIZADO + FIRMA + CUERPO_DATOS_PERSONALES;
            htmlTratamientoDatos = ENCABEZADO + NEW_CUERPO_TRATAMIENTO_DATOS + AUTORIZADO + FIRMA + CUERPO_DATOS_PERSONALES;
            Session["BODY_HTML"] = htmlTratamientoDatos;
            return html;

        }

        catch
        { return html = ""; }

    }
    private byte[] TratamientoDatosPDF()
    {
        string cadenaEncriptada = Encrypt.GetMD5(DateTime.Now.ToString());

        string html = Session["BODY_HTML"].ToString();
        var htmlContent = String.Format(html);
        var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
        pathDescargaTratamiento = "../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf";


        htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + txtId.Text.Trim() + "_" + cadenaEncriptada + ".pdf"));

        string FilePath = Server.MapPath(pathDescargaTratamiento);

        WebClient User = new WebClient();

        FileBufferTratamientoDatos = User.DownloadData(FilePath);

        return FileBufferTratamientoDatos;

    }

    protected bool ValidarDatosHTML()
    {
        bool sw = true;

        if (CheckRepresentante.Checked)
        {
            if (txtNomAut.Text == "")
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                txtNomAut.Focus();
                sw = false;
            }
            if (txtIdAut.Text == "")
            {
                Procedimientos.Script("mensajini", "Mensaje(60)", this.Page);
                txtIdAut.Focus();
                sw = false;
            }
        }
        return sw;
    }
    #endregion

    protected void CheckUtilizarFirmaBD_CheckedChanged(object sender, EventArgs e)
    {
        if (CheckUtilizarFirmaBD.Checked)
        {

            btnCaptureFirm.Visible = true;

        }
        else
        {
            btnCaptureFirm.Visible = false;
        }
    }
}

