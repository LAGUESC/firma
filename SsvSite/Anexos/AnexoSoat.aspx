﻿<%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>

<%@ Page Language="C#" MasterPageFile="~/Masters/Master.master" AutoEventWireup="true" CodeFile="AnexoSoat.aspx.cs" Inherits="Anexos_Anexo3" Title="Anexo 3" %>


<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <table style="border: 1px solid #000000; width: 1003px">
    <tr>
        <td rowspan="4" style="width: 143px" align="center">
            <asp:Image ID="Image1" runat="server" Height="49px" ImageUrl="../img/escudo-minsalud.jpg" Width="106px" />
        </td>
    </tr>
    <tr>
        <td align="center" style="width: 704px"><B>MINISTERIO DE SALUD Y PROTECCION SOCIAL</B></td>
        <td align="center" rowspan="3">
            <asp:Image ID="Image2" runat="server" Height="49px" Width="106px" ImageUrl="../img/nuevopais.jpg" />
        </td>
    </tr>
   
    <tr>
        <td align="center" style="width: 704px"><B>INFORME DE LA ATENCIÓN EN SALUD A VÍCTIMAS DE ACCIDENTE DE TRÁNSITO</B></td>
    </tr>
    </table>
    <p></p>
    <table style="border: 1px solid #000000; width: 1003px">
    <tr>
        <td colspan="68" align="center" style="border-style: none none solid none; border-width: 1px; border-color: #000000;"><b >INFORMACIÓN DEL PRESTADOR</b></td>
    </tr>
    <tr>
        <td style="width: 1836px" colspan="7"><b>Nombre o razón social:</b></td>
        <td colspan="61"><asp:TextBox ID="TextBox38" runat="server" Enabled="False" ReadOnly="True" Width="392px" CssClass="form_input">CORPORACION PARA ESTUDIOS EN SALUD CLINICA CES</asp:TextBox>
        </td>
    </tr>
    <tr>
        <td colspan="7" style="width: 1836px">
            <b>Tipo de Identificación:</b></td>
        <td>
            <b>NIT</b></td>
        <td style="width: 807px" align="left">
            <asp:TextBox ID="TextBox25" runat="server" Width="16px" ReadOnly="True" Font-Underline="False" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">X</asp:TextBox>
        </td>
        <td style="width: 807px" align="left">
            Número</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left">
            &nbsp;</td>
        <td style="width: 807px" align="left">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td align="center" colspan="3">
            DV</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td style="width: 807px" align="left" colspan="2">
            &nbsp;</td>
        <td align="left" colspan="19">
            <b>Código de Habilitación</b></td>
        <td colspan="7">
            <asp:TextBox ID="TextBox41" runat="server" style="text-align: center" Width="114px" Enabled="False" ReadOnly="True" CssClass="form_input">0500102124</asp:TextBox>
        </td>
        <td align="center" colspan="3">
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="7" style="width: 1836px">
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td>
        <td>
            <b>CC</b></td>
        <td>
            <asp:TextBox ID="TextBox39" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1" Enabled="False" ReadOnly="True" CssClass="form_input"></asp:TextBox>
        </td>
        <td colspan="20">
            <asp:TextBox ID="TextBox26" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox27" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">9</asp:TextBox>
            <asp:TextBox ID="TextBox28" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox29" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">9</asp:TextBox>
            <asp:TextBox ID="TextBox30" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox31" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox32" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">6</asp:TextBox>
            <asp:TextBox ID="TextBox33" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">0</asp:TextBox>
            <asp:TextBox ID="TextBox34" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">8</asp:TextBox>
            <asp:TextBox ID="TextBox35" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            <asp:TextBox ID="TextBox36" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">-</asp:TextBox>
        </td>
        <td colspan="2">
            &nbsp;</td>
        <td>
            <asp:TextBox ID="TextBox37" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">1</asp:TextBox>
        </td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="4">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td align="center" colspan="3">
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="7" style="width: 1836px">
            <b>Dirección del prestador:</b></td>
        <td colspan="45">
            <asp:TextBox ID="TextBox66" runat="server" Width="389px" Enabled="False" CssClass="form_input">CALLE 58 # 50C-2 PRADO CENTRO</asp:TextBox>
        </td>
        <td colspan="2">
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="3">
            &nbsp;</td>
    </tr>
    <tr>
        <td colspan="4">
            <b>Teléfono:</b></td>
        <td colspan="2">
            &nbsp;</td>
        <td colspan="17">
            <asp:TextBox ID="TextBox460" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">5</asp:TextBox>
            <asp:TextBox ID="TextBox461" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox462" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">6</asp:TextBox>
            <asp:TextBox ID="TextBox463" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox464" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            <asp:TextBox ID="TextBox465" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7</asp:TextBox>
            <asp:TextBox ID="TextBox466" runat="server" Width="16px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">2</asp:TextBox>
            &nbsp;&nbsp;&nbsp;</td>
        <td colspan="37">
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td colspan="2">
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td colspan="3">
            &nbsp;</td>
    </tr>
    <tr>
        <td align="left">
            &nbsp;</td>
        <td align="left">
            &nbsp;</td>
        <td align="left">
            &nbsp;</td>
        <td align="center" colspan="2">
            &nbsp;</td>
        <td align="center" colspan="6">
            <b>Número</b></td>
        <td align="center" colspan="2">
            &nbsp;</td>
        <td align="center" colspan="2">
            &nbsp;</td>
        <td align="center" colspan="2">
            &nbsp;</td>
        <td align="center" colspan="2">
            &nbsp;</td>
        <td align="center" colspan="2">
            &nbsp;</td>
        <td align="left" colspan="4">
            <b>Departamento:</b></td>
        <td align="left" colspan="28">
            <asp:TextBox ID="TextBox67" runat="server" Enabled="False" ReadOnly="True" Width="114px" CssClass="form_input">ANTIOQUIA</asp:TextBox>
        </td>
        <td align="left" colspan="3">
            &nbsp;</td>
        <td align="left">
            &nbsp;</td>
        <td align="left">
            <b>Municipio:</b></td>
        <td align="left" colspan="7">
            &nbsp;&nbsp;<asp:TextBox ID="TextBox459" runat="server" Enabled="False" ReadOnly="True" Width="106px" CssClass="form_input">MEDELLIN</asp:TextBox>
        </td>
        <td style="width: 71px">
            &nbsp;</td>
        <td style="width: 71px">
            &nbsp;</td>
        <td style="width: 71px">
            &nbsp;</td>
    </tr>
    </table>
   <p></p>
    <table style="border: 1px solid #000000; width: 1003px">
    <tr>
        <td colspan="8" align="center" style="border-color: #000000;  border-bottom-style: solid; border-bottom-width: 1px;"><B>DATOS DE LA VÍCTIMA DEL ACCIDENTE DE TRÁNSITO</b></td>
    </tr>
    <tr>
        <td>
            <asp:TextBox ID="TextBox75" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBox76" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td colspan="3">
            <asp:TextBox ID="TextBox77" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td colspan="3">
            <asp:TextBox ID="TextBox78" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px"><b>Primer Apellido</b></td>
        <td style="height: 23px"><b>Segundo Apellido</b></td>
        <td style="height: 23px" colspan="3"><b>Primer Nombre</b></td>
        <td style="height: 23px" colspan="3"><b>Segundo Nombre</b></td>
    </tr>
    <tr>
        <td colspan="8"><b>Tipo Documento de Identificación</b></td>
    </tr>
    <tr>
        <td colspan="2" rowspan="4">
            <asp:RadioButtonList ID="rblTipoId" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Width="473px" CssClass="form_input">
                <asp:ListItem Value="2">Registro Civil</asp:ListItem>
                <asp:ListItem Value="P">Pasaporte</asp:ListItem>
                <asp:ListItem Value="3">Tarjeta de Identidad</asp:ListItem>
                <asp:ListItem Value="10">Carné diplomático</asp:ListItem>
                <asp:ListItem Value="4">Cédula de Ciudadania</asp:ListItem>
                <asp:ListItem Value="11">Certificado de nacido vivo - DANE</asp:ListItem>
                <asp:ListItem Value="5">Cédula de Extranjería</asp:ListItem>
                <asp:ListItem Value="12">Salvoconducto de permanencia</asp:ListItem>
                <asp:ListItem Value="13">Persona sin identificar</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        <td>&nbsp;</td>
        <td colspan="5">
            <asp:TextBox ID="TextBox81" runat="server" Width="280px" style="text-align:center" Font-Size="Small" MaxLength="10" Height="16px" Enabled="False" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender28" runat="server" TargetControlID="TextBox81" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Anexo3.aspx.cs" Inherits="Anexos_Anexo3" %>--%>
            </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center" colspan="3">Número Documento de Identificación</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td colspan="2">&nbsp;</td>
        <td></td>
        <td></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td><b>Fecha de Nacimiento:</b></td>
        <td colspan="4">
            <asp:TextBox ID="TextBox102" runat="server" Width="163px" style="text-align:center" Font-Size="Small" MaxLength="10" Enabled="False" CssClass="form_input"></asp:TextBox>
            <%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="TextBox10" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
    </tr>
    </table>
    <p></p>
    <table style="border: 1px solid #000000; width: 1003px">
    <tr>
        <td align="center" colspan="20" style="border-bottom-style: solid; border-width: 1px; border-color: #000000"><B>TIPO DE INGRESO A LOS SERVICIOS DE SALUD</b></td>
    </tr>
    <tr>
        <td align="center" colspan="20">
            <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatColumns="3" Width="765px" CssClass="form_input">
                <asp:ListItem Value="1">Atención Inicial de Urgencias</asp:ListItem>
                <asp:ListItem Value="2">Atención de Urgencias</asp:ListItem>
                <asp:ListItem Value="3">Atención Programada</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td align="right" colspan="2"><b>Fecha:</b></td>
        <td style="width: 346px" align="left">
            <asp:TextBox ID="txtFecha" runat="server" onkeyup="mascara(this,'/',true)" 
                        CssClass="form_input" Width="104px" ontextchanged="txtFecha_TextChanged"></asp:TextBox>
                    <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                        runat="server" TargetControlID="txtFecha" FilterType="Custom, Numbers" 
                        ValidChars="/" />        
                    <ajaxToolkit:CalendarExtender Format="yyyy/MM/dd" ID="CalendarExtender1" OnClientDateSelectionChanged="ValidaSemana"
                    runat="server" TargetControlID="txtFecha" PopupButtonID="img1"></ajaxToolkit:CalendarExtender>
                    <img style="cursor:pointer; width: 16px;" src="../icons/calendar.png" runat="server" 
                        id="img1" __designer:mapid="766" />

        </td>
        <td align="center" style="width: 12px">
            &nbsp;</td>
        <td align="center" colspan="9">&nbsp;</td>
        <td align="right" colspan="2"><b>Hora:</b></td>
        <td align="left">
            
            <asp:DropDownList ID="DropDownList1" runat="server" Height="23px" Width="43px">
                <asp:ListItem>00</asp:ListItem>
                <asp:ListItem>01</asp:ListItem>
                <asp:ListItem>02</asp:ListItem>
                <asp:ListItem>03</asp:ListItem>
                <asp:ListItem>04</asp:ListItem>
                <asp:ListItem>05</asp:ListItem>
                <asp:ListItem>06</asp:ListItem>
                <asp:ListItem>07</asp:ListItem>
                <asp:ListItem>08</asp:ListItem>
                <asp:ListItem>09</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>11</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>13</asp:ListItem>
                <asp:ListItem>14</asp:ListItem>
                <asp:ListItem>15</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>17</asp:ListItem>
                <asp:ListItem>18</asp:ListItem>
                <asp:ListItem>19</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>21</asp:ListItem>
                <asp:ListItem>22</asp:ListItem>
                <asp:ListItem>23</asp:ListItem>
            </asp:DropDownList>
            <asp:DropDownList ID="DropDownList2" runat="server" Height="23px" Width="43px">
                <asp:ListItem>00</asp:ListItem>
                <asp:ListItem>01</asp:ListItem>
                <asp:ListItem>02</asp:ListItem>
                <asp:ListItem>03</asp:ListItem>
                <asp:ListItem>04</asp:ListItem>
                <asp:ListItem>05</asp:ListItem>
                <asp:ListItem>06</asp:ListItem>
                <asp:ListItem>07</asp:ListItem>
                <asp:ListItem>08</asp:ListItem>
                <asp:ListItem>09</asp:ListItem>
                <asp:ListItem>10</asp:ListItem>
                <asp:ListItem>11</asp:ListItem>
                <asp:ListItem>12</asp:ListItem>
                <asp:ListItem>13</asp:ListItem>
                <asp:ListItem>14</asp:ListItem>
                <asp:ListItem>15</asp:ListItem>
                <asp:ListItem>16</asp:ListItem>
                <asp:ListItem>17</asp:ListItem>
                <asp:ListItem>18</asp:ListItem>
                <asp:ListItem>19</asp:ListItem>
                <asp:ListItem>20</asp:ListItem>
                <asp:ListItem>21</asp:ListItem>
                <asp:ListItem>22</asp:ListItem>
                <asp:ListItem>23</asp:ListItem>
                <asp:ListItem>24</asp:ListItem>
                <asp:ListItem>25</asp:ListItem>
                <asp:ListItem>26</asp:ListItem>
                <asp:ListItem>27</asp:ListItem>
                <asp:ListItem>28</asp:ListItem>
                <asp:ListItem>29</asp:ListItem>
                <asp:ListItem>30</asp:ListItem>
                <asp:ListItem>31</asp:ListItem>
                <asp:ListItem>32</asp:ListItem>
                <asp:ListItem>33</asp:ListItem>
                <asp:ListItem>34</asp:ListItem>
                <asp:ListItem>35</asp:ListItem>
                <asp:ListItem>36</asp:ListItem>
                <asp:ListItem>37</asp:ListItem>
                <asp:ListItem>38</asp:ListItem>
                <asp:ListItem>39</asp:ListItem>
                <asp:ListItem>40</asp:ListItem>
                <asp:ListItem>41</asp:ListItem>
                <asp:ListItem>42</asp:ListItem>
                <asp:ListItem>43</asp:ListItem>
                <asp:ListItem>44</asp:ListItem>
                <asp:ListItem>45</asp:ListItem>
                <asp:ListItem>46</asp:ListItem>
                <asp:ListItem>47</asp:ListItem>
                <asp:ListItem>48</asp:ListItem>
                <asp:ListItem>49</asp:ListItem>
                <asp:ListItem>50</asp:ListItem>
                <asp:ListItem>51</asp:ListItem>
                <asp:ListItem>52</asp:ListItem>
                <asp:ListItem>53</asp:ListItem>
                <asp:ListItem>54</asp:ListItem>
                <asp:ListItem>55</asp:ListItem>
                <asp:ListItem>56</asp:ListItem>
                <asp:ListItem>57</asp:ListItem>
                <asp:ListItem>58</asp:ListItem>
                <asp:ListItem>59</asp:ListItem>
            </asp:DropDownList>
            
        </td>
        <td align="center">&nbsp;</td>
        <td align="center" colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" colspan="10"><B>Víctima viene remitida</B></td>
        <td align="left" colspan="10">
            <asp:RadioButtonList ID="RadioButtonList4" runat="server" OnSelectedIndexChanged="RadioButtonList4_SelectedIndexChanged" RepeatColumns="2" Width="273px" AutoPostBack="True">
                <asp:ListItem Value="1">SI</asp:ListItem>
                <asp:ListItem Value="2">NO</asp:ListItem>
            </asp:RadioButtonList>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="10"><b>Razón social del prestador de servicios de salud que remite</b></td>
        <td align="center" colspan="5"><b>
            Código de habilitación</b></td>
        <td align="left" colspan="5">
            <asp:TextBox ID="TextBox469" runat="server" Width="118px" CssClass="form_input" Enabled="False"></asp:TextBox>
           
        </td>
    </tr>
    <tr>
        <td align="left" colspan="20">
            <asp:TextBox ID="TextBox470" runat="server" Width="938px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td><b>Departamento:</b></td>
        <td colspan="2">
            <asp:DropDownList ID="dllDepartamento" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dllDepartamento_SelectedIndexChanged" Width="204px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td bgcolor="Gray" colspan="2">&nbsp;</td>
        <td bgcolor="Gray">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td bgcolor="White">&nbsp;</td>
        <td colspan="2"><b>Municipio:</b></td>
        <td colspan="2">
            <asp:DropDownList ID="ddlMunicipio" runat="server" Width="179px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td colspan="2">&nbsp;</td>
        <td>&nbsp;</td>
        <td>&nbsp;</td>
    </tr>
    </table>
   <p>
   </p>
    <table style="border: 1px solid #000000; width: 1003px">
    <tr>
        <td colspan="2" align="center" style="border-bottom-style: solid; border-width: 1px; border-color: #000000"><B>INFORMACIÓN DEL TRANSPORTE AL PRIMER SITIO DE LA ATENCIÓN</B></td>
    </tr>
    <tr>
        <td align="center" style="width: 509px"><B>Victima fue trasladada en transporte especial de pacientes</B></td>
        <td align="center">
            <asp:CheckBoxList ID="CheckBoxList2" runat="server" RepeatColumns="2" Width="469px" CssClass="form_input" AutoPostBack="True" OnSelectedIndexChanged="CheckBoxList2_SelectedIndexChanged">
                <asp:ListItem Value="1">SI</asp:ListItem>
                <asp:ListItem Value="2">NO</asp:ListItem>
            </asp:CheckBoxList>
        </td>
    </tr>
    <tr>
        <td align="center" style="width: 509px">
            <asp:TextBox ID="TextBox471" runat="server" Width="167px" CssClass="form_input" Enabled="False"></asp:TextBox>
          

        </td>
        <td align="center">
            <asp:TextBox ID="TextBox478" runat="server" Width="84px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="center" style="width: 509px"><B>Código del prestador de transporte especial</B></td>
        <td align="center"><b>Placa del vehiculo</b></td>
    </tr>
    </table>
    <p>
    </p>
    <table style="border: 1px solid #000000; width: 1003px">
    <tr>
        <td colspan="17" align="center" style="border-bottom-style: solid; border-width: 1px; border-color: #000000"><B>DATOS DEL ACCIDENTE</B></td>
    </tr>
    <tr>
        <td align="center"><b>Fecha:</b></td>
        <td align="left">
            <asp:TextBox ID="TextBox472" runat="server" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td colspan="2" align="center"><b>Hora:</b></td>
        <td align="left">
            <asp:TextBox ID="TextBox473" runat="server" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td align="center">&nbsp;</td>
        <td align="center"><b>Departamento</b></td>
        <td align="left" colspan="2" style="width: 13px">
            <asp:DropDownList ID="dllDepartamento0" runat="server" AutoPostBack="True"  Width="204px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center"><b>Municipio:</b></td>
        <td align="left" style="width: 9px">
            <asp:DropDownList ID="ddlMunicipio0" runat="server" Width="179px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td align="center" colspan="2">&nbsp;</td>
        <td align="center">&nbsp;</td>
        <td align="center">&nbsp;</td>
    </tr>
    <tr>
        <td align="center" colspan="2">Dirección del accidente:</td>
        <td colspan="15" align="left">
            <asp:TextBox ID="TextBox476" runat="server" Width="636px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td align="center" colspan="17" style="border-bottom-style: solid; border-top-style: solid; border-width: 1px; border-color: #000000"><B>DATOS DEL VEHICULO INVOLUCRADO EN EL ACCIDENTE DE TRÁNSITO</B></td>
    </tr>
    <tr>
        <td align="center" colspan="3"><b>Placa:</b></td>
        <td align="center" colspan="2">
            <asp:TextBox ID="TextBox477" runat="server" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
        <td align="center" colspan="3">&nbsp;</td>
        <td align="center" colspan="3"><b>Vehiculo no identificado:</b></td>
        <td align="center" colspan="3">
            <asp:CheckBox ID="CheckBox1" runat="server" CssClass="form_input" Enabled="False" />
        </td>
        <td align="center" colspan="3">&nbsp;</td>
    </tr>
    </table>
   <p></p>
    <table style="border: 1px solid #000000; width: 1003px">
    <tr>
        <td colspan="11" align="center" style="border-color: #000000; border-bottom-style: solid; border-bottom-width: 1px;"><B>DATOS DEL CONDUCTOR DEL VEHICULO INVOLUCRADO EN EL ACCIDENTE</b></td>
    </tr>
    <tr>
        <td colspan="2">
            <asp:TextBox ID="TextBox1" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBox2" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td colspan="3">
            <asp:TextBox ID="TextBox3" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
        <td colspan="5">
            <asp:TextBox ID="TextBox4" runat="server" Width="233px" Enabled="False" CssClass="form_input"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td style="height: 23px" colspan="2"><b>Primer Apellido</b></td>
        <td style="height: 23px"><b>Segundo Apellido</b></td>
        <td style="height: 23px" colspan="3"><b>Primer Nombre</b></td>
        <td style="height: 23px" colspan="5"><b>Segundo Nombre</b></td>
    </tr>
    <tr>
        <td colspan="11"><b>Tipo Documento de Identificación</b></td>
    </tr>
    <tr>
        <td colspan="3" rowspan="4">
            <asp:RadioButtonList ID="RadioButtonList2" runat="server" RepeatColumns="2" RepeatDirection="Horizontal" Width="473px" CssClass="form_input" Enabled="False" Height="75px">
                <asp:ListItem Value="P">Pasaporte</asp:ListItem>
                <asp:ListItem Value="10">Carné diplomatico</asp:ListItem>
                <asp:ListItem Value="3">Tarjeta de Identidad</asp:ListItem>
                <asp:ListItem Value="12">Salvoconducto de permanencia</asp:ListItem>
                <asp:ListItem Value="4">Cédula de Ciudadania</asp:ListItem>
                <asp:ListItem Enabled="False"></asp:ListItem>
                <asp:ListItem Value="5">Cédula de Extranjería</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        <td colspan="8">
            <asp:TextBox ID="TextBox5" runat="server" Width="196px" style="text-align:center" Font-Size="Small" MaxLength="10" Height="16px" Enabled="False" CssClass="form_input"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="TextBox81" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <%--<asp:TextBox ID="TextBox11" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" runat="server" TargetControlID="TextBox11" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox12" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" runat="server" TargetControlID="TextBox12" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox13" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" runat="server" TargetControlID="TextBox13" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:Label ID="Label3" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox14" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" runat="server" TargetControlID="TextBox14" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox15" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="TextBox15" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:Label ID="Label1" runat="server" Text="-" Font-Bold="True" Font-Size="XX-Large"></asp:Label>
            <asp:TextBox ID="TextBox16" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" runat="server" TargetControlID="TextBox16" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>
            <asp:TextBox ID="TextBox17" runat="server" Width="16px" style="text-align:center" Font-Size="Small" MaxLength="1"></asp:TextBox>
            <ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" runat="server" TargetControlID="TextBox17" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            </td>
    </tr>
    <tr>
        <td colspan="8">Número Documento de Identificación</td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center" colspan="3">&nbsp;</td>
        <td style="width: 6px"></td>
        <td colspan="3"></td>
    </tr>
    <tr>
        <td>&nbsp;</td>
        <td align="center" colspan="3">&nbsp;</td>
        <td style="width: 6px">&nbsp;</td>
        <td colspan="3">&nbsp;</td>
    </tr>
    <tr>
        <td colspan="2">
            <b>Dirección del Conductor:</b></td>
        <td colspan="9">
            <asp:TextBox ID="TextBox479" runat="server" Width="636px" CssClass="form_input" Enabled="False"></asp:TextBox>
        </td>
    </tr>
    <tr>
        <td>
            <%--<ajaxToolkit:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" runat="server" TargetControlID="TextBox10" FilterType="Numbers" ValidChars=" "  ></ajaxToolkit:FilteredTextBoxExtender>--%>
            <b>Teléfono:</b></td>
        <td align="center" colspan="2" style="font-weight: 700">
            <asp:TextBox ID="TextBox480" runat="server" Width="134px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input"></asp:TextBox>
            &nbsp;&nbsp;&nbsp;&nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td>
            &nbsp;</td>
        <td style="width: 6px">
            &nbsp;</td>
    </tr>
    <tr>
        <td>
            &nbsp;</td>
        <td align="center" colspan="2">
            <b>Número</b></td>
        <td>
            <b>Departamento:</b></td>
        <td>
            <asp:DropDownList ID="dllDepartamento1" runat="server" AutoPostBack="True"  Width="204px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td>
            &nbsp;</td>
        <td>
            <b>Municipio:</b></td>
        <td style="width: 6px">
            <asp:DropDownList ID="ddlMunicipio1" runat="server" Width="179px" CssClass="form_input" Enabled="False">
            </asp:DropDownList>
        </td>
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
        <td >
            &nbsp;</td>
    </tr>
    </table>
    <p></p>
    <table style="width: 100%">
        <tr>
            <td>
                &nbsp;</td>
        </tr>
    </table>
    <p></p>
    <table style="border: 1px solid #000000; width: 1003px">
    <tr>
        <td style="border-width: 1px; border-color: #000000; border-bottom-style: solid;" colspan="9" align="center"><B>INFORMACIÓN DE LA PERSONA QUE REPORTA LA ATENCIÓN</B></td>
    </tr>
        <tr>
        <td colspan="2">
            <asp:TextBox ID="TextBox489" runat="server" Width="233px" Enabled="False" CssClass="form_input">HOYOS</asp:TextBox>
        </td>
        <td>
            <asp:TextBox ID="TextBox490" runat="server" Width="233px" Enabled="False" CssClass="form_input">LOPEZ</asp:TextBox>
        </td>
        <td colspan="2">
            <asp:TextBox ID="TextBox491" runat="server" Width="233px" Enabled="False" CssClass="form_input">DORA</asp:TextBox>
        </td>
        <td colspan="3">
            <asp:TextBox ID="TextBox492" runat="server" Width="233px" Enabled="False" CssClass="form_input">LUZ</asp:TextBox>
        </td>
        <td style="width: 268435456px">
            &nbsp;</td>
        </tr>
        <tr>
        <td style="height: 23px" colspan="2"><b>Primer Apellido</b></td>
        <td style="height: 23px"><b>Segundo Apellido</b></td>
        <td style="height: 23px" colspan="2"><b>Primer Nombre</b></td>
        <td style="height: 23px" colspan="3"><b>Segundo Nombre</b></td>
        <td style="height: 23px; width: 268435456px;">&nbsp;</td>
        </tr>
        <tr>
        <td style="height: 23px" rowspan="2" colspan="2"><b>Tipo Documento de Identificación</b></td>
        <td style="height: 23px" colspan="2" rowspan="2">
            <asp:RadioButtonList ID="RadioButtonList3" runat="server" RepeatColumns="2" Width="370px" Enabled="False">
                <asp:ListItem Value="3">Tarjeta de Identidad</asp:ListItem>
                <asp:ListItem Value="4">Cédula de ciudadania</asp:ListItem>
                <asp:ListItem Value="5">Cédula de extranjería</asp:ListItem>
            </asp:RadioButtonList>
            </td>
        <td style="height: 23px" colspan="4">
            <asp:TextBox ID="TextBox493" runat="server" Width="196px" style="text-align:center" Font-Size="Small" MaxLength="10" Height="16px" Enabled="False" CssClass="form_input">43607528</asp:TextBox>
            </td>
        <td align="center" style="height: 23px; width: 268435456px;">
            &nbsp;</td>
        </tr>
        <tr>
        <td style="height: 23px" colspan="4">
            Número documento de identificación</td>
        <td align="center" style="height: 23px; width: 268435456px;">&nbsp;</td>
        </tr>
        <tr>
        <td style="height: 23px"><b>Cargo:</b></td>
        <td style="height: 23px" colspan="4">
            <asp:TextBox ID="TextBox494" runat="server" Width="334px" Enabled="False" CssClass="form_input">EJECUTIVO DE CUENTA SOAT</asp:TextBox>
            </td>
        <td style="height: 23px"><b>Teléfono:</b></td>
        <td style="height: 23px">
            <asp:TextBox ID="TextBox495" runat="server" Width="101px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">5767272</asp:TextBox>
            </td>
        <td align="center" style="height: 23px; width: 268435456px;">
            <asp:TextBox ID="TextBox496" runat="server" Width="48px" ReadOnly="True" style="text-align:center" Font-Size="Small" Enabled="False" CssClass="form_input">7261</asp:TextBox>
            </td>
        <td style="height: 23px; width: 268435456px;">&nbsp;</td>
        </tr>
        <tr>
        <td style="height: 23px" colspan="2">&nbsp;</td>
        <td style="height: 23px" colspan="3">&nbsp;</td>
        <td style="height: 23px">&nbsp;</td>
        <td style="height: 23px" align="center">Número</td>
        <td style="height: 23px; width: 268435456px;" align="center">Extensión</td>
        <td style="height: 23px; width: 268435456px;">&nbsp;</td>
        </tr>
        </table>
    <table style="width: 100%">
        <tr>
            <td style="width: 206px"><b>Observaciones:</b></td>
            <td rowspan="2">
                <asp:TextBox ID="TextBox497" runat="server" TextMode="MultiLine" Width="764px" Style="text-transform: none"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td style="width: 206px">&nbsp;</td>
        </tr>
    </table>
    
    <table style="width: 1003px">
    <tr>
        <td style="height: 23px" align="center">
            <asp:Button ID="Button1" runat="server" Text="Enviar Reporte" OnClick="Button1_Click" />
        &nbsp;<asp:Button ID="Button2" runat="server" Text="Imprimir PDF" OnClick="Button2_Click" Enabled="False" Visible="False" />
        </td>
    </tr>
    <tr>
        <td style="height: 23px" align="center">
            
            <asp:Literal ID="litScript" runat="server"></asp:Literal>
        </td>
    </tr>
</table>
<asp:panel id="Panel1" runat="server" CssClass="modalPopup" Style="display: none;
                text-align: left;" Width="788px" Height="455px" BackColor="Silver" ForeColor="Black" BorderStyle="Solid">
	<div class="HellowWorldPopup">
               <%-- <div class="PopupHeader" id="PopupHeader"><b>Formulario de diligenciamiento de clientes</b></div>
                <div class="PopupBody">--%>
        <table >
            <tr>
                <td >
                    <asp:Label ID="Label1" runat="server" Text="Filtrar (Cédula o Nombre)"></asp:Label>
                </td>
                <td class="auto-style4" >
                    <asp:TextBox ID="txtFiltro" runat="server" AutoPostBack="True" OnTextChanged="txtFiltro_TextChanged" Width="282px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView AutoGenerateColumns="False" ID="gvInforme" runat="server" AllowPaging="True" 
                        CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="15" 
                        onrowdatabound="gvInforme_RowDataBound"
                        onpageindexchanging="gvInforme_PageIndexChanging" Width="780px" EnableModelValidation="True" OnSelectedIndexChanged="gvInforme_SelectedIndexChanged">                   
                        <FooterStyle BackColor="#F7DFB5" />
                        <RowStyle CssClass="normalrow"/>
                        <AlternatingRowStyle CssClass="alterrow" />
                        <HeaderStyle CssClass="cabeza" Font-Bold="True" ForeColor="White"  />                
                        <Columns>      
                             <asp:BoundField DataField="ID" HeaderText="ID" />
                             <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" />
                             <asp:BoundField DataField="ESPECIALIDAD" HeaderText="ESPECIALIDAD" />   
                             <asp:CommandField ShowSelectButton="True" />
                                            
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <div class="Controls" >
                    <input id="btnOkay" type="button" value="Retornar" />
                        </div>
                </td>
            </tr>
        </table>
        </div>
</asp:panel>
  
</asp:Content>
