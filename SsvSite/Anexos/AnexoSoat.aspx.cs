﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using ClinicaCES.Correo;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;
using System.IO;
using System.Net;


//using System.Net.Http;
//using System.Net.Http.Headers;
//using System.Threading.Tasks;


public partial class Anexos_Anexo3 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            datosIniciales();
        }
    }
    protected void datosIniciales()
    {
        // OJO NO OLVIDAR QUITAR EL COMENTARIO

        if (Request.QueryString["cookieSoat"] != null)
        {
            Procedimientos.LlenarCombos(dllDepartamento, new ClinicaCES.Logica.LMaestros().ListarDepartamentos(), "CODDEP", "DEP");
            Procedimientos.LlenarCombos(dllDepartamento0, new ClinicaCES.Logica.LMaestros().ListarDepartamentos(), "CODDEP", "DEP");
            Procedimientos.LlenarCombos(dllDepartamento1, new ClinicaCES.Logica.LMaestros().ListarDepartamentos(), "CODDEP", "DEP");
            TraerDatos(Procedimientos.descifrar(Request.QueryString["cookieSoat"]));
            //TraerDatos(Request.QueryString["cookieSoat"]);
            //TraerDatos("4;71765945|1612000002");
            
            TextBox497.Text.ToLower();
        }
    }

    protected void txtFecha_TextChanged(object sender, EventArgs e)
    {
        if (!string.IsNullOrEmpty(txtFecha.Text.Trim()))
            if (!ValidaSemana())
            {
                Procedimientos.Script("mensajini", "Mensaje(19)", this.Page);
                //pnlInforme.Visible = false;
            }
    }
    private bool ValidaSemana()
    {
        bool valido = true;
        if ((txtFecha.Text != ""))
        {
            DateTime FechaIni = Convert.ToDateTime(txtFecha.Text);
            if (!valido)
            {
                txtFecha.Text = string.Empty;
            }
        }
        return valido;
    }
    protected void TraerDatos(string Busqueda)
    {
        string msg = "";
        try
        {
            string[] Filtros = Busqueda.Split('|');
            DataTable dtGrid = new ClinicaCES.Logica.LBusquedaPacientes().consultarDatosAnexoSoat(Filtros[0], Convert.ToDateTime(Filtros[1]), Filtros[2]);
            //TextBox497.Text = new ClinicaCES.Logica.LBusquedaPacientes().consultarDatosAnexoSoatg(Filtros[0],Convert.ToDateTime( Filtros[1]), Filtros[2]);
            if (dtGrid.Rows.Count > 0)
            {
                DataRow registro = dtGrid.Rows[0];
                TextBox75.Text = registro[0].ToString();
                TextBox76.Text = registro[1].ToString();
                TextBox77.Text = registro[2].ToString();
                TextBox78.Text = registro[3].ToString();
                TextBox81.Text = registro[4].ToString();
                rblTipoId.SelectedValue = registro[5].ToString();
                // TraerIndentificacion(registro[5].ToString());
                TextBox102.Text = registro[6].ToString();
                CheckBoxList2.SelectedValue = registro[7].ToString();
                if (registro[7].ToString() == "1")
                {
                    TextBox471.Enabled = true;
                }
                TextBox478.Text = registro[8].ToString();
               // TextBox472.Text = Convert.ToDateTime(registro[9].ToString()).ToShortDateString();  // fecha accidente
                TextBox472.Text = registro[9].ToString();  // fecha accidente
                
                TextBox473.Text = registro[10].ToString();
                
                // datos del accidente
                dllDepartamento0.SelectedValue = registro[11].ToString();
                Procedimientos.LlenarCombos(ddlMunicipio0, new ClinicaCES.Logica.LMaestros().ListarMunicipios(dllDepartamento0.SelectedValue), "CODMUN", "MUN");
                ddlMunicipio0.SelectedValue = registro[12].ToString();
                
                
                // datos del conductor

                dllDepartamento1.SelectedValue = registro[23].ToString();
                Procedimientos.LlenarCombos(ddlMunicipio1, new ClinicaCES.Logica.LMaestros().ListarMunicipios(dllDepartamento1.SelectedValue), "CODMUN", "MUN");
                ddlMunicipio1.SelectedValue = registro[24].ToString();

                TextBox476.Text = registro[13].ToString();
                TextBox477.Text = registro[14].ToString();
                if (registro[14].ToString() == "" || registro[14].ToString() == "               ")
                {
                    CheckBox1.Checked = true;
                }
                TextBox1.Text = registro[15].ToString();
                TextBox2.Text = registro[16].ToString();
                TextBox3.Text = registro[17].ToString();
                TextBox4.Text = registro[18].ToString();
                RadioButtonList2.SelectedValue = registro[19].ToString();
                TextBox5.Text = registro[20].ToString();
                TextBox479.Text = registro[21].ToString();
                TextBox480.Text = registro[22].ToString();
               
                ViewState["tipoId"] = registro[25].ToString();
                DataTable dt = new ClinicaCES.Logica.LMaestros().ListarProfesionales();
                RadioButtonList3.SelectedValue = "4";
                if (dt.Rows.Count > 0)
                {
                    ViewState["dtInformes"] = dt;
                    Procedimientos.LlenarGrid(dt, gvInforme);
                }
            }

        }
        catch (Exception ex)
        {
            msg = "69";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }

    }

    protected void Button1_Click(object sender, EventArgs e)
    {
        string msg = "";
        try
        {
            if (validar())
            {
                System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
                string[] css = { "form_input", "invalidtxt" };
                if (DropDownList1.SelectedValue == "00" && DropDownList2.SelectedValue == "00")
                {
                    DropDownList2.CssClass = css[1];
                    msg = "81";
                    Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                }
                else
                {
                    DropDownList2.CssClass = css[0];
                    DataRow Cargo = new ClinicaCES.Logica.LMaestros().generarSolicitudAneSoat().Rows[0];
                    string convenio = traerCorreo();
                    if (convenio != "")
                    {
                        //string html = textoTilde(retornarHtml()) + textoTilde(retonrarHtml2());
                        //var htmlContent = String.Format(html);
                        //var htmlToPdf = new NReco.PdfGenerator.HtmlToPdfConverter();
                        string pApellido = "";
                        string sApellido = "";
                        string pNombre = "";
                        string sNombre = "";
                        if (TextBox75.Text != "") { pApellido = TextBox75.Text.Substring(0, TextBox75.Text.Length - TextBox75.Text.Length + 1).Trim(); }
                        if (TextBox76.Text != "") { sApellido = TextBox76.Text.Substring(0, TextBox76.Text.Length - TextBox76.Text.Length + 1).Trim(); }
                        if (TextBox77.Text != "") { pNombre = TextBox77.Text.Substring(0, TextBox77.Text.Length - TextBox77.Text.Length + 1).Trim(); }
                        if (TextBox78.Text != "") { sNombre = TextBox78.Text.Substring(0, TextBox78.Text.Length - TextBox78.Text.Length + 1).Trim(); }
                        string archivo = ViewState["tipoId"].ToString() + "-" + TextBox81.Text.Trim() + "-" + pApellido + sApellido + pNombre + sNombre + "-" + Cargo[0].ToString();
                                             
                        
                        //string Mensaje = construirCorreo();
                        string Mensaje = ""; 
                        
                        //htmlToPdf.GeneratePdf(htmlContent, null, Server.MapPath("../AUTPDF/" + archivo + "_AnexoSoat.pdf"));
                        //if (enviar(convenio, Mensaje, archivo + "_AnexoSoat.pdf", Server.MapPath("../AUTPDF/" + archivo + "_AnexoSoat.pdf")))
                        //{
                            string[] Filtros = Procedimientos.descifrar(Request.QueryString["cookieSoat"]).Split('|');

                        // consumo de siras
                            string[] Respuesta = Siras().ToString().Replace("\r\n", "").Replace("\"", "").Replace("{", "").Replace("}", "").Trim().Split(':');

                            if (Respuesta[0] == "Radicado")
                            {

                                if (new ClinicaCES.Logica.LAnexos().AnexoSoatCrear(Filtros[0].Split(';'), "", Cargo[0].ToString(), Convert.ToDateTime(TextBox472.Text), TextBox473.Text, convenio, Mensaje, "", "4", archivo + "_AnexoSoat.pdf", Server.MapPath("../AUTPDF/" + archivo + "_Anexo3.pdf"), Session["Nick1"].ToString(), Respuesta[1].ToString()))
                                {
                                    msg = "1";
                                    inactivarControles();
                                }
                                else
                                {
                                    msg = "3";
                                    
                                }
                                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                            }
                            else
                            {
                                Procedimientos.Script(Respuesta[1], litScript);
                            }
                    }
                    else
                    {
                        msg = "78";
                        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
                    }
                }
            }
            else
            {
                msg = "68";
                Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            }
        }
        catch
        {
            Button1.Enabled = false;
            msg = "71";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }

    }


    protected string TipoDocumento(string Dato) {


        string TipoDoc = "";

        switch (Dato)
        {
            case "P":
                TipoDoc = "PA";
                break;
            case "3":
                TipoDoc = "TI";
                break;
            case "10":
                TipoDoc = "CD";
                break;
            case "4":
                TipoDoc = "CC";
                break;
            case "11":
                TipoDoc = "CN";
                break;
            case "5":
                TipoDoc = "CE";
                break;
            case "12":
                TipoDoc = "SC";
                break;
            case "13":
                TipoDoc = "PS";
                break;
        };
        return TipoDoc;
    }

    protected string Siras() {

        var httpWebRequest = (HttpWebRequest)WebRequest.Create("https://webservicesiras.fasecolda.com/sirasserv/api/registraratencion");
        httpWebRequest.ContentType = "application/x-www-form-urlencoded";
        httpWebRequest.Headers.Add("SIRAS-API-TOKEN", "UkRhcnNMVE9CSUZtSGFoaFE3T2NkdjUzNWlQSTE1KzlYbG52S1BLeHp3bz06c2lyYXNrZXktMDUwMDEwMjEyNDAxLVdDS0taVjoyNDg3OjYzNjUwMjc1MjAwMDAwMDAwMA==");
        httpWebRequest.Headers.Add("SIRAS-API-KEY", "siraskey-050010212401-WCKKZV");

        httpWebRequest.Method = "POST";
        var result = "";
     



            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {

                string VehiculoIdent = "0";

                if (TextBox477.Text.Trim() == "")
                    VehiculoIdent = "1";

                string Depto = "  ";
                string Municipio = "     ";
               // string Municipio = "";

                if (RadioButtonList4.SelectedValue.ToString().Trim() == "1")
                {

                    Depto = dllDepartamento.SelectedValue.ToString().Trim();
                    Municipio = ddlMunicipio.SelectedValue.ToString().Trim();

                }



                string json = "{\"Victima\": {    \"TipoIdentificacion\": \"" + TipoDocumento(rblTipoId.SelectedValue.ToString()) + "\",    \"Identificacion\": \"" + TextBox81.Text.Trim() + "\",    \"PrimerApellido\": \"" + TextBox75.Text.Trim() + "\",    \"SegundoApellido\": \"" + TextBox76.Text.Trim() + "\", " +
                                "\"PrimerNombre\": \"" + TextBox77.Text.Trim() + "\",    \"SegundoNombre\": \"" + TextBox78.Text.Trim() + "\",    \"FechaNacimiento\": \"" + TextBox102.Text.Trim().Replace('/', '-') + "\"  }, " +

                                "\"Ingreso\": {    \"TipoIngreso\": " + RadioButtonList1.SelectedValue.ToString().Trim() + ",    \"Fecha\": \"" + txtFecha.Text.Trim().Replace('/', '-') + "\", " +
                                "\"Hora\": \"" + DropDownList1.SelectedValue.ToString().Trim() + ":" + DropDownList2.SelectedValue.ToString().Trim() + "\",     \"EsVictimaRemitida\": " + RadioButtonList4.SelectedValue.ToString().Trim() + ",    \"RazonSocialRemitente\": \"" + TextBox470.Text.Trim() + "\",     \"CodigoHabilitacionRemitente\": \"" + TextBox470.Text.Trim() + "\",     \"CodigoDepartamentoRemitente\": \"" + Depto + "\", " +
                                "\"CodigoMunicipioRemitente\": \"" + Municipio + "\"   },  " +

                                "\"TransporteEspecial\": {    \"TrasladoTransporteEspecial\":" + CheckBoxList2.SelectedValue.ToString().Trim() + ",    \"CodigoPrestador\": \"" + TextBox471.Text.Trim() + "\",    \"PlacaTransporte\": \"" + TextBox478.Text.Trim() + "\"  }, " +

                                "\"Accidente\": {     \"Fecha\": \"" + TextBox472.Text.Trim().Replace('/', '-') + "\",   \"Hora\": \"" + TextBox473.Text.Trim() + "\",    \"Direccion\": \"" + TextBox476.Text.Trim() + "\",    \"CodigoDepartamento\": \"" + dllDepartamento0.SelectedValue.ToString().Trim() + "\",    \"CodigoMunicipio\": \"" + ddlMunicipio0.SelectedValue.ToString().Trim() + "\", " +
                                "\"EsVehiculoIdentificado\": " + VehiculoIdent + ",    \"Placa\": \"" + TextBox477.Text.Trim() + "\"  }, " +

                                "\"Conductor\": {    \"PrimerApellido\": \"" + TextBox1.Text.Trim() + "\",    \"SegundoApellido\": \"" + TextBox2.Text.Trim() + "\",    \"PrimerNombre\": \"" + TextBox3.Text.Trim() + "\",    \"SegundoNombre\": \"" + TextBox4.Text.Trim() + "\", " +
                                " \"TipoIdentificacion\": \"" + TipoDocumento(RadioButtonList2.SelectedValue.ToString()) + "\",    \"Identificacion\": \"" + TextBox5.Text.Trim() + "\",    \"Direccion\": \"" + TextBox479.Text.Trim() + "\",    \"Telefono\": \"" + TextBox480.Text.Trim() + "\",    \"CodigoDepartamento\": \"" + dllDepartamento1.SelectedValue.ToString().Trim() + "\",    \"CodigoMunicipio\": \"" + ddlMunicipio1.SelectedValue.ToString().Trim() + "\"  }, " +

                                "\"Reporta\": {     \"TipoIdentificacion\": \"CC\",    \"Identificacion\": \"890982608\",    \"CodigoPrestador\": \"050010212401\"  }}";


                
                streamWriter.Write(json);
                streamWriter.Flush();
                streamWriter.Close();
            }

       try
        {
            var httpResponse = (HttpWebResponse)httpWebRequest.GetResponse();
            using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
            {
                return result = streamReader.ReadToEnd();
            }

        }
        catch(Exception ex) {

            return Convert.ToString(ex);        
        }
        
    
    }




    protected bool validar()
    {
        bool Valido = true;

        System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
        string[] css = { "form_input", "invalidtxt" };


        if (string.IsNullOrEmpty(RadioButtonList1.SelectedValue.ToString().Trim()))
        {
            Valido = false;

            RadioButtonList1.CssClass = css[1];
        }
        else
        {
            RadioButtonList1.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(txtFecha.Text.Trim()))
        {
            Valido = false;
            txtFecha.CssClass = css[1];
        }
        else
        {
            txtFecha.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(RadioButtonList4.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            RadioButtonList4.CssClass = css[1];
        }
        else
        {
            RadioButtonList4.CssClass = css[0];
            if (RadioButtonList4.SelectedValue.ToString().Trim() == "1")
            {
                if (string.IsNullOrEmpty(TextBox469.Text.Trim()))
                {
                    Valido = false;
                    TextBox469.CssClass = css[1];
                }
                else
                {
                    TextBox469.CssClass = css[0];
                }
                if (string.IsNullOrEmpty(TextBox470.Text.Trim()))
                {
                    Valido = false;
                    TextBox470.CssClass = css[1];
                }
                else
                {
                    TextBox470.CssClass = css[0];
                }
                if (string.IsNullOrEmpty(dllDepartamento.SelectedValue.ToString().Trim()) || dllDepartamento.SelectedValue.ToString().Trim() == "-1")
                {
                    Valido = false;

                    dllDepartamento.CssClass = css[1];
                }
                else
                {
                    dllDepartamento.CssClass = css[0];
                }
                if (string.IsNullOrEmpty(ddlMunicipio.SelectedValue.ToString().Trim()) || ddlMunicipio.SelectedValue.ToString().Trim() == "-1")
                {
                    Valido = false;

                    ddlMunicipio.CssClass = css[1];
                }
                else
                {
                    ddlMunicipio.CssClass = css[0];
                }
            }
        }
        if (string.IsNullOrEmpty(CheckBoxList2.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            CheckBoxList2.CssClass = css[1];
        }
        else
        {
            CheckBoxList2.CssClass = css[0];
            if (CheckBoxList2.SelectedValue.ToString().Trim() == "1")
            {
                if (string.IsNullOrEmpty(TextBox471.Text.Trim()))
                {
                    Valido = false;
                    TextBox471.CssClass = css[1];
                    TextBox471.Enabled = true;
                }
                else
                {
                    TextBox471.CssClass = css[0];
                    TextBox471.Enabled = false;
                }
                if (string.IsNullOrEmpty(TextBox478.Text.Trim()))
                {
                    Valido = false;
                    TextBox478.CssClass = css[1];
                }
                else
                {
                    TextBox478.CssClass = css[0];

                }
            }
        }

        if (string.IsNullOrEmpty(TextBox489.Text.Trim()))
        {
            Valido = false;
            TextBox489.CssClass = css[1];
            if (string.IsNullOrEmpty(TextBox490.Text.Trim()))
            {
                Valido = false;
                TextBox490.CssClass = css[1];
            }
            else
            {
                TextBox490.CssClass = css[0];
            }
        }
        else
        {
            TextBox489.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(TextBox491.Text.Trim()))
        {
            Valido = false;
            TextBox491.CssClass = css[1];
            if (string.IsNullOrEmpty(TextBox492.Text.Trim()))
            {
                Valido = false;
                TextBox492.CssClass = css[1];
            }
            else
            {
                TextBox492.CssClass = css[0];
            }
        }
        else
        {
            TextBox491.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(RadioButtonList3.SelectedValue.ToString().Trim()))
        {
            Valido = false;
            RadioButtonList3.CssClass = css[1];
        }
        else
        {
            RadioButtonList3.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(TextBox493.Text.Trim()))
        {
            Valido = false;
            TextBox493.CssClass = css[1];
        }
        else
        {
            TextBox493.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(TextBox494.Text.Trim()))
        {
            Valido = false;
            TextBox494.CssClass = css[1];
        }
        else
        {
            TextBox494.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(TextBox495.Text.Trim()))
        {
            Valido = false;
            TextBox495.CssClass = css[1];
        }
        else
        {
            TextBox495.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(TextBox496.Text.Trim()))
        {
            Valido = false;
            TextBox496.CssClass = css[1];
        }
        else
        {
            TextBox496.CssClass = css[0];
        }
        return Valido;
    }
    protected void AgregarPrintScript(string ruta)
    {
        string msg = "";
        try
        {
            Response.Clear();
            Response.AddHeader("content-disposition", String.Format("attachment;filename={0}", ruta));
            Response.ContentType = "application/pdf";
            Response.WriteFile(Server.MapPath(Path.Combine("~/AUTPDF", ruta)));
            Response.End();
        }
        catch
        {
            Button1.Enabled = false;
            msg = "74";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        }
    }
    protected string construirCorreo()
    {
        string mensajeAdd = "";
        if (TextBox497.Text != "")
        {
            mensajeAdd = "Nota: " + TextBox497.Text;
        }
        string html1 = " <!DOCTYPE html>  " +
" <html xmlns='http://www.w3.org/1999/xhtml'>  " +
" <head><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title>  " +
" </title>  " +
" <style type='text/css'>  " +
" p.MsoNormal  " +
" 	{margin-bottom:.0001pt;  " +
" 	font-size:11.0pt;  " +
" 	font-family:'Calibri',sans-serif;  " +
"	        margin-left: 0cm;  " +
" margin-right: 0cm;  " +
" margin-top: 0cm;  " +
" }  " +
" .auto-style1 {  " +
" width: 100%;  " +
" }  " +
" table.MsoNormalTable  " +
" 	{font-size:11.0pt;  " +
" 	font-family:'Calibri',sans-serif;  " +
" 	}  " +
" </style>  " +
" </head>  " +
" <body>  " +
" <form name='form2' method='post' action='CorreoAnexo3_1.aspx' id='form2'>  " +
" <div>  " +
" <input type='hidden' name='__VIEWSTATE' id='__VIEWSTATE' value='/wEPDwUKLTEzNDM3NzkxOWRktFqUC1uh54j6j8uP3USpOlPZPsc=' />  " +
" </div>  " +
" <div>  " +
" 	<input type='hidden' name='__VIEWSTATEGENERATOR' id='__VIEWSTATEGENERATOR' value='8B02451E' />  " +
" </div>  " +
" <div>  " +
" <p class='MsoNormal'>  " +
" <a name='OLE_LINK1'><span style='mso-bookmark:OLE_LINK2'><span style='mso-bookmark:  " +
" OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordial saludo,<o:p></o:p></span></span></span></span></span></a></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-size:11.0pt;line-height:107%;  " +
" font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:  " +
" Calibri;mso-fareast-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;  " +
" mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;  " +
" mso-ansi-language:ES-CO;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'>Dando cumplimiento a la resolución 3823 de 2016, se envía anexo - INFORME DE LA ATENCIÓN EN SALUD PRESTADA A VÍCTIMAS DE ACCIDENTES DE TRÁNSITO,  del paciente relacionado en el asunto</span></p>  " +
"<p></p> " +
"<p><o:p>&nbsp;</o:p></p> " +
" <p class='MsoNormal'>  " +
" <span style='font-size:11.0pt;line-height:107%;  " +
" font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:  " +
" Calibri;mso-fareast-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;  " +
" mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;  " +
" mso-ansi-language:ES-CO;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'>" + mensajeAdd + "</span></p>  " +
"<p><span></span></p> " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" Quedamos atentos a su gestión.<o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordialmente,<o:p></o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <o:p></o:p>  " +
" <table class='auto-style1'>  " +
" <tr>  " +
" <td>&nbsp;</td>  " +
" <td>&nbsp;</td>  " +
" </tr>  " +
" <tr>  " +
" <td>&nbsp;</td>  " +
" <td>&nbsp;</td>  " +
" </tr>  " +
" </table>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <table border='0' cellpadding='0' cellspacing='0' class='MsoNormalTable'>  " +
" <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;  " +
" height:61.9pt'>  " +
" <td style='width:68.25pt;padding:0cm 5.4pt 0cm 5.4pt;  " +
" height:61.9pt' valign='top' width='91'>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;mso-fareast-language:ES-CO;mso-no-proof:  " +
" yes'><![if !vml]>  " +
" <img src=cid:companylogo style='height: 79px; width: 58px'><![endif]></span><o:p></o:p></p>  " +
" </td>  " +
" <td style='width:366.75pt;padding:0cm 5.4pt 0cm 5.4pt;height:61.9pt' width='489'>  " +
" <p class='MsoNormal'>  " +
" <b><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#00B0F0;  " +
" mso-no-proof:yes'>Autorizaciones</span></b><o:p></o:p></p>  " +
" <u1:p></u1:p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;  " +
" mso-no-proof:yes'>Central de Autorizaciones </span><span style='font-family:  " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:  " +
" minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:  " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:  " +
" minor-fareast;color:#1F4E79;mso-no-proof:yes'> Clínica CES<u1:p></u1:p></span><o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;  " +
" mso-no-proof:yes'>Tel: 5767272 ext 7630</span><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;  " +
" color:#00B0F0;mso-no-proof:yes'> |</span><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;  " +
" color:#1F4E79;mso-no-proof:yes'> Carrera 50c #58-12 </span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;color:#1F4E79;mso-no-proof:yes'> Medellín, Colombia<u1:p></u1:p></span><o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;mso-no-proof:yes'><a href='http://www.clinicaces.com/'><i><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" color:#00B0F0'>www.clinicaces.com</span></i></a></span><o:p></o:p></p>  " +
" </td>  " +
" </tr>  " +
" <u1:p></u1:p>  " +
" </table>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" </div>  " +
" </form>  " +
" </body>  " +
" </html>  ";
        return html1;

    }
    protected string construirCorreo2()
    {
        string nomPaciente = "";
        string html2 = " <!DOCTYPE html>  " +
" <html xmlns='http://www.w3.org/1999/xhtml'>  " +
" <head id='Head1'><meta http-equiv='Content-Type' content='text/html; charset=utf-8' /><title>  " +
" </title>  " +
" <style type='text/css'>  " +
" p.MsoNormal  " +
" 	{margin-bottom:.0001pt;  " +
" 	font-size:11.0pt;  " +
" 	font-family:'Calibri',sans-serif;  " +
" 	        margin-left: 0cm;  " +
" margin-right: 0cm;  " +
" margin-top: 0cm;  " +
" }  " +
" .auto-style1 {  " +
" width: 100%;  " +
" }  " +
" table.MsoNormalTable  " +
" 	{font-size:11.0pt;  " +
" 	font-family:'Calibri',sans-serif;  " +
" 	}  " +
" </style>  " +
" </head>  " +
" <body>  " +
" <form name='form2' method='post' action='CorreoAnexo3_2.aspx' id='form2'>  " +
" <div>  " +
" <input type='hidden' name='__VIEWSTATE' id='__VIEWSTATE' value='/wEPDwUKLTEzNDM3NzkxOWRk6XQ33Nj/m6Y4Tpi0YWt/0sgWDZw=' />  " +
" </div>  " +
" <div>  " +
" 	<input type='hidden' name='__VIEWSTATEGENERATOR' id='__VIEWSTATEGENERATOR' value='C65AAC44' />  " +
" </div>  " +
" <div>  " +
" <p class='MsoNormal'>  " +
" <a name='OLE_LINK1'><span style='mso-bookmark:OLE_LINK2'><span style='mso-bookmark:  " +
" OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordial saludo,<o:p></o:p></span></span></span></span></span></a></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-size:11.0pt;line-height:107%;  " +
" font-family:&quot;Calibri&quot;,sans-serif;mso-ascii-theme-font:minor-latin;mso-fareast-font-family:  " +
" Calibri;mso-fareast-theme-font:minor-latin;mso-hansi-theme-font:minor-latin;  " +
" mso-bidi-font-family:&quot;Times New Roman&quot;;mso-bidi-theme-font:minor-bidi;  " +
" mso-ansi-language:ES-CO;mso-fareast-language:EN-US;mso-bidi-language:AR-SA'>Dando cumplimiento al decreto 4747 de 2007 y la resolución 3047 de 2008, se envía anexo técnico N° 3 solicitando autorización para " + nomPaciente + " del paciente relacionado en el asunto.</span></p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" <p class='MsoNormal'>  " +
" Con soportes de envío se continuará con la atención del paciente; correo que a su vez será enviado a ente territorial como lo menciona la norma.<o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'><o:p>&nbsp;</o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" Quedamos atentos a su gestión.<o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-bookmark:OLE_LINK1'><span style='mso-bookmark:  " +
" OLE_LINK2'><span style='mso-bookmark:OLE_LINK3'><span style='mso-bookmark:OLE_LINK4'><span style='mso-bookmark:OLE_LINK5'><span style='mso-bookmark:OLE_LINK6'>Cordialmente,<o:p></o:p></span></span></span></span></span></span></p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <o:p></o:p>  " +
" <table class='auto-style1'>  " +
" <tr>  " +
" <td>&nbsp;</td>  " +
" <td>&nbsp;</td>  " +
" </tr>  " +
" <tr>  " +
" <td>&nbsp;</td>  " +
" <td>&nbsp;</td>  " +
" </tr>  " +
" </table>  " +
" <p class='MsoNormal'>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" <table border='0' cellpadding='0' cellspacing='0' class='MsoNormalTable'>  " +
" <tr style='mso-yfti-irow:0;mso-yfti-firstrow:yes;mso-yfti-lastrow:yes;  " +
" height:61.9pt'>  " +
" <td style='width:68.25pt;padding:0cm 5.4pt 0cm 5.4pt;  " +
" height:61.9pt' valign='top' width='91'>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;mso-fareast-language:ES-CO;mso-no-proof:  " +
" yes'><![if !vml]>  " +
" <img src=cid:companylogo style='height: 79px; width: 58px'><![endif]></span><o:p></o:p></p>  " +
" </td>  " +
" <td style='width:366.75pt;padding:0cm 5.4pt 0cm 5.4pt;height:61.9pt' width='489'>  " +
" <p class='MsoNormal'>  " +
" <b><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#00B0F0;  " +
" mso-no-proof:yes'>Autorizaciones</span></b><o:p></o:p></p>  " +
" <u1:p></u1:p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;  " +
" mso-no-proof:yes'>Central de Autorizaciones </span><span style='font-family:  " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:  " +
" minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:  " +
" &quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:  " +
" minor-fareast;color:#1F4E79;mso-no-proof:yes'> Clínica CES<u1:p></u1:p></span><o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:  " +
" &quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;color:#1F4E79;  " +
" mso-no-proof:yes'>Tel: 5767272 ext 7470</span><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;  " +
" color:#00B0F0;mso-no-proof:yes'> |</span><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" mso-fareast-font-family:&quot;Times New Roman&quot;;mso-fareast-theme-font:minor-fareast;  " +
" color:#1F4E79;mso-no-proof:yes'> Carrera 50c #58-12 </span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;color:#00B0F0;mso-no-proof:yes'>|</span><span style='font-family:&quot;Arial&quot;,sans-serif;mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;color:#1F4E79;mso-no-proof:yes'> Medellín, Colombia<u1:p></u1:p></span><o:p></o:p></p>  " +
" <p class='MsoNormal'>  " +
" <span style='mso-fareast-font-family:&quot;Times New Roman&quot;;  " +
" mso-fareast-theme-font:minor-fareast;mso-no-proof:yes'><a href='http://www.clinicaces.com/'><i><span style='font-family:&quot;Arial&quot;,sans-serif;  " +
" color:#00B0F0'>www.clinicaces.com</span></i></a></span><o:p></o:p></p>  " +
" </td>  " +
" </tr>  " +
" <u1:p></u1:p>  " +
" </table>  " +
" <o:p></o:p>  " +
" </p>  " +
" <p class='MsoNormal'>  " +
" &nbsp;</p>  " +
" </div>  " +
" </form>  " +
" </body>  " +
" </html>  ";
        return html2;
    }
    protected string traerCorreo()
    {
        string msg = "";
        try
        {
            string[] Filtros = Procedimientos.descifrar(Request.QueryString["cookieSoat"]).Split('|');
            DataRow correo = new ClinicaCES.Logica.LBusquedaPacientes().traerCorreoSoat(Filtros[0].ToString(), Filtros[1].ToString()).Rows[0];
            return correo[0].ToString();
        }
        catch
        {
            msg = "72";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            return "";
        }
    }
    //protected string  agrupar()
    //{
    //    string Codigo = "";
    //    string Descripcion = "";
    //    //if (TextBox166.Text != "") { Codigo = TextBox166.Text; Descripcion = TextBox173.Text; }
    //    //if (TextBox177.Text != "") { Codigo = Codigo + "|" + TextBox177.Text; Descripcion = Descripcion + "|" + TextBox450.Text; }
    //    //if (TextBox188.Text != "") { Codigo = Codigo + "|" + TextBox188.Text; Descripcion = Descripcion + "|" + TextBox431.Text; }
    //    //if (TextBox195.Text != "") { Codigo = Codigo + "|" + TextBox195.Text; Descripcion = Descripcion + "|" + TextBox432.Text; }
    //    //if (TextBox202.Text != "") { Codigo = Codigo + "|" + TextBox202.Text; Descripcion = Descripcion + "|" + TextBox433.Text; }
    //    //if (TextBox209.Text != "") { Codigo = Codigo + "|" + TextBox209.Text; Descripcion = Descripcion + "|" + TextBox434.Text; }
    //    //if (TextBox216.Text != "") { Codigo = Codigo + "|" + TextBox216.Text; Descripcion = Descripcion + "|" + TextBox435.Text; }
    //    //if (TextBox223.Text != "") { Codigo = Codigo + "|" + TextBox223.Text; Descripcion = Descripcion + "|" + TextBox436.Text; }
    //    //if (TextBox230.Text != "") { Codigo = Codigo + "|" + TextBox230.Text; Descripcion = Descripcion + "|" + TextBox437.Text; }
    //    //if (TextBox237.Text != "") { Codigo = Codigo + "|" + TextBox237.Text; Descripcion = Descripcion + "|" + TextBox438.Text; }
    //    //if (TextBox244.Text != "") { Codigo = Codigo + "|" + TextBox244.Text; Descripcion = Descripcion + "|" + TextBox439.Text; }
    //    //if (TextBox251.Text != "") { Codigo = Codigo + "|" + TextBox251.Text; Descripcion = Descripcion + "|" + TextBox440.Text; }
    //    //if (TextBox258.Text != "") { Codigo = Codigo + "|" + TextBox258.Text; Descripcion = Descripcion + "|" + TextBox441.Text; }
    //    //if (TextBox265.Text != "") { Codigo = Codigo + "|" + TextBox265.Text; Descripcion = Descripcion + "|" + TextBox442.Text; }
    //    //if (TextBox272.Text != "") { Codigo = Codigo + "|" + TextBox272.Text; Descripcion = Descripcion + "|" + TextBox443.Text; }
    //    //if (TextBox279.Text != "") { Codigo = Codigo + "|" + TextBox279.Text; Descripcion = Descripcion + "|" + TextBox444.Text; }
    //    //if (TextBox286.Text != "") { Codigo = Codigo + "|" + TextBox286.Text; Descripcion = Descripcion + "|" + TextBox445.Text; }
    //    //if (TextBox293.Text != "") { Codigo = Codigo + "|" + TextBox293.Text; Descripcion = Descripcion + "|" + TextBox446.Text; }
    //    //if (TextBox300.Text != "") { Codigo = Codigo + "|" + TextBox300.Text; Descripcion = Descripcion + "|" + TextBox447.Text; }
    //    //if (TextBox307.Text != "") { Codigo = Codigo + "|" + TextBox307.Text; Descripcion = Descripcion + "|" + TextBox448.Text; }
    //    return Codigo + ";" + Descripcion;
    //}
    protected bool enviar(string Para, string Mensaje, string Asunto, string ruta)
    {
        string msg = "";
        bool bandera = false;
        try
        {
            Correo correo = new Correo();
            correo.Para = Para;
            correo.Mensaje = Mensaje;
            correo.Asunto = Asunto;
            //correo.De = ConfigurationManager.AppSettings["mailSoporte"];
            //string path = Server.MapPath(@"..\AUTPDF\anexo2.pdf"); ;
            string[] result = new string[] { ruta };
            correo.Adjuntos = result;
            if (correo.Enviar())
            { bandera = true; }
            return bandera;
        }
        catch
        {
            msg = "73";
            Procedimientos.Script("Mensaje(" + msg + ")", litScript);
            return bandera;
        }
    }
    protected string textoTilde(string texto)
    {
        string nuevoTexto = "";
        string valor = "";
        for (int i = 0; i < texto.Length; i++)
        {
            valor = texto.Substring(i, 1);
            if (valor == "á") { valor = "&#225"; } else if (valor == "Á") { valor = "&#193"; }
            if (valor == "é") { valor = "&#233"; } else if (valor == "É") { valor = "&#201"; }
            if (valor == "í") { valor = "&#237"; } else if (valor == "Í") { valor = "&#205"; }
            if (valor == "ó") { valor = "&#243"; } else if (valor == "Ó") { valor = "&#211"; }
            if (valor == "ú") { valor = "&#250"; } else if (valor == "Ú") { valor = "&#218"; }
            if (valor == "ñ") { valor = "&#241"; } else if (valor == "Ñ") { valor = "&#209"; }
            nuevoTexto = nuevoTexto + valor;
        }
        return nuevoTexto;
    }
    protected string valorTamano()
    {
        int tam = 20;
        int maxCaracter = 100;
        //if (TextBox449.Text.Length > maxCaracter)
        //{
        //    while (TextBox449.Text.Length > maxCaracter)
        //    {
        //        maxCaracter = maxCaracter + 100;
        //        tam = tam + 20;
        //    }
        //}
        return tam.ToString();
    }
   // protected string retornarHtml()
   // {
   //     string html = "";
   //     try
   //     {
   //         string tam = valorTamano();
   //         string[] tipoId = { "", "", "", "", "", "", "", "", "" };
   //         string[] tipoIng = { "", "", "" };
   //         string[] Remision = { "", "" };
   //         string[] Traslado = { "", "" };

   //         string[] cobertura = { "", "", "", "", "", "", "", "" };
   //         string[] Atencion = { "", "", "", "", "" };
   //         string[] Servicio = { "", "" };
   //         string[] Prioridad = { "", "" };
   //         string[] Ubicacion = { "", "", "" };

   //         if (rblTipoId.SelectedValue == "2") { tipoId[0] = "checked='checked'"; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; tipoId[7] = ""; tipoId[8] = ""; }
   //         if (rblTipoId.SelectedValue == "P") { tipoId[0] = ""; tipoId[1] = "checked='checked'"; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; tipoId[7] = ""; tipoId[8] = ""; }
   //         if (rblTipoId.SelectedValue == "3") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = "checked='checked'"; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; tipoId[7] = ""; tipoId[8] = ""; }
   //         if (rblTipoId.SelectedValue == "10") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = "checked='checked'"; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; tipoId[7] = ""; tipoId[8] = ""; }
   //         if (rblTipoId.SelectedValue == "4") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = "checked='checked'"; tipoId[5] = ""; tipoId[6] = ""; tipoId[7] = ""; tipoId[8] = ""; }
   //         if (rblTipoId.SelectedValue == "11") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = "checked='checked'"; tipoId[6] = ""; tipoId[7] = ""; tipoId[8] = ""; }
   //         if (rblTipoId.SelectedValue == "5") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = "checked='checked'"; tipoId[7] = ""; tipoId[8] = ""; }
   //         if (rblTipoId.SelectedValue == "12") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; tipoId[7] = "checked='checked'"; tipoId[8] = ""; }
   //         if (rblTipoId.SelectedValue == "13") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; tipoId[7] = ""; tipoId[8] = "checked='checked'"; }
   //         if (RadioButtonList1.SelectedValue == "1") { tipoIng[0] = "checked='checked'"; tipoIng[1] = ""; tipoIng[2] = ""; }
   //         if (RadioButtonList1.SelectedValue == "2") { tipoIng[0] = ""; tipoIng[1] = "checked='checked'"; tipoIng[2] = ""; }
   //         if (RadioButtonList1.SelectedValue == "3") { tipoIng[0] = ""; tipoIng[1] = ""; tipoIng[2] = "checked='checked'"; }
   //         if (RadioButtonList4.SelectedValue == "1") { Remision[0] = "checked='checked'"; Remision[1] = ""; }
   //         if (RadioButtonList4.SelectedValue == "2") { Remision[0] = ""; Remision[1] = "checked='checked'"; }
   //         if (CheckBoxList2.SelectedValue == "1") { Traslado[0] = "checked='checked'"; Traslado[1] = ""; }
   //         if (CheckBoxList2.SelectedValue == "2") { Traslado[0] = ""; Traslado[1] = "checked='checked'"; }

   //         string path1 = Server.MapPath("../img/escudo-minsalud.jpg");
   //         string path2 = Server.MapPath("../img/nuevopais.jpg");

   //         string Hora = DropDownList1.SelectedValue.ToString().Trim().ToUpper() + ":" + DropDownList2.SelectedItem.Text.Trim().ToUpper();

   //         string Departamento = "";
   //         if (dllDepartamento.SelectedValue.ToString() != "-1")
   //         {
   //             Departamento = dllDepartamento.SelectedItem.ToString().Trim().ToUpper();
   //         }
   //         string Municipio = "";
   //         if (ddlMunicipio.SelectedIndex.ToString() != "-1")
   //         {
   //             Municipio = ddlMunicipio.SelectedItem.ToString().Trim().ToUpper();
   //         }
   //         html = " <table style='border: 1px solid #000000; width: 1003px'> " +
   //" <tr> " +
   //" <td rowspan='4' style='width: 143px' align='center'> " +
   //" <img src='" + path1 + "' style='height:49px;width:106px;border-width:0px;' /> " +
   //" </td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td align='center' style='width: 704px'><B>MINISTERIO DE SALUD Y PROTECCION SOCIAL</B></td> " +
   //" <td align='center' rowspan='3'> " +
   //" <img src='" + path2 + "' style='height:49px;width:106px;border-width:0px;' /> " +
   //" </td> " +
   //" </tr> " +
   //"  " +
   //" <tr> " +
   //" <td align='center' style='width: 704px'><B>INFORME DE LA ATENCIÓN EN SALUD A VÍCTIMAS DE ACCIDENTE DE TRÁNSITO</B></td> " +
   //" </tr> " +
   //" </table> " +
   //" <p></p> " +
   //" <table style='border: 1px solid #000000; width: 1003px'> " +
   //" <tr> " +
   //" <td colspan='68' align='center' style='border-style: none none solid none; border-width: 1px; border-color: #000000;'><b >INFORMACIÓN DEL PRESTADOR</b></td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td style='width: 1836px' colspan='7'><b>Nombre o razón social:</b></td> " +
   //" <td colspan='61'><input name='ctl00$ContentPlaceHolder1$TextBox38' type='text' value='CORPORACION PARA ESTUDIOS EN SALUD CLINICA CES' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox38' disabled='disabled' class='form_input' style='width:392px;' /> " +
   //" </td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td colspan='7' style='width: 1836px'> " +
   //" <b>Tipo de Identificación:</b></td> " +
   //" <td> " +
   //" <b>NIT</b></td> " +
   //" <td style='width: 807px' align='left'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox25' type='text' value='X' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox25' disabled='disabled' class='form_input' style='font-size:Small;text-decoration:none;width:16px;text-align:center' /> " +
   //" </td> " +
   //" <td style='width: 807px' align='left'> " +
   //" Número</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td align='center' colspan='3'> " +
   //" DV</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td style='width: 807px' align='left' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td align='left' colspan='19'> " +
   //" <b>Código de Habilitación</b></td> " +
   //" <td colspan='7'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox41' type='text' value='0500102124' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox41' disabled='disabled' class='form_input' style='width:114px;text-align: center' /> " +
   //" </td> " +
   //" <td align='center' colspan='3'> " +
   //" &nbsp;</td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td colspan='7' style='width: 1836px'> " +
   //" &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </td> " +
   //" <td> " +
   //" <b>CC</b></td> " +
   //" <td> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox39' type='text' maxlength='1' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox39' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" </td> " +
   //" <td colspan='20'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox26' type='text' value='8' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox26' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox27' type='text' value='9' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox27' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox28' type='text' value='0' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox28' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox29' type='text' value='9' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox29' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox30' type='text' value='8' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox30' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox31' type='text' value='2' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox31' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox32' type='text' value='6' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox32' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox33' type='text' value='0' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox33' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox34' type='text' value='8' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox34' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox35' type='text' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox35' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox36' type='text' value='-' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox36' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" </td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox37' type='text' value='1' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox37' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" </td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='4'> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td align='center' colspan='3'> " +
   //" &nbsp;</td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td colspan='7' style='width: 1836px'> " +
   //" <b>Dirección del prestador:</b></td> " +
   //" <td colspan='45'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox66' type='text' value='CALLE 58 # 50C-2 PRADO CENTRO' id='ctl00_ContentPlaceHolder1_TextBox66' disabled='disabled' class='form_input' style='width:389px;' /> " +
   //" </td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='3'> " +
   //" &nbsp;</td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td colspan='4'> " +
   //" <b>Teléfono:</b></td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td colspan='17'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox460' type='text' value='5' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox460' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox461' type='text' value='7' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox461' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox462' type='text' value='6' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox462' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox463' type='text' value='7' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox463' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox464' type='text' value='2' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox464' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox465' type='text' value='7' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox465' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox466' type='text' value='2' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox466' disabled='disabled' class='form_input' style='font-size:Small;width:16px;text-align:center' /> " +
   //" </td> " +
   //" <td colspan='37'> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td> " +
   //" &nbsp;</td> " +
   //" <td colspan='3'> " +
   //" &nbsp;</td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td align='left'> " +
   //" &nbsp;</td> " +
   //" <td align='left'> " +
   //" &nbsp;</td> " +
   //" <td align='left'> " +
   //" &nbsp;</td> " +
   //" <td align='center' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td align='left' colspan='6'> " +
   //" <b>Número</b></td> " +
   //" <td align='center' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td align='center' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td align='center' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td align='center' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td align='center' colspan='2'> " +
   //" &nbsp;</td> " +
   //" <td align='left' colspan='4'> " +
   //" <b>Departamento:</b></td> " +
   //" <td align='left' colspan='28'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox67' type='text' value='ANTIOQUIA' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox67' disabled='disabled' class='form_input' style='width:114px;' /> " +
   //" </td> " +
   //" <td align='left' colspan='3'> " +
   //" &nbsp;</td> " +
   //" <td align='left'> " +
   //" &nbsp;</td> " +
   //" <td align='left'> " +
   //" <b>Municipio:</b></td> " +
   //" <td align='left' colspan='7'> " +
   //" &nbsp;&nbsp;<input name='ctl00$ContentPlaceHolder1$TextBox459' type='text' value='MEDELLIN' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox459' disabled='disabled' class='form_input' style='width:106px;' /> " +
   //" </td> " +
   //             /* " <td bgcolor='Gray' style='width: 71px'> " +
   //              " &nbsp;</td> " +
   //              " <td bgcolor='Gray' style='width: 71px'> " +
   //              " &nbsp;</td> " +
   //              " <td  bgcolor='Gray' style='width: 71px'> " +
   //              " &nbsp;</td> " +*/
   //" </tr> " +
   //" </table> " +
   //             // termina primera tabla
   //" <p></p> " +
   //             // empieza la segunda tabla 
   //" <table style='border: 1px solid #000000; width: 1003px'> " +
   //" <tr> " +
   //" <td colspan='8' align='center' style='border-color: #000000;  border-bottom-style: solid; border-bottom-width: 1px;'><B>DATOS DE LA VÍCTIMA DEL ACCIDENTE DE TRÁNSITO</b></td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox75' type='text' value='" + TextBox75.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox75' disabled='disabled' class='form_input' style='width:233px;' /> " +
   //" </td> " +
   //" <td> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox76' type='text' value='" + TextBox76.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox76' disabled='disabled' class='form_input' style='width:233px;' /> " +
   //" </td> " +
   //" <td colspan='3'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox77' type='text' value='" + TextBox77.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox77' disabled='disabled' class='form_input' style='width:233px;' /> " +
   //" </td> " +
   //" <td colspan='3'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox78' type='text' value='" + TextBox78.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox78' disabled='disabled' class='form_input' style='width:233px;' /> " +
   //" </td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td style='height: 23px'><b>Primer Apellido</b></td> " +
   //" <td style='height: 23px'><b>Segundo Apellido</b></td> " +
   //" <td style='height: 23px' colspan='3'><b>Primer Nombre</b></td> " +
   //" <td style='height: 23px' colspan='3'><b>Segundo Nombre</b></td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td colspan='8'><b>Tipo Documento de Identificación</b></td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td colspan='2' rowspan='4'> " +
   //" <table id='ctl00_ContentPlaceHolder1_rblTipoId' disabled='disabled' class='form_input' border='0' style='width:473px;'> " +
   //" 	<tr> " +
   //" 		<td><span disabled='disabled'> " +
   //"            <input id='ctl00_ContentPlaceHolder1_rblTipoId_0' type='radio' name='ctl00$ContentPlaceHolder1$rblTipoId' value='2' " + tipoId[0] + " disabled='disabled' /> " +
   //"            <label for='ctl00_ContentPlaceHolder1_rblTipoId_0'>Registro Civil</label></span></td><td><span disabled='disabled'> " +
   //"            <input id='ctl00_ContentPlaceHolder1_rblTipoId_1' type='radio' name='ctl00$ContentPlaceHolder1$rblTipoId' value='P' " + tipoId[1] + " disabled='disabled' /> " +
   //"            <label for='ctl00_ContentPlaceHolder1_rblTipoId_1'>Pasaporte</label></span></td>  " +
   //" 	</tr><tr> " +
   //" 		<td><span disabled='disabled'> " +
   //"        <input id='ctl00_ContentPlaceHolder1_rblTipoId_2' type='radio' name='ctl00$ContentPlaceHolder1$rblTipoId' value='3' " + tipoId[2] + " disabled='disabled' /> " +
   //"        <label for='ctl00_ContentPlaceHolder1_rblTipoId_2'>Tarjeta de Identidad</label></span></td><td><span disabled='disabled'> " +
   //"        <input id='ctl00_ContentPlaceHolder1_rblTipoId_3' type='radio' name='ctl00$ContentPlaceHolder1$rblTipoId' value='10' " + tipoId[3] + "  disabled='disabled' /> " +
   //"        <label for='ctl00_ContentPlaceHolder1_rblTipoId_3'>Carné diplomático</label></span></td>  " +
   //"	</tr><tr> " +
   //" 		<td><span disabled='disabled'> " +
   //"        <input id='ctl00_ContentPlaceHolder1_rblTipoId_4' type='radio' name='ctl00$ContentPlaceHolder1$rblTipoId' value='4' " + tipoId[4] + " disabled='disabled' /> " +
   //"        <label for='ctl00_ContentPlaceHolder1_rblTipoId_4'>Cédula de Ciudadania</label></span></td><td><span disabled='disabled'> " +
   //"        <input id='ctl00_ContentPlaceHolder1_rblTipoId_5' type='radio' name='ctl00$ContentPlaceHolder1$rblTipoId' value='11' " + tipoId[5] + " disabled='disabled' /> " +
   //"        <label for='ctl00_ContentPlaceHolder1_rblTipoId_5'>Certificado de nacido vivo - DANE</label></span></td>  " +
   //"	</tr><tr> " +
   //" 		<td><span disabled='disabled'> " +
   //"        <input id='ctl00_ContentPlaceHolder1_rblTipoId_6' type='radio' name='ctl00$ContentPlaceHolder1$rblTipoId' value='5' " + tipoId[6] + " disabled='disabled' /> " +
   //"        <label for='ctl00_ContentPlaceHolder1_rblTipoId_6'>Cédula de Extranjería</label></span></td><td><span disabled='disabled'> " +
   //"        <input id='ctl00_ContentPlaceHolder1_rblTipoId_7' type='radio' name='ctl00$ContentPlaceHolder1$rblTipoId' value='12' " + tipoId[7] + " disabled='disabled' /> " +
   //"        <label for='ctl00_ContentPlaceHolder1_rblTipoId_7'>Salvoconducto de permanencia</label></span></td>  " +
   //" 	</tr><tr>  " +
   //" 		<td><span disabled='disabled'> " +
   //"        <input id='ctl00_ContentPlaceHolder1_rblTipoId_8' type='radio' name='ctl00$ContentPlaceHolder1$rblTipoId' value='13' " + tipoId[8] + " disabled='disabled' /> " +
   //"        <label for='ctl00_ContentPlaceHolder1_rblTipoId_8'>Persona sin identificar</label></span></td><td></td>  " +
   //" 	</tr> " +
   //" </table> " +
   //" </td> " +
   //" <td>&nbsp;</td> " +
   //" <td colspan='5'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox81' type='text' value='" + TextBox81.Text.Trim().ToUpper() + "' maxlength='10' id='ctl00_ContentPlaceHolder1_TextBox81' disabled='disabled' class='form_input' style='width:210px;text-align:center' /> " +
   //" </td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td>&nbsp;</td> " +
   //" <td align='center' colspan='3'>Número Documento de Identificación</td> " +
   //" <td></td> " +
   //" <td></td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td>&nbsp;</td> " +
   //" <td>&nbsp;</td> " +
   //" <td colspan='2'>&nbsp;</td> " +
   //" <td></td> " +
   //" <td></td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td>&nbsp;</td> " +
   //" <td><b>Fecha de Nacimiento:</b></td> " +
   //" <td colspan='4'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox102' type='text' value='" + TextBox102.Text.Trim().ToUpper() + "' maxlength='10' id='ctl00_ContentPlaceHolder1_TextBox102' disabled='disabled' class='form_input' style='width:163px;text-align:center' /> " +
   //" </td> " +
   //" </tr> " +
   //" </table> " +
   //" <p></p> " +
   //" <table style='border: 1px solid #000000; width: 1003px'> " +
   //" <tr> " +
   //" <td align='center' colspan='20' style='border-bottom-style: solid; border-width: 1px; border-color: #000000'><B>TIPO DE INGRESO A LOS SERVICIOS DE SALUD</b></td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td align='center' colspan='20'> " +
   //" <table id='ctl00_ContentPlaceHolder1_RadioButtonList1' class='form_input' border='0' style='width:995px;'> " +
   //" 	<tr> " +
   //"		<td><input id='ctl00_ContentPlaceHolder1_RadioButtonList1_0' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList1' value='1' " + tipoIng[0] + " /> " +
   //" <label for='ctl00_ContentPlaceHolder1_RadioButtonList1_0'>Atención Inicial de Urgencias</label></td><td> " +
   //"    <input id='ctl00_ContentPlaceHolder1_RadioButtonList1_1' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList1' value='2' " + tipoIng[1] + " /> " +
   //"<label for='ctl00_ContentPlaceHolder1_RadioButtonList1_1'>Atención de Urgencias</label> " +
   //"</td><td><input id='ctl00_ContentPlaceHolder1_RadioButtonList1_2' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList1' value='3' " + tipoIng[2] + " /> " +
   //"<label for='ctl00_ContentPlaceHolder1_RadioButtonList1_2'>Atención Programada</label></td> " +
   //" 	</tr> " +
   //" </table> " +
   //" </td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td align='right' colspan='2'><b>Fecha:</b></td> " +
   //" <td style='width: 346px' align='left'> " +
   //" <input name='ctl00$ContentPlaceHolder1$txtFecha' type='text' id='ctl00_ContentPlaceHolder1_txtFecha' class='form_input' value='" + txtFecha.Text.Trim().ToUpper() + "' style='width:104px;' /> " +
   //" </td> " +
   //" <td align='center' style='width: 12px'> " +
   //" &nbsp;</td> " +
   //" <td align='center' colspan='9'>&nbsp;</td> " +
   //" <td align='right' colspan='2'><b>Hora:</b></td> " +
   //" <td align='left'> " +
   //"  <input name='ctl00$ContentPlaceHolder1$TextBox500' type='text' value='" + Hora + "' id='ctl00_ContentPlaceHolder1_TextBox500' disabled='disabled' class='form_input' style='width:100px;' />" +
   //" </td> " +
   //" <td align='center'>&nbsp;</td> " +
   //" <td align='center' colspan='3'>&nbsp;</td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td align='center' colspan='10'><B>Víctima viene remitida</B></td> " +
   //" <td align='center' colspan='10'> " +
   //" <table id='ctl00_ContentPlaceHolder1_RadioButtonList4' border='0' style='width:293px;'> " +
   //" 	<tr> " +
   //" 		<td><input id='ctl00_ContentPlaceHolder1_RadioButtonList4_0' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList4' value='1' " + Remision[0] + "  /> " +
   //" <label for='ctl00_ContentPlaceHolder1_RadioButtonList4_0'>SI</label></td><td> " +
   //"<input id='ctl00_ContentPlaceHolder1_RadioButtonList4_1' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList4' value='2' " + Remision[1] + " /> " +
   //" <label for='ctl00_ContentPlaceHolder1_RadioButtonList4_1'>NO</label></td> " +
   //" 	</tr> " +
   //" </table> " +
   //" </td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td align='center' colspan='10'><b>Razón social del prestador de servicios de salud que remite</b></td> " +
   //" <td align='center' colspan='5'><b> " +
   //" Código de habilitación</b></td> " +
   //" <td align='center' colspan='5'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox469' type='text' value='" + TextBox469.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox469' disabled='disabled' class='form_input' style='width:175px;' /> " +
   //" </td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td align='center' colspan='20'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox470' type='text' value='" + TextBox470.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox470' disabled='disabled' class='form_input' style='width:981px;' /> " +
   //" </td> " +
   //" </tr> " +
   //" <tr> " +
   //" <td><b>Departamento:</b></td> " +
   //" <td colspan='2'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox501' type='text' value='" + Departamento + "' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox501' disabled='disabled' class='form_input' style='width:114px;' />  " +
   //" </td> " +
   //" <td  colspan='2'>&nbsp;</td> " +
   //" <td >&nbsp;</td> " +
   //" <td>&nbsp;</td> " +
   //" <td>&nbsp;</td> " +
   //" <td>&nbsp;</td> " +
   //" <td>&nbsp;</td> " +
   //" <td>&nbsp;</td> " +
   //" <td bgcolor='White'>&nbsp;</td> " +
   //" <td colspan='2'><b>Municipio:</b></td> " +
   //" <td colspan='2'> " +
   //" <input name='ctl00$ContentPlaceHolder1$TextBox502' type='text' value='" + Municipio + "' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox502' disabled='disabled' class='form_input' style='width:114px;' />  " +
   //" </td> " +
   //             /* " <td colspan='2'>&nbsp;</td> " +
   //              " <td >&nbsp;</td> " +
   //              " <td >&nbsp;</td> " +*/
   //" </tr> " +
   //" </table> " +
   //" <p> " +
   //" </p> " +
   // " <table style='border: 1px solid #000000; width: 1003px'> " +
   // " <tr> " +
   // " <td colspan='2' align='center' style='border-bottom-style: solid; border-width: 1px; border-color: #000000'><B>INFORMACIÓN DEL TRANSPORTE AL PRIMER SITIO DE LA ATENCIÓN</B></td> " +
   // " </tr> " +
   // " <tr> " +
   // " <td align='center' style='width: 509px'><B>Victima fue trasladada en transporte especial de pacientes</B></td> " +
   // " <td align='center'> " +
   // " <table id='ctl00_ContentPlaceHolder1_CheckBoxList2' disabled='disabled' class='form_input' border='0' style='width:469px;'> " +
   // " 	<tr> " +
   // "		<td><span disabled='disabled'> " +
   // " <input id='ctl00_ContentPlaceHolder1_CheckBoxList2_0' type='checkbox' name='ctl00$ContentPlaceHolder1$CheckBoxList2$0' " + Traslado[0] + " disabled='disabled' /> " +
   // " <label for='ctl00_ContentPlaceHolder1_CheckBoxList2_0'>SI</label></span></td><td><span disabled='disabled'> " +
   // " <input id='ctl00_ContentPlaceHolder1_CheckBoxList2_1' type='checkbox' name='ctl00$ContentPlaceHolder1$CheckBoxList2$1' " + Traslado[1] + " disabled='disabled' /> " +
   // " <label for='ctl00_ContentPlaceHolder1_CheckBoxList2_1'>NO</label></span></td> " +
   // "	</tr> " +
   // " </table> " +
   // " </td> " +
   // " </tr> " +
   // " <tr> " +
   // " <td align='center' style='width: 509px'> " +
   // " <input name='ctl00$ContentPlaceHolder1$TextBox471' type='text' value='" + TextBox471.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox471' class='form_input' style='width:167px;' /> " +
   // " </td> " +
   // " <td align='center'> " +
   // " <input name='ctl00$ContentPlaceHolder1$TextBox478' type='text' value='" + TextBox478.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox478' disabled='disabled' class='form_input' style='width:84px;' /> " +
   // " </td> " +
   // " </tr> " +
   // " <tr> " +
   // " <td align='center' style='width: 509px'><B>Código del prestador de transporte especial</B></td> " +
   // " <td align='center'><b>Placa del vehiculo</b></td> " +
   // " </tr> " +
   // " </table> " +
   // " <p> " +
   // " </p> ";
   //     }
   //     catch (InvalidCastException e)
   //     {

   //     }
   //     return html;
   // }
//    protected string retonrarHtml2()
//    {
//        string vehNoId = "";
//        string[] tipoId = { "", "", "", "", "", "", "" };
//        string[] tipoIdReporta = { "", "", "" };
//        if (RadioButtonList2.SelectedValue == "P") { tipoId[0] = "checked='checked'"; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
//        if (RadioButtonList2.SelectedValue == "10") { tipoId[0] = ""; tipoId[1] = "checked='checked'"; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
//        if (RadioButtonList2.SelectedValue == "3") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = "checked='checked'"; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
//        if (RadioButtonList2.SelectedValue == "12") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = "checked='checked'"; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = ""; }
//        if (RadioButtonList2.SelectedValue == "4") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = "checked='checked'"; tipoId[5] = ""; tipoId[6] = ""; }
//        if (RadioButtonList2.SelectedValue == "") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = "checked='checked'"; tipoId[6] = ""; }
//        if (RadioButtonList2.SelectedValue == "5") { tipoId[0] = ""; tipoId[1] = ""; tipoId[2] = ""; tipoId[3] = ""; tipoId[4] = ""; tipoId[5] = ""; tipoId[6] = "checked='checked'"; }
//        if (RadioButtonList3.SelectedValue == "3") { tipoIdReporta[0] = "checked='checked'"; tipoIdReporta[1] = ""; tipoIdReporta[2] = ""; }
//        if (RadioButtonList3.SelectedValue == "5") { tipoIdReporta[0] = ""; tipoIdReporta[1] = "checked='checked'"; tipoIdReporta[2] = ""; }
//        if (RadioButtonList3.SelectedValue == "4") { tipoIdReporta[0] = ""; tipoIdReporta[1] = ""; tipoIdReporta[2] = "checked='checked'"; }
//        if (CheckBox1.Checked == true) { vehNoId = "checked='checked'"; }
//        string html2 = " <table style='border: 1px solid #000000; width: 1003px'> " +
//" <tr> " +
//" <td colspan='17' align='center' style='border-bottom-style: solid; border-width: 1px; border-color: #000000'><B>DATOS DEL ACCIDENTE</B></td> " +
//" </tr> " +
//" <tr> " +
//" <td align='center'><b>Fecha:</b></td> " +
//" <td align='left'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox472' type='text' value='" + TextBox472.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox472' disabled='disabled' class='form_input' /> " +
//" </td> " +
//" <td colspan='2' align='center'><b>Hora:</b></td> " +
//" <td align='left'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox473' type='text' value='" + TextBox473.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox473' disabled='disabled' class='form_input' /> " +
//" </td> " +
//" <td align='center'>&nbsp;</td> " +
//" <td align='center'><b>Departamento</b></td> " +
//" <td align='left' colspan='2'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox474' type='text' value='" + TextBox474.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox474' disabled='disabled' class='form_input' /> " +
//" </td> " +
//            /*" <td bgcolor='Gray' align='center'>&nbsp;</td> " +
//            " <td bgcolor='Gray' align='center'>&nbsp;</td> " +*/
//" <td align='center'><b>Municipio:</b></td> " +
//            //" <td align='left' style='width: 9px'> " +
//" <td align='left' > " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox475' type='text' value='" + TextBox475.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox475' disabled='disabled' class='form_input' style='width:170px; /> " +
//" </td> " +
//" <td align='center'  colspan='2'>&nbsp;</td> " +
//" <td align='center' >&nbsp;</td> " +
//" <td align='center' >&nbsp;</td> " +
//" </tr> " +
//" <tr> " +
//" <td align='center' colspan='2'>Dirección del accidente:</td> " +
//" <td colspan='15' align='left'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox476' type='text' value='" + TextBox476.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox476' disabled='disabled' class='form_input' style='width:636px;' /> " +
//" </td> " +
//" </tr> " +
//" <tr> " +
//" <td align='center' colspan='17' style='border-bottom-style: solid; border-top-style: solid; border-width: 1px; border-color: #000000'><B>DATOS DEL VEHICULO INVOLUCRADO EN EL ACCIDENTE DE TRÁNSITO</B></td> " +
//" </tr> " +
//" <tr> " +
//" <td align='center' colspan='3'><b>Placa:</b></td> " +
//" <td align='center' colspan='2'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox477' type='text' value='" + TextBox477.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox477' disabled='disabled' class='form_input' /> " +
//" </td> " +
//" <td align='center' colspan='3'>&nbsp;</td> " +
//" <td align='center' colspan='3'><b>Vehiculo no identificado:</b></td> " +
//" <td align='center' colspan='3'> " +
//" <span class='form_input' disabled='disabled'><input id='ctl00_ContentPlaceHolder1_CheckBox1' type='checkbox' " + vehNoId + " name='ctl00$ContentPlaceHolder1$CheckBox1' disabled='disabled' /></span> " +
//" </td> " +
//" <td align='center' colspan='3'>&nbsp;</td> " +
//" </tr> " +
//" </table> " +
//" <p></p> " +
//" <table style='border: 1px solid #000000; width: 1003px'> " +
//" <tr> " +
//" <td colspan='11' align='center' style='border-color: #000000; border-bottom-style: solid; border-bottom-width: 1px;'><B>DATOS DEL CONDUCTOR DEL VEHICULO INVOLUCRADO EN EL ACCIDENTE</b></td> " +
//" </tr> " +
//" <tr> " +
//" <td colspan='2'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox1' type='text' value='" + TextBox1.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox1' disabled='disabled' class='form_input' style='width:233px;' /> " +
//" </td> " +
//" <td> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox2' type='text' value='" + TextBox2.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox2' disabled='disabled' class='form_input' style='width:233px;' /> " +
//" </td> " +
//" <td colspan='3'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox3' type='text' value='" + TextBox3.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox3' disabled='disabled' class='form_input' style='width:233px;' /> " +
//" </td> " +
//" <td colspan='5'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox4' type='text' value='" + TextBox4.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox4' disabled='disabled' class='form_input' style='width:233px;' /> " +
//" </td> " +
//" </tr> " +
//" <tr> " +
//" <td style='height: 23px' colspan='2'><b>Primer Apellido</b></td> " +
//" <td style='height: 23px'><b>Segundo Apellido</b></td> " +
//" <td style='height: 23px' colspan='3'><b>Primer Nombre</b></td> " +
//" <td style='height: 23px' colspan='5'><b>Segundo Nombre</b></td> " +
//" </tr> " +
//" <tr> " +
//" <td colspan='11'><b>Tipo Documento de Identificación</b></td> " +
//" </tr> " +
//" <tr> " +
//" <td colspan='3' rowspan='4'> " +
//" <table id='ctl00_ContentPlaceHolder1_RadioButtonList2' disabled='disabled' class='form_input' border='0' style='height:75px;width:473px;'> " +
//" 	<tr> " +
//" 		<td><span disabled='disabled'><input id='ctl00_ContentPlaceHolder1_RadioButtonList2_0' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList2' value='P' " + tipoId[0] + " disabled='disabled' /> " +
//"<label for='ctl00_ContentPlaceHolder1_RadioButtonList2_0'>Pasaporte</label></span></td><td><span disabled='disabled'> " +
//"<input id='ctl00_ContentPlaceHolder1_RadioButtonList2_1' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList2' value='10' " + tipoId[1] + " disabled='disabled' /> " +
//"<label for='ctl00_ContentPlaceHolder1_RadioButtonList2_1'>Carné diplomatico</label></span></td> " +
//" 	</tr><tr> " +
//" 		<td><span disabled='disabled'><input id='ctl00_ContentPlaceHolder1_RadioButtonList2_2' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList2' value='3' " + tipoId[2] + " checked='checked' disabled='disabled' /> " +
//" <label for='ctl00_ContentPlaceHolder1_RadioButtonList2_2'>Tarjeta de Identidad</label></span></td><td><span disabled='disabled'> " +
//"<input id='ctl00_ContentPlaceHolder1_RadioButtonList2_3' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList2' value='12' " + tipoId[3] + " disabled='disabled' /> " +
//" <label for='ctl00_ContentPlaceHolder1_RadioButtonList2_3'>Salvoconducto de permanencia</label></span></td> " +
//" 	</tr><tr> " +
//" 		<td><span disabled='disabled'><input id='ctl00_ContentPlaceHolder1_RadioButtonList2_4' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList2' value='4' " + tipoId[4] + " disabled='disabled' /> " +
//" <label for='ctl00_ContentPlaceHolder1_RadioButtonList2_4'>Cédula de Ciudadania</label></span></td> " +
//            //"<td><span disabled='disabled'> " +
//            //"<input id='ctl00_ContentPlaceHolder1_RadioButtonList2_5' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList2' value='' disabled='disabled' /></span></td> " +
//" 	</tr><tr> " +
//" 		<td><span disabled='disabled'><input id='ctl00_ContentPlaceHolder1_RadioButtonList2_6' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList2' value='5' " + tipoId[6] + " disabled='disabled' /> " +
//" <label for='ctl00_ContentPlaceHolder1_RadioButtonList2_6'>Cédula de Extranjería</label></span></td><td></td> " +
//" 	</tr> " +
//" </table> " +
//" </td> " +
//" <td colspan='8'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox5' type='text' value='" + TextBox5.Text.Trim().ToUpper() + "' maxlength='10' id='ctl00_ContentPlaceHolder1_TextBox5' disabled='disabled' class='form_input' style='width:196px;text-align:center' /> " +
//"  " +
//"  " +
//" </td> " +
//" </tr> " +
//" <tr> " +
//" <td colspan='8'>Número Documento de Identificación</td> " +
//" </tr> " +
//" <tr> " +
//" <td>&nbsp;</td> " +
//" <td align='center' colspan='3'>&nbsp;</td> " +
//" <td></td> " +
//" <td colspan='3'></td> " +
//" </tr> " +
//" <tr> " +
//" <td>&nbsp;</td> " +
//" <td align='center' colspan='3'>&nbsp;</td> " +
//" <td>&nbsp;</td> " +
//" <td colspan='3'>&nbsp;</td> " +
//" </tr> " +
//" <tr> " +
//" <td colspan='2'> " +
//" <b>Dirección del Conductor:</b></td> " +
//" <td colspan='9'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox479' type='text' value='" + TextBox479.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox479' disabled='disabled' class='form_input' style='width:636px;' /> " +
//" </td> " +
//" </tr> " +
//" <tr> " +
//" <td> " +
//"  " +
//" <b>Teléfono:</b></td> " +
//" <td align='center' colspan='2' style='font-weight: 700'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox480' type='text' value='" + TextBox480.Text.Trim().ToUpper() + "' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox480' disabled='disabled' class='form_input' style='width:134px;text-align:center' /> " +
//" &nbsp;&nbsp;&nbsp;&nbsp;</td> " +
//" <td> " +
//" &nbsp;</td> " +
//" <td> " +
//" &nbsp;</td> " +
//" <td> " +
//" &nbsp;</td> " +
//" <td> " +
//" &nbsp;</td> " +
//" <td> " +
//" &nbsp;</td> " +
//" </tr> " +
//" <tr> " +
//" <td> " +
//" &nbsp;</td> " +
//" <td align='center' colspan='2'> " +
//" <b>Número</b></td> " +
//" <td> " +
//" <b>Departamento:</b></td> " +
//" <td> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox487' type='text' value='" + TextBox487.Text.Trim().ToUpper() + "' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox487' disabled='disabled' class='form_input' style='width:114px;' /> " +
//" </td> " +
//" <td> " +
//" &nbsp;</td> " +
//" <td> " +
//" <b>Municipio:</b></td> " +
//" <td> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox488' type='text' value='" + TextBox488.Text.Trim().ToUpper() + "' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox488' disabled='disabled' class='form_input' style='width:150px;' /> " +
//" </td> " +
//            //" <td bgcolor='Gray'> " +
//            //" &nbsp;</td> " +
//            //" <td bgcolor='Gray'> " +
//            //" &nbsp;</td> " +
//            //" <td bgcolor='Gray'> " +
//            //" &nbsp;</td> " +
//" </tr> " +
//" </table> " +
//" <p></p> " +
//" <table class='auto-style5'> " +
//"            <tr> " +
//"                <td>&nbsp;</td> " +
// "           </tr> " +
//"        </table> " +
//" <p></p> " +
//" <table class='auto-style5'> " +
//"            <tr> " +
//"                <td>&nbsp;</td> " +
// "           </tr> " +
//"        </table> " +
//" <p></p> " +
//" <table style='border: 1px solid #000000; width: 1003px'> " +
//" <tr> " +
//" <td style='border-width: 1px; border-color: #000000; border-bottom-style: solid;' colspan='9' align='center'><B>INFORMACIÓN DE LA PERSONA QUE REPORTA LA ATENCIÓN</B></td> " +
//" </tr> " +
//" <tr> " +
//" <td colspan='2'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox489' type='text' value='" + TextBox489.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox489' disabled='disabled' class='form_input' style='width:233px;' /> " +
//" </td> " +
//" <td> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox490' type='text' value='" + TextBox490.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox490' disabled='disabled' class='form_input' style='width:233px;' /> " +
//" </td> " +
//" <td colspan='2'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox491' type='text' value='" + TextBox491.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox491' disabled='disabled' class='form_input' style='width:233px;' /> " +
//" </td> " +
//" <td colspan='3'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox492' type='text' value='" + TextBox492.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox492' disabled='disabled' class='form_input' style='width:233px;' /> " +
//" </td> " +
//" <td style='width: 268435456px'> " +
//" &nbsp;</td> " +
//" </tr> " +
//" <tr> " +
//" <td style='height: 23px' colspan='2'><b>Primer Apellido</b></td> " +
//" <td style='height: 23px'><b>Segundo Apellido</b></td> " +
//" <td style='height: 23px' colspan='2'><b>Primer Nombre</b></td> " +
//" <td style='height: 23px' colspan='3'><b>Segundo Nombre</b></td> " +
//" <td style='height: 23px; width: 268435456px;'>&nbsp;</td> " +
//" </tr> " +
//" <tr> " +
//" <td style='height: 23px' rowspan='2' colspan='2'><b>Tipo Documento de Identificación</b></td> " +
//" <td style='height: 23px' colspan='2' rowspan='2'> " +
//" <table id='ctl00_ContentPlaceHolder1_RadioButtonList3' disabled='disabled' border='0' style='width:370px;'> " +
//" 	<tr> " +
//" 		<td><span disabled='disabled'><input id='ctl00_ContentPlaceHolder1_RadioButtonList3_0' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList3' value='3' " + tipoIdReporta[0] + " disabled='disabled' /> " +
//"<label for='ctl00_ContentPlaceHolder1_RadioButtonList3_0'>Tarjeta de Identidad</label></span></td><td><span disabled='disabled'> " +
//"<input id='ctl00_ContentPlaceHolder1_RadioButtonList3_2' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList3' value='5' " + tipoIdReporta[1] + " disabled='disabled' /> " +
//"<label for='ctl00_ContentPlaceHolder1_RadioButtonList3_2'>Cédula de extranjería</label></span></td> " +
//" 	</tr><tr> " +
//" 		<td><span disabled='disabled'><input id='ctl00_ContentPlaceHolder1_RadioButtonList3_1' type='radio' name='ctl00$ContentPlaceHolder1$RadioButtonList3' value='4' " + tipoIdReporta[2] + " disabled='disabled' /><label for='ctl00_ContentPlaceHolder1_RadioButtonList3_1'>Cédula de ciudadania</label></span></td><td></td> " +
//" 	</tr> " +
//" </table> " +
//" </td> " +
//" <td style='height: 23px' colspan='4'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox493' type='text' value='" + TextBox493.Text.Trim().ToUpper() + "' maxlength='10' id='ctl00_ContentPlaceHolder1_TextBox493' disabled='disabled' class='form_input' style='width:196px;text-align:center' /> " +
//" </td> " +
//" <td align='center' style='height: 23px; width: 268435456px;'> " +
//" &nbsp;</td> " +
//" </tr> " +
//" <tr> " +
//" <td style='height: 23px' colspan='4'> " +
//" Número documento de identificación</td> " +
//" <td align='center' style='height: 23px; width: 268435456px;'>&nbsp;</td> " +
//" </tr> " +
//" <tr> " +
//" <td style='height: 23px'><b>Cargo:</b></td> " +
//" <td style='height: 23px' colspan='4'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox494' type='text' value='" + TextBox494.Text.Trim().ToUpper() + "' id='ctl00_ContentPlaceHolder1_TextBox494' disabled='disabled' class='form_input' style='width:334px;' /> " +
//" </td> " +
//" <td style='height: 23px'><b>Teléfono:</b></td> " +
//" <td style='height: 23px'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox495' type='text' value='" + TextBox495.Text.Trim().ToUpper() + "' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox495' disabled='disabled' class='form_input' style='width:101px;text-align:center' /> " +
//" </td> " +
//" <td align='center' style='height: 23px; width: 268435456px;'> " +
//" <input name='ctl00$ContentPlaceHolder1$TextBox496' type='text' value='" + TextBox496.Text.Trim().ToUpper() + "' readonly='readonly' id='ctl00_ContentPlaceHolder1_TextBox496' disabled='disabled' class='form_input' style='width:48px;text-align:center' /> " +
//" </td> " +
//" <td style='height: 23px; width: 268435456px;'>&nbsp;</td> " +
//" </tr> " +
//" <tr> " +
//" <td style='height: 23px' colspan='2'>&nbsp;</td> " +
//" <td style='height: 23px' colspan='3'>&nbsp;</td> " +
//" <td style='height: 23px'>&nbsp;</td> " +
//" <td style='height: 23px' align='center'>Número</td> " +
//" <td style='height: 23px; width: 268435456px;' align='center'>Extensión</td> " +
//" <td style='height: 23px; width: 268435456px;'>&nbsp;</td> " +
//" </tr> " +
//" </table> " +
//" <p> " +
//" </p> ";


//        return html2;
//    }
    protected void Button2_Click(object sender, EventArgs e)
    {
        AgregarPrintScript(ViewState["ArchivoSoat"].ToString());
    }
    protected void inactivarControles()
    {
        Button1.Enabled = false;
        Button2.Visible = true;
        Button2.Enabled = true;
        rblTipoId.Enabled = false;
        RadioButtonList1.Enabled = false;
        txtFecha.Enabled = false;
        DropDownList1.Enabled = false;
        DropDownList2.Enabled = false;
        RadioButtonList4.Enabled = false;
        TextBox469.Enabled = false;
        TextBox470.Enabled = false;
        dllDepartamento.Enabled = false;
        ddlMunicipio.Enabled = false;
        TextBox471.Enabled = false;
    }
    //    Button1.Enabled = false;
    //    rblCobertura.Enabled = false;
    //    rblServicio.Enabled = false;
    //    rblPrioridad.Enabled = false;
    //    TextBox1.Enabled = false;
    //    TextBox165.Enabled = false;
    //    TextBox166.Enabled = false;
    //    TextBox174.Enabled = false;
    //    TextBox173.Enabled = false;
    //    TextBox177.Enabled = false;
    //    TextBox184.Enabled = false;
    //    TextBox450.Enabled = false;
    //    TextBox188.Enabled = false;
    //    TextBox314.Enabled = false;
    //    TextBox431.Enabled = false;
    //    TextBox195.Enabled = false;
    //    TextBox317.Enabled = false;
    //    TextBox432.Enabled = false;
    //    TextBox202.Enabled = false;
    //    TextBox320.Enabled = false;
    //    TextBox433.Enabled = false;
    //    TextBox209.Enabled = false;
    //    TextBox323.Enabled = false;
    //    TextBox434.Enabled = false;
    //    TextBox216.Enabled = false;
    //    TextBox326.Enabled = false;
    //    TextBox435.Enabled = false;
    //    TextBox223.Enabled = false;
    //    TextBox329.Enabled = false;
    //    TextBox436.Enabled = false;
    //    TextBox230.Enabled = false;
    //    TextBox332.Enabled = false;
    //    TextBox437.Enabled = false;
    //    TextBox237.Enabled = false;
    //    TextBox335.Enabled = false;
    //    TextBox438.Enabled = false;
    //    TextBox244.Enabled = false;
    //    TextBox338.Enabled = false;
    //    TextBox439.Enabled = false;
    //    TextBox251.Enabled = false;
    //    TextBox341.Enabled = false;
    //    TextBox440.Enabled = false;
    //    TextBox258.Enabled = false;
    //    TextBox344.Enabled = false;
    //    TextBox441.Enabled = false;
    //    TextBox265.Enabled = false;
    //    TextBox347.Enabled = false;
    //    TextBox442.Enabled = false;
    //    TextBox272.Enabled = false;
    //    TextBox350.Enabled = false;
    //    TextBox443.Enabled = false;
    //    TextBox279.Enabled = false;
    //    TextBox353.Enabled = false;
    //    TextBox444.Enabled = false;
    //    TextBox286.Enabled = false;
    //    TextBox356.Enabled = false;
    //    TextBox445.Enabled = false;
    //    TextBox293.Enabled = false;
    //    TextBox359.Enabled = false;
    //    TextBox446.Enabled = false;
    //    TextBox300.Enabled = false;
    //    TextBox362.Enabled = false;
    //    TextBox447.Enabled = false;
    //    TextBox307.Enabled = false;
    //    TextBox365.Enabled = false;
    //    TextBox448.Enabled = false;
    //    TextBox449.Enabled = false;
    //    TextBox451.Enabled = false;
    //    TextBox449.Enabled = false;
    //    rblAtencion.Enabled = false;
    //}
    protected void dllDepartamento_SelectedIndexChanged(object sender, EventArgs e)
    {
        Procedimientos.LlenarCombos(ddlMunicipio, new ClinicaCES.Logica.LMaestros().ListarMunicipios(dllDepartamento.SelectedValue), "CODMUN", "MUN");
    }
    //protected void CheckBoxList1_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    if (RadioButtonList4 .SelectedValue == "1")
    //    {
    //        TextBox469.Enabled = true;
    //        TextBox470.Enabled = true;
    //        dllDepartamento.Enabled = true;
    //        ddlMunicipio.Enabled = true;

    //    }
    //}
    protected void RadioButtonList4_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (RadioButtonList4.SelectedValue == "1")
        {
            TextBox469.Enabled = true;
            TextBox470.Enabled = true;
            dllDepartamento.Enabled = true;
            ddlMunicipio.Enabled = true;
        }
        else
        {
            TextBox469.Enabled = false;
            TextBox470.Enabled = false;
            TextBox469.Text = "";
            TextBox470.Text = "";
            dllDepartamento.Enabled = false;
            dllDepartamento.SelectedIndex = -1;
            ddlMunicipio.Enabled = false;
            ddlMunicipio.SelectedIndex = -1;
        }
    }
    protected void gvInforme_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInforme.PageIndex = e.NewPageIndex;
        if (txtFiltro.Text == "")
        {
            gvInforme.DataSource = ViewState["dtInformes"];
        }
        else
        {
            gvInforme.DataSource = ViewState["dtFiltro"];
        }
        gvInforme.DataBind();

    }
    protected void gvInforme_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
        }
    }
    protected void gvInforme_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow row = gvInforme.SelectedRow;
        DataRow dr = new ClinicaCES.Logica.LMaestros().ConsultarProfesional(row.Cells[0].Text).Rows[0];
        TextBox489.Text = dr[0].ToString();
        TextBox490.Text = dr[1].ToString();
        TextBox491.Text = dr[2].ToString();
        TextBox492.Text = dr[3].ToString();
        RadioButtonList3.SelectedValue = dr[4].ToString();
        TextBox493.Text = dr[5].ToString();
        TextBox495.Text = dr[6].ToString();
        TextBox496.Text = dr[7].ToString();
        TextBox494.Text = dr[8].ToString();

    }
    protected void txtFiltro_TextChanged(object sender, EventArgs e)
    {
        DataTable dtNew = Procedimientos.dtFiltrado("NOMBRE", "ID LIKE '*" + txtFiltro.Text.Trim() + "*' OR NOMBRE LIKE '*" + txtFiltro.Text.Trim() + "*'OR ESPECIALIDAD LIKE '*" + txtFiltro.Text.Trim() + "*'", (DataTable)ViewState["dtInformes"]);
        Procedimientos.LlenarGrid(dtNew, gvInforme);
        int Suma = 0;
        if (dtNew.Rows.Count < 15)
        {
            for (int i = 0; i <= dtNew.Rows.Count; i++)
            {
                Suma = Suma + 22;
            }
            Panel1.Height = 105 + Suma;
        }
        else
        {
            Panel1.Height = 455;
        }
        ViewState["dtFiltro"] = dtNew;


    }

    protected void CheckBoxList2_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (CheckBoxList2.SelectedIndex.ToString() != "-1")
        {
            if (CheckBoxList2.SelectedIndex.ToString() == "0")
            {
                CheckBoxList2.Items[1].Selected = false;
                TextBox478.Enabled = true;
                TextBox471.Enabled = true;
            }
            if (CheckBoxList2.SelectedIndex.ToString() == "1")
            {
                CheckBoxList2.Items[0].Selected = false;
                TextBox478.Enabled = false;
                TextBox471.Enabled = false;
                TextBox478.Text = "";
                TextBox471.Text = "";
            }
        }
    }
}

        
//protevoid TraerUbicacion(string ValUbicacion)
//{
//    if (ValUbicacion == "1") { TextBox163.Text = "X"; } else { if (ValUbicacion == "2") { TextBox156.Text = "X"; } }
//}
//protected void TraerIndentificacion(string ValTipoId)
//{
//    if (ValTipoId == "2") { TextBox78.Text = "X"; }
//    if (ValTipoId == "3") { TextBox97.Text = "X"; }
//    if (ValTipoId == "4") { TextBox99.Text = "X"; }
//    if (ValTipoId == "5") { TextBox101.Text = "X"; }
//    if (ValTipoId == "P") { TextBox80.Text = "X"; }
//    if (ValTipoId == "A") { TextBox98.Text = "X"; }
//    if (ValTipoId == "M") { TextBox100.Text = "X"; }
//}
//protected void TraerContingencia(string ValContingencia)
//{
//    if (ValContingencia == "13") { TextBox145.Text = "X"; }
//    if (ValContingencia == "14") { TextBox150.Text = "X"; }
//    if (ValContingencia == "01") { TextBox146.Text = "X"; }
//    if (ValContingencia == "02") { TextBox151.Text = "X"; }
//    if (ValContingencia == "06") { TextBox147.Text = "X"; }
//}