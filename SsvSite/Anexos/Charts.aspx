﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Masters/MasterNew.master" AutoEventWireup="true" CodeFile="Charts.aspx.cs" Inherits="Anexos_Charts" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
     <style type="text/css">
        body
        {
            font-family: Arial;
            font-size: 10pt;
        }
    </style>


    <asp:Label ID = "lblName" Text="Name" runat="server" />
    <asp:HiddenField ID = "hfName" runat = "server" />
    <br />
    <br />
    <asp:Button Text="Set Name" runat="server" OnClientClick = "SetName()" />
    <script type = "text/javascript">
        function SetName() {
            var label = document.getElementById("<%=lblName.ClientID %>");

            //Set the value of Label.
            label.innerHTML = "Mudassar Khan";

            //Set the value of Label in Hidden Field.
            document.getElementById("<%=hfName.ClientID %>").value = label.innerHTML;
        }
    </script>

</asp:Content>

