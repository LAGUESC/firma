﻿<%@ Page Title="Busqueda paciente para generar Anexo1" Language="C#" MasterPageFile="~/Masters/MasterNew.master" AutoEventWireup="true" CodeFile="ConsentimientosUrgencias.aspx.cs" Inherits="Anexos_ConsentimientosUrgencias" MaintainScrollPositionOnPostback="true" %>
   <asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
       <script src="<%= Page.ResolveUrl("~/js/signature.js") %>"></script>
       <script type="text/javascript">
                     function REPRODUCTOR() {
                         var mywindow = window.open("http://cessrvapp1/TutorialesCES/UrgenciasAdmisiones.mp4", "mywindow", "resizable=no,location=1,status=1,scrollbars=1,width=1000,height=700");
                     mywindow.opener = self;
                     mywindow.moveTo(500, 200);
                     mywindow.focus();
                 }
 </script>
      <div class="row">
         <div class="col-md-12">
            <div class="box">
               <div class="box box-primary">
                  <!--div Título-->
                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                     <div class="col-md-4">
                        <h3 align="center" runat="server" class="box-title">Consentimientos Informados Urgencias</h3> </div>
                     <div class="col-md-4">
                        <div class="form-group"> 
                            <asp:LinkButton ID="LinkButton1" runat="server" style ="float:right;font-size:large;" OnClientClick="REPRODUCTOR();" ><h3 class="fa fa-info-circle"></h3>&nbsp;Ayuda&nbsp;&nbsp;&nbsp;&nbsp;</asp:LinkButton>
                        </div>
                     </div>
                  </div>
               </div>
               <!-- /.box-header -->
               <div class="box-body">
                  <!--div campo Tipo identificación-->
                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <%--<h5>Tipo de identificación</h5>
                                    <asp:DropDownList ID="ddlTipoId" runat="server" AutoPostBack="True" class="form-control"> </asp:DropDownList>--%> </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                  </div>
                  <!--div campo Identificación-->
                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                     <div class="col-md-5">
                        <div class="form-group">
                           <h5>Identificación del paciente</h5>
                           <asp:TextBox ID="txtId" runat="server" class="form-control" placeholder="Identificación del paciente" MaxLength="13"></asp:TextBox>
                           <ajaxToolkit:FilteredTextBoxExtender ID="txtId_FilteredTextBoxExtender" runat="server" FilterInterval="17" TargetControlID="txtId" ValidChars="1234567890"> </ajaxToolkit:FilteredTextBoxExtender>
                        </div>
                     </div>
                     <div class="col-md-3">
                        <div class="form-group"> </div>
                     </div>
                  </div>
                  <!--div botón Consultar-->
                  <div class="row">
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group">
                           <label>
                              <asp:CheckBox ID="CheckPaciente" runat="server" Text="Firma cómo paciente" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckPaciente_CheckedChanged" /> </label>
                           <br />
                           <label>
                              <asp:CheckBox ID="CheckRepresentante" runat="server" Text="Firma cómo representante" CssClass="icheck-material-blue" AutoPostBack="True" OnCheckedChanged="CheckRepresentante_CheckedChanged" /> </label>
                           <br />
                           <asp:Button ID="btnConsultar" runat="server" Text="Consultar" CssClass="btn btn-primary" OnClick="btnConsultar_Click" /> </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group"> </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!--div tabla-->
      <div class="box box-primary" id="divtable" runat="server" visible="false">
         <div class="row">
            <div class="box-header with-border">
               <div class="col-md-1">
                  <div class="form-group"> </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group">
                     <h5>Nombre Paciente :</h5> </div>
               </div>
               <div class="col-md-5">
                  <div class="form-group">
                     <asp:TextBox ID="LblNombre" runat="server" READONLY="true" CausesValidation="True" class="form-control"></asp:TextBox>
                  </div>
               </div>
               <div class="col-md-3">
                  <div class="form-group"> </div>
               </div>
            </div>
         </div>
         <!--*****************************************************************-->
         <div class="row" id="divEmpresa" runat="server" visible="false">
             <div class="box-header with-border">
            <div class="col-md-2">
               <div class="form-group"> </div>
            </div>
            <div class="col-md-2">
               <div class="form-group">
                  <h5>Empresa responsable :</h5> </div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                  <div>
                     <asp:Label ID="Label1" runat="server" Text="Campos para constancia de atención" ForeColor="#0066FF"></asp:Label>
                     <asp:TextBox ID="txtEmpresa" runat="server" class="form-control" placeholder="Empresa responsable" MaxLength="100"></asp:TextBox>
                  </div>
               </div>
            </div>
            <div class="col-md-2">
               <div class="form-group"> </div>
            </div>
            </div>
         </div>
         <!--*****************************************************************-->
         <div class="row">
            <div class="col-md-3">
               <div class="form-group"> </div>
            </div>
            <div class="col-md-2">
               <div class="form-group">
                  <%--<h5>Consentiento :</h5>--%> </div>
            </div>
            <div class="col-md-4">
               <div class="form-group">
                  <div>
                     <div class="icheck-material-blue">
                        <label>
                           <asp:CheckBox ID="chkcons6" runat="server" Text="F-SI-5 Autorización para tratamientos de datos personales" CssClass="icheck-material-blue" AutoPostBack="True" Visible="false" Checked="true" /> </label>
                     </div>
                  </div>
               </div>
            </div>
            <div class="col-md-3">
               <div class="form-group"> </div>
            </div>
         </div>
         <!--*****************************************************************-->
         <div id="divAutorizado" runat="server" visible="false">
            <div class="box-header with-border">
               <div class="row">
                  <div class="col-md-2">
                     <div class="form-group"> </div>
                  </div>
                  <div class="col-md-2">
                     <div class="form-group">
                        <h5>Cédula Autorizado :</h5> </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <asp:Label ID="Label2" runat="server" Text="Campos para autorizado" ForeColor="#0066FF"></asp:Label>
                        <asp:TextBox ID="txtIdAut" runat="server" class="form-control" placeholder="Cédula Autorizado" MaxLength="13"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="txtIdAut_FilteredTextBoxExtender" runat="server" TargetControlID="txtIdAut" ValidChars="1234567890"> </ajaxToolkit:FilteredTextBoxExtender>
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="form-group"> </div>
                  </div>
               </div>
               <!--*****************************************************************-->
               <div class="row">
                  <div class="col-md-2">
                     <div class="form-group"> </div>
                  </div>
                  <div class="col-md-2">
                     <div class="form-group">
                        <h5>Nombre Autorizado :</h5> </div>
                  </div>
                  <div class="col-md-6">
                     <div class="form-group">
                        <asp:TextBox ID="txtNomAut" runat="server" class="form-control" placeholder="Nombre Autorizado" MaxLength="100"></asp:TextBox>
                        <ajaxToolkit:FilteredTextBoxExtender ID="txtNomAut_FilteredTextBoxExtender" runat="server" TargetControlID="txtNomAut" ValidChars="abcdefghijklmnñopqrstuvwxyzABCDEFGHIJKLMNÑOPQRSTUVWXYZ´ "> </ajaxToolkit:FilteredTextBoxExtender>
                     </div>
                  </div>
                  <div class="col-md-2">
                     <div class="form-group"> </div>
                  </div>
               </div>
            </div>
         </div>
         <!--*****************************************************************-->
          <div class="box-header with-border">
         <div class="row">
            <div class="col-md-2">
               <div class="form-group"> </div>
            </div>
            <div class="col-md-2">
               <div class="form-group"></div>
            </div>
            <div class="col-md-6">
               <div class="form-group">
                     <asp:Button ID="btnGuardar" runat="server" CssClass="btn btn-primary" Text="Aceptar" OnClick="btnGuardar_Click" visible="false" />
                     <asp:Button ID="btnReview" runat="server" CssClass="btn btn-primary" Text="Previsualizar" OnClick="btnReview_Click" />
                     <asp:Button ID="btnCaptureFirm" type="submit" class="btn btn-danger" runat="server" OnClick="Capture_Firma_Click" Text="Capturar firma" Visible="false" /> &nbsp;&nbsp;
                     <label>
                        <asp:CheckBox ID="CheckAprobacionMedico" runat="server" Text="Aprobación médico" CssClass="icheck-material-blue" visible="false" /> </label>
                   <br />
                   <br />
                   <label>
                                <asp:CheckBox ID="CheckUtilizarFirmaBD" runat="server" Text="Capturar nueva firma" CssClass="icheck-material-blue" AutoPostBack="True" Visible="false" class="form-control" OnCheckedChanged="CheckUtilizarFirmaBD_CheckedChanged" /> </label>
                            <br />
                            <br />
               </div>
            </div>
            <div class="col-md-2">
               <div class="form-group"> 
                   <div id="imageBox" class="boxed" style="height:37mm;width:72mm" ;></div>
               </div>
                </div>
            </div>
         </div>
      </div>
   </asp:Content>