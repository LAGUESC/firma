﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using ClinicaCES.Entidades;

public partial class Seguridad_addUsuario : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Procedimientos.ValidarSession(this.Page);
        if (!IsPostBack)
        {
            CargaInicial();
        }
        litScript.Text = string.Empty;
    }

    private void CargaInicial()
    {
        Procedimientos.Titulo("Creación de Usuarios", this.Page);        
        txtUsuario.Attributes.Add("onkeypress", "return clickButton(event,'" + lnkComprobar.ClientID + "')");
        Procedimientos.LlenarCombos(ddlCliente, new ClinicaCES.Logica.LClientes().ClienteConsultar(), "NIT", "NOMCOMCIAL");
        //btnLupa.Attributes.Add("onclick", "abrirVentana('srcUsuarios.aspx')");
        ListarNivel();
        ListarTema();
        EstadoInicial();
        if (Request.QueryString["usr"] != null)
        {
            Comprobar(Request.QueryString["usr"]);
        }
    }

    private void ListarTema()
    {
        Procedimientos.LlenarCombos(ddlTema, new ClinicaCES.Logica.LUsuarios().TemasConsultar(), "TEMAID", "TEMA");
        ddlTema.Items.RemoveAt(0);
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        if (sender.Equals(lnkComprobar))
        {
            Comprobar(txtUsuario.Text.Trim());
        }
        if (sender.Equals(btnGuardar))
        {
            bool guarda = false;
            if (hdnAccion.Value == "I" && txtClave1.Text == txtClave2.Text && !string.IsNullOrEmpty(txtClave1.Text.Trim()))
                guarda = true;
            else if (hdnAccion.Value == "A" && txtClave1.Text == txtClave2.Text)
                guarda = true;

            if (guarda)
            {

                Guardar(txtUsuario.Text.Trim().ToUpper(), txtClave1.Text, ddlNivel.SelectedValue, txtNombre.Text.Trim().ToUpper(), txtApellido1.Text.Trim().ToUpper(), txtApellido2.Text.Trim().ToUpper(), txtEmail.Text.Trim().ToUpper(), txtDireccion.Text.Trim().ToUpper(), txtTelefono.Text.Trim().ToUpper(), Session["Nick1"].ToString(), ddlTema.SelectedValue, ddlCliente.SelectedValue);
            }
            else
            {
                Procedimientos.Script("Mensaje(8)", litScript);
            }
        }
        else if (sender.Equals(btnCancelar))
        {
            EstadoInicial();
        }
        else if (sender.Equals(btnRetirar))
        {
            Retirar(txtUsuario.Text.Trim());
        }
    }

    private void Retirar(string Usuario)
    {
        Usuarios usr = new Usuarios();

        usr.Usuario = Usuario;

        string msg = "3";

        if (new ClinicaCES.Logica.LUsuarios().UsuarioRetirar(usr))
        {
            //elimino
            msg = "7";
        }
        else
        { 
            //no elimino
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);

        EstadoInicial();
    }

    private void Guardar(string usuario,
         string Clave,
         string Nivel,
         string Nombres,
         string Apellido1,
         string Apellido2,
         string Email,
         string Direccion,
         string Telefono,
         string UsrMod,
         string Tema,
        string Cliente)
    {
        if (!ValidarGuardar())
            return;
        Usuarios usr = new Usuarios();

        usr.Usuario = usuario;

        if(!string.IsNullOrEmpty(Clave))
            usr.Clave = Encripcion.clsEncriptar.Encriptar(Clave);

        usr.Nivel = Nivel;
        usr.Nombres = Nombres;
        usr.Apellido1 = Apellido1;
        usr.Apellido2 = Apellido2;
        usr.Direccion = Direccion;
        usr.Telefono = Telefono;
        usr.UsrMod = UsrMod;
        usr.Email = Email;
        usr.Tema = Tema;
        usr.Cliente = Cliente;

        string msg = "3";

        if (new ClinicaCES.Logica.LUsuarios().UsuarioActualizar(usr))
        {
            //Guardo
            msg = "1";
            string fileExt = System.IO.Path.GetExtension(fuFoto.FileName);
            if (fileExt.ToLower() == ".jpg")
            {
                fuFoto.SaveAs(Server.MapPath("../img/usuarios/" + usuario + ".jpg"));
            }
                
        }

        Procedimientos.Script("Mensaje(" + msg + ")", litScript);
        EstadoInicial();
    }

    private void ListarNivel()
    {   
        Nivel nvl = new Nivel();
        DataTable dtNivel = new ClinicaCES.Logica.LNivel().NivelConsultar(nvl);
        Procedimientos.LlenarCombos(ddlNivel, dtNivel, "CODIGO", "NIVEL");
    }

    private void Comprobar(string usuario)
    {
        Usuarios usr = new Usuarios();

        usr.Usuario = usuario;

        DataTable dtUsuario = new ClinicaCES.Logica.LUsuarios().UsuarioConsultar(usr);

        EstadoControles(true);

        if (dtUsuario.Rows.Count > 0)
        {
            //encontro cargar
            DataRow row = dtUsuario.Rows[0];
            txtUsuario.Text = row["USUARIO"].ToString();
            ddlNivel.SelectedValue = row["NIVEL"].ToString();
            txtNombre.Text = row["NOMBRES"].ToString();
            txtApellido1.Text = row["APELLIDO1"].ToString();
            txtApellido2.Text = row["APELLIDO2"].ToString();
            txtEmail.Text = row["EMAIL"].ToString();
            txtDireccion.Text = row["DIRECCION"].ToString();
            if (!string.IsNullOrEmpty(row["NIT"].ToString()))
                ddlCliente.SelectedValue = row["NIT"].ToString() + "|" + row["NEGOCIO"].ToString();
            txtTelefono.Text = row["TELEFONO"].ToString();
            ddlTema.SelectedValue = row["TEMA"].ToString();

            hdnAccion.Value = "A";
           
        }
        else
        { 
            //registro nuevo
            btnRetirar.Enabled = false;
            hdnAccion.Value = "I";
        }

        Procedimientos.Script("SeleccionarText('" + txtNombre.ClientID + "')", litScript);

    }

    private void EstadoControles(bool Estado)
    {
        txtUsuario.Enabled = !Estado;
        txtClave1.Enabled = Estado;
        txtClave2.Enabled = Estado;
        ddlNivel.Enabled = Estado;
        txtNombre.Enabled = Estado;
        txtApellido1.Enabled = Estado;
        txtApellido2.Enabled = Estado;
        txtEmail.Enabled = Estado;
        txtDireccion.Enabled = Estado;
        ddlCliente.Enabled = Estado;
        txtTelefono.Enabled = Estado;
        ddlTema.Enabled = Estado;
        fuFoto.Enabled = Estado;

        btnCancelar.Enabled = Estado;
        btnGuardar.Enabled = Estado;
        btnRetirar.Enabled = Estado;
    }

    private void EstadoInicial()
    {
        txtUsuario.Text = string.Empty;
        txtClave1.Text = string.Empty;
        txtClave2.Text = string.Empty;
        ddlNivel.SelectedIndex = 0;
        txtNombre.Text = string.Empty;
        txtApellido1.Text = string.Empty;
        txtApellido2.Text = string.Empty;
        txtEmail.Text = string.Empty;
        txtDireccion.Text = string.Empty;
        ddlCliente.SelectedIndex = 0;
        txtTelefono.Text = string.Empty;
        EstadoControles(false);

        System.Drawing.Color blue = System.Drawing.Color.Blue;
        string input = "form_input";

        txtNombre.CssClass = input;
        lblValidaNombres.ForeColor = blue;
        txtApellido1.CssClass = input;
        lblValidaApellido.ForeColor = blue;
        lblValidaNivel.ForeColor = blue;
        txtTelefono.CssClass = input;
        lblValidaTelefono.ForeColor = blue;
        txtEmail.CssClass = input;
        lblValidaCorreo.ForeColor = blue;
        lblValidaCliente.ForeColor = blue;

        txtUsuario.Focus();        
    }

    private bool ValidarGuardar()
    {
        bool Valido = true;

        System.Drawing.Color[] color = { System.Drawing.Color.Blue, System.Drawing.Color.Red };
        string[] css = { "form_input", "invalidtxt" };

        if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
        {
            Valido = false;
            lblValidaNombres.ForeColor = color[1];
            txtNombre.CssClass = css[1];
        }
        else
        {
            lblValidaNombres.ForeColor = color[0];
            txtNombre.CssClass = css[0];
        }
        if (string.IsNullOrEmpty(txtApellido1.Text.Trim()))
        {
            Valido = false;
            lblValidaApellido.ForeColor = color[1];
            txtApellido1.CssClass = css[1];
        }
        else
        {
            lblValidaApellido.ForeColor = color[0];
            txtApellido1.CssClass = css[0];
        }

        if (ddlNivel.SelectedIndex == 0)
        {
            Valido = false;
            lblValidaNivel.ForeColor = color[1];
        }
        else
            lblValidaNivel.ForeColor = color[0];


        if (string.IsNullOrEmpty(txtTelefono.Text.Trim()))
        {
            Valido = false;
            lblValidaTelefono.ForeColor = color[1];
            txtTelefono.CssClass = css[1];
        }
        else
        {
            lblValidaTelefono.ForeColor = color[0];
            txtTelefono.CssClass = css[0];
        }

        if (string.IsNullOrEmpty(txtEmail.Text.Trim()))
        {
            Valido = false;
            lblValidaCorreo.ForeColor = color[1];
            txtEmail.CssClass = css[1];
        }
        else
        {
            lblValidaCorreo.ForeColor = color[0];
            txtEmail.CssClass = css[0];

            if (!Procedimientos.regularExpression(txtEmail.Text.Trim(), "^(([^<;>;()[\\]\\\\.,;:\\s@\\\"]+" + "(\\.[^<;>;()[\\]\\\\.,;:\\s@\\\"]+)*)|(\\\".+\\\"))@" + "((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}" + "\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+" + "[a-zA-Z]{2,}))$"))
            {
                Valido = false;
                lblValidaCorreo.ForeColor = color[1];
                txtEmail.CssClass = css[1];
            }
            else
            {
                lblValidaCorreo.ForeColor = color[0];
                txtEmail.CssClass = css[0];
            }
        }

        if (ddlCliente.SelectedIndex == 0)
        {
            Valido = false;
            lblValidaCliente.ForeColor = color[1];
        }
        else
            lblValidaCliente.ForeColor = color[0];

        return Valido;
    }
}
