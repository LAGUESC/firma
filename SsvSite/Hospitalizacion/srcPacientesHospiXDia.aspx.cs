﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
public partial class Hospitalizacion_srcPacientesHospiXDia : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblDescripcion.Text = "Busqueda de pacientes de Hospitalizacion";
        if (!IsPostBack)
        {
            Procedimientos.Titulo("Busqueda de pacientes de Hospitalizacion", this.Page);
            Procedimientos.LlenarCombos(ddlTipoId, new ClinicaCES.Logica.LMaestros().ListaTiposIdentificacion(), "TIPOID", "ID");
        }
        this.Form.DefaultButton = btnConsultar.UniqueID;
        litScript.Text = string.Empty;
    }

    protected void Click_Botones(object sender, EventArgs e)
    {
        Consultar(ddlTipoId.SelectedValue.ToString(), txtId.Text);
    }
    private void Consultar(string TipoId, string Id)
    {
        DataSet ds = new ClinicaCES.Logica.LBusquedaPacientes().BusquedaHospitalizacion(TipoId, Id);
        DataTable dtInforme = ds.Tables[0];
        DataTable dtPagina = Procedimientos.dtFiltrado("IDENTIFICACION", "", dtInforme);
        string[] campo = { "IDENTIFICACION" };

        if (dtInforme.Rows.Count > 0)
        {
            ViewState["dtInformes"] = dtInforme;
            ViewState["dtPaginas"] = dtPagina;
            Procedimientos.LlenarGrid(dtInforme, gvInforme);
            pnlInforme.Visible = true;
            string[] campos = { "NOMCOMCIAL" };
        }
        else
        {
            pnlInforme.Visible = false;
            Procedimientos.Script("mensajini", "Mensaje(61)", this.Page);
        }
    }
    protected void gvInforme_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvInforme.PageIndex = e.NewPageIndex;
        gvInforme.DataSource = ViewState["dtPaginas"];
        gvInforme.DataBind();
    }


    protected void gvInforme_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.className='overrow'");

            if (e.Row.RowState == DataControlRowState.Normal)
                e.Row.Attributes.Add("onmouseout", "this.className='normalrow'");
            else if (e.Row.RowState == DataControlRowState.Alternate)
                e.Row.Attributes.Add("onmouseout", "this.className='alterrow'");
            if (e.Row.Cells[0].Text.Trim() == "SI")
            {
                string cookie = ddlTipoId.SelectedValue.ToString().Trim() + ";" + e.Row.Cells[1].Text.Trim() + ";" + e.Row.Cells[3].Text.Trim() +  "|" + e.Row.Cells[5].Text.Trim() + "|3";
                //string cookie = e.Row.Cells[1].Text.Trim();
                e.Row.Attributes.Add("onclick", "redirect('../Anexos/Anexo3.aspx?cookie3=" + Procedimientos.cifrar(cookie) + "')");
            }

        }
    }
}