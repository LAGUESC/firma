﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LAnexo2
    {


        public DataSet Anexo2Consultar(string Paciente, string Formulario)
        {
            string[] nomParam = 
            {
                "Paciente",
                "Formulario"

            };

            object[] vlrParam = 
            {
                Paciente ,
                Formulario
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            
            string sql = "SELECT PAC.PAC_PAC_APELLPATER, PAC.PAC_PAC_APELLMATER, " +   
                        "TRIM(SUBSTR(PAC.PAC_PAC_NOMBRE, 1, INSTR(PAC.PAC_PAC_NOMBRE,' '))) PRINOMBRE, " +
                        "TRIM(SUBSTR(PAC.PAC_PAC_NOMBRE,  INSTR(PAC.PAC_PAC_NOMBRE,' '))) SEGNOMBRE, " +         
                        "PAC.PAC_PAC_TIPOIDENTCODIGO, " +
                        "PAC.PAC_PAC_RUT, TO_CHAR(PAC.PAC_PAC_FECHANACIM, 'YYYY/MM/DD') PAC_PAC_FECHANACIM, PAC.PAC_PAC_DIRECCIONGRALHABIT,  SUBSTR(trim(PAC.PAC_PAC_FONO), 1,7) PAC_PAC_FONO, REG.DVP_REG_GLOSA DEPARTAMENTO , " +
                        "PRO.DVP_PRO_GLOSA CIUDAD , TMC.MTVCON_ORIGEN, TRI.TRIAGECOD, TO_CHAR(URG.RPA_FDA_HORAINGRESO,'YYYY/MM/DD') FECHAINGRESO, " +  
                        "TO_CHAR(URG.RPA_FDA_HORAINGRESO,'HH24:MM') HORAINGRESO, MOT.MTVTEXTO,  " +
                        "URG.MTVCORRELATIVO, URG.PAC_PAC_NUMERO, TID.tipidav TIPOID, (SELECT TRIM(rtrim (xmlagg (xmlelement (e, OBJTEXTO || ',')).extract ('//text()'), ',')) as TEXTO FROM TABOBJETIVOS WHERE PAC_PAC_NUMERO =URG.PAC_PAC_NUMERO AND MTVCORRELATIVO=URG.MTVCORRELATIVO GROUP BY PAC_PAC_NUMERO) TEXTO   " +
                        "FROM RPA_FORDAU URG, PAC_PACIENTE PAC, TABTRIAGE TRI, TABMOTIVOCONSDET MOT, DVP_REGION REG, DVP_PROVINCIA PRO, tab_tipoident TID, TABMOTIVOCONS TMC " +
                        "WHERE URG.PAC_PAC_NUMERO = PAC.PAC_PAC_NUMERO " +
                        "AND URG.PAC_PAC_NUMERO = TRI.PAC_PAC_NUMERO " + 
                        "AND URG.RPA_FOR_TIPOFORMU = TRI.RPA_FOR_TIPOFORMU " + 
                        "AND URG.RPA_FOR_NUMERFORMU = TRI.RPA_FOR_NUMERFORMU " +
                        "AND URG.MTVCORRELATIVO = MOT.MTVCORRELATIVO(+) " +
                        "AND URG.PAC_PAC_NUMERO = MOT.PAC_PAC_NUMERO(+) and PAC.pac_pac_tipoidentcodigo=TID.tab_tipoidentcodigo " +
                        "AND PAC.PAC_PAC_REGIOHABIT = REG.DVP_REG_CODIGO " +
                        "AND PAC.PAC_PAC_CIUDAHABIT = PRO.DVP_PRO_CODIGO  AND URG.MTVCORRELATIVO=TMC.MTVCORRELATIVO AND URG.PAC_PAC_NUMERO=TMC.PAC_PAC_NUMERO " +
                        // "AND URG.RPA_FOR_TIPOFORMU = '04  ' " +
                         "AND URG.ATE_PRE_NUMERFORMU = '" + Formulario + "' " +
                         "AND URG.PAC_PAC_NUMERO = (SELECT PAC.PAC_PAC_NUMERO " +
                                                    "FROM PAC_PACIENTE PAC " +
                                                    "WHERE PAC.PAC_PAC_RUT = '" + Paciente +"')";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet Anexo2DiagnosticosConsultar(string Correlativo, string Paciente)
        {
            string[] nomParam = 
            {
                "Correlativo",
                "Paciente"
            };

            object[] vlrParam = 
            {
                Correlativo ,
                Paciente
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT DIA.DIA_DIA_CODIGO CODIGO, MAE.DIA_DIA_DESCRIPCIO DESCRIPCION " +
                         "FROM TABDIAGNOSTICOS DIA, DIA_DIAGNOS MAE " +
                         "WHERE DIA.DIA_DIA_CODIGO = MAE.DIA_DIA_CODIGO " +
                         "AND DIA.MTVCORRELATIVO = " + Correlativo + " " +
                         "AND DIA.PAC_PAC_NUMERO = " + Paciente + " order by dgnprincipal Desc";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        public bool Anexo2Crear(string Id, string Formulario, string NoSolicitud, string Para, string Mensaje, string Mensaje2, string tipoAnexo, string Asunto, string Ruta,  string Usuario)
        {
            bool bandera = false;
            string[] nomParam = { 
            "@NoSolicitud",
            "@Para",
            "@Mensaje",
            "@Mensaje2",
            "@TipoAnexo",
            "@Asunto",
            "@Ruta"
            };
            object[] vlrParam = {
            NoSolicitud,
            Para,
            Mensaje,
            Mensaje2,
            tipoAnexo,
            Asunto,
            Ruta
            };
            string sql1 = "";
            sql1 = "select a.pac_pac_numero, a.ate_pre_tipoformu from rpa_fordau a, pac_paciente b where a.pac_pac_numero=b.pac_pac_numero and trim(b.pac_pac_rut)='" + Id + "' and a.ate_pre_numerformu  = '" + Formulario + "'";
            DataRow Valor = getDataTable1(sql1, "Oracle").Rows[0];
            string SQL = "INSERT INTO CENAUT_ANEXO2 " +
           "(PAC_PAC_NUMERO " +
           ",ATE_PRE_TIPOFORMU " +
           ",ATE_PRE_NUMERFORMU " +
           ",NUM_SOLICITUD " +
           ",USUARIO " +
           ",FECHA ) " +
           " VALUES " +
           "(" + Valor[0] + " " +
           ", '" + Valor[1] + "' " +
           ", '" + Formulario + "' " +
           ", '" + NoSolicitud + "' " +
           ", '" + Usuario + "' " +
           ", sysdate )"
           ;
            if (EjecutarSentenciaOracle(SQL))
            {
                bandera = EjecutarSentencia("uspAnexoActualizar", nomParam, vlrParam);
            }
            return bandera;
        }
        public DataSet TraerUsuario(string Usuario)
        {
            string[] nomParam = 
            {
                "Usuario"
            };
            object[] vlrParam = 
            {
                Usuario
            };
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "select TRIM(fld_username), TRIM(usrcargo) from tbl_user where fld_usercode='" + Usuario + "'";
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataset(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }
        private bool EjecutarSentenciaOracle(string sql)
        {
            return new DA.DA("Oracle").EjecutarSentencia(sql);
            //return new DA.DA().EjecutarSentenciaOracle(sql);
        }
        private DataTable getDataTable1(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTable(sql);
        }

        #endregion
    }
}
