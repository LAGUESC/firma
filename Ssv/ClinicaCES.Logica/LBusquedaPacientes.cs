﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LBusquedaPacientes
    {
        public DataSet BusquedaUrgencias(string FechaIni, string FechaFin, string TipoId, string Id)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select DISTINCT Decode(b.estado,'1','SI','NO') Requiere_Anexo2 ,GlbGetPacCar(P.Pac_pac_Numero) IDENTIFICACION ,nomcompletopaciente(P.PAC_PAC_NUMERO) paciente,   " +
"C.Con_Con_Descripcio convenio, F.Rpa_Fda_Horaingreso fecha_ingreso, " + // DECODE(ane2.ate_pre_numerformu,NULL,'SIN ANEXO',ANE2.NUM_SOLICITUD ) SOLICITUD,    
"F.ate_pre_numerformu , F.ate_pre_tipoformu, P.Pac_pac_Numero   " +
     "   From   Rpa_ForDau F, Con_Convenio C , Pac_Paciente P, cenaut_convenios b, cenaut_anexo2 ane2   " +
     "   Where  F.Pac_Pac_Numero = P.Pac_Pac_Numero  and  F.Con_Con_Codigo = C.Con_Con_Codigo  and P.PAC_PAC_Numero >= 0 and f.urgetd<> 'T'  and F.ate_pre_numerformu<>' '   " +
     "   And    F.Rpa_Fda_Horaingreso >= To_Date(SubStr('" + FechaIni + "',1,10),'YYYY/MM/DD HH:MI:SSAM') And    F.Rpa_Fda_Horaingreso  < To_Date(SubStr('" + FechaFin + "',1,10),'YYYY/MM/DD HH:MI:SSAM') + 1  " +
     "   and    F.Con_Con_Codigo = b.codconvenio AND F.ate_pre_numerformu = ane2.ate_pre_numerformu(+) And ane2.ate_pre_numerformu is null and F.ate_pre_tipoformu = ane2.ate_pre_tipoformu(+) And F.pac_pac_numero = ane2.Pac_Pac_Numero(+) and trim(p.pac_pac_rut)='" + Id + "' and PAC_PAC_TIPOIDENTCODIGO='" + TipoId + "' " +
      "  Order by 3";

          //string sql=  "        Select DISTINCT Decode(b.estado,'1','SI','NO') Requiere_Anexo2 ,GlbGetPacCar(P.Pac_pac_Numero) IDENTIFICACION ,nomcompletopaciente(P.PAC_PAC_NUMERO) paciente,   " +
          //"  C.Con_Con_Descripcio convenio, F.Rpa_Fda_Horaingreso fecha_ingreso, F.ate_pre_numerformu , F.ate_pre_tipoformu, P.Pac_pac_Numero,   ACC.RPA_FOR_NUMERFORMU FORACC  " +
          //  "   From   Rpa_ForDau F, Con_Convenio C , Pac_Paciente P, cenaut_convenios b, cenaut_anexo2 ane2, ACC_ACCIDENTE ACC      Where  F.Pac_Pac_Numero = P.Pac_Pac_Numero    " +
          //"     and  F.Con_Con_Codigo = C.Con_Con_Codigo  and P.PAC_PAC_Numero >= 0 and f.urgetd<> 'T'  and F.ate_pre_numerformu<>' '        " +
          //"     And    F.Rpa_Fda_Horaingreso >= To_Date(SubStr('" + FechaIni + "',1,10),'YYYY/MM/DD HH:MI:SSAM') And    F.Rpa_Fda_Horaingreso   " +
          // "     < To_Date(SubStr('" + FechaFin + "',1,10),'YYYY/MM/DD HH:MI:SSAM') + 1     and    F.Con_Con_Codigo = b.codconvenio   " +
          // "     AND F.ate_pre_numerformu = ane2.ate_pre_numerformu(+) And ane2.ate_pre_numerformu is null and F.ate_pre_tipoformu =   " +
          //"      ane2.ate_pre_tipoformu(+) And F.pac_pac_numero = ane2.Pac_Pac_Numero(+) AND F.PAC_PAC_NUMERO=ACC.PAC_PAC_NUMERO(+) AND F.RPA_FOR_NUMERFORMU=ACC.RPA_FOR_NUMERFORMU(+)  " +
          //"       and trim(p.pac_pac_rut)='" + Id + "' and PAC_PAC_TIPOIDENTCODIGO='" + TipoId + "'   Order by 3  ";


            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        public DataSet BusquedaSoat(string FechaIni, string FechaFin, string TipoId, string Id)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select DISTINCT Decode(b.estado,'1','SI','NO') Requiere_AnexoSoat ,GlbGetPacCar(P.Pac_pac_Numero) IDENTIFICACION ,  " +
                       " nomcompletopaciente(P.PAC_PAC_NUMERO) paciente, C.Con_Con_Descripcio convenio " +
                       "  , A.ACDFEC FECHAACC, A.ACDHRA HORAACC, P.Pac_pac_Numero " +
                        "  from TABACCTRNPAC a, Pac_Paciente P, Con_Convenio C, cenaut_convenios b, TABFORASOACC d, cenaut_anexo_soat anesoat " +
                        "  where a.Pac_Pac_Numero = P.Pac_Pac_Numero " +
                        "   and  a.Con_Con_Codigo = C.Con_Con_Codigo " +
                         "  and a.Con_Con_Codigo = b.codconvenio " +
                        "  and A.PAC_PAC_NUMERO=D.PAC_PAC_NUMERO " +
                        "  and A.ACDHRA=D.ACDTRNHRA and A.ACDFEC=D.ACDTRNFEC " +
                       "  AND D.RPA_FOR_NUMERFORMU = anesoat.RPA_FOR_NUMERFORMU(+)   " +
                       "  and D.RPA_FOR_TIPOFORMU = anesoat.RPA_FOR_TIPOFORMU(+)  " +
                       "  And d.pac_pac_numero = anesoat.PAC_PAC_NUMERO(+) and D.ACDTRNFEC = anesoat.ACDFEC(+) and D.ACDTRNHRA = anesoat.ACDHRA(+)" +
                        "  and trim(p.pac_pac_rut)='" + Id + "' and p.PAC_PAC_TIPOIDENTCODIGO='" + TipoId + "'  " +
                        "  and A.ACDFEC  >= To_Date(SubStr('" + FechaIni + "',1,10),'YYYY/MM/DD HH:MI:SSAM') And   A.ACDFEC   < To_Date(SubStr('" + FechaFin + "',1,10),'YYYY/MM/DD HH:MI:SSAM') + 1 ";


            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        public DataSet BusquedaPacienteUrgencias(string TipoId, string Id)
        {
            string[] nomParam = 
            {
                "In_TipoId",
                "In_Id"
            };

            object[] vlrParam = 
            {
                TipoId,
                Id
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select distinct GlbGetPacCar(P.Pac_pac_Numero) IDENTIFICACION ,nomcompletopaciente(P.PAC_PAC_NUMERO) paciente,     " +
"C.Con_Con_Descripcio convenio, F.Rpa_Fda_Horaingreso fecha_ingreso,   " +
"Round(Sysdate - F.Rpa_Fda_Horaingreso) DiasHosp,    " +
"F.ate_pre_numerformu , F.ate_pre_tipoformu, P.Pac_pac_Numero    " +
"       From   Rpa_ForDau F, Con_Convenio C , Pac_Paciente P, cenaut_convenios b, cenaut_anexo3 ane2   " +
"       Where  F.Pac_Pac_Numero = P.Pac_Pac_Numero  and  F.Con_Con_Codigo = C.Con_Con_Codigo  and P.PAC_PAC_Numero >= 0  AND F.URGETD<>'X'  " +
"      AND TRIM(P.PAC_PAC_RUT) = '" + Id + "' AND TRIM(P.PAC_PAC_TIPOIDENTCODIGO) = '" + TipoId + "' " +
"      and    F.Con_Con_Codigo = b.codconvenio AND F.ate_pre_numerformu = ane2.ate_pre_numerformu(+) And F.ate_pre_tipoformu = ane2.ate_pre_tipoformu(+) And F.pac_pac_numero = ane2.Pac_Pac_Numero(+)  " + 
"        Order by 3";

//            string sql = "Select GlbGetPacCar(P.Pac_pac_Numero) IDENTIFICACION ,nomcompletopaciente(P.PAC_PAC_NUMERO) paciente,     " +
//"C.Con_Con_Descripcio convenio, F.Rpa_Fda_Horaingreso fecha_ingreso, DECODE(ane2.ate_pre_numerformu,NULL,'SIN ANEXO',ANE2.NOSOLICITUD ) SOLICITUD,  " +
//"Round(Sysdate - F.Rpa_Fda_Horaingreso) DiasHosp,    " +
//"F.ate_pre_numerformu , F.ate_pre_tipoformu, P.Pac_pac_Numero    " +
//"       From   Rpa_ForDau F, Con_Convenio C , Pac_Paciente P, cenaut_convenios b, cenaut_anexo3 ane2   " +
//"       Where  F.Pac_Pac_Numero = P.Pac_Pac_Numero  and  F.Con_Con_Codigo = C.Con_Con_Codigo  and P.PAC_PAC_Numero >= 0  AND F.URGETD<>'X'  " +
//"      AND TRIM(P.PAC_PAC_RUT) = '" + Id + "' AND TRIM(P.PAC_PAC_TIPOIDENTCODIGO) = '" + TipoId + "' " +
//"      and    F.Con_Con_Codigo = b.codconvenio AND F.ate_pre_numerformu = ane2.ate_pre_numerformu(+) And F.ate_pre_tipoformu = ane2.ate_pre_tipoformu(+) And F.pac_pac_numero = ane2.Pac_Pac_Numero(+)  " +
//"        Order by 3";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        public DataSet BusquedaPaciente(string Id)
        {
            string[] nomParam = 
            {
                //"In_TipoId",
                "In_Id"
            };

            object[] vlrParam = 
            {
                //TipoId,
                Id
            };




            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };


            //string sql = "SELECT GlbGetPacCar(A.PAC_PAC_NUMERO) IDENTIFICACION, GlbGetNomPac(A.PAC_PAC_NUMERO) PACIENTE, A.SER_OBJ_CODIGO CAMA " +
            //            "FROM ATC_ESTADIA A, ATC_OCUPACAMA B, PAC_PACIENTE C " +
            //            "WHERE TRIM(A.ATCESTADO) IN ('H','C','X') " +
            //            "AND A.PAC_PAC_NUMERO = C.PAC_PAC_NUMERO " +
            //            "AND A.PAC_PAC_NUMERO = B.PAC_PAC_NUMERO(+) " +
            //            "AND A.ATC_EST_NUMERO = B.ATC_EST_NUMERO(+) " +
            //            "AND TRIM(C.PAC_PAC_RUT) = '" + Id + "' ";


            string sql = "SELECT GlbGetPacCar(C.PAC_PAC_NUMERO) IDENTIFICACION, GlbGetNomPac(C.PAC_PAC_NUMERO) PACIENTE,patngetedacli(PAC_PAC_FECHANACIM) EDAD" + 
                         " FROM PAC_PACIENTE C " +
                         " WHERE TRIM(C.PAC_PAC_RUT) = '" + Id + "' ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        public DataSet BusquedaHospitalizacion(string TipoId, string Id)
        {
            string[] nomParam = 
            {
                "In_TipoId",
                "In_Id"
            };

            object[] vlrParam = 
            {
                TipoId,
                Id
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT distinct Decode(b.estado,'1','SI','NO') Requiere_Anexo3, pac_pac_rut AS IDENTIFICACION,  nomcompletopaciente(atc.pac_pac_numerO) AS nombre, cnv.con_con_descripcio AS convenio, atc.atc_est_fechahospi AS fecha_ingreso, atc.atc_est_numero AS estadia, atc.ser_obj_codigo AS cama   " +
       "   FROM atc_estadia atc, pac_paciente pac, con_convenio cnv, cenaut_convenios b, cenaut_anexo3 ane3  " +
        " WHERE atc.atcestado = 'H'  AND NOT EXISTS(SELECT 'X' FROM tabreptcta apt  WHERE atc.atc_est_numero = apt.atc_est_numero AND atc.pac_pac_numero = apt.pac_pac_numero AND apt.reingreso = 0 AND ROWNUM = 1)  " +
         "  AND atc.atc_est_fechahospi >= TO_DATE('1900/01/01','YYYY/MM/DD') AND atc.atc_est_fechahospi < sysdate  " +
          " AND atc.codigocentroaten = 'CES  '  " +
          " AND pac.pac_pac_numero = atc.pac_pac_numero AND cnv.con_con_codigo = atc.con_con_codigo and atc.Con_Con_Codigo = b.codconvenio AND atc.atc_est_numero = ane3.atc_est_numero(+) And atc.pac_pac_numero = ane3.Pac_Pac_Numero(+)  " +
        " and trim(PAC.PAC_PAC_RUT) = trim('" + Id + "') AND PAC.PAC_PAC_TIPOIDENTCODIGO=" + TipoId + "  " +
        "   order by 3";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        public DataTable consultarDatosAnexo3(string Paciente, string Formulario, string Tipo)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            if (Tipo == "1")
            {
//                sql = "select TRIM(a.pac_pac_apellpater) PRIAPELLIDO, TRIM(a.PAC_PAC_APELLMATER) SEGAPELLIDO,  TRIM(SUBSTR(a.PAC_PAC_NOMBRE, 1, INSTR(a.PAC_PAC_NOMBRE,' '))) PRINOMBRE, TRIM(SUBSTR(a.PAC_PAC_NOMBRE,  INSTR(a.PAC_PAC_NOMBRE,' '))) SEGNOMBRE, TRIM(A.PAC_PAC_RUT) ID, " +
//"a.PAC_PAC_TIPOIDENTCODIGO TIPOID, a.PAC_PAC_FECHANACIM FECNACIMIENTO, TRIM(a.PAC_PAC_DIRECCIONGRALHABIT) DIRECCION,  TRIM(SUBSTR(a.PAC_PAC_FONO, 1, INSTR(a.PAC_PAC_FONO,' '))) TELEFONO, TRIM(b.DVP_REG_Glosa) DEPARTAMENTO, TRIM(c.DVP_PRO_Glosa) CIUDAD, " +
//"  TRIM(SUBSTR(a.PAC_PAC_FONO,  INSTR(a.PAC_PAC_FONO,' '))) CELULAR, TRIM(a.DONURL) CORREO, d.tab_cont_codigo CONTINGENCIA " +
//"from pac_paciente a, DVP_Region b, DVP_Provincia c, rpa_fordau d " +
//" WHERE TRIM(A.PAC_PAC_RUT)='" + Paciente + "' " +
//"and a.PAC_PAC_REGIOHABIT = b.DVP_REG_Codigo " +
//"and a.PAC_PAC_CIUDAHABIT= c.DVP_PRO_Codigo " +
//"and a.pac_pac_numero=d.pac_pac_numero " +
//"and d.ate_pre_numerformu='" + Formulario + "'";


                sql = "select TRIM(a.pac_pac_apellpater) PRIAPELLIDO, TRIM(a.PAC_PAC_APELLMATER) SEGAPELLIDO,  TRIM(SUBSTR(a.PAC_PAC_NOMBRE, 1, INSTR(a.PAC_PAC_NOMBRE,' '))) PRINOMBRE, TRIM(SUBSTR(a.PAC_PAC_NOMBRE,  " +
" INSTR(a.PAC_PAC_NOMBRE,' '))) SEGNOMBRE, TRIM(A.PAC_PAC_RUT) ID, a.PAC_PAC_TIPOIDENTCODIGO TIPOID, TO_CHAR(a.PAC_PAC_FECHANACIM, 'YYYY/MM/DD') FECNACIMIENTO, TRIM(a.PAC_PAC_DIRECCIONGRALHABIT) DIRECCION,  " +
"  SUBSTR(trim(a.PAC_PAC_FONO), 1,7) TELEFONO, TRIM(b.DVP_REG_Glosa) DEPARTAMENTO, TRIM(c.DVP_PRO_Glosa) CIUDAD,  SUBSTR(trim(a.PAC_PAC_FONO),8, 10)   " +
"  CELULAR, TRIM(a.DONURL) CORREO, g.MTVCON_ORIGEN CONTINGENCIA, Trim(e.dia_dia_codigo) CIE, trim(f.dia_dia_descripcio) DESCRIPCION, " +
"trim(h.SER_PRO_NOMBRES) || ' ' ||  trim(h.SER_PRO_APELLPATER) || ' ' || trim(h.SER_PRO_APELLMATER) SOLICITANTE, I.TIPOPROFENOMBRE CARGO, j.tipidav, '' CELULAR, '', '', (SELECT TRIM(rtrim (xmlagg (xmlelement (e, OBJTEXTO || ',')).extract ('//text()'), ',')) as TEXTO FROM TABOBJETIVOS WHERE PAC_PAC_NUMERO =d.PAC_PAC_NUMERO AND MTVCORRELATIVO=d.MTVCORRELATIVO GROUP BY PAC_PAC_NUMERO)   " +
"  from pac_paciente a, DVP_Region b, DVP_Provincia c, rpa_fordau d ,tabdiagnosticos e,  dia_diagnos f, tabmotivocons g, ser_profesiona h, tab_tipoprofe i, tab_tipoident j " +
"  WHERE TRIM(A.PAC_PAC_RUT)='" + Paciente + "' and a.PAC_PAC_REGIOHABIT = b.DVP_REG_Codigo and a.PAC_PAC_CIUDAHABIT= c.DVP_PRO_Codigo and a.pac_pac_numero=d.pac_pac_numero  " +
 " and d.ate_pre_numerformu='" + Formulario + "'  and d.PAC_PAC_NUMERO=e.pac_pac_numero   and d.mtvcorrelativo=e.mtvcorrelativo and a.pac_pac_tipoidentcodigo=j.tab_tipoidentcodigo and d.mtvcorrelativo=g.mtvcorrelativo and g.mtvusuario = h.ser_pro_cuenta " +
"and g.ser_pro_rut = h.ser_pro_rut and d.pac_pac_numero=g.pac_pac_numero AND H.SER_PRO_TIPO=I.TIPOPROFECODIGO and   trim(e.dia_dia_codigo)=trim(f.dia_dia_codigo) order by e.dgnprincipal desc";
            }
            else if (Tipo == "3")
            {
                string[] Filtros = Paciente.Split(';');
                sql="SELECT  TRIM(pac.pac_pac_apellpater) PRIAPELLIDO, TRIM(pac.PAC_PAC_APELLMATER) SEGAPELLIDO,  TRIM(SUBSTR(pac.PAC_PAC_NOMBRE, 1, INSTR(pac.PAC_PAC_NOMBRE,' '))) PRINOMBRE, TRIM(SUBSTR(pac.PAC_PAC_NOMBRE,    " +
"  INSTR(pac.PAC_PAC_NOMBRE,' '))) SEGNOMBRE, TRIM(pac.PAC_PAC_RUT) ID, pac.PAC_PAC_TIPOIDENTCODIGO TIPOID, TO_CHAR(pac.PAC_PAC_FECHANACIM, 'YYYY/MM/DD') FECNACIMIENTO, TRIM(pac.PAC_PAC_DIRECCIONGRALHABIT) DIRECCION,    " +
"   SUBSTR(trim(pac.PAC_PAC_FONO), 1,7) TELEFONO, TRIM(REG.DVP_REG_Glosa) DEPARTAMENTO, TRIM(PRO.DVP_PRO_Glosa) CIUDAD,  SUBSTR(trim(pac.PAC_PAC_FONO),8, 10)     " +
"   CELULAR, TRIM(pac.DONURL) CORREO, G.MTVCON_ORIGEN CONTINGENCIA,  Trim(e.dia_dia_codigo) CIE, trim(f.dia_dia_descripcio) DESCRIPCION  " +
 "  , trim(h.SER_PRO_NOMBRES) || ' ' ||  trim(h.SER_PRO_APELLPATER) || ' ' || trim(h.SER_PRO_APELLMATER) SOLICITANTE, I.TIPOPROFENOMBRE CARGO  " +
"   , j.tipidav, '', trim(ATC.SER_OBJ_CODIGO) cama, trim(K.SER_SER_DESCRIPCIO) servici,  (SELECT TRIM(rtrim (xmlagg (xmlelement (e, OBJTEXTO || ',')).extract ('//text()'), ',')) as TEXTO FROM TABOBJETIVOS WHERE PAC_PAC_NUMERO =atc.PAC_PAC_NUMERO AND MTVCORRELATIVO=atc.MTVCORRELATIVO GROUP BY PAC_PAC_NUMERO)  " +
 "          FROM atc_estadia atc, pac_paciente pac , DVP_Region REG, DVP_Provincia PRO ,tabdiagnosticos e,  dia_diagnos f, tabmotivocons g, tab_tipoident j, ser_profesiona h, tab_tipoprofe i, ser_servicios k  " +
 "         WHERE atc.atcestado = 'H'  AND NOT EXISTS(SELECT 'X' FROM tabreptcta apt  WHERE atc.atc_est_numero = apt.atc_est_numero AND atc.pac_pac_numero = apt.pac_pac_numero AND apt.reingreso = 0 AND ROWNUM = 1)  " +
"            AND atc.atc_est_fechahospi >= TO_DATE('2008/01/01','YYYY/MM/DD') AND atc.atc_est_fechahospi < sysdate  " +
 "           AND atc.codigocentroaten = 'CES  ' and pac.pac_pac_tipoidentcodigo=j.tab_tipoidentcodigo   " +
 "           AND pac.pac_pac_numero = atc.pac_pac_numero and PAC.PAC_PAC_REGIOHABIT = REG.DVP_REG_Codigo and PAC.PAC_PAC_CIUDAHABIT= PRO.DVP_PRO_Codigo  " +
 "           and ATC.PAC_PAC_NUMERO=e.pac_pac_numero   and ATC.mtvcorrelativo=e.mtvcorrelativo and ATC.PAC_PAC_NUMERO=g.pac_pac_numero and ATC.mtvcorrelativo=g.mtvcorrelativo  " +
"            and g.ser_pro_rut = h.ser_pro_rut and pac.pac_pac_numero=g.pac_pac_numero and H.SER_PRO_TIPO = I.TIPOPROFECODIGO and ATC.SER_SER_CODIGO = K.SER_SER_CODIGO(+)  " +
 "           and trim(PAC.PAC_PAC_RUT) = trim(" + Filtros[1].ToString().Trim() + ") and pac.PAC_PAC_TIPOIDENTCODIGO =" + Filtros[0].ToString().Trim() + "  " +
 "           AND trim(e.dia_dia_codigo)=trim(f.dia_dia_codigo) order by e.dgnprincipal desc";

 
            }
            //sql = "select a.con_con_codigo CODCONVENIO, a.con_con_descripcio DESCRIPCION, decode(b.estado, 0, 'False', 1, 'True', '', 'False') ESTADO from con_convenio a left join cenaut_convenios b on a.con_con_codigo=b.codconvenio where a.con_con_vigencia='S' and trim(a.con_emp_rut)=trim('" + CodEmpresa + "') order by 2";


            return getDataTable(sql, "Oracle");
        }
        public DataTable consultarDatosAnexoSoat(string Paciente, DateTime Fecha, string Hora)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            
                string[] Filtros = Paciente.Split(';');
                sql = "select DISTINCT  TRIM(p.pac_pac_apellpater) PRIAPELLIDO, TRIM(p.PAC_PAC_APELLMATER) SEGAPELLIDO,  TRIM(SUBSTR(p.PAC_PAC_NOMBRE, 1, INSTR(p.PAC_PAC_NOMBRE,' '))) PRINOMBRE,  " +
                      "  TRIM(SUBSTR(p.PAC_PAC_NOMBRE,    INSTR(p.PAC_PAC_NOMBRE,' '))) SEGNOMBRE, TRIM(p.PAC_PAC_RUT) ID, p.PAC_PAC_TIPOIDENTCODIGO TIPOID, TO_CHAR(p.PAC_PAC_FECHANACIM, 'YYYY/MM/DD') FECNACIMIENTO,  " +
                       " decode(e.TPOTRP, 'M', '1', 'B', '1', '', '2') AMB, e.TRPPACPLA PLACAAMB    ,TO_CHAR(F.ACDFEC,'YYYY/MM/DD') FECACC, F.ACDHRA FECHORA,  " +
                      "  TRIM(H.DVP_REG_CODIGO) DEPACC , TRIM(I.DVP_PRO_CODIGO) MUNACC, G.ACDLUG DIRACC, G.PLCVHC PLACAVEHACC, G.CNDAPELLPAT CONDUCTORPATERNO,  " +
                      "  G.CNDAPELLMAT CONDUCTORMATERNO, G.CNDNOM CONDUCTORNOMBRE,   G.CNDSEGNOM CONDUCTORSEGNOMBRE, E.CNDIDTPO CONDUCTORTIPOID, G.CNDIDE CONDUCTORID, G.CNDDIR CONDUCTORDIR, G.CNDFONO CONDUCTORTEL, TRIM(J.DVP_REG_CODIGO) CONDUCTORDEP,  " +
                       " TRIM(K.DVP_PRO_CODIGO) CONDUCTORMUN, I.TIPIDAV      from TABACCTRNPAC a, Pac_Paciente P, Con_Convenio C, cenaut_convenios b, TABFORASOACC d, cenaut_anexo_soat anesoat, tabacdtrnacc e, ACC_Accidente f, TabAcdTrn g, DVP_Region h, DVP_Provincia i, " +
                      "  DVP_Region J, DVP_Provincia K , tab_tipoident i, RPA_FORDAU DAU  where a.Pac_Pac_Numero = P.Pac_Pac_Numero      " +
                       " and  a.Con_Con_Codigo = C.Con_Con_Codigo     and a.Con_Con_Codigo = b.codconvenio    and A.PAC_PAC_NUMERO=D.PAC_PAC_NUMERO     and A.ACDHRA=D.ACDTRNHRA and A.ACDFEC=D.ACDTRNFEC   and G.DVP_REG_CODIGO=H.DVP_REG_CODIGO and G.DVP_PRO_CODIGO=I.DVP_PRO_CODIGO " +
                       " and E.CNDDEP=J.DVP_REG_CODIGO AND E.CNDCIUCOD=K.DVP_PRO_CODIGO AND A.PAC_PAC_NUMERO=DAU.PAC_PAC_NUMERO " +
                       " AND D.RPA_FOR_NUMERFORMU = anesoat.RPA_FOR_NUMERFORMU(+) And anesoat.RPA_FOR_NUMERFORMU is null    and D.RPA_FOR_TIPOFORMU = anesoat.RPA_FOR_TIPOFORMU(+)    And d.pac_pac_numero = anesoat.PAC_PAC_NUMERO(+)  and D.ACDTRNFEC = anesoat.ACDFEC(+) and D.ACDTRNHRA = anesoat.ACDHRA(+)  " +
                       " and A.ACDNUM=E.ACDNUM2   and A.PAC_PAC_NUMERO= F.PAC_PAC_NUMERO and A.ACDNUM=F.ACDNUM   and DAU.RPA_FOR_TIPOFORMU=F.RPA_FOR_TIPOFORMU and DAU.RPA_FOR_NUMERFORMU=F.RPA_FOR_NUMERFORMU   and A.ACDNUM=G.ACDNUM2  and P.PAC_PAC_TIPOIDENTCODIGO=I.TAB_TIPOIDENTCODIGO  " +
                       "  and trim(p.PAC_PAC_RUT) = trim(" + Filtros[1].ToString().Trim() + ") and p.PAC_PAC_TIPOIDENTCODIGO =" + Filtros[0].ToString().Trim() + " " +
                       "  and f.ACDFEC= TO_DATE('" + Fecha.ToString("yyyy/MM/dd") + "', 'YYYY/MM/DD') AND trim(F.ACDHRA)= trim('" + Hora + "')";
            return getDataTable(sql, "Oracle");
        }
        public string consultarDatosAnexoSoatg(string Paciente, DateTime Fecha, string Hora)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            string[] Filtros = Paciente.Split(';');
            sql = "select DISTINCT  TRIM(p.pac_pac_apellpater) PRIAPELLIDO, TRIM(p.PAC_PAC_APELLMATER) SEGAPELLIDO,  TRIM(SUBSTR(p.PAC_PAC_NOMBRE, 1, INSTR(p.PAC_PAC_NOMBRE,' '))) PRINOMBRE,  " +
                  "  TRIM(SUBSTR(p.PAC_PAC_NOMBRE,    INSTR(p.PAC_PAC_NOMBRE,' '))) SEGNOMBRE, TRIM(p.PAC_PAC_RUT) ID, p.PAC_PAC_TIPOIDENTCODIGO TIPOID, TO_CHAR(p.PAC_PAC_FECHANACIM, 'YYYY/MM/DD') FECNACIMIENTO,  " +
                   " decode(e.TPOTRP, 'M', '1', 'B', '1', '', '2') AMB, e.TRPPACPLA PLACAAMB    ,F.ACDFEC FECACC, F.ACDHRA FECHORA,  " +
                  "  H.DVP_REG_GLOSA DEPACC , I.DVP_PRO_GLOSA MUNACC, G.ACDLUG DIRACC, G.PLCVHC PLACAVEHACC, G.CNDAPELLPAT CONDUCTORPATERNO,  " +
                  "  G.CNDAPELLMAT CONDUCTORMATERNO, G.CNDNOM CONDUCTORNOMBRE,   G.CNDSEGNOM CONDUCTORSEGNOMBRE, E.CNDIDTPO CONDUCTORTIPOID, G.CNDIDE CONDUCTORID, G.CNDDIR CONDUCTORDIR, G.CNDFONO CONDUCTORTEL, J.DVP_REG_GLOSA CONDUCTORDEP,  " +
                   " K.DVP_PRO_GLOSA CONDUCTORMUN, I.TIPIDAV      from TABACCTRNPAC a, Pac_Paciente P, Con_Convenio C, cenaut_convenios b, TABFORASOACC d, cenaut_anexo_soat anesoat, tabacdtrnacc e, ACC_Accidente f, TabAcdTrn g, DVP_Region h, DVP_Provincia i, " +
                  "  DVP_Region J, DVP_Provincia K , tab_tipoident i, RPA_FORDAU DAU  where a.Pac_Pac_Numero = P.Pac_Pac_Numero      " +
                   " and  a.Con_Con_Codigo = C.Con_Con_Codigo     and a.Con_Con_Codigo = b.codconvenio    and A.PAC_PAC_NUMERO=D.PAC_PAC_NUMERO     and A.ACDHRA=D.ACDTRNHRA and A.ACDFEC=D.ACDTRNFEC   and G.DVP_REG_CODIGO=H.DVP_REG_CODIGO and G.DVP_PRO_CODIGO=I.DVP_PRO_CODIGO " +
                   " and E.CNDDEP=J.DVP_REG_CODIGO AND E.CNDCIUCOD=K.DVP_PRO_CODIGO AND A.PAC_PAC_NUMERO=DAU.PAC_PAC_NUMERO " +
                   " AND D.RPA_FOR_NUMERFORMU = anesoat.RPA_FOR_NUMERFORMU(+) And anesoat.RPA_FOR_NUMERFORMU is null    and D.RPA_FOR_TIPOFORMU = anesoat.RPA_FOR_TIPOFORMU(+)    And d.pac_pac_numero = anesoat.PAC_PAC_NUMERO(+)  and D.ACDTRNFEC = anesoat.ACDFEC(+) and D.ACDTRNHRA = anesoat.ACDHRA(+)  " +
                   " and A.ACDNUM=E.ACDNUM2   and A.PAC_PAC_NUMERO= F.PAC_PAC_NUMERO and A.ACDNUM=F.ACDNUM   and DAU.RPA_FOR_TIPOFORMU=F.RPA_FOR_TIPOFORMU and DAU.RPA_FOR_NUMERFORMU=F.RPA_FOR_NUMERFORMU   and A.ACDNUM=G.ACDNUM2  and P.PAC_PAC_TIPOIDENTCODIGO=I.TAB_TIPOIDENTCODIGO  " +
                   "  and trim(p.PAC_PAC_RUT) = trim(" + Filtros[1].ToString().Trim() + ") and p.PAC_PAC_TIPOIDENTCODIGO =" + Filtros[0].ToString().Trim() + " " +
                   "  and f.ACDFEC= TO_DATE('" + Fecha.ToString("yyyy/MM/dd") + "', 'YYYY/MM/DD') AND F.ACDHRA= '" + Hora + "'";
            return sql;
        }
        public DataTable consultarPrestxDefecto()
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "select trim(pre_pre_codigo), trim(pre_pre_descripcio) from pre_prestacion where pre_pre_codigo in ('890602  ', 'S11201  ')";
           
            return getDataTable(sql, "Oracle");
        }


        public DataTable traerCorreo(string Paciente, string Formulario, string Tipo)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            if (Tipo == "1")
            {
                sql = "select  distinct d.correo from rpa_fordau a, pac_paciente b, con_convenio c, cenaut_empresas d where a.pac_pac_numero=b.pac_pac_numero and trim(a.con_con_codigo)=trim(c.con_con_codigo)  " +
                "and trim(c.con_emp_rut) = trim(d.codempresa) and trim(a.ate_pre_numerformu)='" + Formulario + "' and trim(b.pac_pac_rut)=   '" + Paciente + "'";
            }
            if (Tipo == "3")
            {
                string[] Filtros = Paciente.Split(';');
                sql = "select DISTINCT d.correo from atc_estadia a, atc_cuenta h, pac_paciente b, con_convenio c, cenaut_empresas d where A.PAC_PAC_NUMERO=H.PAC_PAC_NUMERO and A.ATC_EST_NUMERO=H.ATC_EST_NUMERO and a.pac_pac_numero=b.pac_pac_numero  " +
                " and trim(h.con_con_codigo)=trim(c.con_con_codigo) and trim(c.con_emp_rut) = trim(d.codempresa) and trim(A.ATC_EST_NUMERO)='" + Formulario + "' and trim(b.pac_pac_rut)=   '" + Filtros[1] + "'";
            }
            return getDataTable(sql, "Oracle");
        }
        public DataTable traerCorreoSoat(string Paciente, string Formulario)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            string[] Filtros = Paciente.Split(';');
            sql = "SELECT DISTINCT d.correo from pac_paciente b, con_convenio c, cenaut_empresas d " +
            " where  trim(B.PAC_PAC_CODIGO)=trim(c.con_con_codigo) and trim(c.con_emp_rut) = trim(d.codempresa)  and trim(b.pac_pac_rut)= '" + Filtros[1] + "' " +
            " and trim(B.PAC_PAC_TIPOIDENTCODIGO) = '" + Filtros[0] + "'";
            return getDataTable(sql, "Oracle");
        }
        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }
        private DataTable getDataTable(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTable(sql);
        }

        #endregion
    }
}
