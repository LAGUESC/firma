﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LAnexos
    {
        //public bool NivelActualizar(Nivel nvl)
        //{
        //    string[] nomParam = { "@Codigo", "@Nivel" };
        //    object[] vlrParam = { nvl.Codigo, nvl.nivel };

        //    return EjecutarSentencia("uspNivelActualizar", nomParam, vlrParam);
        //}

        public bool AnexoActualizar(string Id, string Formulario, string NoSolicitud, string Para, string Mensaje, string Mensaje2, string tipoAnexo, string Asunto, string Ruta, string Codigo, string Descripcion, string Usuario, string tipo)
        {

            bool bandera = false;
            string[] nomParam = { 
            "@NoSolicitud",
            "@Para",
            "@Mensaje",
            "@Mensaje2",
            "@TipoAnexo",
            "@Asunto",
            "@Ruta"
           
            };

            object[] vlrParam = {
            NoSolicitud,
            Para,
            Mensaje,
            Mensaje2,
            tipoAnexo,
            Asunto,
            Ruta
            };
            string sql1 = "";
            if (tipo == "1")
            {
                sql1 = "select a.pac_pac_numero, a.ate_pre_tipoformu from rpa_fordau a, pac_paciente b where a.pac_pac_numero=b.pac_pac_numero and trim(b.pac_pac_rut)='" + Id + "' and a.ate_pre_numerformu  = '" + Formulario + "'";
            }
            else
            {
                if (tipo == "3")
                {
                    string[] identi = Id.Split(';');
                    sql1 = "select a.pac_pac_numero, '' from atc_estadia a, pac_paciente b where a.pac_pac_numero=b.pac_pac_numero and trim(B.PAC_PAC_RUT) = '" + identi[1] + "' and A.ATC_EST_NUMERO='" + Formulario + "'";
                }
            }
                DataRow Valor= getDataTable1(sql1, "Oracle").Rows[0];
            string SQL="";
            if (tipo == "1")
            {
                SQL = "INSERT INTO CENAUT_ANEXO3 " +
                "(NoSolicitud " +
                ",Codigos " +
                ",Descripcion " +
                ",ATE_PRE_NUMERFORMU " +
                ",PAC_PAC_NUMERO " +
                ",ATE_PRE_TIPOFORMU " +
                ",Usuario " +
                ",Fecha ) " +
                " VALUES " +
                "(" + NoSolicitud + " " +
                ", '" + Codigo + "' " +
                ", '" + Descripcion + "' " +
                ", '" + Formulario + "' " +
                ", '" + Valor[0] + "' " +
                ", '" + Valor[1] + "' " +
                ", '" + Usuario + "' " +
                ", sysdate )"
                ;
            }
            else
            {
                if (tipo == "3")
                {
                    SQL = "INSERT INTO CENAUT_ANEXO3 " +
                "(NoSolicitud " +
                ",Codigos " +
                ",Descripcion " +
                ",ATE_PRE_NUMERFORMU " +
                ",PAC_PAC_NUMERO " +
                ",ATE_PRE_TIPOFORMU " +
                ",Usuario " +
                ",Fecha " +
                ", ATC_EST_NUMERO    ) " +
                " VALUES " +
                "(" + NoSolicitud + " " +
                ", '" + Codigo + "' " +
                ", '" + Descripcion + "' " +
                ", ' ' " +
                ", '" + Valor[0] + "' " +
                ", ' ' " +
                ", '" + Usuario + "' " +
                ", sysdate " +
                ",'" + Formulario + "'     )"
                ;
                }
            }
            if (EjecutarSentenciaOracle(SQL))
            {
                bandera= EjecutarSentencia("uspAnexoActualizar", nomParam, vlrParam);
            }
            return bandera;
        }
        public bool AnexoSoatCrear(string[] Id, string Formulario, string NoSolicitud, DateTime FecAcc, string HoraAcc, string Para, string Mensaje, string Mensaje2, string tipoAnexo, string Asunto, string Ruta, string Usuario, string Radicado)
        {
            bool bandera = false;
            string[] nomParam = { 
            "@NoSolicitud",
            "@Para",
            "@Mensaje",
            "@Mensaje2",
            "@TipoAnexo",
            "@Asunto",
            "@Ruta"
            };
            object[] vlrParam = {
            NoSolicitud,
            Para,
            Mensaje,
            Mensaje2,
            tipoAnexo,
            Asunto,
            Ruta
            };
            string sql1 = "";
            sql1 = "select b.pac_pac_numero from pac_paciente b where  trim(b.pac_pac_rut)='" + Id[1] + "'";
            DataRow Valor = getDataTable1(sql1, "Oracle").Rows[0];
            string SQL = "INSERT INTO cenaut_anexo_soat " +
           "(PAC_PAC_NUMERO " +
           ",RPA_FOR_TIPOFORMU " +
           ",RPA_FOR_NUMERFORMU " +
           ",ACDHRA " +
           ",ACDFEC " +
           ",NUM_SOLICITUD " +
           ",USUARIO " +
           ",FECHA, RADICADO ) " +
           " VALUES " +
           "(" + Valor[0] + " " +
           ", '' " +
           ", '" + Formulario + "' " +
           ", '" + HoraAcc + "' " +
           ", TO_DATE('" + FecAcc.ToString("yyyy/MM/dd") + "', 'YYYY/MM/DD') " +
           ", '" + NoSolicitud + "' " +
           ", '" + Usuario + "' " +
           ", sysdate, '" + Radicado + "' )"
           ;
            if (EjecutarSentenciaOracle(SQL))
            {
                bandera = true;
            }
            return bandera;
        }
        public string AnexoSoatCrearGG(string[] Id, string Formulario, string NoSolicitud, DateTime FecAcc, string HoraAcc, string Para, string Mensaje, string Mensaje2, string tipoAnexo, string Asunto, string Ruta, string Usuario)
        {
            bool bandera = false;
            string[] nomParam = { 
            "@NoSolicitud",
            "@Para",
            "@Mensaje",
            "@Mensaje2",
            "@TipoAnexo",
            "@Asunto",
            "@Ruta"
            };
            object[] vlrParam = {
            NoSolicitud,
            Para,
            Mensaje,
            Mensaje2,
            tipoAnexo,
            Asunto,
            Ruta
            };
            string sql1 = "";
            sql1 = "select b.pac_pac_numero from pac_paciente b where  trim(b.pac_pac_rut)='" + Id[1] + "'";
            DataRow Valor = getDataTable1(sql1, "Oracle").Rows[0];
            string SQL = "INSERT INTO cenaut_anexo_soat " +
           "(PAC_PAC_NUMERO " +
           ",RPA_FOR_TIPOFORMU " +
           ",RPA_FOR_NUMERFORMU " +
           ",ACDHRA " +
           ",ACDFEC " +
           ",NUM_SOLICITUD " +
           ",USUARIO " +
           ",FECHA ) " +
           " VALUES " +
           "(" + Valor[0] + " " +
           ", '' " +
           ", '" + Formulario + "' " +
           ", '" + HoraAcc + "' " +
           ", TO_DATE('" + FecAcc.ToString("yyyy/MM/dd") + "', 'YYYY/MM/DD') " +
           ", '" + NoSolicitud + "' " +
           ", '" + Usuario + "' " +
           ", sysdate )"
           ;
          
            return SQL;
        }


        public DataTable AnexosBuscarEstado(string CodAnexo)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";

            sql = "SELECT ANE1.NUMERO ANEXO,  'ANEXO 1' TIPOANEXO, PAC.PAC_PAC_RUT IDPACIENTE, NOMCOMPLETOPACIENTE(PAC.PAC_PAC_NUMERO) PACIENTE  FROM CENAUT_ANEXO1 ANE1, PAC_PACIENTE PAC  " +
"WHERE ANE1.PAC_PAC_NUMERO=PAC.PAC_PAC_NUMERO AND ANE1.NUMERO='" + CodAnexo + "' " +
"UNION " +
"SELECT ANE2.NUM_SOLICITUD ANEXO,  'ANEXO 2' TIPOANEXO, PAC.PAC_PAC_RUT IDPACIENTE, NOMCOMPLETOPACIENTE(PAC.PAC_PAC_NUMERO) PACIENTE  FROM CENAUT_ANEXO2 ANE2, PAC_PACIENTE PAC " +
"WHERE ANE2.PAC_PAC_NUMERO=PAC.PAC_PAC_NUMERO AND ANE2.NUM_SOLICITUD='" + CodAnexo + "' " +
"UNION " +
"SELECT ANE3.NOSOLICITUD ANEXO,  'ANEXO 3' TIPOANEXO, PAC.PAC_PAC_RUT IDPACIENTE, NOMCOMPLETOPACIENTE(PAC.PAC_PAC_NUMERO) PACIENTE  FROM CENAUT_ANEXO3 ANE3, PAC_PACIENTE PAC " +
"WHERE ANE3.PAC_PAC_NUMERO=PAC.PAC_PAC_NUMERO AND ANE3.NOSOLICITUD='" + CodAnexo + " '";

            return getDataTable(sql, "Oracle");
        }
        public DataTable AnexosBuscarEstadoSql(string IdAnexo)
        {
            string[] nomParam = { "@IdAnexo" };
            object[] vlrParam = { IdAnexo };

            return getDataTable("uspAnexosBuscarEstadosSql", nomParam, vlrParam);
            //return getDataTable("uspPrestacionesVigentesConsultar", "Sql");
        }
        public DataTable AnexosBuscarImprimirSql(string IdPaciente)
        {
            string[] nomParam = { "@IdPaciente" };
            object[] vlrParam = { IdPaciente };

            return getDataTable("AnexosBuscarImprimirSql", nomParam, vlrParam);
            //return getDataTable("uspPrestacionesVigentesConsultar", "Sql");
        }
        public bool AnexosActualizarEstado(string CodAnexo, int TipoId, string Usuario)
        {
            string[] nomParam = { 
            "@CodAnexo",
            "@TipoId",
            "@Usuario"
           
            };

            object[] vlrParam = {
            CodAnexo,
            TipoId,
            Usuario
            };
            return EjecutarSentencia("uspAnexoActualizarEstado", nomParam, vlrParam);
        }
        
        


        public string NivelRetirar(Nivel nvl)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { nvl.Codigo };

            DataTable dt = getDataTable("uspNivelRetirar", nomParam, vlrParam);
            string Msg = "3";
            if(dt.Rows.Count > 0)
                Msg = dt.Rows[0]["MSG"].ToString();

            return Msg;
        }

        #region Metodos Privados

        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }
        private DataTable getDataTable(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTable(sql);
        }
        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {            
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }
        private bool EjecutarSentenciaOracle(string sql)
        {
            return new DA.DA("Oracle").EjecutarSentencia(sql);
            //return new DA.DA().EjecutarSentenciaOracle(sql);
        }
        private DataTable getDataTable1(string sql, string tipobd)
        {
            return new DA.DA(tipobd).getDataTable(sql);
        }

        #endregion

    }
}
