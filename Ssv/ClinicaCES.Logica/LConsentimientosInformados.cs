﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LConsentimientosInformados
    {
        #region "Métodos Públicos"
        public DataTable C_INFORMADO_ACTUALIZAR_APROBACION_MEDICO(int ID, bool Revision)
        {
            string[] nomParam =
            {
                "@ID_CONSENTIMIENTO_PDF",
                "@APROBACION_MEDICO"

            };

            object[] vlrParam =
            {
                ID,
                Revision
            };

            return getDataTable("uspC_INFORMADO_ACTUALIZAR_APROBACION_MEDICO", nomParam, vlrParam);
        }
        public DataTable C_INFORMADO_LISTAR_X_CEDULA(string ced)
        {
            string[] nomParam =
            {
                "@IDENTIFICACION_USUARIO"

            };

            object[] vlrParam =
            {
                ced,
            };

            return getDataTable("uspC_INFORMADO_LISTAR_X_CEDULA", nomParam, vlrParam);
        }
        public DataTable C_INFORMADO_LISTAR_X_CEDULAPA(string cedPA)
        {
            string[] nomParam =
            {
                "@IDENTIFICACION_PACIENTE"

            };

            object[] vlrParam =
            {
                cedPA,
            };

            return getDataTable("uspC_INFORMADO_LISTAR_X_CEDULAPA", nomParam, vlrParam);
        }
        public DataTable C_INFORMADO_LISTAR_X_RANGOS_FECHA(string inicio, string fin)
        {
            string[] nomParam =
            {
                "@INICIO",
                "@FIN"

            };

            object[] vlrParam =
            {
                Convert.ToDateTime(inicio),
                Convert.ToDateTime(fin)
            };

            return getDataTable("uspC_INFORMADO_LISTAR_X_RANGOS_FECHAS", nomParam, vlrParam);
        }
        public DataTable C_INFORMADO_LISTAR_X_MES(string mes, string anio)
        {
            string[] nomParam =
            {
                "@MES",
                "@ANIO"

            };

            object[] vlrParam =
            {
                Convert.ToInt16(mes),
                Convert.ToInt16(anio)
            };

            return getDataTable("uspC_INFORMADO_LISTAR_X_MES", nomParam, vlrParam);
        }
        public DataTable C_INFORMADO_LISTAR()
        {
            return getDataTable("uspC_INFORMADO_LISTAR");
        }
        public DataTable C_INFORMADO_COUNT()
        {
            return getDataTable("uspC_INFORMADO_COUNT");
        }

        public DataTable uspC_INFORMADO_LISTAR_F_HC_13()
        {
            return getDataTable("uspC_INFORMADO_LISTAR_F-HC-13");
        }
        public DataTable uspC_INFORMADO_LISTAR_F_HC_7()
        {
            return getDataTable("uspC_INFORMADO_LISTAR_F-HC-7");
        }
        public DataTable uspC_INFORMADO_LISTAR_F_SH_4()
        {
            return getDataTable("uspC_INFORMADO_LISTAR_F-SH-4");
        }
        public DataTable uspC_INFORMADO_LISTAR_F_HC_16()
        {
            return getDataTable("uspC_INFORMADO_LISTAR_F-HC-16");
        }

        public DataTable C_INFORMADO_COUNT_MES()
        {
            return getDataTable("uspC_INFORMADO_COUNT_MES");
        }
        public DataTable C_INFORMADO_VARIOS()
        {
            return getDataTable("uspC_INFORMADO_VARIOS");
        }
        public DataTable C_INFORMADO_CARGAR_TODOS()
        {
            return getDataTable("uspC_INFORMADO_CARGAR_TODOS");
        }
        public DataTable LoadingFirm()
        {
            return getDataTable("sptbCA_LoadingFirmDT");
        }
        public bool InsertFirmTemp(ConsentimientosInformados firm)
        {
            string[] nomParam =
            {
                "@UserFirmPath"
            };

            object[] vlrParam =
            {
                firm.UserFirmPath
            };

            return EjecutarSentencia("sptbCA_InsertImageTemp", nomParam, vlrParam);
        }
        public bool InsertFirmDB(ConsentimientosInformados con)
        {
            string[] nomParam =
            {
                "@Cedula",
                "@UserFirmPath"
            };

            object[] vlrParam =
            {
                con.Cedula,
                con.UserFirmPath
            };

            return EjecutarSentencia("sptbCA_InsertFirmDB", nomParam, vlrParam);
        }
        public bool UpdateFirmDB(ConsentimientosInformados con)
        {
            string[] nomParam =
            {
                "@Id_Firma",
                "@Cedula",
                "@UserFirmPath"
            };

            object[] vlrParam =
            {
                con.Id_Firma,
                con.Cedula,
                con.UserFirmPath
            };

            return EjecutarSentencia("sptbCA_UpdateFirmDB", nomParam, vlrParam);
        }
        public bool InsertPDFDB(ConsentimientosInformados PDF)
        {
            string[] nomParam =
            {
                "@IDENTIFICACION_PACIENTE",
                "@NOMBRE_PACIENTE",
                "@IDENTIFICACION_USUARIO",
                "@NOMBRE_USUARIO",
                "@FECHA_REGISTRO",
                "@CONSENTIMIENTO_HASH",
                "@CONSENTIMIENTO_PDF",
                "@APROBACION_MEDICO",
                "@BODY_HTML"
            };

            object[] vlrParam =
            {
                PDF.Cedula,
                PDF.NombrePaciente,
                PDF.CedulaUsuario,
                PDF.NombreUsuario,
                PDF.FechaRegistro,
                PDF.Hash,
                PDF.Consentimiento_PDF,
                PDF.AprobacionMedico,
                PDF.BodyHtml

            };

            return EjecutarSentencia("sptbCA_InsertPDFDB", nomParam, vlrParam);
        }
        public void InsertEvento(ConsentimientosInformados EVEN)
        {
            string[] nomParam =
            {
                "@CODIGO_CONSENTIMIENTO",
                "@IDENTIFICACION_PACIENTE"
            };

            object[] vlrParam =
            {
                EVEN.CodConsentimiento,
                EVEN.Cedula,
            };

            EjecutarSentencia("sptbCA_InsertEventos", nomParam, vlrParam);
        }
        public DataTable SearchFirm(string ced)
        {
            string[] nomParam =
            {
                "@Cedula"
            };

            object[] vlrParam =
            {
                ced
            };

            return getDataTable("sptbCA_SearchFirm", nomParam, vlrParam);
        }
        #endregion
        #region "Métodos Privados"
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }
        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }
        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }
        private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataset(sql, nomParam, valParam);
        }

        #endregion
    }
}
