﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ClinicaCES.Entidades;
using System.Data;

namespace ClinicaCES.Logica
{
    public class LEspecialidad
    {
        public bool EspecialidadActualizar(Especialidad Esp)
        {
            string[] nomParam = { "@Codigo", "@Especialidad" };
            object[] vlrParam = { Esp.Codigo, Esp.especialidad };

            return EjecutarSentencia("uspEspecialidadActualizar", nomParam, vlrParam);
        }

        public DataTable EspecialidadConsultar(Especialidad Esp)
        { 
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { Esp.Codigo };

            return getDataTable("uspEspecialidadConsultar", nomParam, vlrParam);
        }


        public string EspecialidaRetirar(Especialidad Esp)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { Esp.Codigo };

            DataTable dt = getDataTable("uspEspecialidadRetirar", nomParam, vlrParam);
            string Msg = "3";
            if(dt.Rows.Count > 0)
                Msg = dt.Rows[0]["MSG"].ToString();

            return Msg;
        }

        #region Metodos Privados

        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {            
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion

    }
}
