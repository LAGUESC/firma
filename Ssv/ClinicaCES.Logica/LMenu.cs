﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LMenu
    {
        public DataTable AplicacionListar()
        {
            return getDataTable("uspAplicacionListar");
        }

        public bool MenuEstado(MMenu men)
        {
            string[] nomParam = 
            {
                "@Aplicacion",
	            "@Codigo",
                "@Estado"
            };

            object[] vlrParam = 
            {
                men.Aplicacion,
                men.Codigo,
                men.Estado
            };

            return EjecutarSentencia("uspMenucambiarEstado", nomParam, vlrParam);
        }

        public bool MenuRetirar(string xml,string Usr)
        {
            string[] nomParam = 
            {
                "@xmlMenu" ,
	            "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspMenuRetirarXml", nomParam, vlrParam);
        }

        public bool MenuActualizar(MMenu men)
        { 
            string[] nomParam = 
            {
                "@Aplicacion",
	            "@Codigo",
                "@Menu",
	            "@Usr"
            };

            object[] vlrParam = 
            {
                men.Aplicacion,
                men.Codigo,
                men.Menu,
                men.Usr
            };

            return EjecutarSentencia("uspMenuActualizar", nomParam, vlrParam);
        }

        public DataTable MenuConsultar(MMenu men)
        { 
            string[] nomParam = 
            {
                "@Aplicacion",
	            "@Codigo"
            };

            object[] vlrParam = 
            {
                men.Aplicacion,
                men.Codigo
            };

            return getDataTable("uspMMenuConsultar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        public DataTable PermisosFormularios(string Usuario,int Id_Padre)
        {
            string[] nomParam =
            {
                "@USUARIO",
                "@ID_PADRE"

            };

            object[] vlrParam =
            {
                Usuario,
                Id_Padre
                
            };

            return getDataTable("uspGeneraMenu", nomParam, vlrParam);
        }


        #endregion

    }
}
