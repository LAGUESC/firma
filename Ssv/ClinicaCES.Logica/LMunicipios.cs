﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LMunicipios
    {
        public DataTable MunConsultar(Municipios mun)
        {
            string[] nomParam = 
            {
                "@Pais",
                "@Departamento",
	            "@Codigo"
            };

            object[] vlrParam = 
            {
                mun.Pais,
                mun.Depar,
                mun.Codigo
            };

            return getDataTable("uspMuniConsultar", nomParam, vlrParam);
        }
        public bool MunRetirar(string xml, string Usr)
        {
            string[] nomParam = 
            {
                "@xmlMunicipio" ,
	            "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspMunicipioRetirarXml", nomParam, vlrParam);
        }
        public DataTable PaisListar()
        {
            return getDataTable("uspPaisListar");
        }
        public bool MunEstado(Municipios mun)
        {
            string[] nomParam = 
            {
                "@Pais",
                "@Depar",
	            "@Codigo",
                "@Estado"
            };

            object[] vlrParam = 
            {
                mun.Pais,
                mun.Depar,
                mun.Codigo,
                mun.Estado
            };

            return EjecutarSentencia("uspMunicipiocambiarEstado", nomParam, vlrParam);
        }
        public bool MunicipioActualizar(Municipios mun)
        {
            string[] nomParam = 
            {
                "@Pais",
                "@Dep",
	            "@Codigo",
                "@Municipio",
	            "@Usr"
            };

            object[] vlrParam = 
            {
                mun.Pais,
                mun.Depar,
                mun.Codigo,
                mun.Municipio,
                mun.Usr
            };

            return EjecutarSentencia("uspMunicipioActualizar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }
        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }
        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
