﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LPaises
    {
        public bool PaisGuardar(Paises pais)
        {
            string[] nomParam = {"@Codigo" ,"@Nombre" ,"@Indicativo" ,"@Usuario"  };
            object[] vlrParam = { pais.Codigo, pais.Nombre, pais.Indicativo, pais.Usuario };

            return EjecutarSentencia("uspPaisesActualizar", nomParam, vlrParam);
        }
        
        public DataTable PaisesConsultar(Paises pais)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { pais.Codigo };

            return getDataTable("uspPaisConsultar", nomParam, vlrParam);
        }

        public bool PaisReactivar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Pais", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspPaisReactivar", nomParam, valParam);
        }

        public bool PaisRetirar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Pais", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspPaisRetirar", nomParam, valParam);
        }
               
        public DataTable PaisesConsultar()
        {
            return getDataTable("uspPaisConsultarCodigos");
        }

        public bool PaisesRetirar(string xmlPais, string Usuario)
        {
            string[] nomParam = { "@xmlPais", "@Usuario" };
            object[] valParam = { xmlPais, Usuario };

            return EjecutarSentencia("uspPaisRetirarXml", nomParam, valParam);
        }

        public bool PaisesEstado(Paises app)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                app.Codigo,
                app.Estado
            };

            return EjecutarSentencia("uspPaisesEstado", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
