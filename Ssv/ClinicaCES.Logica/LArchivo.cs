﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LArchivo
    {
        public DataSet NProcedimientosAmbEsp(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select ambito, especialidad, sum(prestacion) N_Procedimientos from (SELECT decode(D.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO,F .TIPOPROFENOMBRE ESPECIALIDAD, count(TRIM(H.PRE_PRE_DESCRIPCIO)) PRESTACION " +
                            "FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d,tabprdrel e,tab_tipoprofe f,ser_profesiona g, pre_prestacion H, ATE_PRESTACION I " +
                            "WHERE a.ORDNUMERO=d.ORDNUMERO and A.ORDNUMERO=E.ORDNUMERO and E.SER_PRO_RUT=G.SER_PRO_RUT and G.SER_PRO_TIPO=F.TIPOPROFECODIGO " +
                            "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO AND A.ORDNUMERO=B.ORDNUMERO AND E.PRE_PRE_CODIGO = H.PRE_PRE_CODIGO and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') " +
                            "and to_date('" + FechaFin + "','yyyy/mm/dd') AND A.ORDTIPO='P' AND E.PRE_PRE_CODIGO=I.ATE_PRE_CODIGO AND E.RPA_FOR_NUMERFORMU=I.ATE_PRE_NUMERFORMU " +
                            "AND E.RPA_FOR_TIPOFORMU = I.ATE_PRE_TIPOFORMU AND E.PAC_PAC_NUMERO = I.ATE_PRE_NUMERPACIE AND d.ORDCIRESTCOD IN ('V','X') " +
                            "And D.HORAFIN <> '00:00' and trim(C.PAC_PAC_RUT) not in ('10','12') and D.AMBITOCODIGO='01' " +
                            "and f.TIPOPROFECODIGO in ('CT','IF','IM','QF','TF','1S','1Y','01','02','03','04','05','06','07','08','09','10','11','12','13','15','17','18','19','20','21','22','23','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','44','45','46','47','48','49','50','51','53','54','55','56','57','58','60','62','66','67','84','85','86','87','88','89','90','91','92','93','94','95','96','97','98','99') " +
                            "GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,D.AMBITOCODIGO,F.TIPOPROFENOMBRE,a.ORDNUMERO, H.PRE_PRE_DESCRIPCIO  " +
                            "ORDER BY 1, F.TIPOPROFENOMBRE, 3) " +
                            "group by ambito, especialidad " +
                          "union all " +
                            "select ambito, especialidad, sum(prestacion) N_Procedimientos from (SELECT decode(D.AMBITOCODIGO,'01','AMBULATORIO','02','HOSPITALARIO','03','URGENCIAS') AMBITO,F .TIPOPROFENOMBRE ESPECIALIDAD, count(TRIM(H.PRE_PRE_DESCRIPCIO)) PRESTACION " +
                            "FROM TABORDENESSERV A,TABPRGQRF B,PAC_PACIENTE C,taborddetcir d,tabprdrel e,tab_tipoprofe f,ser_profesiona g, pre_prestacion H, ATE_PRESTACION I  " +
                            "WHERE a.ORDNUMERO=d.ORDNUMERO and A.ORDNUMERO=E.ORDNUMERO and E.SER_PRO_RUT=G.SER_PRO_RUT and G.SER_PRO_TIPO=F.TIPOPROFECODIGO " +
                            "AND A.PAC_PAC_NUMERO=C.PAC_PAC_NUMERO AND A.ORDNUMERO=B.ORDNUMERO AND E.PRE_PRE_CODIGO = H.PRE_PRE_CODIGO and trunc(B.FECQRF) between to_date('" + FechaIni + "','yyyy/mm/dd') " +
                            "and to_date('" + FechaFin + "','yyyy/mm/dd') AND A.ORDTIPO='P' AND E.PRE_PRE_CODIGO=I.ATE_PRE_CODIGO AND E.RPA_FOR_NUMERFORMU=I.ATE_PRE_NUMERFORMU " +
                            "AND E.RPA_FOR_TIPOFORMU = I.ATE_PRE_TIPOFORMU AND E.PAC_PAC_NUMERO = I.ATE_PRE_NUMERPACIE AND d.ORDCIRESTCOD IN ('V','X') " +
                            "And D.HORAFIN <> '00:00' and trim(C.PAC_PAC_RUT) not in ('10','12') and D.AMBITOCODIGO='02'  " +
                            "and f.TIPOPROFECODIGO in ('CT','IF','IM','QF','TF','1S','1Y','01','02','03','04','05','06','07','08','09','10','11','12','13','15','17','18','19','20','21','22','23','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','44','45','46','47','48','49','50','51','53','54','55','56','57','58','60','62','66','67','84','85','86','87','88','89','90','91','92','93','94','95','96','97','98','99') " +
                            "GROUP BY C.PAC_PAC_RUT,C.PAC_PAC_NUMERO ,A.ORDFECHA,B.FECQRF,D.AMBITOCODIGO,F.TIPOPROFENOMBRE,a.ORDNUMERO, H.PRE_PRE_DESCRIPCIO  " +
                            "ORDER BY 1, F.TIPOPROFENOMBRE, 3) " +
                            "group by ambito, especialidad " +
                           "order by ambito, especialidad ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }

        public DataSet NConsultasEspecialidad(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select F.TIPOPROFENOMBRE Especialidad,count( distinct e.PAC_PAC_RUT) N_Pacientes " +
                          "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e,tab_tipoprofe f ,tabmotivocons g,con_convenio h " +
                          "where  TRUNC (b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_TIPOFORMU=b.RPA_FOR_TIPOFORMU " +
                          "and a.PCA_AGE_NUMERFORMU=b.RPA_FOR_NUMERFORMU and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO " +
                          "and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and E.PAC_PAC_NUMERO not in ('1308', '5024') and c.SER_PRO_TIPO=f.TIPOPROFECODIGO and a.MTVCORRELATIVO=g.MTVCORRELATIVO " +
                          "and a.PCA_AGE_NUMERPACIE=g.PAC_PAC_NUMERO AND A.PCA_AGE_RECEPCIONADO='S' and h.CON_CON_CODIGO=b.CON_CON_CODIGO " +
                          "And c.SER_PRO_TIPO in ('CT','IF','IM','QF','TF','1S','1Y','01','02','03','04','05','06','07','08','09','10','11','12','13','15','17','18','19','20','21','22','23','25','26','27','28','29','30','31','32','33','34','35','36','37','38','39','40','44','45','46','47','48','49','50','51','53','54','55','56','57','58','60','62','66','67','84','85','86','87','88','89','90','91','92','93','94','95','96','97','98','99') " +
                          "group by F.TIPOPROFENOMBRE ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }

    }
}
