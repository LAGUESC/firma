﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LAsunto
    {
        public DataTable AsuntoListar()
        {
            return getDataTable("uspAsuntoListar");
        }

        public DataTable PrioridadListar()
        {
            return getDataTable("uspPrioridadListar");
        }

        public DataTable ClasificacionListar()
        {
            return getDataTable("uspClasificacionListar");
        }

        public DataTable EstadosListar()
        {
            return getDataTable("uspEstadosListar");
        }

        public DataTable EncargadoListar()
        {
            return getDataTable("uspEncargadoListar");
        }

        public bool AsuntoDetalleActualizar(Asunto asun)
        {
            string[] nomParam = 
            { 
            	"@idAsunto" 
	            ,"@idEstado" 
	            ,"@idUsuClie" 
	            ,"@Informacion" 
	            ,"@IdUsuario" 
	            ,"@HoraIni" 
	            ,"@HoraFin" 
	            ,"@Recibieron" 
	            ,"@FRecibieron" 
	            ,"@IdUsuarioRecibio"
                ,"@IdResponsable"
                ,"@xmlArchivos"
                ,"@Asignado"
            };

            object[] vlrParam = 
            { 
            	asun.IdAsunto ,
	            asun.idEstado,
	            asun.idUsuClie,
	            asun.Informacion ,
	            asun.idatendio,
	            asun.HoraIni,
	            asun.HoraFin,
	            asun.Recibieron ,
	            asun.FRecibieron ,
	            asun.IdUsuarioRecibio, 
                asun.IdResponsable,
                asun.xmlArchivos,
                asun.Asignado

            };

            return EjecutarSentencia("uspAsuntoDetalleActualizar", nomParam, vlrParam);
        }

        public DataTable FestivosConsultar(int Mes, int Year)
        {

            string[] NombreParametros = new string[] { 
                "@Mes"
                ,"@Year"
                };

            object[] ValoresParametros = new object[] { 
                    Mes
                    ,Year
                };

            return getDataTable("uspFestivosConsultar", NombreParametros, ValoresParametros);
          
        }

        public bool FestivosActualizar(string Xml)
        {
            string[] NombreParametros = new string[] { 
                    "@strItems"
                };

            object[] ValoresParametros = new object[] { 
                    Xml
                };

            return EjecutarSentencia("uspFestivosActualizar", NombreParametros, ValoresParametros);
            //bool rp = objAccesoDatos.EjecutarSentencia("uspNOMCargarDatosNomina");
        }

        public DataSet AsuntoConsultar(string IDasu)
        {
            string[] nomParam = 
            {
                "@idAsunto"
            };

            object[] vlrParam = 
            {
                IDasu
            };

            return getDataSet("uspAsuntoConsultar", nomParam, vlrParam);
        }

        public DataSet AsuntoConsultarExcel(string IDasu)
        {
            string[] nomParam = 
            {
                "@idAsunto"
            };

            object[] vlrParam = 
            {
                IDasu
            };

            return getDataSet("uspAsuntoConsultarExcel", nomParam, vlrParam);
        }

        public DataTable AsuntoConsultarAdjuntos(string Caso, string Cons)
        {
            string[] nomParam = 
            {
                "@Caso",
	            "@Consencutivo"
            };

            object[] vlrParam = 
            {
                Caso,
                Cons
            };

            return getDataTable("uspAsuntoAdjuntosConsultar", nomParam, vlrParam);
        }

        public DataSet ConsultarPendientes(string IDasu, string idEstado)
        {
            string[] nomParam = 
            {
                "@idAsunto",
                "@idEstado"
            };

            object[] vlrParam = 
            {
                IDasu,
                idEstado
            };

            return getDataSet("uspAsuntosPendientes", nomParam, vlrParam);
        }

        public DataTable MesConsultar()
        {
            return getDataTable("uspMesConsultar");
        }

        public DataTable AnioConsultar()
        {
            return getDataTable("uspAnioDisponibleListar");
        }
        

        public DataTable CiaConsultar()
        {
            return getDataTable("uspCompaniaListar");
        }

        public DataSet InformeMensual(string TipoHonorario, string FechaIni, string FechaFin, string tipoprofes)
        {
            string[] nomParam = 
            {
                "In_TipoHonorario",
                "In_FechaIni",
                "In_FechaFin",
                "Regrepcursor"
            };

            object[] vlrParam = 
            {
                TipoHonorario,
                FechaIni,
                FechaFin,
                tipoprofes
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT nombremes (EXTRACT (MONTH FROM a.fecqrf)) AS mes, a.ordnumero AS norden, TO_CHAR(a.fecqrf,'YYYY/MM/DD')AS feccirugia," +
                "d.horaini AS horacirugia, e.pac_pac_rut AS idpaciente, RTRIM (e.pac_pac_apellpater)|| ' ' || RTRIM (e.pac_pac_apellmater)|| ' '|| RTRIM (e.pac_pac_nombre) AS paciente," +
                "f.ser_pro_rut AS idcirugia,RTRIM (f.ser_pro_apellpater)|| ' ' || RTRIM (f.ser_pro_apellmater) || ' ' || RTRIM (f.ser_pro_nombres) AS profesional," +
                "c.pre_pre_codigo AS codpresta,h.pre_pre_descripcio AS nprestacion,c.porhonpor AS phonorario, c.ate_pre_montopagar AS monto," +
                "c.soccod AS sociedad, d.horaini AS horaini,SUBSTR (d.horafin, 1, 2) AS horafin, d.horafin AS horacompfin, a.ser_obj_codigo AS servicio " +
                "FROM tabprgqrf a,tabprdrel b,tabprdreldet c,taborddetcir d,pac_paciente e,ser_profesiona f,pre_prestacion h,ate_prestacion k " +
                "WHERE a.ordnumero = b.ordnumero AND a.ordnumero = c.ordnumero AND b.ordnumero = k.ordnumero AND b.pre_pre_codigo = k.ate_pre_codigo "+
                "AND c.cnpqrgcod = '" + TipoHonorario + "' " +
                "and trim(e.PAC_PAC_RUT) not in ( '10','12') " +
                "AND b.pre_pre_codigo = c.pre_pre_codigo AND a.ordnumero = d.ordnumero AND a.pac_pac_numero = e.pac_pac_numero "+
                "AND " + tipoprofes +" = f.ser_pro_rut AND h.pre_pre_codigo = c.pre_pre_codigo AND TRUNC(a.fecqrf) BETWEEN TO_DATE ('" + FechaIni + "', 'yyyy/mm/dd') "+
                "AND TO_DATE ('" + FechaFin + "', 'yyyy/mm/dd') AND (d.horaini BETWEEN ('19:00') AND ('23:59') OR d.horaini BETWEEN ('00:00') AND ('05:59')) " +
                "AND (d.horafin BETWEEN ('19:00') AND ('23:59') OR d.horafin BETWEEN ('00:00') AND ('05:59'))  ORDER BY a.FECQRF,a.HORQRF ASC";
            
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }


        public DataSet InformeInsumos(string TServicio,string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "TServicio",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                TServicio,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;

            if (TServicio == "IN")
            {
                sql = "select E.GRPPRDNOM TIPO,a.ATE_INS_CODIGO CODIGO,c.FLD_PRODUCTOGLOSA DESCRIPCION,d.Con_Con_codigo COD_CONVENIO,d.CON_CON_DESCRIPCIO CONVENIO,D.CON_EMP_RUT RUT ,sum(a.ATE_INS_CANTIDAD) CANTIDAD,sum(a.ATE_INS_MONTOTARIF) TARIFA,sum(A.ATE_INS_MONTOPAGAR) COPAGO,(sum(a.ATE_INS_MONTOTARIF)-sum(A.ATE_INS_MONTOPAGAR))TOTAL_EMPRESA " +
                         "from ate_insumos a,aba_producto c,con_convenio d,tabgrpprd e " +
                         "where a.ATE_INS_CODIGO=c.FLD_PRODUCTOCODIGO and trunc(a.ATE_INS_FECHAENTRE) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                         "and a.CON_CON_CODIGO=d.CON_CON_CODIGO and A.ATE_INS_VIGENCIA <>'N' and c.GRPPRDCOD=E.GRPPRDCOD and a.ate_ins_numerpacie not in ( '1308','5024') " +
                         "group by E.GRPPRDNOM,a.ATE_INS_CODIGO,c.FLD_PRODUCTOGLOSA,d.Con_Con_codigo,d.CON_CON_DESCRIPCIO,D.CON_EMP_RUT " +
                         "order by E.GRPPRDNOM,c.FLD_PRODUCTOGLOSA,d.CON_CON_DESCRIPCIO";
            }
            else
            {
                if (TServicio == "PR")
                {
                    sql = "select  D.PRE_TIP_DESCRIPCIO TIPO,a.ATE_PRE_CODIGO CODIGO,c.PRE_PRE_DESCRIPCIO DESCRIPCION,b.Con_Con_codigo COD_CONVENIO,b.CON_CON_DESCRIPCIO CONVENIO,B.CON_EMP_RUT RUT,sum(a.ATE_PRE_CANTIDAD) CANTIDAD,sum(a.ate_pre_montotarif) TARIFA,sum(A.ATE_PRE_MONTOPAGAR ) COPAGO,(sum(a.ate_pre_montotarif)-sum(A.ATE_PRE_MONTOPAGAR)) TOTAL_EMPRESA " +
                              "from ate_prestacion a, con_convenio b,pre_prestacion c,rpa_formulario e,pre_tipo d " +
                              "where  trunc(a.ate_pre_fechaentre) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                              "and a.CON_CON_CODIGO=b.CON_CON_CODIGO and a.ATE_PRE_CODIGO=c.PRE_PRE_CODIGO and A.ATE_PRE_VIGENCIA<>'N' " +
                              "and A.ATE_PRE_NUMERFORMU=E.RPA_FOR_NUMERFORMU and A.ATE_PRE_TIPOFORMU=E.RPA_FOR_TIPOFORMU and a.ate_pre_numerpacie not in ( '1308','5024') " +
                              "and E.RPA_FOR_FECHATENCION <> to_date('1900/01/01','yyyy/mm/dd') and C.PRE_PRE_TIPO=D.PRE_TIP_TIPO " +
                              "group by D.PRE_TIP_DESCRIPCIO,B.CON_EMP_RUT,a.ATE_PRE_CODIGO,b.Con_Con_codigo,b.CON_CON_DESCRIPCIO,c.PRE_PRE_DESCRIPCIO " +
                              "order by D.PRE_TIP_DESCRIPCIO,c.PRE_PRE_DESCRIPCIO,B.CON_CON_CODIGO";
                }
                else
                {
                    if (TServicio == "IP")
                    {
                        sql = "(select E.GRPPRDNOM TIPO,a.ATE_INS_CODIGO CODIGO,c.FLD_PRODUCTOGLOSA DESCRIPCION,d.Con_Con_codigo COD_CONVENIO,d.CON_CON_DESCRIPCIO CONVENIO,D.CON_EMP_RUT RUT ,sum(a.ATE_INS_CANTIDAD) CANTIDAD,sum(a.ATE_INS_MONTOTARIF) TARIFA,sum(A.ATE_INS_MONTOPAGAR) COPAGO,(sum(a.ATE_INS_MONTOTARIF)-sum(A.ATE_INS_MONTOPAGAR))TOTAL_EMPRESA " +
                                "from ate_insumos a,aba_producto c,con_convenio d,tabgrpprd e " +
                                "where a.ATE_INS_CODIGO=c.FLD_PRODUCTOCODIGO and trunc(a.ATE_INS_FECHAENTRE) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                "and a.CON_CON_CODIGO=d.CON_CON_CODIGO and A.ATE_INS_VIGENCIA <>'N' and c.GRPPRDCOD=E.GRPPRDCOD and a.ate_ins_numerpacie not in ( '1308','5024') " +
                                "group by E.GRPPRDNOM,a.ATE_INS_CODIGO,c.FLD_PRODUCTOGLOSA,d.Con_Con_codigo,d.CON_CON_DESCRIPCIO,D.CON_EMP_RUT " +
                                "union all "+
                                "select  D.PRE_TIP_DESCRIPCIO TIPO,a.ATE_PRE_CODIGO CODIGO,c.PRE_PRE_DESCRIPCIO DESCRIPCION,b.Con_Con_codigo COD_CONVENIO,b.CON_CON_DESCRIPCIO CONVENIO,B.CON_EMP_RUT RUT,sum(a.ATE_PRE_CANTIDAD) CANTIDAD,sum(a.ate_pre_montotarif) TARIFA,sum(A.ATE_PRE_MONTOPAGAR ) COPAGO,(sum(a.ate_pre_montotarif)-sum(A.ATE_PRE_MONTOPAGAR)) TOTAL_EMPRESA " +
                                "from ate_prestacion a, con_convenio b,pre_prestacion c,rpa_formulario e,pre_tipo d " +
                                "where  trunc(a.ate_pre_fechaentre) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                "and a.CON_CON_CODIGO=b.CON_CON_CODIGO and a.ATE_PRE_CODIGO=c.PRE_PRE_CODIGO and A.ATE_PRE_VIGENCIA<>'N' " +
                                "and A.ATE_PRE_NUMERFORMU=E.RPA_FOR_NUMERFORMU and A.ATE_PRE_TIPOFORMU=E.RPA_FOR_TIPOFORMU and a.ate_pre_numerpacie not in ( '1308','5024') " +
                                "and E.RPA_FOR_FECHATENCION <> to_date('1900/01/01','yyyy/mm/dd') and C.PRE_PRE_TIPO=D.PRE_TIP_TIPO " +
                                "group by D.PRE_TIP_DESCRIPCIO,B.CON_EMP_RUT,a.ATE_PRE_CODIGO,b.Con_Con_codigo,b.CON_CON_DESCRIPCIO,c.PRE_PRE_DESCRIPCIO) ORDER BY 1,4";
                    }
                }
            }
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }


        public DataSet InformeInsPreFacturadas(string TServicio, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "TServicio",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                TServicio,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;

            if (TServicio == "INF")
            {
                sql = "select '' TIPO, ' ' CODIGO,'Medicamentos e insumos' DESCRIPCION,fct.Con_Con_codigo COD_CONVENIO, GlbGetNomCnv(fct.Con_Con_Codigo) CONVENIO,CON.CON_EMP_RUT RUT, " +
                       "decode (c.rpa_For_Urgencia ,'H','Hospitalizacion','S','Urgencias','C.Externa') AMBITO, 0 CANTIDAD,  0 TARIFA,  0 COPAGO, sum(ate.ATE_INS_CANTIDAD * ate.ATE_INS_valorunid) TOTAL_EMPRESA " +        
                       "from tabfctemp fct,tabfctgrp grp,ate_insumos ate,rpa_Formulario c,CON_CONVENIO CON "+
                       "where trunc(fct.fac_fac_fechafacturacion) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                       "and fct.FCTID  =  grp.FCTIDD and grp.RPA_FOR_TIPOFORMU  = ate.ATE_INS_TIPOFORMU and grp.RPA_FOR_NUMERFORMU = ate.ATE_INS_NUMERFORMU "+
                       "And c.RPA_FOR_TIPOFORMU = grp.Rpa_For_Tipoformu and c.RPA_FOR_NUMERFORMU = grp.Rpa_For_numerformu And c.Pac_Pac_Numero = grp.PAC_PAC_NUMERO "+
                       "and ate.ATE_INS_NUMERPACIE = c.PAC_PAC_NUMERO AND FCT.CON_CON_CODIGO=CON.CON_CON_CODIGO and ate.ate_ins_numerpacie not in ('1308','5024')"+
                       "group by fct.Con_Con_codigo , GlbGetNomCnv(fct.Con_Con_Codigo),CON.CON_EMP_RUT, c.rpa_For_Urgencia order by 1,4";
            }
            else
            {
                if (TServicio == "PRF")
                {
                    sql = "Select TIP.PRE_TIP_DESCRIPCIO TIPO, ate.ATE_PRE_CODIGO CODIGO,pre.PRE_PRE_DESCRIPCIO DESCRIPCION,fct.Con_Con_codigo COD_CONVENIO, GlbGetNomCnv(fct.Con_Con_Codigo) CONVENIO, "+
                            "CON.CON_EMP_RUT RUT,decode(rpa.rpa_For_Urgencia ,'H','Hospitalizacion','S','Urgencias','C.Externa') AMBITO,sum(ate.ATE_PRE_CANTIDAD)CANTIDAD, "+
                            "sum(ate.ATE_PRE_MONTOTARIF) TARIFA,sum(ate.ATE_PRE_MONTOPAGAR) COPAGO,(sum(ate.ATE_PRE_MONTOTARIF)-sum(ate.ATE_PRE_MONTOPAGAR)) TOTAL_EMPRESA "+   
                            "from tabfctemp fct, tabfctgrp grp, rpa_Formulario rpa, ate_prestacion ate, pre_prestacion pre,PRE_TIPO tip,CON_CONVENIO CON "+
                            "where FCT.ESTADO <> 'A' And fct.fac_Fac_Factura = grp.Fac_Fac_Factura and rpa.RPA_FOR_NUMERFORMU = ate.ATE_PRE_NUMERFORMU "+
                            "and ate.ATE_PRE_CODIGO = pre.PRE_PRE_CODIGO and PRE.PRE_PRE_TIPO=TIP.PRE_TIP_TIPO and grp.fctidd = fct.fctId and ate.ate_pre_numerpacie not in ('1308','5024') "+
                            "And grp.Fac_fac_Factura = fct.Fac_fac_Factura And rpa.RPA_FOR_TIPOFORMU = grp.Rpa_For_Tipoformu and rpa.RPA_FOR_NUMERFORMU = grp.Rpa_For_numerformu "+
                            "And rpa.Pac_Pac_Numero = grp.PAC_PAC_NUMERO AND FCT.CON_CON_CODIGO =CON.CON_CON_CODIGO and ate.ATE_PRE_NUMERpacie = rpa.PAC_PAC_NUMERO "+
                            "And trunc(FCT.FAC_FAC_FECHAFACTURACION) between To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') " +
                            "group by TIP.PRE_TIP_DESCRIPCIO,ate.ATE_PRE_CODIGO,pre.PRE_PRE_DESCRIPCIO,fct.Con_Con_codigo , GlbGetNomCnv(fct.Con_Con_Codigo), CON.CON_EMP_RUT,rpa.rpa_For_Urgencia order by 1,4";
                }
                else
                {
                    if (TServicio == "IPF")
                    {
                        sql = "select '' TIPO, ' ' CODIGO,'Medicamentos e insumos' DESCRIPCION,fct.Con_Con_codigo COD_CONVENIO, GlbGetNomCnv(fct.Con_Con_Codigo) CONVENIO,CON.CON_EMP_RUT RUT, " +
                               "decode (c.rpa_For_Urgencia ,'H','Hospitalizacion','S','Urgencias','C.Externa') AMBITO, 0 CANTIDAD,  0 TARIFA,  0 COPAGO, sum(ate.ATE_INS_CANTIDAD * ate.ATE_INS_valorunid) TOTAL_EMPRESA " +        
                               "from tabfctemp fct,tabfctgrp grp,ate_insumos ate,rpa_Formulario c,CON_CONVENIO CON "+
                               "where trunc(fct.fac_fac_fechafacturacion) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                               "and fct.FCTID  =  grp.FCTIDD and grp.RPA_FOR_TIPOFORMU  = ate.ATE_INS_TIPOFORMU and grp.RPA_FOR_NUMERFORMU = ate.ATE_INS_NUMERFORMU "+
                               "And c.RPA_FOR_TIPOFORMU = grp.Rpa_For_Tipoformu and c.RPA_FOR_NUMERFORMU = grp.Rpa_For_numerformu And c.Pac_Pac_Numero = grp.PAC_PAC_NUMERO "+
                               "and ate.ATE_INS_NUMERPACIE = c.PAC_PAC_NUMERO AND FCT.CON_CON_CODIGO=CON.CON_CON_CODIGO and ate.ate_ins_numerpacie not in ('1308', '5024') "+
                               "group by fct.Con_Con_codigo , GlbGetNomCnv(fct.Con_Con_Codigo),CON.CON_EMP_RUT, c.rpa_For_Urgencia "+
                                "UNION ALL " +
                                "Select TIP.PRE_TIP_DESCRIPCIO TIPO, ate.ATE_PRE_CODIGO CODIGO,pre.PRE_PRE_DESCRIPCIO DESCRIPCION,fct.Con_Con_codigo COD_CONVENIO, GlbGetNomCnv(fct.Con_Con_Codigo) CONVENIO, " +
                                "CON.CON_EMP_RUT RUT,decode(rpa.rpa_For_Urgencia ,'H','Hospitalizacion','S','Urgencias','C.Externa') AMBITO,sum(ate.ATE_PRE_CANTIDAD)CANTIDAD, " +
                                "sum(ate.ATE_PRE_MONTOTARIF) TARIFA,sum(ate.ATE_PRE_MONTOPAGAR) COPAGO,(sum(ate.ATE_PRE_MONTOTARIF)-sum(ate.ATE_PRE_MONTOPAGAR)) TOTAL_EMPRESA " +
                                "from tabfctemp fct, tabfctgrp grp, rpa_Formulario rpa, ate_prestacion ate, pre_prestacion pre,PRE_TIPO tip,CON_CONVENIO CON " +
                                "where FCT.ESTADO <> 'A' And fct.fac_Fac_Factura = grp.Fac_Fac_Factura and rpa.RPA_FOR_NUMERFORMU = ate.ATE_PRE_NUMERFORMU " +
                                "and ate.ATE_PRE_CODIGO = pre.PRE_PRE_CODIGO and PRE.PRE_PRE_TIPO=TIP.PRE_TIP_TIPO and grp.fctidd = fct.fctId and ate.ate_pre_numerpacie not in ('1308', '5024') " +
                                "And grp.Fac_fac_Factura = fct.Fac_fac_Factura And rpa.RPA_FOR_TIPOFORMU = grp.Rpa_For_Tipoformu and rpa.RPA_FOR_NUMERFORMU = grp.Rpa_For_numerformu " +
                                "And rpa.Pac_Pac_Numero = grp.PAC_PAC_NUMERO AND FCT.CON_CON_CODIGO =CON.CON_CON_CODIGO and ate.ATE_PRE_NUMERpacie = rpa.PAC_PAC_NUMERO " +
                                "And trunc(FCT.FAC_FAC_FECHAFACTURACION) between To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') " +
                                "group by TIP.PRE_TIP_DESCRIPCIO,ate.ATE_PRE_CODIGO,pre.PRE_PRE_DESCRIPCIO,fct.Con_Con_codigo , GlbGetNomCnv(fct.Con_Con_Codigo), CON.CON_EMP_RUT,rpa.rpa_For_Urgencia order by 1,4";
                    }
                }
            }
            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }

        
        public DataSet AsuntoValidar(string IDasu, string tipo)
        {
            string sp = string.Empty;

            if (tipo == "C")
                sp = "uspAsuntoValidar";
            else if (tipo == "R")
                sp = "uspRequerimientoValidar";

            string[] nomParam = 
            {
                "@idasunto"
            };

            object[] vlrParam = 
            {
                IDasu
            };

            return getDataSet(sp, nomParam, vlrParam);

            //DataTable dt = getDataTable(sp, nomParam, vlrParam);

            //if (dt.Rows.Count > 0)
            //    return true;
            //else
            //    return false;
        }


        public DataSet HonorarioQX(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select   NOMBREMES( EXTRACT (MONTH FROM a.SER_PRO_FECHAINI )) mes,trim(b.SER_PRO_RUT) rut ,(rtrim (b.SER_PRO_APELLPATER) ||' '||  rtrim(b.SER_PRO_APELLMATER) ||' '|| rtrim(b.SER_PRO_NOMBRES)) PROFESIONAL, " +
                         "sum(a.VALHONMED) VALORH,DECODE(TRIM(a.soccod),'CES','CES','TERCERO') sociedad " +
                         "from tabprdreldetpro a,ser_profesiona b,taborddetcir c, ate_prestacion d " +
                         "where A.ORDNUMERO=C.ORDNUMERO and A.SER_PRO_RUT=B.SER_PRO_RUT and trunc (a.SER_PRO_FECHAINI) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                         "and C.HORAFIN <> '00:00' and A.ORDNUMERO = D.ORDNUMERO and A.PRE_PRE_CODIGO = D.ATE_PRE_CODIGO and d.ate_pre_numerpacie not in ('1308','5024') and d.ate_pre_vigencia<>'N' and D.ORDNUMERO<>' ' " +
                         "group by  trim(b.SER_PRO_RUT),DECODE(TRIM(a.soccod),'CES','CES','TERCERO'),b.SER_PRO_NOMBRES,b.SER_PRO_APELLPATER,b.SER_PRO_APELLMATER,EXTRACT (MONTH FROM a.SER_PRO_FECHAINI) " +
                         "UNION ALL " +

                         "(select   NOMBREMES( EXTRACT (MONTH FROM V.SER_PRO_FECHAINI )) mes ,trim(X.SER_PRO_RUT) rut,(rtrim (X.SER_PRO_APELLPATER) ||' '||  rtrim(X.SER_PRO_APELLMATER) ||' '|| rtrim(X.SER_PRO_NOMBRES)) PROFESIONAL, " +
                         "count(distinct (v.ORDNUMERO)),'TOTAL CX' CES " +
                         "from tabprdreldetpro v,ser_profesiona x,taborddetcir y, ate_prestacion d " +
                         "where v.ORDNUMERO=y.ORDNUMERO and v.SER_PRO_RUT=x.SER_PRO_RUT and trunc (v.SER_PRO_FECHAINI) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                         "and y.HORAFIN <> '00:00' and v.pac_pac_numero not in ('1308','5024') and v.PRE_PRE_CODIGO = D.ATE_PRE_CODIGO and d.ate_pre_vigencia<>'N' and D.ORDNUMERO<>' ' " +
                         "group by  TRIM(X.SER_PRO_RUT),EXTRACT (MONTH FROM V.SER_PRO_FECHAINI),X.SER_PRO_NOMBRES,X.SER_PRO_APELLPATER,X.SER_PRO_APELLMATER) " +
                         "ORDER BY  MES,RUT,PROFESIONAL";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }


        public DataSet HonorarioNOQX(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select  NOMBREMES( EXTRACT (MONTH FROM a.ATE_PRE_FECHAENTRE)) mes, trim(D.SER_PRO_RUT) RUT ,(rtrim (d.SER_PRO_APELLPATER) ||' '||  rtrim(d.SER_PRO_APELLMATER) ||' '|| rtrim(d.SER_PRO_NOMBRES)) PROFESIONAL " +
                         ",SUM(a.VALHONMED) VALORH ,DECODE(TRIM(a.soccod),'CES','CES','TERCERO') SOCIEDAD " +
                         "from ate_prestacion a,con_convenio b,tabsoccop c,ser_profesiona d " +
                         "where c.SOCCOD=a.SOCCOD and b.CON_CON_CODIGO=a.CON_CON_CODIGO and a.ATE_PRE_PROFERESPO = d.SER_PRO_RUT and a.ate_pre_numerpacie not in ('1308','5024') " +
                         "and trunc (a.ATE_PRE_FECHAENTRE) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ate_pre_vigencia<>'N' and A.ORDNUMERO=' ' " +
                         "group by  NOMBREMES( EXTRACT (MONTH FROM a.ATE_PRE_FECHAENTRE)), trim(D.SER_PRO_RUT),rtrim (d.SER_PRO_APELLPATER) , rtrim(d.SER_PRO_APELLMATER), rtrim(d.SER_PRO_NOMBRES) , DECODE(TRIM(a.soccod),'CES','CES','TERCERO') " +
                         "UNION ALL " +

                         "select  NOMBREMES( EXTRACT (MONTH FROM a.ATE_PRE_FECHAENTRE)) mes, trim(D.SER_PRO_RUT) RUT ,(rtrim (d.SER_PRO_APELLPATER) ||' '||  rtrim(d.SER_PRO_APELLMATER) ||' '|| rtrim(d.SER_PRO_NOMBRES)) PROFESIONAL " +
                         ",count(a.VALHONMED),'TOTAL_NCX' " +
                         "from ate_prestacion a,con_convenio b,tabsoccop c,ser_profesiona d " +
                         "where c.SOCCOD=a.SOCCOD and b.CON_CON_CODIGO=a.CON_CON_CODIGO and a.ATE_PRE_PROFERESPO = d.SER_PRO_RUT and a.ate_pre_numerpacie not in ('1308','5024') " +
                         "and trunc (a.ATE_PRE_FECHAENTRE) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ate_pre_vigencia<>'N' and A.ORDNUMERO=' ' " +
                         "group by  NOMBREMES( EXTRACT (MONTH FROM a.ATE_PRE_FECHAENTRE)), trim(D.SER_PRO_RUT),rtrim (d.SER_PRO_APELLPATER) , rtrim(d.SER_PRO_APELLMATER), rtrim(d.SER_PRO_NOMBRES) ,a.SOCCOD " +
                         "ORDER BY  MES,RUT,PROFESIONAL";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }

        
        public DataTable ListaEspecialidades()
        {
            string sql = "select A.TIPOPROFECODIGO, trim(A.TIPOPROFENOMBRE) TIPOPROFENOMBRE from tab_tipoprofe a order by A.TIPOPROFENOMBRE";

            return getDataTable(sql);
        }


        public DataTable Especialidades()
        {
            string sql = "select distinct (to_number( A.SER_ESP_CODIGO)) codigo,  trim(A.SER_ESP_DESCRIPCIO) descripcion from Ser_Especiali a order by 2";

            return getDataTable(sql);
        }


        public DataTable ListaCausasCancelacionCX()
        {
            string sql = "select B.ORDCAUCANCOD,trim(B.ORDCAUCANNOM) ORDCAUCANNOM  from tabordcircaucan b order by B.ORDCAUCANNOM";

            return getDataTable(sql);
        }


        public DataTable ListaServiciosHospitalarios()
        {
            string sql = "select A.SER_SER_CODIGO, A.SER_SER_DESCRIPCIO from ser_servicios a where A.SER_SER_AMBITO='02' and A.SER_SER_VIGENCIA<>'N' AND A.SER_SER_CODIGO<>'AMB     ' order by A.SER_SER_CODIGO";

            return getDataTable(sql);
        }


        public DataTable ListaServiciosHospSINURGYREC()
        {
            string sql = "select A.SER_SER_CODIGO, A.SER_SER_DESCRIPCIO from ser_servicios a where A.SER_SER_AMBITO='02' and A.SER_SER_VIGENCIA<>'N' AND A.SER_SER_CODIGO not in('AMB     ','001     ','004     ') order by A.SER_SER_CODIGO";

            return getDataTable(sql);
        }

        public DataTable ListaGruposEtareos()
        {
            string sql = "select (A.GRUPETACODIGO||'-'||A.RANGOEDADLI||'-'||A.RANGOEDADLS) CODIGO ,A.GRUPETANOMBRE  from TAB_GRUPOETAREO a where A.CODIGOTABLAGRUPETA='99' order by A.GRUPETACODIGO";

            return getDataTable(sql);
        }


        public DataTable ListaProfesionales()
        {
            string sql = "select H.SER_PRO_RUT,(trim(H.SER_PRO_NOMBRES) || ' ' || trim(H.SER_PRO_APELLPATER) || ' '  ||trim( H.SER_PRO_APELLMATER) ) profesional from ser_profesiona h where H.SER_PRO_ESTADO='A' and H.SER_PRO_TIPO<>'12' order by 2 ";

            return getDataTable(sql);
        }


        public DataTable ListaAlmacenes()
        {
            string sql = "SELECT A.OPA_BODEGACODIGO, A.OPA_BODEGAGLOSA FROM OPA_BODEGA A WHERE A.OPA_VIGENTE='1'  AND A.OPA_BODEGACODIGO <>'          ' AND A.CODIGOCENTROATEN='CES' ORDER BY 1 ";

            return getDataTable(sql);
        }

        
        public DataTable ListaPrioridades()
        {
            string sql = "select triagecod, triagenom from TabTriageCal c ORDER BY 2 ";

            return getDataTable(sql);
        }


        public DataTable ListaCausasExternas()
        {
            string sql = "select A.TAB_CONT_CODIGO codigo, A.TAB_CONT_DESCRIPCION descripcion from tab_contigencia a order by 2 ";

            return getDataTable(sql);
        }

        public DataTable ListaConvenios()
        {
            string sql = "SELECT A.CON_CON_CODIGO CODIGO, TRIM(A.CON_CON_DESCRIPCIO) CONVENIO FROM CON_CONVENIO A WHERE A.CON_CON_VIGENCIA<>'N' order by 2 ";

            return getDataTable(sql);
        }


        public DataTable BuscarPAciente(string Historia)
        {
            string[] nomParam = 
            {
                "In_Historia"
            };

            object[] vlrParam = 
            {
                Historia
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select nomcompletopaciente(A.PAC_PAC_NUMERO) paciente from pac_paciente a where trim(A.PAC_PAC_RUT) ='" + Historia + "'";

            return new DA.DA("Oracle").getDataTable(sql);
            //return getDataSet("HonorarioNoc_PKG.HonorarioNoc", nomParam, vlrParam);
        }


        public DataTable EliminarCamarotes()
        {
            string sql = "update atc_estadia a set a.ATCESTADO='X' where a.ATCESTADO='H' and trunc(a.ATC_EST_FECEGRESO) <> to_date('1900/01/01','yyyy/mm/dd') " +
                         "and A.SER_OBJ_CODIGO in (select C.SER_OBJ_CODIGO from ser_objetos c where C.SER_OBJ_ESTADO='02') and A.ATC_EST_NUMERO in (select B.ATC_EST_NUMERO from  tabaltord b)  ";

            return getDataTable(sql);
        }


        public DataTable DesbloquearDispensacion()
        {
            string sql = "delete from tabblqdisp ";

            return getDataTable(sql);
        }


        public DataTable DesbloquearCuenta()
        {
            string sql = "delete from tabbloqcuenta ";

            return getDataTable(sql);
        }


        public DataTable DesbloquearUsuario(string Usuario)
        {
            string sql = "ALTER USER " + Usuario + " ACCOUNT UNLOCK ";

            return getDataTable(sql);
        }


        public DataTable ActualizarTriage(string NumUrgencia, string TriageCod)
        {
            string sql = "update tabtriage a set A.TRIAGECOD = '" + TriageCod + "' where A.RPA_FOR_TIPOFORMU='04' and A.RPA_FOR_NUMERFORMU = '" + NumUrgencia + "'  ";

            return getDataTable(sql);
        }


        public DataTable ActualizarTriageCausa(string NumUrgencia, string CausaExterna)
        {
            string sql = "update rpa_fordau c set C.TAB_CONT_CODIGO = '" + CausaExterna + "' where C.RPA_FOR_TIPOFORMU='04' and C.RPA_FOR_NUMERFORMU = '" + NumUrgencia + "'  ";

            return getDataTable(sql);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA("Oracle").getDataset(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA("Oracle").getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
