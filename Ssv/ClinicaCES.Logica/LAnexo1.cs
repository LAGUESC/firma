﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LAnexo1
    {

        public bool Anexo1Crear(Anexo1 ane1)
        {
            bool bandera = false;
            string[] nomParam = 
            {
                "@NoSolicitud"
               ,"@Para"
               ,"@Mensaje"
               ,"@Mensaje2"
               ,"@TipoAnexo"
               ,"@Asunto"
               ,"@Ruta"
            };

            object[] vlrParam = 
            {
                ane1.Numero,
                ane1.Para,
                ane1.Mensaje,
                ane1.Mensaje2,
                ane1.TipoAnexo,
                ane1.Asunto,
                ane1.Ruta
            };
            string sql1 = "select pac_pac_numero from pac_paciente where trim(pac_pac_rut)='" + ane1.Identificacion + "'";
            DataRow Valor = getDataTable(sql1, "Oracle").Rows[0];
            string SQL = "INSERT INTO CENAUT_ANEXO1 " +
                         "( NUMERO, " +
                         "FECHA, " +
                         "HORA, " +
                         "TIPOINCONSISTENCIA, " +
                         "COBERTURA, " +
                         "INFOBD, " +
                         "CORRECCION, " +
                         "OBSERVACION, " +
                         "SOLICITANTE, " +
                         "PDF, " +
                         "PAC_PAC_NUMERO, " +
                         "USUARIO)  " +
                         " VALUES " +
                         "(" + ane1.Numero + " " +
                         ", sysdate " +
                         ", '" + ane1.Hora + "' " +
                         ", '" + ane1.TipoInconsistencia + "' " +
                         ", '" + ane1.Cobertura + "' " +
                         ", '" + ane1.InfoBD + "' " +
                         ", '" + ane1.Correccion + "' " +
                         ", '" + ane1.Observacion + "' " +
                         ", '" + ane1.Solicitante + "' " +
                         ", '" + ane1.pdf + "' " +
                         ", '" + Valor[0] + "' " +
                         ", '" + ane1.Usuario + "') ";


            if (EjecutarSentenciaOracle(SQL))
            {
                bandera = EjecutarSentencia("uspAnexoActualizar", nomParam, vlrParam);
            }
            return bandera;
        }
        public DataTable TraerPaciente(string Paciente, string TipoId)
        {
            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };
            string sql = "";
            sql = "select TRIM(a.pac_pac_apellpater) PRIAPELLIDO, TRIM(a.PAC_PAC_APELLMATER) SEGAPELLIDO,  TRIM(SUBSTR(a.PAC_PAC_NOMBRE, 1, INSTR(a.PAC_PAC_NOMBRE,' '))) PRINOMBRE, TRIM(SUBSTR(a.PAC_PAC_NOMBRE,  " +
" INSTR(a.PAC_PAC_NOMBRE,' '))) SEGNOMBRE, TRIM(A.PAC_PAC_RUT) ID, a.PAC_PAC_TIPOIDENTCODIGO TIPOID, TO_CHAR(a.PAC_PAC_FECHANACIM, 'YYYY/MM/DD') FECNACIMIENTO, TRIM(a.PAC_PAC_DIRECCIONGRALHABIT) DIRECCION,  " +
"   SUBSTR(trim(a.PAC_PAC_FONO), 1,7) TELEFONO, TRIM(b.DVP_REG_Codigo) DEPARTAMENTO, TRIM(c.DVP_PRO_Codigo) CIUDAD,  SUBSTR(trim(a.PAC_PAC_FONO),8, 10),  TID.tipidav TIPOIDCOD   " +
" from pac_paciente a, DVP_Region b, DVP_Provincia c, tab_tipoident TID  " +
" WHERE TRIM(A.PAC_PAC_RUT)='" + Paciente + "' and TRIM(A.PAC_PAC_TIPOIDENTCODIGO)='" + TipoId + "'  and a.PAC_PAC_REGIOHABIT = b.DVP_REG_Codigo and a.PAC_PAC_CIUDAHABIT= c.DVP_PRO_Codigo and a.pac_pac_tipoidentcodigo=TID.tab_tipoidentcodigo";
            
            return getDataTable(sql, "Oracle");
        }

            #region Metodos Privados
            private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
            {
                return new DA.DA().getDataTable(sql, nomParam, valParam);
            }

            private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
            {
                return new DA.DA().getDataset(sql, nomParam, valParam);
            }

            private DataTable getDataTable(string sql)
            {
                return new DA.DA().getDataTable(sql);
            }

            private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
            {
                return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
            }
            private bool EjecutarSentenciaOracle(string sql)
            {
                return new DA.DA("Oracle").EjecutarSentencia(sql);
                //return new DA.DA().EjecutarSentenciaOracle(sql);
            }
            private DataTable getDataTable(string sql, string tipobd)
            {
                return new DA.DA(tipobd).getDataTable(sql);
            }


            #endregion
    }
}
