﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LUsuarios
    {

        public DataSet InicioSesion(string usr)
        {
            string[] nomParam =
            {
                "In_Usuario"
              
            };

            object[] vlrParam =
            {
                usr
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT ID.TAB_TIPOIDENTGLOSA TIPOID, PV.FLD_USERRUT IDENTIFICACION, PV.FLD_USERNAME NOMBRE, PV.USRCARGO CARGO, PRO.SER_PRO_REGPROF REGISTRO, TRAERCONTRASENA('" + usr.Trim().ToUpper() + "') PASS " +
                         "FROM PRIV_ADM.TBL_USER PV, SER_PROFESIONA PRO, TAB_TIPOIDENT ID " +
                         "WHERE PV.FLD_USERCODE = '" + usr.Trim().ToUpper() +  "' " +
                         "AND PV.TAB_TIPOIDENTCODIGO = ID.TAB_TIPOIDENTCODIGO " +
                         "AND PV.FLD_USERRUT = PRO.SER_PRO_RUT(+) " +
                         "AND PV.FLD_USERCODE = PRO.SER_PRO_CUENTA(+) " +
                         "AND ACTUSR = 1";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }





        public DataTable UsuarioConsultar(Usuarios usr)
        {
            string[] nomParam = { "@Usuario" };
            object[] vlrParam = { usr.Usuario };

            return getDataTable("uspUsuariosConsultar", nomParam, vlrParam);
        }

        public DataTable TemasConsultar()
        {
            return getDataTable("uspTemaListar");
        }
        public DataTable ResponsableConsultar()
        {
            return getDataTable("uspResponsableConsultar");
        }
        
        public string Email(string Usuario)
        {
            string[] nomParam = { "@usuario" };
            object[] vlrParam = { Usuario };

            DataTable dt = getDataTable("uspUsuarioEmail", nomParam, vlrParam);

            return dt.Rows[0]["EMAIL"].ToString();
        }

        public DataTable UsuarioVSConsultar()
        {
            return getDataTable("uspUsuarioVisualSoftistar");
        }

        public DataTable UsuarioConsultar()
        {
            return getDataTable("uspUsuarioListar");
        }

        public bool UsuarioRetirar(Usuarios usr)
        {
            string[] nomParam = { "@Usuario" };
            object[] vlrParam = { usr.Usuario };

            return EjecutarSentencia("uspUsuarioRetirar", nomParam, vlrParam);
        }

        public bool UsuarioActualizar(Usuarios usr)
        {
            string[] Cliente = usr.Cliente.Split('|');

            string[] nomParam = { 
            "@Usuario",
	        "@Clave" ,
	        "@Nivel" ,
	        "@Nombre" ,
	        "@Apellido1" ,
	        "@Apellido2",
	        "@Direccion" ,
	        "@Telefono" ,
	        "@UsrMod" ,
	        "@Email" ,
            "@Tema",
            "@Nit",
            "@Negocio"
            };

            object[] vlrParam = {
            usr.Usuario,
            usr.Clave,
            usr.Nivel,
            usr.Nombres,
            usr.Apellido1,
            usr.Apellido2,
            usr.Direccion,
            usr.Telefono,
            usr.UsrMod,
            usr.Email,
            usr.Tema,
            Cliente[0],Cliente[1]
            };

            return EjecutarSentencia("uspUsuarioActualizar", nomParam, vlrParam);
        }

        #region Metodos Privados

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }
        #endregion
    }
}
