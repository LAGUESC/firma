﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Requerimientos
    {
        public string fecha;
        public string horaReq;
        public string idcliente;
        public string negocio;
        public string idusuario;
        public string telefono;
        public string email;
        public string idaplicacion;
        public string idestado;
        public string idencargado;
        public string idatendio;
        public string fechaComp;
        public string IdReq;
        public string Descripcion;
        public bool? facturable;

        public string idEstado;
        public string idUsuClie;
        public string Informacion;
        public string IdUsuario;
        public string Fecha;
        public string Hora;
        public string IdResponsable;
        public string idAsignado;
    }
}
