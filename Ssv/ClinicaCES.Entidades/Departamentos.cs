﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Departamentos
    {
        public string Pais;
        public string CodDepto;
        public string Departamento;
        public string Indicativo;
        public string Usuario;
        public bool? Estado;
    }
}
