﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class FactoresEntorno
    {
        public string Codigo;
        public string Descripcion;
        public string Nombre;
        public string usr;
        public bool? Estado;
        public string xml;
        public string Proyecto;
        public string Peso;
    }
}
