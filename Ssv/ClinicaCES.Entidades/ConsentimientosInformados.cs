﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class ConsentimientosInformados
    {
        public string Cedula;
        public string CedulaUsuario;
        public DateTime FechaRegistro;
        public string Hash;      
        public byte[] Consentimiento_PDF;
        public string NombreUsuario;
        public string NombrePaciente;
        public bool AprobacionMedico;
        public string BodyHtml;
        public byte[] PDF_Tratamiento;

        public int Id_Firma;
        public byte[] UserFirmPath;

        public string CodConsentimiento; 
    }
}
