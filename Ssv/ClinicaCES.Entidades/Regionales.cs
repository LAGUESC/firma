﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Regionales
    {
        public string Codigo;
        public string Regional;
        public string Compania;
        public string Usr;
        public bool? Estado;
    }
}
