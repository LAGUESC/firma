﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class MMenu
    {
        public string Codigo;
        public string Menu;
        public string Aplicacion;
        public string Usr;
        public bool? Estado;

        public int Id_Padre;
        public string Usuario;
    }
}
