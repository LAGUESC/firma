﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ClinicaCES.Entidades
{
    public class Estimacion_Proyectos
    {
        public string Codigo;
        public string Descripcion;
        public string Porcentaje;
        public string usr;
        public bool? Estado;

        public string Nit;
        public string Negocio;
        public string Gerente;
        public string xmlUsos;
        public string xmlComplejidad;
        public string xmlFactor;
        public string simple;
        public string medio;
        public string complejo;
        public string Horas;
        public string Proyecto;
        public string Vlr;
        public string xmlEstimacion;
    }
}
