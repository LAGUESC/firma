﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data.SqlClient;
using System.Data;
using System.Data.OracleClient;
using System.Data.Odbc;

/*using Oracle.DataAccess.Client;  */



namespace ClinicaCES.DA
{
    public class DA
    {
        #region Variables declaradas

        private string strStringConexion;

        #region Variables Sql
        private SqlConnection cnn;
        private SqlCommand cmd;
        #endregion

        #region Variables Oracle;
        private OracleConnection cnno;
        private OracleCommand cmdo;
        
        #endregion
        #region Variables SqlWeb;
        private SqlConnection cnnoSqlWeb;

        private SqlCommand cmdmSqlWeb;
        #endregion
        #region Variables Mysql;
        //private OdbcConnection cnnoMysql;
        private OdbcConnection cnnoMysql;
        private OdbcCommand cmdmMysql;
        #endregion
        #region Tipo Conexion
        public enum TipoCnn { Sql, Oracle, SqlWeb, Mysql };
        private TipoCnn strTipo_Conexion = TipoCnn.Sql;
        public TipoCnn TipoConexion
        {
            get { return strTipo_Conexion; }
            set { this.strTipo_Conexion = (TipoCnn)value; }
        }
        #endregion


        public string StringConexion
        {
            set
            {
                strStringConexion = value;
            }
        }


        #endregion

        /// <summary>
        /// El contructor instancia un objeto conexion
        /// </summary>
        public DA()
        {
            strTipo_Conexion = TipoCnn.Sql;
            strStringConexion = ConfigurationManager.AppSettings["CadenaConexion"];// ConnectionStrings["CadenaConexion"].ToString();
            cnn = new SqlConnection(strStringConexion);

            strTipo_Conexion = TipoCnn.Sql;
            strStringConexion = ConfigurationManager.AppSettings["CadenaConexion1"];// ConnectionStrings["CadenaConexion"].ToString();
            cnn = new SqlConnection(strStringConexion);
        }

        public DA(string TipoCon)
        {
            if (TipoCon == "Oracle")
            {
                strTipo_Conexion = TipoCnn.Oracle;
            }
            else
            {
                if (TipoCon == "SqlWeb")
                { strTipo_Conexion = TipoCnn.SqlWeb; }
                else
                    if (TipoCon == "Mysql")
                    { strTipo_Conexion = TipoCnn.Mysql; }
                    else
                        { strTipo_Conexion = TipoCnn.Sql; }
            }

            if (strTipo_Conexion == TipoCnn.Oracle)
            {
                strStringConexion = ConfigurationManager.AppSettings["CadenaConexionOracle"];// ConnectionStrings["CadenaConexion"].ToString();
                cnno = new OracleConnection(strStringConexion);
            }
            else
            {
                if (strTipo_Conexion == TipoCnn.SqlWeb)
                {
                    strStringConexion = ConfigurationManager.AppSettings["CadenaConexionSqlWeb"];// ConnectionStrings["CadenaConexion"].ToString();
                    cnnoSqlWeb = new SqlConnection (strStringConexion);
                }
                else
                    if (strTipo_Conexion == TipoCnn.Mysql)
                    {
                        strStringConexion = ConfigurationManager.AppSettings["CadenaConexionMysql"];// ConnectionStrings["CadenaConexion"].ToString();
                        cnnoMysql = new OdbcConnection(strStringConexion);
                        //cnnoMysql = new OdbcConnection ("File Name = C:\\prueba.udl");
                    }
                    else
                {
                    strStringConexion = ConfigurationManager.AppSettings["CadenaConexion"];// ConnectionStrings["CadenaConexion"].ToString();
                    cnn = new SqlConnection(strStringConexion);

                    strTipo_Conexion = TipoCnn.Sql;
                    strStringConexion = ConfigurationManager.AppSettings["CadenaConexion1"];// ConnectionStrings["CadenaConexion"].ToString();
                    cnn = new SqlConnection(strStringConexion);
                }
            }
        }

        #region Metodos Publicos

        /// <summary>
        /// Ejecuta un sp y devuelve un dataset lleno
        /// </summary>
        /// <param name="strSQL">El nombre del sp a ejecutar</param>
        /// <param name="NombreParametros">Vector con el nombre de los parametros del sp</param>
        /// <param name="ValoresParametros">Vector con los valores de los parametros del sp</param>
        /// <returns>Dataset de la consulta realizada</returns>
        public DataSet getDataset(string strSQL, string[] NombreParametros, object[] ValoresParametros)
        {
            DataSet ds = new DataSet();

            try
            {
                if (strTipo_Conexion == TipoCnn.Oracle)
                {
                    cmdo = cnno.CreateCommand();
                    cmdo.CommandText = strSQL;
                    cmdo.CommandType = CommandType.StoredProcedure;
                    this.setParametrosOracle(cmdo, getParamertosOracle(NombreParametros, ValoresParametros));
                    OracleDataAdapter da = new OracleDataAdapter(cmdo);
                    da.Fill(ds);
                }
                else
                {
                    cmd = cnn.CreateCommand();
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = 99999999;
                    cmd.CommandType = CommandType.StoredProcedure;
                    this.setParametros(cmd, getParamertos(NombreParametros, ValoresParametros));
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }


                return ds;
            }
            catch (Exception ex)
            {
                EscribirError(ex.ToString());
                return (null);
            }
        }

        /// <summary>
        /// Ejecuta un sp y devuelve un dataset lleno
        /// </summary>
        /// <param name="strSQL">El nombre del sp a ejecutar</param>
        /// <param name="NombreParametros">Vector con el nombre de los parametros del sp</param>
        /// <param name="ValoresParametros">Vector con los valores de los parametros del sp</param>
        /// <returns>Dataset de la consulta realizada</returns>
        public DataSet getDataset(string strSQL, string[] NombreParametros, object[] ValoresParametros, OracleType[] typParam, ParameterDirection[] DirParam)
        {
            DataSet ds = new DataSet();

            try
            {
                if (strTipo_Conexion == TipoCnn.Oracle)
                {
                    cmdo = cnno.CreateCommand();
                    cmdo.CommandText = strSQL;
                    cmdo.CommandType = CommandType.Text;// .StoredProcedure;
                    //cmdo.Parameters.Add("Regrepcursor", OracleType.Cursor).Direction = ParameterDirection.Output;

                    //this.setParametrosOracle(cmdo, NombreParametros, typParam, DirParam);
                    //this.setParametrosOracle(cmdo, getParamertosOracle(NombreParametros, ValoresParametros));
                    OracleDataAdapter da = new OracleDataAdapter(cmdo);
                    da.Fill(ds);
                }
                
                else if (strTipo_Conexion == TipoCnn.Mysql)
                {
                    cmdmMysql = cnnoMysql.CreateCommand();
                    cmdmMysql.CommandText = strSQL;
                    cmdmMysql.CommandType = CommandType.Text;// .StoredProcedure;
                    //cmdo.Parameters.Add("Regrepcursor", OracleType.Cursor).Direction = ParameterDirection.Output;

                    //this.setParametrosOracle(cmdo, NombreParametros, typParam, DirParam);
                    //this.setParametrosOracle(cmdo, getParamertosOracle(NombreParametros, ValoresParametros));
                    OdbcDataAdapter da = new OdbcDataAdapter(cmdmMysql);
                    da.Fill(ds);
                }
                else
                {
                    cmd = cnn.CreateCommand();
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = 99999999;
                    cmd.CommandType = CommandType.StoredProcedure;
                    this.setParametros(cmd, getParamertos(NombreParametros, ValoresParametros));
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }


                return ds;
            }
            catch (Exception ex)
            {
                EscribirError(ex.ToString());
                return (null);
            }
        }
        public DataSet getDataset(string strSQL, string[] NombreParametros, object[] ValoresParametros,  SqlDbType[] typParam, ParameterDirection[] DirParam)
        {
            DataSet ds = new DataSet();

            try
            {
                if (strTipo_Conexion == TipoCnn.Oracle)
                {
                    cmdo = cnno.CreateCommand();
                    cmdo.CommandText = strSQL;
                    cmdo.CommandType = CommandType.Text;// .StoredProcedure;
                    //cmdo.Parameters.Add("Regrepcursor", OracleType.Cursor).Direction = ParameterDirection.Output;

                    //this.setParametrosOracle(cmdo, NombreParametros, typParam, DirParam);
                    //this.setParametrosOracle(cmdo, getParamertosOracle(NombreParametros, ValoresParametros));
                    OracleDataAdapter da = new OracleDataAdapter(cmdo);
                    da.Fill(ds);
                }
                else if (strTipo_Conexion == TipoCnn.SqlWeb)
                {
                    cmdmSqlWeb = cnnoSqlWeb.CreateCommand();
                    cmdmSqlWeb.CommandText = strSQL;
                    cmdmSqlWeb.CommandType = CommandType.Text;// .StoredProcedure;
                    //cmdo.Parameters.Add("Regrepcursor", OracleType.Cursor).Direction = ParameterDirection.Output;

                    //this.setParametrosOracle(cmdo, NombreParametros, typParam, DirParam);
                    //this.setParametrosOracle(cmdo, getParamertosOracle(NombreParametros, ValoresParametros));
                    SqlDataAdapter da = new SqlDataAdapter(cmdmSqlWeb);
                    da.Fill(ds);
                }
                else
                {
                    cmd = cnn.CreateCommand();
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = 99999999;
                    cmd.CommandType = CommandType.StoredProcedure;
                    this.setParametros(cmd, getParamertos(NombreParametros, ValoresParametros));
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }


                return ds;
            }
            catch (Exception ex)
            {
                EscribirError(ex.ToString());
                return (null);
            }
        }
        /// <summary>
        /// Ejecuta un sp y devuelve un dataset lleno
        /// </summary>
        /// <param name="strSQL">El nombre del sp a ejecutar</param>
        /// <returns>Dataset de la consulta realizda</returns>
        public DataSet getDataset(string strSQL)
        {
            DataSet ds = new DataSet();

            try
            {
                if (strTipo_Conexion == TipoCnn.Oracle)
                {
                    cmdo = cnno.CreateCommand();
                    cmdo.CommandText = strSQL;
                    cmdo.CommandType = CommandType.StoredProcedure;
                    OracleDataAdapter da = new OracleDataAdapter(cmdo);
                    da.Fill(ds);
                }
                else
                {
                    cmd = cnn.CreateCommand();
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = 99999999;
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(ds);
                }

                return ds;
            }
            catch (Exception ex)
            {
                EscribirError(ex.ToString());
                return null;
            }
        }

        /// <summary>
        /// Ejecuta un sp y devuelve un datatable lleno
        /// </summary>
        /// <param name="strSQL">El nombre del sp a ejecutar</param>
        /// <returns>DataTable de la consulta realizda</returns>
        public DataTable getDataTable(string strSQL)
        {
           
            DataTable dt = new DataTable();

            try
            {
                if (strTipo_Conexion == TipoCnn.Oracle)
                {
                    cmdo = cnno.CreateCommand();
                    cmdo.CommandText = strSQL;
                    OracleDataAdapter da = new OracleDataAdapter(cmdo);
                    da.Fill(dt);
                }
                else
                {
                    cmd = cnn.CreateCommand();
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = 99999999;
                    cmd.CommandType = CommandType.StoredProcedure;
                    //this.setParametros(cmd, getParamertos(NombreParametros, ValoresParametros));
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);


                    //cmd = cnn.CreateCommand();
                    //cmd.CommandText = strSQL;
                    //cmd.CommandTimeout = 99999999;
                    //SqlDataAdapter da = new SqlDataAdapter(cmd);
                    //da.Fill(dt);
                }
                return dt;
            }
            catch (Exception ex)
            {
                EscribirError(ex.ToString());
                return null;
            }
        }
        public string getDataTableprueba(string strSQL)
        {

            DataTable dt = new DataTable();

            try
            {
                if (strTipo_Conexion == TipoCnn.Oracle)
                {
                    cmdo = cnno.CreateCommand();
                    cmdo.CommandText = strSQL;
                    OracleDataAdapter da = new OracleDataAdapter(cmdo);
                    da.Fill(dt);
                }
                else
                {
                    cmd = cnn.CreateCommand();
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = 99999999;
                    cmd.CommandType = CommandType.StoredProcedure;
                    //this.setParametros(cmd, getParamertos(NombreParametros, ValoresParametros));
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);


                    //cmd = cnn.CreateCommand();
                    //cmd.CommandText = strSQL;
                    //cmd.CommandTimeout = 99999999;
                    //SqlDataAdapter da = new SqlDataAdapter(cmd);
                    //da.Fill(dt);
                }
                return cnno.ConnectionString.ToString();
            }
            catch (Exception ex)
            {
                EscribirError(ex.ToString());
                return ex.ToString() + "     " + cnno.ConnectionString.ToString(); 
            }
        }
        public string GetDbmsOutputLine(string strSQL)
    {
             try
            {
        string line="";
            if (strTipo_Conexion == TipoCnn.Oracle)
                {
                    cmdo = cnno.CreateCommand();
                    cmdo.CommandType=CommandType.Text;
                    cmdo.CommandText = strSQL;
                    OracleParameter lineParameter = new OracleParameter("line",
           OracleType.VarChar);
                    lineParameter.Size = 32000;
                    lineParameter.Direction = ParameterDirection.Output;
                    cmdo.Parameters.Add(lineParameter);

                    OracleParameter statusParameter = new OracleParameter("status",
                        OracleType.Int32);
                    statusParameter.Direction = ParameterDirection.Output;
                    cmdo.Parameters.Add(statusParameter);

                    cmdo.ExecuteNonQuery();

                    if (cmdo.Parameters["line"].Value is DBNull)
                        return null;

                     line = cmdo.Parameters["line"].Value as string;

                    
                }

            return line;
            }
             catch (Exception ex)
             {
                 EscribirError(ex.ToString());
                 return null;
             }

       
    }

        /// <summary>
        /// Ejecuta un sp y devuelve un DataTable lleno
        /// </summary>
        /// <param name="strSQL">El nombre del sp a ejecutar</param>
        /// <param name="NombreParametros">Vector con el nombre de los parametros del sp</param>
        /// <param name="ValoresParametros">Vector con los valores de los parametros del sp</param>
        /// <returns>DataTable de la consulta realizada</returns>
        public DataTable getDataTable(string strSQL, string[] NombreParametros, object[] ValoresParametros)
        {
            DataTable dt = new DataTable();

            try
            {
                if (strTipo_Conexion == TipoCnn.Oracle)
                {
                    cmdo = cnno.CreateCommand();
                    cmdo.CommandText = strSQL;
                    cmdo.CommandType = CommandType.Text;
                   // this.setParametrosOracle(cmdo, getParamertosOracle(NombreParametros, ValoresParametros));
                    OracleDataAdapter da = new OracleDataAdapter(cmdo);
                    da.Fill(dt);
                }
                else
                {
                    cmd = cnn.CreateCommand();
                    cmd.CommandText = strSQL;
                    cmd.CommandTimeout = 99999999;
                    cmd.CommandType = CommandType.StoredProcedure;
                    this.setParametros(cmd, getParamertos(NombreParametros, ValoresParametros));
                    SqlDataAdapter da = new SqlDataAdapter(cmd);
                    da.Fill(dt);
                }
                return dt;
            }
            catch (Exception ex)
            {
                EscribirError(ex.ToString());
                return (null);
            }
        }

        /// <summary>
        /// Ejecuacion de consultas de Update, Insert y Delete, este metodo no devuelve resultados
        /// </summary>
        /// <param name="strSQL">El nombre del sp a ejecutar</param>
        /// <param name="NombreParametros">Vector con el nombre de los parametros del sp</param>
        /// <param name="ValoresParametros">Vector con los valores de los parametros del sp</param>
        /// <returns>retorna si pudo ejecutar la consulta</returns>
        public bool EjecutarSentencia(string strSQL, string[] NombreParametros, object[] ValoresParametros)
        {
            if (AbrirConexion())
            {
                try
                {
                    if (strTipo_Conexion == TipoCnn.Oracle)
                    {
                        cmdo = cnno.CreateCommand();
                        cmdo.CommandType = CommandType.StoredProcedure;
                        cmdo.CommandText = strSQL;
                        this.setParametrosOracle(cmdo, getParamertosOracle(NombreParametros, ValoresParametros));
                        cmdo.ExecuteNonQuery();
                        //CerrarConexion();
                    }
                    else
                    {
                        cmd = cnn.CreateCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 99999999;
                        cmd.CommandText = strSQL;
                        this.setParametros(cmd, getParamertos(NombreParametros, ValoresParametros));
                        cmd.ExecuteNonQuery();
                        //CerrarConexion();
                    }

                    CerrarConexion();
                    return (true);
                }
                catch (SqlException ex)
                {
                    EscribirError(ex.ToString());
                    return (false);
                }
            }
            else
            {
                CerrarConexion();
                return (false);
            }
        }

        /// <summary>
        /// Ejecuacion de consultas de Update, Insert y Delete, este metodo no devuelve resultados
        /// </summary>
        /// <param name="strSQL">El nombre del sp a ejecutar</param>        
        /// <returns>retorna si pudo ejecuatar la consulta</returns>
        public bool EjecutarSentencia(string strSQL)
        {
            if (AbrirConexion())
            {
                try
                {
                    if (strTipo_Conexion == TipoCnn.Oracle)
                    {
                        cmdo = cnno.CreateCommand();
                        cmdo.CommandType = CommandType.StoredProcedure;
                        cmdo.CommandText = strSQL;
                        cmdo.ExecuteNonQuery();
                    }
                    else
                    {
                        cmd = cnn.CreateCommand();
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.CommandTimeout = 99999999;
                        cmd.CommandText = strSQL;
                        cmd.ExecuteNonQuery();
                    }


                    CerrarConexion();
                    return (true);
                }
                catch (SqlException ex)
                {
                    EscribirError(ex.ToString());
                    return false;
                }
            }
            else
            {
                CerrarConexion();
                return (false);
            }
        }
        public bool EjecutarSentenciaOracle(string strSQL)
        {
            if (AbrirConexion())
            {
                try
                {
                    cmdo = cnno.CreateCommand();
                    cmdo.CommandType = CommandType.Text;
                    cmdo.CommandText = strSQL;
                    cmdo.ExecuteNonQuery();
                    CerrarConexion();
                    return (true);
                }
                catch (SqlException ex)
                {
                    EscribirError(ex.ToString());
                    return false;
                }
            }
            else
            {
                CerrarConexion();
                return (false);
            }
        }

        #endregion

        #region Metodos Privados
        /// <summary>
        /// Asigna parametros a un SqlCommand
        /// </summary>
        /// <param name="setCmd">SqlCommand para la asignacion de parametros</param>
        /// <param name="setParameter">Un array de parametros SqlParameter para ser asinados</param>
        private void setParametros(SqlCommand setCmd, SqlParameter[] setParameter)
        {
            if (setParameter != null)
            {
                for (int ic = 0; ic <= setParameter.Length - 1; ic++)
                {
                    setCmd.Parameters.Add(setParameter[ic]);
                }
            }

        }
        /// <summary>
        /// Asigna parametros a un OracleCommand
        /// </summary>
        /// <param name="setCmd">OracleCommand para la asignacion de parametros</param>
        /// <param name="setParameter">Un array de parametros OracleParameter para ser asinados</param>
        private void setParametrosOracle(OracleCommand setCmd, OracleParameter[] setParameter)
        {
            if (setParameter != null)
            {
                for (int ic = 0; ic <= setParameter.Length - 1; ic++)
                {
                    setCmd.Parameters.Add(setParameter[ic]);
                }
            }
            
        }


        /// <summary>
        /// Asigna parametros a un OracleCommand
        /// </summary>
        /// <param name="setCmd">OracleCommand para la asignacion de parametros</param>
        /// <param name="setParameter">Un array de parametros OracleParameter para ser asinados</param>
        private void setParametrosOracle(OracleCommand setCmd, string[] nomParametro, OracleType[] typParam, ParameterDirection[] DirParam)
        {
            if (nomParametro != null)
            {
                for (int ic = 0; ic <= nomParametro.Length - 1; ic++)
                {
                    
                    setCmd.Parameters[nomParametro[ic]].Direction = DirParam[ic];
                    setCmd.Parameters.Add(nomParametro[ic], typParam[ic]);
                }
            }

        }
        /// <summary>
        /// Metodo bool que abre una conexion con la base de datos
        /// </summary>
        /// <returns>retorna si pudo abrir la conexion</returns>
        private bool AbrirConexion()
        {
            if (strStringConexion == null)
            {
                return false;
            }
            else
            {
                try
                {
                    if (strTipo_Conexion == TipoCnn.Oracle)
                    {
                        cnno.ConnectionString = strStringConexion;
                        cnno.Open();
                    }
                    else
                    {
                        cnn.ConnectionString = strStringConexion;
                        cnn.Open();
                    }

                    return true;
                }
                catch (Exception ex)
                {
                    return false;
                }
            }
        }
        /// <summary>
        /// Cierra conexiones abiertas y elimina Objetos Instanciados 
        /// </summary>
        private void CerrarConexion()
        {
            if (strTipo_Conexion == TipoCnn.Oracle)
            {
                if (cnno.State == ConnectionState.Open)
                {
                    cnno.Close();
                }
            }
            else
            {
                if (cnn.State == ConnectionState.Open)
                {
                    cnn.Close();
                }
            }
        }

        private SqlParameter[] getParamertos(string[] NombreParametros, object[] ValoresParametros)
        {
            SqlParameter[] arrParametros = new SqlParameter[NombreParametros.Length];

            for (int ic = 0; ic <= NombreParametros.Length - 1; ic++)
            {
                SqlParameter Parametro = new SqlParameter();
                Parametro.ParameterName = NombreParametros[ic];
                Parametro.Value = ValoresParametros[ic];
                arrParametros[ic] = Parametro;
                Parametro = null;
            }
            return arrParametros;
        }

        private OracleParameter[] getParamertosOracle(string[] NombreParametros, object[] ValoresParametros)
        {
            OracleParameter[] arrParametros = new OracleParameter[NombreParametros.Length];

            for (int ic = 0; ic <= NombreParametros.Length - 1; ic++)
            {
                OracleParameter Parametro = new OracleParameter();
                Parametro.ParameterName = NombreParametros[ic];
                Parametro.Value = ValoresParametros[ic];
                arrParametros[ic] = Parametro;
                Parametro = null;
            }
            return arrParametros;
        }

        private void EscribirError(string Error)
        {
            //new Error.Error().EscibirError(Error);
        }

        #endregion
    }
}
