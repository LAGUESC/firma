﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Configuration;

namespace ClinicaCES.Error
{
    public class Error
    {
        /// <summary>
        /// Escribe en un archivo plano los erros cometidos, la ruta de este esta en el web.config
        /// </summary>
        /// <param name="Mensaje">Cadena a Escribir en el Archivo</param>
        public void EscibirError(string Mensaje)
        {
            try
            {
                string Ruta = ConfigurationManager.AppSettings["ArchivoError"];
                StreamWriter sw = new StreamWriter(Ruta, true);
                sw.WriteLine(DateTime.Now + " // " + Mensaje);
                sw.Close();
                ClinicaCES.Correo.Correo objCorreo = new ClinicaCES.Correo.Correo();
                objCorreo.Asunto = "Error SICES";
                objCorreo.De = "luzcuervo@clinicaces.com";
                objCorreo.Mensaje = Mensaje;
                objCorreo.Para = "luzcuervo@clinicaces.com";

                objCorreo.Enviar();
            }
            catch (Exception ex)
            {
                string strerror = ex.Message;
            }
        }
        /// <summary>
        /// envia por correo electronico todos los errores cometidos por cual quier estancia de la aplicaccion
        /// </summary>
        /// <param name="Mensaje">El mesaje de error resivido por el aplicativo</param>
        //public void EnviarError(string Mensaje)
        //{
        //    System.Net.Mail.MailMessage correo = new System.Net.Mail.MailMessage();
        //    correo.From = new System.Net.Mail.MailAddress("Igerencia@Igerencia.com");
        //    correo.To.Add(ConfigurationManager.AppSettings["CorreosError"]);
        //    correo.Subject = "Aplicativo Error";
        //    correo.Body = Mensaje;
        //    correo.IsBodyHtml = false;
        //    System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient();
        //    smtp.Host = "antares.igerencia.com";
        //    //smtp.Credentials = new System.Net.NetworkCredential("usuario", "password");
        //    try
        //    {
        //        smtp.Send(correo);
        //    }
        //    catch (Exception ex)
        //    {
        //        EscibirError(ex.Message + "--" + ex.StackTrace);
        //    }
        //}
    }
}
