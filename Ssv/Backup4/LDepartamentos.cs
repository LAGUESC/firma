﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LDepartamentos
    {
        public bool DepartamentoGuardar(Departamentos departamento)
        {
            string[] nomParam = { "@Codigo", "@Pais", "@Departamento", "@Indicativo", "@Usuario" };

            object[] vlrParam = { departamento.CodDepto, departamento.Pais, departamento.Departamento, departamento.Indicativo, departamento.Usuario };

            return EjecutarSentencia("uspDepartamentoActualizar", nomParam, vlrParam);
        }

        public DataTable DepartamentoConsultar(string Codigo)
        { 
            string[] nomParam = { "@Codigo" };

            object[] vlrParam = { Codigo };

            return getDataTable("uspDepartamentoConsultar", nomParam, vlrParam);
        }

        public DataTable DeparConsultar(Departamentos dep)
        {
            string[] nomParam = 
            {
                "@Pais",
	            "@Codigo"
            };

            object[] vlrParam = 
            {
                dep.Pais,
                dep.CodDepto
            };

            return getDataTable("uspDepConsultar", nomParam, vlrParam);
        }

        public bool DepRetirar(string xml, string Usr)
        {
            string[] nomParam = 
            {
                "@xmlDep" ,
	            "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspDepRetirarXml", nomParam, vlrParam);
        }

        public DataTable PaisListar()
        {
            return getDataTable("uspPaisListar");
        }

        public bool DepActualizar(Departamentos dep)
        {
            string[] nomParam = 
            {
                "@Pais",
	            "@Codigo",
                "@Dep",
	            "@Usr"
            };

            object[] vlrParam = 
            {
                dep.Pais,
                dep.CodDepto,
                dep.Departamento,
                dep.Usuario
            };

            return EjecutarSentencia("uspDepActualizar", nomParam, vlrParam);
        }

        public bool DepEstado(Departamentos dep)
        {
            string[] nomParam = 
            {
                "@Pais",
	            "@Codigo",
                "@Estado"
            };

            object[] vlrParam = 
            {
                dep.Pais,
                dep.CodDepto,
                dep.Estado
            };

            return EjecutarSentencia("uspDepcambiarEstado", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
