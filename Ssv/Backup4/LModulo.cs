﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LModulo
    {
        public DataTable AplicacionListar()
        {
            return getDataTable("uspAplicacionListar");
        }

        public bool ModuloEstado(Modulo men)
        {
            string[] nomParam = 
            {
                "@Aplicacion",
	            "@Codigo",
                "@Estado",
                "@Menu"
            };

            object[] vlrParam = 
            {
                men.Aplicacion,
                men.Codigo,
                men.Estado,
                men.Menu
            };

            return EjecutarSentencia("uspModulocambiarEstado", nomParam, vlrParam);
        }

        public bool ModuloRetirar(string xml, string Usr)
        {
            string[] nomParam = 
            {
                "@xmlModulo" ,
	            "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspModuloRetirarXml", nomParam, vlrParam);
        }

        public bool ModuloActualizar(Modulo men)
        {
            string[] nomParam = 
            {
                "@Aplicacion",
	            "@Codigo",
                "@Modulo",
	            "@Usr",
                "@Menu"
            };

            object[] vlrParam = 
            {
                men.Aplicacion,
                men.Codigo,
                men.Mod,
                men.Usr,
                men.Menu
            };

            return EjecutarSentencia("uspModuloActualizar", nomParam, vlrParam);
        }

        public DataTable ModuloConsultar(Modulo men)
        {
            string[] nomParam = 
            {
                "@Aplicacion",
                "@Menu",
	            "@Codigo"
            };

            object[] vlrParam = 
            {
                men.Aplicacion,
                men.Menu,
                men.Codigo
            };

            return getDataTable("uspModuloConsultar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
