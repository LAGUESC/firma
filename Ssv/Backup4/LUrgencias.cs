﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LUrgencias
    {

        public DataSet PuntualidadUrgencias(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select a.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,b.RPA_FDA_HORAINGRESO triage_admon,b.FECPRC triage_medico,c.MTVFECHACONSULTA apertura_hc,b.RPA_FDA_HORAEGRESO egreso_urg, " +
                                 "round( (c.MTVFECHACONSULTA-b.RPA_FDA_HORAINGRESO)*24*60 ,2) OPORT_ADM_HC, ROUND((b.FECPRC-b.RPA_FDA_HORAINGRESO)*24*60,2) OPORT_ADM_MD, ROUND((c.MTVFECHACONSULTA-b.FECPRC)*24*60,2) OPORT_MD_HC,decode(e.triagecod, '2', 'TRIAGE II', '1','TRIAGE III', '0', 'SIN INFORMACION', '3', 'TRIAGE I', '4', 'TRIAGE IV') tipo_triage " +
                         "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e,CON_Convenio D,tabtriagecal f,tabespdsturg g,TAB_Contigencia h " +
                         "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO " +
                            "and b.MTVCORRELATIVO=c.MTVCORRELATIVO " +
                            "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO " +
                            "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU " +
                            "and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU " +
                            "and trunc(b.RPA_FDA_HORAINGRESO) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                            "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                            "and e.ESPDSTCOD='01' " +
                            "AND B.CON_CON_CODIGO=D.CON_CON_CODIGO " +
                            "and e.TRIAGECOD=f.TRIAGECOD " +
                            "and e.ESPDSTCOD=g.ESPDSTCOD " +
                            "and trim(A.PAC_PAC_RUT) not in ('10','12') " +
                            "and c.MTVCON_ORIGEN=h.TAB_CONT_CODIGO " +
                         "order by 3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet HorasProduccionHCUrg(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT HOR hora,COUNT(*) nhistorias FROM ( " +
                            "select to_char(E.FECPRC,'HH24')HOR " +
                              "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e,CON_Convenio D,tabtriagecal f,tabespdsturg g,TAB_Contigencia h " +
                             "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and b.MTVCORRELATIVO=c.MTVCORRELATIVO and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO " +
                               "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and e.TRIAGECOD=f.TRIAGECOD " +
                               "and trunc(E.FECPRC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                               "and e.ESPDSTCOD='01' AND B.CON_CON_CODIGO=D.CON_CON_CODIGO and e.ESPDSTCOD=g.ESPDSTCOD and trim(A.PAC_PAC_RUT) not in ('10','12') " +
                               "and c.MTVCON_ORIGEN=h.TAB_CONT_CODIGO " +
                            ") GROUP BY HOR order by 1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet HorasProduccionTRIAGEUrg(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select hor hora,count(*) ntriages from ( " +
                            "Select to_char(A.FECPRC,'yyyy/mm/dd')fec, to_CHAR(A.FECPRC,'HH24')hor " +
                              "from tabTriage a, TabDstPac b " +
                             "Where a.dstpaccod = b.dstpaccod  And a.espdstCod IN ('01') AND A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                               "and trunc(a.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                               "And Exists(Select 'x' from Rpa_forDau c Where c.Rpa_For_Tipoformu = a.Rpa_For_TipoFormu And c.Rpa_For_Numerformu = a.Rpa_For_NumerFormu And c.Pac_Pac_Numero = a.Pac_Pac_Numero)) " +
                         "group by hor order by 1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet PromedioHorasHCUrg(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT fecha fecha,COUNT(*) nhistorias FROM ( " +
                            "select to_char(E.FECPRC,'yyyy/mm/dd')fecha " +
                              "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e,CON_Convenio D,tabtriagecal f,tabespdsturg g,TAB_Contigencia h " +
                             "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and b.MTVCORRELATIVO=c.MTVCORRELATIVO and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO " +
                               "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and e.TRIAGECOD=f.TRIAGECOD " +
                               "and trunc(E.FECPRC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                               "and e.ESPDSTCOD='01' AND B.CON_CON_CODIGO=D.CON_CON_CODIGO and e.ESPDSTCOD=g.ESPDSTCOD and trim(A.PAC_PAC_RUT) not in ('10','12') " +
                               "and c.MTVCON_ORIGEN=h.TAB_CONT_CODIGO " +
                            ") GROUP BY FECHA order by 1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet PromedioHorasTRIAGEUrg(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select fec fecha,count(*) ntriages from ( " +
                            "Select to_char(A.FECPRC,'yyyy/mm/dd')fec, to_CHAR(A.FECPRC,'HH24')hor " +
                              "from tabTriage a, TabDstPac b " +
                             "Where a.dstpaccod = b.dstpaccod  And a.espdstCod IN ('01') AND A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                               "and trunc(a.FECPRC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                               "And Exists(Select 'x' from Rpa_forDau c Where c.Rpa_For_Tipoformu = a.Rpa_For_TipoFormu And c.Rpa_For_Numerformu = a.Rpa_For_NumerFormu And c.Pac_Pac_Numero = a.Pac_Pac_Numero)) " +
                         "group by fec order by 1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet UrgenciasHospitalizadas(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select paci.PAC_PAC_RUT identificacion, nomcompletopaciente(PACI.PAC_PAC_NUMERO) paciente , fordau.RPA_FDA_HORAINGRESO ingreso_urgencias,fordau.RPA_FDA_HORAEGRESO egreso_urgencias, " +
                                 "est.ATC_EST_FECHAHOSPI ingreso_hospitalizacion,est.ATC_EST_FECEGRESO egreso_hospitalizacion,round((est.ATC_EST_FECEGRESO-est.ATC_EST_FECHAHOSPI),2) dias_estancia " +
                           "from rpa_formulario formu, rpa_fordau fordau, pac_paciente paci,atc_estadia est,atc_cuenta cue " +
                          "where formu.RPA_FOR_NUMERFORMU = fordau.ATE_PRE_NUMERFORMU and  FORMU.rpa_for_urgencia = 'H' and formu.RPA_FOR_NUMERFORMU=cue.RPA_FOR_NUMERFORMU and est.ATC_EST_NUMERO=cue.ATC_EST_NUMERO  and formu.PAC_PAC_NUMERO=est.PAC_PAC_NUMERO and formu.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                            "and trunc (fordau.RPA_FDA_HORAINGRESO) between to_date ('" + FechaIni + "','YYYY/MM/DD') and to_date ('" + FechaFin + "','YYYY/MM/DD') and  paci.PAC_PAC_NUMERO = formu.PAC_PAC_NUMERO and paci.PAC_PAC_NUMERO = fordau.PAC_PAC_NUMERO order by 2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet ReingresosUrgencias(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select * from " +
                            "(select C.PAC_PAC_RUT identificacion, nomcompletopaciente(D.PAC_PAC_NUMERO) paciente, D.RPA_FDA_HORAINGRESO fecha_ingreso, D.RPA_FDA_HORAEGRESO fecha_egreso,G.DIA_DIA_CODIGO cie10,G.DIA_DIA_DESCRIPCIO diagnostico, " +
                            "round(((LEAD(D.RPA_FDA_HORAEGRESO, 1, NULL) OVER(ORDER BY d.PAC_PAC_NUMERO,D.RPA_FDA_HORAINGRESO desc)  - D.RPA_FDA_HORAINGRESO)*-1),2)AS total_dias, D.MTVCORRELATIVO evento " +
                            "from pac_paciente c, rpa_fordau d, tabtriage e, tabdiagnosticos f, dia_diagnos g, (select * from  (select A.PAC_PAC_NUMERO paciente,count(*) ingresos from  rpa_fordau a, tabtriage b " +
                                                                                                                                 "where  trunc(A.RPA_FDA_HORAEGRESO ) between to_date('" + FechaIni + "','yyyy/mm/dd')-3 and to_date('" + FechaFin + "','yyyy/mm/dd') and B.ESPDSTCOD='01' " +
                                                                                                                                 "and A.PAC_PAC_NUMERO=B.PAC_PAC_NUMERO and A.RPA_FOR_TIPOFORMU=B.RPA_FOR_TIPOFORMU and A.RPA_FOR_NUMERFORMU=B.RPA_FOR_NUMERFORMU " +
                                                                                                                                 "group by a.PAC_PAC_NUMERO order by 2 desc) where ingresos >1) " +
                            "where C.PAC_PAC_NUMERO=D.PAC_PAC_NUMERO and D.PAC_PAC_NUMERO=E.PAC_PAC_NUMERO and D.PAC_PAC_NUMERO=paciente and  d.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU and d.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU " +
                              "and e.ESPDSTCOD='01' and trunc(d.RPA_FDA_HORAEGRESO ) between to_date('" + FechaIni + "','yyyy/mm/dd')-3 and to_date('" + FechaFin + "','yyyy/mm/dd') and D.MTVCORRELATIVO=F.MTVCORRELATIVO and D.PAC_PAC_NUMERO=F.PAC_PAC_NUMERO " +
                              "and f.DGNPRINCIPAL='S' and F.DIA_DIA_CODIGO=G.DIA_DIA_CODIGO ORDER BY c.PAC_PAC_RUT,D.RPA_FDA_HORAINGRESO) " +
                        "where total_dias <=3 and total_dias >0 and trunc (fecha_egreso) >= to_date ('" + FechaIni + "','yyyy/mm/dd') and identificacion not in ('10           ','12           ')  ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
