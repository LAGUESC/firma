﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LActividades
    {

        public DataTable LineaListar()
        {
            return getDataTable("uspLineaListar");
        }

        public DataTable AtividadConsultar(ActividadReg act)
        {
            string[] nomParam = {
                "@Semana"
                ,"@Responsable"
            };

            object[] vlrParam = 
            {
                act.Semana,
                act.Responsable,
            };

            return getDataTable("uspRegActividadesConsultar", nomParam, vlrParam);
        }

        public DataTable AtividadBuscar(ActividadReg act)
        {
            string[] nomParam = {
                "@semana"
                ,"@usr"
            };

            object[] vlrParam = 
            {
                act.Semana,
                act.Responsable,
            };

            return getDataTable("uspRegActBuscar", nomParam, vlrParam);
        }



        public bool RegActividadesRetirar(string codigo)
        {
            string[] nomParam = {
                "@codigo"
            };

            object[] vlrParam = 
            {
                codigo
            };

            return EjecutarSentencia("uspRegActividadesRetirar", nomParam, vlrParam);
        }

        public bool ActividadActualizarUsr(string xmlActividad)
        {
            string[] nomParam = {
                "@xmlReg"
            };

            object[] vlrParam = 
            {
                xmlActividad
            };

            return EjecutarSentencia("uspRegistroActividadActualizarUsr", nomParam, vlrParam);
        }

        public DataTable ActividadResponsable(string Caso)
        {
            string[] nomParam = {
                "@Caso"
            };

            object[] vlrParam = 
            {
                Caso
            };

            return getDataTable("uspRegistroActividadesAsignado", nomParam, vlrParam);

        }

        public DataTable ActividadResponsableReq(string Req)
        {
            string[] nomParam = {
                "@Req"
            };

            object[] vlrParam = 
            {
                Req
            };

            return getDataTable("uspRegistroActividadesAsignadoReq", nomParam, vlrParam);

        }

        public bool ActividadActualizar(ActividadReg act)
        {
            string[] nomParam = {
                "@Semana"
                ,"@Responsable"
                ,"@Cliente"
                ,"@Negocio"
                ,"@Linea"
                ,"@Aplicacion"
                ,"@Programa"
                ,"@CNA"
                ,"@Asunto"
                ,"@Actividad"
                ,"@Planeada"
                ,"@Fecha"
                ,"@Tiempo"
                ,"@Codigo"
                ,"@Cerrar"
                ,"@Finalizado"
                ,"@Requerimiento"
            };

            object[] vlrParam = 
            {
                act.Semana,
                act.Responsable,
                act.Cliente,
                act.Negocio,
                act.Linea,
                act.Aplicacion,
                act.Programa,
                act.CNA,
                act.Asunto,
                act.Actividad,
                act.Planeada,
                act.Fecha,
                act.Tiempo,
                act.Codigo,
                act.Borrar,
                act.Finalizado,
                act.requerimiento
            };

            return EjecutarSentencia("uspRegistroActividadActualizar", nomParam, vlrParam);
        }


        public DataTable ActividadListar()
        {
            return getDataTable("uspActividadListar");
        }
        

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataSet getDataSet(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataset(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion

    }
}
