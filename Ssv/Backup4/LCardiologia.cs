﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LCardiologia
    {

        public DataSet LosR(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select E.ATE_INS_FECHAENTRE FEC_ATENCION, D.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(D.PAC_PAC_NUMERO) PACIENTE,CON.CON_CON_DESCRIPCIO CONVENIO,C.FAC_FAC_FACTURA FACTURA,decode(E.OPA_NUMMOVIMIENTO,'             ','ADICIONADO A LA CUENTA','DISPENSADO POR ALMACEN 12' ) tipo, " +
                                 "E.ATE_INS_CODIGO CODIGO,G.FLD_PRODUCTOGLOSA INSUMO,sum(E.ATE_INS_CANTIDAD) CANTIDAD,SUM(E.ATE_INS_MONTOTARIF) VALOR_TARIFA, PRECIO.FLD_PRECIO PRECIO_UNIDAD " +
                           "from ate_insumos E, rpa_Formulario c, CON_CONVENIO CON,pac_paciente d, ABA_PRODUCTO G, aba_precioprv precio " +
                          "where trunc(E.ATE_INS_FECHAENTRE) between To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') and C.RPA_FOR_TIPOFORMU=E.ATE_INS_TIPOFORMU " +
                            "and C.RPA_FOR_NUMERFORMU=E.ATE_INS_NUMERFORMU and C.PAC_PAC_NUMERO=E.ATE_INS_NUMERPACIE and  C.CON_CON_CODIGO=E.CON_CON_CODIGO and C.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
                            "and C.PAC_PAC_NUMERO=D.PAC_PAC_NUMERO and E.ATE_INS_CODIGO=G.FLD_PRODUCTOCODIGO and E.ATE_INS_CODIGO = PRECIO.FLD_PRODUCTOCODIGO and G.GRPPRDCOD='25' " +
                            "and E.CODIGOCENTROATEN='CES  ' and E.ATE_INS_VIGENCIA<>'N' " +
                            "and (E.ATE_INS_CODIGUSUAR = 'INCASGRI' or E.ATE_INS_CODIGUSUAR = 'IJMEJMOR' or E.ATE_INS_CODIGUSUAR = 'ILOSOGIR') " +
                          "group by E.ATE_INS_FECHAENTRE, D.PAC_PAC_RUT,D.PAC_PAC_NUMERO ,CON.CON_CON_DESCRIPCIO,C.FAC_FAC_FACTURA,E.ATE_INS_CODIGO,G.FLD_PRODUCTOGLOSA, PRECIO.FLD_PRECIO,E.OPA_NUMMOVIMIENTO order by 3,1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet HonorarioQXCardio(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select A.SER_PRO_FECHAINI fecha,G.PAC_PAC_RUT identificacion,nomcompletopaciente(G.PAC_PAC_NUMERO) paciente,A.PRE_PRE_CODIGO codigo,F.PRE_PRE_DESCRIPCIO descripcion,H.CON_CON_DESCRIPCIO convenio,D.ATE_PRE_MONTOTARIF tarifa, " +
                                "CCO.EST_CEN_CODIGO CCO,trim(b.SER_PRO_RUT) RUT ,(rtrim (b.SER_PRO_APELLPATER) ||' '||  rtrim(b.SER_PRO_APELLMATER) ||' '|| rtrim(b.SER_PRO_NOMBRES)) PROFESIONAL, " +
                                "A.CNPQRGCOD Tipo,J.ATE_PRE_MONTOPAGAR Honorario ,I.CODIGOCENTROATEN centro,I.FAC_FAC_FACTURA factura " +
                            "from tabprdreldetpro a,ser_profesiona b,taborddetcir c, ate_prestacion d ,net_prepreestcen cco,pre_prestacion f,pac_paciente g,con_convenio h,rpa_formulario i, tabprdreldet j " +
                                "where A.ORDNUMERO=C.ORDNUMERO and A.SER_PRO_RUT=B.SER_PRO_RUT and trunc (A.SER_PRO_FECHAINI ) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " + 
                                    "and C.HORAFIN <> '00:00' and A.ORDNUMERO = D.ORDNUMERO and A.PRE_PRE_CODIGO = D.ATE_PRE_CODIGO and d.ate_pre_numerpacie not in ('1308','5024') AND CCO.PRE_PRE_CODIGO=D.ATE_PRE_CODIGO " +
                                    "AND CCO.EST_CEN_CODIGO in ('950     ','910     ','920     ','940     ','930     ','960     ') " +
                                    "and A.ORDNUMERO=J.ORDNUMERo(+) and A.PAC_PAC_NUMERO= J.PAC_PAC_NUMERO(+) and A.PRE_PRE_CODIGO=J.PRE_PRE_CODIGO(+) and A.CNPQRGCOD = J.CNPQRGCOD(+) " +
                                    "and D.ATE_PRE_CODIGO=F.PRE_PRE_CODIGO and A.PAC_PAC_NUMERO=G.PAC_PAC_NUMERO and D.CON_CON_CODIGO=H.CON_CON_CODIGO " +
                                    "and D.ATE_PRE_VIGENCIA<>'N' and D.ORDNUMERO<>' ' and D.ATE_PRE_TIPOFORMU=I.RPA_FOR_TIPOFORMU and D.ATE_PRE_NUMERFORMU=I.RPA_FOR_NUMERFORMU " +
                            "group by   A.SER_PRO_FECHAINI,G.PAC_PAC_RUT,G.PAC_PAC_NUMERO,A.PRE_PRE_CODIGO,F.PRE_PRE_DESCRIPCIO,D.ATE_PRE_MONTOTARIF,CCO.EST_CEN_CODIGO,b.SER_PRO_RUT, " +
                                   "b.SER_PRO_NOMBRES,b.SER_PRO_APELLPATER,b.SER_PRO_APELLMATER, " +
                                   "A.CNPQRGCOD,H.CON_CON_DESCRIPCIO,I.FAC_FAC_FACTURA,I.CODIGOCENTROATEN,J.ATE_PRE_MONTOPAGAR " +
                            "order by b.SER_PRO_RUT,  A.SER_PRO_FECHAINI ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet HonorarioNOQXCardio(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select A.ATE_PRE_FECHAENTRE FECHA,G.PAC_PAC_RUT IDENTIFICACION,NOMCOMPLETOPACIENTE(A.ATE_PRE_NUMERPACIE) PACIENTE,A.ATE_PRE_CODIGO CODIGO,F.PRE_PRE_DESCRIPCIO DESCRIPCION,H.CON_CON_DESCRIPCIO CONVENIO,A.ATE_PRE_MONTOTARIF TARIFA, " +
                                  "CCO.EST_CEN_CODIGO CCO,trim(D.SER_PRO_RUT) RUT ,(rtrim (d.SER_PRO_APELLPATER) ||' '||  rtrim(d.SER_PRO_APELLMATER) ||' '|| rtrim(d.SER_PRO_NOMBRES)) PROFESIONAL ,I.CODIGOCENTROATEN CENTRO,I.FAC_FAC_FACTURA FACTURA " +
                              "from ate_prestacion a,con_convenio b,tabsoccop c,ser_profesiona d ,net_prepreestcen cco,pre_prestacion f,pac_paciente g,con_convenio h,rpa_formulario i " +
                            "where c.SOCCOD=a.SOCCOD and b.CON_CON_CODIGO=a.CON_CON_CODIGO and a.ATE_PRE_PROFERESPO = d.SER_PRO_RUT (+) " +
                                "and a.ate_pre_numerpacie not in ('1308','5024') and trunc (a.ATE_PRE_FECHAENTRE) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                "AND A.ATE_PRE_CODIGO=F.PRE_PRE_CODIGO AND F.PRE_PRE_CODIGO=CCO.PRE_PRE_CODIGO AND A.ATE_PRE_NUMERPACIE=G.PAC_PAC_NUMERO " +
                                "AND A.CON_CON_CODIGO=H.CON_CON_CODIGO AND A.ATE_PRE_TIPOFORMU=I.RPA_FOR_TIPOFORMU  and A.ORDNUMERO=' ' " +
                                "AND A.ATE_PRE_NUMERFORMU=I.RPA_FOR_NUMERFORMU AND A.ATE_PRE_VIGENCIA<>'N' " +
                                "AND CCO.EST_CEN_CODIGO in ('950     ','910     ','920     ','940     ','930     ','960     ') " +                                        
                         "union all " +
                            "select  A.ATE_PRE_FECHAENTRE,G.PAC_PAC_RUT,NOMCOMPLETOPACIENTE(A.ATE_PRE_NUMERPACIE),A.ATE_PRE_CODIGO,F.PRE_PRE_DESCRIPCIO,H.CON_CON_DESCRIPCIO,A.ATE_PRE_MONTOTARIF, " +
                                      "CCO.EST_CEN_CODIGO,trim(D.SER_PRO_RUT) RUT ,(rtrim (d.SER_PRO_APELLPATER) ||' '||  rtrim(d.SER_PRO_APELLMATER) ||' '|| rtrim(d.SER_PRO_NOMBRES)) PROFESIONAL ,I.CODIGOCENTROATEN,I.FAC_FAC_FACTURA " +   
                              "from ate_prestacion a,con_convenio b,ser_profesiona d ,net_prepreestcen cco,pre_prestacion f,pac_paciente g,con_convenio h,rpa_formulario i " +
                            "where b.CON_CON_CODIGO=a.CON_CON_CODIGO and a.ATE_PRE_PROFERESPO = d.SER_PRO_RUT(+) and a.ate_pre_numerpacie not in ('1308','5024') " +
                                            "and trunc (a.ATE_PRE_FECHAENTRE) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                            "AND A.ATE_PRE_CODIGO=F.PRE_PRE_CODIGO AND F.PRE_PRE_CODIGO=CCO.PRE_PRE_CODIGO AND A.ATE_PRE_NUMERPACIE=G.PAC_PAC_NUMERO " +
                                            "AND A.CON_CON_CODIGO=H.CON_CON_CODIGO AND A.ATE_PRE_TIPOFORMU=I.RPA_FOR_TIPOFORMU AND A.ATE_PRE_NUMERFORMU=I.RPA_FOR_NUMERFORMU " +
                                            "and A.ATE_PRE_CODIGO IN ('890202CA','890602CA','890402CA','890302CA','890702CA') AND A.ATE_PRE_VIGENCIA<>'N' and A.ORDNUMERO=' ' " +
                         "group by  A.ATE_PRE_FECHAENTRE,G.PAC_PAC_RUT,NOMCOMPLETOPACIENTE(A.ATE_PRE_NUMERPACIE),A.ATE_PRE_CODIGO,F.PRE_PRE_DESCRIPCIO,H.CON_CON_DESCRIPCIO,A.ATE_PRE_MONTOTARIF, " +
                                "CCO.EST_CEN_CODIGO, (D.SER_PRO_RUT), (d.SER_PRO_APELLPATER) , (d.SER_PRO_APELLMATER), (d.SER_PRO_NOMBRES) ,I.CODIGOCENTROATEN,I.FAC_FAC_FACTURA "; 

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet MedInsPresCardioFacyNoFac(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select C.RPA_FOR_FECHATENCION FECHA,TIP.PRE_TIP_DESCRIPCIO tipo,  D.PAC_PAC_RUT identificacion,  NomCompletoPaciente(D.PAC_PAC_NUMERO) PACIENTE, A.ATE_PRE_CODIGO codigo, " +
                              "B.PRE_PRE_DESCRIPCIO descripcion, A.CON_CON_CODIGO Cnv, GlbGetNomCnv(A.CON_CON_CODIGO) Convenio, CON.CON_EMP_RUT RUT, " +
                              "sum(A.ATE_PRE_CANTIDAD) CANTIDAD, sum(A.ATE_PRE_MONTOTARIF) TARIFA , " +
                              "sum(A.ATE_PRE_MONTOPAGAR) PAGO_PACIENTE, DECODE(TRIM(A.ATE_PRE_CONCEPAGO ),'TP', sum( a.ATE_pre_MONTOPAGAR) ,(sum(A.ATE_PRE_MONTOTARIF) -sum( a.ATE_pre_MONTOPAGAR) )) Total_pagar, " +
                              "GRP.FAC_FAC_FACTURA FACTURA, FAC.FAC_FAC_VALORFACT VALORFACT, FAC.FAC_FAC_FECHAFACTURACION fec_factura " +
                          "from rpa_Formulario C,  ate_prestacion A, pre_prestacion B, PRE_TIPO tip, CON_CONVENIO CON, pac_paciente D, tabfctgrp grp, TABFCTEMP FAC " +
                          "where  C.RPA_FOR_NUMERFORMU = A.ATE_PRE_NUMERFORMU AND A.ATE_PRE_TIPOFORMU = C.RPA_FOR_TIPOFORMU and A.ATE_PRE_CODIGO = B.PRE_PRE_CODIGO " +
                             "and B.PRE_PRE_TIPO=TIP.PRE_TIP_TIPO and A.ATE_PRE_NUMERpacie = C.PAC_PAC_NUMERO and c.PAC_PAC_NUMERO not in ('1308','5024') AND A.CON_CON_CODIGO = CON.CON_CON_CODIGO " +
                             "And trunc(C.RPA_FOR_FECHATENCION) between    To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') and (B.PRE_PRE_TIPO ='0039' OR B.PRE_PRE_TIPO='0043') " +
                             "and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO AND TRIM(D.PAC_PAC_RUT)<>'10' AND A.ATE_PRE_NUMERPACIE = GRP.PAC_PAC_NUMERO(+) " +
                             "AND A.ATE_PRE_TIPOFORMU = GRP.RPA_FOR_TIPOFORMU(+) AND A.ATE_PRE_NUMERFORMU = GRP.RPA_FOR_NUMERFORMU(+) AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) " +
                        "group by NomCompletoPaciente(D.PAC_PAC_NUMERO),A.ATE_PRE_CODIGO, TIP.PRE_TIP_DESCRIPCIO,B.PRE_PRE_DESCRIPCIO,A.CON_CON_CODIGO , GlbGetNomCnv(A.CON_CON_CODIGO) , " +
                               "CON.CON_EMP_RUT,C.rpa_For_Urgencia,GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,A.ATE_PRE_CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION " +       
                        "union all " +
                        "select C.RPA_FOR_FECHATENCION,'Medicamentos e insumos', D.PAC_PAC_RUT, NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO,G.FLD_PRODUCTOGLOSA, E.CON_CON_CODIGO Cnv, " +
                              "GlbGetNomCnv(E.CON_CON_CODIGO) Convenio,CON.CON_EMP_RUT RUT,  sum(E.ATE_INS_CANTIDAD), " +
                              "sum(E.ATE_INS_MONTOTARIF ), sum(E.ATE_INS_MONTOPAGAR ), DECODE(TRIM(E.CONCEPAGO),'TP', sum( E.ATE_INS_MONTOPAGAR) ,(sum(E.ATE_INS_MONTOTARIF) -sum( E.ATE_INS_MONTOPAGAR) )), " +
                              "GRP.FAC_FAC_FACTURA, FAC.FAC_FAC_VALORFACT, FAC.FAC_FAC_FECHAFACTURACION " +
                        "from tabfctgrp grp, ate_insumos E, rpa_Formulario c, CON_CONVENIO CON, OPA_DETMOV H, pac_paciente d, ABA_PRODUCTO G, TABFCTEMP FAC " +
                        "where   trunc (C.RPA_FOR_FECHATENCION) between  To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd')  and E.ATE_INS_TIPOFORMU = grp.RPA_FOR_TIPOFORMU(+) " +
                            "and E.ATE_INS_NUMERFORMU = grp.RPA_FOR_NUMERFORMU(+) AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) And c.RPA_FOR_TIPOFORMU = E.ATE_INS_TIPOFORMU " +
                            "and c.RPA_FOR_NUMERFORMU = E.ATE_INS_NUMERFORMU and E.ATE_INS_NUMERPACIE = c.PAC_PAC_NUMERO and c.PAC_PAC_NUMERO not in ('1308','5024') AND E.OPA_NUMMOVIMIENTO = H.OPA_NUMMOVIMIENTO " +
                            "and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO AND C.CON_CON_CODIGO=CON.CON_CON_CODIGO AND E.ATE_INS_CODIGO = H.FLD_PRODUCTOCODIGO " +
                            "AND H.OPA_BODEGACODIGO ='ALMACEN 12' and trim(C.CODIGOCENTROATEN) = 'CES'  AND E.ATE_INS_CODIGO = G.FLD_PRODUCTOCODIGO " +
                        "group by NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO,G.FLD_PRODUCTOGLOSA,E.CON_CON_CODIGO , GlbGetNomCnv(E.CON_CON_CODIGO) , " +
                               "CON.CON_EMP_RUT, c.rpa_For_Urgencia, GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,E.CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION " +  
                        "UNION ALL " +
                        "select C.RPA_FOR_FECHATENCION,'Medicamentos e insumos', D.PAC_PAC_RUT, NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO, G.FLD_PRODUCTOGLOSA, E.CON_CON_CODIGO Cnv, " +
                             "GlbGetNomCnv(E.CON_CON_CODIGO) Convenio, CON.CON_EMP_RUT RUT,sum(E.ATE_INS_CANTIDAD), " +
                             "sum(E.ATE_INS_MONTOTARIF ), sum(E.ATE_INS_MONTOPAGAR ), DECODE(TRIM(E.CONCEPAGO),'TP', sum( E.ATE_INS_MONTOPAGAR) ,(sum(E.ATE_INS_MONTOTARIF) -sum( E.ATE_INS_MONTOPAGAR) )), " +
                             "GRP.FAC_FAC_FACTURA, FAC.FAC_FAC_VALORFACT, FAC.FAC_FAC_FECHAFACTURACION " +
                        "from tabfctgrp grp, ate_insumos E, rpa_Formulario c, CON_CONVENIO CON, pac_paciente d, ABA_PRODUCTO G,TABFCTEMP FAC " +
                        "where trunc (C.RPA_FOR_FECHATENCION) between  To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') " +
                            "and E.ATE_INS_TIPOFORMU = grp.RPA_FOR_TIPOFORMU(+) and E.ATE_INS_NUMERFORMU = grp.RPA_FOR_NUMERFORMU(+) AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) " +
                            "And c.RPA_FOR_TIPOFORMU = E.ATE_INS_TIPOFORMU and c.RPA_FOR_NUMERFORMU = E.ATE_INS_NUMERFORMU and c.PAC_PAC_NUMERO not in ('1308','5024') and E.ATE_INS_NUMERPACIE = c.PAC_PAC_NUMERO " +
                            "AND (E.OPA_NUMMOVIMIENTO)='             '  and E.ATE_INS_CODIGUSUAR in('INCASGRI', 'IJMEJMOR','ILOSOGIR') and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO " +
                            "AND C.CON_CON_CODIGO=CON.CON_CON_CODIGO and trim(C.CODIGOCENTROATEN) = 'CES' AND E.ATE_INS_CODIGO = G.FLD_PRODUCTOCODIGO " +
                        "group by NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO,G.FLD_PRODUCTOGLOSA,E.CON_CON_CODIGO , GlbGetNomCnv(E.CON_CON_CODIGO) , " +
                                      "CON.CON_EMP_RUT, c.rpa_For_Urgencia, GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,E.CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION " +
                        "order by 1,4,2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet InsPresCardioFacturadas(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select C.RPA_FOR_FECHATENCION FECHA,TIP.PRE_TIP_DESCRIPCIO tipo, D.PAC_PAC_RUT identificacion, NomCompletoPaciente(D.PAC_PAC_NUMERO) PACIENTE, A.ATE_PRE_CODIGO codigo, " +
                               "B.PRE_PRE_DESCRIPCIO descripcion, A.CON_CON_CODIGO Cnv, GlbGetNomCnv(A.CON_CON_CODIGO) Convenio, CON.CON_EMP_RUT RUT, sum(A.ATE_PRE_CANTIDAD) CANTIDAD, " +
                               "sum(A.ATE_PRE_MONTOTARIF) TARIFA , sum(A.ATE_PRE_MONTOPAGAR) PAGO_paciente, " +
                               "DECODE(TRIM(A.ATE_PRE_CONCEPAGO ),'TP', sum( a.ATE_pre_MONTOPAGAR) ,(sum(A.ATE_PRE_MONTOTARIF) -sum( a.ATE_pre_MONTOPAGAR) )) Total_pagar, " +
                               "GRP.FAC_FAC_FACTURA FACTURA, FAC.FAC_FAC_VALORFACT VALORFACT, FAC.FAC_FAC_FECHAFACTURACION fec_factura " +
                        "from rpa_Formulario C, ate_prestacion A, pre_prestacion B, PRE_TIPO tip, CON_CONVENIO CON, pac_paciente D, tabfctgrp grp, TABFCTEMP FAC " +
                        "where  C.RPA_FOR_NUMERFORMU = A.ATE_PRE_NUMERFORMU AND A.ATE_PRE_TIPOFORMU = C.RPA_FOR_TIPOFORMU and   A.ATE_PRE_CODIGO = B.PRE_PRE_CODIGO " +
                            "and B.PRE_PRE_TIPO=TIP.PRE_TIP_TIPO and    A.ATE_PRE_NUMERpacie = C.PAC_PAC_NUMERO AND A.CON_CON_CODIGO = CON.CON_CON_CODIGO " +
                            "And trunc(FAC.FAC_FAC_FECHAFACTURACION ) between To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd')  AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) " +
                            "and (B.PRE_PRE_TIPO ='0039' OR B.PRE_PRE_TIPO='0043') and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO and c.PAC_PAC_NUMERO not in ('1308','5024') " +
                            "AND A.ATE_PRE_NUMERPACIE = GRP.PAC_PAC_NUMERO(+) AND A.ATE_PRE_TIPOFORMU = GRP.RPA_FOR_TIPOFORMU(+) AND A.ATE_PRE_NUMERFORMU = GRP.RPA_FOR_NUMERFORMU(+) " +
                        "group by NomCompletoPaciente(D.PAC_PAC_NUMERO),A.ATE_PRE_CODIGO, TIP.PRE_TIP_DESCRIPCIO,B.PRE_PRE_DESCRIPCIO,A.CON_CON_CODIGO , GlbGetNomCnv(A.CON_CON_CODIGO) ,  " +
                                    "CON.CON_EMP_RUT,C.rpa_For_Urgencia,GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,A.ATE_PRE_CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION " +
                        "union all " +
                        "select C.RPA_FOR_FECHATENCION,'Medicamentos e insumos', D.PAC_PAC_RUT, NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO, G.FLD_PRODUCTOGLOSA, E.CON_CON_CODIGO Cnv, " +
                                  "GlbGetNomCnv(E.CON_CON_CODIGO) Convenio, CON.CON_EMP_RUT RUT, sum(E.ATE_INS_CANTIDAD), sum(E.ATE_INS_MONTOTARIF ), sum(E.ATE_INS_MONTOPAGAR ), " +
                                  "DECODE(TRIM(E.CONCEPAGO),'TP', sum( E.ATE_INS_MONTOPAGAR) ,(sum(E.ATE_INS_MONTOTARIF) -sum( E.ATE_INS_MONTOPAGAR) )), GRP.FAC_FAC_FACTURA, FAC.FAC_FAC_VALORFACT,  FAC.FAC_FAC_FECHAFACTURACION " +
                        "from tabfctgrp grp, ate_insumos E, rpa_Formulario c, CON_CONVENIO CON, OPA_DETMOV H, pac_paciente d, ABA_PRODUCTO G, TABFCTEMP FAC " +
                        "where trunc (FAC.FAC_FAC_FECHAFACTURACION ) between  To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') and E.ATE_INS_TIPOFORMU = grp.RPA_FOR_TIPOFORMU(+) " +
                                  "and E.ATE_INS_NUMERFORMU = grp.RPA_FOR_NUMERFORMU(+) AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) And c.RPA_FOR_TIPOFORMU = E.ATE_INS_TIPOFORMU " +
                                  "and c.RPA_FOR_NUMERFORMU = E.ATE_INS_NUMERFORMU and E.ATE_INS_NUMERPACIE = c.PAC_PAC_NUMERO AND E.OPA_NUMMOVIMIENTO = H.OPA_NUMMOVIMIENTO " +
                                  "and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO and c.PAC_PAC_NUMERO not in ('1308','5024') AND C.CON_CON_CODIGO=CON.CON_CON_CODIGO AND E.ATE_INS_CODIGO = H.FLD_PRODUCTOCODIGO " +
                                  "AND H.OPA_BODEGACODIGO ='ALMACEN 12' and trim(C.CODIGOCENTROATEN) = 'CES' AND E.ATE_INS_CODIGO = G.FLD_PRODUCTOCODIGO " +
                        "group by NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO,G.FLD_PRODUCTOGLOSA,E.CON_CON_CODIGO , GlbGetNomCnv(E.CON_CON_CODIGO) , " +
                                      "CON.CON_EMP_RUT, c.rpa_For_Urgencia, GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,E.CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION " +      
                        "UNION ALL " +
                        "select C.RPA_FOR_FECHATENCION,'Medicamentos e insumos', D.PAC_PAC_RUT, NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO, G.FLD_PRODUCTOGLOSA, E.CON_CON_CODIGO Cnv,  " +
                                 "GlbGetNomCnv(E.CON_CON_CODIGO) Convenio, CON.CON_EMP_RUT RUT, sum(E.ATE_INS_CANTIDAD), sum(E.ATE_INS_MONTOTARIF ), sum(E.ATE_INS_MONTOPAGAR ), " +
                                 "DECODE(TRIM(E.CONCEPAGO),'TP', sum( E.ATE_INS_MONTOPAGAR) ,(sum(E.ATE_INS_MONTOTARIF) -sum( E.ATE_INS_MONTOPAGAR) )),GRP.FAC_FAC_FACTURA,FAC.FAC_FAC_VALORFACT, FAC.FAC_FAC_FECHAFACTURACION " +
                        "from tabfctgrp grp, ate_insumos E, rpa_Formulario c, CON_CONVENIO CON, pac_paciente d, ABA_PRODUCTO G,TABFCTEMP FAC " +
                        "where trunc (FAC.FAC_FAC_FECHAFACTURACION ) between  To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') and E.ATE_INS_TIPOFORMU = grp.RPA_FOR_TIPOFORMU(+) AND E.ATE_INS_CODIGO = G.FLD_PRODUCTOCODIGO " +
                                    "and E.ATE_INS_NUMERFORMU = grp.RPA_FOR_NUMERFORMU(+)  AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) And c.RPA_FOR_TIPOFORMU = E.ATE_INS_TIPOFORMU " +
                                    "and c.RPA_FOR_NUMERFORMU = E.ATE_INS_NUMERFORMU and E.ATE_INS_NUMERPACIE = c.PAC_PAC_NUMERO and c.PAC_PAC_NUMERO not in ('1308','5024') AND (E.OPA_NUMMOVIMIENTO)='             ' " +
                                    "and E.ATE_INS_CODIGUSUAR in('INCASGRI', 'IJMEJMOR','ILOSOGIR') and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO AND C.CON_CON_CODIGO=CON.CON_CON_CODIGO and trim(C.CODIGOCENTROATEN) = 'CES' " +
                        "group by NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO,G.FLD_PRODUCTOGLOSA,E.CON_CON_CODIGO , GlbGetNomCnv(E.CON_CON_CODIGO) , " +
                                      "CON.CON_EMP_RUT, c.rpa_For_Urgencia, GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,E.CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION " +
                        "order by 2,4 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet CarteraCardiologia(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select  Cnv, Convenio, Facturac Factura, FechaFact FEC_FACTURA, FechaRadi fec_radicada,Vlt_Total, valorcardio, Abonos,Notas_Credito,Notas_Debito, xNombre paciente, Nit, Nombre_Empresa, Saldo, Fechacorte from " +
                        "(Select Convenio Cnv, Nombre_Convenio Convenio, ' ' Producto,  ' ' Operacion, Facturac,  Fecha_Facturacion FechaFact, Fecha_Radicacion FechaRadi, ValorFactura Vlt_Total, " +
                               "Abonos, NotasCredito Notas_Credito, NotasDebito Notas_Debito,NombrePaciente xNombre, Nit, Nombre_Empresa, Saldo, '" + FechaFin + "' Fechacorte " +
                        "From ( select A.con_emp_rut Nit, " +
                                       "( Select Con_Emp_Descripcio From Con_empresa where Con_Emp_Rut = A.Con_Emp_Rut ) Nombre_Empresa, " +
                                       "A.Con_Con_Codigo Convenio, (select Con_Con_Descripcio from con_convenio where Con_Emp_Rut = A.Con_Emp_Rut And Con_Con_Codigo = A.Con_Con_Codigo ) Nombre_Convenio, " +
                                       "A.Fac_Fac_Factura Facturac, ( Select Pac_Pac_Rut from Pac_Paciente where Pac_Pac_Numero = A.Pac_Pac_Numero ) Paciente, " +
                                       "(Select Trim(Pac_Pac_ApellPater) || ' ' || Trim(Pac_Pac_ApellMater) || ' ' || Trim(Pac_Pac_Nombre) from Pac_Paciente where Pac_Pac_Numero =  " +
                                                "(Select Pac_Pac_Numero from TabFctEmp where Fac_Fac_Factura = H.Fld_Crd_Num_Car_Cre)) NombrePaciente,  A.Fac_Fac_FechaFacturacion Fecha_Facturacion, " +
                                       "H.FLD_COL_FEC_OTOR Fecha_Radicacion, A.Fac_Fac_Valorfact ValorFactura, Nvl(( Select sum(Fld_Col_Cap_Pag) " +
                                "from can B  where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd')  and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) Abonos, " +
                                        "Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 " +
                                                           "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasCredito, " +
                                        "Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant)  from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 " +
                                                           "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasDebito, " +
                                       "A.Fac_Fac_Valorfact - Nvl(( Select sum(Fld_Col_Cap_Pag) from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                           "and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) - Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num " +
                                                                 "And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) " +
                                                                 "+ Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 " +
                                                                 "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd')  and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) Saldo " +
                                "from TabFctEmp A, CRTCOL H where A.Fac_Fac_FechaFacturacion >= To_Date('1990/01/01','yyyy/mm/dd') and A.Fac_Fac_FechaFacturacion < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 " +
                                        "and A.Fac_Fac_Factura = H.Fld_Crd_Num_Car_Cre and H.CrtEstCod <> '7' AND   NOT EXISTS (SELECT 1 FROM Fac_eventos EVTO  WHERE EVTO.FAC_FAC_FACTURA = A.Fac_Fac_Factura " +
                                                     "AND EVTO.FAC_EVE_FECHAEVENTO >= TO_DATE(SUBSTR('1990/01/01',1,10),'YYYY/MM/DD') AND EVTO.FAC_EVE_FECHAEVENTO < TO_DATE(SUBSTR('" + FechaFin + "',1,10),'YYYY/MM/DD') + 1 AND EVTO.ESTADO = 'A') " +
                                        "and A.Fac_Fac_ValorFact - Nvl(( Select sum(Fld_Col_Cap_Pag) from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                      "and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) - Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act)  from EVE C where C.Fld_Col_Num = H.Fld_Col_Num " +
                                                              "And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd')  and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) " +
                                                        "+ Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 " +
                                                              "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)     > 0 " +
                                "union " +
                                "select A.con_emp_rut Nit, (Select Con_Emp_Descripcio From Con_empresa where Con_Emp_Rut = A.Con_Emp_Rut ) Nombre_Empresa, A.Con_Con_Codigo Convenio, " +
                                       "( select Con_Con_Descripcio from con_convenio where Con_Emp_Rut = A.Con_Emp_Rut And Con_Con_Codigo = A.Con_Con_Codigo ) Nombre_Convenio, " +
                                       "A.Fac_Fac_Factura Facturac,' ' Paciente,' ' NombrePaciente, A.Fac_Fac_FechaFacturacion Fecha_Facturacion,  H.FLD_COL_FEC_OTOR Fecha_Radicacion, A.Mac_Fac_Valor ValorFactura, " +
                                       "Nvl(( Select sum(Fld_Col_Cap_Pag) from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                             "and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) Abonos, Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num " +
                                                        "And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasCredito, " +
                                            "Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 " +
                                                   "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasDebito,A.Mac_Fac_Valor - Nvl(( Select sum(Fld_Col_Cap_Pag) " +
                                                      "from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd')  and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) " +
                                                      "- Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 " +
                                                              "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)  + Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) " +
                                                                "from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                              "and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) Saldo  from TabCarMasFac A, CRTCOL H where  A.Fac_Fac_FechaFacturacion >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                    "and A.Fac_Fac_FechaFacturacion < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 and A.Fac_Fac_Factura = H.Fld_Crd_Num_Car_Cre and H.CrtEstCod <> '7' and A.Mac_Fac_Valor - Nvl(( Select sum(Fld_Col_Cap_Pag) " +
                                                        "from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd') and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) " +
                                                      "- Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act)  from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 " +
                                                       "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) + Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) " +
                                                             "from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                              "and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)     > 0 " +
                                "union " +
                                "select A.con_emp_rut Nit,( Select Con_Emp_Descripcio From Con_empresa where Con_Emp_Rut = A.Con_Emp_Rut ) Nombre_Empresa, A.Con_Con_Codigo Convenio, " +
                                       "( select Con_Con_Descripcio from con_convenio where Con_Emp_Rut = A.Con_Emp_Rut And Con_Con_Codigo = A.Con_Con_Codigo ) Nombre_Convenio, " +
                                       "A.Fac_Fac_Factura Facturac, ( Select Pac_Pac_Rut from Pac_Paciente where Pac_Pac_Numero = A.Pac_Pac_Numero ) Paciente, " +
                                       "(Select Trim(Pac_Pac_ApellPater) || ' ' || Trim(Pac_Pac_ApellMater) || ' ' || Trim(Pac_Pac_Nombre) from Pac_Paciente where Pac_Pac_Numero = A.Pac_Pac_Numero ) NombrePaciente, " +
                                       "( A.Fac_Fac_FechaFacturacion ) Fecha_Facturacion,To_Date('1900/01/01','YYYY/MM/DD') Fecha_Radicacion,A.Fac_Fac_Valorfact ValorFactura, 0 Abonos, " +
                                       "Nvl(( Select SUM(FAC_VALORANTFACT) from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA and C.NTATPO = 'N' and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                       "and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)    NotasCredito, Nvl(( Select SUM(FAC_VALORANTFACT) from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA " +
                                       "and C.NTATPO = 'O'  and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasDebito, A.Fac_Fac_Valorfact " +
                                      "- Nvl(( Select SUM(FAC_VALORANTFACT)  from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA and C.NTATPO = 'N' and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                "and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) + Nvl(( Select SUM(FAC_VALORANTFACT) from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA " +
                                                "and C.NTATPO = 'O' and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd')  and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) Saldo " +
                                "from TabFctEmp A " +
                                "where  A.Fac_Fac_FechaFacturacion >= To_Date('1990/01/01','yyyy/mm/dd') and A.Fac_Fac_FechaFacturacion < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 and A.ESTADO <> 'A' " +
                                "AND NOT EXISTS (SELECT 1 FROM Fac_eventos EVTO WHERE EVTO.FAC_FAC_FACTURA = A.Fac_Fac_Factura AND EVTO.FAC_EVE_FECHAEVENTO >= TO_DATE(SUBSTR('1990/01/01',1,10),'YYYY/MM/DD') " +
                                                            "AND EVTO.FAC_EVE_FECHAEVENTO < TO_DATE(SUBSTR('" + FechaFin + "',1,10),'YYYY/MM/DD') + 1 AND EVTO.ESTADO = 'A') " +
                                "And Not Exists ( Select 1 From CrtCol where Fld_Crd_Num_Car_Cre = A.Fac_Fac_Factura) and A.Fac_Fac_ValorFact  - Nvl(( Select SUM(FAC_VALORANTFACT) from TABFCTNTA C " +
                                                        "where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA and C.NTATPO = 'N' and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) " +
                                                      "+ Nvl(( Select SUM(FAC_VALORANTFACT)  from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA and C.NTATPO = 'O' " +
                                                       "and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)     > 0)), " +
                        "(select distinct factura,sum(valor) valorcardio from  " +
                        "(Select C.RPA_FOR_FECHATENCION FECHA,TIP.PRE_TIP_DESCRIPCIO, D.PAC_PAC_RUT, NomCompletoPaciente(D.PAC_PAC_NUMERO) PACIENTE,A.ATE_PRE_CODIGO, B.PRE_PRE_DESCRIPCIO, A.CON_CON_CODIGO Cnv, " +
                               "GlbGetNomCnv(A.CON_CON_CODIGO) Convenio, CON.CON_EMP_RUT RUT, decode (C.rpa_For_Urgencia ,'H','Hospitalizacion','S','Urgencias','C.Externa') Ambito, sum(A.ATE_PRE_CANTIDAD) CANTIDAD, " +
                               "sum(A.ATE_PRE_MONTOTARIF) TARIFA , sum(A.ATE_PRE_MONTOPAGAR) PAGO1, DECODE(TRIM(A.ATE_PRE_CONCEPAGO ),'TP', sum( a.ATE_pre_MONTOPAGAR) ,(sum(A.ATE_PRE_MONTOTARIF) -sum( a.ATE_pre_MONTOPAGAR) )) valor, " +
                               "GRP.FAC_FAC_FACTURA FACTURA, FAC.FAC_FAC_VALORFACT,  FAC.FAC_FAC_FECHAFACTURACION,'1' " +
                        "from rpa_Formulario C, ate_prestacion A, pre_prestacion B, PRE_TIPO tip,CON_CONVENIO CON, pac_paciente D, tabfctgrp grp,TABFCTEMP FAC " +
                        "where  C.RPA_FOR_NUMERFORMU = A.ATE_PRE_NUMERFORMU AND A.ATE_PRE_TIPOFORMU = C.RPA_FOR_TIPOFORMU and  A.ATE_PRE_CODIGO = B.PRE_PRE_CODIGO and B.PRE_PRE_TIPO=TIP.PRE_TIP_TIPO " +
                            "and  A.ATE_PRE_NUMERpacie = C.PAC_PAC_NUMERO AND A.CON_CON_CODIGO = CON.CON_CON_CODIGO And  trunc(FAC.FAC_FAC_FECHAFACTURACION ) between    To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') " +
                            "and (B.PRE_PRE_TIPO ='0039' OR B.PRE_PRE_TIPO='0043') and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO AND TRIM(D.PAC_PAC_RUT) NOT IN ('10','12') AND A.ATE_PRE_NUMERPACIE = GRP.PAC_PAC_NUMERO(+) " +
                            "AND A.ATE_PRE_TIPOFORMU = GRP.RPA_FOR_TIPOFORMU(+) AND A.ATE_PRE_NUMERFORMU = GRP.RPA_FOR_NUMERFORMU(+) AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) " +
                        "group by NomCompletoPaciente(D.PAC_PAC_NUMERO),A.ATE_PRE_CODIGO, TIP.PRE_TIP_DESCRIPCIO,B.PRE_PRE_DESCRIPCIO,A.CON_CON_CODIGO , GlbGetNomCnv(A.CON_CON_CODIGO) , " +
                                    "CON.CON_EMP_RUT,C.rpa_For_Urgencia,GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,A.ATE_PRE_CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION " +
                        "union all " +
                        "select C.RPA_FOR_FECHATENCION,'Medicamentos e insumos', D.PAC_PAC_RUT, NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO, G.FLD_PRODUCTOGLOSA, E.CON_CON_CODIGO Cnv, " +
                                  "GlbGetNomCnv(E.CON_CON_CODIGO) Convenio, CON.CON_EMP_RUT RUT, decode (c.rpa_For_Urgencia ,'H','Hospitalizacion','S','Urgencias','C.Externa') Ambito, sum(E.ATE_INS_CANTIDAD), " +
                                  "sum(E.ATE_INS_MONTOTARIF ),sum(E.ATE_INS_MONTOPAGAR ), DECODE(TRIM(E.CONCEPAGO),'TP', sum( E.ATE_INS_MONTOPAGAR) ,(sum(E.ATE_INS_MONTOTARIF) -sum( E.ATE_INS_MONTOPAGAR) )) valor, " +
                                  "GRP.FAC_FAC_FACTURA, FAC.FAC_FAC_VALORFACT, FAC.FAC_FAC_FECHAFACTURACION,'2' " +
                        "from tabfctgrp grp, ate_insumos E, rpa_Formulario c,CON_CONVENIO CON, OPA_DETMOV H,pac_paciente d, ABA_PRODUCTO G,TABFCTEMP FAC " +
                        "where trunc (FAC.FAC_FAC_FECHAFACTURACION ) between  To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') and E.ATE_INS_TIPOFORMU = grp.RPA_FOR_TIPOFORMU(+) " +
                                  "and E.ATE_INS_NUMERFORMU = grp.RPA_FOR_NUMERFORMU(+) AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) And c.RPA_FOR_TIPOFORMU = E.ATE_INS_TIPOFORMU " +
                                  "and c.RPA_FOR_NUMERFORMU = E.ATE_INS_NUMERFORMU and E.ATE_INS_NUMERPACIE = c.PAC_PAC_NUMERO AND TRIM(D.PAC_PAC_RUT) NOT IN ('10','12') AND E.OPA_NUMMOVIMIENTO = H.OPA_NUMMOVIMIENTO " +
                                  "and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO AND C.CON_CON_CODIGO=CON.CON_CON_CODIGO AND E.ATE_INS_CODIGO = H.FLD_PRODUCTOCODIGO AND H.OPA_BODEGACODIGO ='ALMACEN 12' " +
                                  "and trim(C.CODIGOCENTROATEN) = 'CES' AND E.ATE_INS_CODIGO = G.FLD_PRODUCTOCODIGO " +
                        "group by NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO,G.FLD_PRODUCTOGLOSA,E.CON_CON_CODIGO , GlbGetNomCnv(E.CON_CON_CODIGO) , " +
                                      "CON.CON_EMP_RUT, c.rpa_For_Urgencia, GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,E.CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION  " +      
                        "UNION ALL " +
                        "select C.RPA_FOR_FECHATENCION,'Medicamentos e insumos', D.PAC_PAC_RUT, NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO,G.FLD_PRODUCTOGLOSA, E.CON_CON_CODIGO Cnv, " +
                                  "GlbGetNomCnv(E.CON_CON_CODIGO) Convenio,CON.CON_EMP_RUT RUT, decode (c.rpa_For_Urgencia ,'H','Hospitalizacion','S','Urgencias','C.Externa') Ambito, sum(E.ATE_INS_CANTIDAD), " +
                                  "sum(E.ATE_INS_MONTOTARIF ), sum(E.ATE_INS_MONTOPAGAR ),DECODE(TRIM(E.CONCEPAGO),'TP', sum( E.ATE_INS_MONTOPAGAR) ,(sum(E.ATE_INS_MONTOTARIF) -sum( E.ATE_INS_MONTOPAGAR) )) valor, " +
                                  "GRP.FAC_FAC_FACTURA, FAC.FAC_FAC_VALORFACT,FAC.FAC_FAC_FECHAFACTURACION,'3'  " +
                        "from tabfctgrp grp, ate_insumos E, rpa_Formulario c, CON_CONVENIO CON, pac_paciente d, ABA_PRODUCTO G,TABFCTEMP FAC " +
                        "where trunc (FAC.FAC_FAC_FECHAFACTURACION ) between  To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') and E.ATE_INS_TIPOFORMU = grp.RPA_FOR_TIPOFORMU(+) " +
                                  "and E.ATE_INS_NUMERFORMU = grp.RPA_FOR_NUMERFORMU(+) AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) And c.RPA_FOR_TIPOFORMU = E.ATE_INS_TIPOFORMU " +
                                  "and c.RPA_FOR_NUMERFORMU = E.ATE_INS_NUMERFORMU and E.ATE_INS_NUMERPACIE = c.PAC_PAC_NUMERO AND TRIM(D.PAC_PAC_RUT) NOT IN ('10','12') AND (E.OPA_NUMMOVIMIENTO)='             ' and trim(C.CODIGOCENTROATEN) = 'CES' " +
                                  "and E.ATE_INS_CODIGUSUAR in('INCASGRI', 'IJMEJMOR','ILOSOGIR') and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO AND C.CON_CON_CODIGO=CON.CON_CON_CODIGO " +
                                  "AND E.ATE_INS_CODIGO = G.FLD_PRODUCTOCODIGO " +
                        "group by NomCompletoPaciente(c.Pac_Pac_Numero), E.ATE_INS_CODIGO,G.FLD_PRODUCTOGLOSA,E.CON_CON_CODIGO , GlbGetNomCnv(E.CON_CON_CODIGO) , " +
                                      "CON.CON_EMP_RUT, c.rpa_For_Urgencia, GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,E.CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION " +
                        "order by 2,4) group by factura) where factura=facturac  ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet CarteraFalabella(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select  Cnv, Convenio, Facturac Factura, FechaFact FEC_FACTURA, FechaRadi fec_radicada,Vlt_Total, valorcardio, Abonos,Notas_Credito,Notas_Debito, xNombre paciente, Nit, Nombre_Empresa, Saldo, Fechacorte from " +
                        "(Select Convenio Cnv, Nombre_Convenio Convenio, ' ' Producto,  ' ' Operacion, Facturac,  Fecha_Facturacion FechaFact, Fecha_Radicacion FechaRadi, ValorFactura Vlt_Total, " +
                               "Abonos, NotasCredito Notas_Credito, NotasDebito Notas_Debito,NombrePaciente xNombre, Nit, Nombre_Empresa, Saldo, '" + FechaFin + "' Fechacorte " +
                        "From ( select A.con_emp_rut Nit, " +
                                       "( Select Con_Emp_Descripcio From Con_empresa where Con_Emp_Rut = A.Con_Emp_Rut ) Nombre_Empresa, " +
                                       "A.Con_Con_Codigo Convenio, (select Con_Con_Descripcio from con_convenio where Con_Emp_Rut = A.Con_Emp_Rut And Con_Con_Codigo = A.Con_Con_Codigo ) Nombre_Convenio, " +
                                       "A.Fac_Fac_Factura Facturac, ( Select Pac_Pac_Rut from Pac_Paciente where Pac_Pac_Numero = A.Pac_Pac_Numero ) Paciente, " +
                                       "(Select Trim(Pac_Pac_ApellPater) || ' ' || Trim(Pac_Pac_ApellMater) || ' ' || Trim(Pac_Pac_Nombre) from Pac_Paciente where Pac_Pac_Numero =  " +
                                                "(Select Pac_Pac_Numero from TabFctEmp where Fac_Fac_Factura = H.Fld_Crd_Num_Car_Cre)) NombrePaciente,  A.Fac_Fac_FechaFacturacion Fecha_Facturacion, " +
                                       "H.FLD_COL_FEC_OTOR Fecha_Radicacion, A.Fac_Fac_Valorfact ValorFactura, Nvl(( Select sum(Fld_Col_Cap_Pag) " +
                                "from can B  where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd')  and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) Abonos, " +
                                        "Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 " +
                                                           "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasCredito, " +
                                        "Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant)  from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 " +
                                                           "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasDebito, " +
                                       "A.Fac_Fac_Valorfact - Nvl(( Select sum(Fld_Col_Cap_Pag) from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                           "and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) - Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num " +
                                                                 "And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) " +
                                                                 "+ Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 " +
                                                                 "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd')  and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) Saldo " +
                                "from TabFctEmp A, CRTCOL H where A.Fac_Fac_FechaFacturacion >= To_Date('1990/01/01','yyyy/mm/dd') and A.Fac_Fac_FechaFacturacion < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 " +
                                        "and A.Fac_Fac_Factura = H.Fld_Crd_Num_Car_Cre and H.CrtEstCod <> '7' AND   NOT EXISTS (SELECT 1 FROM Fac_eventos EVTO  WHERE EVTO.FAC_FAC_FACTURA = A.Fac_Fac_Factura " +
                                                     "AND EVTO.FAC_EVE_FECHAEVENTO >= TO_DATE(SUBSTR('1990/01/01',1,10),'YYYY/MM/DD') AND EVTO.FAC_EVE_FECHAEVENTO < TO_DATE(SUBSTR('" + FechaFin + "',1,10),'YYYY/MM/DD') + 1 AND EVTO.ESTADO = 'A') " +
                                        "and A.Fac_Fac_ValorFact - Nvl(( Select sum(Fld_Col_Cap_Pag) from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                      "and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) - Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act)  from EVE C where C.Fld_Col_Num = H.Fld_Col_Num " +
                                                              "And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd')  and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) " +
                                                        "+ Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 " +
                                                              "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)     > 0 " +
                                "union " +
                                "select A.con_emp_rut Nit, (Select Con_Emp_Descripcio From Con_empresa where Con_Emp_Rut = A.Con_Emp_Rut ) Nombre_Empresa, A.Con_Con_Codigo Convenio, " +
                                       "( select Con_Con_Descripcio from con_convenio where Con_Emp_Rut = A.Con_Emp_Rut And Con_Con_Codigo = A.Con_Con_Codigo ) Nombre_Convenio, " +
                                       "A.Fac_Fac_Factura Facturac,' ' Paciente,' ' NombrePaciente, A.Fac_Fac_FechaFacturacion Fecha_Facturacion,  H.FLD_COL_FEC_OTOR Fecha_Radicacion, A.Mac_Fac_Valor ValorFactura, " +
                                       "Nvl(( Select sum(Fld_Col_Cap_Pag) from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                             "and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) Abonos, Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num " +
                                                        "And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasCredito, " +
                                            "Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 " +
                                                   "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasDebito,A.Mac_Fac_Valor - Nvl(( Select sum(Fld_Col_Cap_Pag) " +
                                                      "from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd')  and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) " +
                                                      "- Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act) from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 " +
                                                              "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)  + Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) " +
                                                                "from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                              "and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) Saldo  from TabCarMasFac A, CRTCOL H where  A.Fac_Fac_FechaFacturacion >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                    "and A.Fac_Fac_FechaFacturacion < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 and A.Fac_Fac_Factura = H.Fld_Crd_Num_Car_Cre and H.CrtEstCod <> '7' and A.Mac_Fac_Valor - Nvl(( Select sum(Fld_Col_Cap_Pag) " +
                                                        "from can B where B.fld_col_num = H.Fld_Col_Num And B.ProCod = H.ProCod and B.fld_col_fec_pag >= To_Date('1990/01/01','yyyy/mm/dd') and B.fld_col_fec_pag < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 ),0) " +
                                                      "- Nvl(( Select sum(Fld_Col_Eve_Val_Ant)-sum(Fld_Col_Eve_Val_Act)  from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 9 " +
                                                       "and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) + Nvl(( Select sum(Fld_Col_Eve_Val_Act)-sum(Fld_Col_Eve_Val_Ant) " +
                                                             "from EVE C where C.Fld_Col_Num = H.Fld_Col_Num And C.ProCod = H.ProCod and C.Fld_Col_Tip_Eve = 11 and C.Fld_Col_Fec_Eve >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                              "and C.Fld_Col_Fec_Eve < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)     > 0 " +
                                "union " +
                                "select A.con_emp_rut Nit,( Select Con_Emp_Descripcio From Con_empresa where Con_Emp_Rut = A.Con_Emp_Rut ) Nombre_Empresa, A.Con_Con_Codigo Convenio, " +
                                       "( select Con_Con_Descripcio from con_convenio where Con_Emp_Rut = A.Con_Emp_Rut And Con_Con_Codigo = A.Con_Con_Codigo ) Nombre_Convenio, " +
                                       "A.Fac_Fac_Factura Facturac, ( Select Pac_Pac_Rut from Pac_Paciente where Pac_Pac_Numero = A.Pac_Pac_Numero ) Paciente, " +
                                       "(Select Trim(Pac_Pac_ApellPater) || ' ' || Trim(Pac_Pac_ApellMater) || ' ' || Trim(Pac_Pac_Nombre) from Pac_Paciente where Pac_Pac_Numero = A.Pac_Pac_Numero ) NombrePaciente, " +
                                       "( A.Fac_Fac_FechaFacturacion ) Fecha_Facturacion,To_Date('1900/01/01','YYYY/MM/DD') Fecha_Radicacion,A.Fac_Fac_Valorfact ValorFactura, 0 Abonos, " +
                                       "Nvl(( Select SUM(FAC_VALORANTFACT) from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA and C.NTATPO = 'N' and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                       "and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)    NotasCredito, Nvl(( Select SUM(FAC_VALORANTFACT) from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA " +
                                       "and C.NTATPO = 'O'  and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) NotasDebito, A.Fac_Fac_Valorfact " +
                                      "- Nvl(( Select SUM(FAC_VALORANTFACT)  from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA and C.NTATPO = 'N' and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') " +
                                                "and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) + Nvl(( Select SUM(FAC_VALORANTFACT) from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA " +
                                                "and C.NTATPO = 'O' and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd')  and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) Saldo " +
                                "from TabFctEmp A " +
                                "where  A.Fac_Fac_FechaFacturacion >= To_Date('1990/01/01','yyyy/mm/dd') and A.Fac_Fac_FechaFacturacion < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1 and A.ESTADO <> 'A' " +
                                "AND NOT EXISTS (SELECT 1 FROM Fac_eventos EVTO WHERE EVTO.FAC_FAC_FACTURA = A.Fac_Fac_Factura AND EVTO.FAC_EVE_FECHAEVENTO >= TO_DATE(SUBSTR('1990/01/01',1,10),'YYYY/MM/DD') " +
                                                            "AND EVTO.FAC_EVE_FECHAEVENTO < TO_DATE(SUBSTR('" + FechaFin + "',1,10),'YYYY/MM/DD') + 1 AND EVTO.ESTADO = 'A') " +
                                "And Not Exists ( Select 1 From CrtCol where Fld_Crd_Num_Car_Cre = A.Fac_Fac_Factura) and A.Fac_Fac_ValorFact  - Nvl(( Select SUM(FAC_VALORANTFACT) from TABFCTNTA C " +
                                                        "where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA and C.NTATPO = 'N' and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0) " +
                                                      "+ Nvl(( Select SUM(FAC_VALORANTFACT)  from TABFCTNTA C where C.FAC_FAC_FACTURA = A.FAC_FAC_FACTURA and C.NTATPO = 'O' " +
                                                       "and C.FECPRC >= To_Date('1990/01/01','yyyy/mm/dd') and C.FECPRC < To_Date('" + FechaFin + "','yyyy/mm/dd') + 1),0)     > 0)), " +
                        "(select distinct factura,sum(valor) valorcardio from  " +
                        "(Select C.RPA_FOR_FECHATENCION FECHA,TIP.PRE_TIP_DESCRIPCIO, D.PAC_PAC_RUT, NomCompletoPaciente(D.PAC_PAC_NUMERO) PACIENTE,A.ATE_PRE_CODIGO, B.PRE_PRE_DESCRIPCIO, A.CON_CON_CODIGO Cnv, " +
                               "GlbGetNomCnv(A.CON_CON_CODIGO) Convenio, CON.CON_EMP_RUT RUT, decode (C.rpa_For_Urgencia ,'H','Hospitalizacion','S','Urgencias','C.Externa') Ambito, sum(A.ATE_PRE_CANTIDAD) CANTIDAD, " +
                               "sum(A.ATE_PRE_MONTOTARIF) TARIFA , sum(A.ATE_PRE_MONTOPAGAR) PAGO1, DECODE(TRIM(A.ATE_PRE_CONCEPAGO ),'TP', sum( a.ATE_pre_MONTOPAGAR) ,(sum(A.ATE_PRE_MONTOTARIF) -sum( a.ATE_pre_MONTOPAGAR) )) valor, " +
                               "GRP.FAC_FAC_FACTURA FACTURA, FAC.FAC_FAC_VALORFACT,  FAC.FAC_FAC_FECHAFACTURACION,'1' " +
                        "from rpa_Formulario C, ate_prestacion A, pre_prestacion B, PRE_TIPO tip,CON_CONVENIO CON, pac_paciente D, tabfctgrp grp,TABFCTEMP FAC " +
                        "where  C.RPA_FOR_NUMERFORMU = A.ATE_PRE_NUMERFORMU AND A.ATE_PRE_TIPOFORMU = C.RPA_FOR_TIPOFORMU and  A.ATE_PRE_CODIGO = B.PRE_PRE_CODIGO and B.PRE_PRE_TIPO=TIP.PRE_TIP_TIPO " +
                            "and  A.ATE_PRE_NUMERpacie = C.PAC_PAC_NUMERO AND A.CON_CON_CODIGO = CON.CON_CON_CODIGO And  trunc(FAC.FAC_FAC_FECHAFACTURACION ) between    To_Date('" + FechaIni + "', 'yyyy/mm/dd') and To_Date('" + FechaFin + "', 'yyyy/mm/dd') " +
                            "and C.PAC_PAC_NUMERO = D.PAC_PAC_NUMERO AND TRIM(D.PAC_PAC_RUT) NOT IN ('10','12') AND A.ATE_PRE_NUMERPACIE = GRP.PAC_PAC_NUMERO(+) and trim(C.CODIGOCENTROATEN) = 'CCF' " +
                            "AND A.ATE_PRE_TIPOFORMU = GRP.RPA_FOR_TIPOFORMU(+) AND A.ATE_PRE_NUMERFORMU = GRP.RPA_FOR_NUMERFORMU(+) AND GRP.FAC_FAC_FACTURA = FAC.FAC_FAC_FACTURA(+) " +
                        "group by NomCompletoPaciente(D.PAC_PAC_NUMERO),A.ATE_PRE_CODIGO, TIP.PRE_TIP_DESCRIPCIO,B.PRE_PRE_DESCRIPCIO,A.CON_CON_CODIGO , GlbGetNomCnv(A.CON_CON_CODIGO) , " +
                                    "CON.CON_EMP_RUT,C.rpa_For_Urgencia,GRP.FAC_FAC_FACTURA, D.PAC_PAC_RUT,C.RPA_FOR_FECHATENCION,FAC.FAC_FAC_VALORFACT,A.ATE_PRE_CONCEPAGO,FAC.FAC_FAC_FECHAFACTURACION " +
                        "order by 2,4) group by factura) where factura=facturac  ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet ProcedimientosCancelados(string CausaCancelar, string FechaIni, string FechaFin, string Historia)
        {
            string[] nomParam = 
            {
                "In_CausaCancelar",
                "In_FechaIni",
                "In_FechaFin",
                "In_Historia"
            };

            object[] vlrParam = 
            {
                CausaCancelar,
                FechaIni,
                FechaFin,
                Historia
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;
            string paciente = "";

            if (Historia != "             ")
            {
                paciente = " and (a.PAC_PAC_RUT) = '" + Historia + "' ";
            }

            if (FechaIni != "" && FechaFin != "")
            {
                FechaIni = " and trunc(C.FECQRF) BETWEEN TO_DATE('" + FechaIni + "','YYYY/MM/DD') AND TO_DATE('" + FechaFin + "','YYYY/MM/DD') ";
            }
            else
            {
                FechaIni = FechaFin = "";
            }

            if (CausaCancelar == "-1")
            {
                sql = "select  a.PAC_PAC_RUT IDENTIFICACION,nomcompletopaciente(A.PAC_PAC_NUMERO) PACIENTE, b.ORDCAUCANNOM CAUSA_CANCELACION,TRIM(c.DESCCANCELACION) DESCRIPCION,c.FECCANCELACION FECHA_CANCELACION " +
                       ",Admsalud.Glbgetnomusr (c.USUCANCELACION)  USUARIO_QUE_CANCELO,I.TIPOPROFENOMBRE ESPECIALIDAD " +
                       "from pac_paciente a,tabordcircaucan b,taborddetcir c, taborddetalle d, ser_profesiona h,tab_tipoprofe i, TABMOTIVOCONS J " +
                           "where a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and c.ORDCAUCANCOD=b.ORDCAUCANCOD " +
                                "and trim(a.PAC_PAC_RUT) not in ( '10','12') " +
                                " " + FechaIni + " " +
                                "and C.PAC_PAC_NUMERO= D.PAC_PAC_NUMERO " +
                                "and C.MTVCORRELATIVO = D.MTVCORRELATIVO " +
                                "and C.TABENCUENTRO = D.TABENCUENTRO " +
                                "and C.ORDNUMERO = D.ORDNUMERO " +
                                "and C.ORDCIRESTCOD='C' and I.TIPOPROFECODIGO = '02' " +
                                "AND J.SER_PRO_RUT=h.SER_PRO_RUT AND D.PAC_PAC_NUMERO=J.PAC_PAC_NUMERO AND D.MTVCORRELATIVO=J.MTVCORRELATIVO  and H.SER_PRO_TIPO=I.TIPOPROFECODIGO " +
                                " " + paciente + " " +
                            "group by B.ORDCAUCANCOD, a.PAC_PAC_RUT,A.PAC_PAC_NUMERO, b.ORDCAUCANNOM ,c.DESCCANCELACION,c.FECCANCELACION ,c.USUCANCELACION,I.TIPOPROFENOMBRE " +
                            "ORDER BY B.ORDCAUCANCOD ";
            }
            else
            {
                sql = "select  a.PAC_PAC_RUT IDENTIFICACION,nomcompletopaciente(A.PAC_PAC_NUMERO) PACIENTE, b.ORDCAUCANNOM CAUSA_CANCELACION,TRIM(c.DESCCANCELACION) DESCRIPCION,c.FECCANCELACION FECHA_CANCELACION " +
                        ",Admsalud.Glbgetnomusr (c.USUCANCELACION)  USUARIO_QUE_CANCELO,I.TIPOPROFENOMBRE ESPECIALIDAD " +
                      "from pac_paciente a,tabordcircaucan b,taborddetcir c, taborddetalle d, ser_profesiona h,tab_tipoprofe i, TABMOTIVOCONS J " +
                           "where a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and c.ORDCAUCANCOD=b.ORDCAUCANCOD " +
                                "and trim(a.PAC_PAC_RUT) not in ( '10','12') " +
                                " " + FechaIni + " " +
                                "and C.PAC_PAC_NUMERO= D.PAC_PAC_NUMERO " +
                                "and C.MTVCORRELATIVO = D.MTVCORRELATIVO " +
                                "and C.TABENCUENTRO = D.TABENCUENTRO " +
                                "and C.ORDNUMERO = D.ORDNUMERO " +
                                "and C.ORDCIRESTCOD='C' and I.TIPOPROFECODIGO = '02' " +
                                "AND J.SER_PRO_RUT=h.SER_PRO_RUT AND D.PAC_PAC_NUMERO=J.PAC_PAC_NUMERO AND D.MTVCORRELATIVO=J.MTVCORRELATIVO  and H.SER_PRO_TIPO=I.TIPOPROFECODIGO " +
                                "and c.ORDCAUCANCOD='" + CausaCancelar + "' " +
                                " " + paciente + " " +
                            "group by B.ORDCAUCANCOD, a.PAC_PAC_RUT,A.PAC_PAC_NUMERO, b.ORDCAUCANNOM ,c.DESCCANCELACION,c.FECCANCELACION ,c.USUCANCELACION,I.TIPOPROFENOMBRE " +
                            "ORDER BY B.ORDCAUCANCOD ";

            }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
