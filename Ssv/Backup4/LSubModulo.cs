﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LSubModulo
    {
        public DataTable ModuloListar()
        {
            return getDataTable("uspModuloListar");
        }

        public bool SubModuloEstado(SubModulo men)
        {
            string[] nomParam = 
            {
                "@Aplicacion",
                "@Menu",
                "@Modulo",
	            "@Codigo",
                "@Estado"
            };

            object[] vlrParam = 
            {
                men.Aplicacion,
                men.Menu,
                men.Mod,
                men.Codigo,
                men.Estado
            };

            return EjecutarSentencia("uspSubModulocambiarEstado", nomParam, vlrParam);
        }

        public bool SubModuloRetirar(string xml, string Usr)
        {
            string[] nomParam = 
            {
                "@xmlSubModulo" ,
	            "@Usuario"
            };

            object[] vlrParam = 
            {
                xml,
                Usr
            };

            return EjecutarSentencia("uspSubModuloRetirarXml", nomParam, vlrParam);
        }

        public bool SubModuloActualizar(SubModulo men)
        {
            string[] nomParam = 
            {
                "@Modulo",
	            "@Codigo",
                "@SubModulo",
	            "@Usr",
                "@Menu",
                "@Aplicacion"
            };

            object[] vlrParam = 
            {
                men.Mod,
                men.Codigo,
                men.SubMod,
                men.Usr,
                men.Menu,
                men.Aplicacion
            };

            return EjecutarSentencia("uspSubModuloActualizar", nomParam, vlrParam);
        }

        public DataTable SubModuloConsultar(SubModulo men)
        {
            string[] nomParam = 
            {
                "@Aplicacion",
                "@Menu",
                "@Modulo",
	            "@Codigo"
            };

            object[] vlrParam = 
            {
                men.Aplicacion,
                men.Menu,
                men.Mod,
                men.Codigo
            };

            return getDataTable("uspSubModuloConsultar", nomParam, vlrParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
