﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LCExterna
    {
        public DataSet AgendaxProfesional(string Profesional, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_Profesional",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                Profesional,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select age.PCA_AGE_FECHACITAC fecha_cita,age.PCA_AGE_HORACITAC hora,age.PCA_AGE_BLOQUEADO RESERVADO,NOMCOMPLETOPACIENTE(age.PCA_AGE_NUMERPACIE) paciente, C.CON_CON_DESCRIPCIO convenio, age.PCA_AGE_CODIGPROFE rut_profesional, trim(D.SER_PRO_APELLPATER) || ' ' || trim(D.SER_PRO_APELLMATER) || ' ' || trim(D.SER_PRO_NOMBRES) profesional " +
                           "from pac_paciente b, con_convenio c, ser_profesiona d, pca_agenda age " +
                          "where age.PCA_AGE_CODIGPROFE='" + Profesional + "' and age.PCA_AGE_NUMERPACIE=B.PAC_PAC_NUMERO(+) and b .PAC_PAC_CODIGO=C.CON_CON_CODIGO(+) " +
                            "and age.PCA_AGE_CODIGPROFE=D.SER_PRO_RUT and trim(b.PAC_PAC_RUT) not in ('10','12') and trunc(age.PCA_AGE_FECHACITAC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')  order by 1,2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet ConfirmacionCitas(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select age.PCA_AGE_FECHACITAC fecha_cita,age.PCA_AGE_HORACITAC hora,B.PAC_PAC_RUT IDENTIFICACION ,NOMCOMPLETOPACIENTE(age.PCA_AGE_NUMERPACIE) paciente,B.PAC_PAC_FONO TELEFONO,C.CON_CON_DESCRIPCIO convenio, age.PCA_AGE_CODIGPROFE rut, trim(D.SER_PRO_APELLPATER) || ' ' || trim(D.SER_PRO_APELLMATER) || ' ' || trim(D.SER_PRO_NOMBRES) profesional, E.TIPOPROFENOMBRE TIPO_PROFESION " +
                           "from pac_paciente b, con_convenio c, ser_profesiona d, pca_agenda age, TAB_TIPOPROFE E " +
                          "where age.PCA_AGE_NUMERPACIE=B.PAC_PAC_NUMERO and b .PAC_PAC_CODIGO=C.CON_CON_CODIGO AND D.SER_PRO_TIPO = E.TIPOPROFECODIGO AND B.PAC_PAC_NUMERO NOT IN ('5024', '1308') " +
                            "and age.PCA_AGE_CODIGPROFE=D.SER_PRO_RUT and trim(b.PAC_PAC_RUT) not in ('10','12') and trunc(age.PCA_AGE_FECHACITAC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')  order by 1,8,2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet CitasRealizadasxProfesional(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select C.SER_PRO_RUT rut_profesional, trim(C.SER_PRO_APELLPATER) || ' '  ||trim(C.SER_PRO_APELLMATER) || ' ' || trim(C.SER_PRO_NOMBRES) Profesional, F.TIPOPROFENOMBRE ESPECIALIDAD ,COUNT(*) CONSULTAS " +
                           "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F " +
                           "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                             "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC " +
                             "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' " +
                             "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') " +
                           "GROUP BY F.TIPOPROFENOMBRE , C.SER_PRO_RUT, C.SER_PRO_APELLPATER, C.SER_PRO_APELLMATER,C.SER_PRO_NOMBRES ORDER BY 2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet OportunidadCitas(string FechaIni, string FechaFin, string Centro)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin",
                "In_Centro"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin, 
                Centro
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente,a.PCA_AGE_FECHASOLICITUD FEC_SOLICITUD,to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) FEC_CITA,a.PCA_AGE_FECHACITAC -(trunc(a.PCA_AGE_FECHASOLICITUD)) OPORTUNIDAD, F.TIPOPROFENOMBRE especialidad, G.PRE_TIP_DESCRIPCIO TIPO, H.PRE_SUB_DESCRIPCIO SUBTIPO, D.PRE_PRE_CODIGO codigo , D.PRE_PRE_DESCRIPCIO prestacion, I.CON_CON_DESCRIPCIO CONVENIO, decode(K.PAC_PAC_NUMERO,'','NO','SI') HICIERON_HC " +
                           "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, PRE_TIPO G, PRE_SUBTIPO H, CON_CONVENIO I, tabmotivocons K " +
                           "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE " +
                             "and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC and a.MTVCORRELATIVO=K.MTVCORRELATIVO(+) and a.PCA_AGE_NUMERPACIE=K.PAC_PAC_NUMERO(+) " +
                             "and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and a.PCA_AGE_LUGAR = '" + Centro + "' " +
                             "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND C.SER_PRO_TIPO not in ('57','10','11','84','86') AND D.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO AND B.CON_CON_CODIGO=I.CON_CON_CODIGO ORDER BY 2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet PuntualidadCitas(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select distinct e.PAC_PAC_RUT identificacion,nomcompletopaciente(a.PCA_AGE_NUMERPACIE) paciente,to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) fec_cita, G.FECPRC fec_atencion ,round((G.FECPRC -to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) )*1440) puntualidad, F.TIPOPROFENOMBRE especialidad,trim(C.SER_PRO_NOMBRES) || ' ' || trim(C.SER_PRO_APELLPATER ) || ' ' || trim(C.SER_PRO_APELLMATER ) profesional " +
                           "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e, TAB_TIPOPROFE F, TABMOTIVOCONSdet G " +
                           "where C.SER_PRO_TIPO=F.TIPOPROFECODIGO AND trunc(b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                           "and a.PCA_AGE_CODIGPROFE=b.RPA_FCI_CODIGPROFE and a.PCA_AGE_HORACITAC=b.RPA_FCI_HORACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI " +
                           "and a.PCA_AGE_CORRELHORA=b.RPA_FCI_CORREHORA and a.PCA_AGE_FECHACITAC=b.RPA_FCI_FECHACITAC and a.PCA_AGE_CODIGSERVI=b.RPA_FCI_CODIGSERVI " +
                           "and a.PCA_AGE_NUMERPACIE != 0 and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT and A.PCA_AGE_RECEPCIONADO='S' and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO " +
                           "and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO and trim(e.PAC_PAC_RUT) not in ('10','12') AND A.PCA_AGE_NUMERPACIE=G.PAC_PAC_NUMERO AND A.MTVCORRELATIVO=G.MTVCORRELATIVO " +
                         "ORDER BY 3,2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet AsignacionCitasXUsuario(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select AGE.PCA_AGE_FECHASOLICITUD fecha_solicitud, Admsalud.Glbgetnomusr(AGE.PCA_AGE_CODIGRECEP) USUARIO_ASIGNO ,age.PCA_AGE_FECHACITAC fecha_cita,age.PCA_AGE_HORACITAC hora,B.PAC_PAC_RUT IDENTIFICACION ,NOMCOMPLETOPACIENTE(age.PCA_AGE_NUMERPACIE) paciente, " +
                                "age.PCA_AGE_CODIGPROFE rut, trim(D.SER_PRO_APELLPATER) || ' ' || trim(D.SER_PRO_APELLMATER) || ' ' || trim(D.SER_PRO_NOMBRES) profesional, E.TIPOPROFENOMBRE TIPO_PROFESION " +
                           "from pac_paciente b, ser_profesiona d, pca_agenda age, TAB_TIPOPROFE E " +
                          "where age.PCA_AGE_NUMERPACIE=B.PAC_PAC_NUMERO AND D.SER_PRO_TIPO = E.TIPOPROFECODIGO AND B.PAC_PAC_NUMERO NOT IN ('5024', '1308') and age.PCA_AGE_CODIGPROFE=D.SER_PRO_RUT " +
                            "and trim(b.PAC_PAC_RUT) not in ('10','12') and trunc(age.PCA_AGE_FECHASOLICITUD) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')  order by 2,1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet CancelacionCitasXUsuario(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select a.PCA_HCA_FECHADIGIT fecha_cancelacion, Admsalud.Glbgetnomusr(A.PCA_HCA_CODIGUSUAR) USUARIO_cancelo,C.CAUSAELIMNOMBRE causa_cancelacion ,a.PCA_AGE_FECHACITAC fecha_cita,a.PCA_AGE_HORACITAC hora,B.PAC_PAC_RUT IDENTIFICACION , " +
                                "NOMCOMPLETOPACIENTE(a.PCA_HCA_NUMERPACIE) paciente, A.PCA_AGE_CODIGPROFE rut, trim(D.SER_PRO_APELLPATER) || ' ' || trim(D.SER_PRO_APELLMATER) || ' ' || trim(D.SER_PRO_NOMBRES) profesional, E.TIPOPROFENOMBRE TIPO_PROFESION  " +
                          "from pac_paciente b, ser_profesiona d, TAB_TIPOPROFE E, PCA_HISTOCAMBI A, TAB_CausaElim C " +
                         "where A.PCA_HCA_NUMERPACIE=B.PAC_PAC_NUMERO AND D.SER_PRO_TIPO = E.TIPOPROFECODIGO AND B.PAC_PAC_NUMERO NOT IN ('5024', '1308') and A.PCA_AGE_CODIGPROFE=D.SER_PRO_RUT and trim(b.PAC_PAC_RUT) not in ('10','12') " +
                           "and trunc(a.PCA_HCA_FECHADIGIT) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')  AND A.PCA_HCA_CAUSA <>' ' AND A.PCA_HCA_CAUSA=C.CAUSAELIMCODIGO  order by 2,1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
