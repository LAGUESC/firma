﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LClasificaciones
    {
        public bool ClasiGuardar(Clasificaciones clasi)
        {
            string[] nomParam = { "@Codigo", "@Nombre", "@Usuario" };
            object[] vlrParam = { clasi.Codigo, clasi.Nombre, clasi.Usuario };

            return EjecutarSentencia("uspClasiActualizar", nomParam, vlrParam);
        }

        public bool ClasiEstado(Clasificaciones app)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                app.Codigo,
                app.Estado
            };

            return EjecutarSentencia("uspClasiEstado", nomParam, vlrParam);
        }

        public DataTable ClasiConsultar(Clasificaciones clasi)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { clasi.Codigo };

            return getDataTable("uspClasiConsultar", nomParam, vlrParam);
        }

        public bool ClasiReactivar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Clasi", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspClasiReactivar", nomParam, valParam);
        }

        public bool ClasiRetirar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Clasi", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspClasiRetirar", nomParam, valParam);
        }

        public DataTable ClasiConsultar()
        {
            return getDataTable("uspClasiConsultarCodigos");
        }

        public bool ClasisRetirar(string xmlClasi, string Usuario)
        {
            string[] nomParam = { "@xmlClasi", "@Usuario" };
            object[] valParam = { xmlClasi, Usuario };

            return EjecutarSentencia("uspClasiRetirarXml", nomParam, valParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }


}
