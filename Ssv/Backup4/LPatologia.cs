﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LPatologia
    {
        public DataSet OportunidadPatologiaAmbul(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT IDENTIFICACION, PACIENTE, CONVENIO, CODIGO, DESCRIPCION, FEC_SOLICITUD, RECEPCION, FEC_RECEPCION, ROUND(OPORTUNIDAD_RECEPCION,2) OPORTUNIDAD_RECEP, EVENTO,FECHADIG FEC_REVISION, ROUND(OPORT,2) OPORTUNIDAD_REVI, numero1 NORDEN  FROM (SELECT a.ATE_PRE_TIPOFORMU tipo1,a.ATE_PRE_NUMERFORMU numero1,b.CEX_EXS_CODIGPREST cod1,a.PAC_PAC_NUMERO pacnum1,c.PAC_PAC_RUT identificacion,nomcompletopaciente(C.PAC_PAC_NUMERO) paciente,e.CON_CON_DESCRIPCIO convenio,b.CEX_EXS_CODIGPREST codigo,f.PRE_PRE_DESCRIPCIO descripcion,a.RPA_FOR_FECHASOLIC FEC_SOLICITUD,DECODE(b.CEX_EVE_TIPOEVENT,1,'RECEPCION') RECEPCION,b.ATE_PRE_FECHADIGIT FEC_RECEPCION,(b.ATE_PRE_FECHADIGIT-a.RPA_FOR_FECHASOLIC)*24 OPORTUNIDAD_RECEPCION " +
                           "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                                   "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU " +
                                      "and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU " +
                                      "and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                      "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU " +
                                      "and d.CON_CON_CODIGO=e.CON_CON_CODIGO " +
                                      "and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO " +
                                      "and a.ORDTIPO='A' " +
                                      "and a.SER_SER_AMBITO='01' " +
                                      "and b.CEX_EVE_TIPOEVENT in ('1 ') " +
                                      "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                      "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')), " +
                                    "(SELECT a.ATE_PRE_TIPOFORMU tipo,a.ATE_PRE_NUMERFORMU numero,b.CEX_EXS_CODIGPREST cod,a.PAC_PAC_NUMERO pacnum,DECODE(b.CEX_EVE_TIPOEVENT,6,'REVISADA') evento,b.ATE_PRE_FECHADIGIT fechadig,(b.ATE_PRE_FECHADIGIT-a.RPA_FOR_FECHASOLIC) oport " +
                                                       "from rpa_forlab a,cex_eventos b,pac_paciente c,rpa_formulario d,con_convenio e,pre_prestacion f " +
                                                               "where a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU " +
                                                                  "and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU " +
                                                                  "and a.RPA_FOR_TIPOFORMU=d.RPA_FOR_TIPOFORMU " +
                                                                  "and a.RPA_FOR_NUMERFORMU=d.RPA_FOR_NUMERFORMU " +
                                                                  "and d.CON_CON_CODIGO=e.CON_CON_CODIGO " +
                                                                  "and b.CEX_EXS_CODIGPREST=f.PRE_PRE_CODIGO " +
                                                                  "and a.ORDTIPO='A' " +
                                                                  "and a.SER_SER_AMBITO='01' " +
                                                                  "and b.CEX_EVE_TIPOEVENT in ('6 ') " +
                                                                  "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO and A.PAC_PAC_NUMERO NOT IN ('1308','5024') " +
                                                                  "and trunc(a.RPA_FOR_FECHASOLIC) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd')) " +
                    "WHERE TIPO1=TIPO(+) AND NUMERO1=NUMERO(+) AND COD1=COD(+) AND PACNUM1=PACNUM(+) " +
                    "GROUP BY IDENTIFICACION, PACIENTE, CONVENIO, CODIGO, DESCRIPCION, FEC_SOLICITUD, RECEPCION, OPORTUNIDAD_RECEPCION, EVENTO,FECHADIG, OPORT, FEC_RECEPCION, numero1 order by 2";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet ExamenesSolicitados(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select PAC.PAC_PAC_RUT identificacion, nomcompletopaciente(PAC.PAC_PAC_NUMERO) paciente,ORD.OrdNumero OrdNumero" +
                            ",SubStr(To_Char(ORD.OrdFecha,'MM/DD HH24:MI') || ' - ' || GlbGetUbiPac(ORD.PAC_PAC_Numero,ORD.MtvCorrelativo,ORD.SER_SER_Ambito),1,60) servicio " +
                            "From   TabOrdenesServ ORD,PAC_Paciente PAC " +
                            "Where  PAC.PAC_PAC_Numero  = ORD.PAC_PAC_Numero And ORD.OrdFecha >= To_Date(SubStr('" + FechaIni + "',1,10),'YYYY/MM/DD') And ORD.OrdFecha < To_Date(SubStr('" + FechaFin + "',1,10),'YYYY/MM/DD') + 1 " +
                            "And Exists (Select 'x'  From  TabOrdDetalle Where PAC_PAC_Numero = ORD.PAC_PAC_Numero And  MtvCorrelativo = ORD.MtvCorrelativo And TabEncuentro = ORD.TabEncuentro And OrdTipo = ORD.OrdTipo And OrdNumero = ORD.OrdNumero) " +
                            "And (Exists (Select 'x' From   ATC_Estadia Where  PAC_PAC_Numero   = ORD.PAC_PAC_Numero And CodigoCentroAten = 'CES  ') Or Exists (Select 'x'  From RPA_ForDau Where PAC_PAC_Numero= ORD.PAC_PAC_Numero " +
                                    "And RPA_FOR_NumerFormu = (Select Max(RPA_FOR_NumerFormu) From   RPA_ForDau  Where  PAC_PAC_Numero    = ORD.PAC_PAC_Numero And RPA_FOR_TipoFormu = '04' And MtvCorrelativo    <> 0) " +
                                    "And UrgEtd<> 'X' And CodigoCentroAten   = 'CES  ')) And  Exists (Select 'X'  From TabOrdSrvTpo T,PRE_SubTipo  P,OrdenSalud_AppParam  O  Where  T.OrdTipo = ORD.OrdTipo And T.PRE_Tip_Tipo = P.PRE_Tip_Tipo And P.PRE_SUB_SubTipo  = '0012     ' ) " +
                            "And ORD.SER_SER_Ambito != '01'  And  ORD.OrdEstado = 'S' and PAC.PAC_PAC_NUMERO NOT IN ('1308','5024') ORDER BY 2,3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet OportunidadCitologias(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "Select PAC.PAC_PAC_RUT identificacion, nomcompletopaciente(PAC.PAC_PAC_NUMERO) paciente,ORD.OrdNumero OrdNumero" +
                            ",SubStr(To_Char(ORD.OrdFecha,'MM/DD HH24:MI') || ' - ' || GlbGetUbiPac(ORD.PAC_PAC_Numero,ORD.MtvCorrelativo,ORD.SER_SER_Ambito),1,60) servicio " +
                            "From   TabOrdenesServ ORD,PAC_Paciente PAC " +
                            "Where  PAC.PAC_PAC_Numero  = ORD.PAC_PAC_Numero And ORD.OrdFecha >= To_Date(SubStr('" + FechaIni + "',1,10),'YYYY/MM/DD') And ORD.OrdFecha < To_Date(SubStr('" + FechaFin + "',1,10),'YYYY/MM/DD') + 1 " +
                            "And Exists (Select 'x'  From  TabOrdDetalle Where PAC_PAC_Numero = ORD.PAC_PAC_Numero And  MtvCorrelativo = ORD.MtvCorrelativo And TabEncuentro = ORD.TabEncuentro And OrdTipo = ORD.OrdTipo And OrdNumero = ORD.OrdNumero) " +
                            "And (Exists (Select 'x' From   ATC_Estadia Where  PAC_PAC_Numero   = ORD.PAC_PAC_Numero And CodigoCentroAten = 'CES  ') Or Exists (Select 'x'  From RPA_ForDau Where PAC_PAC_Numero= ORD.PAC_PAC_Numero " +
                                    "And RPA_FOR_NumerFormu = (Select Max(RPA_FOR_NumerFormu) From   RPA_ForDau  Where  PAC_PAC_Numero    = ORD.PAC_PAC_Numero And RPA_FOR_TipoFormu = '04' And MtvCorrelativo    <> 0) " +
                                    "And UrgEtd<> 'X' And CodigoCentroAten   = 'CES  ')) And  Exists (Select 'X'  From TabOrdSrvTpo T,PRE_SubTipo  P,OrdenSalud_AppParam  O  Where  T.OrdTipo = ORD.OrdTipo And T.PRE_Tip_Tipo = P.PRE_Tip_Tipo And P.PRE_SUB_SubTipo  = '0012     ' ) " +
                            "And ORD.SER_SER_Ambito != '01'  And  ORD.OrdEstado = 'S' and PAC.PAC_PAC_NUMERO NOT IN ('1308','5024') ORDER BY 2,3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
    }
}
