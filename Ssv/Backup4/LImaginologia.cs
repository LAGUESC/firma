﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LImaginologia
    {
        public DataSet OportunidadImaginologiaAmb(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select identificacion, paciente, fecha_solicitud, fecha_cita, TO_NUMBER(DECODE(fecha_cita,'','', round(to_date(fecha_cita,'yyyy/mm/dd hh24:mi') - fecha_solicitud,2)))*24 oport_cita_horas , fec_confirmacion, round(DECODE(fecha_cita,'','', ( fec_confirmacion -  to_date(fecha_cita,'yyyy/mm/dd hh24:mi')))*1440,2) puntualidad_MIN ,x.ate_pre_fechadigit fecha_resultado ,round( (x.ate_pre_fechadigit - fec_confirmacion),1) oport_Resultado_dias ,tipo, subtipo,codigo, prestacion, convenio, formulario  " +
                           "from (SELECT d.PAC_PAC_RUT identificacion ,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente,a.RPA_FOR_FECHASOLIC FECha_SOLICITUD, DECODE(I.PCA_AGE_FECHACITAC,'','', to_char(I.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' || I.PCA_AGE_HORACITAC) fecha_cita,   a.RPA_FOR_FECHARECEP FEC_confirmacion, " +
                                   "G.PRE_TIP_DESCRIPCIO TIPO, H.PRE_SUB_DESCRIPCIO SUBTIPO,c.PRE_PRE_CODIGO codigo ,TRIM(c.PRE_PRE_DESCRIPCIO) prestacion,E.CON_CON_DESCRIPCIO convenio, A.RPA_FOR_NUMERFORMU FORMULARIO, A.ATE_PRE_NUMERFORMU formu " +
                                   "FROM RPA_FORLAB A,ATE_PRESTACION B,PRE_PRESTACION C,pac_paciente d,con_convenio e,  PRE_TIPO G, PRE_SUBTIPO H, PCA_Agenda I " +
                                   "WHERE A.ORDTIPO='I' AND A.SER_SER_AMBITO='01' AND A.RPA_FOR_TIPOFORMU=B.ATE_PRE_TIPOFORMU AND A.RPA_FOR_NUMERFORMU=B.ATE_PRE_NUMERFORMU " +
                                   "and B.ATE_PRE_VIGENCIA ='N' and B.CON_CON_CODIGO='1420    '  and D.PAC_PAC_NUMERO NOT IN ('1308','5024') AND B.ATE_PRE_CODIGO=C.PRE_PRE_CODIGO and a.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO " +
                                   "and B.CON_CON_CODIGO=E.CON_CON_CODIGO  and trunc(a.RPA_FOR_FECHARECEP) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                   "AND c.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND c.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND c.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO " +
                                   "AND A.RPA_FOR_TIPOFORMU=I.PCA_AGE_TIPOFORMU(+) AND A.RPA_FOR_NUMERFORMU=I.PCA_AGE_NUMERFORMU(+) " +
                                "union all " +
                                "SELECT d.PAC_PAC_RUT identificacion ,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente,a.RPA_FOR_FECHASOLIC FECha_SOLICITUD, DECODE(I.PCA_AGE_FECHACITAC,'','', to_char(I.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' || I.PCA_AGE_HORACITAC) fecha_cita,   a.RPA_FOR_FECHARECEP FEC_confirmacion, " +
                                   "G.PRE_TIP_DESCRIPCIO TIPO, H.PRE_SUB_DESCRIPCIO SUBTIPO,c.PRE_PRE_CODIGO codigo ,TRIM(c.PRE_PRE_DESCRIPCIO) prestacion,E.CON_CON_DESCRIPCIO convenio, A.RPA_FOR_NUMERFORMU FORMULARIO, A.ATE_PRE_NUMERFORMU formu " +
                                   "FROM RPA_FORLAB A,ATE_PRESTACION B,PRE_PRESTACION C,pac_paciente d,con_convenio e, PRE_TIPO G, PRE_SUBTIPO H , PCA_Agenda I " +
                                   "WHERE A.ORDTIPO='I'  AND A.SER_SER_AMBITO='01' AND A.RPA_FOR_TIPOFORMU=B.ATE_PRE_TIPOFORMU AND A.RPA_FOR_NUMERFORMU=B.ATE_PRE_NUMERFORMU and B.ATE_PRE_VIGENCIA <>'N' " +
                                   "and D.PAC_PAC_NUMERO NOT IN ('1308','5024') AND B.ATE_PRE_CODIGO=C.PRE_PRE_CODIGO and a.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO and B.CON_CON_CODIGO=E.CON_CON_CODIGO " +
                                   "and trunc(a.RPA_FOR_FECHARECEP) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                   "AND c.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND c.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND c.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO " +
                                   "AND A.RPA_FOR_TIPOFORMU=I.PCA_AGE_TIPOFORMU(+) AND A.RPA_FOR_NUMERFORMU=I.PCA_AGE_NUMERFORMU(+) " +
                                "union all " +
                                "SELECT d.PAC_PAC_RUT identificacion ,nomcompletopaciente(a.PAC_PAC_NUMERO) paciente,a.RPA_FOR_FECHASOLIC FECha_SOLICITUD, DECODE(I.PCA_AGE_FECHACITAC,'','', to_char(I.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' || I.PCA_AGE_HORACITAC) fecha_cita,   a.RPA_FOR_FECHARECEP FEC_confirmacion, " +
                                   "G.PRE_TIP_DESCRIPCIO TIPO, H.PRE_SUB_DESCRIPCIO SUBTIPO,c.PRE_PRE_CODIGO codigo1 ,TRIM(c.PRE_PRE_DESCRIPCIO) prestacion,E.CON_CON_DESCRIPCIO convenio, A.RPA_FOR_NUMERFORMU FORMULARIO, A.ATE_PRE_NUMERFORMU formu1 " +
                                   "FROM RPA_FORLAB A,ATE_PRESTACION B,PRE_PRESTACION C,pac_paciente d,con_convenio e, PRE_TIPO G, PRE_SUBTIPO H , PCA_Agenda I " +
                                   "WHERE C.PRE_PRE_TIPO='0037' AND A.SER_SER_AMBITO='01' AND A.RPA_FOR_TIPOFORMU=B.ATE_PRE_TIPOFORMU AND A.RPA_FOR_NUMERFORMU=B.ATE_PRE_NUMERFORMU and B.ATE_PRE_VIGENCIA <>'N' " +
                                   "and D.PAC_PAC_NUMERO NOT IN ('1308','5024') AND B.ATE_PRE_CODIGO=C.PRE_PRE_CODIGO and a.PAC_PAC_NUMERO=d.PAC_PAC_NUMERO and B.CON_CON_CODIGO=E.CON_CON_CODIGO " +
                                   "and trunc(a.RPA_FOR_FECHARECEP) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                                   "AND c.PRE_PRE_TIPO=G.PRE_TIP_TIPO AND c.PRE_PRE_TIPO=H.PRE_TIP_TIPO AND c.PRE_PRE_SUBTIPO=H.PRE_SUB_SUBTIPO " +
                                   "AND A.RPA_FOR_TIPOFORMU=I.PCA_AGE_TIPOFORMU(+) AND A.RPA_FOR_NUMERFORMU=I.PCA_AGE_NUMERFORMU(+)) " +
                                   ",cex_eventos x " +
                                   "where formu=x.ate_pre_numerformu(+) and x.CEX_EVE_TIPOEVENT='4 ' and codigo=x.cex_exs_codigprest(+) " +
                           "Order by 2 " ;

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        
        public DataSet ParametrosRadiacion(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select b.PAC_PAC_RUT identificacion,nomcompletopaciente(B.PAC_PAC_NUMERO) paciente ,a.CEX_EXS_FECHAMUEST fecha_muestra,a.CEX_EXS_CODIGPREST CODIGO,c.PRE_PRE_DESCRIPCIO PRESTACION, ' '||trim(a.OBSREAALE) ||' ' radiacion " +
                           "from CEX_ExamSolic a,pac_paciente b,pre_prestacion c " +
                          "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO and a.CEX_EXS_CODIGPREST=c.PRE_PRE_CODIGO and c.PRE_PRE_SUBTIPO='0025' " +
                            "and trunc(a.CEX_EXS_FECHAMUEST) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') order by 2,3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet OportunidadImaginologiaHosp(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select c.PAC_PAC_RUT identificacion,nomcompletopaciente(c.PAC_PAC_NUMERO) paciente ,a.CEX_EXS_CODIGPREST codigo,d.PRE_PRE_DESCRIPCIO descripcion,f.ORDFECHA fecha_ordenado,H.ATE_PRE_FECHADIGIT fecha_recepcionado , " +
                                "round((h.ATE_PRE_FECHADIGIT-f.ORDFECHA)*24,2) ordrec_horas ,a.ATE_PRE_FECHADIGIT fecha_confirmado,round((a.ATE_PRE_FECHADIGIT-H.ATE_PRE_FECHADIGIT)*24,2) reccon_horas ,b.ATE_PRE_FECHADIGIT fecha_revisado,round((b.ATE_PRE_FECHADIGIT-a.ATE_PRE_FECHADIGIT)*24,2) conrev_horas,a.ATE_PRE_TIPOFORMU tipoformu,a.ATE_PRE_NUMERFORMU formulario " +
                           "from cex_eventos a,cex_eventos b,pac_paciente c,pre_prestacion d,rpa_forlab e,tabordenesserv f, tab_ambito g, cex_eventos h " +
                          "where a.CEX_EVE_TIPOEVENT=10 and b.CEX_EVE_TIPOEVENT = 6  and H.CEX_EVE_TIPOEVENT = 1 and h.ATE_PRE_NUMERFORMU=e.ATE_PRE_NUMERFORMU and h.ATE_PRE_TIPOFORMU=e.ATE_PRE_TIPOFORMU and e.ORDNUMERO=f.ORDNUMERO and f.PAC_PAC_NUMERO=h.ATE_PRE_NUMERPACIE " +
                            "and h.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU and h.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU and h.CEX_EXS_CODIGPREST=b.CEX_EXS_CODIGPREST and a.ATE_PRE_TIPOFORMU=h.ATE_PRE_TIPOFORMU and a.ATE_PRE_NUMERFORMU=h.ATE_PRE_NUMERFORMU " +
                            "and a.CEX_EXS_CODIGPREST=h.CEX_EXS_CODIGPREST and trunc(h.ATE_PRE_FECHADIGIT) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and h.ATE_PRE_NUMERPACIE=c.PAC_PAC_NUMERO and h.CEX_EXS_CODIGPREST=d.PRE_PRE_CODIGO " +
                            "and h.ATE_PRE_TIPOFORMU='06  ' and f.SER_SER_AMBITO=g.AMBITOCODIGO and F.SER_SER_AMBITO='02' " +
                       "group by c.PAC_PAC_NUMERO,c.PAC_PAC_RUT,a.ATE_PRE_NUMERFORMU ,a.CEX_EXS_CODIGPREST,d.PRE_PRE_DESCRIPCIO,a.ATE_PRE_FECHADIGIT,b.ATE_PRE_FECHADIGIT,b.CEX_EVE_USUARRESPO,a.ATE_PRE_TIPOFORMU,f.ORDFECHA,g.AMBITONOMBRE,H.ATE_PRE_FECHADIGIT " +
                       "union all " +
                         "select c.PAC_PAC_RUT identificacion,nomcompletopaciente(c.PAC_PAC_NUMERO) paciente ,a.CEX_EXS_CODIGPREST codigo,d.PRE_PRE_DESCRIPCIO descripcion,f.ORDFECHA ordenado,H.ATE_PRE_FECHADIGIT recepcionado , " +
                                "round((h.ATE_PRE_FECHADIGIT-f.ORDFECHA)*24,2) ordrec_horas ,a.ATE_PRE_FECHADIGIT confirmado,round((a.ATE_PRE_FECHADIGIT-H.ATE_PRE_FECHADIGIT)*24,2) reccon_horas ,b.ATE_PRE_FECHADIGIT revisado,round((b.ATE_PRE_FECHADIGIT-a.ATE_PRE_FECHADIGIT)*24,2) conrev_horas,a.ATE_PRE_TIPOFORMU tipoformu,a.ATE_PRE_NUMERFORMU formulario " +
                           "from cex_eventos a,cex_eventos b,pac_paciente c,pre_prestacion d,rpa_forlab e,tabordenesserv f, tab_ambito g, cex_eventos h " +
                          "where a.CEX_EVE_TIPOEVENT=17 and b.CEX_EVE_TIPOEVENT = 6  and H.CEX_EVE_TIPOEVENT = 1 and h.ATE_PRE_NUMERFORMU=e.ATE_PRE_NUMERFORMU and h.ATE_PRE_TIPOFORMU=e.ATE_PRE_TIPOFORMU and e.ORDNUMERO=f.ORDNUMERO and f.PAC_PAC_NUMERO=h.ATE_PRE_NUMERPACIE " +
                            "and h.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU and h.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU and h.CEX_EXS_CODIGPREST=b.CEX_EXS_CODIGPREST and a.ATE_PRE_TIPOFORMU=h.ATE_PRE_TIPOFORMU and a.ATE_PRE_NUMERFORMU=h.ATE_PRE_NUMERFORMU " +
                            "and a.CEX_EXS_CODIGPREST=h.CEX_EXS_CODIGPREST and trunc(h.ATE_PRE_FECHADIGIT) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and h.ATE_PRE_NUMERPACIE=c.PAC_PAC_NUMERO and h.CEX_EXS_CODIGPREST=d.PRE_PRE_CODIGO " +
                            "and h.ATE_PRE_TIPOFORMU='46  ' and f.SER_SER_AMBITO=g.AMBITOCODIGO and F.SER_SER_AMBITO='02' and D.PRE_PRE_TIPO='0037' " +
                       "group by c.PAC_PAC_NUMERO,c.PAC_PAC_RUT,a.ATE_PRE_NUMERFORMU ,a.CEX_EXS_CODIGPREST,d.PRE_PRE_DESCRIPCIO,a.ATE_PRE_FECHADIGIT,b.ATE_PRE_FECHADIGIT,b.CEX_EVE_USUARRESPO,a.ATE_PRE_TIPOFORMU,f.ORDFECHA,g.AMBITONOMBRE,H.ATE_PRE_FECHADIGIT " +
                       "order by 2,5  ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet ConfirmadoRXsinAprobar(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select distinct c.PAC_PAC_RUT identificacion,nomcompletopaciente(c.PAC_PAC_NUMERO) paciente,A.ATE_PRE_TIPOFORMU tipo, a.ATE_PRE_NUMERFORMU formulario, " +
                                "a.CEX_EXS_CODIGPREST codigo,h.PRE_PRE_DESCRIPCIO prestacion,a.ATE_PRE_FECHADIGIT fecha " +
                           "from cex_eventos a,cex_eventos b,pac_paciente c,pre_prestacion h, rpa_forlab i, ate_prestacion j " +
                          "where c.PAC_PAC_NUMERO not in ('1308','5024') and a.CEX_EVE_TIPOEVENT=10 and  not exists (select * from cex_eventos b where b.CEX_EVE_TIPOEVENT=6 " +
                            "and a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU  and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU and a.CEX_EXS_CODIGPREST=b.CEX_EXS_CODIGPREST) " +
                            "and trunc(a.ATE_PRE_FECHADIGIT) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') and a.ATE_PRE_NUMERPACIE=c.PAC_PAC_NUMERO " +
                            "and a.ATE_PRE_TIPOFORMU=b.ATE_PRE_TIPOFORMU and a.ATE_PRE_NUMERFORMU=b.ATE_PRE_NUMERFORMU and a.CEX_EXS_CODIGPREST=b.CEX_EXS_CODIGPREST " +
                            "and a.CEX_EXS_CODIGPREST=h.PRE_PRE_CODIGO and a.ATE_PRE_NUMERFORMU=i.ATE_PRE_NUMERFORMU and a.ATE_PRE_TIPOFORMU=i.ATE_PRE_TIPOFORMU " +
                            "and i.RPA_FOR_NUMERFORMU=j.ATE_PRE_NUMERFORMU and i.RPA_FOR_TIPOFORMU=j.ATE_PRE_TIPOFORMU order by 2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }

}
