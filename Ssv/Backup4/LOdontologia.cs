﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LOdontologia
    {
        public DataSet OportunidadOdontologica(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select e.PAC_PAC_RUT identificacion,nomcompletopaciente(E.PAC_PAC_NUMERO) paciente,a.PCA_AGE_FECHASOLICITUD fecha_solicitud,to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) FEC_CITA,round((to_date((to_char(a.PCA_AGE_FECHACITAC,'yyyy/mm/dd')|| ' ' ||A.PCA_AGE_HORACITAC),'yyyy/mm/dd hh24:mi' ) -(trunc(a.PCA_AGE_FECHASOLICITUD))),2) OPORTUNIDAD,I.PRE_TIP_DESCRIPCIO TIPO ,J.PRE_SUB_DESCRIPCIO SUBTIPO, b.PRE_PRE_CODIGO codigo,d.PRE_PRE_DESCRIPCIO prestacion, H.CON_CON_DESCRIPCIO CONVENIO " +
                           "from PCA_Agenda a,rpa_forcit b,ser_profesiona c,pre_prestacion d,pac_paciente e,tab_tipoprofe f ,tabmotivocons g,con_convenio h,  PRE_TIPO I, PRE_SUBTIPO J " +
                           "where  TRUNC (b.RPA_FCI_FECHACITAC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                              "and a.PCA_AGE_TIPOFORMU=b.RPA_FOR_TIPOFORMU " +
                              "and a.PCA_AGE_NUMERFORMU=b.RPA_FOR_NUMERFORMU " +
                              "and a.PCA_AGE_CODIGPROFE=c.SER_PRO_RUT " +
                              "and b.PRE_PRE_CODIGO=d.PRE_PRE_CODIGO " +
                              "and a.PCA_AGE_NUMERPACIE=e.PAC_PAC_NUMERO " +
                              "and E.PAC_PAC_NUMERO not in ('1308', '5024') " +
                              "and c.SER_PRO_TIPO=f.TIPOPROFECODIGO " +
                              "and a.MTVCORRELATIVO=g.MTVCORRELATIVO " +
                              "and a.PCA_AGE_NUMERPACIE=g.PAC_PAC_NUMERO " +
                              "AND A.PCA_AGE_RECEPCIONADO='S' " +
                              "and h.CON_CON_CODIGO=b.CON_CON_CODIGO " +
                              "AND D.PRE_PRE_TIPO=I.PRE_TIP_TIPO AND D.PRE_PRE_TIPO=J.PRE_TIP_TIPO AND D.PRE_PRE_SUBTIPO=J.PRE_SUB_SUBTIPO " +
                              "And c.SER_PRO_TIPO in ('10','11','84','86') " +
                          "order by 2 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }


        public DataSet PuntualidadURGOdontologica(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select a.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,b.RPA_FDA_HORAINGRESO triage_admon,b.FECPRC triage_medico,c.MTVFECHACONSULTA apertura_hc,b.RPA_FDA_HORAEGRESO egreso_urg, " +
                                 "round( (c.MTVFECHACONSULTA-b.RPA_FDA_HORAINGRESO)*24*60 ,0) OPORTUNIDAD " +
                         "from pac_paciente a,rpa_fordau b,tabmotivocons c,tabtriage e,CON_Convenio D,tabtriagecal f,tabespdsturg g,TAB_Contigencia h " +
                         "where a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO " +
                            "and b.MTVCORRELATIVO=c.MTVCORRELATIVO " +
                            "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO " +
                            "and b.RPA_FOR_NUMERFORMU=e.RPA_FOR_NUMERFORMU " +
                            "and b.RPA_FOR_TIPOFORMU=e.RPA_FOR_TIPOFORMU " +
                            "and trunc(b.FECPRC) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                            "and a.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                            "and e.ESPDSTCOD='05' " +
                            "AND B.CON_CON_CODIGO=D.CON_CON_CODIGO " +
                            "and e.TRIAGECOD=f.TRIAGECOD " +
                            "and e.ESPDSTCOD=g.ESPDSTCOD " +
                            "and c.MTVCON_ORIGEN=h.TAB_CONT_CODIGO " +
                         "order by 3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
