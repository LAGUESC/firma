﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;

namespace ClinicaCES.Logica
{
    public class LAreas
    {

        public bool AreaGuardar(Areas area)
        {
            string[] nomParam = { "@Codigo", "@Nombre", "@Usuario" };
            object[] vlrParam = { area.Codigo, area.Nombre, area.Usuario };

            return EjecutarSentencia("uspAreasActualizar", nomParam, vlrParam);
        }

        public bool AreasEstado(Areas app)
        {
            string[] nomParam = 
            {
                "@Codigo" ,
                "@Estado"
            };

            object[] vlrParam = 
            {
                app.Codigo,
                app.Estado
            };

            return EjecutarSentencia("uspAreasEstado", nomParam, vlrParam);
        }

        public DataTable AreasConsultar(Areas area)
        {
            string[] nomParam = { "@Codigo" };
            object[] vlrParam = { area.Codigo };

            return getDataTable("uspAreaConsultar", nomParam, vlrParam);
        }


        public bool AreaReactivar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Area", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspAreaReactivar", nomParam, valParam);
        }

        public bool AreaRetirar(string codigo, string usuario)
        {
            string[] nomParam = { "@Cod_Area", "@Usuario" };
            object[] valParam = { codigo, usuario };

            return EjecutarSentencia("uspAreaRetirar", nomParam, valParam);
        }


        public DataTable AreasConsultar()
        {
            return getDataTable("uspAreaConsultarCodigos");
        }


        public bool AreasRetirar(string xmlArea, string Usuario)
        {
            string[] nomParam = { "@xmlArea", "@Usuario" };
            object[] valParam = { xmlArea, Usuario };

            return EjecutarSentencia("uspAreaRetirarXml", nomParam, valParam);
        }

        #region Metodos Privados
        private DataTable getDataTable(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().getDataTable(sql, nomParam, valParam);
        }

        private DataTable getDataTable(string sql)
        {
            return new DA.DA().getDataTable(sql);
        }

        private bool EjecutarSentencia(string sql, string[] nomParam, object[] valParam)
        {
            return new DA.DA().EjecutarSentencia(sql, nomParam, valParam);
        }

        #endregion
    }
}
