﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using ClinicaCES.Entidades;
using System.Data.OracleClient;

namespace ClinicaCES.Logica
{
    public class LFarmacia
    {
        public DataSet AntibioticosxExamen(string Historia, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_Historia",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                Historia,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select distinct c.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente ,'EXAMEN' TIPO,G.ATE_PRE_CODIGO codigo,d.PRE_PRE_DESCRIPCIO descripcion,f.AMBITONOMBRE ambito,decode(a.ORDESTADO,'R','Realizado','S','Solicitado') Estado " +
                          "from tabordenesserv a,pac_paciente c,pre_prestacion d,tab_ambito f,ate_prestacion g, RPA_FORLAB h " +
                          "where G.ATE_PRE_NUMERFORMU=H.RPA_FOR_NUMERFORMU " +
                            "and H.ORDNUMERO=A.ORDNUMERO " +
                            "and G.ATE_PRE_CODIGO=D.PRE_PRE_CODIGO " +
                            "and A.SER_SER_AMBITO=F.AMBITOCODIGO " +
                            "and a.PAC_PAC_NUMERO=c.PAC_PAC_NUMERO " +
                            "and C.PAC_PAC_RUT not in ('1308', '5024') " +
                            "and G.ATE_PRE_CODIGO in ('901221  ','901223  ','901235  ','901217  ') " +
                            "and a.ORDFECHA between  to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                            "and G.ATE_PRE_VIGENCIA<>'N' " +
                            "and (C.PAC_PAC_RUT)='" + Historia + "' " +
                        "union all " +
                         "select distinct c.PAC_PAC_RUT rut,nomcompletopaciente(A.ATE_INS_NUMERPACIE) ,'ANTIBIOTICO' TIPO,a.ATE_INS_CODIGO,b.FLD_PRODUCTOGLOSA,'','' " +
                          "from ate_insumos a,aba_producto b,pac_paciente c " +
                          "where a.ATE_INS_CODIGO=b.FLD_PRODUCTOCODIGO " +
                            "and b.FAMPRDCOD='01' and b.GRPPRDCOD='11' and b.SUBGRPCOD='44' " +
                            "and trunc (a.ATE_INS_FECHAENTRE) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                            "and a.ATE_INS_NUMERPACIE=c.PAC_PAC_NUMERO " +
                            "AND A.ATE_INS_VIGENCIA<>'N' " +
                            "and a.ate_ins_numerpacie not in ('1308', '5024') " +
                            "and (C.PAC_PAC_RUT)='" + Historia + "' " +
                     "ORDER BY 3,4,7 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }
        
        public DataSet PacientesAntibioticos(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select distinct c.PAC_PAC_RUT IDENTIFICACION,nomcompletopaciente(A.ATE_INS_NUMERPACIE) PACIENTE,a.ATE_INS_CODIGO CODIGO,b.FLD_PRODUCTOGLOSA ANTIBIOTICO " +
                          "from ate_insumos a,aba_producto b,pac_paciente c " +
                          "where a.ATE_INS_CODIGO=b.FLD_PRODUCTOCODIGO " +
                            "and b.FAMPRDCOD='01' and b.GRPPRDCOD='11' and b.SUBGRPCOD='44' " +
                            "and trunc (a.ATE_INS_FECHAENTRE) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                            "and a.ATE_INS_NUMERPACIE=c.PAC_PAC_NUMERO " +
                            "AND A.ATE_INS_VIGENCIA<>'N' " +
                            "and a.ate_ins_numerpacie not in ('1308', '5024') " +
                     "ORDER BY 2,3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet RotacionMedicamento(string Codigo, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_Codigo",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                Codigo,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = string.Empty;

            if (Codigo == "            ")
            {
                sql = "select a.ATE_INS_CODIGO CODIGO,c.FLD_PRODUCTOGLOSA MEDICAMENTO,sum(a.ATE_INS_CANTIDAD- a.FRMCNTDEV) cantidad, d.OPA_VALORPROMEDIO cOSTO_promedio " +
                      "from ate_insumos a,aba_producto c,opa_bodprd d " +
                      "where a.ATE_INS_CODIGO=c.FLD_PRODUCTOCODIGO " +
                        "and trunc(a.ATE_INS_FECHAENTRE) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                        "and a.ATE_INS_CODIGO=d.FLD_PRODUCTOCODIGO " +
                        "And c.GRPPRDCOD='11' " +
                        "and A.ATE_INS_NUMERPACIE not in ('1308','5024') " +
                        "AND A.ATE_INS_VIGENCIA<>'N' " +
                  "group by a.ATE_INS_CODIGO,d.OPA_VALORPROMEDIO,c.FLD_PRODUCTOGLOSA " +
                  "ORDER BY 2 ";
                /*adicionar check con grupo 11 y 26 medicamentos, 25,12,14,13,18 insumos */
                /*incluir lo consumible no facturable, como los anestesicos*/
            }
            else
            {
                sql = "select a.ATE_INS_CODIGO CODIGO,c.FLD_PRODUCTOGLOSA MEDICAMENTO,sum(a.ATE_INS_CANTIDAD- a.FRMCNTDEV) cantidad, d.OPA_VALORPROMEDIO cOSTO_promedio " +
                      "from ate_insumos a,aba_producto c,opa_bodprd d " +
                      "where a.ATE_INS_CODIGO=c.FLD_PRODUCTOCODIGO " +
                        "and trunc(a.ATE_INS_FECHAENTRE) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                        "and a.ATE_INS_CODIGO=d.FLD_PRODUCTOCODIGO " +
                        "And c.GRPPRDCOD='11' " +
                        "AND A.ATE_INS_VIGENCIA<>'N' " +
                        "and A.ATE_INS_NUMERPACIE not in ('1308','5024') " +
                        "and a.ATE_INS_CODIGO = '" + Codigo + "' " +
                  "group by a.ATE_INS_CODIGO,d.OPA_VALORPROMEDIO,c.FLD_PRODUCTOGLOSA " +
                  "ORDER BY 2 ";

            }

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet PacientesServicioHOSP(string Servicio, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_Servicio",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                Servicio,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select e.PAC_PAC_RUT identificacion,nomcompletopaciente(A.PAC_PAC_NUMERO) paciente,b.SER_OBJ_CODIGO cama,b.RPA_FDA_HORAINGRESO FECHAINGRESO, b.RPA_FDA_HORAEGRESO FECHAEGRESO, " +
                                "c.FECPRC fechaalta,c.DIA_DIA_CODIGO dxalta,f.DIA_DIA_DESCRIPCIO diagnostico,i.CON_CON_DESCRIPCIO convenio,a.ATC_EST_NUMERO estadia, Reapertura " + 
                         "from atc_estadia a,atc_ocupacama b,tabaltord c,pac_paciente e,dia_diagnos f,tabaltcnd g,tabdstalt h,con_convenio i,ser_objetos j,(select distinct K.ATC_EST_NUMERO estadia, 'SI' Reapertura from tabrepthst k) " +
                        "where b.PAC_PAC_NUMERO=e.PAC_PAC_NUMERO " +
                            "and trunc (b.RPA_FDA_HORAEGRESO) between to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "and a.SER_OBJ_CODIGO <> '        '   and b.SER_OBJ_CODIGO=J.SER_OBJ_CODIGO " +
                            "and J.SER_SER_CODIGO='" + Servicio + "' " +
                            "and a.ATC_EST_FECEGRESO <> to_date('1900/01/01','yyyy/mm/dd') and a.ATC_EST_NUMERO=c.ATC_EST_NUMERO " +
                            "and A.ATC_EST_NUMERO = estadia(+) and e.PAC_PAC_NUMERO not in ('1308','5024') " +
                            "and a.ATC_EST_NUMERO=b.ATC_EST_NUMERO  and a.PAC_PAC_NUMERO=b.PAC_PAC_NUMERO " +
                            "and c.DIA_DIA_CODIGO=f.DIA_DIA_CODIGO and c.ALTCNDCOD=g.ALTCNDCOD " +
                            "and c.DSTALTCOD=h.DSTALTCOD and a.CON_CON_CODIGO=i.CON_CON_CODIGO " +
                        "order by 2,4";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet PacientesxInsumo(string Insumo, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_Insumo",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                Insumo,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select c.ATE_INS_FECHAENTRE fecHA,b.PAC_PAC_RUT identificacion,nomcompletopaciente(C.ATE_INS_NUMERPACIE) paciente,B.PAC_PAC_FONO TELEFONO,d.CON_CON_DESCRIPCIO convenio,E.FLD_PRODUCTOCODIGO cod_INSUMO,E.INS_INS_DESCRIPCIO INSUMO,c.ATE_INS_MONTOTARIF tarifa, (C.ATE_INS_CANTIDAD-C.FRMCNTDEV) cantidad " +
                           "from pac_paciente b,ate_INSUMOS c,con_convenio d,ins_insumos e " +
                          "where b.PAC_PAC_NUMERO=c.ATE_INS_NUMERPACIE and c.CON_CON_CODIGO=d.CON_CON_CODIGO and C.ATE_INS_CODIGO =E.FLD_PRODUCTOCODIGO and C.ATE_INS_VIGENCIA<>'N' " +
                            "and B.PAC_PAC_NUMERO not in ('5024', '1308') and trunc(c.ATE_INS_FECHAENTRE) between  to_date('" + FechaIni + "','yyyy/mm/dd') and to_date('" + FechaFin + "','yyyy/mm/dd') " +
                            "and C.ATE_INS_CODIGO = '" + Insumo + "' " +
                            "order by 3,1 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
            /*que busque por cualquier codigo insumo*/
            /*quitar telefono y tarifa*/
            /*que agrupe por fecha, no tener en cuenta la hora*/
            /*ademas adicionar la cama donde esta, y si el paciente ya egreso la cama de donde salio, adicionar fecha y hora de egreso*/
        }

        public DataSet PacientesXAntibioticosXServicio(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select distinct c.PAC_PAC_RUT IDENTIFICACION,nomcompletopaciente(A.ATE_INS_NUMERPACIE) PACIENTE,a.ATE_INS_CODIGO CODIGO,b.FLD_PRODUCTOGLOSA ANTIBIOTICO,E.SER_SER_CODIGO cod_servicio,E.SER_SER_DESCRIPCIO servicio " +
                           "from ate_insumos a,aba_producto b,pac_paciente c, atc_ocupacama d, ser_servicios e " +
                          "where a.ATE_INS_CODIGO=b.FLD_PRODUCTOCODIGO and b.FAMPRDCOD='01' and b.GRPPRDCOD='11' and b.SUBGRPCOD='44'  and D.SER_SER_CODIGO=E.SER_SER_CODIGO and a.ATE_INS_NUMERPACIE=c.PAC_PAC_NUMERO " +
                            "AND A.ATE_INS_VIGENCIA<>'N' and a.ate_ins_numerpacie not in ('1308', '5024') and A.ATE_INS_NUMERPACIE=D.PAC_PAC_NUMERO and A.ATC_EST_NUMERO=D.ATC_EST_NUMERO " +
                            "and trunc(D.RPA_FDA_HORAINGRESO) >=to_date ('" + FechaIni + "','yyyy/mm/dd')  and trunc(D.RPA_FDA_HORAEGRESO) <= to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                            "union all " +
                            "select distinct c.PAC_PAC_RUT IDENTIFICACION,nomcompletopaciente(A.ATE_INS_NUMERPACIE) PACIENTE,a.ATE_INS_CODIGO CODIGO,b.FLD_PRODUCTOGLOSA ANTIBIOTICO, '001     ' COD_SERVICIO, 'URGENCIAS                                                                                           ' SERVICIO " +
                              "from ate_insumos a,aba_producto b,pac_paciente c " +
                             "where a.ATE_INS_CODIGO=b.FLD_PRODUCTOCODIGO and b.FAMPRDCOD='01' and b.GRPPRDCOD='11' and b.SUBGRPCOD='44' " +
                               "and trunc (a.ATE_INS_FECHAENTRE) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') and a.ATE_INS_NUMERPACIE=c.PAC_PAC_NUMERO " +
                               "AND A.ATE_INS_VIGENCIA<>'N' and a.ate_ins_numerpacie not in ('1308', '5024') and A.ATC_EST_NUMERO='0' " +
                            "ORDER BY 2,3 ";

            /*adiconar b.SUBGRPCOD=43 */
            /*y quitar el codigo del servicio*/
            /*quitar paciente*/
            /*el nombre del reporte es: consumo antibioticos por servicio*/

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet PacientesXFamiliaMedicamentos(string Familia, string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_Familia",
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                Familia,
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "select distinct c.PAC_PAC_RUT IDENTIFICACION,nomcompletopaciente(A.ATE_INS_NUMERPACIE) PACIENTE,a.ATE_INS_CODIGO CODIGO,b.FLD_PRODUCTOGLOSA MEDICAMENTO, decode(b.SUBGRPCOD,'43','ANTIINFECCIOSO', DECODE(b.SUBGRPCOD,'44','ANTIMICROBIANOS','SANGRE') ) FAMILIA " +
                          "from ate_insumos a,aba_producto b,pac_paciente c " +
                          "where a.ATE_INS_CODIGO=b.FLD_PRODUCTOCODIGO " +
                            "and b.FAMPRDCOD='01' and b.GRPPRDCOD='11' and b.SUBGRPCOD='" + Familia + "' " +
                            "and trunc (a.ATE_INS_FECHAENTRE) between to_date ('" + FechaIni + "','yyyy/mm/dd') and to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                            "and a.ATE_INS_NUMERPACIE=c.PAC_PAC_NUMERO " +
                            "AND A.ATE_INS_VIGENCIA<>'N' " +
                            "and a.ate_ins_numerpacie not in ('1308', '5024') " +
                     "ORDER BY 2,3 ";

            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

        public DataSet AntibioticosXServicio(string FechaIni, string FechaFin)
        {
            string[] nomParam = 
            {
                "In_FechaIni",
                "In_FechaFin"
            };

            object[] vlrParam = 
            {
                FechaIni,
                FechaFin
            };

            OracleType[] typParam = { OracleType.VarChar, OracleType.VarChar };
            ParameterDirection[] DirParam = { ParameterDirection.Input, ParameterDirection.Output };

            string sql = "SELECT CODIGO, ANTIBIOTICO,COD_SERVICIO,SERVICIO,DECODE(CANTIDADDEV,null, CANTID,(CANTID-CANTIDADDEV)) CANTIDAD " +
                          " FROM (select  E.FLD_PRODUCTOCODIGO CODIGO,b.FLD_PRODUCTOGLOSA ANTIBIOTICO, D.SER_SER_CODIGO COD_SERVICIO,F.SER_SER_DESCRIPCIO SERVICIO, sum(A.FLD_CANTDESPACHADA) CANTID " +
                               " from tabfrmdetalle a,ins_insumos e ,aba_producto b, atc_ocupacama d, ser_servicios f " +
                               " where trunc(A.FECPRC ) >=to_date ('" + FechaIni + "','yyyy/mm/dd')  and trunc(A.FECPRC) <= to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                               " and A.INS_INS_CODIGINSUM = E.INS_INS_CODIGINSUM and E.FLD_PRODUCTOCODIGO=B.FLD_PRODUCTOCODIGO and b.FAMPRDCOD='01' and b.GRPPRDCOD='11' and b.SUBGRPCOD='44' " +
                               " and A.FECPRC  between D.RPA_FDA_HORAINGRESO and D.RPA_FDA_HORAEGRESO " +
                               " and A.PAC_PAC_NUMERO=D.PAC_PAC_NUMERO and D.SER_SER_CODIGO=F.SER_SER_CODIGO " +
                               " GROUP BY E.FLD_PRODUCTOCODIGO,b.FLD_PRODUCTOGLOSA , D.SER_SER_CODIGO ,F.SER_SER_DESCRIPCIO), " +
                               " (select  E.FLD_PRODUCTOCODIGO CODIGDEV, D.SER_SER_CODIGO COD_SERVICIODEV, sum(C.FRMCNTDEV) CANTIDADDEV " +
                               " from tabDEVPRD a,ins_insumos e ,aba_producto b, atc_ocupacama d,  TABDEVPRDDET C " +
                               " where trunc(A.FRMFECDEV ) >=to_date ('" + FechaIni + "','yyyy/mm/dd')  and trunc(A.FRMFECDEV) <= to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                               " AND A.FRMCORRELATIVO=C.FRMCORRELATIVO AND A.RPA_FOR_NUMERFORMU=C.RPA_FOR_NUMERFORMU AND A.RPA_FOR_TIPOFORMU=C.RPA_FOR_TIPOFORMU AND A.TIPOFORMUCODIGO=C.TIPOFORMUCODIGO " +
                               " and C.FLD_PRODUCTOCODIGO = E.FLD_PRODUCTOCODIGO and E.FLD_PRODUCTOCODIGO=B.FLD_PRODUCTOCODIGO and b.FAMPRDCOD='01' and b.GRPPRDCOD='11' and b.SUBGRPCOD='44' " +
                               " and A.FRMFECDEV  between D.RPA_FDA_HORAINGRESO and D.RPA_FDA_HORAEGRESO " +
                               " and A.PAC_PAC_NUMERO=D.PAC_PAC_NUMERO " +
                               " GROUP BY E.FLD_PRODUCTOCODIGO,b.FLD_PRODUCTOGLOSA , D.SER_SER_CODIGO) " +
                          " WHERE CODIGDEV(+) = CODIGO " +
                          " AND COD_SERVICIODEV(+)=COD_SERVICIO " +
                        " UNION ALL " +
                        " SELECT CODIGO, ANTIBIOTICO,COD_SERVICIO,SERVICIO,DECODE(CANTIDADDEV,null, CANTID,(CANTID-CANTIDADDEV)) CANTIDAD " +
                          " FROM (select  E.FLD_PRODUCTOCODIGO CODIGO,b.FLD_PRODUCTOGLOSA ANTIBIOTICO, D.SER_SER_CODIGO COD_SERVICIO,F.SER_SER_DESCRIPCIO SERVICIO, sum(A.FLD_CANTDESPACHADA) CANTID " +
                               " from tabfrmdetalle a,ins_insumos e ,aba_producto b, atc_ocupacama d, ser_servicios f " +
                               " where trunc(A.FECPRC ) >=to_date ('" + FechaIni + "','yyyy/mm/dd')  and trunc(A.FECPRC) <= to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                               " and A.INS_INS_CODIGINSUM = E.INS_INS_CODIGINSUM and E.FLD_PRODUCTOCODIGO=B.FLD_PRODUCTOCODIGO  and b.GRPPRDCOD='11' and b.SUBGRPCOD='44' " +
                               " AND E.FLD_PRODUCTOCODIGO IN ('1144030008  ', '1144030004  ','1144030005  ') " +
                               " and A.FECPRC  between D.RPA_FDA_HORAINGRESO and D.RPA_FDA_HORAEGRESO " +
                               " and A.PAC_PAC_NUMERO=D.PAC_PAC_NUMERO and D.SER_SER_CODIGO=F.SER_SER_CODIGO " +
                               " GROUP BY E.FLD_PRODUCTOCODIGO,b.FLD_PRODUCTOGLOSA , D.SER_SER_CODIGO ,F.SER_SER_DESCRIPCIO), " +
                               " (select  E.FLD_PRODUCTOCODIGO CODIGDEV, D.SER_SER_CODIGO COD_SERVICIODEV, sum(C.FRMCNTDEV) CANTIDADDEV " +
                               " from tabDEVPRD a,ins_insumos e ,aba_producto b, atc_ocupacama d,  TABDEVPRDDET C " +
                               " where trunc(A.FRMFECDEV ) >=to_date ('" + FechaIni + "','yyyy/mm/dd')  and trunc(A.FRMFECDEV) <= to_date ('" + FechaFin + "','yyyy/mm/dd') " +
                               " AND A.FRMCORRELATIVO=C.FRMCORRELATIVO AND A.RPA_FOR_NUMERFORMU=C.RPA_FOR_NUMERFORMU AND A.RPA_FOR_TIPOFORMU=C.RPA_FOR_TIPOFORMU AND A.TIPOFORMUCODIGO=C.TIPOFORMUCODIGO " +
                               " and C.FLD_PRODUCTOCODIGO = E.FLD_PRODUCTOCODIGO and E.FLD_PRODUCTOCODIGO=B.FLD_PRODUCTOCODIGO and b.GRPPRDCOD='11' and b.SUBGRPCOD='44' " +
                               " and A.FRMFECDEV  between D.RPA_FDA_HORAINGRESO and D.RPA_FDA_HORAEGRESO " +
                               " AND C.FLD_PRODUCTOCODIGO IN ('1144030008  ', '1144030004  ','1144030005  ') " +
                               " and A.PAC_PAC_NUMERO=D.PAC_PAC_NUMERO " +
                               " GROUP BY E.FLD_PRODUCTOCODIGO,b.FLD_PRODUCTOGLOSA , D.SER_SER_CODIGO) " +
                            " WHERE CODIGDEV(+) = CODIGO " +
                            " AND COD_SERVICIODEV(+)=COD_SERVICIO " +
                        " ORDER BY 2,3 ";


            return new DA.DA("Oracle").getDataset(sql, nomParam, vlrParam, typParam, DirParam);
        }

    }
}
